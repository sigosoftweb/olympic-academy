-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 26, 2021 at 02:22 PM
-- Server version: 10.3.20-MariaDB-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ourwork1_olympic`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` mediumtext NOT NULL,
  `type` enum('admin','subadmin') NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `name`, `username`, `password`, `type`, `timestamp`) VALUES
(1, 'ADMIN', 'admin@olympic.com', '4441e5d70b3657900fa57e66db407e0b', 'admin', '2020-06-04 05:52:34');

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `att_id` bigint(20) NOT NULL,
  `cb_id` bigint(20) NOT NULL,
  `added_by` enum('admin','subadmin','trainer') NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_by` enum('admin','subadmin','trainer') NOT NULL,
  `edited_subadmin` int(11) NOT NULL,
  `added_trainer` int(11) NOT NULL,
  `edited_trainer` int(11) NOT NULL,
  `date` date NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`att_id`, `cb_id`, `added_by`, `added_subadmin`, `edited_by`, `edited_subadmin`, `added_trainer`, `edited_trainer`, `date`, `timestamp`) VALUES
(1, 17, 'admin', 0, 'admin', 0, 0, 0, '2020-08-18', '2020-08-18 10:05:53'),
(2, 12, 'admin', 0, 'admin', 0, 0, 0, '2020-08-18', '2020-08-18 10:06:56'),
(3, 19, 'admin', 0, 'admin', 0, 0, 0, '2020-08-24', '2020-08-24 10:21:59'),
(4, 19, 'admin', 0, 'admin', 0, 0, 0, '2020-08-17', '2020-08-24 10:22:34'),
(5, 19, 'admin', 0, 'admin', 0, 0, 0, '2020-08-15', '2020-08-24 10:31:38'),
(6, 19, 'admin', 0, 'admin', 0, 0, 0, '2020-08-13', '2020-08-24 10:31:49'),
(7, 15, 'admin', 0, 'admin', 0, 0, 0, '2020-09-05', '2020-09-05 10:31:00'),
(8, 15, 'admin', 0, 'admin', 0, 0, 0, '2020-09-03', '2020-09-05 10:31:10'),
(9, 15, 'admin', 0, 'admin', 0, 0, 0, '2020-09-01', '2020-09-05 10:31:19');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_name_arabic` varchar(200) NOT NULL,
  `cat_image` text NOT NULL,
  `cat_status` tinyint(1) NOT NULL DEFAULT 1,
  `added_by` enum('admin','subadmin') NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_by` enum('admin','subadmin') NOT NULL,
  `edited_subadmin` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`, `cat_name_arabic`, `cat_image`, `cat_status`, `added_by`, `added_subadmin`, `edited_by`, `edited_subadmin`, `timestamp`) VALUES
(1, 'Programming languages', 'لغات البرمجة', 'uploads/categories/202007033682slide-1.jpg', 1, 'admin', 0, 'admin', 0, '2020-08-19 04:30:17'),
(3, 'Machine learning', 'التعلم الالي', 'uploads/categories/202007033682slide-1.jpg', 1, 'admin', 0, 'admin', 0, '2020-08-19 04:30:47'),
(4, 'Management', 'إدارة', 'uploads/categories/202007033682slide-1.jpg', 1, 'admin', 0, 'admin', 0, '2020-08-19 04:31:19');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` bigint(20) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `message` mediumtext NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contact_id`, `firstname`, `lastname`, `mobile`, `email`, `message`, `timestamp`) VALUES
(1, 'evan', 'm', '99999999', 'evan@test.com', 'test', '2020-07-21 11:45:13'),
(3, 'test', 'test', '99999999', 'info@sigosoft.com', 'test message', '2020-07-21 11:54:36'),
(4, 'test', 'test', '22222222', 'info@sigosoft.com', 'test', '2020-07-21 12:00:39'),
(5, 'athul', 'baby', '33137593', 'athulbaby@gmail.com', 'this is a test message', '2020-07-21 15:32:30'),
(6, 'test', 'almosawi', '33977400', 'zalmosawi@boc.bh', 'test msg', '2020-07-29 11:59:39'),
(7, 'Test', 't', '78521478', 'test@gmail.com', 'What about the value of certification', '2020-08-24 09:18:07'),
(8, 'yuhj', 'dgg', '15111102', 'hjb@gmail.com', 'gnn', '2020-08-24 10:47:43');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `course_id` int(11) NOT NULL,
  `course_title` varchar(200) NOT NULL,
  `course_description` text NOT NULL,
  `course_title_arabic` varchar(200) NOT NULL,
  `course_description_arabic` text NOT NULL,
  `cat_id` int(11) NOT NULL,
  `course_starting_date` date NOT NULL,
  `course_duration` varchar(100) NOT NULL,
  `course_duration_arabic` varchar(200) NOT NULL,
  `course_sale_price` double NOT NULL,
  `course_price` double NOT NULL,
  `course_banner` varchar(200) NOT NULL,
  `course_status` tinyint(1) NOT NULL DEFAULT 1,
  `complete` tinyint(1) NOT NULL DEFAULT 0,
  `student_limit` int(11) NOT NULL,
  `organiser_id` int(11) NOT NULL DEFAULT 0,
  `sequence_id` bigint(20) NOT NULL DEFAULT 0,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `added_by` enum('a','t','s') NOT NULL,
  `trainer_id` int(11) NOT NULL,
  `edited_by` enum('a','t','s') NOT NULL,
  `edited_trainer` int(11) NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_subadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`course_id`, `course_title`, `course_description`, `course_title_arabic`, `course_description_arabic`, `cat_id`, `course_starting_date`, `course_duration`, `course_duration_arabic`, `course_sale_price`, `course_price`, `course_banner`, `course_status`, `complete`, `student_limit`, `organiser_id`, `sequence_id`, `timestamp`, `added_by`, `trainer_id`, `edited_by`, `edited_trainer`, `added_subadmin`, `edited_subadmin`) VALUES
(2, 'Physical literacy', 'Physical literacy for School Improve Movement Skills- Through fun games and activities, our particpants develop fundamental movement skills', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 4, '2020-06-12', '12 Months', '3 اشهر', 400, 0, 'uploads/courses/20200726741843554.jpg', 1, 0, 100, 2, 0, '2020-08-19 05:16:38', 'a', 0, 's', 0, 0, 12),
(3, 'PHP Training', 'PHP is a popular general-purpose scripting language that is especially suited to web development. ', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 3, '2020-06-15', '10 Months', '3 اشهر', 150, 120, 'uploads/courses/202006292891cou2.jpg', 0, 0, 50, 0, 0, '2020-08-19 05:16:38', 'a', 0, 'a', 0, 0, 0),
(4, 'ASP.NET', 'ASP.NET is an open-source, server-side web-application framework that is especially suited to web development', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 4, '2020-06-18', '2month', '3 اشهر', 700, 700, 'uploads/courses/202006293506cou3.jpg', 0, 0, 50, 0, 0, '2020-08-19 05:16:38', 'a', 0, 't', 6, 0, 0),
(5, 'Human Resource', 'Human Resource Management is the strategic approach to the effective management of people in a company', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 4, '2020-06-28', '1 month', '3 اشهر', 120, 100, 'uploads/courses/202006292582cou4.jpg', 0, 0, 50, 0, 0, '2020-08-19 05:16:38', '', 0, '', 0, 0, 0),
(6, 'test', 'test', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 4, '2020-07-18', '1', '3 اشهر', 123, 123, 'uploads/courses/2020080921892.jpg', 0, 0, 50, 0, 0, '2020-08-19 05:16:38', 'a', 0, 's', 0, 0, 12),
(7, 'Human psychology ', 'Human Psychology is the science of mind and human behavior. The general definition may refer to the profession, also referred to as clinical psychology; the scholarly discipline, referred to as academic psychology or educational psychology; or the scientific pursuit, research psychology.\r\n\r\nA learner pursuing a psychology major may later find work as a psychologist, or specifically as a child psychologist. He or she may specialize in any number of psychology jobs. Such specialties include forensic psychology, developmental psychology, cognitive psychology, social psychology, health psychology, and positive psychology.', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 4, '2020-07-12', '3 Weeks', '3 اشهر', 0.1, 200, 'uploads/courses/202007177107Untitled-1765.jpg', 1, 0, 50, 1, 0, '2020-09-03 09:47:57', 'a', 0, 'a', 0, 0, 0),
(8, 'National Coaching Certification Program (NCCP) – Level 1', 'The aim of this program is to:\r\n• Prepare and train national coaches in the field of sports coaching, \r\n• provide them with the latest information in the field of coaching, and \r\n• expand the number of qualified coaches in the Kingdom.\r\nThis program will enable these coaches to lead their clubs and national teams, prepare them as part of a comprehensive, professional and integrated plan, physically, technically, and mentally.', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 4, '2020-07-31', '8 days', '3 اشهر', 0.1, 10, 'uploads/courses/202007178428Untitled-1e234.jpg', 1, 0, 50, 1, 0, '2021-03-22 05:43:08', 'a', 0, 's', 0, 0, 12),
(9, 'National Coaching Certification Program (NCCP) – Level 2', 'The aim of this program is to:\r\n• Prepare and train national coaches in the field of sports coaching, \r\n• provide them with the latest information in the field of coaching, and \r\n• expand the number of qualified coaches in the Kingdom.\r\nThis program will enable these coaches to lead their clubs and national teams, prepare them as part of a comprehensive, professional and integrated plan, physically, technically, and mentally.\r\n\r\nContents of the National Coaching Certification Program (NCCP):\r\nThe program consists of 4 levels. The first three levels include the theoretical, specialized and practical parts. The fourth level also includes the preparation of a case study on a Sport Specific project, as well as preparing and presenting the Annual Training Plan (ATP) which will be discussed, evaluated and approved by a panel of academic supervisors and technical evaluators.\r\n', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 4, '2020-08-12', '2 Weeks', '3 اشهر', 600, 60, 'uploads/courses/202008072805202007255536B69A7263.jpg', 1, 0, 50, 1, 8, '2020-08-19 05:16:38', 'a', 0, 's', 0, 0, 12),
(10, 'National Coaching Certification Program (NCCP) – Level 3', 'Objectives of the National Coaching Certification Program (NCCP):\r\n\r\nThe aim of this program is to:\r\n• Prepare and train national coaches in the field of sports coaching, \r\n• provide them with the latest information in the field of coaching, and \r\n• expand the number of qualified coaches in the Kingdom.\r\nThis program will enable these coaches to lead their clubs and national teams, prepare them as part of a comprehensive, professional and integrated plan, physically, technically, and mentally.\r\n\r\n\r\nContents of the National Coaching Certification Program (NCCP):\r\nThe program consists of 4 levels. The first three levels include the theoretical, specialized and practical parts. The fourth level also includes the preparation of a case study on a Sport Specific project, as well as preparing and presenting the Annual Training Plan (ATP) which will be discussed, evaluated and approved by a panel of academic supervisors and technical evaluators.\r\n', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 4, '2020-08-24', '2 Weeks', '3 اشهر', 600, 60, 'uploads/courses/202008076346202007258796level3.jpg', 1, 0, 100, 1, 9, '2020-08-19 05:16:38', 'a', 0, 's', 0, 0, 12),
(13, 'Advanced Sports Management Diploma', 'This program aims to prepare participants to enter the field of sports management by providing them with the necessary skills and expertise to work in this sector.\r\nThe participant in the program benefits from the expertise of local and international lecturers in understanding of the main areas related to the field of sports management. He or she is able to enter the advanced sport management programs providing he/she passes the introductory program first.', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 4, '2020-08-02', 'one year', '3 اشهر', 2600, 50, 'uploads/courses/202008073436202007255174Picture3.jpg', 1, 0, 50, 2, 0, '2020-08-19 05:16:38', 'a', 0, 's', 0, 0, 12),
(14, 'Advanced/Specialized programs for elite coaches', 'These advanced/specialized courses provide an opportunity for coaches who have completed the fourth level of NCCP, elite coaches and national team’s coaches, to learn about the latest scientific and practical innovations/developments in the field of coaching as presented by international and world experts in a 5 day workshop.', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 2, '2020-07-23', '5 Days', '3 اشهر', 200, 150, 'uploads/courses/20200805433818.jpg', 1, 0, 50, 2, 0, '2020-08-19 05:16:38', 'a', 0, 's', 0, 0, 12),
(15, 'Program of preparation of lecturers', 'This is a scientific and sport course spanning over 5 days per NCCP Level. Each course is run and taught by experts from the Coaching Association of Canada (CAC). It covers the scientific curricula and the art of practical teaching in the field of sports, in a systematic scientific method that complies with the requirements of modern teaching.\r\nThe aim of this program is to prepare and certify local lecturers to enable them to deliver/teach in the National Coaching Certification Program (NCCP), Levels 1, 2 & 3.\r\n', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 4, '2020-07-19', '5 Days', '3 اشهر', 300, 200, 'uploads/courses/202007174577Untitled-1e234.jpg', 1, 0, 50, 2, 0, '2020-08-19 05:16:38', 'a', 0, 'a', 0, 0, 0),
(16, 'Program for dynamic movement analysis of sports using the computer based DARTFISH', 'Dartfish is a video-analysis solution that allows sports analysts to capture, analyze and share videos of training sessions and matches. The software offers tools to capture the footage directly into the platform, tag events real-time, and upload, organize and share the various videos produced.\r\nAs part of the work methodology at the Bahrain Olympic Academy for the development of coaches and the development of sports excellence, and as part of the agreement that was signed with DARTFISH institution in Switzerland, courses have been conducted since 2006 to teach the DARTFISH program. In these courses, coaches are trained in various fields of sports and coaching analysis skills, measuring the development of the player and analyzing games. The DARTFISH is used by many national and international bodies, sports establishments, professional teams, universities, academies, as well as elite coaches and athletes, to portray and analyze motor performance in all games and sports, both in training and competitions.\r\nThis course is officially approved by the company that produces the program \"DARTFISH\" in Switzerland.\r\n', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 1, '2020-07-25', '5 days', '3 اشهر', 0.1, 50, 'uploads/courses/20200805498217.jpg', 1, 0, 50, 2, 0, '2020-08-28 09:48:54', 'a', 0, 's', 0, 0, 12),
(17, 'Cardiopulmonary Resuscitation (CPR & AED) Course', 'In order to address one of the initiatives of His Highness Sheikh Nasser bin Hamad Al Khalifa in reducing the phenomenon of sudden death among athletes and studying the causes, the Bahrain Olympic Academy in coordination with the US Navy Base, organizes a formal 2 day workshop to train and qualify technical and administrative cadres in various sports organizations in the Kingdom of Bahrain (Federations, Clubs, Private gyms, ..etc.), in Cardiopulmonary Resuscitation (CPR & AED). \r\nAt the end of the workshop the participants get a license from the International Red Cross to practice the profession. This license is valid for two years.\r\nThe Bahrain Olympic Academy started organizing this course back in early 2018. It is held 2 to 3 times a year, depending on the number of registered participants.  \r\nThe aim of the Bahrain Olympic Academy is to eventually train and qualify all the concerned technical and administrative cadres in various sports organizations in the Kingdom of Bahrain.\r\n', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 4, '2020-07-25', '2 Days', '3 اشهر', 35, 35, 'uploads/courses/202007172589Untitled-1e234.jpg', 1, 0, 3, 2, 0, '2020-08-19 05:16:38', 'a', 0, 's', 0, 0, 12),
(18, 'National Coaching Certification Program (NCCP) – Level 4', 'Level 4 of the National Coaching Certification Program (NCCP) is the highest level in the program. Entry to this level requires completion and passing of the first three NCCP levels, and it takes a minimum of two years of study to complete. It is designed for coaches who work with elite athletes and national teams. ', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 4, '2020-08-09', '2 year', '3 اشهر', 2500, 2500, 'uploads/courses/202008084766.png', 1, 0, 11, 0, 10, '2020-08-19 05:16:38', 'a', 0, 'a', 0, 0, 0),
(19, 'Level 1 – Sports Management course.', 'The Sports Management Program provides training opportunities for the administrative cadres of the National Sports Federations, clubs and other entities related to the Bahraini and regional sports organizations.\r\nSince 2011, specialized training programs have been established for administrators working in the field of sport and sport management in the Kingdom of Bahrain and the GCC countries.\r\nThis program aims to prepare participants to enter the field of sports management by providing them with the necessary skills and expertise to work in this sector.\r\nThe participant in the program benefits from the expertise of local and international lecturers in understanding of the main areas related to the field of sports management. He or she is able to enter the advanced sport management programs providing he/she passes the introductory program first\r\n', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 4, '0000-00-00', '10 days', '3 اشهر', 0, 30, 'uploads/courses/202008082018.png', 1, 0, 70, 0, 0, '2020-08-19 05:16:38', 'a', 0, 's', 0, 0, 12),
(20, 'test arabic', 'here we can add details ', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', 4, '2020-08-17', '3 Weeks', '3 اشهر', 50, 500, 'uploads/courses/202008173250.png', 1, 0, 30, 0, 0, '2020-09-20 05:40:53', 'a', 0, 'a', 0, 0, 0),
(21, 'test', 'Testtttttttttttttttttttttttttttttttt', '', '', 4, '2020-08-24', '5', '', 50, 45, 'uploads/courses/202009015998202008272328Business Validation.png', 1, 0, 5, 0, 0, '2020-09-02 15:15:52', 'a', 0, 'a', 0, 0, 0),
(22, 'wqwe', 'sff', 'ربيبيسب', 'ربيبيسب', 1, '2021-03-21', '12', 'ربيبيسب', 12, 12, '', 1, 0, 12, 0, 0, '2021-03-21 07:28:48', 'a', 0, 'a', 0, 0, 0),
(23, 'Jeli', 'awesrdtfyguhijsdxfcgvbhnj', 'اكتملت الدورة', 'اكتملت الدورة', 4, '2002-02-22', '3 month', 'اكتملت الدورة', 200, 20, 'uploads/courses/202103247334.png', 1, 0, 50, 0, 2, '2021-03-24 09:38:05', 'a', 0, 'a', 0, 0, 0),
(24, 'xyzz', 'dsfsfsfsa', 'لابيلالايبلا', 'لابيلالايبلا', 1, '2021-03-25', '12', 'لابيلالايبلا', 12, 1, 'uploads/courses/202103258184.png', 1, 0, 2, 0, 0, '2021-03-25 08:47:56', 'a', 0, 'a', 0, 0, 0),
(25, 'test', 'Test ', 'امتحان', 'امتحان', 1, '2021-03-25', '30', 'امتحان', 35, 30, 'uploads/courses/202103255557.png', 1, 0, 3, 0, 9, '2021-03-25 10:35:53', 'a', 0, 'a', 0, 0, 0),
(26, 'gyhkjo', 'nklboub', 'ىءنسىنى', 'ىؤسنمىؤنىمن', 1, '2021-03-25', 'mxksn', 'ءسشء', 1, 0, 'uploads/courses/202103251305.png', 1, 0, 12, 0, 0, '2021-03-25 14:28:15', 'a', 0, 'a', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `course_assignments`
--

CREATE TABLE `course_assignments` (
  `ca_id` bigint(20) NOT NULL,
  `title` mediumtext NOT NULL,
  `title_arabic` varchar(200) NOT NULL,
  `attachment` mediumtext NOT NULL,
  `cb_id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `added_by` enum('admin','subadmin','trainer') NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `added_trainer` int(11) NOT NULL,
  `edited_by` enum('admin','subadmin','trainer') NOT NULL,
  `edited_subadmin` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_assignments`
--

INSERT INTO `course_assignments` (`ca_id`, `title`, `title_arabic`, `attachment`, `cb_id`, `course_id`, `added_by`, `added_subadmin`, `added_trainer`, `edited_by`, `edited_subadmin`, `timestamp`) VALUES
(4, 'Test assignment', 'عنوان عربي', '', 4, 3, 'admin', 0, 0, 'admin', 0, '2020-07-08 06:44:22'),
(5, 'Introduction to Human Resource', 'عنوان عربي', 'uploads/assignments/202007089910Olymbic  Subadmin.pdf', 8, 5, 'admin', 0, 0, 'admin', 0, '2020-07-08 09:45:15'),
(9, 'test2', 'عنوان عربي', 'uploads/assignments/202007105825AnyConv.com__753a7d71-9b25-416b-a0ac-acf9d7ad354d.jpg', 0, 3, 'admin', 0, 0, 'admin', 0, '2020-07-10 08:09:39'),
(10, 'test', 'عنوان عربي', 'uploads/assignments/202007137951TEST PDF.pdf', 5, 4, 'admin', 0, 0, 'admin', 0, '2020-07-13 05:27:50'),
(11, 'test2', 'عنوان عربي', 'uploads/assignments/202007134246TEST PDF.pdf', 5, 4, 'admin', 0, 0, 'admin', 0, '2020-07-13 05:30:15'),
(13, 'test12', '', 'uploads/assignments/202008244407Test .pdf', 19, 21, 'admin', 0, 0, 'admin', 0, '2020-08-24 10:23:45'),
(14, 'test', '', 'uploads/assignments/202009052469Image-1 - 2020-09-04T123748.167.jpg', 15, 7, 'admin', 0, 0, 'admin', 0, '2020-09-05 10:33:13');

-- --------------------------------------------------------

--
-- Table structure for table `course_attendance`
--

CREATE TABLE `course_attendance` (
  `ca_id` bigint(20) NOT NULL,
  `att_id` bigint(20) NOT NULL,
  `date` date NOT NULL,
  `stu_id` bigint(20) NOT NULL,
  `present` tinyint(1) NOT NULL DEFAULT 0,
  `absent` tinyint(1) NOT NULL DEFAULT 0,
  `late` tinyint(1) NOT NULL DEFAULT 0,
  `added_by` enum('admin','subadmin','trainer') NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_by` enum('admin','subadmin','trainer') NOT NULL,
  `edited_subadmin` int(11) NOT NULL,
  `added_trainer` int(11) NOT NULL,
  `edited_trainer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_attendance`
--

INSERT INTO `course_attendance` (`ca_id`, `att_id`, `date`, `stu_id`, `present`, `absent`, `late`, `added_by`, `added_subadmin`, `edited_by`, `edited_subadmin`, `added_trainer`, `edited_trainer`) VALUES
(9, 3, '2020-08-24', 44, 1, 0, 0, 'admin', 0, 'admin', 0, 0, 0),
(10, 4, '2020-08-17', 44, 1, 0, 0, 'admin', 0, 'admin', 0, 0, 0),
(11, 5, '2020-08-15', 44, 0, 1, 0, 'admin', 0, 'admin', 0, 0, 0),
(12, 6, '2020-08-13', 44, 0, 0, 1, 'admin', 0, 'admin', 0, 0, 0),
(13, 7, '2020-09-05', 44, 1, 0, 0, 'admin', 0, 'admin', 0, 0, 0),
(14, 8, '2020-09-03', 44, 0, 1, 0, 'admin', 0, 'admin', 0, 0, 0),
(15, 9, '2020-09-01', 44, 0, 0, 1, 'admin', 0, 'admin', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `course_batches`
--

CREATE TABLE `course_batches` (
  `cb_id` bigint(20) NOT NULL,
  `batch_name` varchar(200) NOT NULL,
  `batch_name_arabic` varchar(200) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `added_by` enum('a','t','s') NOT NULL,
  `trainer_id` int(11) NOT NULL,
  `edited_by` enum('a','t','s') NOT NULL,
  `edited_trainer` int(11) NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_subadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_batches`
--

INSERT INTO `course_batches` (`cb_id`, `batch_name`, `batch_name_arabic`, `course_id`, `timestamp`, `added_by`, `trainer_id`, `edited_by`, `edited_trainer`, `added_subadmin`, `edited_subadmin`) VALUES
(1, 'Morning batch', 'ما هو لوريم ايبسوم', 2, '2020-08-19 05:32:06', 'a', 0, 'a', 0, 0, 0),
(2, 'Evening batch', 'ما هو لوريم ايبسوم', 2, '2020-08-19 05:32:06', 'a', 0, 'a', 0, 0, 0),
(3, 'Holiday batch', 'ما هو لوريم ايبسوم', 2, '2020-08-19 05:32:06', 'a', 0, 'a', 0, 0, 0),
(4, 'Evening batch', 'ما هو لوريم ايبسوم', 3, '2020-08-19 05:32:06', 'a', 0, 'a', 0, 0, 0),
(5, 'A', 'ما هو لوريم ايبسوم', 4, '2020-08-19 05:32:06', 'a', 0, 'a', 0, 0, 0),
(6, 'B', 'ما هو لوريم ايبسوم', 4, '2020-08-19 05:32:06', 'a', 0, 'a', 0, 0, 0),
(7, 'c', 'ما هو لوريم ايبسوم', 4, '2020-08-19 05:32:06', 'a', 0, 'a', 0, 0, 0),
(8, 'feb 2020', 'ما هو لوريم ايبسوم', 5, '2020-08-19 05:32:06', 'a', 0, 'a', 0, 0, 0),
(9, 'A', 'ما هو لوريم ايبسوم', 10, '2020-08-19 05:32:06', 'a', 0, 'a', 0, 0, 0),
(10, 'B', 'ما هو لوريم ايبسوم', 10, '2020-08-19 05:32:06', 's', 0, 's', 0, 10, 10),
(11, 'B', 'ما هو لوريم ايبسوم', 5, '2020-08-19 05:32:06', 't', 6, 'a', 0, 0, 0),
(12, 'A', 'ما هو لوريم ايبسوم', 8, '2020-08-19 05:32:06', 'a', 0, 'a', 0, 0, 0),
(13, 'First batch', 'ما هو لوريم ايبسوم', 19, '2020-08-19 05:32:06', 'a', 0, 'a', 0, 0, 0),
(14, 'A', 'ما هو لوريم ايبسوم', 7, '2020-08-19 05:32:06', 'a', 0, 'a', 0, 0, 0),
(15, 'B', 'ما هو لوريم ايبسوم', 7, '2020-08-19 05:32:06', 'a', 0, 'a', 0, 0, 0),
(16, 'batch 1', 'ما هو لوريم ايبسوم', 20, '2020-08-19 05:32:06', 'a', 0, 'a', 0, 0, 0),
(17, 'first batch', 'ما هو لوريم ايبسوم', 8, '2020-08-19 05:32:06', 'a', 0, 'a', 0, 0, 0),
(18, 'mahmood ', '', 8, '2020-08-19 07:31:46', 'a', 0, 'a', 0, 0, 0),
(19, 'A', '', 21, '2020-08-24 09:57:18', 'a', 0, 'a', 0, 0, 0),
(20, 'A', '', 19, '2020-10-21 10:52:30', 'a', 0, 'a', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `course_batch_students`
--

CREATE TABLE `course_batch_students` (
  `cbs_id` bigint(20) NOT NULL,
  `cb_id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `stu_id` bigint(20) NOT NULL,
  `added_by` enum('admin','subadmin','trainer') NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `added_trainer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_batch_students`
--

INSERT INTO `course_batch_students` (`cbs_id`, `cb_id`, `course_id`, `stu_id`, `added_by`, `added_subadmin`, `added_trainer`) VALUES
(10, 18, 8, 10, 'admin', 0, 0),
(11, 18, 8, 9, 'admin', 0, 0),
(12, 19, 21, 44, 'admin', 0, 0),
(13, 15, 7, 44, 'admin', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `course_classes`
--

CREATE TABLE `course_classes` (
  `cc_id` bigint(20) NOT NULL,
  `topic` text NOT NULL,
  `topic_arabic` varchar(200) NOT NULL,
  `cb_id` bigint(20) NOT NULL,
  `cs_id` bigint(20) NOT NULL,
  `trainer_id` bigint(20) NOT NULL,
  `starting_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `duration` varchar(20) NOT NULL,
  `host_name` varchar(100) NOT NULL,
  `meeting_password` varchar(100) NOT NULL,
  `alternative_host` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `meeting_id` text NOT NULL,
  `start_url` text NOT NULL,
  `join_url` text NOT NULL,
  `created_by` enum('admin','teacher','subadmin') NOT NULL,
  `sub_id` bigint(20) NOT NULL DEFAULT 0,
  `tea_id` bigint(20) NOT NULL DEFAULT 0,
  `course_id` bigint(20) NOT NULL,
  `class_status` enum('pending','completed') NOT NULL DEFAULT 'pending',
  `admin_id` bigint(20) NOT NULL DEFAULT 0,
  `edited_by` enum('a','t','s') NOT NULL,
  `edited_trainer` int(11) NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_subadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_classes`
--

INSERT INTO `course_classes` (`cc_id`, `topic`, `topic_arabic`, `cb_id`, `cs_id`, `trainer_id`, `starting_time`, `duration`, `host_name`, `meeting_password`, `alternative_host`, `created_at`, `meeting_id`, `start_url`, `join_url`, `created_by`, `sub_id`, `tea_id`, `course_id`, `class_status`, `admin_id`, `edited_by`, `edited_trainer`, `added_subadmin`, `edited_subadmin`) VALUES
(4, 'Introduction to php', 'ما هو لوريم ايبسوم', 4, 5, 2, '2020-06-15 12:30:00', '60', 'Aithin EP', 'java-introduction', 'Suresh raina', '2020-06-13 11:46:30', '', '', '', 'admin', 0, 0, 3, 'completed', 1, 'a', 0, 0, 0),
(11, 'Testing zoom call integration', 'ما هو لوريم ايبسوم', 6, 11, 3, '2020-07-08 09:55:00', '60', 'Aithin EP', 'asaasas', 'athayyil@boc.bh', '2020-07-07 12:25:19', '89381923460', 'https://us02web.zoom.us/s/89381923460?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiIzQU5yRkJvbVJNT2w4ajREaXVubk1BIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6InJwc3RoS0NfRU1UbUZ6ZHFVdXQtWGdCcE9jY01uRDRjZmo3eUpMZWxKbWsuQmdVZ2RUbERSM1ZWVmt0cWRVSjBhMDVpVlM5R1kyVjROMEZwTmxvMlVsQlBTWHBBTlRaaU9ETXdNREZoTmpVNE1tUXdNekU1T1RGak9UUmtPRFkwT1dNNVl6UmhZVFZpT0RsalpXTTRaR1JoWXpFd016WTVabU0yWmpNME16azJOakUwWVFBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NDEzMTkxOSwiaWF0IjoxNTk0MTI0NzE5LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.cDvEabSzKAie4Ovjea5y0rheCSqZSa5RgyzYT3wQSwU', 'https://us02web.zoom.us/j/89381923460?pwd=YkZ4UDFnNElNQmlWbGRKNzBSQzNoQT09', 'admin', 0, 0, 4, 'completed', 1, 'a', 0, 0, 0),
(14, 'Intro', 'ما هو لوريم ايبسوم', 4, 12, 1, '2020-07-07 22:50:00', '10', '', '', '', '2020-07-08 03:47:04', '82464032431', 'https://us02web.zoom.us/s/82464032431?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiIzQU5yRkJvbVJNT2w4ajREaXVubk1BIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6InJwc3RoS0NfRU1UbUZ6ZHFVdXQtWGdCcE9jY01uRDRjZmo3eUpMZWxKbWsuQmdVZ2RUbERSM1ZWVmt0cWRVSjBhMDVpVlM5R1kyVjROMEZwTmxvMlVsQlBTWHBBTlRaaU9ETXdNREZoTmpVNE1tUXdNekU1T1RGak9UUmtPRFkwT1dNNVl6UmhZVFZpT0RsalpXTTRaR1JoWXpFd016WTVabU0yWmpNME16azJOakUwWVFBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NDE4NzIyNCwiaWF0IjoxNTk0MTgwMDI0LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.8P1wLS9TG1sdV5SNXK_s8TYCPNZ1cAdUHYACc_wLC-A', 'https://us02web.zoom.us/j/82464032431', 'admin', 0, 0, 3, 'pending', 1, 'a', 0, 0, 0),
(15, 'test', 'ما هو لوريم ايبسوم', 4, 12, 1, '2020-07-08 02:25:00', '10', '', '', '', '2020-07-08 03:48:27', '86183314117', 'https://us02web.zoom.us/s/86183314117?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiIzQU5yRkJvbVJNT2w4ajREaXVubk1BIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6InJwc3RoS0NfRU1UbUZ6ZHFVdXQtWGdCcE9jY01uRDRjZmo3eUpMZWxKbWsuQmdVZ2RUbERSM1ZWVmt0cWRVSjBhMDVpVlM5R1kyVjROMEZwTmxvMlVsQlBTWHBBTlRaaU9ETXdNREZoTmpVNE1tUXdNekU1T1RGak9UUmtPRFkwT1dNNVl6UmhZVFZpT0RsalpXTTRaR1JoWXpFd016WTVabU0yWmpNME16azJOakUwWVFBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NDE4NzMwNywiaWF0IjoxNTk0MTgwMTA3LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.y8Bkmqg3TDMZPq-JiOXGd1PziqDVb-ozzWz4ad_2Xus', 'https://us02web.zoom.us/j/86183314117', 'admin', 0, 0, 3, 'pending', 1, 'a', 0, 0, 0),
(19, 'Test  zoom', 'ما هو لوريم ايبسوم', 2, 4, 1, '2020-07-08 07:00:00', '10', '', '', '', '2020-07-08 11:57:46', '89110308238', 'https://us02web.zoom.us/s/89110308238?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiIzQU5yRkJvbVJNT2w4ajREaXVubk1BIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6InJwc3RoS0NfRU1UbUZ6ZHFVdXQtWGdCcE9jY01uRDRjZmo3eUpMZWxKbWsuQmdVZ2RUbERSM1ZWVmt0cWRVSjBhMDVpVlM5R1kyVjROMEZwTmxvMlVsQlBTWHBBTlRaaU9ETXdNREZoTmpVNE1tUXdNekU1T1RGak9UUmtPRFkwT1dNNVl6UmhZVFZpT0RsalpXTTRaR1JoWXpFd016WTVabU0yWmpNME16azJOakUwWVFBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NDIxNjY2NiwiaWF0IjoxNTk0MjA5NDY2LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.o9OUV_Fs-mSxhKp2vIauepp3EleYhuHnMbSlqD3qD6E', 'https://us02web.zoom.us/j/89110308238', 'admin', 0, 0, 2, 'completed', 1, 'a', 0, 0, 0),
(20, 'Polymorphism testing', 'ما هو لوريم ايبسوم', 2, 4, 1, '2020-07-08 10:00:00', '60', 'Aithin EP', '', '', '2020-07-08 12:02:24', '83824066122', 'https://us02web.zoom.us/s/83824066122?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiIzQU5yRkJvbVJNT2w4ajREaXVubk1BIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6InJwc3RoS0NfRU1UbUZ6ZHFVdXQtWGdCcE9jY01uRDRjZmo3eUpMZWxKbWsuQmdVZ2RUbERSM1ZWVmt0cWRVSjBhMDVpVlM5R1kyVjROMEZwTmxvMlVsQlBTWHBBTlRaaU9ETXdNREZoTmpVNE1tUXdNekU1T1RGak9UUmtPRFkwT1dNNVl6UmhZVFZpT0RsalpXTTRaR1JoWXpFd016WTVabU0yWmpNME16azJOakUwWVFBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NDIxNjk0NCwiaWF0IjoxNTk0MjA5NzQ0LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.YWwrubtbS-Mc04crlMczL9KhYVAoKMvM0VzSFqngDxg', 'https://us02web.zoom.us/j/83824066122', 'admin', 0, 0, 2, 'pending', 1, 'a', 0, 0, 0),
(21, 'First class ', 'ما هو لوريم ايبسوم', 8, 13, 6, '2020-07-10 01:45:00', '60', 'Aithin EP', 'java', 'aalomari@boc.bh', '2020-07-09 04:15:20', '84917308470', 'https://us02web.zoom.us/s/84917308470?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiIzQU5yRkJvbVJNT2w4ajREaXVubk1BIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6InJwc3RoS0NfRU1UbUZ6ZHFVdXQtWGdCcE9jY01uRDRjZmo3eUpMZWxKbWsuQmdVZ2RUbERSM1ZWVmt0cWRVSjBhMDVpVlM5R1kyVjROMEZwTmxvMlVsQlBTWHBBTlRaaU9ETXdNREZoTmpVNE1tUXdNekU1T1RGak9UUmtPRFkwT1dNNVl6UmhZVFZpT0RsalpXTTRaR1JoWXpFd016WTVabU0yWmpNME16azJOakUwWVFBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NDI3NTMxOSwiaWF0IjoxNTk0MjY4MTE5LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.pFldgsflqzEbhPxMd07Xng9-_PqC__IKUs8LPpZ9VSo', 'https://us02web.zoom.us/j/84917308470?pwd=WTEzM1FZb3hCcGE3MTN2VFZQR3VxQT09', 'admin', 0, 0, 5, 'pending', 1, 'a', 0, 0, 0),
(22, 'Polymorphism', 'ما هو لوريم ايبسوم', 8, 13, 6, '2020-07-09 04:04:26', '60', 'Aithin EP', 'asasas', '', '2020-07-09 09:46:46', '89172725667', 'https://us02web.zoom.us/s/89172725667?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiIzQU5yRkJvbVJNT2w4ajREaXVubk1BIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6InJwc3RoS0NfRU1UbUZ6ZHFVdXQtWGdCcE9jY01uRDRjZmo3eUpMZWxKbWsuQmdVZ2RUbERSM1ZWVmt0cWRVSjBhMDVpVlM5R1kyVjROMEZwTmxvMlVsQlBTWHBBTlRaaU9ETXdNREZoTmpVNE1tUXdNekU1T1RGak9UUmtPRFkwT1dNNVl6UmhZVFZpT0RsalpXTTRaR1JoWXpFd016WTVabU0yWmpNME16azJOakUwWVFBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NDI5NTIwNiwiaWF0IjoxNTk0Mjg4MDA2LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.mIsIIwgki1af2GQLVhAHaPOnQ6mnvsqFBvjPXkBGF_Q', 'https://us02web.zoom.us/j/89172725667?pwd=czJOU1EvVVR5M25iMDlneEh6YmFhUT09', 'admin', 0, 0, 5, 'pending', 1, 'a', 0, 0, 0),
(23, 'athul test', 'ما هو لوريم ايبسوم', 2, 4, 1, '2020-07-09 07:36:00', '10', '', '', 'itlicense@boc.bh', '2020-07-09 12:33:17', '84497129413', 'https://us02web.zoom.us/s/84497129413?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiIzQU5yRkJvbVJNT2w4ajREaXVubk1BIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6InJwc3RoS0NfRU1UbUZ6ZHFVdXQtWGdCcE9jY01uRDRjZmo3eUpMZWxKbWsuQmdVZ2RUbERSM1ZWVmt0cWRVSjBhMDVpVlM5R1kyVjROMEZwTmxvMlVsQlBTWHBBTlRaaU9ETXdNREZoTmpVNE1tUXdNekU1T1RGak9UUmtPRFkwT1dNNVl6UmhZVFZpT0RsalpXTTRaR1JoWXpFd016WTVabU0yWmpNME16azJOakUwWVFBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NDMwNTE5NywiaWF0IjoxNTk0Mjk3OTk3LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.HmmOziXq0esntT-cDpnZfkjFXqXm3qZbPqV3E9ajCSU', 'https://us02web.zoom.us/j/84497129413', 'admin', 0, 0, 2, 'pending', 1, 'a', 0, 0, 0),
(24, 'Testing zoom call integration', 'ما هو لوريم ايبسوم', 7, 11, 3, '2020-07-15 02:10:00', '60', 'Aithin EP', '', 'athayyil@boc.bh', '2020-07-13 04:40:14', '87100415522', 'https://us02web.zoom.us/s/87100415522?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiIzQU5yRkJvbVJNT2w4ajREaXVubk1BIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6InJwc3RoS0NfRU1UbUZ6ZHFVdXQtWGdCcE9jY01uRDRjZmo3eUpMZWxKbWsuQmdVZ2RUbERSM1ZWVmt0cWRVSjBhMDVpVlM5R1kyVjROMEZwTmxvMlVsQlBTWHBBTlRaaU9ETXdNREZoTmpVNE1tUXdNekU1T1RGak9UUmtPRFkwT1dNNVl6UmhZVFZpT0RsalpXTTRaR1JoWXpFd016WTVabU0yWmpNME16azJOakUwWVFBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NDYyMjQxNCwiaWF0IjoxNTk0NjE1MjE0LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.COZLIK1xelpKHORR50WD8EhtQPNWlL9JSYQg0mH7jvg', 'https://us02web.zoom.us/j/87100415522', 'admin', 0, 0, 4, 'pending', 1, 'a', 0, 0, 0),
(25, 'Intro .net', 'ما هو لوريم ايبسوم', 5, 0, 3, '2020-07-14 03:46:00', '30', 'aithin ep', '123456', '', '2020-07-13 05:16:39', '86910227502', 'https://us02web.zoom.us/s/86910227502?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiIzQU5yRkJvbVJNT2w4ajREaXVubk1BIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6InJwc3RoS0NfRU1UbUZ6ZHFVdXQtWGdCcE9jY01uRDRjZmo3eUpMZWxKbWsuQmdVZ2RUbERSM1ZWVmt0cWRVSjBhMDVpVlM5R1kyVjROMEZwTmxvMlVsQlBTWHBBTlRaaU9ETXdNREZoTmpVNE1tUXdNekU1T1RGak9UUmtPRFkwT1dNNVl6UmhZVFZpT0RsalpXTTRaR1JoWXpFd016WTVabU0yWmpNME16azJOakUwWVFBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NDYyNDU5OSwiaWF0IjoxNTk0NjE3Mzk5LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.u9QIpHsLplfKH4eaKatKFoIRX_FMHyXDfEnj0B4N1Sk', 'https://us02web.zoom.us/j/86910227502?pwd=Z2lIS3VDSU82YkpHL0VRa0lKMC9Wdz09', 'admin', 0, 0, 4, 'pending', 1, 'a', 0, 0, 0),
(26, 'Intro .net 2', 'ما هو لوريم ايبسوم', 5, 0, 6, '2020-07-13 04:04:00', '30', 'aithin ep', '123456', '', '2020-07-13 06:32:57', '86993645533', 'https://us02web.zoom.us/s/86993645533?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiIzQU5yRkJvbVJNT2w4ajREaXVubk1BIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6InJwc3RoS0NfRU1UbUZ6ZHFVdXQtWGdCcE9jY01uRDRjZmo3eUpMZWxKbWsuQmdVZ2RUbERSM1ZWVmt0cWRVSjBhMDVpVlM5R1kyVjROMEZwTmxvMlVsQlBTWHBBTlRaaU9ETXdNREZoTmpVNE1tUXdNekU1T1RGak9UUmtPRFkwT1dNNVl6UmhZVFZpT0RsalpXTTRaR1JoWXpFd016WTVabU0yWmpNME16azJOakUwWVFBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NDYyOTE3NywiaWF0IjoxNTk0NjIxOTc3LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.TJ5b-wxEfdld-QDL6vtDAAYqa_alWjmOFUbV85B5sQU', 'https://us02web.zoom.us/j/86993645533?pwd=cTRpTE1PZlY0emZ6OTRjT3J1ZHJCUT09', '', 0, 0, 4, 'pending', 0, 'a', 0, 0, 0),
(30, 'Test', 'ما هو لوريم ايبسوم', 0, 38, 8, '2020-08-12 03:03:00', '10', '', '', 'zalmosawi@boc.bh', '2020-08-12 08:00:56', '83393703565', 'https://us02web.zoom.us/s/83393703565?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NzIyNjQ1NiwiaWF0IjoxNTk3MjE5MjU2LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.AhCDDW_qjgl82_NfqJZ1Cc0l_7W5HZ8xl9RojPXGQ5E', 'https://us02web.zoom.us/j/83393703565', 'admin', 0, 0, 18, 'pending', 1, 'a', 0, 0, 0),
(33, 'First class ', 'ما هو لوريم ايبسوم', 12, 14, 9, '0000-00-00 00:00:00', '30', '', '', '', '2020-08-12 09:16:11', '82093300412', 'https://us02web.zoom.us/s/82093300412?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NzIzMDk3MSwiaWF0IjoxNTk3MjIzNzcxLCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.ecdxYOLL1Pl_BkiRe4FZWsxDxXGUk0Gsdwc0JY7ET6E', 'https://us02web.zoom.us/j/82093300412', 'admin', 0, 0, 8, 'pending', 1, 'a', 0, 0, 0),
(34, 'First class 222', 'ما هو لوريم ايبسوم', 12, 14, 9, '2020-08-12 04:22:00', '34', '', '', '', '2020-08-12 09:17:09', '88663789207', 'https://us02web.zoom.us/s/88663789207?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NzIzMTAyOCwiaWF0IjoxNTk3MjIzODI4LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.JosGIvwcBVyYgKTLDF8io_RHd_vDAFZHo3Db-_JEpIg', 'https://us02web.zoom.us/j/88663789207', 'admin', 0, 0, 8, 'pending', 1, 'a', 0, 0, 0),
(35, 'test class level 111', 'ما هو لوريم ايبسوم', 12, 14, 9, '2020-08-12 09:26:00', '40', '', '', 'zalmosawi@boc.bh', '2020-08-12 09:26:28', '84268843256', 'https://us02web.zoom.us/s/84268843256?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NzIzMTU4OCwiaWF0IjoxNTk3MjI0Mzg4LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.MU-MtO2E4rg7UWjy5oWV8611EHUFcUJxQDaL0uE6KrA', 'https://us02web.zoom.us/j/84268843256', 'admin', 0, 0, 8, 'pending', 1, 'a', 0, 0, 0),
(41, 'SportsManagement', 'ما هو لوريم ايبسوم', 12, 14, 9, '2020-08-13 02:52:00', '10', '', '', 'zalmosawi@boc.bh', '2020-08-13 07:48:34', '86575620844', 'https://us02web.zoom.us/s/86575620844?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NzMxMjExNCwiaWF0IjoxNTk3MzA0OTE0LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.19OELvRTyfpadP0r1NR79hgu_MXqaA3LIpjD8BFAJzI', 'https://us02web.zoom.us/j/86575620844', 'admin', 0, 0, 8, 'pending', 1, 'a', 0, 0, 0),
(42, 'Test Class', 'ما هو لوريم ايبسوم', 13, 0, 9, '2020-08-13 03:45:00', '20', '', '', '', '2020-08-13 08:45:19', '89358162365', 'https://us02web.zoom.us/s/89358162365?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NzMxNTUxOSwiaWF0IjoxNTk3MzA4MzE5LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.RrIRg9T99zvplkdogBdjRFNv6kYgW7EczseURpEENSs', 'https://us02web.zoom.us/j/89358162365', 'admin', 0, 0, 19, 'completed', 1, 'a', 0, 0, 0),
(46, 'First class ', 'ما هو لوريم ايبسوم', 14, 39, 35, '2020-08-13 05:44:00', '20', '', '', '', '2020-08-13 10:44:19', '85419892855', 'https://us02web.zoom.us/s/85419892855?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NzMyMjY1OSwiaWF0IjoxNTk3MzE1NDU5LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.paflfPCuAx226hq_B6OKckaz6mFLgfO2hwldDHXJh94', 'https://us02web.zoom.us/j/85419892855', 'admin', 0, 0, 7, 'completed', 1, 'a', 0, 0, 0),
(47, 'INTRODUCTION', 'ما هو لوريم ايبسوم', 14, 39, 35, '2020-08-13 06:12:00', '60', '', '', '', '2020-08-13 11:07:13', '89052969506', 'https://us02web.zoom.us/s/89052969506?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NzMyNDAzMywiaWF0IjoxNTk3MzE2ODMzLCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.6-5GLSEaXbnAckTJwWWtHEKI32J8IhSWG7Q1beWxT1Q', 'https://us02web.zoom.us/j/89052969506', 'admin', 0, 0, 7, 'pending', 1, 'a', 0, 0, 0),
(48, 'Level 1 Certification', 'ما هو لوريم ايبسوم', 12, 14, 9, '2020-08-17 05:26:00', '60', '', '', '', '2020-08-17 10:05:41', '84708473762', 'https://us02web.zoom.us/s/84708473762?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NzY2NTk0MSwiaWF0IjoxNTk3NjU4NzQxLCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.pjQ1ed4XBvrDa8GKkmBZ1mY97y10ro9DJb_-rOFR5k8', 'https://us02web.zoom.us/j/84708473762', 'admin', 0, 0, 8, 'pending', 1, 'a', 0, 0, 0),
(49, 'Introduction', 'ما هو لوريم ايبسوم', 12, 14, 9, '2020-08-22 05:12:00', '60', '', '', '', '2020-08-17 10:14:12', '84018676010', 'https://us02web.zoom.us/s/84018676010?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NzY2NjQ1MSwiaWF0IjoxNTk3NjU5MjUxLCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.T7bONniLEtO7kKjO9W1yLw7aSm_Gc8C45oqo-A8CMGc', 'https://us02web.zoom.us/j/84018676010', 'admin', 0, 0, 8, 'pending', 1, 'a', 0, 0, 0),
(50, 'First class ', 'ما هو لوريم ايبسوم', 16, 0, 9, '2020-08-17 05:35:00', '60', '', '', '', '2020-08-17 10:36:02', '81683621617', 'https://us02web.zoom.us/s/81683621617?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NzY2Nzc2MiwiaWF0IjoxNTk3NjYwNTYyLCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.eMvHxe-xoM_W_5KXubpMzxKUfgVNXDBoZrkL1dnks4o', 'https://us02web.zoom.us/j/81683621617', 'admin', 0, 0, 20, 'pending', 1, 'a', 0, 0, 0),
(51, 'introduction to BOA', 'ما هو لوريم ايبسوم', 17, 41, 9, '2020-08-18 04:18:00', '80', '', '', '', '2020-08-18 09:19:42', '86326565703', 'https://us02web.zoom.us/s/86326565703?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5Nzc0OTU4MiwiaWF0IjoxNTk3NzQyMzgyLCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.OJF3dQxJ0UE_yE7xmxqEfHmgb9BnXXs_yBN5v_GZbBg', 'https://us02web.zoom.us/j/86326565703', 'admin', 0, 0, 8, 'pending', 1, 'a', 0, 0, 0),
(52, '111', 'ما هو لوريم ايبسوم', 17, 41, 9, '2020-08-19 04:18:00', '100', '', '', '', '2020-08-18 09:19:42', '88465370457', 'https://us02web.zoom.us/s/88465370457?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5Nzc0OTU4MiwiaWF0IjoxNTk3NzQyMzgyLCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.OJF3dQxJ0UE_yE7xmxqEfHmgb9BnXXs_yBN5v_GZbBg', 'https://us02web.zoom.us/j/88465370457', 'admin', 0, 0, 8, 'pending', 1, 'a', 0, 0, 0),
(53, 'first class', 'ما هو لوريم ايبسوم', 13, 40, 9, '2020-08-20 04:19:00', '60', '', '', '', '2020-08-18 09:20:07', '83706464296', 'https://us02web.zoom.us/s/83706464296?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5Nzc0OTYwNywiaWF0IjoxNTk3NzQyNDA3LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.wDS6ST2sXdWWgdE3gSlhL1OC4dpZVoVImE0WbooNXgY', 'https://us02web.zoom.us/j/83706464296', 'admin', 0, 0, 19, 'completed', 1, 'a', 0, 0, 0),
(54, 'strength', '', 18, 41, 9, '2020-08-19 03:30:00', '120', '', '', '', '2020-08-19 07:36:10', '82020470999', 'https://us02web.zoom.us/s/82020470999?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5NzgyOTc3MCwiaWF0IjoxNTk3ODIyNTcwLCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.ImpmXCcULPPZQo2CONLLtIPvOL4pOZc7tfnWGkGYnNY', 'https://us02web.zoom.us/j/82020470999', 'admin', 0, 0, 8, 'pending', 1, 'a', 0, 0, 0),
(55, 'Intro', '', 19, 43, 36, '2020-08-24 07:28:00', '30', 'aithin ep', '123456', '', '2020-08-24 09:58:35', '87535063789', 'https://us02web.zoom.us/s/87535063789?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5ODI3MDMxNSwiaWF0IjoxNTk4MjYzMTE1LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.ukk5xQu1mbs0DMfQKMt1rpUupXAVeuuwlItzpKOWnC0', 'https://us02web.zoom.us/j/87535063789?pwd=ZmQwYkRrRi9sNlBKaWh6MXJFVDNYZz09', 'admin', 0, 0, 21, 'pending', 1, 'a', 0, 0, 0),
(56, 'test', '', 19, 42, 36, '2020-09-06 09:28:00', '10', '', '', '', '2020-09-06 13:28:18', '84186277824', 'https://us02web.zoom.us/s/84186277824?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6Im0yZUdJZ2FXdnd2Z3VOYWk0ZHlMWTQtRnhSWE94ZjRqdDZpcm9taXMweU0uQmdVZ2FWUkJlbWhLZVc1aWVFeE5jREJhVnpnME9VUnBURUZwTmxvMlVsQlBTWHBBT0dVMFpXUTJOV0U0Tm1aaFptWTRPRE0yTnpreU16TmtZak5pWkRBM05qZ3pNamxsWXpnNE9HSTRZVFJpWlRBek1tRm1OREppTTJVNVl6SmpNakpsTWdBTU0wTkNRWFZ2YVZsVE0zTTlBQVIxY3pBeSIsImV4cCI6MTU5OTQwNjA5OCwiaWF0IjoxNTk5Mzk4ODk4LCJhaWQiOiJIcTNxeDZNRlJVcV9Bazd1SVJPQjdnIiwiY2lkIjoiIn0.kfcJbi1ZqjmKHGmi8aYnlv9ShXA81e6969ThgStauHs', 'https://us02web.zoom.us/j/84186277824', 'admin', 0, 0, 21, 'pending', 1, 'a', 0, 0, 0),
(57, 'test123', '', 20, 40, 9, '2020-10-21 05:52:00', '10', '', '12345', '', '2020-10-21 10:53:23', '82530902310', 'https://us02web.zoom.us/s/82530902310?zak=eyJ6bV9za20iOiJ6bV9vMm0iLCJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjbGllbnQiLCJ1aWQiOiJLUExpM0laV1ItU29iY2JrMUlXcmlnIiwiaXNzIjoid2ViIiwic3R5IjoxMDAsIndjZCI6InVzMDIiLCJjbHQiOjAsInN0ayI6InktOFpFYU12RF9zanN6LTRJVEh3enNUQ2VGZ2FjRGROMTFFemVIOVNtYnMuQmdjUWVtRnNiVzl6WVhkcFFHSnZZeTVpYUVBNFpUUmxaRFkxWVRnMlptRm1aamc0TXpZM09USXpNMlJpTTJKa01EYzJPRE15T1dWak9EZzRZamhoTkdKbE1ETXlZV1kwTW1JelpUbGpNbU15TW1VeUFBRXdBQVIxY3pBeUFBQUJkVXJLdzBzQUVuVUEiLCJleHAiOjE2MDMyODQ4MDMsImlhdCI6MTYwMzI3NzYwMywiYWlkIjoiSHEzcXg2TUZSVXFfQWs3dUlST0I3ZyIsImNpZCI6IiJ9.sIHwbv1qmxeve8hebsbgTF_wmOlEh8vgsRoFaEzCrW0', 'https://us02web.zoom.us/j/82530902310?pwd=T0xkNDV5bW4vSVd4TVpEM3dCRHFkZz09', 'admin', 0, 0, 19, 'pending', 1, 'a', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `course_class_attachments`
--

CREATE TABLE `course_class_attachments` (
  `cca_id` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `title_arabic` varchar(200) NOT NULL,
  `type` enum('Documents','Video','Image') NOT NULL,
  `attachment` text NOT NULL,
  `cc_id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_class_attachments`
--

INSERT INTO `course_class_attachments` (`cca_id`, `title`, `title_arabic`, `type`, `attachment`, `cc_id`, `course_id`, `added_by`, `added_subadmin`, `timestamp`) VALUES
(2, 'testtttttttttttttt', 'ما هو لوريم ايبسوم', 'Documents', 'uploads/class/20200623123115926539025eedf84e67cb3.pdf', 5, 4, 0, 0, '2020-08-19 05:37:40');

-- --------------------------------------------------------

--
-- Table structure for table `course_organisers`
--

CREATE TABLE `course_organisers` (
  `co_id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `organiser_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_organisers`
--

INSERT INTO `course_organisers` (`co_id`, `course_id`, `organiser_id`) VALUES
(4, 4, 2),
(5, 4, 1),
(6, 18, 2),
(16, 21, 2),
(17, 20, 2),
(18, 22, 1),
(19, 23, 2),
(21, 24, 2),
(22, 25, 2),
(23, 26, 3);

-- --------------------------------------------------------

--
-- Table structure for table `course_resources`
--

CREATE TABLE `course_resources` (
  `cr_id` bigint(20) NOT NULL,
  `cr_title` varchar(200) NOT NULL,
  `cr_title_arabic` varchar(200) NOT NULL,
  `cr_type` enum('Document','Video','Image') NOT NULL,
  `cr_attachment` mediumtext NOT NULL,
  `cr_status` tinyint(1) NOT NULL DEFAULT 1,
  `course_id` bigint(20) NOT NULL DEFAULT 0,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `added_by` enum('admin','subadmin','trainer') NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `added_trainer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_resources`
--

INSERT INTO `course_resources` (`cr_id`, `cr_title`, `cr_title_arabic`, `cr_type`, `cr_attachment`, `cr_status`, `course_id`, `timestamp`, `added_by`, `added_subadmin`, `added_trainer`) VALUES
(6, 'test', 'ما هو لوريم ايبسوم', 'Document', 'uploads/resources/202007298239Bahrain Olympic Academy features.pdf', 1, 17, '2020-08-19 05:39:42', 'admin', 0, 0),
(8, 'categroty', 'ما هو لوريم ايبسوم', 'Image', 'uploads/resources/2020080596073.jpg', 1, 0, '2020-08-19 05:39:42', 'admin', 0, 0),
(9, 'category 1', 'ما هو لوريم ايبسوم', 'Document', 'uploads/resources/202008056811 ???????? ??????? ?????????? ???????????.pdf', 1, 0, '2020-08-19 05:39:42', 'admin', 0, 0),
(10, 'test1232', 'ما هو لوريم ايبسوم', 'Document', 'uploads/resources/202008067555test1.docx', 1, 0, '2020-08-19 05:39:42', 'admin', 0, 0),
(11, 'fgyh', 'ما هو لوريم ايبسوم', 'Document', 'uploads/resources/202008066616test1.pdf', 1, 0, '2020-08-19 05:39:42', 'admin', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `course_sections`
--

CREATE TABLE `course_sections` (
  `cs_id` bigint(20) NOT NULL,
  `section_title` varchar(200) NOT NULL,
  `section_description` text NOT NULL,
  `section_title_arabic` varchar(200) NOT NULL,
  `section_description_arabic` text NOT NULL,
  `section_start_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `section_file_type` enum('Documents','Video','Image','Nil') NOT NULL,
  `section_attachment` text NOT NULL,
  `section_youtube` text NOT NULL,
  `section_other` text NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `section_status` tinyint(1) NOT NULL DEFAULT 1,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `added_by` enum('a','t','s') NOT NULL,
  `trainer_id` int(11) NOT NULL,
  `edited_by` enum('a','t','s') NOT NULL,
  `edited_trainer` int(11) NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_subadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_sections`
--

INSERT INTO `course_sections` (`cs_id`, `section_title`, `section_description`, `section_title_arabic`, `section_description_arabic`, `section_start_time`, `section_file_type`, `section_attachment`, `section_youtube`, `section_other`, `course_id`, `section_status`, `timestamp`, `added_by`, `trainer_id`, `edited_by`, `edited_trainer`, `added_subadmin`, `edited_subadmin`) VALUES
(2, 'Object oriented concepts', 'Object-oriented programming (OOP) is a programming paradigm based on the concept of \"objects\", which can contain data, in the form of fields and code', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-07-08 08:40:42', 'Documents', 'uploads/sections/202006298927php.pdf', 'https://www.youtube.com/watch?v=8cm1x4bC610', '', 2, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(3, 'Polymorphism', 'Polymorphism is one of the core concepts in OOP languages. It describes the concept that different classes can be used with the same interface. Each of these classes can provide its own implementation of the interface. Java supports two kinds of polymorphism. You can overload a method with different sets of parameters', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-07-08 08:40:42', 'Documents', '', 'youtube.com/watch?v=grEKMHGYyns', '', 2, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(4, 'Data binding', 'Java handles binding either statically or dynamically. Static binding is done at compile time. This is for methods that can\'t be overridden, such as static or final methods. Dynamic binding is done at run-time.', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-07-08 08:40:42', 'Documents', 'uploads/sections/202006298416.net.pdf', 'https://www.youtube.com/watch?v=eIrMbAQSU34', '', 2, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(11, 'Introduction To Dot Net', 'ASP.NET is an open-source,[2] server-side web-application framework designed for web development to produce dynamic web pages. It was developed by Microsoft to allow programmers to build dynamic web sites, applications and services.\r\n\r\nIt was first released in January 2002 with version 1.0 of the .NET Framework and is the successor to Microsoft\'s Active Server Pages (ASP) technology. ASP.NET is built on the Common Language Runtime (CLR), allowing programmers to write ASP.NET code using any supported .NET language. The ASP.NET SOAP extension framework allows ASP.NET components to process SOAP messages.\r\n\r\nASP.NET\'s successor is ASP.NET Core. It is a re-implementation of ASP.NET as a modular web framework, together with other frameworks like Entity Framework. The new framework uses the new open-source .NET Compiler Platform (codename \"Roslyn\") and is cross platform. ASP.NET MVC, ASP.NET Web API, and ASP.NET Web Pages (a platform using only Razor pages) have merged into a unified MVC 6.[3]', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-07-08 08:40:00', 'Documents', 'uploads/sections/202006299315.net.pdf', 'https://www.youtube.com/watch?v=eIHKZfgddLM', '', 4, 0, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(12, 'Introduction To PHP', 'PHP is a popular general-purpose scripting language that is especially suited to web development.[5] It was originally created by Danish-Canadian programmer Rasmus Lerdorf in 1994;[6] the PHP reference implementation is now produced by The PHP Group.[7] PHP originally stood for Personal Home Page,[6] but it now stands for the recursive initialism PHP: Hypertext Preprocessor.[8]\r\n\r\nPHP code is usually processed on a web server by a PHP interpreter implemented as a module, a daemon or as a Common Gateway Interface (CGI) executable. On a web server, the result of the interpreted and executed PHP code – which may be any type of data, such as generated HTML or binary image data – would form the whole or part of a HTTP response. Various web template systems, web content management systems, and web frameworks exist which can be employed to orchestrate or facilitate the generation of that response. Additionally, PHP can be used for many programming tasks outside of the web context, such as standalone graphical applications[9] and robotic drone control.[10] Arbitrary PHP code can also be interpreted and executed via command-line interface (CLI).\r\n\r\nThe standard PHP interpreter, powered by the Zend Engine, is free software released under the PHP License. PHP has been widely ported and can be deployed on most web servers on almost every operating system and platform, free of charge.[11]', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-07-08 08:40:42', 'Documents', 'uploads/sections/202006292528php.pdf', 'https://www.youtube.com/watch?v=ZdP0KM49IVk', '', 3, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(13, 'Introduction to Human Resource', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-07-16 06:54:00', 'Nil', '', '', '', 5, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(14, 'Level One ', 'The duration of this level is one week (30 hours). This level focuses on establishing the coaches to acquire the basics of sports coaching.\r\nTopics covered at level One include:\r\n• Planning as part of coaching science (Planning of a single training module).\r\n• Develop strength using resistance exercises.\r\n• Teaching and learning.\r\n• Making ethical decisions.\r\n', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-08-24 13:00:00', 'Nil', 'uploads/sections/202008173037Academy portal.xlsx', '', '', 8, 1, '2020-08-23 09:23:18', 'a', 0, 'a', 0, 0, 0),
(15, '1. Energy Systems & Physiology', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '0000-00-00 00:00:00', 'Nil', '', '', '', 11, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(16, '2. Advanced Performance Planning A and B', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '0000-00-00 00:00:00', 'Nil', '', '', '', 11, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(17, '3. Sport Psychology: ', 'a. Coach\'s Mental Preparation for International Competition. \r\nb. Athlete\'s Mental Preparation for International Competition.', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '0000-00-00 00:00:00', 'Nil', '', '', '', 11, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(18, '4. LTAD Implementation in HP Coaching', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '0000-00-00 00:00:00', 'Nil', '', '', '', 11, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(19, '5. Advanced Recovery and Regeneration', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '0000-00-00 00:00:00', 'Nil', '', '', '', 11, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(20, '6. High Performance Analysis', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '0000-00-00 00:00:00', 'Nil', '', '', '', 11, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(21, '7. International Leadership Skills', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '0000-00-00 00:00:00', 'Nil', '', '', '', 11, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(22, '8. Strength Development & Environmental Factors.', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '0000-00-00 00:00:00', 'Nil', '', '', '', 11, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(23, 'Organization of sports and Olympic institutions.', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '0000-00-00 00:00:00', 'Nil', '', '', '', 13, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(24, 'Strategic management.', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '0000-00-00 00:00:00', 'Nil', '', '', '', 13, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(25, 'Human Resource Management.', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '0000-00-00 00:00:00', 'Nil', '', '', '', 13, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(26, 'Financial Affairs Administration.', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '0000-00-00 00:00:00', 'Nil', '', '', '', 13, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(27, 'Marketing Management.', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '0000-00-00 00:00:00', 'Nil', '', '', '', 13, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(28, 'Organizing major sports events.', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '0000-00-00 00:00:00', 'Nil', '', '', '', 13, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(29, 'test', 'testtttttttttttttttttttttttttttttttttttttttttttttttttttt', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-07-18 09:21:00', 'Documents', 'uploads/sections/202007179666TEST PDF.pdf', '', '', 2, 1, '2020-08-19 05:45:22', 's', 0, 'a', 0, 10, 0),
(31, 'Energy Systems & Physiology', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-08-09 15:47:00', 'Nil', '', '', '', 18, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(32, 'Advanced Performance Planning A and B', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-08-10 15:47:00', 'Nil', '', '', '', 18, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(33, 'Sport Psychology:', 'a. Coach\'s Mental Preparation for International Competition. b. Athlete\'s Mental Preparation for International Competition.', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-08-11 15:48:00', 'Nil', '', '', '', 18, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(34, 'LTAD Implementation in HP Coaching', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-08-12 15:48:00', 'Nil', '', '', '', 18, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(35, 'Advanced Recovery and Regeneration', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-08-13 15:55:00', 'Nil', '', '', '', 18, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(36, 'High Performance Analysis', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-08-14 15:55:00', 'Nil', '', '', '', 18, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(37, 'International Leadership Skills', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-08-15 15:55:00', 'Nil', '', '', '', 18, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(38, 'Strength Development & Environmental Factors.', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-08-16 15:55:00', 'Nil', '', '', '', 18, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(39, 'Introduction to human psychology', '', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-08-13 05:15:00', 'Nil', '', '', '', 7, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(40, 'Test', 'test', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-08-16 03:20:00', 'Nil', 'uploads/sections/202008245532Test.xlsx', '', '', 19, 1, '2020-08-24 09:05:53', 'a', 0, 'a', 0, 0, 0),
(41, 'boa film', 'we can add details here ', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '0000-00-00 00:00:00', 'Nil', '', 'https://www.youtube.com/watch?v=CMNZzJ_pB8E', '', 8, 1, '2020-08-19 05:45:22', 'a', 0, 'a', 0, 0, 0),
(42, 'test', 'kjlllllll', 'العصبة ضرب قد. وباءت الأمريكي', 'العصبة ضرب قد. وباءت الأمريكي', '2020-08-24 07:25:00', 'Documents', 'uploads/sections/202008241448Test .pdf', '', '', 21, 1, '2020-10-12 09:56:23', 'a', 0, 'a', 0, 0, 0),
(45, 'test', 'test', 'يصي', 'سب', '2021-03-20 05:24:00', 'Documents', '', '', '', 0, 1, '2021-03-20 10:26:09', 'a', 0, 'a', 0, 0, 0),
(46, 'testone', 'test one', 'قبثمةلكة', 'ةرثقكنةركثقةك', '2021-03-20 05:26:00', 'Documents', '', '', '', 0, 1, '2021-03-20 10:27:24', 'a', 0, 'a', 0, 0, 0),
(47, 'test', 'test section', 'امتحان', '', '2021-03-22 07:25:00', 'Documents', '', '', '', 0, 1, '2021-03-22 10:06:57', 'a', 0, 'a', 0, 0, 0),
(48, 'test', '', 'امتحان', '', '2021-03-16 01:19:00', 'Documents', '', '', '', 0, 1, '2021-03-23 03:50:16', 'a', 0, 'a', 0, 0, 0),
(49, 'test', 'test details', 'امتحان', '', '2021-03-23 01:36:00', 'Documents', '', '', '', 0, 1, '2021-03-23 04:07:16', 'a', 0, 'a', 0, 0, 0),
(50, 'test', '', 'امتحان', '', '2021-03-17 01:40:00', 'Documents', '', '', '', 21, 1, '2021-03-23 04:22:45', 'a', 0, 'a', 0, 0, 0),
(51, 'fdsg', 'gdsgds', 'بلالابيىىيبى', 'بلاسلاسبلاسبلا', '2021-03-25 03:50:00', 'Documents', '', '', '', 24, 1, '2021-03-25 09:04:16', 'a', 0, 'a', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `course_section_attachments`
--

CREATE TABLE `course_section_attachments` (
  `csa_id` bigint(20) NOT NULL,
  `csa_file_type` enum('Documents','Video','Image','Nil') NOT NULL DEFAULT 'Nil',
  `csa_youtube` text NOT NULL,
  `csa_attachment` text NOT NULL,
  `csa_other` text NOT NULL,
  `cs_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_section_attachments`
--

INSERT INTO `course_section_attachments` (`csa_id`, `csa_file_type`, `csa_youtube`, `csa_attachment`, `csa_other`, `cs_id`) VALUES
(1, 'Documents', 'This is a test link.', 'uploads/sections/2020101254921.jpg', 'https://www.instagram.com/37890490390', 44),
(3, 'Image', 'This is a test link.', 'uploads/sections/202010129900event1.jpg', 'https://www.instagram.com/37890490390', 44),
(4, 'Documents', 'This is a test link.', 'uploads/sections/202010122172image.jpg', 'https://www.instagram.com/37890490390', 42),
(5, 'Documents', 'This is a test link.', 'uploads/sections/202010129602event1.jpg', 'https://www.instagram.com/37890490390', 44),
(6, 'Image', '', 'uploads/sections/2021032096884x6...3.JPG', '', 45),
(7, 'Image', '', 'uploads/sections/2021032091354x6...3.JPG', '', 46),
(8, 'Image', '', 'uploads/sections/202103225478default.jpg', '', 47),
(9, 'Nil', '', '', '', 48),
(10, 'Image', '', 'uploads/sections/202103236816default.jpg', '', 49),
(11, 'Image', '', 'uploads/sections/202103234262default.jpg', '', 50),
(12, 'Documents', '', 'uploads/sections/202103256169Aldar-Logo.png', '', 51),
(13, 'Nil', '', 'uploads/sections/20210325439511-Physics-RevisionNotes-Chapter-1.pdf', '', 51),
(14, 'Documents', '', 'uploads/sections/202103255941ACFrOgBcVj6kjQLqGL9PEJmmiw12YFahT6AJfsWtMsoVFXFEOJfhGStVbPzVmVyWRO8mFKHVMIhyVXpNzqfoUhBL7hm242I_7rtTRJm1YaSxbk8Gg6zIBIGba4xY84g=.pdf', '', 52);

-- --------------------------------------------------------

--
-- Table structure for table `course_students_assignments`
--

CREATE TABLE `course_students_assignments` (
  `csa_id` bigint(20) NOT NULL,
  `ca_id` bigint(20) NOT NULL,
  `stu_id` bigint(20) NOT NULL,
  `answer` mediumtext NOT NULL,
  `answer_attachment` mediumtext NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_students_assignments`
--

INSERT INTO `course_students_assignments` (`csa_id`, `ca_id`, `stu_id`, `answer`, `answer_attachment`, `timestamp`) VALUES
(1, 13, 44, 'n nmmnm', 'uploads/students-assignments/202008248978Test .pdf', '2020-08-24 10:28:47'),
(2, 14, 44, '', 'uploads/students-assignments/202009053466Image-1 - 2020-09-04T123748.167.jpg', '2020-09-05 10:35:11');

-- --------------------------------------------------------

--
-- Table structure for table `course_trainers`
--

CREATE TABLE `course_trainers` (
  `ct_id` int(11) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `trainer_id` bigint(20) NOT NULL,
  `ct_status` tinyint(1) NOT NULL DEFAULT 1,
  `added_by` enum('a','t','s') NOT NULL,
  `added_trainer` int(11) NOT NULL,
  `edited_by` enum('a','t','s') NOT NULL,
  `edited_trainer` int(11) NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_subadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_trainers`
--

INSERT INTO `course_trainers` (`ct_id`, `course_id`, `trainer_id`, `ct_status`, `added_by`, `added_trainer`, `edited_by`, `edited_trainer`, `added_subadmin`, `edited_subadmin`) VALUES
(2, 2, 1, 1, 'a', 0, 'a', 0, 0, 0),
(3, 2, 2, 1, 'a', 0, 'a', 0, 0, 0),
(4, 3, 1, 1, 'a', 0, 'a', 0, 0, 0),
(5, 3, 2, 1, 'a', 0, 'a', 0, 0, 0),
(6, 4, 3, 1, 'a', 0, 'a', 0, 0, 0),
(7, 4, 4, 1, 'a', 0, 'a', 0, 0, 0),
(8, 4, 5, 1, 'a', 0, 'a', 0, 0, 0),
(9, 5, 6, 1, 'a', 0, 'a', 0, 0, 0),
(10, 6, 2, 1, 'a', 0, 'a', 0, 0, 0),
(11, 7, 1, 1, 'a', 0, 'a', 0, 0, 0),
(12, 7, 2, 1, 'a', 0, 'a', 0, 0, 0),
(13, 7, 3, 1, 'a', 0, 'a', 0, 0, 0),
(14, 4, 6, 1, 'a', 0, 'a', 0, 0, 0),
(15, 5, 7, 1, 't', 7, 'a', 0, 0, 0),
(16, 8, 9, 1, 'a', 0, 'a', 0, 0, 0),
(17, 9, 10, 1, 'a', 0, 'a', 0, 0, 0),
(18, 10, 8, 1, 'a', 0, 'a', 0, 0, 0),
(19, 10, 10, 0, 's', 10, 'a', 0, 0, 0),
(20, 10, 9, 0, 'a', 0, 'a', 0, 0, 0),
(21, 11, 8, 1, 'a', 0, 'a', 0, 0, 0),
(22, 12, 9, 1, 'a', 0, 'a', 0, 0, 0),
(23, 13, 10, 1, 'a', 0, 'a', 0, 0, 0),
(24, 14, 9, 1, 'a', 0, 'a', 0, 0, 0),
(25, 15, 9, 1, 'a', 0, 'a', 0, 0, 0),
(26, 16, 9, 1, 'a', 0, 'a', 0, 0, 0),
(27, 2, 8, 0, 's', 10, 'a', 0, 0, 0),
(28, 17, 8, 1, 'a', 0, 'a', 0, 0, 0),
(29, 17, 9, 1, 'a', 0, 'a', 0, 0, 0),
(30, 3, 32, 1, 'a', 0, 'a', 0, 0, 0),
(31, 18, 8, 1, 'a', 0, 'a', 0, 0, 0),
(32, 19, 9, 1, 'a', 0, 'a', 0, 0, 0),
(33, 7, 35, 1, 'a', 0, 'a', 0, 0, 0),
(34, 20, 9, 1, 'a', 0, 'a', 0, 0, 0),
(35, 20, 14, 1, 'a', 0, 'a', 0, 0, 0),
(36, 20, 16, 1, 'a', 0, 'a', 0, 0, 0),
(37, 21, 36, 1, 'a', 0, 'a', 0, 0, 0),
(38, 19, 36, 1, 'a', 0, 'a', 0, 0, 0),
(39, 22, 8, 1, 'a', 0, 'a', 0, 0, 0),
(40, 23, 36, 1, 'a', 0, 'a', 0, 0, 0),
(41, 24, 13, 1, 'a', 0, 'a', 0, 0, 0),
(42, 25, 13, 1, 'a', 0, 'a', 0, 0, 0),
(43, 26, 9, 1, 'a', 0, 'a', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `educations`
--

CREATE TABLE `educations` (
  `edu_id` int(11) NOT NULL,
  `education` mediumtext NOT NULL,
  `education_arabic` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `added_by` enum('admin','subadmin') NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_by` enum('admin','subadmin') NOT NULL,
  `edited_subadmin` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `educations`
--

INSERT INTO `educations` (`edu_id`, `education`, `education_arabic`, `status`, `added_by`, `added_subadmin`, `edited_by`, `edited_subadmin`, `timestamp`) VALUES
(1, 'BSC', 'ما هو لوريم ايبسوم', 1, 'admin', 0, 'admin', 0, '2020-08-19 05:48:40'),
(2, 'Testing', 'ما هو لوريم ايبسوم', 0, 'admin', 0, 'admin', 0, '2020-08-19 05:48:40'),
(3, 'Bachelor of technology', 'ما هو لوريم ايبسوم', 0, 'admin', 0, 'admin', 0, '2020-08-19 05:48:40'),
(4, 'MCA', 'ما هو لوريم ايبسوم', 1, 'admin', 0, 'admin', 0, '2020-08-19 05:48:40'),
(5, 'BCA ', 'ما هو لوريم ايبسوم', 1, 'admin', 0, 'admin', 0, '2020-08-19 05:48:40'),
(6, 'BSC', 'ما هو لوريم ايبسوم', 1, 'admin', 0, 'admin', 0, '2020-08-19 05:48:40'),
(7, 'MBA', 'ما هو لوريم ايبسوم', 1, 'admin', 0, 'admin', 0, '2020-08-19 05:48:40'),
(8, 'BSC', 'ما هو لوريم ايبسوم', 0, 'admin', 0, 'admin', 0, '2020-08-19 05:48:40'),
(9, 'degree', 'ما هو لوريم ايبسوم', 1, 'admin', 0, 'admin', 0, '2020-08-19 05:48:40'),
(10, 'TEST', 'ما هو لوريم ايبسوم', 1, 'admin', 0, 'admin', 0, '2020-08-19 05:48:40');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `event_id` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(200) NOT NULL,
  `description_arabic` text NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `image` text NOT NULL,
  `added_by` enum('admin','subadmin') NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_by` enum('admin','subadmin') NOT NULL,
  `edited_subadmin` int(11) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_id`, `title`, `description`, `title_arabic`, `description_arabic`, `start_time`, `end_time`, `image`, `added_by`, `added_subadmin`, `edited_by`, `edited_subadmin`, `status`, `timestamp`) VALUES
(2, 'Proin nec odio id ante ultricies pharetra', 'Proin nec odio id ante ultricies pharetra vel sit amet ligula. Ut nunc nibh, venenatis a iaculis non, tincidunt et sem. Suspendisse posuere efficitur urna, id tempus mi varius a. Vivamus id nisi a purus convallis mollis vestibulum dapibus dolor. Nunc cursus, sem sollicitudin consequat condimentum, tortor lacus fringilla turpis, sit amet gravida ex eros quis eros. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam sit amet mauris mauris. In eleifend rutrum nibh et porttitor. Sed eget fringilla felis. Donec turpis ante, luctus eget dapibus eu, euismod a risus. Donec egestas eget nibh a aliquet. Duis dui felis, ultrices eget ex et, hendrerit cursus velit. Etiam felis dolor, vulputate eget erat a, tempus rutrum dolor. Nunc dignissim porttitor nisi varius consequat. Sed sit amet nisl purus.', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-08-19 05:53:00', '0000-00-00 00:00:00', 'uploads/events/event-sample2.jpg', 'admin', 0, 'admin', 0, '1', '2020-08-19 05:53:00'),
(4, 'INUC Conference 2020', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-08-19 05:53:00', '2020-07-09 09:00:00', 'uploads/events/202007075292slide-3.jpg', 'admin', 0, 'admin', 0, '1', '2020-08-19 05:53:00'),
(6, 'Intro class', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-08-19 05:53:00', '2020-07-15 04:34:00', 'uploads/events/202007134758image_search_1594025261584.jpg', 'admin', 0, 'subadmin', 10, '1', '2020-08-19 05:53:00'),
(9, 'Graduation Party for National Coaching Course', 'Graduation Party for National Coaching Course', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-08-19 05:53:00', '2020-07-23 04:53:00', 'uploads/events/202007191533435.jpg', 'admin', 0, 'admin', 0, '1', '2020-08-19 05:53:00'),
(11, 'Test Event', 'Test event.....', 'امتحان', 'امتحان', '2021-03-16 11:43:00', '2021-03-31 11:43:00', 'uploads/events/202103234159default.jpg', 'admin', 0, 'admin', 0, '1', '2021-03-23 14:30:44'),
(12, 'Rahali', 'qweryiuisuiy', 'اكتملت الدورة', 'اكتملت الدورة', '2021-03-25 07:08:00', '2021-03-27 07:08:00', 'uploads/events/Rahali202103249452.png', 'admin', 0, 'admin', 0, '1', '2021-03-24 09:39:23');

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `exam_id` int(11) NOT NULL,
  `exam_name` varchar(100) NOT NULL,
  `exam_name_arabic` varchar(100) NOT NULL,
  `type` enum('section','batch','course') NOT NULL,
  `course_id` int(11) NOT NULL DEFAULT 0,
  `cs_id` int(11) NOT NULL DEFAULT 0,
  `cb_id` bigint(20) NOT NULL DEFAULT 0,
  `no_questions` int(11) NOT NULL,
  `exam_time` int(11) NOT NULL DEFAULT 0,
  `time_status` tinyint(1) NOT NULL,
  `from_time` timestamp NULL DEFAULT NULL,
  `to_time` timestamp NULL DEFAULT NULL,
  `show_result` tinyint(1) NOT NULL DEFAULT 1,
  `leaderboard_limit` int(11) NOT NULL,
  `exam_status` tinyint(1) NOT NULL DEFAULT 0,
  `registration_status` tinyint(1) NOT NULL DEFAULT 0,
  `answer` varchar(200) NOT NULL,
  `calendar_datetime` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp(),
  `added_by` enum('a','t','s') NOT NULL,
  `trainer_id` int(11) NOT NULL,
  `edited_by` enum('a','t','s') NOT NULL,
  `edited_trainer` int(11) NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_subadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`exam_id`, `exam_name`, `exam_name_arabic`, `type`, `course_id`, `cs_id`, `cb_id`, `no_questions`, `exam_time`, `time_status`, `from_time`, `to_time`, `show_result`, `leaderboard_limit`, `exam_status`, `registration_status`, `answer`, `calendar_datetime`, `timestamp`, `added_by`, `trainer_id`, `edited_by`, `edited_trainer`, `added_subadmin`, `edited_subadmin`) VALUES
(10, 'ASP.net MCQ', '', 'course', 4, 0, 0, 5, 6, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-06-29 09:53:53', 'a', 0, 'a', 0, 0, 0),
(11, 'Test', '', 'course', 4, 0, 0, 4, 5, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 1, '', '0000-00-00 00:00:00', '2020-06-29 10:38:56', 'a', 0, 'a', 0, 0, 0),
(12, 'test', '', 'course', 3, 0, 0, 2, 2, 0, NULL, NULL, 1, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-06-30 09:13:43', 'a', 0, 'a', 0, 0, 0),
(16, 'ASP .NET CONCEPTS', '', 'course', 4, 0, 0, 10, 60, 0, NULL, NULL, 0, 0, 0, 0, '', '0000-00-00 00:00:00', '2020-07-03 04:55:38', 'a', 0, 'a', 0, 0, 0),
(20, 'Duplicate exam', '', 'course', 3, 0, 0, 10, 60, 1, '2020-07-08 05:00:00', '2020-07-08 06:00:00', 1, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-07-06 07:23:28', 'a', 0, 'a', 0, 0, 0),
(21, 'Public service commission Police Test', '', 'batch', 3, 12, 4, 10, 60, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-07-06 07:27:40', 'a', 0, 'a', 0, 0, 0),
(23, 'test', '', 'course', 2, 0, 0, 10, 10, 0, NULL, NULL, 1, 0, 0, 0, '', '0000-00-00 00:00:00', '2020-07-08 08:35:30', 'a', 0, 'a', 0, 0, 0),
(24, 'test25', '', 'batch', 4, 0, 5, 2, 20, 1, '2020-07-13 02:55:00', '2020-07-13 03:15:00', 0, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-07-13 05:22:34', 'a', 0, 'a', 0, 0, 0),
(25, 'test258', '', 'batch', 4, 0, 5, 1, 12, 0, NULL, NULL, 0, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-07-13 06:10:41', 'a', 0, 'a', 0, 0, 0),
(26, 'test', '', 'batch', 4, 0, 5, 1, 12, 0, NULL, NULL, 0, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-07-13 06:12:26', 'a', 0, 'a', 0, 0, 0),
(27, 'test123', '', 'batch', 4, 0, 5, 2, 20, 0, NULL, NULL, 0, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-07-13 06:14:58', 'a', 0, 'a', 0, 0, 0),
(28, 'test144', '', 'batch', 4, 0, 5, 2, 20, 0, NULL, NULL, 1, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-07-13 06:17:51', 'a', 0, 'a', 0, 0, 0),
(29, 'test', '', 'batch', 4, 0, 5, 1, 10, 0, NULL, NULL, 1, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-07-13 06:21:05', 'a', 0, 'a', 0, 0, 0),
(31, 'test111111111111111', '', 'batch', 4, 0, 5, 1, 20, 0, NULL, NULL, 1, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-07-13 06:39:02', 'a', 0, 'a', 0, 0, 0),
(36, 'test exam', '', 'batch', 21, 43, 19, 2, 12, 0, NULL, NULL, 1, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-08-24 10:10:26', 'a', 0, 'a', 0, 0, 0),
(37, 'test78', '', 'batch', 21, 0, 19, 1, 5, 0, NULL, NULL, 0, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-08-24 10:18:21', 'a', 0, 'a', 0, 0, 0),
(38, 'test101', '', 'course', 2, 0, 0, 1, 1, 0, NULL, NULL, 0, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-09-02 08:20:35', 'a', 0, 'a', 0, 0, 0),
(39, 'الامتحان الأول', '', 'course', 2, 0, 0, 1, 1, 0, NULL, NULL, 1, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-09-02 08:23:23', 'a', 0, 'a', 0, 0, 0),
(40, 'test554', '', 'course', 21, 0, 0, 1, 1, 0, NULL, NULL, 1, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-09-02 15:14:19', 'a', 0, 'a', 0, 0, 0),
(41, 'Testing', '', 'course', 16, 0, 0, 5, 10, 0, NULL, NULL, 1, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-09-04 09:06:40', 'a', 0, 'a', 0, 0, 0),
(42, 'intro123', '', 'course', 2, 0, 0, 1, 10, 0, '2020-09-03 16:00:00', NULL, 1, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-09-04 19:02:48', 'a', 0, 'a', 0, 0, 0),
(43, 'Test expense', 'محمد عبد الله', 'course', 21, 0, 0, 2, 5, 0, NULL, NULL, 1, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-09-05 05:10:47', 'a', 0, 'a', 0, 0, 0),
(44, 'test', '', 'course', 7, 0, 0, 2, 15, 0, NULL, NULL, 1, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-09-05 10:24:37', 'a', 0, 'a', 0, 0, 0),
(45, 'test', 'يسششسبششسؤشس', 'course', 20, 0, 0, 1, 2, 0, NULL, NULL, 1, 0, 1, 1, '', '0000-00-00 00:00:00', '2020-09-20 05:34:55', 'a', 0, 'a', 0, 0, 0),
(46, 'test 123', '', 'course', 19, 0, 0, 1, 10, 0, NULL, NULL, 1, 0, 1, 1, '', '1970-01-01 03:00:00', '2020-10-21 10:56:20', 'a', 0, 'a', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exam_instructions`
--

CREATE TABLE `exam_instructions` (
  `ei_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `instruction` text NOT NULL,
  `instruction_arabic` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp(),
  `added_by` enum('a','t','s') NOT NULL,
  `trainer_id` int(11) NOT NULL,
  `edited_by` enum('a','t','s') NOT NULL,
  `edited_trainer` int(11) NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_subadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exam_instructions`
--

INSERT INTO `exam_instructions` (`ei_id`, `exam_id`, `instruction`, `instruction_arabic`, `timestamp`, `added_by`, `trainer_id`, `edited_by`, `edited_trainer`, `added_subadmin`, `edited_subadmin`) VALUES
(1, 2, 'This is a test instruction', '', '2020-06-16 06:31:20', 'a', 0, 'a', 0, 0, 0),
(2, 2, 'This is a test instruction', '', '2020-06-16 06:31:20', 'a', 0, 'a', 0, 0, 0),
(4, 3, 'Testing', '', '2020-06-16 06:33:20', 'a', 0, 'a', 0, 0, 0),
(5, 3, 'This is a test instruction', '', '2020-06-16 06:33:20', 'a', 0, 'a', 0, 0, 0),
(6, 3, 'Test exam', '', '2020-06-16 06:33:20', 'a', 0, 'a', 0, 0, 0),
(7, 4, 'Testing', '', '2020-06-16 06:41:15', 'a', 0, 'a', 0, 0, 0),
(8, 4, 'Test instruction', '', '2020-06-16 06:41:15', 'a', 0, 'a', 0, 0, 0),
(9, 4, 'This is a test instruction', '', '2020-06-16 06:41:15', 'a', 0, 'a', 0, 0, 0),
(10, 5, 'Please atten', '', '2020-06-17 06:05:28', 'a', 0, 'a', 0, 0, 0),
(11, 6, 'Please attend', '', '2020-06-17 06:37:50', 'a', 0, 'a', 0, 0, 0),
(12, 8, 'nothinggggggggg', '', '2020-06-26 03:07:29', 'a', 0, 'a', 0, 0, 0),
(13, 9, 'Answers to all questions are to be recorded on the multiple-choice form, either in the format A-E or True/False. ', '', '2020-06-29 06:45:58', 'a', 0, 'a', 0, 0, 0),
(14, 9, 'Mark one box only to indicate the answer you consider correct for each question.', '', '2020-06-29 06:45:58', 'a', 0, 'a', 0, 0, 0),
(15, 9, 'Close all programs, including email', '', '2020-06-29 06:45:58', 'a', 0, 'a', 0, 0, 0),
(16, 10, 'Exam time will be 6 min', '', '2020-06-29 07:23:53', 'a', 0, 'a', 0, 0, 0),
(17, 10, 'each question there will be 1.20 ', '', '2020-06-29 07:23:53', 'a', 0, 'a', 0, 0, 0),
(18, 11, 'test', '', '2020-06-29 08:08:56', 'a', 0, 'a', 0, 0, 0),
(19, 11, 'test2', '', '2020-06-29 08:08:56', 'a', 0, 'a', 0, 0, 0),
(20, 12, 'test', '', '2020-06-30 06:43:43', 'a', 0, 'a', 0, 0, 0),
(21, 13, 'test', '', '2020-07-01 03:22:03', 'a', 0, 'a', 0, 0, 0),
(22, 14, 'Test ', '', '2020-07-01 08:33:15', 'a', 0, 'a', 0, 0, 0),
(23, 15, 'Test ', '', '2020-07-01 08:51:08', 'a', 0, 'a', 0, 0, 0),
(24, 16, 'This is a test instruction', '', '2020-07-03 02:25:38', 'a', 0, 'a', 0, 0, 0),
(25, 17, 'test', '', '2020-07-03 05:36:07', 'a', 0, 'a', 0, 0, 0),
(26, 18, 'test', '', '2020-07-03 10:34:31', 'a', 0, 'a', 0, 0, 0),
(27, 19, 'test', '', '2020-07-06 00:11:47', 'a', 0, 'a', 0, 0, 0),
(28, 20, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '', '2020-07-06 04:53:28', 'a', 0, 'a', 0, 0, 0),
(29, 20, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '', '2020-07-06 04:53:28', 'a', 0, 'a', 0, 0, 0),
(30, 21, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '', '2020-07-06 04:57:40', 'a', 0, 'a', 0, 0, 0),
(31, 21, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '', '2020-07-06 04:57:40', 'a', 0, 'a', 0, 0, 0),
(32, 21, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '', '2020-07-06 04:57:40', 'a', 0, 'a', 0, 0, 0),
(33, 22, 'test', '', '2020-07-06 05:06:24', 'a', 0, 'a', 0, 0, 0),
(34, 23, 'test', '', '2020-07-08 06:05:30', 'a', 0, 'a', 0, 0, 0),
(35, 24, 'test', '', '2020-07-13 00:22:34', 'a', 0, 'a', 0, 0, 0),
(36, 25, 'test', '', '2020-07-13 01:10:41', 'a', 0, 'a', 0, 0, 0),
(37, 26, 'test', '', '2020-07-13 01:12:26', 'a', 0, 'a', 0, 0, 0),
(38, 27, 'test', '', '2020-07-13 01:14:58', 'a', 0, 'a', 0, 0, 0),
(39, 28, 'test', '', '2020-07-13 01:17:52', 'a', 0, 'a', 0, 0, 0),
(40, 29, 'test', '', '2020-07-13 01:21:05', 'a', 0, 'a', 0, 0, 0),
(41, 30, 'test', '', '2020-07-13 01:37:17', 'a', 0, 'a', 0, 0, 0),
(42, 31, 'test', '', '2020-07-13 01:39:02', 'a', 0, 'a', 0, 0, 0),
(43, 32, 'test', '', '2020-07-15 06:12:07', 'a', 0, 'a', 0, 0, 0),
(44, 33, 'test', '', '2020-07-15 06:25:59', 'a', 0, 'a', 0, 0, 0),
(45, 34, 'test', '', '2020-07-17 06:58:49', 'a', 0, 'a', 0, 0, 0),
(46, 35, 'test', '', '2020-07-17 07:29:54', 'a', 0, 'a', 0, 0, 0),
(47, 36, 'test', '', '2020-08-24 05:10:26', 'a', 0, 'a', 0, 0, 0),
(48, 37, 'test', '', '2020-08-24 05:18:21', 'a', 0, 'a', 0, 0, 0),
(49, 38, 'test', '', '2020-09-02 03:20:35', 'a', 0, 'a', 0, 0, 0),
(50, 39, 'test', '', '2020-09-02 03:23:23', 'a', 0, 'a', 0, 0, 0),
(51, 40, 'test', '', '2020-09-02 10:14:19', 'a', 0, 'a', 0, 0, 0),
(52, 41, 'testing', '', '2020-09-04 04:06:40', 'a', 0, 'a', 0, 0, 0),
(53, 41, 'testing', '', '2020-09-04 04:06:40', 'a', 0, 'a', 0, 0, 0),
(54, 41, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '', '2020-09-04 04:06:40', 'a', 0, 'a', 0, 0, 0),
(55, 41, 'This is a test instruction', '', '2020-09-04 04:06:40', 'a', 0, 'a', 0, 0, 0),
(56, 42, 'test', '', '2020-09-04 14:02:48', 'a', 0, 'a', 0, 0, 0),
(57, 43, 'testing', 'testing', '2020-09-05 00:10:47', 'a', 0, 'a', 0, 0, 0),
(58, 43, 'testing1', 'testing1', '2020-09-05 00:10:47', 'a', 0, 'a', 0, 0, 0),
(59, 44, 'test', '', '2020-09-05 05:24:37', 'a', 0, 'a', 0, 0, 0),
(60, 45, 'test', 'يسشيشرؤشسؤشسؤ', '2020-09-20 00:34:55', 'a', 0, 'a', 0, 0, 0),
(61, 46, 'test12', '', '2020-10-21 05:56:20', 'a', 0, 'a', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `home_slider`
--

CREATE TABLE `home_slider` (
  `slide_id` int(11) NOT NULL,
  `slide_image` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_slider`
--

INSERT INTO `home_slider` (`slide_id`, `slide_image`, `status`) VALUES
(1, 'uploads/slider/slide-3.jpg', 1),
(2, 'uploads/slider/slide-2.jpg', 1),
(3, 'uploads/slider/slide-1.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `m_id` int(11) NOT NULL,
  `item` varchar(100) NOT NULL,
  `status` enum('1','0') NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`m_id`, `item`, `status`, `timestamp`) VALUES
(1, 'Students', '1', '2020-06-26 18:30:00'),
(2, 'Course', '1', '2020-06-26 18:30:00'),
(3, 'Trainers', '1', '2020-06-26 18:30:00'),
(4, 'Category', '1', '2020-06-26 18:30:00'),
(5, 'Sports', '1', '2020-06-26 18:30:00'),
(6, 'Organization', '1', '2020-06-26 18:30:00'),
(22, 'Section', '1', '2020-06-26 18:30:00'),
(23, 'Batch', '1', '2020-06-26 18:30:00'),
(24, 'Class', '1', '2020-06-26 18:30:00'),
(25, 'Events', '1', '2020-06-26 18:30:00'),
(26, 'Notice', '1', '2020-06-26 18:30:00'),
(27, 'Finance', '1', '2020-06-26 18:30:00'),
(28, 'Subadmin', '1', '2020-06-26 18:30:00'),
(29, 'Exam', '1', '2020-06-26 18:30:00'),
(30, 'Question', '1', '2020-06-26 18:30:00'),
(31, 'Instruction', '1', '2020-06-26 18:30:00'),
(32, 'Education', '1', '2020-06-26 18:30:00'),
(33, 'Sliders', '1', '2020-07-04 04:22:09'),
(34, 'Attendance', '1', '2020-07-04 07:42:40'),
(35, 'Assignment', '1', '2020-07-13 02:24:53'),
(36, 'Organizers', '1', '2020-07-29 00:42:46'),
(37, 'Resources', '1', '2020-07-29 00:56:55');

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `note_id` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(200) NOT NULL,
  `description_arabic` text NOT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `type` enum('Teacher','Student') NOT NULL DEFAULT 'Teacher',
  `added_by` enum('admin','subadmin') NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_by` enum('admin','subadmin') NOT NULL,
  `edited_subadmin` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`note_id`, `title`, `description`, `title_arabic`, `description_arabic`, `start_time`, `end_time`, `type`, `added_by`, `added_subadmin`, `edited_by`, `edited_subadmin`, `timestamp`) VALUES
(3, 'JAVA', 'Java is a general-purpose programming language that is class-based, object-oriented, and designed to have as few implementation dependencies as possible', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-07-06 23:58:00', '2020-07-07 23:58:00', 'Teacher', 'admin', 0, 'admin', 0, '2020-08-19 06:00:34'),
(9, 'test', 'تأجيل موعد الدورة', 'ما هو لوريم ايبسوم', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم\r\n\r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت\r\n\r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي\r\n\r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي\r\n\r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم\r\n\r\nايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد\r\n\r\nكيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام\r\n\r\nألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم\r\n\r\nيلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي\r\n\r\nكونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي\r\n\r\nفيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم فيوجايات كيو\r\n\r\nفوليوبتاس نيولا باراياتيور؟', '2020-07-20 05:28:00', '2020-07-23 05:28:00', 'Student', 'admin', 0, 'admin', 0, '2020-08-19 06:00:34');

-- --------------------------------------------------------

--
-- Table structure for table `notes_students`
--

CREATE TABLE `notes_students` (
  `ns_id` bigint(20) NOT NULL,
  `note_id` bigint(20) NOT NULL,
  `stu_id` bigint(20) NOT NULL,
  `read_status` tinyint(1) NOT NULL DEFAULT 0,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notes_students`
--

INSERT INTO `notes_students` (`ns_id`, `note_id`, `stu_id`, `read_status`, `timestamp`) VALUES
(5, 11, 44, 1, '2020-08-24 08:05:57'),
(6, 12, 44, 1, '2020-08-24 11:07:46');

-- --------------------------------------------------------

--
-- Table structure for table `organisers`
--

CREATE TABLE `organisers` (
  `organiser_id` int(11) NOT NULL,
  `organiser_first` varchar(200) NOT NULL,
  `organiser_second` varchar(200) NOT NULL,
  `arabic_first` varchar(200) NOT NULL,
  `arabic_second` varchar(200) NOT NULL,
  `organiser_cpr` varchar(100) NOT NULL,
  `organiser_mobile` varchar(20) NOT NULL,
  `organiser_email` varchar(200) NOT NULL,
  `organiser_education` varchar(200) NOT NULL,
  `organiser_experience` text NOT NULL,
  `education_arabic` varchar(100) NOT NULL,
  `experience_arabic` varchar(200) NOT NULL,
  `organiser_contact` text NOT NULL,
  `organiser_image` varchar(200) NOT NULL,
  `organiser_status` tinyint(1) NOT NULL DEFAULT 1,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `added_by` enum('admin','subadmin') NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_by` enum('admin','subadmin') NOT NULL,
  `edited_subadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `organisers`
--

INSERT INTO `organisers` (`organiser_id`, `organiser_first`, `organiser_second`, `arabic_first`, `arabic_second`, `organiser_cpr`, `organiser_mobile`, `organiser_email`, `organiser_education`, `organiser_experience`, `education_arabic`, `experience_arabic`, `organiser_contact`, `organiser_image`, `organiser_status`, `timestamp`, `added_by`, `added_subadmin`, `edited_by`, `edited_subadmin`) VALUES
(1, 'Aithiner', 'EP', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'CPR889IU887', '87145012', 'aithinep@gmail.com', 'Bachelor of Technology', '3 years in business development', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Aithin EP\r\nKozhikode - 673014', '', 1, '2020-08-19 06:10:09', 'admin', 0, 'admin', 0),
(2, 'Test', 'test', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '5555555', '66666666', 'test@gmail.com', 'bsc', '3', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'manama', 'uploads/organisers/202007178760image_search_1594964919900.png', 1, '2020-08-19 06:10:09', 'admin', 0, 'subadmin', 1),
(3, 'Testtttttttttttttt', 'T', '', '', '7877777777', '12254785', 'teSt@gmail.com', 'BSC', '5', '', '', 'Manama', 'uploads/organisers/202008243318.png', 1, '2020-08-24 07:15:03', 'admin', 0, 'admin', 0),
(4, 'Jahu', 'Jah', 'حسين', 'حسين', '21', '212121515454', 'jahu@gmail.com', 'PG', '20', 'حسين', 'حسين', 'Bahrain', 'uploads/organisers/202103242461.png', 1, '2021-03-24 09:35:57', 'admin', 0, 'admin', 0);

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `org_id` int(11) NOT NULL,
  `organization` mediumtext NOT NULL,
  `organization_arabic` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `added_by` enum('admin','subadmin') NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_by` enum('admin','subadmin') NOT NULL,
  `edited_subadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`org_id`, `organization`, `organization_arabic`, `status`, `timestamp`, `added_by`, `added_subadmin`, `edited_by`, `edited_subadmin`) VALUES
(1, 'World Health Organizations qq', 'ما هو لوريم ايبسوم', 0, '2020-08-19 06:25:49', 'admin', 0, 'admin', 0),
(2, 'WHO', 'ما هو لوريم ايبسوم', 0, '2020-08-19 06:25:49', 'admin', 0, 'admin', 0),
(3, 'TEST', 'ما هو لوريم ايبسوم', 1, '2020-08-19 06:25:49', 'admin', 0, 'admin', 0),
(4, 'testtttt', 'ما هو لوريم ايبسوم', 1, '2020-08-19 06:25:49', 'admin', 0, 'admin', 0),
(5, 'whoo', 'ما هو لوريم ايبسوم', 1, '2020-08-19 06:25:49', 'admin', 0, 'admin', 0);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset`
--

CREATE TABLE `password_reset` (
  `reset_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `signature` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `priority`
--

CREATE TABLE `priority` (
  `pr_id` int(11) NOT NULL,
  `item` varchar(200) NOT NULL,
  `all` enum('1','0','2') NOT NULL,
  `add` enum('1','0') NOT NULL,
  `edit` enum('1','0') NOT NULL,
  `view` enum('1','0') NOT NULL,
  `block` enum('1','0') NOT NULL,
  `delete` enum('1','0') NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `priority`
--

INSERT INTO `priority` (`pr_id`, `item`, `all`, `add`, `edit`, `view`, `block`, `delete`, `timestamp`) VALUES
(1, 'Category', '0', '0', '0', '0', '0', '0', '2020-06-24 18:30:00'),
(2, 'Students', '0', '0', '0', '0', '0', '0', '2020-06-24 18:30:00'),
(3, 'Trainers', '0', '0', '0', '0', '0', '0', '2020-06-24 18:30:00'),
(4, 'Course', '0', '0', '0', '0', '0', '0', '2020-06-24 18:30:00'),
(5, 'Batches', '0', '0', '0', '0', '0', '0', '2020-06-24 18:30:00'),
(6, 'Sections', '0', '0', '0', '0', '0', '0', '2020-06-24 18:30:00'),
(8, 'Events', '0', '0', '0', '0', '0', '0', '2020-06-24 18:30:00'),
(9, 'Notice', '0', '0', '0', '0', '0', '0', '2020-06-24 18:30:00'),
(10, 'Finance', '0', '0', '0', '0', '0', '0', '2020-06-24 18:30:00'),
(14, 'Class', '0', '0', '0', '0', '0', '0', '2020-06-24 18:30:00'),
(15, 'Education', '0', '0', '0', '0', '0', '0', '2020-06-24 18:30:00'),
(16, 'Sports', '0', '0', '0', '0', '0', '0', '2020-06-24 18:30:00'),
(17, 'Organization', '0', '0', '0', '0', '0', '0', '2020-06-24 18:30:00'),
(18, 'Exams', '0', '0', '0', '0', '0', '0', '2020-06-25 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `que_id` bigint(20) NOT NULL,
  `question` mediumtext NOT NULL,
  `formula_question` text DEFAULT NULL,
  `formula_question_arabic` text DEFAULT NULL,
  `formula_a` text DEFAULT '',
  `formula_b` text DEFAULT '',
  `formula_c` text DEFAULT '',
  `formula_d` text DEFAULT '',
  `formula_e` text DEFAULT '',
  `formula_a_arabic` text DEFAULT '',
  `formula_b_arabic` text DEFAULT '',
  `formula_c_arabic` text DEFAULT '',
  `formula_d_arabic` text DEFAULT '',
  `formula_e_arabic` text DEFAULT '',
  `attachment` tinyint(1) NOT NULL DEFAULT 0,
  `image` text NOT NULL,
  `ans` enum('a','b','c','d','e') NOT NULL,
  `correct` double NOT NULL DEFAULT 2,
  `negative` double NOT NULL DEFAULT 1,
  `exam_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `added_by` enum('a','t','s') NOT NULL,
  `trainer_id` int(11) NOT NULL,
  `edited_by` enum('a','t','s') NOT NULL,
  `edited_trainer` int(11) NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_subadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`que_id`, `question`, `formula_question`, `formula_question_arabic`, `formula_a`, `formula_b`, `formula_c`, `formula_d`, `formula_e`, `formula_a_arabic`, `formula_b_arabic`, `formula_c_arabic`, `formula_d_arabic`, `formula_e_arabic`, `attachment`, `image`, `ans`, `correct`, `negative`, `exam_id`, `timestamp`, `added_by`, `trainer_id`, `edited_by`, `edited_trainer`, `added_subadmin`, `edited_subadmin`) VALUES
(40, '', '<p><b style=\"padding: 0px; margin: 0px; font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">Which file contains settings for all .NET application types, such as Windows, Console, ClassLibrary, and Web applications?</b><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">&nbsp;Web.config</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">&nbsp;Machine.config</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">Global.asax</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">All of the above</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'b', 1, 1, 10, '2020-06-29 09:54:56', 'a', 0, 'a', 0, 0, 0),
(41, '', '<p><b style=\"padding: 0px; margin: 0px; font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">Which programming model should you implement if you want to separate your server-side code from your client-side layout code in a Web page?</b><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">Single-file model</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">Code-behind model</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">Inline model</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">Client-server model</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'b', 1, 1, 10, '2020-06-29 09:55:38', 'a', 0, 'a', 0, 0, 0),
(42, '', '<p><b style=\"padding: 0px; margin: 0px; font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">You want to make a configuration setting change that will affect only the current Web application. Which file will you change?</b><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">Global.asax</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">Web.config in the root of the Web application</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">&nbsp;Machine.config</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">All of the above</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'b', 1, 1, 10, '2020-06-29 09:56:20', 'a', 0, 'a', 0, 0, 0),
(43, '', '<p><b style=\"padding: 0px; margin: 0px; font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">Which of the following is not an ASP.NET page event?</b><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">&nbsp;Init</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">&nbsp;Load</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">Import</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">None of the above.</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'c', 1, 1, 10, '2020-06-29 09:57:23', 'a', 0, 'a', 0, 0, 0),
(44, '', '<p><b style=\"padding: 0px; margin: 0px; font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">&nbsp;To implement a specified .NET Framework interface which directive is used?</b><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">@Register</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">@Control</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">@Reference</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">@Implements</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'd', 1, 1, 10, '2020-06-29 09:58:09', 'a', 0, 'a', 0, 0, 0),
(45, '', '<p><b style=\"padding: 0px; margin: 0px; font-family: Helvetica, &quot;Open Sans&quot;, Arial, sans-serif, Verdana; background-color: rgb(255, 255, 255);\">To create your application on a remote server which option you will choose in ASP.NET?</b><br data-mce-bogus=\"1\"></p>', '', '<p>a</p>', '<p>s</p>', '<p>d</p>', '', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'b', 1, 1, 11, '2020-06-29 10:39:51', 'a', 0, 'a', 0, 0, 0),
(46, '', '<p>test2</p>', '', '<p>1</p>', '<p>3</p>', '<p>3</p>', '<p>4</p>', '<p>5</p>', NULL, NULL, NULL, NULL, NULL, 0, '', 'c', 1, 1, 11, '2020-06-29 10:40:12', 'a', 0, 'a', 0, 0, 0),
(47, '', '<p>test3</p>', '', '<p>4</p>', '<p>6</p>', '<p>6</p>', '', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'c', 1, 1, 11, '2020-06-29 10:41:02', 'a', 0, 'a', 0, 0, 0),
(48, '', '<p>test6</p>', '', '<p>5</p>', '<p>6</p>', '<p>7</p>', '', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 1, 1, 11, '2020-06-29 10:41:17', 'a', 0, 'a', 0, 0, 0),
(49, '', '<p>test1</p>', '', '<p>1</p>', '<p>2</p>', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 10, 10, 12, '2020-06-30 09:14:24', 'a', 0, 'a', 0, 0, 0),
(50, '', '<p>test2</p>', '', '<p>1</p>', '<p>2</p>', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'b', 10, 10, 12, '2020-06-30 09:14:47', 'a', 0, 'a', 0, 0, 0),
(59, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">Which of the following option leads to the portability and security of Java?</span><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Bytecode is executed by JVM</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">The applet makes the Java code secure and portable</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Use of exception handling</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Dynamic binding between objects</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 1, 1, 20, '2020-07-06 07:23:28', 'a', 0, 'a', 0, 0, 0),
(60, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">Which of the following is not a Java features?</span><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Dynamic</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Architecture Neutral</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Use of pointers</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Object-oriented</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'c', 2, 1, 20, '2020-07-06 07:23:28', 'a', 0, 'a', 0, 0, 0),
(61, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">&nbsp;What should be the execution order, if a class has a method, static block, instance block, and constructor, as shown below?</span><br data-mce-bogus=\"1\"></p><ol start=\"1\" class=\"dp-j\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: none; font-family: verdana; font-size: 13px; background-color: rgba(220, 206, 206, 0.3);\"><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\"><span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">public</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;</span><span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">class</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;First_C&nbsp;{&nbsp;&nbsp;</span></span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">public</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;</span><span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">void</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;myMethod()&nbsp;&nbsp;&nbsp;</span></span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;{&nbsp;&nbsp;</span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(<span class=\"string\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 0, 255); background-color: inherit;\">\"Method\"</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">);&nbsp;&nbsp;</span></span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;&nbsp;</span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;{&nbsp;&nbsp;</span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(<span class=\"string\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 0, 255); background-color: inherit;\">\"&nbsp;Instance&nbsp;Block\"</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">);&nbsp;&nbsp;</span></span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;&nbsp;</span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">public</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;</span><span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">void</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;First_C()&nbsp;&nbsp;</span></span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;{&nbsp;&nbsp;</span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(<span class=\"string\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 0, 255); background-color: inherit;\">\"Constructor&nbsp;\"</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">);&nbsp;&nbsp;</span></span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;&nbsp;</span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">static</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;{&nbsp;&nbsp;</span></span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(<span class=\"string\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 0, 255); background-color: inherit;\">\"static&nbsp;block\"</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">);&nbsp;&nbsp;</span></span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;&nbsp;</span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">public</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;</span><span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">static</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;</span><span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">void</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;main(String[]&nbsp;args)&nbsp;{&nbsp;&nbsp;</span></span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;First_C&nbsp;c&nbsp;=&nbsp;<span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">new</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;First_C();&nbsp;&nbsp;</span></span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;c.First_C();&nbsp;&nbsp;</span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;c.myMethod();&nbsp;&nbsp;</span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;}&nbsp;&nbsp;</span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">}&nbsp; &nbsp;</span></li></ol>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Instance block, method, static block, and constructor</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Method, constructor, instance block, and static block</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Static block, method, instance block, and constructor</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Static block, instance block, constructor, and method</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'd', 4, 2, 20, '2020-07-06 07:23:28', 'a', 0, 'a', 0, 0, 0),
(62, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">The \\u0021 article referred to as a</span><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Unicode escape sequence</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Octal escape</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Hexadecimal</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Line feed</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 1, 1, 20, '2020-07-06 07:23:28', 'a', 0, 'a', 0, 0, 0),
(63, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">_____ is used to find and fix bugs in the Java programs.</span><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">JVM</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">JRE</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">JDK</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">JDB</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'd', 1, 1, 20, '2020-07-06 07:23:28', 'a', 0, 'a', 0, 0, 0),
(64, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">Which of the following is a valid declaration of a char?</span><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">char ch = \'\\utea\';</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">char ca = \'tea\';</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">char cr = \\u0223;</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">char cc = \'\\itea\';</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 1, 1, 20, '2020-07-06 07:23:28', 'a', 0, 'a', 0, 0, 0),
(65, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">What is the return type of the hashCode() method in the Object class?</span><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Object</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">int</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">long</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">void</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'b', 1, 1, 20, '2020-07-06 07:23:28', 'a', 0, 'a', 0, 0, 0),
(66, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">Which of the following is a valid long literal?</span><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">ABH8097</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">L990023</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">904423</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">0xnf029L</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'd', 1, 1, 20, '2020-07-06 07:23:28', 'a', 0, 'a', 0, 0, 0),
(67, '', '<p>Java</p>', '', '<p>a</p>', '<p>b</p>', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 2, 1, 20, '2020-07-06 07:23:28', 'a', 0, 'a', 0, 0, 0),
(68, '', '<p>Oops concept</p>', '', '<p>Data abstraction</p>', '<p>class</p>', '<p>Object</p>', '<p>All of above</p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'd', 2, 1, 20, '2020-07-06 07:23:28', 'a', 0, 'a', 0, 0, 0),
(74, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">Which of the following option leads to the portability and security of Java?</span><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Bytecode is executed by JVM</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">The applet makes the Java code secure and portable</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Use of exception handling</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Dynamic binding between objects</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 1, 1, 21, '2020-07-06 07:27:40', 'a', 0, 'a', 0, 0, 0),
(75, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">Which of the following is not a Java features?</span><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Dynamic</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Architecture Neutral</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Use of pointers</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Object-oriented</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'c', 2, 1, 21, '2020-07-06 07:27:40', 'a', 0, 'a', 0, 0, 0),
(76, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">&nbsp;What should be the execution order, if a class has a method, static block, instance block, and constructor, as shown below?</span><br data-mce-bogus=\"1\"></p><ol start=\"1\" class=\"dp-j\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: none; font-family: verdana; font-size: 13px; background-color: rgba(220, 206, 206, 0.3);\"><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\"><span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">public</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;</span><span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">class</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;First_C&nbsp;{&nbsp;&nbsp;</span></span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">public</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;</span><span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">void</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;myMethod()&nbsp;&nbsp;&nbsp;</span></span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;{&nbsp;&nbsp;</span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(<span class=\"string\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 0, 255); background-color: inherit;\">\"Method\"</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">);&nbsp;&nbsp;</span></span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;&nbsp;</span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;{&nbsp;&nbsp;</span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(<span class=\"string\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 0, 255); background-color: inherit;\">\"&nbsp;Instance&nbsp;Block\"</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">);&nbsp;&nbsp;</span></span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;&nbsp;</span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">public</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;</span><span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">void</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;First_C()&nbsp;&nbsp;</span></span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;{&nbsp;&nbsp;</span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(<span class=\"string\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 0, 255); background-color: inherit;\">\"Constructor&nbsp;\"</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">);&nbsp;&nbsp;</span></span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;&nbsp;</span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">static</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;{&nbsp;&nbsp;</span></span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(<span class=\"string\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 0, 255); background-color: inherit;\">\"static&nbsp;block\"</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">);&nbsp;&nbsp;</span></span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;&nbsp;</span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">public</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;</span><span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">static</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;</span><span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">void</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;main(String[]&nbsp;args)&nbsp;{&nbsp;&nbsp;</span></span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;First_C&nbsp;c&nbsp;=&nbsp;<span class=\"keyword\" style=\"margin: 0px; padding: 0px; border: none; color: rgb(0, 102, 153); background-color: inherit; font-weight: 700;\">new</span><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;First_C();&nbsp;&nbsp;</span></span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;c.First_C();&nbsp;&nbsp;</span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;&nbsp;&nbsp;c.myMethod();&nbsp;&nbsp;</span></li><li class=\"\" style=\"margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">&nbsp;&nbsp;}&nbsp;&nbsp;</span></li><li class=\"alt\" style=\"width: 850px; margin: 4px 0px 0px; border: none; line-height: 21px; padding: 0px 3px 0px 10px !important;\"><span style=\"margin: 0px; padding: 0px; border: none; background-color: inherit;\">}&nbsp; &nbsp;</span></li></ol>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Instance block, method, static block, and constructor</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Method, constructor, instance block, and static block</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Static block, method, instance block, and constructor</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Static block, instance block, constructor, and method</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'd', 4, 2, 21, '2020-07-06 07:27:40', 'a', 0, 'a', 0, 0, 0),
(77, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">The \\u0021 article referred to as a</span><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Unicode escape sequence</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Octal escape</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Hexadecimal</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Line feed</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 1, 1, 21, '2020-07-06 07:27:40', 'a', 0, 'a', 0, 0, 0),
(78, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">_____ is used to find and fix bugs in the Java programs.</span><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">JVM</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">JRE</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">JDK</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">JDB</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'd', 1, 1, 21, '2020-07-06 07:27:40', 'a', 0, 'a', 0, 0, 0),
(79, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">Which of the following is a valid declaration of a char?</span><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">char ch = \'\\utea\';</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">char ca = \'tea\';</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">char cr = \\u0223;</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">char cc = \'\\itea\';</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 1, 1, 21, '2020-07-06 07:27:40', 'a', 0, 'a', 0, 0, 0),
(80, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">What is the return type of the hashCode() method in the Object class?</span><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">Object</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">int</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">long</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">void</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'b', 1, 1, 21, '2020-07-06 07:27:40', 'a', 0, 'a', 0, 0, 0),
(81, '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">Which of the following is a valid long literal?</span><br data-mce-bogus=\"1\"></p>', '', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">ABH8097</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">L990023</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">904423</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">0xnf029L</span><br data-mce-bogus=\"1\"></p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'd', 1, 1, 21, '2020-07-06 07:27:40', 'a', 0, 'a', 0, 0, 0),
(82, '', '<p>Java</p>', '', '<p>a</p>', '<p>b</p>', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 2, 1, 21, '2020-07-06 07:27:40', 'a', 0, 'a', 0, 0, 0),
(83, '', '<p>Oops concept</p>', '', '<p>Data abstraction</p>', '<p>class</p>', '<p>Object</p>', '<p>All of above</p>', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'd', 2, 1, 21, '2020-07-06 07:27:40', 'a', 0, 'a', 0, 0, 0),
(99, '', '<p>test1</p>', '', '<p>23</p>', '<p>34</p>', '<p>44</p>', '<p>33</p>', '<p>56</p>', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 5, 5, 24, '2020-07-13 05:22:58', 'a', 0, 'a', 0, 0, 0),
(100, '', '<p>test45555</p>', '', '<p>h</p>', '<p>t</p>', '<p>tt</p>', '<p>rr</p>', '<p>wi</p>', NULL, NULL, NULL, NULL, NULL, 0, '', 'e', 10, 1, 24, '2020-07-13 05:23:37', 'a', 0, 'a', 0, 0, 0),
(101, '', '<p>test</p>', '', '<p>w</p>', '<p>e</p>', '<p>d</p>', '<p>kj</p>', '<p>vg</p>', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 10, 10, 25, '2020-07-13 06:11:13', 'a', 0, 'a', 0, 0, 0),
(102, '', '<p>test</p>', '', '<p>12</p>', '<p>21</p>', '<p>23</p>', '<p>32</p>', '<p>34</p>', NULL, NULL, NULL, NULL, NULL, 0, '', 'e', 10, 10, 26, '2020-07-13 06:12:51', 'a', 0, 'a', 0, 0, 0),
(103, '', '<p>test1</p>', '', '<p>1</p>', '<p>2</p>', '<p>3</p>', '<p>4</p>', '<p>5</p>', NULL, NULL, NULL, NULL, NULL, 0, '', 'e', 10, 10, 27, '2020-07-13 06:15:22', 'a', 0, 'a', 0, 0, 0),
(104, '', '<p>test3</p>', '', '<p>g</p>', '<p>b</p>', '<p>4</p>', '<p>5</p>', '<p>7</p>', NULL, NULL, NULL, NULL, NULL, 1, 'uploads/questions/202007133919.png', 'b', 10, 10, 27, '2020-07-13 06:15:51', 'a', 0, 'a', 0, 0, 0),
(105, '', '<p>test1</p>', '', '<p>1</p>', '<p>2</p>', '<p>3</p>', '<p>4</p>', '<p>5</p>', NULL, NULL, NULL, NULL, NULL, 0, '', 'e', 10, 10, 28, '2020-07-13 06:18:21', 'a', 0, 'a', 0, 0, 0),
(106, '', '<p>ngt</p>', '', '<p>78</p>', '<p>25</p>', '<p>52</p>', '', '<p>01</p>', NULL, NULL, NULL, NULL, NULL, 1, 'uploads/questions/202007137476.png', 'e', 10, 10, 28, '2020-07-13 06:19:06', 'a', 0, 'a', 0, 0, 0),
(107, '', '<p>testtttttttttttttttttttttttttttttt</p>', '', '<p>12</p>', '<p>123</p>', '<p>1234</p>', '', '<p>12345<br data-mce-bogus=\"1\"></p>', NULL, NULL, NULL, NULL, NULL, 0, '', 'e', 10, 10, 29, '2020-07-13 06:21:46', 'a', 0, 'a', 0, 0, 0),
(110, '', '<p>test</p>', '', '<p>1</p>', '<p>2</p>', '', '', '', NULL, NULL, NULL, NULL, NULL, 1, 'uploads/questions/202007133705.png', 'a', 10, 10, 31, '2020-07-13 06:39:26', 'a', 0, 'a', 0, 0, 0),
(117, '', '<p>test1</p>', '', '<p>1</p>', '<p>2</p>', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 10, 10, 36, '2020-08-24 10:10:54', 'a', 0, 'a', 0, 0, 0),
(118, '', '<p>test2</p>', '', '<p>12</p>', '<p>23</p>', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'b', 10, 10, 36, '2020-08-24 10:11:14', 'a', 0, 'a', 0, 0, 0),
(119, '', '<p>test</p>', '', '<p>21</p>', '<p>52</p>', '', '', '', NULL, NULL, NULL, NULL, NULL, 1, 'uploads/questions/202008246410.png', 'a', 10, 10, 37, '2020-08-24 10:18:50', 'a', 0, 'a', 0, 0, 0),
(120, '', '<p>a</p>', NULL, '<p>1</p>', '<p>2</p>', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'b', 1, 1, 38, '2020-09-02 08:20:53', 'a', 0, 'a', 0, 0, 0);
INSERT INTO `questions` (`que_id`, `question`, `formula_question`, `formula_question_arabic`, `formula_a`, `formula_b`, `formula_c`, `formula_d`, `formula_e`, `formula_a_arabic`, `formula_b_arabic`, `formula_c_arabic`, `formula_d_arabic`, `formula_e_arabic`, `attachment`, `image`, `ans`, `correct`, `negative`, `exam_id`, `timestamp`, `added_by`, `trainer_id`, `edited_by`, `edited_trainer`, `added_subadmin`, `edited_subadmin`) VALUES
(121, '', '<p><span style=\"font-weight: 700; color: rgb(51, 51, 51); font-family: Roboto, Arial, sans-serif; font-size: 24px; text-indent: -24px; background-color: rgba(255, 255, 255, 0.1);\"><span data-mce-style=\"line-height: 115%; font-family: \'Sakkal Majalla\';\" style=\"line-height: 27.6px; font-family: &quot;Sakkal Majalla&quot;; background-color: transparent !important;\"><span data-mce-style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: \'Times New Roman\';\" style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-family: &quot;Times New Roman&quot;; background-color: transparent !important;\">&nbsp;</span></span><span lang=\"AR-SA\" data-mce-style=\"line-height: 115%; font-family: \'Sakkal Majalla\';\" style=\"line-height: 27.6px; font-family: &quot;Sakkal Majalla&quot;; background-color: transparent !important;\">التدريب ذو الهيكلية الجيدة يتكون من:</span></span><br data-mce-bogus=\"1\"></p>', NULL, '<p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Sakkal Majalla&quot;; font-size: 18.6667px; text-indent: -23.8px; background-color: rgba(255, 255, 255, 0.1);\">الأحماء _ القسم الرئيسي_ التهدئة.</span><br data-mce-bogus=\"1\"></p>', '<p><span style=\"color: rgb(51, 51, 51); font-family: &quot;Sakkal Majalla&quot;; font-size: 18.6667px; text-indent: -23.8px; background-color: rgba(255, 255, 255, 0.1);\">المقدمة _ القسم الرئيسي _ الخاتمة.</span><br data-mce-bogus=\"1\"></p>', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 1, 1, 39, '2020-09-02 08:24:02', 'a', 0, 'a', 0, 0, 0),
(122, '', '<p>a</p>', NULL, '<p>1</p>', '<p>2</p>', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 1, 1, 40, '2020-09-02 15:14:36', 'a', 0, 'a', 0, 0, 0),
(123, '', '<p>This is a test question-test</p>', '<p>test123</p>', '<p>Test option-test</p>', '<p>Test option-test</p>', '<p>Test option-test</p>', '<p>Test option-test</p>', '<p>Test option-test</p>', '<p>test</p>', '<p>test</p>', '<p>test</p>', '<p>test</p>', '<p>test</p>', 0, '', 'd', 2, 0.5, 41, '2020-09-04 12:02:39', 'a', 0, 'a', 0, 0, 0),
(124, '', '<p>Testing</p>', '', '<p>1</p>', '<p>2</p>', '<p>3</p>', '', '', '', '', '', '', '', 0, '', 'b', 2, 1, 41, '2020-09-04 11:32:10', 'a', 0, 'a', 0, 0, 0),
(125, '', '<p>Testing</p>', '', '<p>1</p>', '<p>2</p>', '<p>3</p>', '', '', '', '', '', '', '', 0, '', 'b', 2, 1, 41, '2020-09-04 11:34:27', 'a', 0, 'a', 0, 0, 0),
(126, '', '<p>Testing</p>', '', '<p>a</p>', '<p>b</p>', '<p>c</p>', '', '', '', '', '', '', '', 0, '', 'c', 2, 1, 41, '2020-09-04 11:34:48', 'a', 0, 'a', 0, 0, 0),
(127, '', '<p>Testing</p>', '', '<p>ass</p>', '<p>as</p>', '<p>asas</p>', '', '', '', '', '', '', '', 0, '', 'b', 2, 2, 41, '2020-09-04 11:36:29', 'a', 0, 'a', 0, 0, 0),
(128, '', '<p>a</p>', NULL, '<p>1</p>', '<p>2</p>', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 1, 1, 42, '2020-09-04 19:02:48', 'a', 0, 'a', 0, 0, 0),
(129, '', '<p>test1</p>', '', '<p>1</p>', '<p>2</p>', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'a', 10, 10, 43, '2020-09-05 05:10:47', 'a', 0, 'a', 0, 0, 0),
(130, '', '<p>test2</p>', '', '<p>1</p>', '<p>2</p>', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, '', 'b', 10, 10, 43, '2020-09-05 05:10:47', 'a', 0, 'a', 0, 0, 0),
(132, '', '<p>wdd</p>', '', '<p>saaaaaaaaaa</p>', '<p>wwwwwwwwwwww</p>', '<p>eeeeeeeeeee</p>', '', '', '', '', '', '', '', 0, '', 'b', 12, 12, 44, '2020-09-05 10:25:09', 'a', 0, 'a', 0, 0, 0),
(133, '', '<p>zsdxfcvg</p>', '', '<p>werty</p>', '<p>hk</p>', '', '', '', '', '', '', '', '', 1, 'uploads/questions/202009054336.png', 'a', 11, 11, 44, '2020-09-05 10:25:47', 'a', 0, 'a', 0, 0, 0),
(134, '', '<p>adsad</p>', '<p>تنيبتشمنىبشمنىبؤمشنبىمن</p>', '<p>1</p>', '<p>2</p>', '', '', '', '<p>ىسشمىؤر</p>', '<p>&nbsp;يوسشزؤنشم</p>', '', '', '', 0, '', 'a', 1, 1, 45, '2020-09-20 05:35:37', 'a', 0, 'a', 0, 0, 0),
(135, '', '<p>test</p>', '', '<p>1</p>', '<p>6</p>', '', '', '', '', '', '', '', '', 0, '', 'a', 1, 1, 46, '2020-10-21 10:56:46', 'a', 0, 'a', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `address` mediumtext NOT NULL,
  `address_arabic` mediumtext NOT NULL,
  `email` mediumtext NOT NULL,
  `mobile` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `address`, `address_arabic`, `email`, `mobile`) VALUES
(1, 'Manama, Bahrain', 'المنامة ، البحرين', 'boa@boc.bh', ' + 973 17176666');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `slider_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `added_by` enum('admin','subadmin') NOT NULL,
  `added_subadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`slider_id`, `name`, `image`, `status`, `timestamp`, `added_by`, `added_subadmin`) VALUES
(31, 'slider two', 'uploads/sliders/202007197954886.jpg', 1, '2020-07-19 08:28:23', 'admin', 0),
(32, 'slider three', 'uploads/sliders/20200719518987.jpg', 1, '2020-07-19 08:28:42', 'admin', 0),
(33, 'slider four', 'uploads/sliders/202007198904467.jpg', 1, '2020-07-19 08:29:05', 'admin', 0),
(34, 'slider five', 'uploads/sliders/2020071996792020070880206564.jpg', 1, '2020-07-19 08:29:23', 'admin', 0),
(40, 'level', 'uploads/sliders/202008042541rsz_202007282589b69a7423.jpg', 1, '2020-08-04 12:21:07', 'admin', 0),
(41, 'level', 'uploads/sliders/202008045115rsz_1202007283737b69a7263.jpg', 1, '2020-08-04 12:24:04', 'admin', 0),
(42, 'level', 'uploads/sliders/202008045975rsz_202007288328b69a7359.jpg', 1, '2020-08-04 12:25:02', 'admin', 0),
(43, 'level', 'uploads/sliders/202008042135rsz_202007289156b69a7313.jpg', 1, '2020-08-04 12:25:30', 'admin', 0),
(44, 'test', 'uploads/sliders/2020080576822.jpg', 1, '2020-08-05 09:29:28', 'admin', 0),
(49, 'Sliderss', 'uploads/trainers/202103237088.png', 1, '2021-03-23 14:11:51', 'admin', 0),
(50, 'Student', 'uploads/trainers/202103248268.png', 1, '2021-03-24 09:40:12', 'admin', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sports`
--

CREATE TABLE `sports` (
  `sports_id` int(11) NOT NULL,
  `sport` mediumtext NOT NULL,
  `sport_arabic` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `added_by` enum('admin','subadmin') NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_by` enum('admin','subadmin') NOT NULL,
  `edited_subadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sports`
--

INSERT INTO `sports` (`sports_id`, `sport`, `sport_arabic`, `status`, `timestamp`, `added_by`, `added_subadmin`, `edited_by`, `edited_subadmin`) VALUES
(1, 'Cricket ', 'ما هو لوريم ايبسوم', 1, '2020-08-19 06:37:36', 'admin', 0, 'admin', 0),
(2, 'Foot ball', 'ما هو لوريم ايبسوم', 1, '2020-08-19 06:37:36', 'admin', 0, 'admin', 0),
(3, 'TEST', 'ما هو لوريم ايبسوم', 1, '2020-08-19 06:37:36', 'admin', 0, 'admin', 0),
(4, 'testttttts', 'ما هو لوريم ايبسوم', 1, '2020-08-19 06:37:36', 'admin', 0, 'admin', 0),
(5, 'Badminton', 'ما هو لوريم ايبسوم', 1, '2020-08-19 06:37:36', 'admin', 0, 'admin', 0),
(6, 'testttt', 'ما هو لوريم ايبسوم', 0, '2020-08-19 06:37:36', 'subadmin', 10, 'subadmin', 10);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `stu_id` bigint(20) NOT NULL,
  `stu_name_english` varchar(100) NOT NULL,
  `stu_name_arabic` varchar(100) NOT NULL,
  `first_arabic` varchar(100) NOT NULL,
  `second_arabic` varchar(100) NOT NULL,
  `stu_gender` enum('Male','Female','Other') NOT NULL DEFAULT 'Male',
  `stu_cpr` varchar(100) NOT NULL,
  `stu_dob` date DEFAULT NULL,
  `stu_education` varchar(200) NOT NULL,
  `stu_mobile` varchar(20) NOT NULL,
  `stu_email` varchar(200) NOT NULL,
  `stu_timezone` varchar(100) NOT NULL,
  `stu_image` mediumtext NOT NULL,
  `stu_cv` text NOT NULL,
  `stu_contact` mediumtext NOT NULL,
  `stu_status` tinyint(1) NOT NULL DEFAULT 1,
  `stu_registration` enum('Waiting','Approved','Rejected') NOT NULL DEFAULT 'Approved',
  `stu_password` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `added_by` enum('admin','subadmin') NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_by` enum('admin','subadmin') NOT NULL,
  `edited_subadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`stu_id`, `stu_name_english`, `stu_name_arabic`, `first_arabic`, `second_arabic`, `stu_gender`, `stu_cpr`, `stu_dob`, `stu_education`, `stu_mobile`, `stu_email`, `stu_timezone`, `stu_image`, `stu_cv`, `stu_contact`, `stu_status`, `stu_registration`, `stu_password`, `timestamp`, `added_by`, `added_subadmin`, `edited_by`, `edited_subadmin`) VALUES
(1, 'Zainab', 'Sadeq', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '', NULL, '', '33988763', 'zalmosawi22@gmail.com', '', '', '', '', 1, 'Approved', '343fde5f3dc43fc04e82c3808dedba1b', '2020-08-19 08:52:42', 'admin', 0, 'admin', 0),
(2, 'athul', 'baby', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '', NULL, '', '33137593', 'a@gmail.com', '', 'uploads/students/202008184549audi-logo-vector-download.jpg', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-19 08:52:42', 'admin', 0, 'admin', 0),
(4, 'Marwa', 'Ahmedi', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '911105603', '1991-11-11', '', '39990062', 'myahmed@boc.bh', '', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-19 08:52:42', 'admin', 0, 'admin', 0),
(6, 'husain', 'ali', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '830804803', '1983-08-08', 'secondry', '33228000', 'hussain83h@gmail.com', '', '', '', 'manama-bahrain', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-19 08:52:42', 'admin', 0, 'admin', 0),
(7, 'Zainab ', 'Almosawi', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '9743343433', '1993-02-27', '', '33974997', 'zalmosawi@boc.bh', '', '', '', 'bahrain', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-19 08:52:42', 'admin', 0, 'admin', 0),
(8, 'mahmood', 'yateem', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '631202544', '2020-08-12', 'deploma', '39858781', 'myateem@bic.bh', '', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-19 08:52:42', 'admin', 0, 'admin', 0),
(9, 'zainab', 'almosawi', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '', NULL, '', '33977500', 'zalmosawi2@boc.bh', '', '', '', '', 1, 'Approved', '343fde5f3dc43fc04e82c3808dedba1b', '2020-08-19 08:52:42', 'admin', 0, 'admin', 0),
(10, 'mahmood', 'yateem', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '', NULL, '', '39858782', 'myateem@boc.bh', '', '', '', '', 1, 'Approved', '1fd58da3241bd9ae02de5a91db6d575f', '2020-08-19 08:52:42', 'admin', 0, 'admin', 0),
(11, 'Aithin', 'EP', '', '', 'Male', '', '0000-00-00', '', '121212121212', 'epaithin@gmail.com', '', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2021-03-13 07:29:28', 'admin', 0, 'admin', 0),
(12, 'Abdullah ', 'Alsaleem ', '', '', 'Male', '', '1995-08-25', '', '97680525', 'Boabood7@live.com', '', 'uploads/students/202008203650837EAA80-2D06-4E1B-BF5C-8E9A84C3E6CB.jpeg', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-20 14:43:59', 'admin', 0, 'admin', 0),
(13, 'Aït oufella', 'Meriem', '', '', 'Male', '', NULL, '', '0554294536', 'aitoufellameriem21@gmail.com', '', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-20 11:02:14', 'admin', 0, 'admin', 0),
(14, 'Alya', 'Alameery', '', '', 'Male', '870509438', '1987-05-13', 'A university student majoring in human resource management', '33019797', 'alyah87ma@gmail.com', '', '', '', 'H:1439\r\nR:1442\r\nB:814 isa town ', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-20 12:41:38', 'admin', 0, 'admin', 0),
(15, 'Hanya', 'george', '', '', 'Female', '', '0000-00-00', '', '56522011', 'hanyageorge@gmail.com', '', '', '', '', 1, 'Approved', '305d230e546ee6c3eeba4bf8c9a4ff08', '2020-08-24 05:13:45', 'admin', 0, 'admin', 0),
(16, 'Aithin', 'EP', '', '', 'Male', '', NULL, '', '871450127022', 'aithinep123@gmail.com', '', '', '', '', 1, 'Approved', '3b2ded44bdfea478dbef75e31a318b19', '2020-08-20 11:07:58', 'admin', 0, 'admin', 0),
(17, 'sasi', 's', '', '', 'Male', '', NULL, '', '3344556677', 'sa@gmail.com', '', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-20 11:08:56', 'admin', 0, 'admin', 0),
(18, 'جاسم', 'الزويد', '', '', 'Male', '', '1984-08-27', 'الثانوية ', '69693330', 'js5js@hotmail.com', '', '', '', 'المملكة العربية السعودية\r\nالشرقية /الاحساء', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-20 20:44:52', 'admin', 0, 'admin', 0),
(19, 'Hani', 'Al mohana', '', '', 'Male', '', NULL, '', '81914200', 'haniaa0x@gmail.com', '', 'uploads/students/202008235072.jpg', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-23 14:25:13', 'admin', 0, 'admin', 0),
(20, 'Manaf ', 'Alsaeed ', '', '', 'Male', '', NULL, '', '0505842395', 'manaf-121212@hotmail.com', '', '', '', '', 1, 'Approved', 'd1045f36be0e6f5ed10e1e8198f886c9', '2020-08-20 11:19:03', 'admin', 0, 'admin', 0),
(21, 'Hussain ', 'Abdullah ', '', '', 'Male', '', NULL, '', '0594352727', 'Hus12sain12@hotmail.com', '', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-20 11:22:23', 'admin', 0, 'admin', 0),
(22, 'Hussain', 'Alhanabi', '', '', 'Male', '', '1988-08-26', '', '08842339', 'geegar_4@hotmail.com', '', 'uploads/students/202008208276D6B460F6-9B5A-4C03-B78B-7DC146207E3D.jpeg', '', 'Saudi Arabia -safwa city', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-20 11:48:49', 'admin', 0, 'admin', 0),
(23, 'Atif', 'Mohammed Ahmed Elzaki', '', '', 'Male', '', NULL, '', '0125122716', 'afefff234@gmail.com', '', '', '', '', 1, 'Approved', '40c0f74c10520cd67812b62f84cc035a', '2020-08-20 12:00:13', 'admin', 0, 'admin', 0),
(24, 'Rabih bin Saleh bin Heikal Al Balushi', ' Al Balushi', '', '', 'Male', '', NULL, '', '99007300', 'Albndri521@gmqil.com', '', '', '', '', 1, 'Approved', 'ec21d44bcd007ffca26eb2331fed4caf', '2020-08-20 12:12:25', 'admin', 0, 'admin', 0),
(25, 'Hilal ', 'aljabri ', '', '', 'Male', '', '1972-09-30', 'bakaloryos', '91114990', 'hilal91114990@hotmail.com', '', 'uploads/students/202008204816DSC00009_2.JPG', '', 'oman', 1, 'Approved', '4176141268379e48e1027ec0290a62fd', '2020-08-20 12:25:35', 'admin', 0, 'admin', 0),
(26, 'Dalal', 'Alotaibi', '', '', 'Male', '', NULL, '', '966594557183', 'deealrooqi@gmail.com', '', '', '', '', 1, 'Approved', '9d02e5943a71d66befa5ffccdb52d509', '2020-08-20 12:28:37', 'admin', 0, 'admin', 0),
(27, 'wiem', 'messaadi', '', '', 'Male', '', NULL, '', '60987278', 'wiemmessaadi@gmail.com', '', 'uploads/students/20200823599520200823_054831.png', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-23 02:49:30', 'admin', 0, 'admin', 0),
(28, 'Amina', 'Mayouf ', '', '', 'Male', '', '1994-06-11', 'Bachelor + 5 years', '99593878', 'mayoufamina1@gmail.com', '', '', '', 'Algérie ', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-21 15:35:17', 'admin', 0, 'admin', 0),
(29, 'Mohammed ', 'Albanai', '', '', 'Male', '', NULL, '', '966549970978', 'yahay223356@gmai.com', '', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-20 20:13:37', 'admin', 0, 'admin', 0),
(30, 'Abdelmalek ', 'Mohand said ', '', '', 'Male', '', '1995-07-05', '', '4081694 ', 'malek.nadit11@gmail.com', '', 'uploads/students/202008225229IMG_9188.jpg', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-22 10:30:05', 'admin', 0, 'admin', 0),
(31, 'Abdulnabi ', 'Mandani ', '', '', 'Male', '', '1982-08-14', 'Middle school', '97323888', 'mandani_abd82@outlook.com', '', 'uploads/students/2020082155598E6E710A-6D2A-44E6-B9B4-03C64ACF3CDB.jpeg', '', 'Kuwait \r\nAdan city\r\nBlock 1\r\n47 street \r\nHome number 15\r\n', 1, 'Approved', '03881b9ad3d373dd9bd609cebb2a1c0a', '2020-08-21 12:14:51', 'admin', 0, 'admin', 0),
(32, 'Muneera', 'Al Qalaleef ', '', '', 'Male', '740205269', '1974-02-01', '', '36123450', 'olaa.x@hotmail.com', '', 'uploads/students/20200821696220200821_142612.jpg', '', 'Um al hassam, Bahrain', 1, 'Approved', '50438d8fca45a9bf20d72774430047f0', '2020-08-21 11:27:59', 'admin', 0, 'admin', 0),
(33, 'Faisal', 'Almandhari', '', '', 'Male', '', NULL, '', '96400229 ', 'falmanthry1993@gmail.com', '', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-21 12:31:59', 'admin', 0, 'admin', 0),
(34, 'mohamed', 'alfarsi', '', '', 'Male', '', NULL, '', '97338317', 'mohamed.farsi@moe.om', '', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-21 13:21:24', 'admin', 0, 'admin', 0),
(35, 'Ali', 'AL_HATAMI ', '', '', 'Male', '', NULL, '', '94179977', 'alialhatmi592@gmail.com', '', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-21 15:57:31', 'admin', 0, 'admin', 0),
(36, 'Thamer ', 'Aldossri', '', '', 'Male', '', NULL, '', '966559944944', 'thamer141414@hotmail.com', '', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-22 23:07:20', 'admin', 0, 'admin', 0),
(37, 'ABDE RAOUF', 'BOUDEHANE', '', '', 'Male', '', '1996-01-01', 'PhD Student - Strength and Conditioning', '21365550', 'boudehane.abderaouf@gmail.com', '', 'uploads/students/202008234669good one.jpg', '', '1016 LOGT Bt A23 N1 OUED ROMANE - EL ACHOUR  - ALGIERS', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-22 23:48:32', 'admin', 0, 'admin', 0),
(38, 'DOUNIA', 'BENAYACHE ', '', '', 'Male', '', '1997-01-14', '', '98657687', 'douniabenayache2828@gmail.com', '', 'uploads/students/202008234837inbound4395350278742287272.jpg', '', 'Algeria', 1, 'Approved', '3c772354496a62d149f63f8939ff8bce', '2020-08-23 07:22:46', 'admin', 0, 'admin', 0),
(39, 'Bouthaina', 'Benarfa', '', '', 'Male', '', NULL, '', '0672501539', 'bouthainabenarfa06@gmail.com', '', '', '', '', 1, 'Approved', '0334b98d8938c293399d717672da10eb', '2020-08-23 09:14:24', 'admin', 0, 'admin', 0),
(40, 'Ibrahim', 'Alharbi', '', '', 'Male', '', NULL, '', '966557770748', 'iibrahim956@gmail.com', '', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-23 09:51:30', 'admin', 0, 'admin', 0),
(41, 'يوسف حسين ', 'الناشي', '', '', 'Male', '', NULL, '', '٠٧٨٠٥٨٢٨٠٨٨', 'yusseifa@yahoo.com', '', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-23 15:18:47', 'admin', 0, 'admin', 0),
(42, 'Hafsa', 'khan', '', '', 'Female', 'CPR23455', '2020-08-04', '', '21457846', 'hafsakhan@gmail.com', '', 'uploads/students/202008247538.png', '', '', 0, 'Approved', '133564794a1bfbfad19be33f1b1e341c', '2020-08-24 05:24:50', 'admin', 0, 'admin', 0),
(43, 'Rasoii', 'Ras', '', '', 'Male', 'CPR234588', '2019-12-31', 'bsc', '77777777', 'rasoi@gmail.comi', '', '', '', 'Manama ', 1, 'Approved', '305d230e546ee6c3eeba4bf8c9a4ff08', '2020-08-24 05:24:40', 'admin', 0, 'admin', 0),
(44, 'Izza', '', '', '', 'Female', 'CPR2345877', '2000-07-06', 'bsc', '78787878', 'izza@gmail.com', '', 'uploads/students/202008244610image_search_1598253467956.jpg', '', 'manama', 1, 'Approved', '98b01be2d96e5eafef66db258107caf4', '2020-08-24 11:07:06', 'admin', 0, 'admin', 0),
(45, 'athul', 'baby', '', '', 'Male', '', NULL, '', '33137594', 'athul@gmail.com', '', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-24 06:07:17', 'admin', 0, 'admin', 0),
(46, 'Test', 'TESt', '', '', 'Male', '', NULL, '', '33224844', 'alaradi09@gmail.com', '', '', '', '', 1, 'Approved', '978f1897d559b90dbe328931df6f3dac', '2020-08-28 06:45:09', 'admin', 0, 'admin', 0),
(47, 'Hesham', 'Abdulaal', '', '', 'Male', '', NULL, '', '39645533', 'habdulaal@boc.bh', '', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-28 14:08:28', 'admin', 0, 'admin', 0),
(48, 'rose', 'الناشي', '', '', 'Female', '', '2000-07-15', '', '2424242424', 'rose@test.com', 'Asia/Kolkata', '', '', '', 1, 'Approved', '79ca407f4de1af3be6e8ebc85f214105', '2020-09-11 09:10:06', '', 0, '', 0),
(49, 'RIntu', 'RIntu Jose', '', '', 'Male', 'CPR5567HH767', '0000-00-00', 'Bachelor of technology', '9847661197', 'info@cafiyarestaurant.com', '', '', 'uploads/students-cv/202009015114download (2).jpg', 'Pantheerankavu\r\nKerala\r\nPantheerankave', 1, 'Approved', '133564794a1bfbfad19be33f1b1e341c', '2020-09-01 10:44:48', 'admin', 0, 'admin', 0),
(50, 'baby', 'mathew', '', '', 'Male', '22112211', '2020-09-02', '', '55445544', '', '', '', '', '', 1, 'Approved', '133564794a1bfbfad19be33f1b1e341c', '2020-09-01 13:07:19', 'admin', 0, 'admin', 0),
(51, 'Aithin', 'ep', '', '', 'Male', '', NULL, '', '10000002121212121231', 'epaithin123@gmail.com', '', '', '', '', 1, 'Approved', '3b2ded44bdfea478dbef75e31a318b19', '2020-09-10 04:42:55', 'admin', 0, 'admin', 0),
(52, 'RIntu', 'RIntu Jose', '', '', 'Male', '125447', '2020-10-06', 'Bachelor of technology', '9847661197', 'aithinep@gmail.com', '', 'uploads/students/202010065945.png', '', 'Pantheerankavu\r\nKerala\r\nPantheerankave', 1, 'Approved', '133564794a1bfbfad19be33f1b1e341c', '2020-10-06 07:15:29', 'admin', 0, 'admin', 0),
(53, 'Aithin', 'EP', '', '', 'Male', '', NULL, '', '9847661100', 'aithinep134@gmail.com', '', '', '', '', 1, 'Approved', '3b2ded44bdfea478dbef75e31a318b19', '2020-10-06 07:22:03', 'admin', 0, 'admin', 0),
(54, 'Aithin', 'EP', '', '', 'Male', '', NULL, '', '9847661100', 'aithinep134@gmail.com', '', '', '', '', 1, 'Approved', '3b2ded44bdfea478dbef75e31a318b19', '2021-03-22 08:12:05', 'admin', 0, 'admin', 0),
(55, 'testt', 'tesr', 'امتحان', 'امتحان', 'Male', '200', '2021-03-14', 'degree', '100008651', 'testh@gmail.com', 'Asia/Almaty', '', '', '', 1, 'Approved', '98b01be2d96e5eafef66db258107caf4', '2021-03-22 08:12:16', 'admin', 0, 'admin', 0),
(56, 'shaas', 'as', 'شاس', 'شاس', 'Male', '23455', '2021-03-30', 'bsc', '9873456756', 'as@gmail.com', 'Africa/Abidjan', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2021-03-22 09:37:20', 'admin', 0, 'admin', 0),
(57, 'testtt', 'test', 'شاس', 'شاس', 'Male', '1234', '2021-03-11', 'cs', '9873456750', 'as1@gmail.com', 'Africa/Abidjan', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2021-03-22 09:39:07', 'admin', 0, 'admin', 0),
(58, 'sha', 'as', 'شاس', '', 'Female', '12345', '2021-03-15', 'bsc', '9873456755', 'as11@gmail.com', 'Africa/Asmara', '', '', 'sssss', 1, 'Approved', 'fcea920f7412b5da7be0cf42b8c93759', '2021-03-22 09:44:48', 'admin', 0, 'admin', 0),
(59, 'jo', 'jo', 'جو', 'جو', 'Male', '20', '2000-02-24', 'Degree', '7878852545', 'jo@gmail.com', 'Africa/Accra', '', '', '', 1, 'Approved', 'e10adc3949ba59abbe56e057f20f883e', '2021-03-22 10:04:45', 'admin', 0, 'admin', 0),
(60, 'jua', 'jua', 'جوا', 'جوا', 'Female', '54', '1998-03-12', 'MSC', '7898564512', 'jua@gmail.com', 'Indian/Maldives', '', '', '', 1, 'Approved', '98b01be2d96e5eafef66db258107caf4', '2021-03-26 04:22:35', 'admin', 0, 'admin', 0),
(61, 'sasi', 'mal', 'ءاشخس', 'نتلاسنتؤلاه', 'Female', '123432524', '2021-03-09', '12345678', '8778986789', 'sasi@gmail.com', 'Africa/Abidjan', '', '', '', 1, 'Approved', '25d55ad283aa400af464c76d713c07ad', '2021-03-26 05:33:10', 'admin', 0, 'admin', 0);

-- --------------------------------------------------------

--
-- Table structure for table `students_certificates`
--

CREATE TABLE `students_certificates` (
  `sc_id` int(11) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `certificate` text NOT NULL,
  `title` varchar(100) NOT NULL,
  `title_arabic` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `content_arabic` text NOT NULL,
  `stu_id` bigint(20) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `added_by` enum('admin','subadmin','trainer') NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `added_trainer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students_certificates`
--

INSERT INTO `students_certificates` (`sc_id`, `course_id`, `certificate`, `title`, `title_arabic`, `content`, `content_arabic`, `stu_id`, `date`, `added_by`, `added_subadmin`, `added_trainer`) VALUES
(1, 21, 'uploads/certificates/202008241266Test .pdf', 'test11', '', '', '', 44, '2020-08-24 05:05:22', 'admin', 0, 0),
(2, 21, '', 'Test for content ', 'امتحان', 'Test content for certificate ', '', 44, '2021-03-04 00:43:56', 'admin', 0, 0),
(4, 21, '', 'Certificate for completion ', 'امتحان', ' Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available. It is also used to temporarily replace text in a process called greeking, which allows designers to consider the form of a webpage or publication, without the meaning of the text influencing the design.', '', 44, '2021-03-04 00:52:01', 'admin', 0, 0),
(5, 21, '', 'Test no2', 'امتحان', 'Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.', '', 45, '2021-03-04 04:11:49', 'admin', 0, 0),
(7, 21, '', 'Certificate for completion ', 'شهادة للإنجاز', 'has successfully completed test course for six moths. ', 'أكمل بنجاح دورة الاختبار لمدة ستة أشهر.', 45, '2021-03-12 09:40:20', 'admin', 0, 0),
(8, 21, '', 'Certificate for completion of three moths test course.', 'شهادة إتمام دورة اختبار العث الثلاث.', 'has successfully completed three moths of test course.', 'أكملت بنجاح ثلاثة عث من دورة الاختبار.', 45, '2021-03-12 09:42:29', 'admin', 0, 0),
(12, 2, '', 'HI', 'اكتملت الدورة', 'test', '', 11, '2021-03-13 06:36:38', 'admin', 0, 0),
(13, 22, '', 'Software Engineer', 'ربيبيسب', 'Software Engineer', '', 45, '2021-03-24 00:10:59', 'admin', 0, 0),
(14, 24, '', 'xyz', 'يسلسيليس', 'gfdsgsrgrhf', '', 45, '2021-03-25 03:49:27', 'admin', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `students_exams`
--

CREATE TABLE `students_exams` (
  `se_id` bigint(20) NOT NULL,
  `exam_id` bigint(20) NOT NULL,
  `stu_id` bigint(20) NOT NULL,
  `questions` int(11) NOT NULL DEFAULT 0,
  `attended` int(11) NOT NULL DEFAULT 0,
  `left_questions` int(11) NOT NULL DEFAULT 0,
  `correct_answers` int(11) NOT NULL DEFAULT 0,
  `wrong_answers` int(11) NOT NULL DEFAULT 0,
  `mark` int(11) NOT NULL DEFAULT 0,
  `total_mark` double NOT NULL,
  `percentage` double NOT NULL DEFAULT 0,
  `grade` enum('Excellent','Average','Good','Poor','') NOT NULL DEFAULT '',
  `time_taken` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(20) NOT NULL,
  `started` tinyint(1) NOT NULL DEFAULT 0,
  `completed` tinyint(1) NOT NULL DEFAULT 0,
  `starting_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `ending_time` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students_exams`
--

INSERT INTO `students_exams` (`se_id`, `exam_id`, `stu_id`, `questions`, `attended`, `left_questions`, `correct_answers`, `wrong_answers`, `mark`, `total_mark`, `percentage`, `grade`, `time_taken`, `date`, `time`, `started`, `completed`, `starting_time`, `ending_time`, `timestamp`) VALUES
(1, 36, 44, 2, 2, 0, 1, 1, 0, 20, 0, 'Poor', '00:0:32', '2020-08-24', '01:17 PM', 1, 1, '2020-08-24 05:17:10', '2020-08-24 05:17:42', '2020-08-24 10:17:42'),
(2, 37, 44, 1, 0, 1, 0, 0, 0, 10, 0, 'Poor', '00:0:8', '2020-08-24', '01:19 PM', 1, 1, '2020-08-24 05:19:27', '2020-08-24 05:19:35', '2020-08-24 10:19:35'),
(3, 38, 45, 1, 1, 0, 0, 1, -1, 1, 0, 'Poor', '00:0:7', '2020-09-02', '11:22 AM', 1, 1, '2020-09-02 03:22:47', '2020-09-02 03:22:54', '2020-09-02 08:22:54'),
(4, 39, 45, 1, 0, 1, 0, 0, 0, 1, 0, 'Poor', '00:0:5', '2020-09-02', '11:24 AM', 1, 1, '2020-09-02 03:24:19', '2020-09-02 03:24:24', '2020-09-02 08:24:24'),
(5, 40, 45, 1, 0, 1, 0, 0, 50, 1, 0, 'Poor', '00:0:4', '2020-09-02', '06:17 PM', 1, 1, '2020-09-02 10:17:08', '2020-09-02 10:17:12', '2021-03-20 10:33:14'),
(6, 42, 45, 1, 1, 0, 0, 1, -1, 1, 0, 'Poor', '00:1:6', '2020-09-04', '10:04 PM', 1, 1, '2020-09-04 14:04:36', '2020-09-04 14:05:42', '2020-09-04 19:05:42'),
(7, 41, 11, 5, 5, 0, 2, 3, 2, 10, 0, 'Poor', '00:9:20', '2020-09-05', '12:07 PM', 1, 1, '2020-09-05 04:07:57', '2020-09-05 04:17:17', '2020-09-05 06:47:17'),
(8, 44, 44, 2, 1, 1, 0, 1, -12, 23, 0, 'Poor', '00:0:36', '2020-09-05', '03:57 PM', 1, 1, '2020-09-05 07:57:46', '2020-09-05 07:58:22', '2020-09-05 10:28:22');

-- --------------------------------------------------------

--
-- Table structure for table `students_grade`
--

CREATE TABLE `students_grade` (
  `sg_id` int(11) NOT NULL,
  `stu_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `grade` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students_grade`
--

INSERT INTO `students_grade` (`sg_id`, `stu_id`, `course_id`, `grade`, `date`, `timestamp`) VALUES
(1, 45, 22, 'A+', '2021-03-24', '2021-03-24 06:23:42'),
(2, 45, 21, 'A', '2021-03-24', '2021-03-24 06:32:15'),
(3, 44, 21, 'B', '2021-03-24', '2021-03-24 06:32:22'),
(4, 45, 24, 'A+', '2021-03-25', '2021-03-25 03:50:03');

-- --------------------------------------------------------

--
-- Table structure for table `students_oraganizations`
--

CREATE TABLE `students_oraganizations` (
  `so_id` bigint(20) NOT NULL,
  `org_id` int(11) NOT NULL,
  `stu_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `students_packages`
--

CREATE TABLE `students_packages` (
  `sp_id` bigint(20) NOT NULL,
  `stu_id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `payment_id` varchar(100) NOT NULL,
  `payment_type` enum('Online','Tamkeen','By cash') NOT NULL DEFAULT 'Online',
  `amount` double NOT NULL,
  `sp_status` enum('Waiting','Approved','Rejected') NOT NULL DEFAULT 'Waiting',
  `reject_reason` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students_packages`
--

INSERT INTO `students_packages` (`sp_id`, `stu_id`, `course_id`, `date`, `payment_id`, `payment_type`, `amount`, `sp_status`, `reject_reason`, `timestamp`) VALUES
(9, 6, 16, '2020-08-18 13:47:13', '', 'Online', 500, 'Approved', '', '2020-08-18 13:47:13'),
(10, 9, 8, '2020-08-19 07:18:45', '', 'Online', 40, 'Approved', '', '2020-08-19 07:18:45'),
(11, 10, 8, '2020-08-19 07:18:46', '', 'Online', 40, 'Approved', '', '2020-08-19 07:18:46'),
(12, 11, 2, '2020-08-20 08:01:40', '', 'Online', 400, 'Approved', '', '2020-08-20 08:01:40'),
(13, 20, 8, '2020-08-20 11:19:27', '', 'Online', 40, 'Approved', '', '2020-08-20 11:19:27'),
(14, 19, 8, '2020-08-20 11:47:23', '', 'Online', 40, 'Approved', '', '2020-08-20 11:47:23'),
(15, 22, 8, '2020-08-22 23:26:25', '', 'Online', 40, 'Approved', '', '2020-08-22 23:26:25'),
(16, 37, 8, '2020-08-22 23:58:19', '', 'Online', 40, 'Approved', '', '2020-08-22 23:58:19'),
(17, 30, 8, '2020-08-23 00:01:59', '', 'Online', 40, 'Approved', '', '2020-08-23 00:01:59'),
(18, 18, 8, '2020-08-23 10:12:31', '', 'Online', 40, 'Waiting', '', '2020-08-23 10:12:31'),
(19, 34, 8, '2020-08-23 10:39:32', '', 'Online', 40, 'Waiting', '', '2020-08-23 10:39:32'),
(20, 27, 8, '2020-08-23 14:59:56', '', 'Online', 40, 'Waiting', '', '2020-08-23 14:59:56'),
(21, 41, 8, '2020-08-23 18:23:30', '', 'Online', 40, 'Waiting', '', '2020-08-23 18:23:30'),
(22, 35, 8, '2020-08-23 18:42:23', '', 'Online', 40, 'Waiting', '', '2020-08-23 18:42:23'),
(23, 42, 7, '2020-08-24 00:18:27', '', 'Online', 250, 'Approved', '', '2020-08-24 05:18:27'),
(24, 43, 8, '2020-08-24 00:24:40', '', 'Online', 40, 'Approved', '', '2020-08-24 05:24:40'),
(25, 44, 2, '2020-08-24 05:58:25', '', 'Online', 400, 'Approved', 'No seat', '2020-08-24 05:58:25'),
(26, 44, 16, '2020-08-24 07:01:53', '', 'Online', 500, 'Approved', '', '2020-08-24 07:01:53'),
(27, 44, 7, '2020-08-24 07:04:38', '', 'Online', 250, 'Approved', 'Nothing', '2020-08-24 07:04:38'),
(28, 44, 21, '2020-08-24 09:52:45', '', 'Online', 50, 'Approved', '', '2020-08-24 09:52:45'),
(29, 44, 8, '2020-08-26 10:58:59', '', 'Online', 40, 'Approved', 'rfftt', '2020-08-26 10:58:59'),
(30, 45, 2, '2020-08-27 14:52:10', '', 'Online', 400, 'Approved', '', '2020-08-27 14:52:10'),
(31, 16, 16, '2020-08-28 10:13:48', '', 'Online', 0.1, 'Waiting', '', '2020-08-28 10:13:48'),
(32, 11, 16, '2020-08-28 10:25:49', '', 'Online', 0.1, 'Approved', '', '2020-08-28 10:25:49'),
(33, 47, 16, '2020-08-28 14:11:50', '', 'Online', 0.1, 'Approved', '', '2020-08-28 14:11:50'),
(34, 11, 7, '2020-09-01 12:01:32', '', 'Online', 250, 'Waiting', '', '2020-09-01 12:01:32'),
(35, 50, 8, '2020-09-01 08:07:19', '', 'Tamkeen', 40, 'Approved', '', '2020-09-01 13:07:19'),
(36, 45, 21, '2020-09-02 15:16:01', '', 'Online', 50, 'Approved', '', '2020-09-02 15:16:01'),
(37, 45, 8, '2020-09-08 09:00:16', '', 'Online', 40, 'Waiting', '', '2020-09-08 09:00:16'),
(38, 45, 20, '2020-09-20 00:43:55', '', 'Online', 50, 'Approved', '', '2020-09-20 05:43:55'),
(39, 45, 7, '2020-09-28 08:21:48', '', 'Online', 0.1, 'Approved', '', '2020-09-28 08:21:48'),
(40, 45, 13, '2020-10-13 15:35:17', '', 'Online', 2600, 'Approved', '', '2020-10-13 15:35:17'),
(41, 45, 17, '2020-10-21 10:44:16', '', 'Online', 35, 'Approved', '', '2020-10-21 10:44:16'),
(42, 45, 15, '2021-03-22 07:00:48', '', 'Online', 300, 'Waiting', '', '2021-03-22 07:00:48'),
(43, 45, 22, '2021-03-22 07:44:40', '', 'Online', 12, 'Waiting', '', '2021-03-22 07:44:40'),
(44, 58, 21, '2021-03-22 04:44:48', '', 'Tamkeen', 2000, 'Approved', '', '2021-03-22 09:44:48'),
(45, 45, 24, '2021-03-25 08:48:20', '', 'Online', 12, 'Approved', '', '2021-03-25 08:48:20');

-- --------------------------------------------------------

--
-- Table structure for table `students_question_answers`
--

CREATE TABLE `students_question_answers` (
  `sqa_id` bigint(20) NOT NULL,
  `stu_id` int(11) NOT NULL,
  `que_id` int(11) NOT NULL,
  `ans` enum('a','b','c','d','e','') NOT NULL,
  `exam_id` bigint(20) NOT NULL,
  `se_id` bigint(20) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students_question_answers`
--

INSERT INTO `students_question_answers` (`sqa_id`, `stu_id`, `que_id`, `ans`, `exam_id`, `se_id`, `timestamp`) VALUES
(1, 44, 117, 'b', 36, 1, '2020-08-24 10:17:34'),
(2, 44, 118, 'b', 36, 1, '2020-08-24 10:17:38'),
(3, 44, 119, '', 37, 2, '2020-08-24 10:19:27'),
(4, 45, 120, 'a', 38, 3, '2020-09-02 08:22:50'),
(5, 45, 121, '', 39, 4, '2020-09-02 08:24:19'),
(6, 45, 122, '', 40, 5, '2020-09-02 15:17:08'),
(7, 45, 128, 'b', 42, 6, '2020-09-04 19:05:34'),
(8, 11, 123, 'a', 41, 7, '2020-09-05 06:46:49'),
(9, 11, 124, 'a', 41, 7, '2020-09-05 06:46:54'),
(10, 11, 125, 'b', 41, 7, '2020-09-05 06:46:57'),
(11, 11, 126, 'a', 41, 7, '2020-09-05 06:47:01'),
(12, 11, 127, 'b', 41, 7, '2020-09-05 06:47:08'),
(13, 44, 132, 'a', 44, 8, '2020-09-05 10:28:16'),
(14, 44, 133, '', 44, 8, '2020-09-05 10:27:46');

-- --------------------------------------------------------

--
-- Table structure for table `students_sports`
--

CREATE TABLE `students_sports` (
  `ss_id` bigint(20) NOT NULL,
  `sport_id` int(11) NOT NULL,
  `stu_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subadmin_previleges`
--

CREATE TABLE `subadmin_previleges` (
  `sp_id` int(11) NOT NULL,
  `subadmin_id` int(11) NOT NULL,
  `m_id` int(11) NOT NULL,
  `view` enum('1','0') NOT NULL,
  `add` enum('1','0') NOT NULL,
  `edit` enum('1','0') NOT NULL,
  `block` enum('1','0') NOT NULL,
  `delete` enum('1','0') NOT NULL,
  `all` enum('1','0','2') NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subadmin_previleges`
--

INSERT INTO `subadmin_previleges` (`sp_id`, `subadmin_id`, `m_id`, `view`, `add`, `edit`, `block`, `delete`, `all`, `timestamp`) VALUES
(1, 1, 1, '1', '1', '1', '1', '1', '1', '2020-06-26 18:30:00'),
(2, 1, 2, '1', '1', '1', '1', '1', '1', '2020-06-26 18:30:00'),
(3, 1, 3, '1', '1', '1', '1', '1', '1', '2020-06-27 09:59:04'),
(4, 1, 4, '0', '0', '0', '0', '0', '0', '2020-06-26 18:30:00'),
(5, 1, 5, '0', '0', '0', '0', '0', '0', '2020-06-26 18:30:00'),
(6, 1, 6, '0', '0', '0', '0', '0', '0', '2020-06-26 18:30:00'),
(8, 1, 22, '1', '1', '1', '1', '1', '1', '2020-06-26 18:30:00'),
(9, 1, 23, '1', '1', '1', '1', '1', '1', '2020-06-26 18:30:00'),
(10, 1, 24, '0', '0', '0', '0', '0', '0', '2020-06-26 18:30:00'),
(11, 1, 25, '0', '0', '0', '0', '0', '0', '2020-06-26 18:30:00'),
(12, 1, 26, '0', '0', '0', '0', '0', '0', '2020-06-26 18:30:00'),
(13, 1, 27, '0', '0', '0', '0', '0', '0', '2020-06-26 18:30:00'),
(14, 1, 28, '0', '0', '0', '0', '0', '0', '2020-06-26 18:30:00'),
(15, 8, 1, '0', '0', '0', '0', '0', '0', '2020-06-27 10:06:25'),
(16, 8, 2, '1', '0', '0', '0', '0', '2', '2020-06-27 10:06:25'),
(17, 8, 3, '0', '0', '0', '0', '0', '0', '2020-06-27 10:06:25'),
(18, 8, 4, '1', '1', '1', '1', '1', '1', '2020-06-27 10:06:25'),
(19, 8, 5, '0', '0', '0', '0', '0', '0', '2020-06-27 10:06:25'),
(20, 8, 6, '0', '0', '0', '0', '0', '0', '2020-06-27 10:06:25'),
(21, 8, 22, '0', '0', '0', '0', '0', '0', '2020-06-27 10:06:25'),
(22, 8, 23, '0', '0', '0', '0', '0', '0', '2020-06-27 10:06:25'),
(23, 8, 24, '0', '0', '0', '0', '0', '0', '2020-06-27 10:06:25'),
(24, 8, 25, '0', '0', '0', '0', '0', '0', '2020-06-27 10:06:25'),
(25, 8, 26, '0', '0', '0', '0', '0', '0', '2020-06-27 10:06:25'),
(26, 8, 27, '0', '0', '0', '0', '0', '0', '2020-06-27 10:06:25'),
(27, 8, 28, '0', '0', '0', '0', '0', '0', '2020-06-27 10:06:25'),
(28, 8, 29, '0', '0', '0', '0', '0', '0', '2020-06-27 10:06:25'),
(29, 8, 30, '0', '0', '0', '0', '0', '0', '2020-06-27 10:06:25'),
(30, 8, 31, '0', '0', '0', '0', '0', '0', '2020-06-27 10:06:25'),
(31, 1, 29, '1', '1', '1', '1', '1', '1', '2020-06-26 18:30:00'),
(32, 1, 30, '0', '0', '0', '0', '0', '0', '2020-06-26 18:30:00'),
(33, 1, 31, '0', '0', '0', '0', '0', '0', '2020-06-26 18:30:00'),
(34, 1, 32, '0', '0', '0', '0', '0', '0', '2020-06-26 18:30:00'),
(35, 8, 32, '0', '0', '0', '0', '0', '0', '2020-06-26 18:30:00'),
(36, 9, 1, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(37, 9, 2, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(38, 9, 3, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(39, 9, 4, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(40, 9, 5, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(41, 9, 6, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(42, 9, 22, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(43, 9, 23, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(44, 9, 24, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(45, 9, 25, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(46, 9, 26, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(47, 9, 27, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(48, 9, 28, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(49, 9, 29, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(50, 9, 30, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(51, 9, 31, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(52, 9, 32, '0', '0', '0', '0', '0', '0', '2020-06-28 08:13:31'),
(53, 10, 1, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(54, 10, 2, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(55, 10, 3, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(56, 10, 4, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(57, 10, 5, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(58, 10, 6, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(59, 10, 22, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(60, 10, 23, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(61, 10, 24, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(62, 10, 25, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(63, 10, 26, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(64, 10, 27, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(65, 10, 28, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(66, 10, 29, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(67, 10, 30, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(68, 10, 31, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(69, 10, 32, '1', '1', '1', '1', '1', '1', '2020-06-30 03:19:01'),
(70, 1, 33, '0', '0', '0', '0', '0', '0', '2020-07-04 04:24:45'),
(71, 8, 33, '0', '0', '0', '0', '0', '0', '2020-07-04 04:24:45'),
(72, 9, 33, '0', '0', '0', '0', '0', '0', '2020-07-04 04:25:29'),
(73, 10, 33, '1', '1', '1', '1', '1', '1', '2020-07-04 04:25:29'),
(74, 1, 34, '0', '0', '0', '0', '0', '0', '2020-07-04 07:43:49'),
(75, 8, 34, '1', '0', '0', '0', '0', '0', '2020-07-04 07:43:49'),
(76, 9, 34, '0', '0', '0', '0', '0', '0', '2020-07-04 07:45:13'),
(77, 10, 34, '1', '1', '1', '1', '1', '1', '2020-07-04 07:45:13'),
(78, 1, 35, '1', '1', '1', '1', '1', '1', '2020-07-13 02:35:15'),
(79, 8, 35, '0', '0', '0', '0', '0', '0', '2020-07-13 02:35:15'),
(80, 9, 35, '0', '0', '0', '0', '0', '0', '2020-07-13 02:38:46'),
(81, 10, 35, '1', '1', '1', '1', '1', '1', '2020-07-13 02:38:46'),
(82, 11, 1, '0', '0', '0', '0', '0', '0', '2020-07-20 04:37:14'),
(83, 11, 2, '0', '0', '0', '0', '0', '0', '2020-07-20 04:37:14'),
(84, 11, 3, '1', '1', '1', '1', '1', '1', '2020-07-20 04:37:14'),
(85, 11, 4, '1', '1', '1', '1', '1', '1', '2020-07-20 04:37:14'),
(86, 11, 5, '1', '1', '1', '1', '1', '1', '2020-07-20 04:37:14'),
(87, 11, 6, '1', '1', '1', '1', '1', '1', '2020-07-20 04:37:14'),
(88, 11, 22, '0', '0', '0', '0', '0', '0', '2020-07-20 04:37:14'),
(89, 11, 23, '0', '0', '0', '0', '0', '0', '2020-07-20 04:37:14'),
(90, 11, 24, '0', '0', '0', '0', '0', '0', '2020-07-20 04:37:14'),
(91, 11, 25, '1', '1', '1', '1', '1', '1', '2020-07-20 04:37:14'),
(92, 11, 26, '1', '1', '1', '1', '1', '1', '2020-07-20 04:37:14'),
(93, 11, 27, '1', '1', '1', '1', '1', '1', '2020-07-20 04:37:14'),
(94, 11, 28, '0', '0', '0', '0', '0', '0', '2020-07-20 04:37:14'),
(95, 11, 29, '0', '0', '0', '0', '0', '0', '2020-07-20 04:37:14'),
(96, 11, 30, '0', '0', '0', '0', '0', '0', '2020-07-20 04:37:14'),
(97, 11, 31, '0', '0', '0', '0', '0', '0', '2020-07-20 04:37:14'),
(98, 11, 32, '1', '1', '1', '1', '1', '1', '2020-07-20 04:37:14'),
(99, 11, 33, '1', '1', '1', '1', '1', '1', '2020-07-20 04:37:14'),
(100, 11, 34, '0', '0', '0', '0', '0', '0', '2020-07-20 04:37:14'),
(101, 11, 35, '0', '0', '0', '0', '0', '0', '2020-07-20 04:37:14'),
(102, 12, 1, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(103, 12, 2, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(104, 12, 3, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(105, 12, 4, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(106, 12, 5, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(107, 12, 6, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(108, 12, 22, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(109, 12, 23, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(110, 12, 24, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(111, 12, 25, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(112, 12, 26, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(113, 12, 27, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(114, 12, 28, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(115, 12, 29, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(116, 12, 30, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(117, 12, 31, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(118, 12, 32, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(119, 12, 33, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(120, 12, 34, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(121, 12, 35, '1', '1', '1', '1', '1', '1', '2020-07-21 02:59:57'),
(122, 13, 1, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(123, 13, 2, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(124, 13, 3, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(125, 13, 4, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(126, 13, 5, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(127, 13, 6, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(128, 13, 22, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(129, 13, 23, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(130, 13, 24, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(131, 13, 25, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(132, 13, 26, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(133, 13, 27, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(134, 13, 28, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(135, 13, 29, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(136, 13, 30, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(137, 13, 31, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(138, 13, 32, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(139, 13, 33, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(140, 13, 34, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(141, 13, 35, '1', '1', '1', '1', '1', '1', '2020-07-22 02:36:49'),
(142, 1, 36, '1', '0', '1', '1', '1', '2', '2020-07-29 00:51:58'),
(143, 8, 36, '1', '1', '1', '1', '1', '1', '2020-07-29 00:51:58'),
(144, 9, 36, '1', '1', '1', '1', '1', '1', '2020-07-29 00:53:12'),
(145, 10, 36, '1', '1', '1', '1', '1', '1', '2020-07-29 00:53:12'),
(146, 11, 36, '1', '1', '1', '1', '1', '1', '2020-07-29 00:53:56'),
(147, 12, 36, '1', '1', '1', '1', '1', '1', '2020-07-29 00:53:56'),
(148, 13, 36, '1', '1', '1', '1', '1', '1', '2020-07-29 00:54:37'),
(150, 1, 37, '1', '1', '0', '0', '1', '2', '2020-07-29 00:57:47'),
(151, 8, 37, '1', '1', '1', '1', '1', '1', '2020-07-29 00:57:47'),
(152, 9, 37, '1', '1', '1', '1', '1', '1', '2020-07-29 00:58:32'),
(153, 10, 37, '1', '1', '1', '1', '1', '1', '2020-07-29 00:58:32'),
(154, 11, 37, '1', '1', '1', '1', '1', '1', '2020-07-29 00:59:17'),
(155, 12, 37, '1', '1', '1', '1', '1', '1', '2020-07-29 00:59:17'),
(156, 13, 37, '1', '1', '1', '1', '1', '1', '2020-07-29 01:00:29'),
(157, 14, 1, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(158, 14, 2, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(159, 14, 3, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(160, 14, 4, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(161, 14, 5, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(162, 14, 6, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(163, 14, 22, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(164, 14, 23, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(165, 14, 24, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(166, 14, 25, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(167, 14, 26, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(168, 14, 27, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(169, 14, 28, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(170, 14, 29, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(171, 14, 30, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(172, 14, 31, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(173, 14, 32, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(174, 14, 33, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(175, 14, 34, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(176, 14, 35, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(177, 14, 36, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(178, 14, 37, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(179, 15, 1, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(180, 15, 2, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(181, 15, 3, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(182, 15, 4, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(183, 15, 5, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(184, 15, 6, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(185, 15, 22, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(186, 15, 23, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(187, 15, 24, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(188, 15, 25, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(189, 15, 26, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(190, 15, 27, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(191, 15, 28, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(192, 15, 29, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(193, 15, 30, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(194, 15, 31, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(195, 15, 32, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(196, 15, 33, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(197, 15, 34, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(198, 15, 35, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(199, 15, 36, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(200, 15, 37, '1', '1', '1', '1', '1', '1', '2020-08-17 02:31:02'),
(201, 16, 1, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(202, 16, 2, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(203, 16, 3, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(204, 16, 4, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(205, 16, 5, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(206, 16, 6, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(207, 16, 22, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(208, 16, 23, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(209, 16, 24, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(210, 16, 25, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(211, 16, 26, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(212, 16, 27, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(213, 16, 28, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(214, 16, 29, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(215, 16, 30, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(216, 16, 31, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(217, 16, 32, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(218, 16, 33, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(219, 16, 34, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(220, 16, 35, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(221, 16, 36, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(222, 16, 37, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:10'),
(223, 17, 1, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(224, 17, 2, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(225, 17, 3, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(226, 17, 4, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(227, 17, 5, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(228, 17, 6, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(229, 17, 22, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(230, 17, 23, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(231, 17, 24, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(232, 17, 25, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(233, 17, 26, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(234, 17, 27, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(235, 17, 28, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(236, 17, 29, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(237, 17, 30, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(238, 17, 31, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(239, 17, 32, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(240, 17, 33, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(241, 17, 34, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(242, 17, 35, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(243, 17, 36, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(244, 17, 37, '1', '1', '1', '1', '1', '1', '2020-08-17 02:47:47'),
(245, 18, 1, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(246, 18, 2, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(247, 18, 3, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(248, 18, 4, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(249, 18, 5, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(250, 18, 6, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(251, 18, 22, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(252, 18, 23, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(253, 18, 24, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(254, 18, 25, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(255, 18, 26, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(256, 18, 27, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(257, 18, 28, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(258, 18, 29, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(259, 18, 30, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(260, 18, 31, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(261, 18, 32, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(262, 18, 33, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(263, 18, 34, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(264, 18, 35, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(265, 18, 36, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(266, 18, 37, '1', '1', '1', '1', '1', '1', '2020-08-17 02:48:28'),
(267, 19, 1, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(268, 19, 2, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(269, 19, 3, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(270, 19, 4, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(271, 19, 5, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(272, 19, 6, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(273, 19, 22, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(274, 19, 23, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(275, 19, 24, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(276, 19, 25, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(277, 19, 26, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(278, 19, 27, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(279, 19, 28, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(280, 19, 29, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(281, 19, 30, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(282, 19, 31, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(283, 19, 32, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(284, 19, 33, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(285, 19, 34, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(286, 19, 35, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(287, 19, 36, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02'),
(288, 19, 37, '1', '1', '1', '1', '1', '1', '2021-03-24 04:43:02');

-- --------------------------------------------------------

--
-- Table structure for table `sub_admin`
--

CREATE TABLE `sub_admin` (
  `sa_id` int(11) NOT NULL,
  `name_english` varchar(100) NOT NULL,
  `name_arabic` varchar(200) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp(),
  `cpr` varchar(200) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `status` enum('1','0') NOT NULL,
  `address` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_admin`
--

INSERT INTO `sub_admin` (`sa_id`, `name_english`, `name_arabic`, `phone`, `email`, `password`, `timestamp`, `cpr`, `photo`, `status`, `address`) VALUES
(1, 'SUB ADMIN', 'المشرف الفرعي', 34678923, 'subadmin@olympic.com', '6b44ab687a723672fef3e6753e561b63', '2020-06-24 18:30:00', '123', 'uploads/subadmin/202009015875user.png', '1', ''),
(8, 'test', '', 89073245, 'test@gmail.com', 'fcea920f7412b5da7be0cf42b8c93759', '2020-06-27 10:06:25', '', '', '1', ''),
(9, 'athull', '', 33224433, 'athul@gmail.com', 'fcea920f7412b5da7be0cf42b8c93759', '2020-06-28 10:43:31', '232132', '', '1', 'manama'),
(10, 'TESTT', '', 12345678, 'testttttttt@gmail.com', '1d678ec32a1411f6d05b4ce3ba9ab46a', '2020-06-30 05:49:01', '123', 'uploads/subadmin/202006308617image_search_1593149355299.jpg', '1', 'Manama'),
(11, 'Adel Alasfoor', 'شش', 33137633, 'adelal@gmail.com', 'fcea920f7412b5da7be0cf42b8c93759', '2020-07-20 09:37:14', '', '', '1', ''),
(12, 'boa', 'صض', 33556677, 'boa@boc.bh', '3370cdfdd273382612d85cb5991a2384', '2020-07-21 07:59:57', '', '', '1', ''),
(13, 'zainab', '', 22445566, 'zalmosawi@boc.bh', '64f1408b61a3db62a78ee3477d3686c1', '2020-07-22 07:36:49', '', '', '1', ''),
(14, 'Mahmood H. Yateem ', '', 44556677, 'Myateem@BOC.BH', '133564794a1bfbfad19be33f1b1e341c', '2020-08-17 07:31:02', '', '', '1', ''),
(16, 'Marwa Y. Ahmed', '', 99775544, 'mamatar@BOC.BH', '133564794a1bfbfad19be33f1b1e341c', '2020-08-17 07:47:10', '', '', '1', ''),
(17, 'Amani J. Mansoor', '', 44772255, 'amansoor@BOC.BH', '133564794a1bfbfad19be33f1b1e341c', '2020-08-17 07:47:47', '', '', '1', ''),
(18, 'Dr. Nabeel T. Alshehab', '', 7766334455, 'Nalshehab@BOC.BH', '133564794a1bfbfad19be33f1b1e341c', '2020-08-17 07:48:28', '', '', '1', ''),
(19, 'Gikh', 'شريده', 5751212121, 'gikh@gmail.com', '133564794a1bfbfad19be33f1b1e341c', '2021-03-24 09:43:02', '54855', 'uploads/subadmin/202103247614image_search_1616577837433.jpg', '1', 'Bahrain');

-- --------------------------------------------------------

--
-- Table structure for table `tap_payment`
--

CREATE TABLE `tap_payment` (
  `payment_id` int(11) NOT NULL,
  `payment` text NOT NULL,
  `header` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tap_payment`
--

INSERT INTO `tap_payment` (`payment_id`, `payment`, `header`) VALUES
(1, '{\r\n  \"id\": \"chg_TS034520201250x3NO2708424\",\r\n  \"object\": \"charge\",\r\n  \"live_mode\": false,\r\n  \"api_version\": \"V2\",\r\n  \"method\": \"POST\",\r\n  \"status\": \"DECLINED\",\r\n  \"amount\": 1.000,\r\n  \"currency\": \"BHD\",\r\n  \"threeDSecure\": true,\r\n  \"card_threeDSecure\": false,\r\n  \"save_card\": false,\r\n  \"merchant_id\": \"\",\r\n  \"product\": \"\",\r\n  \"statement_descriptor\": \"Sample\",\r\n  \"description\": \"Test Description\",\r\n  \"metadata\": {},\r\n  \"transaction\": {\r\n    \"timezone\": \"UTC+03:00\",\r\n    \"created\": \"1598532645432\",\r\n    \"expiry\": {\r\n      \"period\": 30,\r\n      \"type\": \"MINUTE\"\r\n    },\r\n    \"asynchronous\": false,\r\n    \"amount\": 1.000,\r\n    \"currency\": \"BHD\"\r\n  },\r\n  \"reference\": {\r\n    \"track\": \"tck_TS074620201250Ly942708047\",\r\n    \"payment\": \"4627201250080475984\",\r\n    \"gateway\": \"00\",\r\n    \"acquirer\": \"024009311685\",\r\n    \"transaction\": \"txn_0001\",\r\n    \"order\": \"ord_0001\"\r\n  },\r\n  \"response\": {\r\n    \"code\": \"507\",\r\n    \"message\": \"Declined, Card Issuer\"\r\n  },\r\n  \"security\": {\r\n    \"threeDSecure\": {\r\n      \"id\": \"3ds_TS034520201250h4FL2708432\",\r\n      \"status\": \"Y\"\r\n    }\r\n  },\r\n  \"acquirer\": {\r\n    \"response\": {\r\n      \"code\": \"14\",\r\n      \"message\": \"Invalid Card Number (No such Number)\"\r\n    }\r\n  },\r\n  \"card\": {\r\n    \"object\": \"card\",\r\n    \"first_six\": \"459115\",\r\n    \"last_four\": \"5623\"\r\n  },\r\n  \"receipt\": {\r\n    \"id\": \"204627201250081049\",\r\n    \"email\": false,\r\n    \"sms\": true\r\n  },\r\n  \"customer\": {\r\n    \"first_name\": \"First Name\",\r\n    \"middle_name\": \"Middle Name\",\r\n    \"last_name\": \"Last Name\",\r\n    \"email\": \"epaithin@gmail.com\",\r\n    \"phone\": {\r\n      \"country_code\": \"973\",\r\n      \"number\": \"121212121212\"\r\n    }\r\n  },\r\n  \"source\": {\r\n    \"object\": \"token\",\r\n    \"type\": \"CARD_NOT_PRESENT\",\r\n    \"payment_type\": \"DEBIT\",\r\n    \"payment_method\": \"VISA\",\r\n    \"channel\": \"INTERNET\",\r\n    \"id\": \"tok_C6ac61311012pUEi527764\"\r\n  },\r\n  \"redirect\": {\r\n    \"status\": \"PENDING\",\r\n    \"url\": \"https://ourworks.co.in/olympic/student/test_success?token=5f478038f3c95200e7e453bd&mode=popup\"\r\n  },\r\n  \"post\": {\r\n    \"attempt\": 1,\r\n    \"status\": \"PENDING\",\r\n    \"url\": \"https://ourworks.co.in/olympic/payment/postURL\"\r\n  }\r\n}', ''),
(2, '{\r\n  \"id\": \"chg_TS010420201320l4Z62708033\",\r\n  \"object\": \"charge\",\r\n  \"live_mode\": false,\r\n  \"api_version\": \"V2\",\r\n  \"method\": \"POST\",\r\n  \"status\": \"DECLINED\",\r\n  \"amount\": 1.000,\r\n  \"currency\": \"BHD\",\r\n  \"threeDSecure\": true,\r\n  \"card_threeDSecure\": false,\r\n  \"save_card\": false,\r\n  \"merchant_id\": \"\",\r\n  \"product\": \"\",\r\n  \"statement_descriptor\": \"Sample\",\r\n  \"description\": \"Test Description\",\r\n  \"metadata\": {},\r\n  \"transaction\": {\r\n    \"timezone\": \"UTC+03:00\",\r\n    \"created\": \"1598534404048\",\r\n    \"expiry\": {\r\n      \"period\": 30,\r\n      \"type\": \"MINUTE\"\r\n    },\r\n    \"asynchronous\": false,\r\n    \"amount\": 1.000,\r\n    \"currency\": \"BHD\"\r\n  },\r\n  \"reference\": {\r\n    \"track\": \"tck_TS060420201320n4MK2708671\",\r\n    \"payment\": \"4727201320086714240\",\r\n    \"gateway\": \"00\",\r\n    \"acquirer\": \"024010307438\",\r\n    \"transaction\": \"txn_0001\",\r\n    \"order\": \"ord_0001\"\r\n  },\r\n  \"response\": {\r\n    \"code\": \"507\",\r\n    \"message\": \"Declined, Card Issuer\"\r\n  },\r\n  \"security\": {\r\n    \"threeDSecure\": {\r\n      \"id\": \"3ds_TS020420201320g6L42708048\",\r\n      \"status\": \"Y\"\r\n    }\r\n  },\r\n  \"acquirer\": {\r\n    \"response\": {\r\n      \"code\": \"14\",\r\n      \"message\": \"Invalid Card Number (No such Number)\"\r\n    }\r\n  },\r\n  \"card\": {\r\n    \"object\": \"card\",\r\n    \"first_six\": \"459115\",\r\n    \"last_four\": \"5623\"\r\n  },\r\n  \"receipt\": {\r\n    \"id\": \"204027201320087490\",\r\n    \"email\": false,\r\n    \"sms\": true\r\n  },\r\n  \"customer\": {\r\n    \"first_name\": \"First Name\",\r\n    \"middle_name\": \"Middle Name\",\r\n    \"last_name\": \"Last Name\",\r\n    \"email\": \"epaithin@gmail.com\",\r\n    \"phone\": {\r\n      \"country_code\": \"973\",\r\n      \"number\": \"121212121212\"\r\n    }\r\n  },\r\n  \"source\": {\r\n    \"object\": \"token\",\r\n    \"type\": \"CARD_NOT_PRESENT\",\r\n    \"payment_type\": \"DEBIT\",\r\n    \"payment_method\": \"VISA\",\r\n    \"channel\": \"INTERNET\",\r\n    \"id\": \"tok_CPdJV1311012ofqu527737\"\r\n  },\r\n  \"redirect\": {\r\n    \"status\": \"PENDING\",\r\n    \"url\": \"https://ourworks.co.in/olympic/student/test_success?token=5f47881af3c95200e7e45950&mode=popup\"\r\n  },\r\n  \"post\": {\r\n    \"attempt\": 1,\r\n    \"status\": \"PENDING\",\r\n    \"url\": \"https://ourworks.co.in/olympic/payment/postURL\"\r\n  }\r\n}', '{\"Content-Type\":\"application\\/json\",\"hashstring\":\"56196b768e205d82cc1f347d8c5335a3082f0726409c20e2b067a3b0914f13c5\",\"Host\":\"ourworks.co.in\",\"Content-Length\":\"2120\",\"Expect\":\"100-continue\",\"X-HTTPS\":\"1\"}'),
(3, '{\r\n  \"id\": \"chg_TS033220201441s6FL2708487\",\r\n  \"object\": \"charge\",\r\n  \"live_mode\": false,\r\n  \"api_version\": \"V2\",\r\n  \"method\": \"POST\",\r\n  \"status\": \"DECLINED\",\r\n  \"amount\": 1.000,\r\n  \"currency\": \"BHD\",\r\n  \"threeDSecure\": true,\r\n  \"card_threeDSecure\": false,\r\n  \"save_card\": false,\r\n  \"merchant_id\": \"\",\r\n  \"product\": \"\",\r\n  \"statement_descriptor\": \"Sample\",\r\n  \"description\": \"Test Description\",\r\n  \"metadata\": {},\r\n  \"transaction\": {\r\n    \"timezone\": \"UTC+03:00\",\r\n    \"created\": \"1598539292503\",\r\n    \"expiry\": {\r\n      \"period\": 30,\r\n      \"type\": \"MINUTE\"\r\n    },\r\n    \"asynchronous\": false,\r\n    \"amount\": 1.000,\r\n    \"currency\": \"BHD\"\r\n  },\r\n  \"reference\": {\r\n    \"track\": \"tck_TS043220201441t9O42708772\",\r\n    \"payment\": \"3227201441087728645\",\r\n    \"gateway\": \"00\",\r\n    \"acquirer\": \"024011315905\",\r\n    \"transaction\": \"txn_0001\",\r\n    \"order\": \"ord_0001\"\r\n  },\r\n  \"response\": {\r\n    \"code\": \"507\",\r\n    \"message\": \"Declined, Card Issuer\"\r\n  },\r\n  \"security\": {\r\n    \"threeDSecure\": {\r\n      \"id\": \"3ds_TS013220201441l6B42708503\",\r\n      \"status\": \"Y\"\r\n    }\r\n  },\r\n  \"acquirer\": {\r\n    \"response\": {\r\n      \"code\": \"14\",\r\n      \"message\": \"Invalid Card Number (No such Number)\"\r\n    }\r\n  },\r\n  \"card\": {\r\n    \"object\": \"card\",\r\n    \"first_six\": \"459115\",\r\n    \"last_four\": \"5623\"\r\n  },\r\n  \"receipt\": {\r\n    \"id\": \"203227201441081879\",\r\n    \"email\": false,\r\n    \"sms\": true\r\n  },\r\n  \"customer\": {\r\n    \"first_name\": \"First Name\",\r\n    \"middle_name\": \"Middle Name\",\r\n    \"last_name\": \"Last Name\",\r\n    \"email\": \"epaithin@gmail.com\",\r\n    \"phone\": {\r\n      \"country_code\": \"973\",\r\n      \"number\": \"121212121212\"\r\n    }\r\n  },\r\n  \"source\": {\r\n    \"object\": \"token\",\r\n    \"type\": \"CARD_NOT_PRESENT\",\r\n    \"payment_type\": \"DEBIT\",\r\n    \"payment_method\": \"VISA\",\r\n    \"channel\": \"INTERNET\",\r\n    \"id\": \"tok_CYuPG13110129br4527615\"\r\n  },\r\n  \"redirect\": {\r\n    \"status\": \"PENDING\",\r\n    \"url\": \"https://ourworks.co.in/olympic/student/test_success?token=5f479b3028ff340100876589&mode=popup\"\r\n  },\r\n  \"post\": {\r\n    \"attempt\": 1,\r\n    \"status\": \"PENDING\",\r\n    \"url\": \"https://ourworks.co.in/olympic/payment/postURL\"\r\n  }\r\n}', '{\"Content-Type\":\"application\\/json\",\"hashstring\":\"6e4b4ca26d6eb42d26b7291058b2275140912646d226da8f84929aade57e8f1a\",\"Host\":\"ourworks.co.in\",\"Content-Length\":\"2120\",\"Expect\":\"100-continue\",\"Connection\":\"Keep-Alive\",\"X-HTTPS\":\"1\"}');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `team_id` int(11) NOT NULL,
  `team_first` varchar(100) NOT NULL,
  `team_second` varchar(100) NOT NULL,
  `team_designation` varchar(100) NOT NULL,
  `first_arabic` varchar(200) NOT NULL,
  `second_arabic` varchar(200) NOT NULL,
  `designation_arabic` varchar(200) NOT NULL,
  `team_image` text NOT NULL,
  `team_status` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`team_id`, `team_first`, `team_second`, `team_designation`, `first_arabic`, `second_arabic`, `designation_arabic`, `team_image`, `team_status`, `timestamp`) VALUES
(5, 'zainab', '', 'programer', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'uploads/team/202008099860.png', 1, '2021-03-22 08:23:02'),
(7, 'Zaimal', 'za', 'bahrain', 'حسين', 'حسين', 'حسين', 'uploads/team/202103247709.png', 0, '2021-03-24 09:45:02');

-- --------------------------------------------------------

--
-- Table structure for table `trainers`
--

CREATE TABLE `trainers` (
  `trainer_id` int(11) NOT NULL,
  `trainer_name_english` varchar(200) NOT NULL,
  `trainer_name_arabic` varchar(200) NOT NULL,
  `arabic_first` varchar(200) NOT NULL,
  `arabic_second` varchar(200) NOT NULL,
  `trainer_gender` enum('Male','Female','Other') NOT NULL DEFAULT 'Male',
  `trainer_cpr` varchar(100) NOT NULL,
  `trainer_mobile` varchar(20) NOT NULL,
  `trainer_email` varchar(200) NOT NULL,
  `trainer_education` varchar(200) NOT NULL,
  `trainer_experience` varchar(100) NOT NULL,
  `education_arabic` varchar(200) NOT NULL,
  `experience_arabic` varchar(200) NOT NULL,
  `trainer_contact` text NOT NULL,
  `trainer_image` varchar(200) NOT NULL,
  `trainer_status` tinyint(1) NOT NULL DEFAULT 1,
  `trainer_password` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `added_by` enum('admin','subadmin') NOT NULL,
  `added_subadmin` int(11) NOT NULL,
  `edited_by` enum('admin','subadmin') NOT NULL,
  `edited_subadmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trainers`
--

INSERT INTO `trainers` (`trainer_id`, `trainer_name_english`, `trainer_name_arabic`, `arabic_first`, `arabic_second`, `trainer_gender`, `trainer_cpr`, `trainer_mobile`, `trainer_email`, `trainer_education`, `trainer_experience`, `education_arabic`, `experience_arabic`, `trainer_contact`, `trainer_image`, `trainer_status`, `trainer_password`, `timestamp`, `added_by`, `added_subadmin`, `edited_by`, `edited_subadmin`) VALUES
(8, 'Ali', 'Hassan', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '630028826', '33875454', 'ali_alanzoor@hotmail.com', 'Diploma in Physiical Education', '25', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/202007136216Untitled-1.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'admin', 0, 'subadmin', 12),
(9, 'Abdulla ', 'Alsdakil', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '710303815', '39635393', '', 'Bachelor in Physiical Education', '20', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'HOUSE:116 - ROAD 2116 - BLOAK:721 BAHRAIN- JIDALI', 'uploads/trainers/20200713256176545.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'admin', 0, 'subadmin', 12),
(13, 'Adel', 'Alasfoor', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '', '33255456', 'adelalasfoor@hotmail.com', 'Bachelor in Physiical Education', '20', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/20200720445220200713428064563.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'admin', 0, 'subadmin', 12),
(14, 'mohseen Ali Abdulla', 'mohseen  ', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '800803728', '39044443', 'm.80.n@hotmail.com', 'Bachelor in Physiical Education', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/202007253975643.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'admin', 0),
(15, 'Dr. Istvan Balyi', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '', '00000000', '', ' Professor ', 'advisor on Long-Term Athlete Development and high performance planning in 22 different countries  in', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/2020072513196344.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'subadmin', 12),
(16, 'DR Nabeel Taha hussain Al Shehab', 'Nabeel', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '600120201', '39522995', 'nalshehab@boc.bh', 'PHD', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/20200725451964333.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'subadmin', 12),
(17, 'Andre Lachance:', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '0000000', '32185114', 'aasirois@coach.ca', ' Professor ', '• Long-Term Athlete Development • Competition Review • Athlete Development Matrix • Branding • Coach', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Canada', 'uploads/trainers/20200725473885345.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'subadmin', 12),
(18, 'Dr. Stephen Norris', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '0000000', '00000000', '', ' Professor ', '30', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/202007258492563.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'subadmin', 12),
(19, 'Dr. Don Clark, Ph.D.', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '0000000', '00000000', '', ' Professor ', '• Olympic Games • Pan American Games • Commonwealth Games • World University Games • Canada Games • ', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/202007253810653.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'subadmin', 12),
(20, 'Jason Reindl', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '', '00000000', '', ' Professor ', '25<br><br><br><br>', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/202007256942121211.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'subadmin', 12),
(21, 'Dr. Nicolas Berryman', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '', '00000000', '', ' Professor ', '<br><br><br><br>', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/2020072632553243.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'admin', 0),
(22, 'Gérard Lauzière', 'STEEV NORRIS', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '', '00000000', '', '<br><br>', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/202007268963Untitled-3233.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'admin', 0),
(23, 'Ahmed Hafhed Ali', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '', '39631116', 'ainge13@hotmail.com', '', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/202007268303202007257241.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'admin', 0),
(24, ' hassan Ahmed', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '', '33330238', '', '', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/202007264697Untitled-289.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'admin', 0),
(25, 'Ahmed abdulqafar', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '', '37775353', '', '', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/202007261028Untitled-245.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'admin', 0),
(26, 'Zanab Anwer', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Female', '', '36200402', '', '', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/202007267487Untitled-24341.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'admin', 0),
(27, 'Mahmood Alshak', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '', '39459938', '', '<br>', '<br>', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/202007268361Untitled-212.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'admin', 0),
(28, 'Maryaim Aldhain', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Female', '', '38838680', 'maryamaldhain@live.co.uk', 'PHD', 'Present Bahrain Football Association', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'V I LLA 1 2 6, B L O C K 8 09 ,\r\nD O H A AVENUE, ROAD 13, P O B O X 3 2 7 7 8\r\nI S A T O W N', 'uploads/trainers/202007268987Untitled-443.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'subadmin', 12),
(29, 'Mahmood fakroo', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '', '39660987', '', '<br>', '<br>', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/202007266375Untitled-211.jpg', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'subadmin', 12, 'admin', 0),
(33, 'Salah Sayyab ', 'Ahmad Almayal ', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Male', '270020200093', '000000000', 'salmayyal@gmail.com', '', 'Professor ', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/202008097686.png', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-20 11:01:37', 'admin', 0, 'admin', 0),
(34, 'Maryam Mardana', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Female', '', '36817477', 'MMardana@BOC.BH', '', '', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/202008093051.png', 1, 'e10adc3949ba59abbe56e057f20f883e', '2020-08-22 10:48:54', 'admin', 0, 'admin', 0),
(35, 'za', 'زم', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'Female', '875756756', '23456789', 'zalmosawi@boc.bh', 'bh', '3', 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '', 'uploads/trainers/202008095418.png', 1, 'e10adc3949ba59abbe56e057f20f883e', '2020-09-01 09:14:46', 'admin', 0, 'admin', 0),
(36, 'zaima', '', '', '', 'Female', '1234567', '12345678', 'zaima@gmail.com', 'BSC', '3year', '', '', 'manama', '', 1, 'e10adc3949ba59abbe56e057f20f883e', '2021-01-18 08:08:47', 'admin', 0, 'admin', 0),
(37, 'Husai', '', 'حسين', '', 'Male', '2000', '789966666', 'husai@gmail.com', 'BSC mathematics', '3', 'رياضيات البكالوريوس', '', '', '', 1, 'e10adc3949ba59abbe56e057f20f883e', '2021-03-22 08:18:31', 'admin', 0, 'admin', 0),
(38, 'Husa', 'Husa', 'حسين', 'حسين', 'Female', '2000', '87754210', 'husa@gmail.com', 'BSC mathematics', '3', 'رياضيات البكالوريوس', 'حسين', 'Bahrain', 'uploads/trainers/202103242574.png', 1, 'd41d8cd98f00b204e9800998ecf8427e', '2021-03-24 09:34:15', 'admin', 0, 'admin', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `trans_id` bigint(20) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `currency` varchar(20) NOT NULL,
  `payment_reference` varchar(50) NOT NULL,
  `gateway_reference` varchar(50) NOT NULL,
  `response_code` varchar(20) NOT NULL,
  `error_message` text NOT NULL,
  `tap_id` varchar(100) NOT NULL,
  `transaction_status` enum('Pending','Completed') NOT NULL DEFAULT 'Pending',
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`trans_id`, `amount`, `currency`, `payment_reference`, `gateway_reference`, `response_code`, `error_message`, `tap_id`, `transaction_status`, `timestamp`) VALUES
(1, '1', 'BHD', '3227201441087728645', '00', '507', 'Declined, Card Issuer', 'chg_TS033220201441s6FL2708487', 'Pending', '2020-08-28 04:38:53'),
(2, '500', 'BHD', '5628200833080852906', '00', '507', 'Declined, Card Issuer', 'chg_TS055520200833w4RF2808467', 'Pending', '2020-08-28 05:31:18'),
(3, '500', 'BHD', '5728200854084739560', '00', '507', 'Declined, Card Issuer', 'chg_TS070520200854l6H62808188', 'Pending', '2020-08-28 05:51:28'),
(4, '500', 'BHD', '0728200934083632440', '00', '507', 'Declined, Card Issuer', 'chg_TS035920200933Mq432808730', 'Pending', '2020-08-28 06:31:23'),
(5, '1', 'BHD', '1628200937084685962', '00', '507', 'Declined, Card Issuer', 'chg_TS011620200937s6ZK2808193', 'Pending', '2020-08-28 06:34:37'),
(6, '0.1', 'BHD', '0728201251081430360', '30000402729', '501', 'Declined', 'chg_LV045920201250o7K72808940', 'Pending', '2020-08-28 09:51:19'),
(7, '0.1', 'BHD', '2328201311081549034', '30000402854', '501', 'Declined', 'chg_LV022220201311Xt362808982', 'Pending', '2020-08-28 10:11:41'),
(8, '0.1', 'BHD', '5928201312081789263', '30000402868', '000', 'Captured', 'chg_LV015820201312Pn9r2808928', 'Pending', '2020-08-28 10:13:47'),
(9, '0.1', 'BHD', '5728201321085943425', '00', '503', 'Declined, 3D Security - Incorrect', 'chg_LV010520201321x7R42808375', 'Pending', '2020-08-28 10:22:41'),
(10, '0.1', 'BHD', '5528201323088362576', '30000402929', '501', 'Declined', 'chg_LV025520201323Oa9w2808633', 'Pending', '2020-08-28 10:24:13'),
(11, '0.1', 'BHD', '4628201324082784206', '1366912251302410', '000', 'Captured', 'chg_LV054620201324Kq4h2808138', 'Completed', '2020-08-28 10:25:46'),
(12, '400', 'BHD', '2228201708084320028', '30000404383', '514', 'Declined, Card Issuer - Risk Check', 'chg_LV042220201708q6H22808213', 'Pending', '2020-08-28 14:08:36'),
(13, '0.1', 'BHD', '1228201711088150775', '30000404400', '000', 'Captured', 'chg_LV011220201711n7TY2808643', 'Completed', '2020-08-28 14:11:50'),
(14, '0.1', 'BHD', '2728201121092215692', '5146823211102720', '000', 'Captured', 'chg_LV060220201121e2F72809098', 'Completed', '2020-09-28 08:21:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`att_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `course_assignments`
--
ALTER TABLE `course_assignments`
  ADD PRIMARY KEY (`ca_id`);

--
-- Indexes for table `course_attendance`
--
ALTER TABLE `course_attendance`
  ADD PRIMARY KEY (`ca_id`);

--
-- Indexes for table `course_batches`
--
ALTER TABLE `course_batches`
  ADD PRIMARY KEY (`cb_id`);

--
-- Indexes for table `course_batch_students`
--
ALTER TABLE `course_batch_students`
  ADD PRIMARY KEY (`cbs_id`);

--
-- Indexes for table `course_classes`
--
ALTER TABLE `course_classes`
  ADD PRIMARY KEY (`cc_id`);

--
-- Indexes for table `course_class_attachments`
--
ALTER TABLE `course_class_attachments`
  ADD PRIMARY KEY (`cca_id`);

--
-- Indexes for table `course_organisers`
--
ALTER TABLE `course_organisers`
  ADD PRIMARY KEY (`co_id`);

--
-- Indexes for table `course_resources`
--
ALTER TABLE `course_resources`
  ADD PRIMARY KEY (`cr_id`);

--
-- Indexes for table `course_sections`
--
ALTER TABLE `course_sections`
  ADD PRIMARY KEY (`cs_id`);

--
-- Indexes for table `course_section_attachments`
--
ALTER TABLE `course_section_attachments`
  ADD PRIMARY KEY (`csa_id`);

--
-- Indexes for table `course_students_assignments`
--
ALTER TABLE `course_students_assignments`
  ADD PRIMARY KEY (`csa_id`);

--
-- Indexes for table `course_trainers`
--
ALTER TABLE `course_trainers`
  ADD PRIMARY KEY (`ct_id`);

--
-- Indexes for table `educations`
--
ALTER TABLE `educations`
  ADD PRIMARY KEY (`edu_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`exam_id`);

--
-- Indexes for table `exam_instructions`
--
ALTER TABLE `exam_instructions`
  ADD PRIMARY KEY (`ei_id`);

--
-- Indexes for table `home_slider`
--
ALTER TABLE `home_slider`
  ADD PRIMARY KEY (`slide_id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `notes_students`
--
ALTER TABLE `notes_students`
  ADD PRIMARY KEY (`ns_id`);

--
-- Indexes for table `organisers`
--
ALTER TABLE `organisers`
  ADD PRIMARY KEY (`organiser_id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`org_id`);

--
-- Indexes for table `password_reset`
--
ALTER TABLE `password_reset`
  ADD PRIMARY KEY (`reset_id`);

--
-- Indexes for table `priority`
--
ALTER TABLE `priority`
  ADD PRIMARY KEY (`pr_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`que_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `sports`
--
ALTER TABLE `sports`
  ADD PRIMARY KEY (`sports_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`stu_id`);

--
-- Indexes for table `students_certificates`
--
ALTER TABLE `students_certificates`
  ADD PRIMARY KEY (`sc_id`);

--
-- Indexes for table `students_exams`
--
ALTER TABLE `students_exams`
  ADD PRIMARY KEY (`se_id`);

--
-- Indexes for table `students_grade`
--
ALTER TABLE `students_grade`
  ADD PRIMARY KEY (`sg_id`);

--
-- Indexes for table `students_oraganizations`
--
ALTER TABLE `students_oraganizations`
  ADD PRIMARY KEY (`so_id`);

--
-- Indexes for table `students_packages`
--
ALTER TABLE `students_packages`
  ADD PRIMARY KEY (`sp_id`);

--
-- Indexes for table `students_question_answers`
--
ALTER TABLE `students_question_answers`
  ADD PRIMARY KEY (`sqa_id`);

--
-- Indexes for table `students_sports`
--
ALTER TABLE `students_sports`
  ADD PRIMARY KEY (`ss_id`);

--
-- Indexes for table `subadmin_previleges`
--
ALTER TABLE `subadmin_previleges`
  ADD PRIMARY KEY (`sp_id`);

--
-- Indexes for table `sub_admin`
--
ALTER TABLE `sub_admin`
  ADD PRIMARY KEY (`sa_id`);

--
-- Indexes for table `tap_payment`
--
ALTER TABLE `tap_payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexes for table `trainers`
--
ALTER TABLE `trainers`
  ADD PRIMARY KEY (`trainer_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`trans_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `att_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `course_assignments`
--
ALTER TABLE `course_assignments`
  MODIFY `ca_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `course_attendance`
--
ALTER TABLE `course_attendance`
  MODIFY `ca_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `course_batches`
--
ALTER TABLE `course_batches`
  MODIFY `cb_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `course_batch_students`
--
ALTER TABLE `course_batch_students`
  MODIFY `cbs_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `course_classes`
--
ALTER TABLE `course_classes`
  MODIFY `cc_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `course_class_attachments`
--
ALTER TABLE `course_class_attachments`
  MODIFY `cca_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `course_organisers`
--
ALTER TABLE `course_organisers`
  MODIFY `co_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `course_resources`
--
ALTER TABLE `course_resources`
  MODIFY `cr_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `course_sections`
--
ALTER TABLE `course_sections`
  MODIFY `cs_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `course_section_attachments`
--
ALTER TABLE `course_section_attachments`
  MODIFY `csa_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `course_students_assignments`
--
ALTER TABLE `course_students_assignments`
  MODIFY `csa_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `course_trainers`
--
ALTER TABLE `course_trainers`
  MODIFY `ct_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `educations`
--
ALTER TABLE `educations`
  MODIFY `edu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `event_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `exam_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `exam_instructions`
--
ALTER TABLE `exam_instructions`
  MODIFY `ei_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `home_slider`
--
ALTER TABLE `home_slider`
  MODIFY `slide_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `note_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `notes_students`
--
ALTER TABLE `notes_students`
  MODIFY `ns_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `organisers`
--
ALTER TABLE `organisers`
  MODIFY `organiser_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `org_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `password_reset`
--
ALTER TABLE `password_reset`
  MODIFY `reset_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `priority`
--
ALTER TABLE `priority`
  MODIFY `pr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `que_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `sports`
--
ALTER TABLE `sports`
  MODIFY `sports_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `stu_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `students_certificates`
--
ALTER TABLE `students_certificates`
  MODIFY `sc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `students_exams`
--
ALTER TABLE `students_exams`
  MODIFY `se_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `students_grade`
--
ALTER TABLE `students_grade`
  MODIFY `sg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `students_oraganizations`
--
ALTER TABLE `students_oraganizations`
  MODIFY `so_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `students_packages`
--
ALTER TABLE `students_packages`
  MODIFY `sp_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `students_question_answers`
--
ALTER TABLE `students_question_answers`
  MODIFY `sqa_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `students_sports`
--
ALTER TABLE `students_sports`
  MODIFY `ss_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subadmin_previleges`
--
ALTER TABLE `subadmin_previleges`
  MODIFY `sp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=289;

--
-- AUTO_INCREMENT for table `sub_admin`
--
ALTER TABLE `sub_admin`
  MODIFY `sa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tap_payment`
--
ALTER TABLE `tap_payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `trainers`
--
ALTER TABLE `trainers`
  MODIFY `trainer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `trans_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
