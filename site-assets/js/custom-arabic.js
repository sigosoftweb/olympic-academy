    // Accordion
    function toggleChevron(e) {
        $(e.target)
            .prev('.card-header')
            .find("i.indicator")
            .toggleClass('icon-minus icon-plus');
    }
    $('#accordion_classes').on('hidden.bs.collapse shown.bs.collapse', toggleChevron);
		function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".indicator")
            .toggleClass('icon-minus icon-plus');
    } 

    $('#accordion_completed_classes').on('hidden.bs.collapse shown.bs.collapse', toggleChevron);
        function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".indicator")
            .toggleClass('icon-minus icon-plus');
    } 

    $('#accordion_exams_upcoming').on('hidden.bs.collapse shown.bs.collapse', toggleChevron);
        function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".indicator")
            .toggleClass('icon-minus icon-plus');
    }
    
    $('#accordion_exams_attended').on('hidden.bs.collapse shown.bs.collapse', toggleChevron);
        function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".indicator")
            .toggleClass('icon-minus icon-plus');
    }

    $('#accordion_exams_unattempted').on('hidden.bs.collapse shown.bs.collapse', toggleChevron);
        function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".indicator")
            .toggleClass('icon-minus icon-plus');
    }

    $('#accordion_assignments').on('hidden.bs.collapse shown.bs.collapse', toggleChevron);
        function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".indicator")
            .toggleClass('icon-minus icon-plus');
    }

    $('#related-courses').owlCarousel({
        center: true,
        items: 2,
        loop: true,
        margin: 0,
        responsive: {
            0: {
                items: 1
            },
            767: {
                items: 2
            },
            1000: {
                items: 3
            },
            1400: {
                items: 4
            }
        }
    });

    $('#trainers-carousel').owlCarousel({
        center: true,
        items: 2,
        loop: true,
        margin: 20,
        autoplay: true,
        responsive: {
            0: {
                items: 1
            },
            767: {
                items: 2
            },
            1000: {
                items: 3
            },
            1400: {
                items: 4
            }
        }
    });

    