<!DOCTYPE html>
<html>
<head>
    <title>Olympic Academy | Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8" />
    <link rel="icon" type="image/ico" href="<?=base_url()?>assets/images/favicon.ico" />
    <link href="<?=base_url()?>assets/css/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/vendor/bootstrap-checkbox.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/vendor/bootstrap/bootstrap-dropdown-multilevel.css">
    <link href="<?=base_url()?>assets/css/minimal.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/toaster/toaster.min.css">
  </head>
  <body class="bg-1">
    <div id="wrap">
      <div class="row">
        <div id="content" class="col-md-12 full-page login">

          <div class="inside-block">
            <img src="<?=base_url()?>assets/images/logo-big.png" alt class="logo" width="70px">
            <h1><strong>Welcome</strong> Back</h1>
            <h5>TRAINER LOGIN</h5>

            <form id="form-signin" class="form-signin" method="post" action="<?=site_url('teacher/loginCheck')?>">
              <section>
                <div class="input-group">
                  <input type="text" class="form-control" name="email" placeholder="Email" <?php if(isset($_COOKIE['teacher_username'])){ ?>value="<?=$_COOKIE['teacher_username']?>"<?php } ?>>
                  <div class="input-group-addon"><i class="fa fa-user"></i></div>
                </div>
                <div class="input-group">
                  <input type="password" class="form-control" name="password" placeholder="Password" <?php if(isset($_COOKIE['teacher_password'])){ ?>value="<?=$_COOKIE['teacher_password']?>"<?php } ?>>
                  <div class="input-group-addon"><i class="fa fa-key"></i></div>
                </div>
              </section>
              <section class="controls">
                <div class="checkbox check-transparent">
                  <input type="checkbox" value="1" id="remember" name="remember" checked>
                  <label for="remember">Remember me</label>
                </div>
              </section>
              <section class="log-in">
                <button class="btn btn-greensea">Log In</button>
              </section>
            </form>
          </div>

        </div>
      </div>
    </div>
  </body>
  <script src="<?=base_url()?>assets/js/jquery.js"></script>
  <script src="<?=base_url()?>assets/plugins/toaster/toaster.min.js"></script>
  <script type="text/javascript">
	  toastr.options = {
	    "closeButton": true,
	    "debug": false,
	    "newestOnTop": false,
	    "progressBar": true,
	    "positionClass": "toast-top-right",
	    "preventDuplicates": false,
	    "showDuration": "300",
	    "hideDuration": "1000",
	    "timeOut": "5000",
	    "extendedTimeOut": "1000",
	    "showEasing": "swing",
	    "hideEasing": "linear",
	    "showMethod": "fadeIn",
	    "hideMethod": "fadeOut"
	  }
	  <?php
	      $alert = $this->session->flashdata('alert_type');
	      if ($alert) { ?>
	          toastr.<?=$this->session->flashdata('alert_type')?>("<?=$this->session->flashdata('alert_message')?>","<?php echo $this->session->flashdata('alert_title')?>");
	      <?php }
	  ?>
  </script>
</html>
