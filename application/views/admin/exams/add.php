<!DOCTYPE html>

<html>

   <head>

      <title><?=$title?></title>

      <?php $this->load->view('admin/includes/includes.php'); ?>

      <?php $this->load->view('admin/includes/table-css.php'); ?>



      <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/chosen/css/chosen.min.css">

      <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/chosen/css/chosen-bootstrap.css">

	  <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/datepicker/css/bootstrap-datetimepicker.css">

   </head>

   <body class="solid-bg-6">

      <div class="mask">

         <div id="loader"></div>

      </div>

      <div id="wrap">

         <div class="row">

            <?php $this->load->view('admin/includes/sidebar.php'); ?>

            <div id="content" class="col-md-12">

               <div class="pageheader">

                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i>Course : <?=$course->course_title?></h2>

                  <div class="breadcrumbs">

                     <ol class="breadcrumb">

                        <li>Olymbic</li>

                        <li>Add Course</li>

                     </ol>

                  </div>

               </div>

               <div class="main">

                  <div class="row">

                     <div class="col-md-12">

                        <section class="tile color transparent-black">

                           <div class="tile-body">

							   <form class="new-added-form" method="POST" action="<?=site_url('admin/exams/addExam')?>" id="add-form">

								   <input type="hidden" name="course_id" value="<?=$course->course_id?>">

                                     <div class="row">

                                         <div class="col-md-6 form-group">

                                             <label>Exam name *</label>

                                             <input type="text" class="form-control" name="name" required>

                                         </div>

										 <div class="col-md-6 form-group">

                                             <label>Exam name ( Arabic )*</label>

                                             <input type="text" class="form-control" name="name_arabic" id="exam-name-arabic" required>

                                         </div>

                                         <div class="col-md-3 form-group">

                                             <label>Number of questions *</label>

                                             <input type="number" min="0" max="1000" class="form-control" name="no_of_questions" required>

                                         </div>

                                         <div class="col-md-3 form-group">

                                             <label>Time (In minutes) *</label>

                                             <input type="number" min="0" max="1000" class="form-control" name="exam_time" required>

                                         </div>

										 <div class="col-md-3 form-group">

                                             <label>Batch</label>

                                             <select class="form-control" name="cb_id">

												 <option value="0">For all batch</option>

												 <?php foreach ($batches as $batch) { ?>

												 	<option value="<?=$batch->cb_id?>"><?=$batch->batch_name?></option>

												 <?php } ?>

                                             </select>

                                         </div>

										 <div class="col-md-3 form-group">

                                             <label>Sections</label>

                                             <select class="form-control" name="cs_id">

												 <option value="0">No section</option>

												 <?php foreach ($sections as $section) { ?>

												 	<option value="<?=$section->cs_id?>"><?=$section->section_title?></option>

												 <?php } ?>

                                             </select>

                                         </div>

                                         <?php /*<div class="col-lg-12 col-12 form-group">

                                             <label>Set time</label>

                                             <label class="radio-inline"><input type="radio" name="time" id="timeTrue" value="1">&nbsp; Yes</label>

                                             <label class="radio-inline"><input type="radio" name="time" id="timeFalse" value="0" checked>&nbsp;  No  </label>

                                         </div> */ ?>

                                         <div id="time-div" >

											 <div class="form-group col-sm-12 col-md-12">

												<label for="exampleInputCountry">Starting time</label>

												<input type="text" class="form-control" id="datepicker" required name="from_time">

											</div>

                                         </div>



                                         <div class="col-md-12 form-group">

                                             <label>Instructions *</label>

                                             <div id="options-div">

                                                 <table id="options" width="100%">

													 <thead>

													 	<th>Instruction</th>

														<th>Instruction ( Arabic )</th>

														<th></th>

													 </thead>

                                                   <tr>

                                                     <td width="45%"><input type="text" class="form-control" name="instructions[]" required></td>

													 <td width="45%"><input type="text" class="form-control" name="instructions_arabic[]" required></td>

                                                     <td width="10%"></td>

                                                   </tr>

                                                 </table>

                                               </div>



                                               <div class="text-center">

                                                 <button type="button" class="btn btn-link" onclick="addRow()"><i style="font-size:25px;" class="fa fa-plus-circle"></i></button>

                                               </div>

                                         </div>

                                         <div class="col-md-12 form-group">

                                             <label>Show result</label>

                                             <label class="radio-inline"><input type="radio" name="show_result" value="1" checked> Yes</label>

                                             <label class="radio-inline"><input type="radio" name="show_result" value="0">&nbsp;  No  </label>

                                         </div>

                                         <div class="col-md-12 form-group">

                                             <button type="submit" class="btn btn-primary" id="submit-button">Add</button>

                                         </div>

                                     </div>

                                 </form>

                           </div>

                        </section>

                     </div>

                  </div>

               </div>

            </div>

         </div>

      </div>

      <?php $this->load->view('admin/includes/scripts.php'); ?>

      <?php $this->load->view('admin/includes/table-script.php'); ?>

      <script src="<?=base_url()?>assets/js/vendor/chosen/chosen.jquery.min.js"></script>

	  <script src="<?=base_url()?>assets/js/vendor/datepicker/bootstrap-datetimepicker.min.js"></script>

	  <script type="text/javascript">

		  $('#add-form').on('submit', function(e){

			  e.preventDefault();

              $('#submit-button').attr('disabled',true);



			  var exam_name = $('#exam-name-arabic').val();

  			  if (arabicCheck(exam_name)) {

				  //if (document.getElementById('timeTrue').checked) {

	                  var start_time = $('#datepicker').val();

	                  start = changeFormat(start_time);

	                  $('#start-time').val(start);

	                  date = new Date();

	                  d1 = new Date(start);

	                  d2 = new Date(date);

	                  if (new Date(start) > new Date(date)) {

	                      document.getElementById("add-form").submit();

	                  }

	                  else {

	                      toastr.error('Start time must be greater than current time',"");

	                      $('#submit-button').attr('disabled',false);

	                  }

	              //}

	              //else {

	                  //document.getElementById("add-form").submit();

	              //}

			  }

			  else {

				  toastr.error('Please check arabic exam name',"");

				  $('#submit-button').attr('disabled',false);

			  }

		  });

		  function arabicCheck(arabic)

  		  {

  			if (arabic == '') {

  				return true;

  			}

  			else {

  				var string = arabic.replace(/\s/g,'');

  				var isArabic =  /[\u0600-\u06FF\u0750-\u077F]/;

  				if (isArabic.test(string)){

  					 return true;

  				}

  				else {

  					return false;

  				}

  			}

  		  }

		  function changeFormat(date)

          {

			  arr = date.split(" ");

              if (arr[2] == 'PM') {

                  tm = arr[1].split(":");

                  if (tm[0] == 12) {

                      hour = tm[0];

                  }

                  else {

                      hour = +12 + +tm[0];

                  }

                  time = hour + ':' + tm[1] + ':00';

              }

              else {

                  time = arr[1] + ':00';

              }

              return arr[0].split("/").reverse().join("-") + ' ' + time;

          }

		  $(document).on('change','input:radio[name="time"]',function(){

            if (document.getElementById('timeTrue').checked) {

                $("#datepicker").prop('required',true);

                $('#time-div').css('display','block');

            }

            if (document.getElementById('timeFalse').checked) {

                $("#datepicker").prop('required',false);

                $('#time-div').css('display','none');

            }

        });

		  $(function(){

			  $(".chosen-select").chosen({disable_search_threshold: 10});

			  $('#datepicker').datetimepicker({

				  format:'DD/MM/YYYY HH:mm A',

		          icons: {

		              time: "fa fa-clock-o",

		              date: "fa fa-calendar",

		              up: "fa fa-arrow-up",

		              down: "fa fa-arrow-down"

		          }

		      });

		      $("#datepicker").on("dp.show",function (e) {

		        var newtop = $('.bootstrap-datetimepicker-widget').position().top - 45;

		        $('.bootstrap-datetimepicker-widget').css('top', newtop + 'px');

		      });

		  });

		  function addRow()

          {

	          var col1 = "<tr><td><input type='text' class='form-control' name='instructions[]' required></td>";

			  var col2 = "<td><input type='text' class='form-control' name='instructions_arabic[]' required></td>";

	          var col3 = "<td><a class='btn btn-link' onclick='deleteRow(this);'><i style='font-size:25px; color:red;' class='fa fa-minus-circle'></i></a></td></tr>";

	          var row = col1 + col2 + col3;

	          $('#options').append(row);

          }

          function deleteRow(row)

          {

          	  $(row).closest('tr').remove();

          }

	  </script>

   </body>

</html>

