<!DOCTYPE html>

<html>

   <head>

      <title><?=$title?></title>

      <?php $this->load->view('admin/includes/includes.php'); ?>

      <?php $this->load->view('admin/includes/table-css.php'); ?>

   </head>

   <body class="solid-bg-6">

      <div class="mask">

         <div id="loader"></div>

      </div>

      <div id="wrap">

         <div class="row">

            <?php $this->load->view('admin/includes/sidebar.php'); ?>

            <div id="content" class="col-md-12">

               <div class="pageheader">

                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i><?=$exam_name?> - Exam result</h2>

                  <div class="breadcrumbs">

                     <ol class="breadcrumb">

                        <li>Olymbic</li>

                        <li>Add Course</li>

                     </ol>

                  </div>

               </div>

               <div class="main">

                  <div class="row">

                     <div class="col-md-12">

                        <section class="tile color transparent-black">

                           <div class="tile-body">

							   <table class="table table-striped table-bordered" style="width :100%;" id="datatable-export">

					              <thead>

					                   <th width="10%">Name</th>

					                   <th width="10%">Questions</th>

					                   <th width="10%">Answered</th>

					                   <th width="10%">Skipped</th>

					                   <th width="10%">Correct<br>Answers</th>

					                   <th width="10%">Wrong<br>Answers</th>

					                   <th width="10%">Mark</th>

					                   <th width="10%">Total</th>

					                   <th width="10%">Time taken</th>
                                  <th width="10%">Mobile No.</th>
                                  <th width="5%">Options</th>

					              </thead>

								  <tbody>

								  	<?php foreach ($leaders as $leader) { ?>

								  		<tr>

								  			<td><?=$leader->stu_name_english . ' ' . $leader->stu_name_arabic?></td>

											<td><?=$leader->questions?></td>

											<td><?=$leader->attended?></td>

											<td><?=$leader->left_questions?></td>

											<td><?=$leader->correct_answers?></td>

											<td><?=$leader->wrong_answers?></td>

											<td><?=$leader->mark?></td>

											<td><?=$leader->total_mark?></td>

											<td><?=$leader->time_taken?></td>
                                            <td><?=$leader->stu_mobile?></td>
                                            <td>
                                                <a class="btn btn-danger btn-xs" onclick="return confirm('Are you sure to delete this entry?')" href="<?=site_url('admin/exams/deleteEntry/' . $leader->se_id)?>">Delete</a><br>
                                                <button class="btn btn-secondary btn-xs" style="margin-top:5px;" onclick="updateMark(<?=$leader->se_id?>,<?=$leader->mark?>)">Mark</button>
                                            </td>
								  		</tr>

								  	<?php } ?>

								  	

								  </tbody>

					            </table>

                           </div>

                        </section>

                     </div>

                  </div>

               </div>

            </div>

         </div>
        <div class="modal fade" id="update-mark" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
              <h3 class="modal-title" id="modalConfirmLabel"><strong>Update</strong> Mark</h3>
            </div>
            <div class="modal-body">
              <form role="form" id="add-form" action="<?=site_url('admin/exams/updateMark')?>" method="post">
                  <input type="hidden" id="se_id" name="se_id">
                <div class="form-group">
                  <label for="placeholderInput">Mark *</label>
                  <input type="number" step="any" name="mark" id="mark" class="form-control" required>
                </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
              <button type="submit" class="btn btn-green" id="submit-button">Update</button>
            </div>
			</form>
          </div>
        </div>
      </div>
      </div>

      <?php $this->load->view('admin/includes/scripts.php'); ?>

      <?php $this->load->view('admin/includes/table-script.php'); ?>

    <script>
        function updateMark(se_id,mark)
        {
            $('#se_id').val(se_id);
            $('#mark').val(mark);
            $('#update-mark').modal('show');
        }
    </script>
   </body>

</html>

