<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('admin/includes/includes.php'); ?>
	  <?php $this->load->view('admin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-graduation-cap" style="line-height: 48px;padding-left: 2px;"></i> Exam instructions</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Exam instructions</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">


		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">
								 <button type="button" class="btn btn-default btn-lg margin-bottom-20" data-toggle="modal" data-target="#add-instruction">Add Instruction</button>
		                     </div>
		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom">
								   <thead>
  		                           <tr>
  		                             <th width="40%">Instruction</th>
									 <th width="40%">Instruction( Arabic )</th>
  		                             <th width="10%">Edit</th>
                                     <th width="10%">Delete</th>
  		                           </tr>
  		                         </thead>
  		                         <tbody>
									 <?php foreach ($instructions as $instruction) { ?>
										 <tr>
										 	<td><?=$instruction->instruction?></td>
											<td><?=$instruction->instruction_arabic?></td>
										 	<td><button type="button" class="btn btn-primary btn-xs margin-bottom-20" onclick="edit(<?=$instruction->ei_id?>)">Edit</button></td>
                                            <td><a href="<?=site_url('admin/exams/deleteInstruction/'.$instruction->ei_id)?>"><button type="button" class="btn btn-danger btn-xs margin-bottom-20">Delete</button></a></td>
										 </tr>
									 <?php } ?>
  		                         </tbody>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="add-instruction" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
              <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> Instruction</h3>
            </div>
            <div class="modal-body">
              <form role="form" id="add-form" action="<?=site_url('admin/exams/addInstruction')?>" method="post">

                <div class="form-group">
                  <label for="placeholderInput">Instruction</label>
                  <textarea type="text" name="instruction" class="form-control" placeholder="Instruction..." required></textarea>
                  <input type="hidden" name="exam_id" value="<?=$exam->exam_id?>">
                </div>
				<div class="form-group">
                  <label for="placeholderInput">Instruction ( Arabic )</label>
                  <textarea type="text" name="instruction_arabic" class="form-control" id="instruction-arabic" required></textarea>
                </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
              <button type="submit" class="btn btn-green" id="submit-button">Add</button>
            </div>
         </form>
          </div>
        </div>
      </div>

     <div class="modal fade" id="edit-instruction" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
              <h3 class="modal-title" id="modalConfirmLabel"><strong>Edit</strong> Instruction</h3>
            </div>
            <div class="modal-body">
              <form role="form" id="edit-form" action="<?=site_url('admin/exams/editInstruction')?>" method="post">
                <input type="hidden" name="inst_id" id="inst_id">
                <input type="hidden" name="exam_id" id="exam_id">
                <div class="form-group">
                  <label for="placeholderInput">Instruction</label>
                  <textarea type="text" name="instruction" id="instruction" class="form-control" placeholder="Instruction..." required></textarea>
                </div>
				<div class="form-group">
                  <label for="placeholderInput">Instruction ( Arabic )</label>
                  <textarea type="text" name="instruction_arabic" class="form-control" id="instruction-arabic-edit" required></textarea>
                </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
              <button type="submit" class="btn btn-green" id="submit-button-edit">Update</button>
            </div>
         </form>
          </div>
        </div>
      </div>

      <section class="videocontent" id="video"></section>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
	  <?php $this->load->view('admin/includes/table-script.php'); ?>
       <script type="">
          function edit(inst_id)
          {
              $('#inst_id').val(inst_id);
              $.ajax({
                method: "POST",
                url: "<?=site_url('admin/exams/getInstructionDetails');?>",
                data : { inst_id : inst_id },
                dataType : "json",
                success : function( data ){
                    $('#instruction').val(data.instruction);
					$('#instruction-arabic-edit').val(data.instruction_arabic);
                    $('#exam_id').val(data.exam_id);
                    $('#edit-instruction').modal('show');
                }
              });
          }
		  $('#add-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button').attr('disabled',true);

			  var instruction = $('#instruction-arabic').val();
  			  if (arabicCheck(instruction)) {
				  document.getElementById("add-form").submit();
			  }
			  else {
				  toastr.error('Please check arabic instruction',"");
				  $('#submit-button').attr('disabled',false);
			  }
		  });
		  $('#edit-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button-edit').attr('disabled',true);

			  var instruction = $('#instruction-arabic-edit').val();
  			  if (arabicCheck(instruction)) {
				  document.getElementById("edit-form").submit();
			  }
			  else {
				  toastr.error('Please check arabic instruction',"");
				  $('#submit-button-edit').attr('disabled',false);
			  }
		  });
		  function arabicCheck(arabic)
  		  {
  			if (arabic == '') {
  				return true;
  			}
  			else {
  				var string = arabic.replace(/\s/g,'');
  				var isArabic =  /[\u0600-\u06FF\u0750-\u077F]/;
  				if (isArabic.test(string)){
  					 return true;
  				}
  				else {
  					return false;
  				}
  			}
  		  }
    </script>
   </body>
</html>
