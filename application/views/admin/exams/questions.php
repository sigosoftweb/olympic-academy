<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8" />
    <link rel="icon" type="image/ico" href="<?=base_url()?>assets/images/favicon.png" />
    <link href="<?=base_url()?>assets/css/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/vendor/animate/animate.css">
    <link type="text/css" rel="stylesheet" media="all" href="<?=base_url()?>assets/js/vendor/mmenu/css/jquery.mmenu.all.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/vendor/bootstrap-checkbox.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/vendor/bootstrap/bootstrap-dropdown-multilevel.css">
    <link href="<?=base_url()?>assets/css/minimal.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/toaster/toaster.min.css">

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>

	<script type="text/javascript" src="<?=base_url()?>assets/plugins/wiris/tinymce4/tinymce.min.js"></script>
	<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/plugins/wiris/css/prism.css" />
	<link rel="stylesheet" href="<?=base_url()?>assets/plugins/wiris/css/style.css">
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
      <style>
	    .tinyclass{
	        border : 1px solid #A9A9A9;
	        margin-bottom : 5px;
	        min-height : 70px;
	        color:black;
	    }
	</style>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i>Add Questions</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Add Course</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
							   <form class="new-added-form" method="POST" action="<?=site_url('admin/exams/addQ')?>" id="add-form" enctype="multipart/form-data">
									<div class="row">
										<div class="col-xl-12 col-lg-12 col-12">
                                            <h5>Question <?=$questions+1?>/<?=$exam->no_questions?></h5>
                                        </div>
										<div class="col-md-3">
											<label class="text-dark-medium">Upload Image</label>
                                            <input type="file" class="form-control-file" name="image" placeholder="Default Input" id="upload">
										</div>
									</div>
                                    <div class="row" style="margin-top:20px;">
                                        <input type="hidden" name="exam_id" value="<?=$exam->exam_id?>">
                                        <div class="col-xl-6 col-lg-6 col-6 form-group">
                                            <div id="editorContainer1">
                                                <span>Question</span>
										        <div class="tinyclass myeditablediv" id="formula-question"></div>
										        <input type="hidden" id="question_src" name="formula_question">
								            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-6 form-group">
                                            <div id="editorContainer1">
                                                <span>Question ( In arabic )</span>
										        <div class="tinyclass myeditablediv" id="formula-question-arabic"></div>
										        <input type="hidden" id="question_src_arabic" name="formula_question_arabic">
								            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-6 form-group">
                                            <span>A<span style="color: red;">*</span></span>
                                            <div id="editorContainer1">
										        <div class="tinyclass myeditablediv1" id="formula-a"></div>
										        <input type="hidden" id="formula_a" name="formula_a">
								            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-6 form-group">
                                            <span>A<span style="color: red;">*</span>( Arabic )</span>
                                            <div id="editorContainer1">
										        <div class="tinyclass myeditablediv1" id="formula-a-arabic"></div>
										        <input type="hidden" id="formula_a_arabic" name="formula_a_arabic">
								            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-6 form-group">
                                            <span>B<span style="color: red;">*</span></span>
                                            <div id="editorContainer2">
										        <div class="tinyclass myeditablediv1" id="formula-b"></div>
										        <input type="hidden" id="formula_b" name="formula_b">
								            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-6 form-group">
                                            <span>B<span style="color: red;">*</span>( Arabic )</span>
                                            <div id="editorContainer2">
										        <div class="tinyclass myeditablediv1" id="formula-b-arabic"></div>
										        <input type="hidden" id="formula_b_arabic" name="formula_b_arabic">
								            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-6 form-group">
                                            <span>C</span>
                                            <div id="editorContainer3">
										        <div class="tinyclass myeditablediv1" id="formula-c"></div>
										        <input type="hidden" id="formula_c" name="formula_c">
								            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-6 form-group">
                                            <span>C ( Arabic )</span>
                                            <div id="editorContainer3">
										        <div class="tinyclass myeditablediv1" id="formula-c-arabic"></div>
										        <input type="hidden" id="formula_c_arabic" name="formula_c_arabic">
								            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-6 form-group">
                                            <span>D</span>
                                            <div id="editorContainer4">
										        <div class="tinyclass myeditablediv1" id="formula-d"></div>
										        <input type="hidden" id="formula_d" name="formula_d">
								            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-6 form-group">
                                            <span>D ( Arabic )</span>
                                            <div id="editorContainer4">
										        <div class="tinyclass myeditablediv1" id="formula-d-arabic"></div>
										        <input type="hidden" id="formula_d_arabic" name="formula_d_arabic">
								            </div>
                                        </div>
										<div class="col-xl-6 col-lg-6 col-6 form-group">
                                            <span>E</span>
                                            <div id="editorContainer5">
										        <div class="tinyclass myeditablediv1" id="formula-e"><p></p></div>
										        <input type="hidden" id="formula_e" name="formula_e">
								            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-6 form-group">
                                            <span>E ( Arabic )</span>
                                            <div id="editorContainer5">
										        <div class="tinyclass myeditablediv1" id="formula-e-arabic"><p></p></div>
										        <input type="hidden" id="formula_e_arabic" name="formula_e_arabic">
								            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-3 col-3 form-group">
                                            <label>Answer</label>
                                            <select class="form-control" name="answer" id="answer" required>
                                                <option value="">-- Please select answer --</option>
                                                <option value="a">A</option>
                                                <option value="b">B</option>
                                                <option value="c">C</option>
                                                <option value="d">D</option>
												<option value="e">E</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 form-group">
                                            <label>Mark</label>
                                            <input type="number" step="any" class="form-control" name="correct" required>
                                        </div>
                                        <div class="col-md-3 form-group">
                                            <label>Negative mark</label>
                                            <input type="number" step="any" class="form-control" name="negative" required>
                                        </div>
                                        <div class="col-md-12 form-group float-right pull-right">
                                            <button type="submit" id="submit-button" class="btn btn-primary pull-right float-right">Add</button>
                                        </div>
                                    </div>
                                </form>
                           </div>
                        </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--<script src="<?=base_url()?>assets/js/jquery.js"></script>-->
<script src="<?=base_url()?>assets/js/vendor/bootstrap/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/js/vendor/bootstrap/bootstrap-dropdown-multilevel.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/vendor/mmenu/js/jquery.mmenu.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/vendor/sparkline/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/vendor/nicescroll/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/vendor/animate-numbers/jquery.animateNumbers.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/vendor/videobackground/jquery.videobackground.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/vendor/blockui/jquery.blockUI.js"></script>
<script src="<?=base_url()?>assets/js/vendor/momentjs/moment-with-langs.min.js"></script>
<script src="<?=base_url()?>assets/js/minimal.min.js"></script>
<script src="<?=base_url()?>assets/plugins/toaster/toaster.min.js"></script>
<script type="text/javascript">
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": true,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
    toastr.<?=$this->session->flashdata('alert_type')?>("<?=$this->session->flashdata('alert_message')?>","<?php echo $this->session->flashdata('alert_title')?>");
    </script>
      <script type="text/javascript" src="<?=base_url()?>assets/plugins/wiris/js/prism.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/plugins/wiris/js/wirislib.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/plugins/wiris/tinymce4/plugins/tiny_mce_wiris/integration/WIRISplugins.js?viewer=image"></script>
	<script>
		tinymce.init({
			inline: true,
			selector: '.myeditablediv',
			language: 'en',
			directionality : 'ltr',
			menubar : false,
			plugins: 'tiny_mce_wiris',
			toolbar: 'code,|,bold,italic,underline,|,cut,copy,paste,|,search,|,undo,redo,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,fontselect,fontsizeselect,|,tiny_mce_wiris_formulaEditor,tiny_mce_wiris_formulaEditorChemistry,|,fullscreen',
			mathTypeParameters : {'editorParameters' : {'fontSize' : '26px'}},
			setup : function(ed)
			{
				ed.on('init', function()
				{
					this.getDoc().body.style.fontSize = '16px';
					this.getDoc().body.style.fontFamily = 'Arial, "Helvetica Neue", Helvetica, sans-serif';
				});
			},

		});

		tinymce.init({
			inline: true,
			selector: '.myeditablediv',
			language: 'en',
			directionality : 'ltr',
			menubar : false,
			plugins: 'tiny_mce_wiris',
			toolbar: 'code,|,bold,italic,underline,|,cut,copy,paste,|,search,|,undo,redo,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,fontselect,fontsizeselect,|,tiny_mce_wiris_formulaEditor,tiny_mce_wiris_formulaEditorChemistry,|,fullscreen',
			mathTypeParameters : {'editorParameters' : {'fontSize' : '26px'}},
			setup : function(ed)
			{
				ed.on('init', function()
				{
					this.getDoc().body.style.fontSize = '16px';
					this.getDoc().body.style.fontFamily = 'Arial, "Helvetica Neue", Helvetica, sans-serif';
				});
			},

		});

		tinymce.init({
			inline: true,
			selector: '.myeditablediv1',
			language: 'en',
			directionality : 'ltr',
			menubar : false,
			plugins: 'tiny_mce_wiris',
			toolbar: 'code,|,bold,italic,underline,|,cut,copy,paste,|,search,|,undo,redo,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,fontselect,fontsizeselect,|,tiny_mce_wiris_formulaEditor,tiny_mce_wiris_formulaEditorChemistry,|,fullscreen',
			mathTypeParameters : {'editorParameters' : {'fontSize' : '26px'}},
			setup : function(ed)
			{
				ed.on('init', function()
				{
					this.getDoc().body.style.fontSize = '16px';
					this.getDoc().body.style.fontFamily = 'Arial, "Helvetica Neue", Helvetica, sans-serif';
				});
			},

		});

		tinymce.init({
			inline: true,
			selector: '.myeditablediv2',
			language: 'en',
			directionality : 'ltr',
			menubar : false,
			plugins: 'tiny_mce_wiris',
			toolbar: 'code,|,bold,italic,underline,|,cut,copy,paste,|,search,|,undo,redo,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,fontselect,fontsizeselect,|,tiny_mce_wiris_formulaEditor,tiny_mce_wiris_formulaEditorChemistry,|,fullscreen',
			mathTypeParameters : {'editorParameters' : {'fontSize' : '26px'}},
			setup : function(ed)
			{
				ed.on('init', function()
				{
					this.getDoc().body.style.fontSize = '16px';
					this.getDoc().body.style.fontFamily = 'Arial, "Helvetica Neue", Helvetica, sans-serif';
				});
			},

		});

		tinymce.init({
			inline: true,
			selector: '.myeditablediv3',
			language: 'en',
			directionality : 'ltr',
			menubar : false,
			plugins: 'tiny_mce_wiris',
			toolbar: 'code,|,bold,italic,underline,|,cut,copy,paste,|,search,|,undo,redo,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,fontselect,fontsizeselect,|,tiny_mce_wiris_formulaEditor,tiny_mce_wiris_formulaEditorChemistry,|,fullscreen',
			mathTypeParameters : {'editorParameters' : {'fontSize' : '26px'}},
			setup : function(ed)
			{
				ed.on('init', function()
				{
					this.getDoc().body.style.fontSize = '16px';
					this.getDoc().body.style.fontFamily = 'Arial, "Helvetica Neue", Helvetica, sans-serif';
				});
			},

		});

		tinymce.init({
			inline: true,
			selector: '.myeditablediv4',
			language: 'en',
			directionality : 'ltr',
			menubar : false,
			plugins: 'tiny_mce_wiris',
			toolbar: 'code,|,bold,italic,underline,|,cut,copy,paste,|,search,|,undo,redo,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,fontselect,fontsizeselect,|,tiny_mce_wiris_formulaEditor,tiny_mce_wiris_formulaEditorChemistry,|,fullscreen',
			mathTypeParameters : {'editorParameters' : {'fontSize' : '26px'}},
			setup : function(ed)
			{
				ed.on('init', function()
				{
					this.getDoc().body.style.fontSize = '16px';
					this.getDoc().body.style.fontFamily = 'Arial, "Helvetica Neue", Helvetica, sans-serif';
				});
			},

		});

		tinymce.init({
			inline: true,
			selector: '.myeditablediv5',
			language: 'en',
			directionality : 'ltr',
			menubar : false,
			plugins: 'tiny_mce_wiris',
			toolbar: 'code,|,bold,italic,underline,|,cut,copy,paste,|,search,|,undo,redo,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,fontselect,fontsizeselect,|,tiny_mce_wiris_formulaEditor,tiny_mce_wiris_formulaEditorChemistry,|,fullscreen',
			mathTypeParameters : {'editorParameters' : {'fontSize' : '26px'}},
			setup : function(ed)
			{
				ed.on('init', function()
				{
					this.getDoc().body.style.fontSize = '16px';
					this.getDoc().body.style.fontFamily = 'Arial, "Helvetica Neue", Helvetica, sans-serif';
				});
			},

		});

	</script>
	<script>
        $('#add-form').on('submit', function(e){
            e.preventDefault();
            $('#submit-button').attr('disabled',true);
            
            flag = true;
            var question_formula = $('#formula-question').html();
            if(question_formula.match(/<img/))
            {
                $('#question_src').val(question_formula);
            }
            else
            {
                var question_para = $(question_formula).find("p").andSelf().filter("p:first").first().text();
                if (!question_para.replace(/\s/g, '').length) {
                    toastr.error('','Please enter the question');
                    $('#quetsion_src').val('');
                    flag = false;
                }
                else{
                    $('#question_src').val(question_formula);
                }
            }
            
            var question_formula_arabic = $('#formula-question-arabic').html();
            if(question_formula_arabic.match(/<img/))
            {
                $('#question_src').val(question_formula_arabic);
            }
            else
            {
                var question_para_arabic = $(question_formula_arabic).find("p").andSelf().filter("p:first").first().text();
                if (!question_para_arabic.replace(/\s/g, '').length) {
                    $('#quetsion_src_arabic').val('');
                }
                else{
                    $('#question_src_arabic').val(question_formula_arabic);
                }
            }
            
            var formula_a = $('#formula-a').html();
            if(formula_a.match(/<img/))
            {
                $('#formula_a').val(formula_a);
            }
            else
            {
                var para_a = $(formula_a).find("p").andSelf().filter("p:first").first().text();
                if (!para_a.replace(/\s/g, '').length) {
                    toastr.error('','Please enter option A');
                    $('#formula_a').val('');
                    flag = false;
                }
                else{
                    $('#formula_a').val(formula_a);
                }
            }
            
            var formula_a_arabic = $('#formula-a-arabic').html();
            if(formula_a_arabic.match(/<img/))
            {
                $('#formula_a_arabic').val(formula_a_arabic);
            }
            else
            {
                var para_a_arabic = $(formula_a_arabic).find("p").andSelf().filter("p:first").first().text();
                if (!para_a_arabic.replace(/\s/g, '').length) {
                    $('#formula_a_arabic').val('');
                }
                else{
                    $('#formula_a_arabic').val(formula_a_arabic);
                }
            }
            
            var formula_b = $('#formula-b').html();
            if(formula_b.match(/<img/))
            {
                $('#formula_b').val(formula_b);
            }
            else
            {
                var para_b = $(formula_b).find("p").andSelf().filter("p:first").first().text();
                if (!para_b.replace(/\s/g, '').length) {
                    toastr.error('','Please enter option B');
                    $('#formula_b').val('');
                    flag = false;
                }
                else{
                    $('#formula_b').val(formula_b);
                }
            }
            
            var formula_b_arabic = $('#formula-b-arabic').html();
            if(formula_b_arabic.match(/<img/))
            {
                $('#formula_b_arabic').val(formula_b_arabic);
            }
            else
            {
                var para_b_arabic = $(formula_b_arabic).find("p").andSelf().filter("p:first").first().text();
                if (!para_b_arabic.replace(/\s/g, '').length) {
                    $('#formula_b_arabic').val('');
                }
                else{
                    $('#formula_b_arabic').val(formula_b_arabic);
                }
            }
            
            var formula_c = $('#formula-c').html();
            if(formula_c.match(/<img/))
            {
                $('#formula_c').val(formula_c);
            }
            else
            {
                var para_c = $(formula_c).find("p").andSelf().filter("p:first").first().text();
                if (!para_c.replace(/\s/g, '').length) {
                    $('#formula_c').val('');
                }
                else{
                    $('#formula_c').val(formula_c);
                }
            }
            
            var formula_c_arabic = $('#formula-c-arabic').html();
            if(formula_c_arabic.match(/<img/))
            {
                $('#formula_c_arabic').val(formula_c_arabic);
            }
            else
            {
                var para_c_arabic = $(formula_c_arabic).find("p").andSelf().filter("p:first").first().text();
                if (!para_c_arabic.replace(/\s/g, '').length) {
                    $('#formula_c_arabic').val('');
                }
                else{
                    $('#formula_c_arabic').val(formula_c_arabic);
                }
            }
            
            var formula_d = $('#formula-d').html();
            if(formula_d.match(/<img/))
            {
                $('#formula_d').val(formula_d);
            }
            else
            {
                var para_d = $(formula_d).find("p").andSelf().filter("p:first").first().text();
                if (!para_d.replace(/\s/g, '').length) {
                    $('#formula_d').val('');
                }
                else{
                    $('#formula_d').val(formula_d);
                }
            }
            
            var formula_d_arabic = $('#formula-d-arabic').html();
            if(formula_d_arabic.match(/<img/))
            {
                $('#formula_d_arabic').val(formula_d_arabic);
            }
            else
            {
                var para_d_arabic = $(formula_d_arabic).find("p").andSelf().filter("p:first").first().text();
                if (!para_d_arabic.replace(/\s/g, '').length) {
                    $('#formula_d_arabic').val('');
                }
                else{
                    $('#formula_d_arabic').val(formula_d_arabic);
                }
            }
            
            var formula_e = $('#formula-e').html();
            if(formula_e.match(/<img/))
            {
                $('#formula_e').val(formula_e);
            }
            else
            {
                var para_e = $(formula_e).find("p").andSelf().filter("p:first").first().text();
                if (!para_e.replace(/\s/g, '').length) {
                    $('#formula_e').val('');
                }
                else{
                    $('#formula_e').val(formula_e);
                }
            }
            
            var formula_e_arabic = $('#formula-e-arabic').html();
            if(formula_e_arabic.match(/<img/))
            {
                $('#formula_e_arabic').val(formula_e_arabic);
            }
            else
            {
                var para_e_arabic = $(formula_e_arabic).find("p").andSelf().filter("p:first").first().text();
                if (!para_e_arabic.replace(/\s/g, '').length) {
                    $('#formula_e_arabic').val('');
                }
                else{
                    $('#formula_e_arabic').val(formula_e_arabic);
                }
            }
            
            if(flag)
            {
                var answer = $('#answer').val();
                flag = true;
                if (answer == 'c') {
                    if ( $('#formula_c').val() == '' ) {
                        flag = false;
                        message = "Since option c is empty it can't be considered as answer";
                    }
                }
                else if (answer == 'd') {
                    if ( $('#formula_d').val() == '' ) {
                        flag = false;
                        message = "Since option d is empty it can't be considered as answer";
                    }
                }
    			else if (answer == 'e') {
                    if ( $('#formula_e').val() == '' ) {
                        flag = false;
                        message = "Since option e is empty it can't be considered as answer";
                    }
                }
                if (flag) {
                    document.getElementById("add-form").submit();
                }
                else {
                    toastr.error(message);
                    $('#submit-button').attr('disabled',false);
                }
            }
            else
            {
                $('#submit-button').attr('disabled',false);
            }
        });
    </script>
   </body>
</html>
