<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('admin/includes/includes.php'); ?>
      <?php $this->load->view('admin/includes/table-css.php'); ?>
	  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/image-crop/croppie.css">
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-users" style="line-height: 48px;padding-left: 2px;"></i>Edit Trainer</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Edit Trainer</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
                              <form action="<?=site_url('admin/trainers/editTrainer')?>" method="post" id="edit-form" enctype="multipart/form-data">
								  <input type="hidden" name="trainer_id" value="<?=$trainer->trainer_id?>">
								  <div class="row">
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">First name</label>
	                                    <input type="text" class="form-control" name="trainer_name_english" value="<?=$trainer->trainer_name_english?>" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Second name</label>
	                                    <input type="text" class="form-control" id="name-arabic" value="<?=$trainer->trainer_name_arabic?>" name="trainer_name_arabic" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'>
	                                 </div>
									 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">First name ( Arabic )</label>
	                                    <input type="text" class="form-control" name="first_arabic" id="first_arabic" value="<?=$trainer->arabic_first?>" required>
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Second name ( Arabic )</label>
	                                    <input type="text" class="form-control" name="second_arabic" id="second_arabic" value="<?=$trainer->arabic_second?>">
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Gender</label>
	                                    <select class="form-control" name="trainer_gender">
	                                        <option value="Male" <?php if($trainer->trainer_gender == 'Male'){ ?>selected<?php } ?>>Male</option>
	                                        <option value="Female" <?php if($trainer->trainer_gender == 'Female'){ ?>selected<?php } ?>>Female</option>
	                                        <option value="Other" <?php if($trainer->trainer_gender == 'Other'){ ?>selected<?php } ?>>Other</option>
	                                    </select>
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCity">CPR</label>
	                                    <input type="text" class="form-control" name="trainer_cpr" value="<?=$trainer->trainer_cpr?>">
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Education</label>
	                                    <input type="text" name="trainer_education" class="form-control" value="<?=$trainer->trainer_education?>">
	                                 </div>
									 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Education ( Arabic )</label>
	                                    <input type="text" name="trainer_education_arabic" id="trainer_education_arabic" class="form-control" value="<?=$trainer->education_arabic?>">
	                                 </div>
									 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Experience</label>
	                                    <input type="text" name="trainer_experience" class="form-control" value="<?=$trainer->trainer_experience?>">
	                                 </div>
									 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Experience ( Arabic )</label>
	                                    <input type="text" name="trainer_experience_arabic" id="trainer_experience_arabic" class="form-control" value="<?=$trainer->experience_arabic?>">
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputAddress">Mobile number</label>
	                                    <input type="text" name="trainer_mobile" id="mobile-number" class="form-control" value="<?=$trainer->trainer_mobile?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Email address</label>
	                                    <input type="email" name="trainer_email" id="email" class="form-control" value="<?=$trainer->trainer_email?>">
	                                 </div>
									 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCity">Address</label>
	                                    <textarea name="trainer_contact" rows="3" cols="80" class="form-control"><?=$trainer->trainer_contact?></textarea>
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCity">Trainer image ( Choose image if you wish to change the image )( 400px * 400px )</label>
	                                    <input type="file" name="image" id="upload" class="form-control">
	                                 </div>
									 <div class="col-md-12 col-sm-12 form-group">
					                   <div class="upload-div" style="display:none;">
					                     <div id="upload-demo"></div>
					                     <div class="col-12 text-center">
					                       <a href="#" class="btn-fill-lg bg-blue-dark btn-hover-yellow" style="border-radius : 5px;" id="crop-button">Crop</a>
					                     </div>
					                   </div>
					                   <div class="upload-result" id="upload-result" style="display : none; margin-bottom:10px;">

					                   </div>
					                   <input type="hidden" name="image" id="ameimg" >
					                 </div>
	                                 <div class="form-group col-sm-4">
										 <button type="submit" class="btn btn-primary" id="submit-button-edit">Update</button>
	                                 </div>
	                              </div>
                              </form>
                           </div>
                        </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
      <?php $this->load->view('admin/includes/table-script.php'); ?>
	  <script src="<?=base_url()?>assets/plugins/image-crop/croppie.js"></script>
	  <script type="text/javascript">
		  $uploadCrop = $('#upload-demo').croppie({
			enableExif: true,
			viewport: {
				width: 400,
				height: 400,
				type: 'rectangle'
			},
			boundary: {
				width: 800,
				height: 800
			}
		  });


		 $('#upload').on('change', function () {
		  $("#submit-button").css("display", "none");
		  var file = $("#upload")[0].files[0];
		  var val = file.type;
		  var type = val.substr(val.indexOf("/") + 1);
		  if (type == 'png' || type == 'jpg' || type == 'jpeg') {

			$("#current-image").css("display", "none");
			$("#submit-button").css("display", "none");

			$(".upload-div").css("display", "block");
			$("#submit-button").css("display", "none");
			var reader = new FileReader();
			  reader.onload = function (e) {
				$uploadCrop.croppie('bind', {
				  url: e.target.result
				}).then(function(){
				  console.log('jQuery bind complete');
				});

			  }
			  reader.readAsDataURL(this.files[0]);
		  }
		  else {
			alert('This file format is not supported.');
			document.getElementById("upload").value = "";
			$("#upload-result").css("display", "none");
			$("#submit-button").css("display", "none");
			$("#current-image").css("display", "block");
			$('#ameimg').val('');
		  }
		});


		$('#crop-button').on('click', function (ev) {
			$("#submit-button").css("display", "block");
		  $uploadCrop.croppie('result', {
			type: 'canvas',
			size: 'viewport'
		  }).then(function (resp) {
			html = '<img src="' + resp + '" />';
			$("#upload-result").html(html);
			$("#upload-result").css("display", "block");
			$(".upload-div").css("display", "none");
			$("#submit-button").css("display", "block");
			$('#ameimg').val(resp);
		  });
		});
		  $('#edit-form').on('submit', function(e){
			  e.preventDefault();
			  toastr.clear();
			  $('#submit-button-edit').attr('disabled',true);

			  var first = $('#first_arabic').val();
  			  var second = $('#second_arabic').val();
			  var education = $('#trainer_education_arabic').val();
  			  var experience = $('#trainer_experience_arabic').val();
  			  if (arabicCheck(first) && arabicCheck(second) && arabicCheck(education) && arabicCheck(experience)) {
				  var mobile = $('#mobile-number').val();
				  var email = $('#email').val();
				  var arabic = '';
				  if (mobile.length < 8 || mobile.length > 12) {
					  toastr.error('Mobile number should contains 8 - 12 numbers');
					  $('#submit-button-edit').attr('disabled',false);
				  }
				  else {
					  $.ajax({
	                    method: "POST",
	                    url: "<?php echo site_url('admin/trainers/editValidation');?>",
	                    dataType : "json",
	                    data : { mobile : mobile , email : email , trainer_id : <?=$trainer->trainer_id?> },
	                    success : function( data ){
	                      if (data.status)
	                      {
	                          document.getElementById("edit-form").submit();
	                      }
	                      else {
							  toastr.error(data.message,"");
							  $('#submit-button-edit').attr('disabled',false);
	                      }
	                    }
	                  });
				  }
			  }
			  else {
				  if (!arabicCheck(first)) {
					  toastr.error('Please check arabic first name',"");
				  }
				  if (!arabicCheck(second)) {
					  toastr.error('Please check arabic second name',"");
				  }
				  if (!arabicCheck(education)) {
					  toastr.error('Please check arabic education',"");
				  }
				  if (!arabicCheck(experience)) {
					  toastr.error('Please check arabic experience',"");
				  }
				  $('#submit-button-edit').attr('disabled',false);
			  }
		  });
		  function arabicCheck(arabic)
  		  {
  			if (arabic == '') {
  				return true;
  			}
  			else {
  				var string = arabic.replace(/\s/g,'');
  				var isArabic =  /[\u0600-\u06FF\u0750-\u077F]/;
  				if (isArabic.test(string)){
  					 return true;
  				}
  				else {
  					return false;
  				}
  			}
  		  }
	  </script>
   </body>
</html>
