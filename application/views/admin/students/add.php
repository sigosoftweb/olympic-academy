<!DOCTYPE html>

<html>

   <head>

      <title><?=$title?></title>

      <?php $this->load->view('admin/includes/includes.php'); ?>

	  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/image-crop/croppie.css">

   </head>

   <body class="solid-bg-6">

      <div class="mask">

         <div id="loader"></div>

      </div>

      <div id="wrap">

         <div class="row">

            <?php $this->load->view('admin/includes/sidebar.php'); ?>

            <div id="content" class="col-md-12">

               <div class="pageheader">

                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i>Add Student</h2>

                  <div class="breadcrumbs">

                     <ol class="breadcrumb">

                        <li>Olymbic</li>

                        <li>Add Student</li>

                     </ol>

                  </div>

               </div>

               <div class="main">

                  <div class="row">

                     <div class="col-md-12">

                        <section class="tile color transparent-black">

                           <div class="tile-body">

                              <form action="<?=site_url('admin/students/addStudent')?>" method="post" id="add-form" enctype="multipart/form-data">

								  <div class="row">

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCountry">Full name</label>

	                                    <input type="text" class="form-control" name="stu_name_english" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCountry">Family name</label>

	                                    <input type="text" class="form-control" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' name="stu_name_arabic">

	                                 </div>

									 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCountry">Full name ( Arabic )</label>

	                                    <input type="text" class="form-control" name="first_arabic" id="first_arabic" required>

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCountry">Family name ( Arabic )</label>

	                                    <input type="text" class="form-control" name="second_arabic" id="second_arabic">

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCountry">Gender</label>

	                                    <select class="form-control" name="stu_gender">

	                                        <option value="Male">Male</option>

	                                        <option value="Female">Female</option>

	                                        <option value="Other">Other</option>

	                                    </select>

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="stu_timezone">Timezone</label>

	                                    <select class="form-control" name="stu_timezone" required="">

	                                        <option value="">None</option>
											<?php foreach ($timezones as $timezone) { ?>
												<option value="<?= $timezone?>"><?= $timezone?></option>
											<?php } ?>

	                                    </select>

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCity">CPR</label>

	                                    <input type="text" class="form-control" name="stu_cpr">

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputAddress">Date Of Birth</label>

	                                    <input type="date" name="stu_dob" class="form-control">

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCountry">Education</label>

	                                    <input type="text" name="stu_education" class="form-control">

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCity">Address</label>

	                                    <textarea name="stu_contact" rows="2" cols="80" class="form-control"></textarea>

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputAddress">Mobile number</label>

	                                    <input type="text" name="stu_mobile" id="mobile-number" class="form-control"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCountry">Email address</label>

	                                    <input type="email" name="stu_email" id="stu_email" class="form-control">

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCity">Student image ( 400px * 400px )</label>

	                                    <input type="file" id="upload" class="form-control">

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputAddress">Password</label>

	                                    <input type="text" name="stu_password" id="password" class="form-control" value="olympic" required>

	                                 </div>

									 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCity">Student cv</label>

	                                    <input type="file" name="cv" class="form-control">

	                                 </div>

									 <div class="col-md-12 col-sm-12 form-group">

					                   <div class="upload-div" style="display:none;">

					                     <div id="upload-demo"></div>

					                     <div class="col-12 text-center">

					                       <a href="#" class="btn-fill-lg bg-blue-dark btn-hover-yellow" style="border-radius : 5px;" id="crop-button">Crop</a>

					                     </div>

					                   </div>

					                   <div class="upload-result" id="upload-result" style="display : none; margin-bottom:10px;">



					                   </div>

					                   <input type="hidden" name="image" id="ameimg" >

					                 </div>

	                              </div>

	                              <div class="row">

									  <div class="col-md-12">

										  <h4>Subscribe course</h4>

										  <hr>

									  </div>

								  </div>

	                              <div class="row">

									  <div class="form-group col-sm-3">

 	                                    <label for="exampleInputAddress">Choose course</label>

 	                                    <select class="form-control" name="course_id" id="course_id">

											<option value="0">--- No course selected ---</option>

											<?php foreach ($courses as $course) { ?>

												<option value="<?=$course->course_id?>"><?=$course->course_title?></option>

											<?php } ?>

 	                                    </select>

 	                                 </div>

									 <div class="form-group col-sm-3">

 									    <label for="exampleInputCountry">Payment type</label>

 									    <select class="form-control" name="payment_type" id="payment-type">

											<option value="Online">Online payment</option>

											<option value="Tamkeen">Tamkeen</option>

											<option value="By cash">By cash</option>

 									    </select>

 								     </div>

									 <div class="form-group col-sm-3">

									   <label for="exampleInputCountry">Package price</label>

									   <input type="text" class="form-control" id="package-price">

								     </div>

 								     <div class="form-group col-sm-3">

 										<label for="exampleInputCountry">Paid amount</label>

 										<input type="number" class="form-control" name="amount" id="paid-amount">

 								 	 </div>

								  </div>

	                              <div class="row">

	                                 <div class="form-group col-sm-4">

										 <button type="submit" class="btn btn-primary" id="submit-button">Add</button>

	                                 </div>

	                              </div>

                              </form>

                           </div>

                        </section>

                     </div>

                  </div>

               </div>

            </div>

         </div>

      </div>

      <?php $this->load->view('admin/includes/scripts.php'); ?>

	  <script src="<?=base_url()?>assets/plugins/image-crop/croppie.js"></script>

	  <script type="text/javascript">



		  $('#add-form').on('submit', function(e){

			e.preventDefault();

			toastr.clear();

			$('#submit-button').attr('disabled',true);



			var first = $('#first_arabic').val();

			var second = $('#second_arabic').val();

			if (arabicCheck(first) && arabicCheck(second)) {

				var mobile = $('#mobile-number').val();

				var email = $('#stu_email').val();

				var password = $('#password').val();

				/*if (mobile.length < 8 || mobile.length >12 || password.length < 7) {

					if ( mobile.length < 8 || mobile.length >12 ) {

						toastr.error('Mobile number should contain 8 - 12 numbers');

					}

					else {

						toastr.error('Password should contains atleast 6 characters');

					}

					$('#submit-button').attr('disabled',false);

				}*/

				if (password.length < 7) {					

					toastr.error('Password should contains atleast 6 characters');

					$('#submit-button').attr('disabled',false);

				}
				else {

					$.ajax({

                      method: "POST",

                      url: "<?php echo site_url('admin/students/addValidation');?>",

                      dataType : "json",

                      data : { mobile : mobile , email : email },

                      success : function( data ){

                        if (data.status)

                        {

                            document.getElementById("add-form").submit();

                        }

                        else {

  						  toastr.error(data.message,"");

  						  $('#submit-button').attr('disabled',false);

                        }

                      }

                    });

				}

			}

			else {

				if (!arabicCheck(first) && !arabicCheck(second)) {

					toastr.error('Please check arabic first name and second name',"");

					$('#submit-button').attr('disabled',false);

				}

				else {

					if (!arabicCheck(first)) {

						toastr.error('Please check arabic first name',"");

						$('#submit-button').attr('disabled',false);

					}

					else {

						toastr.error('Please check arabic second name',"");

						$('#submit-button').attr('disabled',false);

					}

				}

			}

		});

		function arabicCheck(arabic)

		{

			if (arabic == '') {

				return true;

			}

			else {

				var string = arabic.replace(/\s/g,'');

				var isArabic =  /[\u0600-\u06FF\u0750-\u077F]/;

				if (isArabic.test(string)){

					 return true;

				}

				else {

					return false;

				}

			}

		}



		  $uploadCrop = $('#upload-demo').croppie({

			enableExif: true,

			viewport: {

				width: 400,

				height: 400,

				type: 'rectangle'

			},

			boundary: {

				width: 800,

				height: 800

			}

		  });





		 $('#upload').on('change', function () {

		  $("#submit-button").css("display", "none");

		  var file = $("#upload")[0].files[0];

		  var val = file.type;

		  var type = val.substr(val.indexOf("/") + 1);

		  if (type == 'png' || type == 'jpg' || type == 'jpeg') {



			$("#current-image").css("display", "none");

			$("#submit-button").css("display", "none");



			$(".upload-div").css("display", "block");

			$("#submit-button").css("display", "none");

			var reader = new FileReader();

			  reader.onload = function (e) {

				$uploadCrop.croppie('bind', {

				  url: e.target.result

				}).then(function(){

				  console.log('jQuery bind complete');

				});



			  }

			  reader.readAsDataURL(this.files[0]);

		  }

		  else {

			alert('This file format is not supported.');

			document.getElementById("upload").value = "";

			$("#upload-result").css("display", "none");

			$("#submit-button").css("display", "none");

			$("#current-image").css("display", "block");

			$('#ameimg').val('');

		  }

		});





		$('#crop-button').on('click', function (ev) {

			$("#submit-button").css("display", "block");

		  $uploadCrop.croppie('result', {

			type: 'canvas',

			size: 'viewport'

		  }).then(function (resp) {

			html = '<img src="' + resp + '" />';

			$("#upload-result").html(html);

			$("#upload-result").css("display", "block");

			$(".upload-div").css("display", "none");

			$("#submit-button").css("display", "block");

			$('#ameimg').val(resp);

		  });

		});



		  $('#course_id').on('change', function() {

	        var course_id = this.value;

	        if (course_id == '0') {

	            $('#package-price').val('');

				$("#paid-amount").prop('required',false);

	        }

	        else {

				$("#paid-amount").prop('required',true);

	            $.ajax({

	                method: "POST",

	                url: "<?php echo site_url('admin/students/getCourse');?>",

	                dataType : "json",

	                data : { course_id : course_id },

	                success : function( data ){

	                    $('#package-price').val(data.course_sale_price);

	                }

	            });

	        }

	    });

	  </script>

   </body>

</html>

