<!DOCTYPE html>

<html>

   <head>

      <title><?=$title?></title>

      <?php $this->load->view('admin/includes/includes.php'); ?>

      <link rel="stylesheet" href="<?=base_url()?>assets/plugins/image-crop/croppie.css">

   </head>

   <body class="solid-bg-6">

      <div class="mask">

         <div id="loader"></div>

      </div>

      <div id="wrap">

         <div class="row">

            <?php $this->load->view('admin/includes/sidebar.php'); ?>

            <div id="content" class="col-md-12">

               <div class="pageheader">

                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i>Edit Student</h2>

                  <div class="breadcrumbs">

                     <ol class="breadcrumb">

                        <li>Olymbic</li>

                        <li>Edit Student</li>

                     </ol>

                  </div>

               </div>

               <div class="main">

                  <div class="row">

                     <div class="col-md-12">

                        <section class="tile color transparent-black">

                           <div class="tile-body">

                              <form action="<?=site_url('admin/students/editStudent')?>" method="post" id="edit-form" enctype="multipart/form-data">

								  <input type="hidden" name="stu_id" value="<?=$student->stu_id?>">

								  <div class="row">

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCountry">Full name</label>

	                                    <input type="text" class="form-control" name="stu_name_english" value="<?=$student->stu_name_english?>" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCountry">Family name</label>

	                                    <input type="text" class="form-control" value="<?=$student->stu_name_arabic?>" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' name="stu_name_arabic">

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCountry">Gender</label>

	                                    <select class="form-control" name="stu_gender">

	                                        <option value="Male" <?php if($student->stu_gender == 'Male'){ ?>selected<?php } ?>>Male</option>

	                                        <option value="Female" <?php if($student->stu_gender == 'Female'){ ?>selected<?php } ?>>Female</option>

	                                        <option value="Other" <?php if($student->stu_gender == 'Other'){ ?>selected<?php } ?>>Other</option>

	                                    </select>

	                                 </div>

	                                <div class="form-group col-sm-4">

	                                    <label for="stu_timezone">Gender</label>

	                                    <select class="form-control" name="stu_timezone" required>
	                                    	<option value="">None</option>
											<?php foreach ($timezones as $timezone) { ?>
												<option value="<?= $timezone?>" <?php if($student->stu_timezone == $timezone){ echo 'selected'; } ?> ><?= $timezone?></option>
											<?php } ?>

	                                    </select>
	                                </div>


	                              </div>

	                              <div class="row">

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCity">CPR</label>

	                                    <input type="text" class="form-control" name="stu_cpr" value="<?=$student->stu_cpr?>">

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputAddress">Date Of Birth</label>

	                                    <input type="date" name="stu_dob" class="form-control" value="<?=$student->stu_dob?>">

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCountry">Education</label>

	                                    <input type="text" name="stu_education" class="form-control" value="<?=$student->stu_education?>">

	                                 </div>

	                              </div>

	                              <div class="row">

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCity">Address</label>

	                                    <textarea name="stu_contact" rows="3" cols="80" class="form-control"><?=$student->stu_contact?></textarea>

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputAddress">Mobile number</label>

	                                    <input type="text" name="stu_mobile" id="mobile-number" class="form-control" value="<?=$student->stu_mobile?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>

	                                 </div>

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCountry">Email address</label>

	                                    <input type="email" name="stu_email" class="form-control" value="<?=$student->stu_email?>">

	                                 </div>

	                              </div>

	                              <div class="row">

	                                 <div class="form-group col-sm-4">

	                                    <label for="exampleInputCity">Student image ( Choose image if you wish to change the image )( 400px * 400px )</label>

	                                    <input type="file" name="image" id="upload" class="form-control">

	                                 </div>

									 <div class="col-md-12 col-sm-12 form-group">

					                   <div class="upload-div" style="display:none;">

					                     <div id="upload-demo"></div>

					                     <div class="col-12 text-center">

					                       <a href="#" class="btn-fill-lg bg-blue-dark btn-hover-yellow" style="border-radius : 5px;" id="crop-button">Crop</a>

					                     </div>

					                   </div>

					                   <div class="upload-result" id="upload-result" style="display : none; margin-bottom:10px;">



					                   </div>

					                   <input type="hidden" name="image" id="ameimg" >

					                 </div>

	                              </div>

	                              <div class="row">

	                                 <div class="form-group col-sm-4">

										 <button type="submit" class="btn btn-primary" id="submit-button-edit">Update</button>

	                                 </div>

	                              </div>

                              </form>

                           </div>

                        </section>

                     </div>

                  </div>

               </div>

            </div>

         </div>

      </div>

      <?php $this->load->view('admin/includes/scripts.php'); ?>

      <script src="<?=base_url()?>assets/plugins/image-crop/croppie.js"></script>

	  <script type="text/javascript">

		  $uploadCrop = $('#upload-demo').croppie({

			enableExif: true,

			viewport: {

				width: 400,

				height: 400,

				type: 'rectangle'

			},

			boundary: {

				width: 800,

				height: 800

			}

		  });





		 $('#upload').on('change', function () {

		  $("#submit-button").css("display", "none");

		  var file = $("#upload")[0].files[0];

		  var val = file.type;

		  var type = val.substr(val.indexOf("/") + 1);

		  if (type == 'png' || type == 'jpg' || type == 'jpeg') {



			$("#current-image").css("display", "none");

			$("#submit-button").css("display", "none");



			$(".upload-div").css("display", "block");

			$("#submit-button").css("display", "none");

			var reader = new FileReader();

			  reader.onload = function (e) {

				$uploadCrop.croppie('bind', {

				  url: e.target.result

				}).then(function(){

				  console.log('jQuery bind complete');

				});



			  }

			  reader.readAsDataURL(this.files[0]);

		  }

		  else {

			alert('This file format is not supported.');

			document.getElementById("upload").value = "";

			$("#upload-result").css("display", "none");

			$("#submit-button").css("display", "none");

			$("#current-image").css("display", "block");

			$('#ameimg').val('');

		  }

		});





		$('#crop-button').on('click', function (ev) {

			$("#submit-button").css("display", "block");

		  $uploadCrop.croppie('result', {

			type: 'canvas',

			size: 'viewport'

		  }).then(function (resp) {

			html = '<img src="' + resp + '" />';

			$("#upload-result").html(html);

			$("#upload-result").css("display", "block");

			$(".upload-div").css("display", "none");

			$("#submit-button").css("display", "block");

			$('#ameimg').val(resp);

		  });

		});

		  $('#edit-form').on('submit', function(e){

			  e.preventDefault();

			  $('#submit-button').attr('disabled',true);



			  var mobile = $('#mobile-number').val();

			  var arabic = '';

			  if (mobile.length < 8 || mobile.length > 12) {

				  toastr.error('Mobile number should contains 8 - 12  numbers');

				  $('#submit-button-edit').attr('disabled',false);

			  }

			  else {

				  var isArabic = /^([\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufbc1]|[\ufbd3-\ufd3f]|[\ufd50-\ufd8f]|[\ufd92-\ufdc7]|[\ufe70-\ufefc]|[\ufdf0-\ufdfd]|[ ])*$/g;

			      if (isArabic.test(arabic)){

			           document.getElementById("edit-form").submit();

			      }

			      else {

			          toastr.error("Field contains Non-arabic letters","");

			          $('#submit-button-edit').attr('disabled',false);

			      }

			  }

		  });

		  $('#upload').on('change', function () {

		    var file = $("#upload")[0].files[0];

		    var val = file.type;

		    var type = val.substr(val.indexOf("/") + 1);

		    if (type == 'png' || type == 'jpg' || type == 'jpeg') {

		      var reader = new FileReader();

		        reader.onload = function (e) {

		          $uploadCrop.croppie('bind', {

		            url: e.target.result

		          }).then(function(){

		            console.log('jQuery bind complete');

		          });

		        }

		        reader.readAsDataURL(this.files[0]);

		    }

		    else {

		      document.getElementById("upload").value = "";

			  toastr.error("Failed to upload image, the format is not supported","");

		    }

		  });

	  </script>

   </body>

</html>

