<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('admin/includes/includes.php'); ?>
	  <?php $this->load->view('admin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i> Students</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Students</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">

		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">

		                     </div>
		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom my-table" id="datatable">
								   <thead>
	  		                           <tr>
										 <th width="10%">Image</th>
	  		                             <th width="10%">Name</th>
										 <th width="10%">Contact</th>
	  		                             <th width="10%">Gender</th>
										 <th width="10%">DOB</th>
	  		                             <th width="10%">Education</th>
										 <th width="10%">Address</th>
										 <th width="10%">Approve</th>
	  		                           </tr>
  		                           </thead>
								 <tbody>
								 	<?php foreach ($students as $stu) { ?>
								 		<tr>
								 			<td>
												<?php if ($stu->stu_image != '') { ?>
													<img src="<?=base_url() . $stu->stu_image?>" width="100px" height="100px">
												<?php }
												else { ?>
													<img src="<?=base_url() . 'uploads/students/user.png'?>" width="100px" height="100px">
												<?php } ?>
											</td>
											<td><?=$stu->stu_name_english . ' ' . $stu->stu_name_arabic?></td>
											<td><?=$stu->stu_email . '<br>' . $stu->stu_mobile?></td>
											<td><?=$stu->stu_gender?></td>
											<td><?=$stu->stu_dob?></td>
											<td><?=$stu->stu_education?></td>
											<td><?=$stu->stu_contact?></td>
											<td>
												<a class="btn btn-success btn-xs" onclick="return confirm('Are you sure?')" href="<?=site_url('admin/students/approve/'.$stu->stu_id)?>" style="margin-bottom:5px;">Approve</a><br>
												<a class="btn btn-danger btn-xs" onclick="return confirm('Are you sure?')" href="<?=site_url('admin/students/reject/'.$stu->stu_id)?>">Reject</a>
											</td>
								 		</tr>
								 	<?php } ?>
								 </tbody>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <?php $this->load->view('admin/includes/scripts.php'); ?>
	  <?php $this->load->view('admin/includes/table-script.php'); ?>
   </body>
</html>
