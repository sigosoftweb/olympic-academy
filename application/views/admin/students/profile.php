<!DOCTYPE html>

<html>

   <head>
      <title><?=$title?></title>
      <?php $this->load->view('admin/includes/includes.php'); ?>
      <?php $this->load->view('admin/includes/table-css.php'); ?>
      <link rel="stylesheet" href="<?=base_url()?>assets/plugins/image-crop/croppie.css">
   </head>

   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>

      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i><?=$student->stu_name_english?></h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Student profile</li>
                     </ol>
                  </div>
               </div>
			   <div class="main">
       <!-- row -->
            <div class="row">
              <!-- col 4 -->
              <div class="col-md-4">
              <!-- tile -->
                <section class="tile transparent">
                  <div class="tile-widget color transparent-white rounded-top-corners">

                    <div class="user-card">

                      <h3><?=$student->stu_name_english?></h3>

                      <ul class="profile-controls inline">

                        <li class="avatar">

          							<?php if ($student->stu_image == '') { ?>

          								<img src="<?=base_url() . 'uploads/students/user.png'?>" alt="" class="img-circle" width="100px">

          							<?php }else { ?>

          								<img src="<?=base_url() . $student->stu_image?>" alt="" class="img-circle" width="200px">

          							<?php } ?>
          						</li>
                      </ul>
                    </div>
                  </div>

                  <!-- /tile widget -->
                  <!-- tile body -->

                  <div class="tile-body color transparent-black textured rounded-bottom-corners">

                    <ul class="inline divided social-feed">

                      <li>

                        <h4>126</h4>

                        Tweets

                      </li>

                      <li>

                        <h4>324</h4>

                        Following

                      </li>

                    </ul>

                  </div>

                  <!-- /tile body -->
                </section>
                <section class="tile">
                  <div class="tile-header">

                  <h1><strong>Subscribed</strong> Packages</h1>

                    <div class="controls">

                      <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a>

                      <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>

                      <a href="#" class="remove"><i class="fa fa-times"></i></a>

                    </div>

                  </div>

                  <!-- /tile header -->
                  <!-- tile body -->

                  <div class="tile-body">

                    <ul class="project-list">

                      <li>

                        <div class="col-md-3 project-name">

                          Minimal

                        </div>

                        <div class="col-md-9">

                          <div class="progress-info">

                            <div class="percent">20%</div>

                          </div>

                          <div class="progress">

                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">

                              <span class="sr-only">20% Complete</span>

                            </div>

                          </div>

                        </div>

                      </li>

                      <li>

                        <div class="col-md-3 project-name">

                          Minoral

                        </div>

                        <div class="col-md-9">

                          <div class="progress-info">

                            <div class="percent">36%</div>

                          </div>

                          <div class="progress">

                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 36%">

                              <span class="sr-only">36% Complete</span>

                            </div>

                          </div>

                        </div>
                      </li>
                    </ul>

                  </div>

                </section>

              </div>
              <div class="col-md-8">
                <section class="tile transparent">
                  <div class="tile-widget nopadding color transparent-black rounded-top-corners">

                    <!-- Nav tabs -->

                    <ul class="nav nav-tabs tabdrop">

                      <li class="active"><a href="#feed-tab" data-toggle="tab">Profile</a></li>

                      <li><a href="#tasks-tab" data-toggle="tab">Subscribed packages</a></li>

                      <li><a href="#settings-tab" data-toggle="tab">Payments</a></li>

                    </ul>

                    <!-- / Nav tabs -->

                  </div>

                  <!-- /tile widget -->



                  <!-- tile body -->

                  <div class="tile-body tab-content rounded-bottom-corners">



                    <!-- Tab panes -->

                    <div id="feed-tab" class="tab-pane fade in active">

						<div class="row">

                          <div class="form-group col-md-12 legend">

                              <button type="button" class="btn btn-primary margin-bottom-20 pull-right" id="edit-button">Edit</button>

                            <h4><strong>Student</strong> Profile</h4>

                            <p></p>

                          </div>

                        </div>

                        <input type="hidden" name="edit" id="edit" value="0">

						<form action="<?=site_url('admin/students/updateProfile')?>" method="post" id="edit-form" enctype="multipart/form-data">

  						  <input type="hidden" name="stu_id" value="<?=$student->stu_id?>">

  						  <div class="row">

  							 <div class="form-group col-sm-6">

  								<label for="exampleInputCountry">Full name</label>

  								<input type="text" class="form-control" name="stu_name_english" id="name-english" value="<?=$student->stu_name_english?>" readonly onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>

  							 </div>

  							 <div class="form-group col-sm-6">

  								<label for="exampleInputCountry">Family name</label>

  								<input type="text" class="form-control" id="name-arabic" value="<?=$student->stu_name_arabic?>" name="stu_name_arabic" readonly>

  							 </div>

							 <div class="form-group col-sm-6">

								<label for="exampleInputCountry">Full name ( Arabic )</label>

								<input type="text" class="form-control" name="first_arabic" id="first_arabic" value="<?=$student->first_arabic?>" required readonly>

							 </div>

							 <div class="form-group col-sm-6">

								<label for="exampleInputCountry">Family name ( Arabic )</label>

								<input type="text" class="form-control" name="second_arabic" id="second_arabic" value="<?=$student->second_arabic?>" readonly>

							 </div>

  							 <div class="form-group col-sm-6">

  								<label for="exampleInputCity">CPR</label>

  								<input type="text" class="form-control" name="stu_cpr" id="stu_cpr" value="<?=$student->stu_cpr?>" readonly>

  							 </div>

  							 <div class="form-group col-sm-6">

  								<label for="exampleInputAddress">Date Of Birth</label>

  								<input type="date" name="stu_dob" id="stu_dob" class="form-control" value="<?=$student->stu_dob?>" readonly>

  							 </div>

  							 <div class="form-group col-sm-6">

  								<label for="exampleInputCountry">Education</label>

  								<input type="text" name="stu_education" id="stu_education" class="form-control" value="<?=$student->stu_education?>" readonly>

  							 </div>

                 <div class="form-group col-sm-6">

                  <label for="exampleInputCountry">Timezone</label>

                  
                  <select class="form-control" name="stu_timezone" required="" id="stu_timezone" readonly>

                    <option value="">None</option>
                    <?php foreach ($timezones as $timezone) { ?>
                        <option value="<?= $timezone?>" <?php if($student->stu_timezone==$timezone) { ?>selected <?php } ?>><?= $timezone?></option>
                    <?php } ?>

                  </select>

                 </div>

  							 <div class="form-group col-sm-6">

  								<label for="exampleInputCity">Address</label>

  								<textarea name="stu_contact" id="stu_contact" rows="3" cols="80" class="form-control" readonly><?=$student->stu_contact?></textarea>

  							 </div>

  							 <div class="form-group col-sm-6">

  								<label for="exampleInputAddress">Mobile number</label>

  								<input type="text" name="stu_mobile" id="mobile-number" class="form-control" value="<?=$student->stu_mobile?>" readonly onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>

  							 </div>

  							 <div class="form-group col-sm-6">

  								<label for="exampleInputCountry">Email address</label>

  								<input type="email" name="stu_email" id="stu_email" class="form-control" value="<?=$student->stu_email?>" readonly>

  							 </div>

							 <div class="form-group col-sm-6">

	                            <label for="exampleInputCity">Student cv</label>

	                            <input type="file" name="cv" class="form-control">

	                         </div>

  						  </div>

  						  <div class="row" id="image-row" style="display:none;">

  							  <div class="form-group col-sm-4">
                      <label for="exampleInputCity">Student image ( Choose image if you wish to change the image )( 400px * 400px )</label>
                      <input type="file" id="upload" class="form-control">
                   </div>
							     <div class="col-md-12 col-sm-12 form-group">
  					          <div id="current-image">
                        <img src="<?=base_url() . $student->stu_image?>" height="400px" width="400px">
                      </div>
  	                  <div class="upload-div" style="display:none;">
                         <div id="upload-demo"></div>
                         <div class="col-12 text-center">
                           <a href="#" class="btn-fill-lg bg-blue-dark btn-hover-yellow" style="border-radius : 5px;" id="crop-button">Crop</a>
                         </div>
                       </div>
                       <div class="upload-result" id="upload-result" style="display : none; margin-bottom:10px;">

                       </div>
                       <input type="hidden" name="image" id="ameimg" >
	                 </div>

  						  </div>

  						  <div class="row" id="update-button" style="display:none;">
    							 <div class="form-group col-sm-6">
    								 <button type="submit" class="btn btn-primary" id="submit-button-edit">Update</button>
    							 </div>
  						  </div>

  					  </form>
          </div>
          <div id="tasks-tab" class="tab-pane fade in">

	          <div class="row">
                <div class="form-group col-md-12 legend">

		               <button type="button" class="btn btn-primary margin-bottom-20 pull-right" data-toggle="modal" data-target="#subscribe-package">Subscribe package</button>

                  <h4><strong>Subscribed</strong> Packages</h4>

                  <p></p>

                </div>
            </div>

						<div class="row">

							<div class="col-md-12">

								<table class="table table-datatable table-custom">

									<thead>

										<th width="50%">Course</th>

										<th width="30%">Subscribed at</th>

										<th width="10%">View</th>

									</thead>

									<tbody>

										<?php foreach ($courses as $course) { ?>

											<tr>

												<td><?=$course->course_title?></td>

												<td><?=date('d/m/Y h:i A',strtotime($course->date))?></td>

												<td>View</td>

											</tr>

										<?php } ?>

									</tbody>

								</table>

							</div>

						</div>

						<div class="modal fade" id="subscribe-package" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">

						  <div class="modal-dialog">

							<div class="modal-content">

							  <div class="modal-header">

								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>

								<h3 class="modal-title" id="modalConfirmLabel"><strong>Subscribe</strong> Course</h3>

							  </div>

							  <div class="modal-body">

								<form role="form" id="subscribe-form" action="<?=site_url('admin/students/subscribeCourse')?>" method="post">

								   <input type="hidden" name="stu_id" value="<?=$student->stu_id?>">

								   <div class="row">

									 <div class="form-group col-sm-12 col-md-12">

										<label for="exampleInputCountry">Choose course</label>

										<select class="form-control" id="course_id" name="course_id">

											<option value="">-- Select course -- </option>

										  <?php foreach ($available as $cour) { ?>

											  <option value="<?=$cour->course_id?>"><?=$cour->course_title?></option>

										  <?php } ?>

										</select>

									 </div>

								    <div class="form-group col-sm-12 col-md-12">

									    <label for="exampleInputCountry">Package price</label>

									    <input type="text" class="form-control" id="package-price" readonly>

								    </div>

								    <div class="form-group col-sm-12 col-md-12">

										<label for="exampleInputCountry">Paid amount</label>

										<input type="text" class="form-control" name="amount" required>

								 	</div>

								  </div>
							  </div>

							  <div class="modal-footer">

								<button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>

								<button type="submit" class="btn btn-green" id="submit-button">Save changes</button>

							  </div>

							 </form>

							</div>

						  </div>

						</div>

					</div>

          <div id="settings-tab" class="tab-pane fade in">
						<div class="row">

  						<div class="form-group col-md-12 legend">

  						  <h4><strong>Students</strong> Payments</h4>

  						  <p></p>

  						</div>

  					  </div>

  					  <div class="row">

  						  <div class="col-md-12">

  							  <table class="table table-datatable table-custom">

  								  <thead>

  									  <th width="50%">Course</th>

  									  <th width="30%">Date</th>

									  <th width="30%">Course price</th>

  									  <th width="10%">Paid amount</th>

  								  </thead>

  								  <tbody>

  									  <?php foreach ($courses as $course) { ?>

  										  <tr>

  											  <td><?=$course->course_title?></td>

  											  <td><?=date('d/m/Y h:i A',strtotime($course->date))?></td>

  											  <td><?=$course->course_sale_price?></td>

											  <td><?=$course->amount?></td>

  										  </tr>

  									  <?php } ?>

  								  </tbody>

  							  </table>

  						  </div>

  					  </div>

                    </div>

                    <!-- / Tab panes -->
                  </div>

                  <!-- /tile body -->
                </section>

                <!-- /tile -->

              </div>

              <!-- /col 8 -->
            </div>

            <!-- /row -->
          </div>

            </div>

         </div>

      </div>

      <?php $this->load->view('admin/includes/scripts.php'); ?>

      <?php $this->load->view('admin/includes/table-script.php'); ?>
      <script src="<?=base_url()?>assets/plugins/image-crop/croppie.js"></script>
      <script type="text/javascript">
        $uploadCrop = $('#upload-demo').croppie({
          enableExif: true,
          viewport: {
              width: 400,
              height: 400,
              type: 'rectangle'
          },
          boundary: {
              width: 500,
              height: 500
          }
        });


       $('#upload').on('change', function () {
        $("#submit-button").css("display", "none");
        var file = $("#upload")[0].files[0];
        var val = file.type;
        var type = val.substr(val.indexOf("/") + 1);
        if (type == 'png' || type == 'jpg' || type == 'jpeg') {

          $("#current-image").css("display", "none");
          $("#submit-button").css("display", "none");

          $(".upload-div").css("display", "block");
          $("#submit-button").css("display", "none");
          var reader = new FileReader();
            reader.onload = function (e) {
              $uploadCrop.croppie('bind', {
                url: e.target.result
              }).then(function(){
                console.log('jQuery bind complete');
              });

            }
            reader.readAsDataURL(this.files[0]);
        }
        else {
          alert('This file format is not supported.');
          document.getElementById("upload").value = "";
          $("#upload-result").css("display", "none");
          $("#submit-button").css("display", "none");
          $("#current-image").css("display", "block");
          $('#ameimg').val('');
        }
      });


      $('#crop-button').on('click', function (ev) {
          $("#submit-button").css("display", "block");
        $uploadCrop.croppie('result', {
          type: 'canvas',
          size: 'viewport'
        }).then(function (resp) {
          html = '<img src="' + resp + '" />';
          $("#upload-result").html(html);
          $("#upload-result").css("display", "block");
          $(".upload-div").css("display", "none");
          $("#submit-button").css("display", "block");
          $('#ameimg').val(resp);
        });
      });
      </script>

	    <script type="text/javascript">

	        $('#edit-button').on('click',function(){

			  var edit = $('#edit').val();

			  if (edit == '1') {

				  $('#name-english').attr("readonly", true);

				  $('#name-arabic').attr("readonly", true);

				  $('#first_arabic').attr("readonly", true);

				  $('#second_arabic').attr("readonly", true);

				  $('#stu_cpr').attr("readonly", true);

				  $('#stu_dob').attr("readonly", true);

				  $('#stu_education').attr("readonly", true);
          $('#stu_timezone').attr("readonly", true);

				  $('#stu_contact').attr("readonly", true);

				  $('#mobile-number').attr("readonly", true);

				  $('#stu_email').attr("readonly", true);

				  $('#image-row').css("display", 'none');

				  $('#update-button').css("display", 'none');

				  $('#edit').val('0');

				  $("#edit-button").html('Edit');

			  }

			  else {

				  $('#name-english').attr("readonly", false);

				  $('#name-arabic').attr("readonly", false);

				  $('#first_arabic').attr("readonly", false);

				  $('#second_arabic').attr("readonly", false);

				  $('#stu_cpr').attr("readonly", false);

				  $('#stu_dob').attr("readonly", false);

				  $('#stu_education').attr("readonly", false);
           $('#stu_timezone').attr("readonly", false);

				  $('#stu_contact').attr("readonly", false);

				  $('#mobile-number').attr("readonly", false);

				  $('#stu_email').attr("readonly", false);

				  $('#image-row').css("display", 'block');

				  $('#update-button').css("display", 'block');

				  $('#edit').val('1');

				  $("#edit-button").html('Cancel Edit');

			  }

		  });

		  $('#edit-form').on('submit', function(e){

			  e.preventDefault();

			  toastr.clear();

			  $('#submit-button').attr('disabled',true);



			  var first = $('#first_arabic').val();

  			  var second = $('#second_arabic').val();

  			  if (arabicCheck(first) && arabicCheck(second)) {

				  var mobile = $('#mobile-number').val();

				  var email = $('#stu_email').val();

				  if (mobile.length < 8 || mobile.length >12) {

					  toastr.error('Mobile number should contain 8 - 12 numbers');

					  $('#submit-button-edit').attr('disabled',false);

				  }

				  else {

					  $.ajax({

                        method: "POST",

                        url: "<?php echo site_url('admin/students/editValidation');?>",

                        dataType : "json",

                        data : { mobile : mobile , email : email , stu_id : <?=$student->stu_id?> },

                        success : function( data ){

                          if (data.status)

                          {

                              document.getElementById("edit-form").submit();

                          }

                          else {

    						  toastr.error(data.message,"");

    						  $('#submit-button-edit').attr('disabled',false);

                          }

                        }

                      });

				  }

			  }

			  else {

  				if (!arabicCheck(first) && !arabicCheck(second)) {

  					toastr.error('Please check arabic first name and second name',"");

  					$('#submit-button-edit').attr('disabled',false);

  				}

  				else {

  					if (!arabicCheck(first)) {

  						toastr.error('Please check arabic first name',"");

  						$('#submit-button-edit').attr('disabled',false);

  					}

  					else {

  						toastr.error('Please check arabic second name',"");

  						$('#submit-button-edit').attr('disabled',false);

  					}

  				}

  			  }

		  });

		  function arabicCheck(arabic)

  		  {

  			  if (arabic == '') {

  				  return true;

  			  }

  			  else {

  				  var string = arabic.replace(/\s/g,'');

  				  var isArabic =  /[\u0600-\u06FF\u0750-\u077F]/;

  				  if (isArabic.test(string)){

  					  return true;

  				  }

  				  else {

  					  return false;

  				  }

  			  }

  		  }

		  $('#upload').on('change', function () {

		    var file = $("#upload")[0].files[0];

		    var val = file.type;

		    var type = val.substr(val.indexOf("/") + 1);

		    if (type == 'png' || type == 'jpg' || type == 'jpeg') {

		      var reader = new FileReader();

		        reader.onload = function (e) {

		          $uploadCrop.croppie('bind', {

		            url: e.target.result

		          }).then(function(){

		            console.log('jQuery bind complete');

		          });

		        }

		        reader.readAsDataURL(this.files[0]);

		    }

		    else {

		      document.getElementById("upload").value = "";

			  toastr.error("Failed to upload image, the format is not supported","");

		    }

		  });

		  $('#course_id').on('change', function() {

	        var course_id = this.value;

	        if (course_id == '') {

	            $('#package-price').val('');

	        }

	        else {

	            $.ajax({

	                method: "POST",

	                url: "<?php echo site_url('admin/students/getCourse');?>",

	                dataType : "json",

	                data : { course_id : course_id },

	                success : function( data ){

	                    $('#package-price').val(data.course_sale_price);

	                }

	            });

	        }

	    });

	  </script>

   </body>

</html>

