<!DOCTYPE html>
<html>
   <head>
       <style>
       body {
            font-family: "Roboto", "Arial", sans-serif;
       }

table {
border-collapse: collapse;
}


table, th, td {
border: 1px solid #ddd;
padding-left: 10px;
padding-right: 10px;
    padding: .75rem;
}
       </style>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i>Course Report</h2>
                  <div class="breadcrumbs">
                  </div>
               </div>

               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
								  <div class="row">
  									  <div class="col-sm-12">
  									    
  									  	<h4><b>Course :</b> <?=$course->course_title?><br><br>
  									  	<?php if($date!='All Data') {?>
  									  	<b>Date &nbsp;&nbsp;&nbsp; :</b> <?=$date?><br><br>
  									  	<?php };?>
  									  	<b>Subscribed Students :</b> <?=$subscribed?><br><br>
  									  	<b>Earnings :</b> <?=number_format($earnings,'2')?> BHD<br><br>
  									  	<b>Online Payments:</b> <?=number_format($online,'2')?> BHD  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     <b>Tamkeens:</b> <?=number_format($tamkeen,'2')?> BHD   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     <b>By Cash:</b> <?=number_format($cash,'2')?> BHD</h4><br>
  									  	<hr>
  									  	</div>
  									  	<div class="col-sm-12">
                                            <h4>Subscribed Students</h4>
                                            <table style="width:100%">
                                               <tr>
                                                    <th>Name</th>
                                                    <th>Contact</th>
                                                    <th>Address</th>
                                                </tr>
                                                <?php foreach($students as $student) {?>
                                                <tr>
                                                    <td><?=$student->stu_name_english?> <?=$student->stu_name_arabic?></td>
                                                    <td><?=$student->stu_mobile?><br> <?=$student->stu_email?></td>
                                                    <td><?=$student->stu_contact?></td>
                                                </tr>
                                                <?php };?> 
    	                                    </table>  
    	                              </div>
  									  </div>
                                      
                                      
                           </div>
                        </section>
                     </div>
                  </div>
                  
               </div>
            </div>

         </div>

      </div>


   </body>

</html>

