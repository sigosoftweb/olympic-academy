<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('admin/includes/includes.php'); ?>
   </head>
   <body class="solid-bg-6">

      <div class="mask">

         <div id="loader"></div>

      </div>

      <div id="wrap">

         <div class="row">

            <?php $this->load->view('admin/includes/sidebar.php'); ?>

            <div id="content" class="col-md-12">

               <div class="pageheader">

                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i>Course Report</h2>

                  <div class="breadcrumbs">

                     <ol class="breadcrumb">

                        <li>Olymbic</li>

                        <li>Report</li>

                     </ol>

                  </div>

               </div>

               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
							  <form action="<?=site_url('admin/reports/report')?>" method="post">
								  <div class="row">
  									  <div class="col-sm-12">
  									  	<h4></h4><hr>
  									  </div>
                                     <div class="form-group col-sm-6">
                                       <label for="exampleInputCountry">Select Course</label>
                                       <select class="form-control" name="course_id" required>
                                           <option value="">---Select Course---</option>
                                           <?php foreach($cources as $cource) {?>
                                            <option value="<?=$cource->course_id?>"><?=$cource->course_title?></option>
                                           <?php };?>
                                       </select>
                                     </div>
	                                 <div class="form-group col-sm-3">
	                                    <label for="exampleInputCountry">From</label>
	                                    <input type="date" class="form-control" name="from">
	                                 </div>
									 <div class="form-group col-sm-3">
	                                    <label for="exampleInputCountry">To</label>
	                                    <input type="date" class="form-control" name="to">
	                                 </div>
                                    <div class="form-group col-sm-12">
                                       <button type="submit" class="btn btn-primary" id="submit-button" style="margin-top:15px; float: right;">Submit</button>
                                   </div>
	                              </div>
                              </form>
                           </div>
                        </section>
                     </div>
                  </div>
               </div>
            </div>

         </div>

      </div>

      <?php $this->load->view('admin/includes/scripts.php'); ?>

   </body>

</html>

