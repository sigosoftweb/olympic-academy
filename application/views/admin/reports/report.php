<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('admin/includes/includes.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i>Course Report</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Report</li>
                     </ol>
                  </div>
               </div>

               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
								  <div class="row">
  									  <div class="col-sm-12">
  									      <span style="float:right;font-size:10px;">
                                              <form  method="POST" action="<?=site_url('admin/reports/reportPrint')?>">
                                                  <input type="hidden" name="from" value="<?=$from?>">
                                                  <input type="hidden" name="to" value="<?=$to?>">
                                                  <input type="hidden" name="course_id" value="<?=$course_id?>">
                                                 <button class="btn btn-primary" type="submit" name="submit">Download</button>
                                              </form>
                                         </span><br>
  									  	<h4><b>Course :</b> <?=$course->course_title?><br><br>
  									  	<?php if($date!='All Data') {?>
  									  	<b>Date &nbsp;&nbsp;&nbsp; :</b> <?=$date?><br><br>
  									  	<?php };?>
  									  	<b>Subscribed Students :</b> <?=$subscribed?><br><br>
  									  	<b>Earnings :</b> <?=number_format($earnings,'2')?> BHD<br><br>
  									  	<b>Online Payments:</b> <?=number_format($online,'2')?> BHD  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     <b>Tamkeens:</b> <?=number_format($tamkeen,'2')?> BHD   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     <b>By Cash:</b> <?=number_format($cash,'2')?> BHD</h4><br>
  									  	<hr>
  									  	
  									  	<div class="table-responsive">
                                            <h4>Subscribed Students</h4>
                                            <table  class="table table-striped table-bordered">
                                               <thead> 
                                                 <th>Name</th>
                                                 <th>Contact</th>
                                                 <th>Address</th>
                                               </thead>
                                               <tbody>
                                                  <?php foreach($students as $student) {?>
                                                   <tr>
                                                       <td><?=$student->stu_name_english?> <?=$student->stu_name_arabic?></td>
                                                       <td><?=$student->stu_mobile?><br> <?=$student->stu_email?></td>
                                                       <td><?=$student->stu_contact?></td>
                                                   </tr>
                                                  <?php };?> 
                                               </tbody>
    	                                    </table>  
    	                              </div>
  									  </div>
                                      
                                      
                           </div>
                        </section>
                     </div>
                  </div>
                  
               </div>
            </div>

         </div>

      </div>

      <?php $this->load->view('admin/includes/scripts.php'); ?>

   </body>

</html>

