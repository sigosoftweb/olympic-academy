<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('admin/includes/includes.php'); ?>
      <?php $this->load->view('admin/includes/table-css.php'); ?>

      <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/chosen/css/chosen.min.css">
      <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/chosen/css/chosen-bootstrap.css">
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i>Add Course</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Add Course</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
                              <form action="<?=site_url('admin/courses/addCourse')?>" method="post" id="add-form" enctype="multipart/form-data">
								  <div class="row">
	                                 <div class="form-group col-sm-6">
	                                    <label for="exampleInputCountry">Course title</label>
	                                    <input type="text" class="form-control" name="title" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
	                                 </div>
	                                 <div class="form-group col-sm-6">
	                                    <label for="exampleInputCountry">Details</label>
	                                    <textarea name="description" class="form-control" rows="2" cols="80" required></textarea>
	                                 </div>
	                              </div>
	                              <div class="row">
									  <div class="form-group col-sm-4">
 	                                    <label for="exampleInputAddress">Starting date</label>
 	                                    <input type="date" name="starting_date" class="form-control" required>
 	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Duration</label>
	                                    <input type="text" name="duration" class="form-control" required>
	                                 </div>
									 <div class="form-group col-sm-4">
	                                    <label for="exampleInputAddress">Sale price</label>
	                                    <input type="number" step="any" name="sale_price" class="form-control" required>
	                                 </div>
	                              </div>
	                              <div class="row">
									  <div class="form-group col-sm-4">
 	                                    <label for="exampleInputAddress">Price</label>
 	                                    <input type="number" step="any" name="price" class="form-control" required>
 	                                 </div>

	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Choose trainers</label>
										<select multiple class="chosen-select chosen-transparent form-control" id="input08" name="trainers[]" required>
										  <?php foreach ($trainers as $trainer) { ?>
  											  <option value="<?=$trainer->trainer_id?>" data-badge=""><?=$trainer->trainer_name_english?></option>
  										  <?php } ?>
										</select>
	                                 </div>
									 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCity">Banner image ( 1600px * 800px )</label>
	                                    <input type="file" name="image" id="upload" class="form-control" required>
	                                 </div>
	                              </div>
								  <div class="row">
									  <div class="form-group col-sm-4">
 	                                    <label for="exampleInputAddress">Choose category</label>
 	                                    <select class="form-control" name="cat_id">
											<?php foreach ($categories as $cat) { ?>
												<option value="<?=$cat->cat_id?>"><?=$cat->cat_name?></option>
											<?php } ?>
 	                                    </select>
 	                                 </div>
 	                                 <div class="form-group col-sm-4">
 	                                    <label for="exampleInputAddress">Parent course</label>
 	                                    <select class="form-control" name="sequence_id">
 	                                        <option value="0">No parent</option>
											<?php foreach ($courses as $cou) { ?>
												<option value="<?=$cou->course_id?>"><?=$cou->course_title?></option>
											<?php } ?>
 	                                    </select>
 	                                 </div>
 	                                 <div class="form-group col-sm-4">
 	                                    <label for="exampleInputAddress">Choose Organiser</label>
 	                                    <select multiple class="chosen-select chosen-transparent form-control" name="organisers[]">
											<?php foreach ($organisers as $org) { ?>
												<option value="<?=$org->organiser_id?>"><?=$org->organiser_first . ' ' . $org->organiser_second?></option>
											<?php } ?>
 	                                    </select>
 	                                 </div>
 	                                 <div class="clearfix"></div>
 	                                 <div class="form-group col-sm-4">
 	                                    <label for="exampleInputCity">Student limit</label>
 	                                    <input type="number" name="limit" class="form-control" required>
 	                                 </div>
								  </div>
	                              <div class="row">
	                                 <div class="form-group col-sm-4">
										 <button type="submit" class="btn btn-primary" id="submit-button">Add</button>
	                                 </div>
	                              </div>
                              </form>
                           </div>
                        </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
      <?php $this->load->view('admin/includes/table-script.php'); ?>
      <script src="<?=base_url()?>assets/js/vendor/chosen/chosen.jquery.min.js"></script>
	  <script type="text/javascript">
		  $('#add-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button').attr('disabled',true);
			  document.getElementById("add-form").submit();
		  });
		  $('#upload').on('change', function () {
		    var file = $("#upload")[0].files[0];
		    var val = file.type;
		    var type = val.substr(val.indexOf("/") + 1);
		    if (type == 'png' || type == 'jpg' || type == 'jpeg') {
		      var reader = new FileReader();
		        reader.onload = function (e) {
		          $uploadCrop.croppie('bind', {
		            url: e.target.result
		          }).then(function(){
		            console.log('jQuery bind complete');
		          });
		        }
		        reader.readAsDataURL(this.files[0]);
		    }
		    else {
		      document.getElementById("upload").value = "";
			  toastr.error("Failed to upload image, the format is not supported","");
		    }
		  });
		  $(function(){
			  $(".chosen-select").chosen({disable_search_threshold: 10});
		  });
	  </script>
   </body>
</html>
