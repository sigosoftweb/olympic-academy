<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <style>
         th, td {
         	padding: 15px;
         	text-align: left;
         }
         table, th, td {
         	border: 1px solid black;
         	border-collapse: collapse;
         }
         th {
         	background-color: #d9d9d9;
         }
		 table tr {
			line-height: 14px !important;
		 }
      </style>
   </head>
   <body>
      <div id="container">
         <center>
            <h3 style="text-align: center">ATTENDANCE</h3>
         </center>
         <p style="text-align: left"><strong>Course: </strong><?=$course?></p>
         <p style="text-align: left"><strong>Batch: </strong><?=$batch?></p>
		 <p style="text-align: left"><strong>Date: </strong><?=$date?></p>
         <table autosize="1">
			 <thead>
				 <tr>
					 <th width="10%">Sl. No.</th>
    				 <th width="45%">Student name</th>
    				 <th width="45%">Attendance</th>
				 </tr>
			 </thead>
			 <?php $i=1; foreach ($students as $stu) { ?>
				 <tr>
					 <td><?=$i?></td>
					 <td><?=$stu->stu_name_english?></td>
					 <td>
						 <?php
						 	if($stu->present){ ?>
								Present
							<?php }elseif($stu->absent){ ?>
								Absent
							<?php }else{ ?>
								Late
							<?php }
						 ?>
					 </td>
				 </tr>
			 <?php $i++; } ?>
		 </table>
      </div>
   </body>
</html>
