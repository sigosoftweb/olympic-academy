<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('admin/includes/includes.php'); ?>
      <?php $this->load->view('admin/includes/table-css.php'); ?>
      <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/chosen/css/chosen.min.css">
      <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/chosen/css/chosen-bootstrap.css">
	  <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/datepicker/css/bootstrap-datetimepicker.css">
	  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/image-crop/croppie.css">
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i><?=$course_name?></h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Course Details</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
					  <div class="col-md-12">
 	                   <section class="tile transparent">
 	                     <div class="tile-widget nopadding color transparent-black rounded-top-corners">
 	                       <ul class="nav nav-tabs tabdrop">
 	                         <li <?php if($active == '1'){ ?>class="active"<?php } ?>><a href="<?=site_url('admin/courses/details/1/' . $course_id)?>">Course details</a></li>
 	                         <li <?php if($active == '2'){ ?>class="active"<?php } ?>><a href="<?=site_url('admin/courses/details/2/' . $course_id)?>">Teachers</a></li>
							 <li <?php if($active == '3'){ ?>class="active"<?php } ?>><a href="<?=site_url('admin/courses/details/3/' . $course_id)?>">Sections</a></li>
							 <li <?php if($active == '4'){ ?>class="active"<?php } ?>><a href="<?=site_url('admin/courses/details/4/' . $course_id)?>">Batches</a></li>
 	                         <li <?php if($active == '5'){ ?>class="active"<?php } ?>><a href="<?=site_url('admin/courses/details/5/' . $course_id)?>">Online class</a></li>
							 <li <?php if($active == '6'){ ?>class="active"<?php } ?>><a href="<?=site_url('admin/courses/details/6/' . $course_id)?>">Subscribed students</a></li>
							 <li <?php if($active == '7'){ ?>class="active"<?php } ?>><a href="<?=site_url('admin/courses/details/7/' . $course_id)?>">Exams</a></li>
							 <li <?php if($active == '8'){ ?>class="active"<?php } ?>><a href="<?=site_url('admin/courses/details/8/' . $course_id)?>">Attendance</a></li>
							 <li <?php if($active == '9'){ ?>class="active"<?php } ?>><a href="<?=site_url('admin/courses/details/9/' . $course_id)?>">Assignments</a></li>
 	                       </ul>
 	                     </div>
 	                     <div class="tile-body tab-content rounded-bottom-corners">
 	                       <div id="feed-tab" class="tab-pane fade in ">
 	                         <div class="feed-form">
 	                           <textarea class="form-control" placeholder="What's up dude?" rows="5"></textarea>
 	                           <div class="post-toolbar">
 	                             <a href="#" title="Add File"><i class="fa fa-paperclip"></i></a>
 	                             <a href="#" title="Add Image"><i class="fa fa-camera"></i></a>
 	                             <a href="#" title="Post it!" class="pull-right"><i class="fa fa-share"></i></a>
 	                           </div>
 	                         </div>
 	                       </div>

 	                       <?php if ($active == '1') { ?>
							   <div id="course-details" class="tab-pane fade in <?php if($active == '1'){ ?>active<?php } ?>">
								   <div class="row">
	 	                             <div class="form-group col-md-12 legend">
	 	                               <h4><strong>Course</strong> Details</h4>
	 	                               <p>Manage course details here</p>
	 	                             </div>
	 	                           </div>
								   <form action="<?=site_url('admin/courses/editCourse')?>" method="post" id="course-edit-form" enctype="multipart/form-data">
									   <input type="hidden" name="course_id" value="<?=$course->course_id?>">
	 								  <div class="row">
	 	                                 <div class="form-group col-sm-6">
	 	                                    <label for="exampleInputCountry">Course title</label>
	 	                                    <input type="text" class="form-control" name="title" value="<?=$course->course_title?>" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
	 	                                 </div>
	 	                                 <div class="form-group col-sm-6">
	 	                                    <label for="exampleInputCountry">Description</label>
	 	                                    <textarea name="description" class="form-control" rows="2" cols="80" required><?=$course->course_description?></textarea>
	 	                                 </div>
										 <div class="form-group col-sm-6">
		                                    <label for="exampleInputCountry">Course title ( Arabic )</label>
		                                    <input type="text" class="form-control" id="course-title-arabic" name="title_arabic" value="<?=$course->course_title_arabic?>" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
		                                 </div>
		                                 <div class="form-group col-sm-6">
		                                    <label for="exampleInputCountry">Details ( Arabic )</label>
		                                    <textarea name="description_arabic" id="course-description-arabic" class="form-control" rows="2" cols="80" required><?=$course->course_description_arabic?></textarea>
		                                 </div>
	 	                              </div>
	 	                              <div class="row">
	 									  <div class="form-group col-sm-4">
	  	                                    <label for="exampleInputAddress">Starting date</label>
	  	                                    <input type="date" name="starting_date" class="form-control" value="<?=$course->course_starting_date?>" required>
	  	                                 </div>
	 	                                 <div class="form-group col-sm-4">
	 	                                    <label for="exampleInputCountry">Duration</label>
	 	                                    <input type="text" name="duration" class="form-control" value="<?=$course->course_duration?>" required>
	 	                                 </div>
										 <div class="form-group col-sm-4">
	 	                                    <label for="exampleInputCountry">Duration ( Arabic ) </label>
	 	                                    <input type="text" id="course-duration-arabic" name="duration_arabic" class="form-control" value="<?=$course->course_duration_arabic?>" required>
	 	                                 </div>
	 									 <div class="form-group col-sm-4">
	 	                                    <label for="exampleInputAddress">Induvidual price</label>
	 	                                    <input type="number" step="any" name="sale_price" class="form-control" value="<?=$course->course_sale_price?>" required>
	 	                                 </div>
	 									  <div class="form-group col-sm-4">
	  	                                    <label for="exampleInputAddress">Tamkeen Price</label>
	  	                                    <input type="number" step="any" name="price" class="form-control" value="<?=$course->course_price?>" required>
	  	                                 </div>
										 <div class="form-group col-sm-4">
										   <label for="exampleInputAddress">Choose category</label>
										   <select class="form-control" name="cat_id">
											   <?php foreach ($categories as $cat) { ?>
												   <option value="<?=$cat->cat_id?>" <?php if($course->cat_id == $cat->cat_id){ ?>selected<?php } ?>><?=$cat->cat_name?></option>
											   <?php } ?>
										   </select>
										</div>
										<div class="form-group col-sm-4">
 	                                        <label for="exampleInputAddress">Choose Organiser</label>
 	                                        <select multiple class="chosen-select chosen-transparent form-control" name="organisers[]">
											    <?php foreach ($organisers as $org) { ?>
												<option value="<?=$org->organiser_id?>" <?php if(in_array($org->organiser_id,$corg)){ ?>selected<?php } ?>><?=$org->organiser_first . ' ' . $org->organiser_second?></option>
											    <?php } ?>
 	                                        </select>
 	                                    </div>
 	                                    <div class="form-group col-sm-4">
     	                                    <label for="exampleInputAddress">Parent course</label>
     	                                    <select class="form-control" name="sequence_id">
     	                                        <option value="0">No parent</option>
    											<?php foreach ($courses as $cou) { ?>
    												<option value="<?=$cou->course_id?>" <?php if($cou->course_id == $course->sequence_id){ ?>selected<?php } ?>><?=$cou->course_title?></option>
    											<?php } ?>
     	                                    </select>
     	                                </div>
 	                                    <div class="form-group col-sm-4">
	 	                                    <label for="exampleInputCity">Banner image</label>
	 	                                    <input type="file" name="image" id="upload" class="form-control">
	 	                                 </div>
	 	                                 <div class="form-group col-sm-4">
	 	                                    <label for="exampleInputCity">Student limit</label>
	 	                                    <input type="number" name="limit" class="form-control" value="<?=$course->student_limit?>" required>
	 	                                 </div>
	 	                              </div>
									  <div class="row">										 <div class="col-md-12">     
	                                         <div id="current-image">
	                                              <img src="<?=base_url() . $course->course_banner?>" height="400px" width="600px">
	                                         </div>

	                                        <div class="upload-div" style="display:none;">
	                                          <div id="upload-demo"></div>
	                                          <div class="col-12 text-center">
	                                            <a href="#" class="btn btn-primary btn-flat" style="border-radius : 5px;" id="crop-button">Crop</a>
	                                          </div>
	                                        </div>

	                                        <div class="upload-result text-center" id="upload-result" style="display : none; margin-bottom:10px;">

	                                        </div>
	                                        <input type="hidden" name="image" id="ameimg" >
	                                      </div>
									  </div>
	 	                              <div class="row" style="margin-top:10px;">
	 	                                 <div class="form-group col-sm-4">
	 										 <button type="submit" class="btn btn-primary" id="submit-button-course">Update</button>
	 	                                 </div>
	 	                              </div>
	                               </form>
	 	                       </div>
 	                       <?php } ?>

						   <?php if ($active == '2') { ?>
							   <div class="tab-pane fade in <?php if($active == '2'){ ?>active<?php } ?>">
								   <div class="row">
	 	                             <div class="form-group col-md-12 legend">
										 <button type="button" class="btn btn-primary margin-bottom-20 pull-right" data-toggle="modal" data-target="#add-trainers">Add teacher</button>
	 	                               <h4><strong>Course</strong> Teachers</h4>
	 	                               <p>Manage course teachers here</p>
	 	                             </div>
	 	                           </div>
								   <div class="row">
									   <div class="col-md-12">
										   <table  class="table table-datatable table-custom">
											   <thead>
				  		                           <tr>
													 <th width="10%">Image</th>
				  		                             <th width="10%">Name</th>
													 <th width="10%">Contact</th>
				  		                             <th width="10%">Education</th>
													 <th width="10%">Experience</th>
													 <th width="5%">Status</th>
				  		                           </tr>
			  		                           </thead>
											   <tbody>
												   <?php foreach ($trainers as $trainer) { ?>
													   <tr>
														   <td>
    														   <?php if ($trainer->trainer_image != '') { ?>
    															   <img src="<?=base_url() . $trainer->trainer_image?>" width="70px">
    														   <?php }else { ?>
    															   <img src="<?=base_url() . 'uploads/trainers/user.png'?>" width="70px">
    														   <?php } ?>
    													   </td>
    													   <td><?=$trainer->trainer_name_english . '<br>' . $trainer->trainer_name_arabic?></td>
    													   <td><?=$trainer->trainer_mobile . '<br>' . $trainer->trainer_email?></td>
    													   <td><?=$trainer->trainer_education?></td>
    													   <td><?=$trainer->trainer_experience?></td>
    													   <td>
    														   <?php if ($trainer->ct_status) { ?>
    															   <a class="btn btn-danger btn-xs" href="<?=site_url('admin/courses/statusTeacher/' . $trainer->ct_id . '/0')?>">Block</a><br>Active
    														   <?php }else { ?>
    															   <a class="btn btn-success btn-xs" href="<?=site_url('admin/courses/statusTeacher/' . $trainer->ct_id . '/1')?>">Activate</a><br>Blocked
    														   <?php } ?>
    													   </td>
													   </tr>
												   <?php } ?>
											   </tbody>
					                       </table>
									   </div>
								   </div>
							   </div>
							   <div class="modal fade" id="add-trainers" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> Trainers</h3>
						             </div>
						             <div class="modal-body">
						               <form role="form" id="add-form" action="<?=site_url('admin/courses/addTrainers')?>" method="post">
						 				  <input type="hidden" name="course_id" value="<?=$course_id?>">
						 				  <div class="row">
						 					  <div class="form-group col-sm-12 col-md-12">
						       				   <label for="exampleInputCountry">Choose trainers</label>
						       				   <select multiple class="chosen-select chosen-transparent form-control" id="input08" name="trainers[]" required>
						       					 <?php foreach ($teachers as $trainer) { ?>
						       						 <option value="<?=$trainer->trainer_id?>" data-badge=""><?=$trainer->trainer_name_english?></option>
						       					 <?php } ?>
						       				   </select>
						       				</div>
						 				  </div>

						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						               <button type="submit" class="btn btn-green" id="submit-button">Save changes</button>
						             </div>
						 			</form>
						           </div>
						         </div>
						       </div>
						   <?php } ?>

						   <?php if ($active == '3') { ?>
							   <div class="tab-pane fade in <?php if($active == '3'){ ?>active<?php } ?>">
								   <div class="row">
	 	                             <div class="form-group col-md-12 legend">
										 <button type="button" class="btn btn-primary margin-bottom-20 pull-right" data-toggle="modal" data-target="#add-section">Add section</button>
	 	                               <h4><strong>Course</strong> Sections</h4>
	 	                               <p>Manage course sections here</p>
	 	                             </div>
	 	                           </div>
								   <div class="row">
									   <div class="col-md-12">
										   <table  class="table table-datatable table-custom">
											   <thead>
				  		                           <tr>
													 <th width="10%">Title</th>
				  		                             <th width="30%">Description</th>
				  		                             <th width="30%">Description ( Arabic )</th>
				  		                             <th width="10%">Time</th>
				  		                             <th width="10%">Attachments</th>
													 <th width="10%">Options</th>
				  		                           </tr>
			  		                           </thead>
											   <tbody>
												   <?php foreach ($sections as $section) { ?>
													   <tr>
														   <td><?=$section->section_title . '<br>' . $section->section_title_arabic?></td>
    													   <td><?=$section->section_description?></td>
    													   <td><?=$section->section_description_arabic?></td>
    													   <td><?=date('d/m/Y h:i A',strtotime($section->section_start_time))?></td>
    													   <td>
    													       <button type="button" class="btn btn-primary btn-xs" onclick="add_attachment(<?=$section->cs_id?>)">Add attachment</button></br>
    													       <button type="button" class="btn btn-primary btn-xs" onclick="view_attachment(<?=$section->cs_id?>)">Attachment</button>
    													   </td>
    													   <td>
    														   <?php if ($section->section_status) { ?>
    															   <a class="btn btn-danger btn-xs" href="<?=site_url('admin/courses/statusSection/' . $section->cs_id . '/0')?>">Block</a>
    														   <?php }else { ?>
    															   <a class="btn btn-success btn-xs" href="<?=site_url('admin/courses/statusSection/' . $section->cs_id . '/1')?>">Activate</a>
    														   <?php } ?>
    														   <br>
    														   <button type="button" class="btn btn-primary btn-xs" onclick="return edit(<?=$section->cs_id?>)">Edit</button></br>
														       <a class="btn btn-danger btn-xs" href="<?=site_url('admin/courses/deleteSection/' . $section->cs_id . '/1')?>" onclick="return del_section()">Delete</a></td>
													   </tr>
												   <?php } ?>
												   <?php if(count($sections) == 0){ ?>
												        <tr style="text-align:center;">
												            <td colspan="7">No sections available<td>
												        </tr>
												   <?php } ?>
											   </tbody>
					                       </table>
									   </div>
								   </div>
							   </div>
							   <div class="modal fade" id="add-section" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog madal-lg">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> Sections</h3>
						             </div>
						             <div class="modal-body">
						               <form role="form" id="add-section-form" action="<?=site_url('admin/courses/addSection')?>" method="post" enctype="multipart/form-data">
						 				  <input type="hidden" name="course_id" value="<?=$course_id?>">
						 				  <div class="row">
											  <div class="form-group col-sm-12">
	 	 	                                    <label for="exampleInputCountry">Section title</label>
	 	 	                                    <input type="text" class="form-control" name="title" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
	 	 	                                  </div>
											  <div class="form-group">
						                        <div class="col-sm-12">
													<label class="control-label">Section details</label>
						                          	<textarea name="description" class="form-control" rows="6" cols="80"></textarea>
						                        </div>
						                      </div>
											  <div class="form-group col-sm-12">
	 	 	                                    <label for="exampleInputCountry">Section title ( Arabic )</label>
	 	 	                                    <input type="text" class="form-control" name="title_arabic" id="section-title-arabic"  required>
	 	 	                                  </div>
											  <div class="form-group">
						                        <div class="col-sm-12">
													<label class="control-label">Section details ( Arabic )</label>
						                          	<textarea name="description_arabic" class="form-control" id="section-description-arabic" rows="6" cols="80"></textarea>
						                        </div>
						                      </div>
						                      <div class="form-group col-sm-12 col-md-12">
											 	<label for="exampleInputCountry">Starting time</label>
											 	<input type="text" class="form-control" id="datepicker-section" name="section_start_time">
										  	  </div>
											  <div class="form-group col-sm-12">
	 	 	                                    <label for="exampleInputCountry">Attachment type</label>
	 	 	                                    <select class="form-control" name="type">
													<option value="Nil">No attachment</option>
													<option value="Image">Image</option>
													<option value="Documents">Documents</option>
													<option value="Video">Video</option>
	 	 	                                    </select>
	 	 	                                  </div>
											  <div class="form-group col-sm-12">
	 	 	                                    <label for="exampleInputCountry">Choose attachment</label>
	 	 	                                    <input type="file" name="file" id="upload" class="form-control">
	 	 	                                  </div>
											  <div class="form-group col-sm-12">
	 	 	                                    <label for="exampleInputCountry">Youtube Link</label>
	 	 	                                    <input type="text" class="form-control" name="youtube">
	 	 	                                  </div>
											  <div class="form-group col-sm-12">
	 	 	                                    <label for="exampleInputCountry">Other Link</label>
	 	 	                                    <input type="text" class="form-control" name="other" other>
	 	 	                                  </div>
						 				  </div>
						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						               <button type="submit" class="btn btn-green" id="submit-button-section">Add</button>
						             </div>
						 			</form>
						           </div>
						         </div>
						       </div>
							   <div class="modal fade" id="edit-section" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog madal-lg">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Edit</strong> Sections</h3>
						             </div>
						             <div class="modal-body">
						               <form role="form" id="edit-section-form" action="<?=site_url('admin/courses/editSection')?>" method="post" enctype="multipart/form-data">
						 				  <input type="hidden" name="cs_id" id="cs_id">
						 				  <div class="row">
											  <div class="form-group col-sm-12">
	 	 	                                    <label for="exampleInputCountry">Section title</label>
	 	 	                                    <input type="text" class="form-control" name="title" id="section-title" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
	 	 	                                  </div>
											  <div class="form-group">
						                        <div class="col-sm-12">
													<label class="control-label">Section details</label>
						                          	<textarea name="description" id="section-description" class="form-control" rows="6" cols="80"></textarea>
						                        </div>
						                      </div>
											  <div class="form-group col-sm-12">
	 	 	                                    <label for="exampleInputCountry">Section title ( Arabic )</label>
	 	 	                                    <input type="text" class="form-control" name="title_arabic" id="section-title-arabic-edit"  required>
	 	 	                                  </div>
											  <div class="form-group">
						                        <div class="col-sm-12">
													<label class="control-label">Section details ( Arabic )</label>
						                          	<textarea name="description_arabic" class="form-control" id="section-description-arabic-edit" rows="6" cols="80"></textarea>
						                        </div>
						                      </div>
						                      <div class="form-group col-sm-12 col-md-12">
											 	<label for="exampleInputCountry">Starting time</label>
											 	<input type="text" class="form-control" id="datepicker-section-edit" name="section_start_time">
										  	  </div>
						 				  </div>
						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						               <button type="submit" class="btn btn-green" id="submit-button-section-edit">Update</button>
						             </div>
						 			</form>
						           </div>
						         </div>
						       </div>
						       <div class="modal fade" id="add-section-attachment" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog madal-lg">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> Attachment</h3>
						             </div>
						             <div class="modal-body">
						               <form role="form" id="add-section-attachment-form" action="<?=site_url('admin/courses/addSectionAttachment')?>" method="post" enctype="multipart/form-data">
						 				  <input type="hidden" name="course_id" value="<?=$course_id?>">
						 				  <input type="hidden" name="cs_id" id="cs_add_attachment_id">
						 				  <div class="row">
											  <div class="form-group col-sm-12">
	 	 	                                    <label for="exampleInputCountry">Attachment type</label>
	 	 	                                    <select class="form-control" name="type">
													<option value="Nil">No attachment</option>
													<option value="Image">Image</option>
													<option value="Documents">Documents</option>
													<option value="Video">Video</option>
	 	 	                                    </select>
	 	 	                                  </div>
											  <div class="form-group col-sm-12">
	 	 	                                    <label for="exampleInputCountry">Choose attachment</label>
	 	 	                                    <input type="file" name="file" id="upload" class="form-control" required>
	 	 	                                  </div>
											  <div class="form-group col-sm-12">
	 	 	                                    <label for="exampleInputCountry">Youtube Link</label>
	 	 	                                    <input type="text" class="form-control" name="youtube">
	 	 	                                  </div>
											  <div class="form-group col-sm-12">
	 	 	                                    <label for="exampleInputCountry">Other Link</label>
	 	 	                                    <input type="text" class="form-control" name="other" other>
	 	 	                                  </div>
						 				  </div>
						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						               <button type="submit" class="btn btn-green" id="submit-button-section-attachment">Add</button>
						             </div>
						 			</form>
						           </div>
						         </div>
						       </div>
						       <div class="modal fade" id="view-section-attachment" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Section</strong> attachment</h3>
						             </div>
						             <div class="modal-body">
						               <div class="row">
										   <div class="col-md-12" id="view-attachment-table">

										   </div>
						               </div>
						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						             </div>
						           </div>
						         </div>
						       </div>
						   <?php } ?>

						   <?php if ($active == '4') { ?>
							   <div class="tab-pane fade in <?php if($active == '4'){ ?>active<?php } ?>">
								   <div class="row">
	 	                             <div class="form-group col-md-12 legend">
										 <button type="button" class="btn btn-primary margin-bottom-20 pull-right" data-toggle="modal" data-target="#add-batch">Add batch</button>
	 	                               <h4><strong>Course</strong> Batches</h4>
	 	                               <p>Manage course batches here</p>
	 	                             </div>
	 	                           </div>
								   <div class="row">
									   <div class="col-md-12">
										   <table  class="table table-datatable table-custom">
											   <thead>
				  		                           <tr>
				  		                             <th width="20%">Btach name</th>
													 <th width="20%">Btach name Arabic</th>
													 <th width="10%">Batch students</th>
													 <th width="5%">Edit</th>
				  		                           </tr>
			  		                           </thead>
											   <tbody>
												   <?php foreach ($batches as $batch) { ?>
													   <tr>
    													   <td><?=$batch->batch_name?></td>
														   <td><?=$batch->batch_name_arabic?></td>
														   <td>
															   <button class="btn btn-primary btn-xs" onclick="getBatchStudents(<?=$batch->cb_id?>)" style="margin-bottom:5px;">View Students</button><br>
															   <button class="btn btn-primary btn-xs" onclick="addBatchStudents(<?=$batch->cb_id?>,<?=$course_id?>)" style="margin-bottom:5px;">Add Students</button><br>
															   <button class="btn btn-danger btn-xs" onclick="removeBatchStudents(<?=$batch->cb_id?>)" style="margin-bottom:5px;">Remove Students</button>
														   </td>
														   <td><a class="btn btn-primary btn-xs margin-bottom-20" onclick="return editBatch(<?=$batch->cb_id?>)">Edit</a></td>
													   </tr>
												   <?php } ?>
											   </tbody>
					                       </table>
									   </div>
								   </div>
							   </div>
							   <div class="modal fade" id="add-batch" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> Batch</h3>
						             </div>
						             <div class="modal-body">
						               <form role="form" id="add-batch-form" action="<?=site_url('admin/courses/addBatch')?>" method="post">
						 				  <input type="hidden" name="course_id" value="<?=$course_id?>">
						 				  <div class="row">
						 					  <div class="form-group col-sm-12 col-md-12">
						       				   <label for="exampleInputCountry">Batch name</label>
											   <input type="text" class="form-control" name="batch_name" required>
						       				</div>
											<div class="form-group col-sm-12 col-md-12">
											 	<label for="exampleInputCountry">Batch name ( Arabic )</label>
											 	<input type="text" class="form-control" id="batch-name" name="batch_name_arabic" required>
										  	</div>
						 				  </div>

						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						               <button type="submit" class="btn btn-green" id="submit-button-batch">Add</button>
						             </div>
						 			</form>
						           </div>
						         </div>
						       </div>
							   <div class="modal fade" id="edit-batch" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Edit</strong> Batch</h3>
						             </div>
						             <div class="modal-body">
						               <form role="form" id="edit-batch-form" action="<?=site_url('admin/courses/editBatch')?>" method="post">
						 				  <input type="hidden" name="cb_id" id="cb_id">
						 				  <div class="row">
						 					  <div class="form-group col-sm-12 col-md-12">
						       				   <label for="exampleInputCountry">Batch name</label>
											   <input type="text" class="form-control" name="batch_name" id="batch_name">
						       				</div>
											<div class="form-group col-sm-12 col-md-12">
											 	<label for="exampleInputCountry">Batch name ( Arabic )</label>
											 	<input type="text" class="form-control" id="batch-name-edit" name="batch_name_arabic" required>
										  	</div>
						 				  </div>

						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						               <button type="submit" class="btn btn-green" id="submit-button-batch-edit">Update</button>
						             </div>
						 			</form>
						           </div>
						         </div>
						       </div>
							   <div class="modal fade" id="add-batch-students" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> Students to batch</h3>
						             </div>
						             <div class="modal-body">
						               <form role="form" id="add-batch-student-form" action="<?=site_url('admin/courses/addBatchStudents')?>" method="post">
										   <input type="hidden" name="course_id" value="<?=$course_id?>">
										   <input type="hidden" name="course_batch_id" id="course_batch_id">
 						 				  <div class="row">
 						 					  <div class="form-group col-sm-12 col-md-12">
 						       				   <label for="exampleInputCountry">Choose students</label>
 						       				   <select multiple class="chosen-select chosen-transparent form-control" id="input09" name="students[]" required>

 						       				   </select>
 						       				</div>
 						 				  </div>

						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						               <button type="submit" class="btn btn-green" id="submit-button-batch-student">Add</button>
						             </div>
						 			</form>
						           </div>
						         </div>
						       </div>
							   <div class="modal fade" id="view-batch-students" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Batch</strong> Students</h3>
						             </div>
						             <div class="modal-body">
						               <div class="row">
										   <div class="col-md-12" id="batch-students">

										   </div>
						               </div>
						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						             </div>
						           </div>
						         </div>
						       </div>
							   <div class="modal fade" id="remove-batch-students" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> Students to batch</h3>
						             </div>
						             <div class="modal-body">
						               <form role="form" id="add-batch-student-form" action="<?=site_url('admin/courses/removeBatchStudents')?>" method="post">
										   <input type="hidden" name="cb_id" id="course_batch_remove_id">
										   <input type="hidden" name="course_id" value="<?=$course_id?>">
 						 				  <div class="row">
 						 					  <div class="form-group col-sm-12 col-md-12">
 						       				   <label for="exampleInputCountry">Choose students to remove from the batch</label>
 						       				   <select multiple class="chosen-select chosen-transparent form-control" id="input10" name="students[]" required>

 						       				   </select>
 						       				</div>
 						 				  </div>

						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						               <button type="submit" class="btn btn-green" id="submit-button-batch-student">Remove</button>
						             </div>
						 			</form>
						           </div>
						         </div>
						       </div>
						   <?php } ?>

						   <?php if ($active == '5') { ?>
							   <div class="tab-pane fade in <?php if($active == '5'){ ?>active<?php } ?>">
								   <div class="row">
	 	                             <div class="form-group col-md-12 legend">
										 <button type="button" class="btn btn-primary margin-bottom-20 pull-right" data-toggle="modal" data-target="#add-class">Add Class</button>
	 	                               <h4><strong>Online</strong> Classes</h4>
	 	                               <p>Manage online classes here</p>
	 	                             </div>
	 	                           </div>
								   <div class="row">
									   <div class="col-md-12">
										   <table class="table table-datatable table-custom" id="datatable">
											   <thead>
				  		                           <tr>
				  		                             <th width="15%">Topic</th>
													 <th width="10%">Batch</th>
													 <th width="15%">Section</th>
				  		                             <th width="10%">Trainer</th>
													 <th width="10%">Starting</th>
				  		                             <th width="10%">Host</th>
													 <th width="5%">Options</th>
				  		                           </tr>
			  		                           </thead>
											   <tbody>
												   <?php foreach ($classes as $class) { ?>
													   <tr>
    													   <td><?=$class->topic?></td>
														   <td><?=$class->batch_name?></td>
														   <td><?=$class->section_title?></td>
														   <td><?=$class->trainer_name_english?></td>
														   <td><?=date('d/m/Y h:i A',strtotime($class->starting_time)) . '<br>' . $class->duration . ' Minutes'?></td>
														   <td><?=$class->host_name . '<br>' . $class->alternative_host?></td>
    													   <td>
															   <?php
															        $date = date('Y-m-d H:i:s');
															        $start = date('Y-m-d H:i:s',strtotime($class->starting_time . ' +' . $class->duration . ' minutes'));
															        if (strtotime($date) > strtotime($start)) { ?>
																    <?php if ($class->class_status == 'pending') { ?>
																		<a class="btn btn-success btn-xs" href="<?=site_url('admin/courses/completeCourse/' . $class->cc_id)?>">Complete</a>
																	<?php }else { ?>
																		<a class="btn btn-primary btn-xs" href="<?=site_url('admin/courses/class/' . $class->cc_id)?>">View</a>
																	<?php } ?>
															   <?php }else { ?>
																   <a class="btn btn-primary btn-xs" onclick="return editClass(<?=$class->cc_id?>)">Edit</a>
																   <a class="btn btn-primary btn-xs" href="<?=$class->start_url?>" target="_blank">Start class</a>
															   <?php } ?>
															   <a class="btn btn-danger btn-xs" href="<?=site_url('admin/courses/deleteClass/' . $class->cc_id . '/1')?>">Delete</a>
														   </td>
													   </tr>
												   <?php } ?>
											   </tbody>
					                       </table>
									   </div>
								   </div>
							   </div>
							   <div class="modal fade" id="add-class" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> Class</h3>
						             </div>
						             <div class="modal-body">
						               <form role="form" id="add-class-form" action="<?=site_url('admin/courses/addClass')?>" method="post">
						 				  <input type="hidden" name="course_id" value="<?=$course_id?>">
						 				  <div class="row">
						 					<div class="form-group col-sm-12 col-md-12">
						       				   <label for="exampleInputCountry">Topic</label>
											   <input type="text" class="form-control" name="topic" required>
						       				</div>
											<div class="form-group col-sm-12 col-md-12">
						       				   <label for="exampleInputCountry">Topic ( Arabic ) </label>
											   <input type="text" class="form-control" name="topic_arabic" id="topic-arabic" required>
						       				</div>
											<div class="form-group col-sm-12">
											  <label for="exampleInputCountry">Batch</label>
											  <select class="form-control" name="cb_id">
												  <?php foreach ($batches as $batch) { ?>
												  	<option value="<?=$batch->cb_id?>"><?=$batch->batch_name?></option>
												  <?php } ?>
											  </select>
											</div>
											<div class="form-group col-sm-12">
											  <label for="exampleInputCountry">Section</label>
											  <select class="form-control" name="cs_id">
												  <?php foreach ($sections as $section) { ?>
												  	<option value="<?=$section->cs_id?>"><?=$section->section_title?></option>
												  <?php } ?>
											  </select>
											</div>
											<div class="form-group col-sm-12">
											  <label for="exampleInputCountry">Trainer</label>
											  <select class="form-control" name="trainer_id">
												  <?php foreach ($trainers as $trainer) { ?>
												  	<option value="<?=$trainer->trainer_id?>"><?=$trainer->trainer_name_english?></option>
												  <?php } ?>
											  </select>
											</div>
											<div class="form-group col-sm-12 col-md-12">
											 	<label for="exampleInputCountry">Starting time</label>
											 	<input type="text" class="form-control" id="datepicker" name="starting_time">
										  	</div>
											<div class="form-group col-sm-12 col-md-12">
						       				   <label for="exampleInputCountry">Duration (In minutes)</label>
											   <input type="text" class="form-control" name="duration" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
						       				</div>
											<div class="form-group col-sm-12 col-md-12">
						       				   <label for="exampleInputCountry">Host name</label>
											   <input type="text" class="form-control" name="host_name">
						       				</div>
											<div class="form-group col-sm-12 col-md-12">
						       				   <label for="exampleInputCountry">Alternative host</label>
											   <select class="form-control" name="alternative_host">
												   <option value="">No alternative host</option>
												   <?php foreach ($alternative_hosts as $host) { ?>
													   <option value="<?=$host['email']?>"><?=$host['name']?></option>
												   <?php } ?>
											   </select>
						       				</div>
											<div class="form-group col-sm-12 col-md-12">
						       				   <label for="exampleInputCountry">Meeting password</label>
											   <input type="text" class="form-control" name="meeting_password">
						       				</div>
						 				  </div>

						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						               <button type="submit" class="btn btn-green" id="submit-button-class">Add</button>
						             </div>
						 			</form>
						           </div>
						         </div>
						       </div>
							   <div class="modal fade" id="edit-class" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Edit</strong> Class</h3>
						             </div>
						             <div class="modal-body">
						               <form role="form" id="edit-class-form" action="<?=site_url('admin/courses/editClass')?>" method="post">
						 				  <input type="hidden" name="cc_id" id="cc_id">
						 				  <div class="row">
						 					<div class="form-group col-sm-12 col-md-12">
						       				   <label for="exampleInputCountry">Topic</label>
											   <input type="text" class="form-control" name="topic" id="topic">
						       				</div>
											<div class="form-group col-sm-12 col-md-12">
						       				   <label for="exampleInputCountry">Topic ( Arabic ) </label>
											   <input type="text" class="form-control" name="topic_arabic" id="topic-arabic-edit" required>
						       				</div>
											<div class="form-group col-sm-12">
											  <label for="exampleInputCountry">Batch</label>
											  <select class="form-control" name="cb_id" id="cb_id">
												  <?php foreach ($batches as $batch) { ?>
												  	<option value="<?=$batch->cb_id?>"><?=$batch->batch_name?></option>
												  <?php } ?>
											  </select>
											</div>
											<div class="form-group col-sm-12">
											  <label for="exampleInputCountry">Section</label>
											  <select class="form-control" name="cs_id" id="cs_id">
												  <?php foreach ($sections as $section) { ?>
												  	<option value="<?=$section->cs_id?>"><?=$section->section_title?></option>
												  <?php } ?>
											  </select>
											</div>
											<div class="form-group col-sm-12">
											  <label for="exampleInputCountry">Trainer</label>
											  <select class="form-control" name="trainer_id" id="trainer_id">
												  <?php foreach ($trainers as $trainer) { ?>
												  	<option value="<?=$trainer->trainer_id?>"><?=$trainer->trainer_name_english?></option>
												  <?php } ?>
											  </select>
											</div>
											<div class="form-group col-sm-12 col-md-12">
											 	<label for="exampleInputCountry">Starting time</label>
											 	<input type="text" class="form-control" id="datepicker-edit" name="starting_time">
										  	</div>
											<div class="form-group col-sm-12 col-md-12">
						       				   <label for="exampleInputCountry">Duration (In minutes)</label>
											   <input type="text" class="form-control" name="duration" id="duration" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
						       				</div>
											<div class="form-group col-sm-12 col-md-12">
						       				   <label for="exampleInputCountry">Host name</label>
											   <input type="text" class="form-control" name="host_name" id="host_name">
						       				</div>
											<div class="form-group col-sm-12 col-md-12">
						       				   <label for="exampleInputCountry">Alternative host</label>
											   <select class="form-control" name="alternative_host" id="alternative_host">
												   <option value="">No alternative host</option>
												   <?php foreach ($alternative_hosts as $host) { ?>
													   <option value="<?=$host['email']?>"><?=$host['name']?></option>
												   <?php } ?>
											   </select>
						       				</div>
											<div class="form-group col-sm-12 col-md-12">
						       				   <label for="exampleInputCountry">Meeting password</label>
											   <input type="text" class="form-control" name="meeting_password" id="meeting_password">
						       				</div>
						 				  </div>

						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						               <button type="submit" class="btn btn-green" id="submit-button-class">Update</button>
						             </div>
						 			</form>
						           </div>
						         </div>
						       </div>
						   <?php } ?>
						   <?php if ($active == '6') { ?>
							   <div class="tab-pane fade in <?php if($active == '6'){ ?>active<?php } ?>">
								   <div class="row">
	 	                             <div class="form-group col-md-12 legend">
	 	                                 <button type="button" class="btn btn-primary margin-bottom-20 pull-right" data-toggle="modal" data-target="#add-certificate-all">Add certificate</button>
	 	                               <h4><strong>Subscribed</strong> Students</h4>
	 	                               <p></p>
	 	                             </div>
	 	                           </div>
								   <div class="row">
									   <div class="col-md-12">
										   <table  class="table table-datatable table-custom" id="user_data">
											   <thead>
				  		                           <tr>
				  		                             <th width="10%">Name</th>
													 <th width="10%">Contact</th>
				  		                             <th width="10%">Education</th>
													 <th width="10%">Certificates</th>
													 <th width="10%">Grade</th>
				  		                           </tr>
			  		                           </thead>
											   <tbody>
												   <?php foreach ($students as $student) { ?>
													   <tr>
    													   <td><?=$student->stu_name_english . '<br>' . $student->stu_name_arabic?></td>
    													   <td><?=$student->stu_mobile . '<br>' . $student->stu_email?></td>
    													   <td><?=$student->stu_education?></td>
														   <td>
															   <button class="btn btn-primary btn-xs" onclick="getCertificate(<?=$student->stu_id?>,<?=$course_id?>)" style="margin-bottom:5px;">View Certificates</button><br>
															   <button class="btn btn-primary btn-xs" onclick="addCertificate(<?=$student->stu_id?>)" style="margin-bottom:5px;">Add Certificate</button>
														   </td>
														   <?php if($student->grades>0) {?>
														   <td><b><?=$student->grade?></b><br><button class="btn btn-primary btn-xs" onclick="getGrade(<?=$student->garde_id?>,`<?=$student->grade?>`)" style="margin-bottom:5px;">Edit Grade</button></td>
														   <?php } else {?>
														   <td><button class="btn btn-primary btn-xs" onclick="addGrade(<?=$student->stu_id?>)" style="margin-bottom:5px;">Add Grade</button></td>
														   <?php };?>
													   </tr>
												   <?php } ?>
											   </tbody>
					                       </table>
									   </div>
								   </div>
							   </div>
							   <div class="modal fade" id="add-certificate" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> certificate</h3>
						             </div>
						             <div class="modal-body">
						               <form role="form" id="add-certificate-form" action="<?=site_url('admin/courses/addCertificate')?>" method="post" enctype="multipart/form-data">
										   <input type="hidden" name="stu_id" id="stuu_id">
										   <input type="hidden" name="course_id" value="<?=$course_id?>">
 						 				   <div class="row">
 						 					  <div class="form-group col-sm-12 col-md-12">
 						       				     <label for="exampleInputCountry">Certificate name ( English )</label>
												 <input type="text" name="title" class="form-control" required>
 						       				  </div>
											  <div class="form-group col-sm-12 col-md-12">
 						       				     <label for="exampleInputCountry">Certificate name ( Arabic )</label>
												 <input type="text" name="title_arabic" id="certificate-name-arabic" class="form-control" required>
 						       				  </div>
											  <!--<div class="form-group col-sm-12 col-md-12">-->
 						      <!-- 				     <label for="exampleInputCountry">Attach certificate</label>-->
												 <!--<input type="file" name="file" id="upload" class="form-control" required>-->
 						      <!-- 				  </div>-->
 						                      <div class="form-group col-sm-12 col-md-12">
  						       				     <label for="exampleInputCountry">Certificate Content</label>
 												 <textarea type="text" name="content" id="certificate-content-all" class="form-control" required></textarea>
  						       				  </div>
  						      <!-- 				  <div class="form-group col-sm-12 col-md-12">-->
  						      <!-- 				     <label for="exampleInputCountry">Certificate Content ( Arabic )</label>-->
 												 <!--<textarea type="text" name="content_arabic" id="certificate-content-arabic-all" class="form-control" required></textarea>-->
  						      <!-- 				  </div>-->
 						 				   </div>
						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						               <button type="submit" class="btn btn-green" id="submit-button-add-certificate">Add</button>
						             </div>
						 			</form>
						           </div>
						         </div>
						       </div>
							   <div class="modal fade" id="view-certificate" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>View</strong> certificate</h3>
						             </div>
						             <div class="modal-body">
						   	 			<div class="row">
						   	 				<div class="col-md-12" id="certificate">

						   	 				</div>
						   	 			</div>
						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						             </div>
						           </div>
						         </div>
						       </div>
						       <div class="modal fade" id="add-certificate-all" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> certificate</h3>
						             </div>
						             <div class="modal-body">
										<form action="<?=site_url('admin/courses/addCertificateAll')?>" id="add-certificate-form-all" method="post" enctype="multipart/form-data">
											<input type="hidden" name="course_id" value="<?=$course_id?>">
											<div class="row">
											   <div class="form-group col-sm-12 col-md-12">
												  <label for="exampleInputCountry">Certificate name ( English )</label>
												  <input type="text" name="title" class="form-control" required>
											   </div>
											   <div class="form-group col-sm-12 col-md-12">
  						       				     <label for="exampleInputCountry">Certificate name ( Arabic )</label>
 												 <input type="text" name="title_arabic" id="certificate-name-arabic-all" class="form-control" required>
  						       				  </div>
  						       				  <div class="form-group col-sm-12 col-md-12">
  						       				     <label for="exampleInputCountry">Certificate Content</label>
 												 <textarea type="text" name="content" id="certificate-content-all" class="form-control" required></textarea>
  						       				  </div>
  						      <!-- 				  <div class="form-group col-sm-12 col-md-12">-->
  						      <!-- 				     <label for="exampleInputCountry">Certificate Content ( Arabic )</label>-->
 												 <!--<textarea type="text" name="content_arabic" id="certificate-content-arabic-all" class="form-control" required></textarea>-->
  						      <!-- 				  </div>-->
											   <!--<div class="form-group col-sm-12 col-md-12">-->
												  <!--<label for="exampleInputCountry">Attach certificate</label>-->
												  <!--<input type="file" name="file" id="upload-all" class="form-control" required>-->
											   <!--</div>-->
											</div>
											<div class="row">
												<div class="form-group col-sm-12 col-md-12">
												 	<label for="exampleInputCountry">Choose students</label>
												 	<select multiple class="chosen-select chosen-transparent form-control" id="input10" name="students[]" required>
														<?php foreach ($students as $student) { ?>
															<option value="<?=$student->stu_id?>"><?=$student->stu_name_english . ' ' . $student->stu_name_arabic?></option>
	 												   <?php } ?>
												 	</select>
											  	</div>
							   	 			</div>

						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
									   <button type="submit" class="btn btn-green" id="submit-button-add-certificate-all">Add</button>
						             </div>
									 </form>
						           </div>
						         </div>
						       </div>
						       
						       <div class="modal fade" id="add-grade" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> Grade</h3>
						             </div>
						             <div class="modal-body">
						               <form role="form" id="add-grade" action="<?=site_url('admin/courses/addGrade')?>" method="post" enctype="multipart/form-data">
										   <input type="hidden" name="stu_id" id="stuuu_id">
										   <input type="hidden" name="course_id" value="<?=$course_id?>">
 						 				   <div class="row">
 						 					  <div class="form-group col-sm-12 col-md-12">
 						       				     <label for="exampleInputCountry">Grade</label>
												 <input type="text" name="grade" class="form-control" required>
 						       				  </div>
											 
 						 				   </div>
						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						               <button type="submit" class="btn btn-green" id="submit-button">Add</button>
						             </div>
						 			</form>
						           </div>
						         </div>
						       </div>
						       
						       <div class="modal fade" id="edit-grade" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Edit</strong> Grade</h3>
						             </div>
						             <div class="modal-body">
						               <form role="form" id="add-grade" action="<?=site_url('admin/courses/editGrade')?>" method="post" enctype="multipart/form-data">
										   <input type="hidden" name="grade_id" id="grade_id">
										   <input type="hidden" name="course_id" value="<?=$course_id?>">
 						 				   <div class="row">
 						 					  <div class="form-group col-sm-12 col-md-12">
 						       				     <label for="exampleInputCountry">Grade</label>
												 <input type="text" name="grade" id="grade" class="form-control" required>
 						       				  </div>
											 
 						 				   </div>
						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						               <button type="submit" class="btn btn-green" id="submit-button">Update</button>
						             </div>
						 			</form>
						           </div>
						         </div>
						       </div>
						   <?php } ?>
						   <?php if ($active == '7') { ?>
							   <div class="tab-pane fade in <?php if($active == '7'){ ?>active<?php } ?>">
								   <div class="row">
	 	                             <div class="form-group col-md-12 legend">
										 <a href="<?=site_url('admin/exams/add/' . $course_id)?>" class="btn btn-primary margin-bottom-20 pull-right">Add Exam</a>
										 <a href="<?=site_url('admin/exams/duplicate/' . $course_id)?>" class="btn btn-primary margin-bottom-20 pull-right" style="margin-left : 5px;margin-right : 5px;">Copy Exam</a>
	 	                               <h4><strong>Exams</h4>
	 	                               <p></p>
	 	                             </div>
	 	                           </div>
								   <div class="row">
									   <div class="col-md-12">
										   <table class="table table-datatable table-custom" id="user_data">
											   <thead>
				  		                           <tr>
				  		                             <th width="10%">Name</th>
													 <th width="10%">Batch</th>
				  		                             <th width="10%">Section</th>
													 <th width="10%">Questions</th>
													 <th width="10%">Time</th>
				  		                             <th width="10%">Section</th>
													 <th width="5%%">Edit</th>
													 <th width="5%%">Questions</th>
				  		                             <th width="5%%">Instructions</th>
				  		                             <th width="5%%">Status</th>
													 <th width="5%%">Delete</th>
				  		                           </tr>
			  		                           </thead>

					                       </table>
									   </div>
								   </div>
							   </div>
						   <?php } ?>
						   <?php if ($active == '8') { ?>
							   <div class="tab-pane fade in <?php if($active == '8'){ ?>active<?php } ?>">
								   <div class="row">
	 	                             <div class="form-group col-md-12 legend">
	 	                               <h4><strong>Attendance</h4>
	 	                               <p></p>
	 	                             </div>
	 	                           </div>
								   <div class="row">
									   <form action="<?=site_url('admin/courses/details/8/' . $course_id)?>" method="post">
										   <div class="col-md-3 form-group">
											  <label>Select batch</label>
											  <select class="form-control" name="cb_id">
												  <?php foreach ($batches as $batch) { ?>
													  <option value="<?=$batch->cb_id?>" <?php if($batch->cb_id == $cb_id){ ?>selected<?php } ?>><?=$batch->batch_name?></option>
												  <?php } ?>
											  </select>
										  </div>
										  <div class="col-md-3 form-group">
											  <label>Date</label>
											  <input type="date" name="date" class="form-control" value="<?=$date?>" required>
										  </div>
										  <div class="col-md-3 form-group">
											  <button type="submit" class="btn btn-green" id="submit-button-add-certificate" style="margin-top:25px;">Submit</button>
										  </div>
									   </form>
								   </div>
								   <div class="row">
									   <div class="col-md-12">
										   <?php if ($param) { ?>
											  <?php if ($attendance) { ?>
											      <a href="<?=site_url('admin/courses/exportAttendance/' . $cb_id . '/' . $date)?>" class="btn btn-primary btn-sm margin-bottom-20 pull-right">Export</a>
												  <h4>Attendance already added</h4>
												  <h4>Date : <?=$date?></h4>

												  <form action="<?=site_url('admin/courses/editAttendance')?>" method="post" id="attendance-form-edit">
													  <input type="hidden" name="course_id" value="<?=$course_id?>">
													  <table class="table table-datatable table-custom">
														  <thead>
															  <th>Student name</th>
															  <th>Attendance</th>
														  </thead>
														  <tbody>
															  <?php $i=0; foreach ($students as $stu) { ?>
																  <input type="hidden" name="ca_id<?=$i?>" value="<?=$stu->ca_id?>">
																  <tr>
																	  <td><?=$stu->stu_name_english?></td>
																	  <td>
																		  <label><input type="radio" name="attendance<?=$i?>" value="present" <?php if($stu->present){ ?>checked<?php } ?>>&nbsp;&nbsp;PRESENT</label>&nbsp;&nbsp;
																		  <label><input type="radio" name="attendance<?=$i?>" value="absent" <?php if($stu->absent){ ?>checked<?php } ?>>&nbsp;&nbsp; ABSENT</label>&nbsp;&nbsp;
																		  <label><input type="radio" name="attendance<?=$i?>" value="late" <?php if($stu->late){ ?>checked<?php } ?>>&nbsp;&nbsp; LATE</label>&nbsp;&nbsp;
																	  </td>
																  </tr>
															  <?php $i++; } ?>
														  </tbody>
													  </table>
													  <input type="hidden" name="student_count" value="<?=$i?>">
													  <div class="pull-right">
														  <button type="submit" class="btn btn-green" id="submit-button-attendance-edit">Update</button>
													  </div>
												  </form>
											  <?php }else { ?>
												  <h4>Attendance not added</h4>
												  <h4>Date : <?=$date?></h4>

												  <form action="<?=site_url('admin/courses/addAttendance')?>" method="post" id="attendance-form">
													  <input type="hidden" name="course_id" value="<?=$course_id?>">
													  <input type="hidden" name="date" value="<?=$date?>">
													  <input type="hidden" name="cb_id" value="<?=$cb_id?>">
													  <table class="table table-datatable table-custom">
														  <thead>
															  <th>Student name</th>
															  <th>Attendance</th>
														  </thead>
														  <tbody>
															  <?php $i=0; foreach ($students as $stu) { ?>
																  <input type="hidden" name="stu_id<?=$i?>" value="<?=$stu->stu_id?>">
																  <tr>
																	  <td><?=$stu->stu_name_english?></td>
																	  <td>
																		  <label><input type="radio" name="attendance<?=$i?>" value="present" checked>&nbsp;&nbsp;PRESENT</label>&nbsp;&nbsp;
																		  <label><input type="radio" name="attendance<?=$i?>" value="absent">&nbsp;&nbsp; ABSENT</label>&nbsp;&nbsp;
																		  <label><input type="radio" name="attendance<?=$i?>" value="late">&nbsp;&nbsp; LATE</label>&nbsp;&nbsp;
																	  </td>
																  </tr>
															  <?php $i++; } ?>
														  </tbody>
													  </table>
													  <input type="hidden" name="student_count" value="<?=$i?>">
													  <div class="pull-right">
														  <button type="submit" class="btn btn-green" id="submit-button-attendance">Add</button>
													  </div>
												  </form>
											  <?php } ?>
										  <?php } ?>
									   </div>
								   </div>
							   </div>
						   <?php } ?>
						   <?php if ($active == '9') { ?>
							   <div class="tab-pane fade in <?php if($active == '9'){ ?>active<?php } ?>">
								   <div class="row">
	 	                             <div class="form-group col-md-12 legend">
										 <button type="button" class="btn btn-primary margin-bottom-20 pull-right" data-toggle="modal" data-target="#add-assignment">Add assignment</button>
	 	                               <h4><strong>Assignments</h4>
	 	                               <p></p>
	 	                             </div>
	 	                           </div>
								   <div class="row">
									   <div class="col-md-12">
										   <table  class="table table-datatable table-custom">
											   <thead>
				  		                           <tr>
				  		                             <th width="10%">Tilte</th>
													 <th width="10%">Tilte( Arabic )</th>
													 <th width="10%">Batch</th>
				  		                             <th width="10%">Attachment</th>
													 <th width="10%">Attended students</th>
													 <th width="5%">Delete</th>
				  		                           </tr>
			  		                           </thead>
											   <tbody>
												   <?php foreach ($assignments as $assignment) { ?>
													   <tr>
    													   <td><?=$assignment->title?></td>
														   <td><?=$assignment->title_arabic?></td>
														   <td>
												                <?php if($assignment->cb_id != '0'){ ?>
												                    <?=$assignment->batch_name?>
												                <?php }else{ ?>
												                    For all batches
												                <?php } ?>
														   </td>
														   <td>
															   <?php if ($assignment->attachment != '') { ?>
																   <a href="<?=base_url() . $assignment->attachment?>" target="_blank">View</a>
															   <?php }else { ?>
																   No attachment
															   <?php } ?>
														   </td>
														   <td>
															   <a class="btn btn-primary btn-xs" href="<?=site_url('admin/courses/assignments/' . $assignment->ca_id)?>">View</a>
														   </td>
														   <td>
															   <a class="btn btn-danger btn-xs" onclick="return confirm('Are you sure to delete this assignment?')" href="<?=site_url('admin/courses/deleteAssignment/' . $assignment->ca_id)?>">Delete</a>
														   </td>
													   </tr>
												   <?php } ?>
											   </tbody>
					                       </table>
									   </div>
								   </div>
							   </div>
							   <div class="modal fade" id="add-assignment" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						         <div class="modal-dialog">
						           <div class="modal-content">
						             <div class="modal-header">
						               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
						               <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> assignment</h3>
						             </div>
						             <div class="modal-body">
						               <form action="<?=site_url('admin/courses/addAssignment')?>" method="post" id="add-assignment-form" enctype="multipart/form-data">
										   <input type="hidden" name="course_id" value="<?=$course_id?>">
 						 				   <div class="row">
 						 					  <div class="form-group col-sm-12 col-md-12">
 						       				     <label for="exampleInputCountry">Assignment title</label>
												 <input type="text" name="title" class="form-control" required>
 						       				  </div>
											  <div class="form-group col-sm-12 col-md-12">
 						       				     <label for="exampleInputCountry">Assignment title ( Arabic )</label>
												 <input type="text" name="title_arabic" id="assignment-arabic" class="form-control" required>
 						       				  </div>
											  <div class="form-group col-sm-12 col-md-12">
 						       				     <label for="exampleInputCountry">Choose class</label>
												 <select class="form-control" name="cb_id">
												     <option value="0">For all batches</option>
													 <?php foreach ($batches as $batch) { ?>
													 	<option value="<?=$batch->cb_id?>"><?=$batch->batch_name?></option>
													 <?php } ?>
												 </select>
 						       				  </div>
											  <div class="form-group col-sm-12 col-md-12">
 						       				     <label for="exampleInputCountry">Attachment</label>
												 <input type="file" name="file" id="upload-assignment" class="form-control" required>
 						       				  </div>
 						 				   </div>
						             </div>
						             <div class="modal-footer">
						               <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
						               <button type="submit" class="btn btn-green" id="submit-button-assignment">Add</button>
						             </div>
						 			</form>
						           </div>
						         </div>
						       </div>
						   <?php } ?>

 	                     </div>
 	                   </section>
 	                 </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
      <?php $this->load->view('admin/includes/table-script.php'); ?>
      <script src="<?=base_url()?>assets/js/vendor/chosen/chosen.jquery.min.js"></script>
	  <script src="<?=base_url()?>assets/js/vendor/datepicker/bootstrap-datetimepicker.min.js"></script>
	  <script src="<?=base_url()?>assets/plugins/image-crop/croppie.js"></script>
      <script type="text/javascript">
	      $uploadCrop = $('#upload-demo').croppie({
	        enableExif: true,
	        viewport: {
	            width: 600,
	            height: 400,
	            type: 'rectangle'
	        },
	        boundary: {
	            width: 700,
	            height: 500
	        }
	      });


	     $('#upload').on('change', function () {
	      $("#submit-button").css("display", "none");
	      var file = $("#upload")[0].files[0];
	      var val = file.type;
	      var type = val.substr(val.indexOf("/") + 1);
	      if (type == 'png' || type == 'jpg' || type == 'jpeg' || type =='pdf') {

	        $("#current-image").css("display", "none");
	        $("#submit-button").css("display", "none");

	        $(".upload-div").css("display", "block");
	        $("#submit-button").css("display", "none");
	        var reader = new FileReader();
	          reader.onload = function (e) {
	            $uploadCrop.croppie('bind', {
	              url: e.target.result
	            }).then(function(){
	              console.log('jQuery bind complete');
	            });

	          }
	          reader.readAsDataURL(this.files[0]);
	      }
	      else {
	        alert('This file format is not supported.');
	        document.getElementById("upload").value = "";
	        $("#upload-result").css("display", "none");
	        $("#submit-button").css("display", "none");
	        $("#current-image").css("display", "block");
	        $('#ameimg').val('');
	      }
	    });


	    $('#crop-button').on('click', function (ev) {
	        $("#submit-button").css("display", "block");
	      $uploadCrop.croppie('result', {
	        type: 'canvas',
	        size: 'viewport'
	      }).then(function (resp) {
	        html = '<img src="' + resp + '" />';
	        $("#upload-result").html(html);
	        $("#upload-result").css("display", "block");
	        $(".upload-div").css("display", "none");
	        $("#submit-button").css("display", "block");
	        $('#ameimg').val(resp);
	      });
	    });
     </script>
	  <?php if ($active == '7') { ?>
		  <script>
		        $(document).ready(function(){
                var tbl = $('.my-table');
                var settings = {
                  "processing":true,
                  "serverSide":true,
                  "order":[],
                  "ajax":{
                    url:"<?=site_url('admin/exams/get')?>",
                    type:"POST",
                    data : { course_id : <?=$course_id?>}
                  },
                  "columnDefs":[
                    {
                      "target":[0,3,4],
                      "orderable":true
                    }
                  ],
                  dom: 'lBfrtip',
                  buttons: [
                      {
                          extend:'pdfHtml5',
                          text:'Pdf',
                          exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5 ]
                          },
                          orientation:'landscape',
                          customize: function (doc) {
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                          }
                      },
                      {
                          extend: 'print',
                          text: 'Print',
                          exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5 ]
                          },
                      },
                      { extend: 'csv',text: 'Csv' },
                  ],
                  lengthMenu: [[25, 100, -1], [25, 100, "All"]],
                  pageLength: 25,
                  customize: function (doc) {
                      doc.content[1].table.widths =
                          Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    }
                };
              var dataTable = $('#user_data').DataTable(settings);

            });

		  </script>
	  <?php } ?>
	  <?php if ($active == '6') { ?>
	  <script>
	      $(document).ready(function(){
        var tbl = $('.my-table');
        var settings = {
          "columnDefs":[
            {
              "target":[0,3,4],
              "orderable":true
            }
          ],
          dom: 'lBfrtip',
          buttons: [
              {
                  extend:'pdfHtml5',
                  text:'Pdf',
                  exportOptions: {
                    columns: [ 0, 1 ]
                  },
                  orientation:'landscape',
                  customize: function (doc) {
                    doc.content[1].table.widths =
                        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                  }
              },
              {
                  extend: 'print',
                  text: 'Print',
                  exportOptions: {
                    columns: [ 0, 1 ]
                  },
              },
              { extend: 'csv',text: 'Csv' },
          ],
          lengthMenu: [[25, 100, -1], [25, 100, "All"]],
          pageLength: 25,
          customize: function (doc) {
              doc.content[1].table.widths =
                  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        };
      var dataTable = $('#user_data').DataTable(settings);

    });
	  </script>
	  <?php } ?>
	  <script type="text/javascript">
		  $('#course-edit-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button-course').attr('disabled',false);

			  var title = $('#course-title-arabic').val();
			  var description = $('#course-description-arabic').val();
			  var duration = $('#course-duration-arabic').val();
			  if (arabicCheck(title) && arabicCheck(description) && arabicCheck(duration)) {
				  document.getElementById("course-edit-form").submit();
			  }
			  else {
				  if (!arabicCheck(title)) {
					  toastr.error('Please check arabic title',"");
				  }
				  if (!arabicCheck(description)) {
					  toastr.error('Please check arabic description',"");
				  }
				  if (!arabicCheck(duration)) {
					  toastr.error('Please check arabic duration',"");
				  }
				  $('#submit-button-course').attr('disabled',false);
			  }
		  });
		  $('#add-section-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button-section').attr('disabled',true);

			  var title = $('#section-title-arabic').val();
  			  var description = $('#section-description-arabic').val();
  			  if (arabicCheck(title) && arabicCheck(description)) {
				  document.getElementById("add-section-form").submit();
			  }
			  else {
				  if (!arabicCheck(title)) {
					  toastr.error('Please check arabic title',"");
				  }
				  if (!arabicCheck(description)) {
					  toastr.error('Please check arabic description',"");
				  }
				  $('#submit-button-section').attr('disabled',false);
			  }
		  });
		  $('#edit-section-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button-section-edit').attr('disabled',true);

			  var title = $('#section-title-arabic-edit').val();
  			  var description = $('#section-description-arabic-edit').val();
  			  if (arabicCheck(title) && arabicCheck(description)) {
				  document.getElementById("edit-section-form").submit();
			  }
			  else {
				  if (!arabicCheck(title)) {
					  toastr.error('Please check arabic title',"");
				  }
				  if (!arabicCheck(description)) {
					  toastr.error('Please check arabic description',"");
				  }
				  $('#submit-button-section-edit').attr('disabled',false);
			  }
		  });
		  $('#add-batch-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button-batch').attr('disabled',true);

			  var batch = $('#batch-name').val();
  			  if (arabicCheck(batch)) {
				  document.getElementById("add-batch-form").submit();
			  }
			  else {
				  toastr.error('Please check arabic batch name',"");
				  $('#submit-button-batch').attr('disabled',false);
			  }
		  });
		  $('#edit-batch-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button-batch-edit').attr('disabled',true);

			  var batch = $('#batch-name-edit').val();
  			  if (arabicCheck(batch)) {
				  document.getElementById("edit-batch-form").submit();
			  }
			  else {
				  toastr.error('Please check arabic batch name',"");
				  $('#submit-button-batch-edit').attr('disabled',false);
			  }
		  });
		  $('#add-class-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button-class').attr('disabled',true);

			  var class_name = $('#topic-arabic').val();
  			  if (arabicCheck(class_name)) {
				  document.getElementById("add-class-form").submit();
			  }
			  else {
				  toastr.error('Please check arabic topic name',"");
				  $('#submit-button-class').attr('disabled',false);
			  }
		  });
		  $('#edit-class-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button-class-edit').attr('disabled',true);

			  var class_name = $('#topic-arabic').val();
  			  if (arabicCheck(class_name)) {
				  document.getElementById("edit-class-form").submit();
			  }
			  else {
				  toastr.error('Please check arabic topic name',"");
				  $('#submit-button-class-edit').attr('disabled',false);
			  }
		  });
		  $('#add-batch-student-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button-batch-student').attr('disabled',true);

			  document.getElementById("add-batch-student-form").submit();
		  });
		  $('#add-certificate-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button-add-certificate').attr('disabled',true);

			  var certificate = $('#certificate-name-arabic').val();
  			  if (arabicCheck(certificate)) {
				  document.getElementById("add-certificate-form").submit();
			  }
			  else {
				  toastr.error('Please check arabic certificate name',"");
				  $('#submit-button-add-certificate').attr('disabled',false);
			  }
		  });
		  $('#add-certificate-form-all').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button-add-certificate-all').attr('disabled',true);

			  var certificate = $('#certificate-name-arabic-all').val();
  			  if (arabicCheck(certificate)) {
				  document.getElementById("add-certificate-form-all").submit();
			  }
			  else {
				  toastr.error('Please check arabic certificate name',"");
				  $('#submit-button-add-certificate-all').attr('disabled',false);
			  }
		  });
		  $('#add-assignment-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button-assignment').attr('disabled',true);

			  var batch = $('#assignment-arabic').val();
  			  if (arabicCheck(batch)) {
				  document.getElementById("add-assignment-form").submit();
			  }
			  else {
				  toastr.error('Please check arabic assignment name',"");
				  $('#submit-button-assignment').attr('disabled',false);
			  }
		  });
		  $('#add-section-attachment-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button-section-attachment').attr('disabled',true);

			  document.getElementById("add-section-attachment-form").submit();
		  });
		  function arabicCheck(arabic)
  		  {
  			if (arabic == '') {
  				return true;
  			}
  			else {
  				var string = arabic.replace(/\s/g,'');
  				var isArabic =  /[\u0600-\u06FF\u0750-\u077F]/;
  				if (isArabic.test(string)){
  					 return true;
  				}
  				else {
  					return false;
  				}
  			}
  		  }
		  $(function(){
			  $(".chosen-select").chosen({disable_search_threshold: 10});
			  $('#datepicker').datetimepicker({
		        icons: {
		          time: "fa fa-clock-o",
		          date: "fa fa-calendar",
		          up: "fa fa-arrow-up",
		          down: "fa fa-arrow-down"
		        }
		      });
			  $('#datepicker-edit').datetimepicker({
		        icons: {
		          time: "fa fa-clock-o",
		          date: "fa fa-calendar",
		          up: "fa fa-arrow-up",
		          down: "fa fa-arrow-down"
		        }
		      });
		      $("#datepicker").on("dp.show",function (e) {
		        var newtop = $('.bootstrap-datetimepicker-widget').position().top - 45;
		        $('.bootstrap-datetimepicker-widget').css('top', newtop + 'px');
		      });
		      $('#datepicker-section').datetimepicker({
		          format:'DD/MM/YYYY hh:mm A',
		        icons: {
		          time: "fa fa-clock-o",
		          date: "fa fa-calendar",
		          up: "fa fa-arrow-up",
		          down: "fa fa-arrow-down"
		        }
		      });
			  $('#datepicker-section-edit').datetimepicker({
			      format:'DD/MM/YYYY hh:mm A',
		        icons: {
		          time: "fa fa-clock-o",
		          date: "fa fa-calendar",
		          up: "fa fa-arrow-up",
		          down: "fa fa-arrow-down"
		        }
		      });
		  });
		  function add_attachment(cs_id)
  	      {
  	          $('#cs_add_attachment_id').val(cs_id);
  	          $('#add-section-attachment').modal('show');
  	      }
  	      function view_attachment(cs_id)
  	      {
  	          $.ajax({
  	          	method: "POST",
  	          	url: "<?=site_url('admin/courses/getSectionAttachment');?>",
  	          	data : { cs_id : cs_id },
  	          	dataType : "json",
  	          	success : function( data ){
  	          	    $('#view-attachment-table').html(data.table);
    	            $('#view-section-attachment').modal('show');
  	          	}
  	          });
  	      }
		  function edit(cs_id)
  	      {
  	          $('#cs_id').val(cs_id);
  	          $.ajax({
  	          	method: "POST",
  	          	url: "<?=site_url('admin/courses/getSection');?>",
  	          	data : { cs_id : cs_id },
  	          	dataType : "json",
  	          	success : function( data ){
  	              $('#section-title').val(data.section_title);
				  $('#section-description').val(data.section_description);
				  $('#section-title-arabic-edit').val(data.section_title_arabic);
				  $('#section-description-arabic-edit').val(data.section_description_arabic);
				  $('#datepicker-section-edit').val(data.section_start_time);

  	              $('#edit-section').modal('show');
  	          	}
  	          });
  	      }
		  function editBatch(cb_id)
  	      {
  	          $('#cb_id').val(cb_id);
  	          $.ajax({
  	          	method: "POST",
  	          	url: "<?=site_url('admin/courses/getBatch');?>",
  	          	data : { cb_id : cb_id },
  	          	dataType : "json",
  	          	success : function( data ){
  	              $('#batch_name').val(data.batch_name);
				  $('#batch-name-edit').val(data.batch_name_arabic);

  	              $('#edit-batch').modal('show');
  	          	}
  	          });
  	      }
		  function editClass(cc_id)
  	      {
  	          $('#cc_id').val(cc_id);
  	          $.ajax({
  	          	method: "POST",
  	          	url: "<?=site_url('admin/courses/getClass');?>",
  	          	data : { cc_id : cc_id },
  	          	dataType : "json",
  	          	success : function( data ){
  	              $('#topic').val(data.topic);
				  $('#topic-arabic-edit').val(data.topic_arabic);
				  $('#cb_id').val(data.cb_id);
				  $('#cs_id').val(data.cs_id);
				  $('#trainer_id').val(data.trainer_id);
				  $('#datepicker-edit').val(data.starting_time);
				  $('#duration').val(data.duration);
				  $('#host_name').val(data.host_name);
				  $('#alternative_host').val(data.alternative_host);
				  $('#meeting_password').val(data.meeting_password);

  	              $('#edit-class').modal('show');
  	          	}
  	          });
  	      }
		  function addBatchStudents(cb_id,course_id)
		  {
			  $('#course_batch_id').val(cb_id);
			  $.ajax({
  	          	method: "POST",
  	          	url: "<?=site_url('admin/courses/getUnassignedStudents');?>",
  	          	data : { cb_id : cb_id , course_id : course_id },
  	          	dataType : "json",
  	          	success : function( data ){
  	              $('#input09').html(data.students);
				  $('#input09').trigger("chosen:updated");

  	              $('#add-batch-students').modal('show');
  	          	}
  	          });
		  }
		  function getBatchStudents(cb_id)
		  {
			  $('#course_batch_id').val(cb_id);
			  $.ajax({
  	          	method: "POST",
  	          	url: "<?=site_url('admin/courses/getBatchStudents');?>",
  	          	data : { cb_id : cb_id },
  	          	dataType : "json",
  	          	success : function( data ){
  	              $('#batch-students').html(data.table);

  	              $('#view-batch-students').modal('show');
  	          	}
  	          });
		  }
		  function removeBatchStudents(cb_id)
		  {
			  $('#course_batch_remove_id').val(cb_id);
			  $.ajax({
  	          	method: "POST",
  	          	url: "<?=site_url('admin/courses/getBatchStudentsRemove');?>",
  	          	data : { cb_id : cb_id },
  	          	dataType : "json",
  	          	success : function( data ){
					$('#input10').html(data.student);
  				  	$('#input10').trigger("chosen:updated");

    	            $('#remove-batch-students').modal('show');
  	          	}
  	          });
		  }
		  function addCertificate(stu_id)
  	      {
			  $('#stuu_id').val(stu_id);
  	          $('#add-certificate').modal('show');
  	      }
		  function getCertificate(stu_id,course_id)
		  {
			  $.ajax({
  	          	method: "POST",
  	          	url: "<?=site_url('admin/courses/getCertificate');?>",
  	          	data : { stu_id : stu_id , course_id : course_id },
  	          	dataType : "json",
  	          	success : function( data ){
					$('#certificate').html(data.table);

    	            $('#view-certificate').modal('show');
  	          	}
  	          });
		  }
		  $('#upload').on('change', function () {
		    var file = $("#upload")[0].files[0];
		    var val = file.type;
		    var type = val.substr(val.indexOf("/") + 1);
		    if (type == 'png' || type == 'jpg' || type == 'jpeg' || type == 'pdf' || type == 'doc' || type == 'dox') {
		        return true;
		    }
		    else {
		      document.getElementById("upload").value = "";
			  toastr.error("Failed to upload image, the format is not supported","");
		    }
		  });
		  $('#upload-all').on('change', function () {
		    var file = $("#upload-all")[0].files[0];
		    var val = file.type;
		    var type = val.substr(val.indexOf("/") + 1);
		    if (type == 'png' || type == 'jpg' || type == 'jpeg' || type == 'pdf') {
		        return true;
		    }
		    else {
		      document.getElementById("upload").value = "";
			  toastr.error("Failed to upload image, the format is not supported","");
		    }
		  });
	  </script>
	  <script>
	    function del_section()
          {
            if (confirm('Are you sure to delete this section?')) {
              return true;
            }
            else {
              return false;
            }
          }
          function getGrade(grade_id,grade)
		  {
			   $('#grade_id').val(grade_id);
			    $('#grade').val(grade);
			    $('#edit-grade').modal('show');
		  }
		  
		  function addGrade(stu_id)
  	      {
			  $('#stuuu_id').val(stu_id);
			 // $('#courc_id').val(course_id);
  	          $('#add-grade').modal('show');
  	      }
	</script>
   </body>
</html>
