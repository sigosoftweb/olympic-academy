<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('admin/includes/includes.php'); ?>
	  <?php $this->load->view('admin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i> Students assignments</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>All sections</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">

		                   <div class="tile-header transparent">
		                     <span class="note"></span>

		                   </div>

		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom" id="user_data">
								   <thead>
									 <th width="10%">Student</th>
  									 <th width="30%">Answer</th>
  									 <th width="10%">Attachment</th>
  		                           </thead>
								   <tbody>
									   <?php foreach ($assignments as $assignment) { ?>
										   <tr>
											   <td><?=$assignment->stu_name_english . ' ' . $assignment->stu_name_arabic?></td>
											   <td><?=$assignment->answer?></td>
											   <td>
												   <?php if ($assignment->answer_attachment != '') { ?>
													   <a class="btn btn-primary btn-xs" href="<?=base_url() . $assignment->answer_attachment?>" target="_blank">View</a>
												   <?php }else { ?>
													   No attachments
												   <?php } ?>
											   </td>
										   </tr>
									   <?php } ?>
								   </tbody>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <?php $this->load->view('admin/includes/scripts.php'); ?>
	  <?php $this->load->view('admin/includes/table-script.php'); ?>
   </body>
</html>
