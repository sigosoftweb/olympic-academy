<!DOCTYPE html>

<html>

   <head>

      <title><?=$title?></title>

      <?php $this->load->view('admin/includes/includes.php'); ?>

   </head>

   <body class="solid-bg-6">

      <div class="mask">

         <div id="loader"></div>

      </div>

      <div id="wrap">

         <div class="row">

            <?php $this->load->view('admin/includes/sidebar.php'); ?>

            <div id="content" class="col-md-12">

               <div class="pageheader">

                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i>Settings</h2>

                  <div class="breadcrumbs">

                     <ol class="breadcrumb">

                        <li>Olymbic</li>

                        <li>Settings</li>

                     </ol>

                  </div>

               </div>

               <div class="main">

                  <div class="row">

                     <div class="col-md-12">

                        <section class="tile color transparent-black">

                           <div class="tile-body">

                              <form action="<?=site_url('admin/settings/updatePassword')?>" method="post">

								  <div class="row">

									  <div class="col-sm-12">

									  	<h4>Admin credentials</h4><hr>

									  </div>

	                                 <div class="form-group col-sm-3">

	                                    <label for="exampleInputCountry">Username</label>

	                                    <input type="text" class="form-control" name="username" value="<?=$admin->username?>" required>

	                                 </div>

	                                 <div class="form-group col-sm-3">

	                                    <label for="exampleInputCountry">New Password</label>

	                                    <input type="text" class="form-control" name="password" required>

	                                 </div>

									 <div class="form-group col-sm-3">

										 <button type="submit" class="btn btn-primary" id="submit-button" style="margin-top:25px;">Update</button>

	                                 </div>

	                              </div>

                              </form>

							  <form action="<?=site_url('admin/settings/updateSettings')?>" method="post">

								  <div class="row">

      									  <div class="col-sm-12">

      									  	<h4>Contact info</h4><hr>

      									  </div>

	                                 <div class="form-group col-sm-3">

	                                    <label for="exampleInputCountry">Address</label>

	                                    <input type="text" class="form-control" name="address" value="<?=$setting->address?>" required>

	                                 </div>

                                    <div class="form-group col-sm-3">

                                       <label for="exampleInputCountry">Address (Arabic)</label>

                                       <input type="text" class="form-control" name="address_arabic" value="<?=$setting->address_arabic?>" required>

                                    </div>
	                                 <div class="form-group col-sm-3">

	                                    <label for="exampleInputCountry">Email address</label>

	                                    <input type="text" class="form-control" name="email" value="<?=$setting->email?>" required>

	                                 </div>

									         <div class="form-group col-sm-3">

	                                    <label for="exampleInputCountry">Contact number</label>

	                                    <input type="text" class="form-control" name="mobile" value="<?=$setting->mobile?>" required>

	                                 </div>

									         
                                    <div class="form-group col-sm-12">

                                       <button type="submit" class="btn btn-primary" id="submit-button" style="margin-top:15px; float: right;">Update</button>

                                   </div>

	                              </div>

                              </form>

                           </div>

                        </section>

                     </div>

                  </div>

               </div>

            </div>

         </div>

      </div>

      <?php $this->load->view('admin/includes/scripts.php'); ?>

   </body>

</html>

