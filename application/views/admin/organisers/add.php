<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('admin/includes/includes.php'); ?>
	  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/image-crop/croppie.css">
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-users" style="line-height: 48px;padding-left: 2px;"></i>Add Organiser</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Add Organiser</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
                              <form action="<?=site_url('admin/organisers/addOrganiser')?>" method="post" id="add-form" enctype="multipart/form-data">
								  <div class="row">
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">First name</label>
	                                    <input type="text" class="form-control" name="organiser_first" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Second name</label>
	                                    <input type="text" class="form-control" id="name-arabic" name="organiser_second">
	                                 </div>
									 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">First name ( Arabic )</label>
	                                    <input type="text" class="form-control" name="first_arabic" id="first_arabic" required>
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Second name ( Arabic )</label>
	                                    <input type="text" class="form-control" name="second_arabic" id="second_arabic">
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCity">CPR</label>
	                                    <input type="text" class="form-control" name="organiser_cpr">
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Education</label>
	                                    <input type="text" name="organiser_education" class="form-control">
	                                 </div>
									 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Education ( Arabic )</label>
	                                    <input type="text" name="organiser_education_arabic" id="organiser_education_arabic" class="form-control">
	                                 </div>
									 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Experience</label>
	                                    <input type="text" name="organiser_experience" class="form-control">
	                                 </div>
									 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Experience ( Arabic )</label>
	                                    <input type="text" name="organiser_experience_arabic" id="organiser_experience_arabic" class="form-control">
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputAddress">Mobile number</label>
	                                    <input type="text" name="organiser_mobile" id="mobile-number" class="form-control"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Email address</label>
	                                    <input type="email" name="organiser_email" class="form-control">
	                                 </div>
									 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCity">Address</label>
	                                    <textarea name="organiser_contact" rows="3" cols="80" class="form-control"></textarea>
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCity">organiser image</label>
	                                    <input type="file" name="image" id="upload" class="form-control">
	                                 </div>
									 <div class="col-md-12 col-sm-12 form-group">
					                   <div class="upload-div" style="display:none;">
					                     <div id="upload-demo"></div>
					                     <div class="col-12 text-center">
					                       <a href="#" class="btn-fill-lg bg-blue-dark btn-hover-yellow" style="border-radius : 5px;" id="crop-button">Crop</a>
					                     </div>
					                   </div>
					                   <div class="upload-result" id="upload-result" style="display : none; margin-bottom:10px;">

					                   </div>
					                   <input type="hidden" name="image" id="ameimg" >
					                 </div>
	                              </div>
	                              <div class="row">
	                                 <div class="form-group col-sm-4">
										 <button type="submit" class="btn btn-primary" id="submit-button">Add</button>
	                                 </div>
	                              </div>
                              </form>
                           </div>
                        </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
      <?php $this->load->view('admin/includes/table-script.php'); ?>
	  <script src="<?=base_url()?>assets/plugins/image-crop/croppie.js"></script>
	  <script type="text/javascript">
		  $uploadCrop = $('#upload-demo').croppie({
			enableExif: true,
			viewport: {
				width: 400,
				height: 400,
				type: 'rectangle'
			},
			boundary: {
				width: 800,
				height: 800
			}
		  });


		 $('#upload').on('change', function () {
		  $("#submit-button").css("display", "none");
		  var file = $("#upload")[0].files[0];
		  var val = file.type;
		  var type = val.substr(val.indexOf("/") + 1);
		  if (type == 'png' || type == 'jpg' || type == 'jpeg') {

			$("#current-image").css("display", "none");
			$("#submit-button").css("display", "none");

			$(".upload-div").css("display", "block");
			$("#submit-button").css("display", "none");
			var reader = new FileReader();
			  reader.onload = function (e) {
				$uploadCrop.croppie('bind', {
				  url: e.target.result
				}).then(function(){
				  console.log('jQuery bind complete');
				});

			  }
			  reader.readAsDataURL(this.files[0]);
		  }
		  else {
			alert('This file format is not supported.');
			document.getElementById("upload").value = "";
			$("#upload-result").css("display", "none");
			$("#submit-button").css("display", "none");
			$("#current-image").css("display", "block");
			$('#ameimg').val('');
		  }
		});


		$('#crop-button').on('click', function (ev) {
			$("#submit-button").css("display", "block");
		  $uploadCrop.croppie('result', {
			type: 'canvas',
			size: 'viewport'
		  }).then(function (resp) {
			html = '<img src="' + resp + '" />';
			$("#upload-result").html(html);
			$("#upload-result").css("display", "block");
			$(".upload-div").css("display", "none");
			$("#submit-button").css("display", "block");
			$('#ameimg').val(resp);
		  });
		});
		  $('#add-form').on('submit', function(e){
			  e.preventDefault();
			  toastr.clear();
			  $('#submit-button').attr('disabled',true);

			  var first = $('#first_arabic').val();
  			  var second = $('#second_arabic').val();
			  var education = $('#organiser_education_arabic').val();
  			  var experience = $('#organiser_experience_arabic').val();
  			  if (arabicCheck(first) && arabicCheck(second) && arabicCheck(education) && arabicCheck(experience)) {
				  var mobile = $('#mobile-number').val();
				  if (mobile.length < 8 || mobile.length >12) {
					  if ( mobile.length < 8 || mobile.length >12 ) {
						  toastr.error('Mobile number should contains 8 - 12 numbers');
					  }
					  $('#submit-button').attr('disabled',false);
				  }
				  else {
					  document.getElementById("add-form").submit();
				  }
			  }
			  else {
				  if (!arabicCheck(first)) {
					  toastr.error('Please check arabic first name',"");
				  }
				  if (!arabicCheck(second)) {
					  toastr.error('Please check arabic second name',"");
				  }
				  if (!arabicCheck(education)) {
					  toastr.error('Please check arabic education',"");
				  }
				  if (!arabicCheck(experience)) {
					  toastr.error('Please check arabic experience',"");
				  }
				  $('#submit-button').attr('disabled',false);
			  }
		  });
		  function arabicCheck(arabic)
  		  {
  			if (arabic == '') {
  				return true;
  			}
  			else {
  				var string = arabic.replace(/\s/g,'');
  				var isArabic =  /[\u0600-\u06FF\u0750-\u077F]/;
  				if (isArabic.test(string)){
  					 return true;
  				}
  				else {
  					return false;
  				}
  			}
  		  }
	  </script>
   </body>
</html>
