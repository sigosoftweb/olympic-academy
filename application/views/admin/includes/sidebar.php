<div class="navbar navbar-default navbar-fixed-top navbar-transparent-black mm-fixed-top" role="navigation" id="navbar">
   <div class="navbar-header col-md-2">
      <a class="navbar-brand" href="<?=site_url('admin/dashboard')?>">
      <strong>OLYMPIC</strong>ACADEMY
      </a>
      <div class="sidebar-collapse">
         <a href="#"><i class="fa fa-bars"></i></a>
      </div>
   </div>
   <div class="navbar-collapse">
      <ul class="nav navbar-nav refresh">
         <li class="divided">
            <a href="#" class="page-refresh"><i class="fa fa-refresh"></i></a>
         </li>
      </ul>
      <ul class="nav navbar-nav quick-actions">
         <li class="dropdown divided user" id="current-user">
            <div class="profile-photo">
               <img src="assets/images/profile-photo.jpg" alt />
            </div>
            <a class="dropdown-toggle options" data-toggle="dropdown" href="#">Olymbic academy <i class="fa fa-caret-down"></i></a>
            <ul class="dropdown-menu arrow settings">
               <li>
                  <a href="<?=site_url('Admin_Login/logout')?>"><i class="fa fa-power-off"></i> Logout</a>
               </li>
            </ul>
         </li>
      </ul>
      <ul class="nav navbar-nav side-nav" id="sidebar">
      <li class="collapsed-content">
         <ul>
            <li class="search">

            </li>
         </ul>
      </li>
      <li class="navigation" id="navigation">
         <a href="#" class="sidebar-toggle" data-toggle="#navigation">Navigation <i class="fa fa-angle-up"></i></a>
         <ul class="menu">
            <li>
               <a href="<?=site_url('admin/dashboard')?>"><i class="fa fa-tachometer"></i> Dashboard</a>
            </li>
			<li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Students <b class="fa fa-plus dropdown-plus"></b></a>
               <ul class="dropdown-menu">
                  <li>
                     <a href="<?=site_url('admin/students')?>"><i class="fa fa-caret-right"></i> Manage students</a>
                  </li>
      <!--            <li>-->
      <!--               <a href="<?=site_url('admin/students/approvals')?>"><i class="fa fa-caret-right"></i> Approvals</a>-->
      <!--            </li>-->
				  <!--<li>-->
      <!--               <a href="<?=site_url('admin/students/rejected')?>"><i class="fa fa-caret-right"></i> Rejected Students</a>-->
      <!--            </li>-->
                  <li>
                     <a href="<?=site_url('admin/students/add')?>"><i class="fa fa-caret-right"></i> Add student</a>
                  </li>
               </ul>
            </li>
			<li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-users"></i> Trainers <b class="fa fa-plus dropdown-plus"></b></a>
               <ul class="dropdown-menu">
                  <li>
                     <a href="<?=site_url('admin/trainers')?>"><i class="fa fa-caret-right"></i> Manage trainers</a>
                  </li>
                  <li>
                     <a href="<?=site_url('admin/trainers/add')?>"><i class="fa fa-caret-right"></i> Add trainer</a>
                  </li>
               </ul>
            </li>
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Subscriptions <b class="fa fa-plus dropdown-plus"></b></a>
               <ul class="dropdown-menu">
                  <li>
                     <a href="<?=site_url('admin/subscriptions/approved')?>"><i class="fa fa-caret-right"></i> Approved subscriptions</a>
                  </li>
                  <li>
                     <a href="<?=site_url('admin/subscriptions/pending')?>"><i class="fa fa-caret-right"></i> Pending subscriptions</a>
                  </li>
				  <li>
                     <a href="<?=site_url('admin/subscriptions/rejected')?>"><i class="fa fa-caret-right"></i> Rejected subscriptions</a>
                  </li>
               </ul>
            </li>
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-users"></i> Organisers <b class="fa fa-plus dropdown-plus"></b></a>
               <ul class="dropdown-menu">
                  <li>
                     <a href="<?=site_url('admin/organisers')?>"><i class="fa fa-caret-right"></i> Manage organiser</a>
                  </li>
                  <li>
                     <a href="<?=site_url('admin/organisers/add')?>"><i class="fa fa-caret-right"></i> Add organiser</a>
                  </li>
               </ul>
            </li>
            <li>
               <a href="<?=site_url('admin/courses')?>"><i class="fa fa-book"></i> Courses</a>
            </li>
			<!--<li class="dropdown">-->
   <!--            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-book"></i> Courses <b class="fa fa-plus dropdown-plus"></b></a>-->
   <!--            <ul class="dropdown-menu">-->
   <!--               <li>-->
   <!--                  <a href="<?=site_url('admin/courses')?>"><i class="fa fa-caret-right"></i> Manage course</a>-->
   <!--               </li>-->
   <!--               <li>-->
   <!--                  <a href="<?=site_url('admin/courses/sections')?>"><i class="fa fa-caret-right"></i> Manage sections</a>-->
   <!--               </li>-->
			<!--	  <li>-->
   <!--                  <a href="<?=site_url('admin/courses/classes')?>"><i class="fa fa-caret-right"></i> Manage online class</a>-->
   <!--               </li>-->
   <!--            </ul>-->
   <!--         </li>-->
			<li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i> Events <b class="fa fa-plus dropdown-plus"></b></a>
               <ul class="dropdown-menu">
                  <li>
                     <a href="<?=site_url('admin/events/upcoming')?>"><i class="fa fa-caret-right"></i> Upcoming events</a>
                  </li>
                  <li>
                     <a href="<?=site_url('admin/events/completed')?>"><i class="fa fa-caret-right"></i> Completed events</a>
                  </li>
               </ul>
            </li>
            <li>
               <a href="<?=site_url('admin/sliders')?>"><i class="fa fa-picture-o"></i> Sliders</a>
            </li>
			<li>
               <a href="<?=site_url('admin/notes')?>"><i class="fa fa-comments-o"></i> Notice</a>
            </li>
            <li>
               <a href="<?=site_url('admin/resources')?>"><i class="fa fa-file"></i> Resources</a>
            </li>
             <li>
               <a href="<?=site_url('admin/reports')?>"><i class="fa fa-file"></i> Reports</a>
            </li>
			<li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-dollar"></i> Finance management <b class="fa fa-plus dropdown-plus"></b></a>
               <ul class="dropdown-menu">
                  <li>
                     <a href="<?=site_url('admin/finance')?>"><i class="fa fa-caret-right"></i> All transactions</a>
                  </li>
                  <li>
                     <a href="<?=site_url('admin/finance/filter')?>"><i class="fa fa-caret-right"></i>Date Filter</a>
                  </li>
                  <li>
                     <a href="<?=site_url('admin/finance/course')?>"><i class="fa fa-caret-right"></i>Course Filter</a>
                  </li>
               </ul>
            </li>
            <!--<li>-->
            <!--   <a href="<?=site_url('admin/previleges')?>"><i class="fa fa-comments-o"></i> Subadmin Previleges</a>-->
            <!--</li>-->
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Subadmin <b class="fa fa-plus dropdown-plus"></b></a>
               <ul class="dropdown-menu">
                  <li>
                     <a href="<?=site_url('admin/subadmin')?>"><i class="fa fa-caret-right"></i> Manage admin</a>
                  </li>
                  <li>
                     <a href="<?=site_url('admin/subadmin/add')?>"><i class="fa fa-caret-right"></i> Add admin</a>
                  </li>
               </ul>
           </li>
           <li>
               <a href="<?=site_url('admin/team')?>"><i class="fa fa-users"></i> Our team</a>
            </li>
           <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-square-o"></i> Category settings <b class="fa fa-plus dropdown-plus"></b></a>
               <ul class="dropdown-menu">
                  <!--<li>-->
                  <!--   <a href="<?=site_url('admin/educations')?>"><i class="fa fa-caret-right"></i> Educations</a>-->
                  <!--</li>-->
                  <li>
                     <a href="<?=site_url('admin/sports')?>"><i class="fa fa-caret-right"></i> Sports</a>
                  </li>
				  <li>
                     <a href="<?=site_url('admin/organizations')?>"><i class="fa fa-caret-right"></i> Work organizations</a>
                  </li>
				  <li>
                     <a href="<?=site_url('admin/categories')?>"><i class="fa fa-caret-right"></i> Course categories</a>
                  </li>
               </ul>
            </li>
			<li>
               <a href="<?=site_url('admin/settings')?>"><i class="fa fa-book"></i> Settings</a>
            </li>
         </ul>
   </div>
</div>
