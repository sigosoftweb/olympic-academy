<script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>

<script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>

<?php /*<script src="<?=base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>*/?>
<script src="https://cdn.datatables.net/buttons/1.6.3/js/dataTables.buttons.min.js"></script>

<script src="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap4.min.js"></script>

<script src="<?=base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>

<script src="<?=base_url()?>assets/plugins/datatables/jszip.min.js"></script>

<script src="<?=base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>

<?php /*<script src="<?=base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>*/?>
<script src="https://cdn.datatables.net/buttons/1.6.3/js/buttons.html5.min.js"></script>

<script src="<?=base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>

<script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>

<script src="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

<script type="text/javascript">

    $(document).ready(function() {

        $('#datatable').DataTable({

            "order" : []

        });



        var table = $('#datatable-buttons').DataTable({

            lengthChange: false,

            buttons: ['csv', 'pdf']

        });



        table.buttons().container()

                .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

    } );

    $('#datatable-export').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'excel'
        ]
    });

</script>

