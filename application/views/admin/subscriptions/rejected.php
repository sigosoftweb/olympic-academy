<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('admin/includes/includes.php'); ?>
	  <?php $this->load->view('admin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-users" style="line-height: 48px;padding-left: 2px;"></i> Rejected Subscriptions</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Rejected Subscriptions</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">


		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>

		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom" id="user_data">
								   <thead>
  		                           <tr>
									 <th width="10%">Student</th>
  		                             <th width="10%">Contact</th>
									 <th width="10%">Course</th>
  		                             <th width="10%">Subscribed at</th>
  		                             <th width="10%">Amount</th>
									 <th width="20%">Reason</th>
									 <th width="10%">Options</th>
  		                           </tr>
  		                         </thead>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
	  <?php $this->load->view('admin/includes/table-script.php'); ?>
	  <script type="text/javascript">
	    $(document).ready(function(){
        var tbl = $('.my-table');
        var settings = {
          "processing":true,
          "serverSide":true,
          "order":[],
          "ajax":{
            url:"<?=site_url('admin/subscriptions/getRejected')?>",
            type:"POST"
          },
          "columnDefs":[
            {
              "target":[0,3,4],
              "orderable":true
            }
          ],
          dom: 'lBfrtip',
          buttons: [
              {
                  extend:'pdfHtml5',
                  text:'Pdf',
                  exportOptions: {
                    columns: [ 1, 2, 3, 4, 5 ]
                  },
                  orientation:'landscape',
                  customize: function (doc) {
                    doc.content[1].table.widths =
                        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                  }
              },
              {
                  extend: 'print',
                  text: 'Print',
                  exportOptions: {
                    columns: [ 1, 2, 3, 4, 5 ]
                  },
              },
              { extend: 'csv',text: 'Csv' },
          ],
          lengthMenu: [[25, 100, -1], [25, 100, "All"]],
          pageLength: 25,
          customize: function (doc) {
              doc.content[1].table.widths =
                  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        };
      var dataTable = $('#user_data').DataTable(settings);

    });
	function confirmAction(param)
	{
		if (param == 0) {
			var message = 'Are you sure to accept this subscription?';
		}
		else {
			var message = 'Are you sure to reject this subscription?';
		}
		return confirm(message);
	}
	  </script>
   </body>
</html>
