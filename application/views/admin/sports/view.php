<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('admin/includes/includes.php'); ?>
	  <?php $this->load->view('admin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-square-o" style="line-height: 48px;padding-left: 2px;"></i> Sports</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Sports</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">


		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">
								 <button type="button" class="btn btn-default btn-lg margin-bottom-20" data-toggle="modal" data-target="#add-sport">Add sports</button>
		                     </div>
		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom" id="datatable">
								   <thead>
  		                           <tr>
  		                             <th width="40%">sport</th>
  		                             <th width="5%">Status</th>
  		                             <th width="5%">Edit</th>
  		                           </tr>
  		                         </thead>
  		                         <tbody>
									 <?php foreach ($sports as $sport) { ?>
									 	<tr>
									 		<td><?=$sport->sport?></td>
											<td>
												<?php if ($sport->status) { ?>
													<a href="<?=site_url('admin/sports/status/' . $sport->sports_id . '/0')?>" class="btn btn-danger btn-xs">Block</a><br>Active
												<?php }else { ?>
													<a href="<?=site_url('admin/sports/status/' . $sport->sports_id . '/1')?>" class="btn btn-success btn-xs">Activate</a><br>Blocked
												<?php } ?>
											</td>
											<td><button type="button" class="btn btn-primary btn-xs margin-bottom-20" onclick="edit(<?=$sport->sports_id?>)">Edit</button></td>
									 	</tr>
									 <?php } ?>
  		                         </tbody>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <div class="modal fade" id="add-sport" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
              <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> sport</h3>
            </div>
            <div class="modal-body">
              <form role="form" id="add-form" action="<?=site_url('admin/sports/add')?>" method="post">

                <div class="form-group">
                  <label for="placeholderInput">sport</label>
                  <input type="text" name="sport" class="form-control" placeholder="sport name..." onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
                </div>

            </div>
            <div class="modal-footer">
              <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
              <button type="submit" class="btn btn-green" id="submit-button">Save changes</button>
            </div>
			</form>
          </div>
        </div>
      </div>
	  <div class="modal fade" id="edit-sport" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
              <h3 class="modal-title" id="modalConfirmLabel"><strong>Edit</strong> sport</h3>
            </div>
            <div class="modal-body">
              <form role="form" id="edit-form" action="<?=site_url('admin/sports/edit')?>" method="post">
				  <input type="hidden" name="sports_id" id="sports_id">
                <div class="form-group">
                  <label for="placeholderInput">sport</label>
                  <input type="text" name="sport" class="form-control" id="sport" placeholder="sport name..." onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
                </div>

            </div>
            <div class="modal-footer">
              <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
              <button type="submit" class="btn btn-green" id="submit-button-edit">Update</button>
            </div>
			</form>
          </div>
        </div>
      </div>
      <section class="videocontent" id="video"></section>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
	  <?php $this->load->view('admin/includes/table-script.php'); ?>
	  <script>
	  	$('#add-form').on('submit', function(e){
		  e.preventDefault();
		  $('#submit-button').attr('disabled',true);
		  document.getElementById("add-form").submit();
	  	});
		function edit(sports_id)
	    {
	        $('#sports_id').val(sports_id);
	        $.ajax({
	          method: "POST",
	          url: "<?=site_url('admin/sports/getDetails');?>",
	          data : { sports_id : sports_id },
	          dataType : "json",
	          success : function( data ){
	              $('#sport').val(data.sport);
	              $('#edit-sport').modal('show');
	          }
	        });
	    }
	  </script>
   </body>
</html>
