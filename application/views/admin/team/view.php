<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('admin/includes/includes.php'); ?>
	  <?php $this->load->view('admin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i> Our team</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Our team</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">


		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">
								 <button class="btn btn-default btn-lg margin-bottom-20" onclick="window.location='<?=site_url('admin/team/add')?>'"><span>Add member</span></button>
		                     </div>
		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom my-table" id="datatable">
								   <thead>
  		                           <tr>
									 <th width="10%">Image</th>
  		                             <th width="10%">Name</th>
									 <th width="10%">Name arabic</th>
									 <th width="10%">Designation</th>
									 <th width="10%">Designation arabic</th>
									 <th width="5%">Status</th>
									 <th width="5%">Edit</th>
									 <th width="5%">Delete</th>
  		                           </tr>
  		                         </thead>
								 <tbody>
									 <?php foreach ($team as $tea) { ?>
									 	<tr>
											<td><img src="<?=base_url() . $tea->team_image?>" alt="Team image" height="100px"></td>
									 		<td><?=$tea->team_first . ' ' . $tea->team_second?></td>
											<td><?=$tea->first_arabic . ' ' . $tea->second_arabic?></td>
											<td><?=$tea->team_designation?></td>
											<td><?=$tea->designation_arabic?></td>
											<td>
												<?php if ($tea->team_status) { ?>
													<a href="<?=site_url('admin/team/status/' . $tea->team_id . '/0')?>" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure to block this member?')">Block</a><br>Active
												<?php }else { ?>
													<a href="<?=site_url('admin/team/status/' . $tea->team_id . '/1')?>" class="btn btn-success btn-xs" onclick="return confirm('Are you sure to unblock this member?')">Activate</a><br>Blocked
												<?php } ?>
											</td>
											<td><a href="<?=site_url('admin/team/edit/' . $tea->team_id)?>" class="btn btn-primary btn-xs margin-bottom-20">Edit</a></td>
											<td><a href="<?=site_url('admin/team/delete/' . $tea->team_id)?>" onclick="return confirm('Are you sure to delete this member?')" class="btn btn-danger btn-xs margin-bottom-20">Delete</a></td>
									 	</tr>
									 <?php } ?>
  		                         </tbody>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <?php $this->load->view('admin/includes/scripts.php'); ?>
	  <?php $this->load->view('admin/includes/table-script.php'); ?>
   </body>
</html>
