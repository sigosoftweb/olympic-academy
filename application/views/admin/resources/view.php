<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('admin/includes/includes.php'); ?>
	  <?php $this->load->view('admin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-graduation-cap" style="line-height: 48px;padding-left: 2px;"></i> Resources</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Resources</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">


		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">
								 <button type="button" class="btn btn-default btn-lg margin-bottom-20" data-toggle="modal" data-target="#add-resource">Add resource</button>
		                     </div>
		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
								 <table id="datatable"  class="table table-datatable table-custom">
  								   <thead>
  									   <tr>
  										 <th width="10%">Title</th>
										 <th width="10%">Title Arabic</th>
  										 <th width="10%">Type</th>
  										 <th width="10%">Attachment</th>
  										 <th width="5%">Status</th>
  										 <th width="5%">Delete</th>
  									   </tr>
  								   </thead>
  								   <tbody>
  									   <?php foreach ($resources as $res) { ?>
  										   <tr>
  											   <td><?=$res->cr_title?></td>
											   <td><?=$res->cr_title_arabic?></td>
  											   <td><?=$res->cr_type?></td>
  											   <td>
  												   <a href="<?=base_url() . $res->cr_attachment?>" target="_blank" class="btn btn-secondary btn-xs">View</a>
  											   </td>
  											   <td>
  												   <?php if ($res->cr_status) { ?>
  													   Active<br><a class="btn btn-danger btn-xs margin-bottom-20" href="<?=site_url('admin/resources/statusResource/' . $res->cr_id . '/0')?>">Block</a>
  												   <?php }else { ?>
  													   Blocked<br><a class="btn btn-success btn-xs margin-bottom-20" href="<?=site_url('admin/resources/statusResource/' . $res->cr_id . '/1')?>">Activate</a>
  												   <?php } ?>
  											   </td>
  											   <td><a class="btn btn-danger btn-xs margin-bottom-20" href="<?=site_url('admin/resources/deleteResource/' . $res->cr_id)?>" onclick="return confirm('Are you sure to delete this resource?')">Delete</a></td>
  										   </tr>
  									   <?php } ?>
  								   </tbody>
  							   </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <div class="modal fade" id="add-resource" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
		<div class="modal-dialog madal-lg">
		  <div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
			  <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> Resource</h3>
			</div>
			<div class="modal-body">
			  <form role="form" id="add-resource-form" action="<?=site_url('admin/resources/addResource')?>" method="post" enctype="multipart/form-data">
				 <div class="row">
					 <div class="form-group col-sm-12">
					   <label for="exampleInputCountry">Resource title</label>
					   <input type="text" class="form-control" name="title" required>
					 </div>
					 <div class="form-group col-sm-12">
					   <label for="exampleInputCountry">Resource title ( Arabic )</label>
					   <input type="text" class="form-control" name="title_arabic" id="title_arabic" required>
					 </div>
					 <div class="form-group col-sm-12">
					   <label for="exampleInputCountry">Attachment type</label>
					   <select class="form-control" name="type">
						   <option value="Document">Document</option>
						   <option value="Image">Image</option>
						   <option value="Video">Video</option>
					   </select>
					 </div>
					 <div class="form-group col-sm-12">
					   <label for="exampleInputCountry">Choose attachment</label>
					   <input type="file" name="file" id="upload" class="form-control">
					 </div>
				 </div>
			</div>
			<div class="modal-footer">
			  <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
			  <button type="submit" class="btn btn-green" id="submit-button-resource">Add</button>
			</div>
		   </form>
		  </div>
		</div>
	  </div>
      <section class="videocontent" id="video"></section>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
	  <?php $this->load->view('admin/includes/table-script.php'); ?>
	  <script>
		  $('#add-resource-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button-resource').attr('disabled',true);
			  var arabic_title = $('#title_arabic').val();
			  if (arabicCheck(arabic_title)) {
				  document.getElementById("add-resource-form").submit();
			  }
			  else {
				  toastr.error('Please check arabic title',"");
				  $('#submit-button-resource').attr('disabled',false);
			  }
		  });
		  function arabicCheck(arabic)
		  {
			  var string = arabic.replace(/\s/g,'');
			  var isArabic =  /[\u0600-\u06FF\u0750-\u077F]/;
			  if (isArabic.test(string)){
				   return true;
			  }
			  else {
				  return false;
			  }
		  }
		  $('#upload-resource').on('change', function () {
			var file = $("#upload")[0].files[0];
			var val = file.type;
			var type = val.substr(val.indexOf("/") + 1);
			if (type == 'png' || type == 'jpg' || type == 'jpeg' || type == 'pdf' || type == 'mp4' || type == 'avg' || type == 'doc' || type == 'docx' || type == 'xls' || type == 'xlsx' || type == 'txt' || type == 'ppt') {
				return true;
			}
			else {
			  document.getElementById("upload").value = "";
			  toastr.error("Failed to upload image, the format is not supported","");
			}
		  });
	  </script>
   </body>
</html>
