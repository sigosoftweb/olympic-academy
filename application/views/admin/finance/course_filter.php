<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('admin/includes/includes.php'); ?>
      <?php $this->load->view('admin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-users" style="line-height: 48px;padding-left: 2px;"></i>Course Filter</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Course Filter</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
                              <form action="<?=site_url('admin/finance/course')?>" method="post" id="add-form">
								  <div class="row">
									 <div class="form-group col-sm-3">
 	                                    <label for="exampleInputCountry">Choose course</label>
 	                                    <select class="form-control" name="course_id">
 											<?php foreach ($courses as $course) { ?>
 												<option value="<?=$course->course_id?>" <?php if($course_id == $course->course_id){ ?>selected<?php } ?>><?=$course->course_title?></option>
 											<?php } ?>
 	                                    </select>
 	                                 </div>
	                                 <div class="form-group col-sm-2">
	                                    <label for="exampleInputCountry">From date</label>
	                                    <input type="date" class="form-control" name="from" value="<?=$from?>">
	                                 </div>
	                                 <div class="form-group col-sm-2">
	                                    <label for="exampleInputCountry">To date</label>
	                                    <input type="date" class="form-control" name="to" value="<?=$to?>">
	                                 </div>
									 <div class="form-group col-sm-3">
	                                    <label for="exampleInputCountry">Payment type</label>
	                                    <select class="form-control" name="payment_type">
											<option value="all" <?php if($payment_type == 'all'){ ?>selected<?php } ?>>All</option>
											<option value="Online" <?php if($payment_type == 'Online'){ ?>selected<?php } ?>>Online payment</option>
											<option value="Tamkeen" <?php if($payment_type == 'Tamkeen'){ ?>selected<?php } ?>>Tamkeen</option>
											<option value="By cash" <?php if($payment_type == 'By cash'){ ?>selected<?php } ?>>By cash</option>
	                                    </select>
	                                 </div>
									 <div class="form-group col-sm-2">
										 <button type="submit" class="btn btn-primary" id="submit-button" style="margin-top:30px;">Filter</button>
	                                 </div>
	                              </div>
                              </form>
                           </div>
                        </section>
                     </div>
                  </div>
               </div>
			   <?php if ($param) { ?>
			        <?php 
			            if(!$from){$from = '0';}
			            if(!$to){$to = '0';}
			        ?>
				   <div class="main">
	                  <div class="row">
	                     <div class="col-md-12">
	                        <section class="tile color transparent-black">
	                           <div class="tile-body">
	                               <a href="<?=site_url('admin/finance/export_course_filter/' . $course_id . '/' . $from . '/' . $to . '/' . $payment_type)?>" class="btn btn-primary btn-sm margin-bottom-20 pull-right">Export</a><br>
								   <div class="table-responsive">
	  		                       <table  class="table table-datatable table-custom" id="datatable">
	  								   <thead>
	    		                           <tr>
	  									 	 <th width="10%">Student</th>
	    		                             <th width="10%">Contact</th>
	  									  	 <th width="10%">Course</th>
	    		                             <th width="10%">Subscribed at</th>
	    		                             <th width="5%">Amount</th>
	  									 	 <th width="10%">Payment ID</th>
											 <th width="10%">Payment type</th>
	    		                           </tr>
	    		                         </thead>
										 <tbody>
										 	<?php foreach ($transactions as $trans) { ?>
										 		<tr>
										 			<td><?=$trans->stu_name_english . '<br>' . $trans->stu_name_arabic?></td>
													<td><?=$trans->stu_mobile . '<br>' . $trans->stu_email?></td>
													<td><?=$trans->course_title?></td>
													<td><?=date('d/m/Y h:i A',strtotime($trans->date))?></td>
													<td><?=$trans->amount?></td>
													<td><?=$trans->payment_id?></td>
													<td><?=$trans->payment_type?></td>
										 		</tr>
										 	<?php } ?>
										 </tbody>
	  		                       </table>
	  		                     </div>
	                           </div>
	                        </section>
	                     </div>
	                  </div>
	               </div>
			   <?php } ?>
            </div>
         </div>
      </div>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
      <?php $this->load->view('admin/includes/table-script.php'); ?>
   </body>
</html>
