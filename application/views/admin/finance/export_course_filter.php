<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <style>
         th, td {
            padding: 15px;
            text-align: left;
         }
         table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 16px !important;
         }
         th {
            background-color: #d9d9d9;
         }
         table tr {
            line-height: 14px !important;
         }
      </style>
   </head>
   <body>
      <div id="container">
         <center>
            <h3 style="text-align: center">FINANCE REPORT</h3>
         </center>
         <p style="text-align: left"><strong>Course: </strong><?=$course?></p>
         <?php if($from){ ?>
            <p style="text-align: left"><strong>From: </strong><?=$from?></p>
         <?php } ?>
         <?php if($to){ ?>
            <p style="text-align: left"><strong>To: </strong><?=$to?></p>
         <?php } ?>
         <p style="text-align: left"><strong>Payment Type: </strong><?=strtoupper($payment_type)?></p>
         <table autosize="1">
            <thead>
               <tr>
                  <th>Name</th>
                  <th>Contact</th>
                  <th>Course</th>
                  <th>Subscribed at</th>
                  <th>Amount</th>
                  <th>Payment ID</th>
                  <th>Type</th>
               </tr>
            </thead>
            <?php foreach ($transactions as $trans) { ?>
               <tr>
                  <td><?=$trans->stu_name_english?></td>
                  <td><?=$trans->stu_mobile?></td>
                  <td><?=$trans->course_title?></td>
                  <td><?=date('d/m/Y h:i A',strtotime($trans->date))?></td>
                  <td><?=$trans->amount?></td>
                  <td><?=$trans->payment_id?></td>
                  <td><?=$trans->payment_type?></td>
               </tr>
               <?php } ?>
         </table>
      </div>
   </body>
</html>
