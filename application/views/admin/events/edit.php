<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('admin/includes/includes.php'); ?>
      <?php $this->load->view('admin/includes/table-css.php'); ?>
	  <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/datepicker/css/bootstrap-datetimepicker.css">
	  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/image-crop/croppie.css">
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i>Edit Event</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Edit Event</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
                              <form action="<?=site_url('admin/events/editEvent')?>" method="post" id="add-form" enctype="multipart/form-data">
								  <input type="hidden" name="event_id" value="<?=$event->event_id?>">
								  <div class="row">
				                    <div class="col-md-12">
				                      <div class="form-group">
				                        <label>Event title * </label>
				                        <input type="text" maxlength="200" placeholder="Enter title here" name="title" class="form-control" value="<?=$event->title?>" required>
				                      </div>
				                    </div>
									<div class="col-md-12">
				                      <div class="form-group">
				                        <label>Event title in arabic * </label>
				                        <input type="text" maxlength="200" placeholder="Enter title here" name="title_arabic" id="title_arabic" class="form-control" value="<?=$event->title_arabic?>" required>
				                      </div>
				                    </div>
									<div class="col-md-6">
				                      <div class="form-group">
				                        <label>Start time *</label>
				                        <input type="text" class="form-control" id="datepicker" name="start_time" autocomplete="off" value="<?=date('d/m/Y h:i A',strtotime($event->start_time))?>" required>
				                      </div>
				                    </div>
									<div class="col-md-6">
				                      <div class="form-group">
				                        <label>End time *</label>
				                        <input type="text" class="form-control" id="datepicker-edit" name="end_time" autocomplete="off" value="<?=date('d/m/Y h:i A',strtotime($event->end_time))?>" required>
				                      </div>
				                    </div>
				                    <div class="col-md-12">
				                      <div class="form-group">
				                        <label>Description *</label>
				                        <textarea name="description" class="form-control" rows="5" cols="80" required><?=$event->description?></textarea>
				                      </div>
				                    </div>
									<div class="col-md-12">
				                      <div class="form-group">
				                        <label>Description in arabic *</label>
				                        <textarea name="description_arabic" id="description_arabic" class="form-control" rows="5" cols="80" required><?=$event->description_arabic?></textarea>
				                      </div>
				                    </div>
				                    <div class="col-md-12">
				                        <div class="row">
    	                                 <div class="form-group col-sm-4">
    	                                    <label for="exampleInputCity">Choose image ( Choose image if you wish to change the image )( 600px * 400px )</label>
    	                                    <input type="file" id="upload" class="form-control">
    	                                 </div>
    									                <div class="col-md-12">     
                                       
                                         <div id="current-image">
                                              <img src="<?=base_url() . $event->image?>" height="400px" width="600px">
                                         </div>

                                        <div class="upload-div" style="display:none;">
                                          <div id="upload-demo"></div>
                                          <div class="col-12 text-center">
                                            <a href="#" class="btn btn-primary btn-flat" style="border-radius : 5px;" id="crop-button">Crop</a>
                                          </div>
                                        </div>

                                        <div class="upload-result text-center" id="upload-result" style="display : none; margin-bottom:10px;">

                                        </div>
                                        <input type="hidden" name="image" id="ameimg" >
                                      </div>
    	                              </div>
				                    </div>
				                    <div class="col-md-12 mt-4">
				                      <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right" id="submit-button">Update</button>
				                    </div>
				                  </div>
                              </form>
                           </div>
                        </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
      <?php $this->load->view('admin/includes/table-script.php'); ?>
	  <script src="<?=base_url()?>assets/js/vendor/datepicker/bootstrap-datetimepicker.min.js"></script>
	  <script src="<?=base_url()?>assets/plugins/image-crop/croppie.js"></script>
    <script type="text/javascript">
      $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: 600,
            height: 400,
            type: 'rectangle'
        },
        boundary: {
            width: 700,
            height: 500
        }
      });


     $('#upload').on('change', function () {
      $("#submit-button").css("display", "none");
      var file = $("#upload")[0].files[0];
      var val = file.type;
      var type = val.substr(val.indexOf("/") + 1);
      if (type == 'png' || type == 'jpg' || type == 'jpeg') {

        $("#current-image").css("display", "none");
        $("#submit-button").css("display", "none");

        $(".upload-div").css("display", "block");
        $("#submit-button").css("display", "none");
        var reader = new FileReader();
          reader.onload = function (e) {
            $uploadCrop.croppie('bind', {
              url: e.target.result
            }).then(function(){
              console.log('jQuery bind complete');
            });

          }
          reader.readAsDataURL(this.files[0]);
      }
      else {
        alert('This file format is not supported.');
        document.getElementById("upload").value = "";
        $("#upload-result").css("display", "none");
        $("#submit-button").css("display", "none");
        $("#current-image").css("display", "block");
        $('#ameimg').val('');
      }
    });


    $('#crop-button').on('click', function (ev) {
        $("#submit-button").css("display", "block");
      $uploadCrop.croppie('result', {
        type: 'canvas',
        size: 'viewport'
      }).then(function (resp) {
        html = '<img src="' + resp + '" />';
        $("#upload-result").html(html);
        $("#upload-result").css("display", "block");
        $(".upload-div").css("display", "none");
        $("#submit-button").css("display", "block");
        $('#ameimg').val(resp);
      });
    });
    </script>
	  <script type="text/javascript">
		  $('#add-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button').attr('disabled',true);

			  var start_time = $('#datepicker').val();
			  var end_time = $('#datepicker-edit').val();

              start = changeFormat(start_time);
              end = changeFormat(end_time);
              if (new Date(start) < new Date(end)) {
				  var arabic_title = $('#title_arabic').val();
				  var arabic_description = $('#description_arabic').val();
				  if (arabicCheck(arabic_title) && arabicCheck(arabic_description)) {
					  document.getElementById("add-form").submit();
				  }
				  else {
					  if (!arabicCheck(arabic_title) && !arabicCheck(arabic_description)) {
						  toastr.error('Please check arabic title and description',"");
						  $('#submit-button').attr('disabled',false);
					  }
					  else {
						  if (!arabicCheck(arabic_title)) {
							  toastr.error('Please check arabic title',"");
							  $('#submit-button').attr('disabled',false);
						  }
						  else {
							  toastr.error('Please check arabic description',"");
							  $('#submit-button').attr('disabled',false);
						  }
					  }
				  }
              }
              else {
                  toastr.error('Start time must be less than end time',"");
                  $('#submit-button').attr('disabled',false);
              }

		  });
		  function arabicCheck(arabic)
		  {
			  var string = arabic.replace(/\s/g,'');
			  var isArabic =  /[\u0600-\u06FF\u0750-\u077F]/;
			  if (isArabic.test(string)){
				   return true;
			  }
			  else {
				  return false;
			  }
		  }
		  function changeFormat(date)
          {
			  arr = date.split(" ");
              if (arr[2] == 'PM') {
                  tm = arr[1].split(":");
                  if (tm[0] == 12) {
                      hour = tm[0];
                  }
                  else {
                      hour = +12 + +tm[0];
                  }
                  time = hour + ':' + tm[1] + ':00';
              }
              else {
                  time = arr[1] + ':00';
              }
              return arr[0].split("/").reverse().join("-") + ' ' + time;
          }
		  $('#upload').on('change', function () {
		    var file = $("#upload")[0].files[0];
		    var val = file.type;
		    var type = val.substr(val.indexOf("/") + 1);
		    if (type == 'png' || type == 'jpg' || type == 'jpeg') {
		      return true;
		    }
		    else {
		      document.getElementById("upload").value = "";
			  toastr.error("Failed to upload image, the format is not supported","");
		    }
		  });
		  $(function(){
			  $('#datepicker').datetimepicker({
				  format:'DD/MM/YYYY hh:mm A',
		        icons: {
		          time: "fa fa-clock-o",
		          date: "fa fa-calendar",
		          up: "fa fa-arrow-up",
		          down: "fa fa-arrow-down"
		        }
		      });
			  $('#datepicker-edit').datetimepicker({
				  format:'DD/MM/YYYY hh:mm A',
		        icons: {
		          time: "fa fa-clock-o",
		          date: "fa fa-calendar",
		          up: "fa fa-arrow-up",
		          down: "fa fa-arrow-down"
		        }
		      });
		      $("#datepicker").on("dp.show",function (e) {
		        var newtop = $('.bootstrap-datetimepicker-widget').position().top - 45;
		        $('.bootstrap-datetimepicker-widget').css('top', newtop + 'px');
		      });
		  });
	  </script>

   </body>
</html>
