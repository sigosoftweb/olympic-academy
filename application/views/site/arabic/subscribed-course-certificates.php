﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/arabic/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/arabic/includes/header'); 
	?>
	
	<main>
		

		<div class="bg_color_1">

			<nav class="subscribed-menu sticky_horizontal">
				<div class="container">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
					  	<li class="nav-item">
					    	<a class="nav-link" id="description-tab" href="<?=base_url('ar/subscriptions/details/') . $course->course_id ?>">التفاصيل</a>
					  	</li>

					  	<li class="nav-item">
					    	<a class="nav-link active" id="classes-tab" href="<?=base_url('ar/subscriptions/classes/') . $course->course_id ?>">الفصول</a>
					  	</li>
					  	
					  	<li class="nav-item">
					    	<a class="nav-link" id="exams-tab" href="<?=base_url('ar/subscriptions/exams/') . $course->course_id ?>">الامتحانات</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="attendance-tab" href="<?=base_url('ar/subscriptions/attendance/') . $course->course_id ?>">الحضور</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="trainers-tab" href="<?=base_url('ar/subscriptions/trainers/') . $course->course_id ?>">المدربون</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('ar/subscriptions/assignments/') . $course->course_id ?>">الواجبات</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('ar/subscriptions/certificates/') . $course->course_id ?>">شهادة</a>
					  	</li>
					</ul>
				</div>
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="tab-content" id="myTabContent">	  	
                         
                         	<div class="tab-pane fade show active" id="exams" role="tabpanel" aria-labelledby="exams-tab">
						  		<div class="container margin_60_35">
						  			<div class="intro_title">
										<h2>الشهادات</h2>
										<ul>
												<?php if (count($certificates) > 1) { ?>
													<li><?=count($certificates)?> الشهادات</li>
												<?php }else { ?>
													<li><?=count($certificates)?> شهادة</li>
												<?php } ?>
											
										</ul>
									</div>
						  			<div id="accordion_exams_upcoming" role="tablist" class="add_bottom_45">
										<?php 
										$i=1;
										foreach ($certificates as $certificate) { ?>
											<div class="card">
												<div class="card-header" role="tab" id="up-exam-title-<?=$i?>">
													<h5 class="mb-0">
															<?=$certificate->title_arabic?>
															<span class="float-left">	<a href="<?= base_url('ar/subscriptions/certificate_download/' . $certificate->sc_id) ?>" target="_blank" class="confirm-button btn btn-primary btn-border-theme float-lg-right">شهادة التحميل</a></span>
													</h5>
												</div>
											</div>
											<?php 
											$i++;
										} ?>
									</div>

						  		</div>
						  	</div> <!-- end of #exams -->	
                         
                        </div>
				    </div>
				</div>
			</div>
			
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php 
	$this->load->view('site/arabic/includes/footer'); 
	?>
	</div>
	<!-- page -->
	
	<?php 
	$this->load->view('site/arabic/includes/scripts'); 
	?>
	
  
</body>
</html>