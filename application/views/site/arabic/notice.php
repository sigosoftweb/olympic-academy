﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/arabic/includes/styles');
    ?>   
    <link rel="stylesheet" href="<?=base_url()?>plugins/image-crop/croppie.css">   

</head>

<body>
	
	
	<div id="page" class="theia-exception">
		
	<?php $this->load->view('site/arabic/includes/header'); ?>
	
	<main>

		
		<div class="container-fluid margin_60_35 student-profile">
			<div class="row">
				<aside class="col-lg-3" id="sidebar">

					<div class="profile">
						<figure>
							<div>
								<?php if($student->stu_image) { ?>
									<img src="<?=base_url($student->stu_image)?>" alt="<?=$student->stu_name_english?>" class="rounded-circle img-fluid">
								<?php } else { ?>
									<img src="<?=base_url('site-assets/img/user.jpg')?>" alt="<?=$student->stu_name_english?>" class="rounded-circle img-fluid">
								<?php } ?>
								
							</div>
						</figure>
						<p><?=$student->first_arabic?></p>
						<ul class="nav flex-column flex-nowrap overflow-hidden">
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('ar/student/profile')?>">ملفي الشخصي</a>    </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('ar/subscriptions')?>">اشتراكاتي</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('ar/subscriptions/pending')?>">الاشتراكات المعلقة</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('ar/subscriptions/rejected')?>">الاشتراكات المرفوضة</a>
			                </li>
			                <?php if(!empty($subscribed_courses)) { ?>
			                <li class="nav-item">
			                    <a class="nav-link collapsed" href="#submenu1" data-toggle="collapse" data-target="#submenu1">تقويم الدورة<span class="float-right"><i class="icon-down-dir"></i></span></a>
			                    <div class="collapse" id="submenu1" aria-expanded="false">
			                        <ul class="flex-column nav">
			                        	<?php foreach ($subscribed_courses as $subscribed_course) { ?>                   		
			                            <li class="nav-item"><a class="nav-link py-0" href="<?=base_url('ar/subscriptions/calendar/'). $subscribed_course->cid ?>"><?=$subscribed_course->course_title_arabic ?></a></li>
			                            <?php } ?>
			                        </ul>
			                    </div>
			                </li>
			            	<?php } ?>
			                <li class="nav-item"><a class="nav-link" href="<?=base_url('ar/student/payments')?>">مدفوعاتي</a></li>
			                <li class="nav-item active">
			                	<a class="nav-link" href="<?=base_url('ar/student/notice')?>">إشعار 
			                		
			                	</a></li>
			                <li class="nav-item"><a class="nav-link" href="<?=base_url('ar/student/password/change')?>">تغيير كلمة المرور</a></li>
			            </ul>
					</div>
				</aside>
				<!--/aside -->

				<div class="col-lg-9">
					<h3 class="sidebar-page-title">Notice</h3>

					<?php if(!empty($notice)) { ?>

						<div class="row">
							<?php foreach($notice as $not) { ?>
								<div class="col-lg-12 ">
									<div class="box-notice">							
											
										<div>
											<h4><?=$not->title_arabic?></h4>
											<p><?=$not->description_arabic?></p>
										</div>
									</div>						
								</div>
							<?php } ?>
						</div>
						
					<?php } else { ?>
						<div class="text-center margin_60_35">
							<figure class=""><img src="<?=base_url()?>site-assets/img/wizard_intro_icon.svg" alt=""></figure>
							<h3>لا جديد!</h3>
						</div>
					<?php } ?>
			
				</div>
				<!-- /col -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->

	</main>
	<!--/main-->
	
	<?php $this->load->view('site/arabic/includes/footer'); ?>
	<!--/footer-->
	</div>
	<!-- page -->

	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/arabic/includes/scripts'); ?>
  
</body>
</html>