﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/arabic/includes/styles.php');
    ?>     

</head>

<body id="login_bg">
	
	<nav id="menu" class="fake_menu"></nav>
	
	<div id="preloader">
		<div data-loader="circle-side"></div>
	</div>
	<!-- End Preload -->
	
	<div id="login">
		<aside>
			<figure class="logo">
					<a href="<?=base_url()?>">
						<img src="<?=base_url('site-assets/img/olympic.PNG')?>" width="75"  data-retina="true" alt="">	
					</a>
			</figure>
			  <form id="login-form"  autocomplete="off" method="post" action="<?=base_url('ar/login/checkStudent')?>">
				
				<div class="form-group">
					<span class="input">
					<input class="input_field" type="text" autocomplete="off" name="username"  required>
						<label class="input_label">
						<span class="input__label-content">البريد الإلكتروني / الهاتف المحمول</span>
					</label>
					</span>

					<span class="input">
					<input class="input_field" type="password"  autocomplete="off" name="password" required>
						<label class="input_label">
						<span class="input__label-content">كلمة السر</span>
					</label>
					</span>
					<small><a href="<?= base_url('ar/login/forgotpassword') ?>">​​نسيت كلمة المرور؟</a></small>
				</div>
				<button type="submit" class="btn_1 rounded full-width add_top_60">تسجيل الدخول</button>
				<div class="text-center add_top_10">جديد في الأكاديمية الأولمبية? <strong><a href="<?=base_url('ar/register')?>">سجل!</a></strong></div>
			</form>
			<div class="copy">الأكاديمية الأولمبية 2020 ©</div>
		</aside>
	</div>
	<!-- /login -->
		
	<?php $this->load->view('site/arabic/includes/scripts.php'); ?>
  
</body>
</html>