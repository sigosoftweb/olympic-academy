﻿<?php
	
	if (isset($this->session->userdata['olympicUser'])) {
	    $user = $this->session->userdata['olympicUser'];
	    if(isset($user)) {
	    	$userid = $user['userID'];
	    	$username = $user['userName'];
	    	//echo $username;
	    	//exit;
	    }
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/arabic/includes/styles.php');
    ?>   
    <link rel="stylesheet" href="<?=base_url()?>plugins/image-crop/croppie.css">   

</head>

<body>
	
	
	<div id="page" class="theia-exception">
		
	<?php $this->load->view('site/arabic/includes/header.php'); ?>
	
	<main>

		
		<div class="container-fluid margin_60_35 student-profile">
			<div class="row">
				<aside class="col-lg-3" id="sidebar">

					<div class="profile">
						<figure>
							<div>
								<?php if($student->stu_image) { ?>
									<img src="<?=base_url($student->stu_image)?>" alt="<?=$student->stu_name_english?>" class="rounded-circle img-fluid">
								<?php } else { ?>
									<img src="<?=base_url('site-assets/img/user.jpg')?>" alt="<?=$student->stu_name_english?>" class="rounded-circle img-fluid">
								<?php } ?>
								
							</div>
						</figure>
						<p><?=$student->first_arabic?></p>
						<?php /*<ul>							
							
							<li><a href="<?=base_url('ar/student/profile')?>">My Profile</a></li>
							<li><a href="<?=base_url('ar/subscriptions')?>">My Subscriptions</a></li>
							<li><a href="<?=base_url('ar/student/payments')?>">My Payments</a></li>
							<li>
								<a href="<?=base_url('ar/student/notice')?>" >Notice</a>
							</li>
							<li>
								<a href="<?=base_url('ar/student/password/change')?>" >Change Password</a>
							</li>
						</ul> */?>
						<ul class="nav flex-column flex-nowrap overflow-hidden">
			                <li class="nav-item active">
			                    <a class="nav-link" href="<?=base_url('ar/student/profile')?>">ملفي الشخصي</a>    </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('ar/subscriptions')?>">اشتراكاتي</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('ar/subscriptions/pending')?>">الاشتراكات المعلقة</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('ar/subscriptions/rejected')?>">الاشتراكات المرفوضة</a>
			                </li>
			                <?php if(!empty($subscribed_courses)) { ?>
			                <li class="nav-item">
			                    <a class="nav-link collapsed" href="#submenu1" data-toggle="collapse" data-target="#submenu1">تقويم الدورة<span class="float-right"><i class="icon-down-dir"></i></span></a>
			                    <div class="collapse" id="submenu1" aria-expanded="false">
			                        <ul class="flex-column nav">
			                        	<?php foreach ($subscribed_courses as $subscribed_course) { ?>                   		
			                            <li class="nav-item"><a class="nav-link py-0" href="<?=base_url('ar/subscriptions/calendar/'). $subscribed_course->cid ?>"><?=$subscribed_course->course_title_arabic ?></a></li>
			                            <?php } ?>
			                        </ul>
			                    </div>
			                </li>
			            	<?php } ?>
			                <li class="nav-item"><a class="nav-link" href="<?=base_url('ar/student/payments')?>">مدفوعاتي</a></li>
			                <li class="nav-item">
			                	<a class="nav-link" href="<?=base_url('ar/student/notice')?>">إشعار 
			                		<?php if($notice_count->ncount) { ?>
				                		<span class="notice-count"><?=$notice_count->ncount?></span>
				                	<?php } ?>
			                	</a></li>
			                <li class="nav-item"><a class="nav-link" href="<?=base_url('ar/student/password/change')?>">تغيير كلمة المرور</a></li>
			            </ul>
							
						
					</div>
				</aside>
				<!--/aside -->

				<div class="col-lg-9">
					<h3 class="sidebar-page-title">ملفي الشخصي</h3>
					<div class="box_teacher edit-form-wrapper">
						<p class="edit-profile">تحرير</p>
						<form id="edit-profile-form" method="post" action="<?=base_url('ar/student/edit_profile')?>"  enctype="multipart/form-data">
							<input type="hidden" name="stu_id" value="<?=$userid?>">
							<div class="row">
								<div class="col-lg-6">
									<div class="indent_title_in">									
										<h6>الاسم الأول (عربي) </h6>
										<div class="field-wrapper">
											<p><?=$student->first_arabic?></p>
											<input type="text" name="first_arabic" value="<?=$student->first_arabic?>" id="first-arabic" class="form-control edit-elements" required onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'>
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="indent_title_in">									
										<h6>اسم العائلة (عربي)</h6>
										<div class="field-wrapper">
											<p><?=$student->second_arabic?></p>
											<input type="text" name="second_arabic" value="<?=$student->second_arabic?>" id="second-arabic" class="form-control edit-elements" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'>
										</div>
									</div>
								</div>

								<hr class="styled_2">

								<div class="col-lg-6">
									<div class="indent_title_in">									
										<h6>الاسم الأول  (الإنجليزية)</h6>
										<div class="field-wrapper">
											<p><?=$student->stu_name_english?></p>
											<input type="text" name="stu_name_english" value="<?=$student->stu_name_english?>" class="form-control edit-elements" required onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'>
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="indent_title_in">									
										<h6>اسم العائلة (الإنجليزية)</h6>
										<div class="field-wrapper">
											<p><?=$student->stu_name_arabic?></p>
											<input type="text" name="stu_name_arabic" value="<?=$student->stu_name_arabic?>" class="form-control edit-elements" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'>
										</div>
									</div>
								</div>

								<hr class="styled_2">

								<div class="col-lg-6">
									<div class="indent_title_in">
										<h6>البريد الإلكتروني</h6>
										<div class="field-wrapper">
											<p><?=$student->stu_email?></p>
											<input type="email" name="stu_email" value="<?=$student->stu_email?>" class="form-control edit-elements" required>
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="indent_title_in">
										<h6>الهاتف المحمول</h6>
										<div class="field-wrapper">
											<p><?=$student->stu_mobile?></p>
											<input type="text" name="stu_mobile" id="mobile" value="<?=$student->stu_mobile?>" class="form-control edit-elements" required onkeypress="return event.charCode >= 48 && event.charCode <= 57">
										</div>
									</div>
								</div>

								<hr class="styled_2">

								<div class="col-lg-6">
									<div class="indent_title_in">
										<h6>التعليم</h6>
										<div class="field-wrapper">
											<p><?=$student->stu_education?></p>
											<input type="text" name="stu_education" value="<?=$student->stu_education?>" class="form-control edit-elements">
										</div>
									</div>
								</div>		

								<div class="col-lg-6">
									<div class="indent_title_in">
										<h6>CPR</h6>
										<div class="field-wrapper">
											<p><?=$student->stu_cpr?></p>
											<input type="text" name="stu_cpr" value="<?=$student->stu_cpr?>" class="form-control edit-elements">
										</div>
									</div>
								</div>					
								
								<hr class="styled_2">

								<div class="col-lg-6">
									<div class="indent_title_in">
										<h6>تاريخ الميلاد</h6>
										<div class="field-wrapper">
											<p><?=$student->stu_dob?></p>
											<input type="date" name="stu_dob" value="<?=$student->stu_dob?>" class="form-control edit-elements">
										</div>
									</div>
								</div>

								<div class="col-lg-6">
									<div class="indent_title_in">
										<h6>نوع الجنس </h6>
										<div class="field-wrapper">
											<p>
												<?php 
												switch($student->stu_gender) {
													case 'Male':
														echo 'ذكر';
														break;
													case 'Female':
														echo 'أنثى';
														break;
													case 'Other':
														echo 'Other';
														break;
													default:
														echo '';
														break;
												}
												
												?>	
											</p>
											<select name="stu_gender" class="form-control edit-elements">
												<option value="Male" <?php if($student->stu_gender == 'Male'){ echo 'selected'; } ?> >ذكر</option>
												<option value="Female" <?php if($student->stu_gender == 'Female'){ echo 'selected'; } ?>>أنثى</option>
												<option value="Other" <?php if($student->stu_gender == 'Other'){ echo 'selected'; } ?>>Other</option>
											</select>										
										</div>
									</div>
								</div>
								

								<hr class="styled_2">

								<div class="col-lg-6">
									<div class="indent_title_in">
										<h6>Timezone</h6>
										<div class="field-wrapper">
											<p><?=$student->stu_timezone?></p>
											<select name="stu_timezone" class="form-control edit-elements">
												<option value="">None</option>
												<?php foreach ($timezones as $timezone) { ?>
													<option value="<?= $timezone?>" <?php if($student->stu_timezone == $timezone){ echo 'selected'; } ?> ><?= $timezone?></option>
												<?php } ?>
												
											</select>										
										</div>
									</div>
								</div>

								<div class="col-lg-6">
									<div class="indent_title_in">
										<h6>العنوان</h6>
										<div class="field-wrapper">
											<p><?=$student->stu_contact?></p>
											<textarea class="form-control edit-elements" name="stu_contact"><?=$student->stu_contact?></textarea>
										</div>
									</div>
									
								</div>

								<div class="col-lg-6">
									<div class="indent_title_in">
										<?php if($student->stu_cv) { ?>
											<a class="link-cv" href="<?=base_url($student->stu_cv)?>" target="_blank">View CV</a>
										<?php } ?>
										
										<h6 class="edit-elements">السيرة الذاتية</h6>
										<div class="field-wrapper">
											
											<?php /* <input type="file" name="stu_cv" class="edit-elements" id="upload_cv"> */ ?>
											<div class="custom-file edit-elements">
											  	<input type="file" name="stu_cv" class="custom-file-input" id="upload_cv">
											  	<label class="custom-file-label" for="upload_cv">لم يتم اختيار ملف</label>
											</div>
										</div>
									</div>
								</div>

								<div class="col-lg-6">
									<div class="indent_title_in">
										<h6 class="edit-elements">صورة الملف الشخصي</h6>
										<div class="field-wrapper">
											
											<?php /*<input type="file" name="stu_image" class="edit-elements" id="upload"> */ ?>
											<div class="custom-file edit-elements">
											  	<input type="file" name="stu_image" class="custom-file-input" id="upload">
											  	<label class="custom-file-label" for="upload_cv">لم يتم اختيار ملف</label>
											</div>
										</div>
									</div>
								</div>

								
								<div class="col-lg-12">
									<h6>&nbsp;</h6>
									<button type="submit" class="btn_1 rounded edit-elements float-left">تقديم</button>
								</div>

							</div>
						</form>
					</div>
				</div>
				<!-- /col -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->

	</main>
	<!--/main-->
	
	<?php $this->load->view('site/arabic/includes/footer.php'); ?>
	<!--/footer-->
	
	<div class="modal fade" id="modal-password" tabindex="-1" role="dialog" aria-labelledby="pswd-modal-title" aria-hidden="true">
  		<div class="modal-dialog" role="document">
    		<div class="modal-content">
      			<div class="modal-header">
        			<h5 class="modal-title" id="pswd-modal-title">Modal title</h5>
        			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          				<span aria-hidden="true">&times;</span>
        			</button>
      			</div>
      			<div class="modal-body">
                </div><!-- end of .modal-body -->
            </div>
        </div>
    </div><!-- end of #modal-password -->	
	

</div>
<!-- page -->


	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/arabic/includes/scripts.php'); ?>
    <script src="<?=base_url()?>site-assets/plugins/image-crop/croppie.js"></script>

    <script type="text/javascript">
    	$('.edit-profile').on('click', function(e){
    		var button_text = $(this).html();
    		if( button_text == 'تحرير') {
	    		$('.edit-elements').css("display","block");
	    		$('.field-wrapper p').css('display','none');
	    		$('.link-cv').css('display','none');
	    		$(this).html('Cancel');
	    	}
	    	else {
	    		$('.edit-elements').css("display","none");
	    		$('.field-wrapper p').css('display','block');
	    		$('.link-cv').css('display','block');
	    		$(this).html('تحرير');
	    	}
    	});

    	$('#edit-profile-form').on('submit', function(e){			
			
			var mobile = $('#mobile').val();

			var first_arabic = $("#first-arabic").val();
			var second_arabic = $("#second-arabic").val();

			var isArabic = /^([\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufbc1]|[\ufbd3-\ufd3f]|[\ufd50-\ufd8f]|[\ufd92-\ufdc7]|[\ufe70-\ufefc]|[\ufdf0-\ufdfd]|[ ])*$/g;
			var isArabic2 = /^([\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufbc1]|[\ufbd3-\ufd3f]|[\ufd50-\ufd8f]|[\ufd92-\ufdc7]|[\ufe70-\ufefc]|[\ufdf0-\ufdfd]|[ ])*$/g;

			if (!isArabic.test(first_arabic)) {
				e.preventDefault();
				toastr.error('First name given has non-arabic characters');
			}

			else if (second_arabic != '' && !isArabic2.test(second_arabic)) {
				e.preventDefault();
				toastr.error('Second name given has non-arabic characters');
			}
		  	
			/*else if (mobile.length < 8 || mobile.length > 12 ) {
				e.preventDefault();
				toastr.error('Mobile number should contains 8 numbers');				  
			}*/

			
		});


		$('#upload').on('change', function () {
		    var file = $("#upload")[0].files[0];
		    var val = file.type;
		    var type = val.substr(val.indexOf("/") + 1);
		    var filename = file.name;
		    //alert(type);
		    if (type == 'png' || type == 'jpg' || type == 'jpeg') {
		      $(this).closest('.custom-file').find('label').html(filename);
		    }
		    else {
		      
			  toastr.error("File format not supported","");
		    }
		});

		$('#upload_cv').on('change', function () {
		    var file = $("#upload_cv")[0].files[0];
		    var val = file.type;
		    //var type = val.substr(val.indexOf("/") + 1);
		    var filename = file.name;
		    var ext = filename.substr(filename.lastIndexOf('.') + 1);
		    
		    //alert(ext);
		    if (ext == 'doc' || ext == 'docx' || ext == 'pdf') {
		      $(this).closest('.custom-file').find('label').html(filename);
		    }
		    else {
		      
			  toastr.error("File format not supported","");
		    }
		});

    </script>

    
	
</body>
</html>