﻿
	
	<!-- COMMON SCRIPTS -->
    <script src="<?=base_url()?>site-assets/js/jquery-2.2.4.min.js"></script>
    <script src="<?=base_url()?>site-assets/js/common_scripts.js"></script>
    <script src="<?=base_url()?>site-assets/js/main-arabic.js"></script>
	<script src="<?=base_url()?>site-assets/assets/validate.js"></script>
	<script src="<?=base_url()?>site-assets/js/custom-arabic.js"></script>
	<script src="<?=base_url()?>site-assets/plugins/toaster/toaster.min.js"></script>

	<script type="text/javascript">
	toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "newestOnTop": false,
	  "progressBar": true,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": false,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": "5000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}
	<?php
	    $alert = $this->session->flashdata('alert_type');
	    if ($alert) { ?>
	        toastr.<?=$this->session->flashdata('alert_type')?>("<?=$this->session->flashdata('alert_message')?>","<?php echo $this->session->flashdata('alert_title')?>");
	    <?php }
	?>
	</script>

	<script type="text/javascript">
		$('#select-language').on('change', function(e) {

	        var lang = $(this).val();
	        //alert(lang);
	        if(lang == "Arabic") {
	            window.location.replace("<?= base_url('ar/home')?>");
	        } else {
	            window.location.replace("<?= base_url('en/home')?>");
	        }

	    });
	</script>
	
