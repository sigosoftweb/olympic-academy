﻿	<?php
	$ci = & get_instance();
    $ci->load->model('site/arabic/Model_Contact');
    $contact = $ci->Model_Contact->get_contact();
    
    ?>

	<footer>
		<div class="container pt-5 pb-4">
			<?php if(!isStudent()) { ?>
			<div class="row">
				<div class="col-lg-5 col-md-12 p-r-5">
					<p class="logo">
						<img src="<?=base_url()?>site-assets/img/olympic.PNG" width="149" height="auto" data-retina="true" alt="">						
					</p>
					<p>الأكاديمية الأولمبية البحرينية هي امتداد لمعهد البحرين الرياضي الذي نشأ في عام 1981 بمبادرة من جلالة الملك حمد بن عيسى ال خليفة ملك مملكة البحرين  </p>
					<div class="follow_us">
						<ul>
							<li>تابعنا</li>
							<li><a href="#0"><i class="ti-facebook"></i></a></li>
							<li><a href="#0"><i class="ti-twitter-alt"></i></a></li>
							<li><a href="#0"><i class="ti-google"></i></a></li>
							<li><a href="#0"><i class="ti-pinterest"></i></a></li>
							<li><a href="#0"><i class="ti-instagram"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 ml-lg-auto">
					<h5>روابط مفيدة</h5>
					<ul class="links">
						<li><a href="<?=base_url('ar/about')?>">حول</a></li>
						<li><a href="<?=base_url('ar/courses')?>">الدورات</a></li>
						<li><a href="<?=base_url('ar/trainers')?>">محاضرينا</a></li>
						<li><a href="<?=base_url('ar/events')?>">الأحداث</a></li>
						<li><a href="<?=base_url('ar/calendar')?>">التقويم</a></li>
						<li><a href="<?=base_url('ar/courses/resources')?>">الموارد</a></li>
						<li><a href="<?=base_url('ar/login')?>">تسجيل الدخول</a></li>
						<li><a href="<?=base_url('ar/register')?>">التسجيل</a></li>
						<li><a href="<?=base_url('ar/teacher')?>">دخول المدربين</a></li>
						<li><a href="<?=base_url('ar/contact')?>">اتصل بنا</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-6">
					<h5>اتصل بنا</h5>
					<ul class="contacts">
						<li><i class="pe-7s-map-marker" style="margin-right: 10px;"></i> <?=$contact->address_arabic?></li>
						<li><a href="tel://+973 34477598"><i class="icon-mobile"></i> <?= $contact->mobile?></a></li>
						<li><a href="mailto:info@olympicacademy.com"><i class="icon-email"></i> <?= $contact->email ?></a></li>
					</ul>
					
				</div>
			</div>
			<!--/row-->
			<hr>
		<?php } ?>
			<div class="row">
				<div class="col-md-8">
					<ul id="additional_links">
						<li><a href="#0">الشروط والأحكام</a></li>
					</ul>
				</div>
				<div class="col-md-4">
					<div id="copy">الأكاديمية الأولمبية 2020 ©</div>
				</div>
			</div>
		</div>
	</footer>
	<!--/footer-->
