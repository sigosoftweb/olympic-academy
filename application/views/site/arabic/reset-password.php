﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >
    
    <?php 
    $this->load->view('site/arabic/includes/styles');
    ?>   
    <link rel="stylesheet" href="<?=base_url()?>plugins/image-crop/croppie.css">   

</head>

<body>
	
	
	<div id="page" class="theia-exception">
		
	<?php $this->load->view('site/arabic/includes/header'); ?>
	
	<main>

		
		<div class="container margin_60_35 student-profile">
			<div class="row">				

				<div class="col-lg-5 justify-center">
					<h3 class="sidebar-page-title">Reset Password</h3>
					<div class="change-password">
			
						<div class="box_teacher password-form-wrapper">
							<div class="">
								<form id="change-pswd-form" method="post" action="<?=base_url('ar/login/changePassword')?>">									<input type="hidden" name="email" value="<?=$email ?>">						

									<span class="input">
									<input type="password" name="password" id="new-password" class="input_field" required>
										<label class="input_label">
										<span class="input__label-content">كلمة سر جديدة</span>
									</label>
									</span>	

									<span class="input">
									<input type="password" name="confirm_password" id="confirm-password" class="input_field" required>
										<label class="input_label">
										<span class="input__label-content">تأكيد كلمة المرور</span>
									</label>
									</span>	

									<button type="submit" class="btn_1 rounded float-left">تقديم</button>																
								</form>
							</div>
						</div>
				
					</div>
		
				</div>
				<!-- /col -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->

	</main>
	<!--/main-->
	
	<?php $this->load->view('site/arabic/includes/footer'); ?>
	<!--/footer-->
	
	

</div>
<!-- page -->


	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/arabic/includes/scripts'); ?>

    <script type="text/javascript">
    	

    	$('#change-pswd-form').on('submit', function(e){
			
			
			var new_password = $('#new-password').val();
			var confirm_password = $('#confirm-password').val();
			if(new_password.length < 6) {
				e.preventDefault();
				toastr.error('Password must contain atleast 6 characters');
			}
		  	
			 if (new_password != confirm_password ) {
				e.preventDefault();
				toastr.error('New password and confirm password does not match');				  
			}

			
		});

		
    </script>

    
	
</body>
</html>