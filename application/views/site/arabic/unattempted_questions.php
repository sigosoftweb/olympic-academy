<!DOCTYPE html>
<html lang="en">

<head>

    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php
    $this->load->view('site/arabic/includes/styles');
    ?>

</head>

<body>

	<div id="page">

	<?php $this->load->view('site/arabic/includes/header'); ?>

	<main>
		

		<div class="bg_color_1">
			<div class="container margin_60_35">
				<h3 class="no-sidebar-page-title"><?=$exam->exam_name_arabic?></h3>
				<div class="row">
					<div class="col-lg-12">
						<?php $i = 1; foreach ($questions as $question) { ?>

							<div class="question">
								<h3>Question <?=$i?></h3>
								<h5><?=$question->formula_question_arabic?></h5>

									<div class="question-options">
										<ul>
											<li><input type="radio" <?php if($question->ans == 'a'){ ?>checked<?php } ?>> <?=$question->formula_a_arabic?></li>
											<li><input type="radio" <?php if($question->ans == 'b'){ ?>checked<?php } ?>> <?=$question->formula_b_arabic?></li>
											<?php if ($question->formula_c_arabic != '') { ?>
												<li><input type="radio" <?php if($question->ans == 'c'){ ?>checked<?php } ?>> <?=$question->formula_c_arabic?></li>
											<?php } ?>
											<?php if ($question->formula_d_arabic != '') { ?>
												<li><input type="radio" <?php if($question->ans == 'd'){ ?>checked<?php } ?>> <?=$question->formula_d_arabic?></li>
											<?php } ?>
											<?php if ($question->formula_e_arabic != '') { ?>
												<li><input type="radio" <?php if($question->ans == 'e'){ ?>checked<?php } ?>> <?=$question->formula_e_arabic?></li>
											<?php } ?>
										</ul>
										
									</div>
							</div>
						<?php $i++; } ?>
					</div>

				</div>
			</div>
		</div>

	</main>
	<!--/main-->

	<?php $this->load->view('site/arabic/includes/footer'); ?>
	</div>
	<!-- page -->

	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/arabic/includes/scripts'); ?>
</body>
</html>
