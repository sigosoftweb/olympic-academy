﻿
<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >
    
    <?php $this->load->view('site/arabic/includes/styles'); ?>

</head>

<body>
	
	<div id="page">
		
	<?php $this->load->view('site/arabic/includes/header'); ?>
	
	<main>

		
		<section id="hero_in" class="courses banner-courses">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>الدورات</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->
		

		<div class="filters_listing sticky_horizontal">
			<div class="container">
				<form name="form_filter" id="form-filter" method="post" action="<?=base_url('ar/courses')?>">
					<ul class="clearfix course-filters">
						<li>
							<div class="switch-field">
								<input type="radio" id="all" class="listing-filter" name="listing_filter" value="all" checked>
								<label for="all">الكل</label>
								<input type="radio" id="popular" class="listing-filter" name="listing_filter" value="popular" >
								<label for="popular">شعبية</label>
								<?php /*<input type="radio" id="latest" class="listing-filter" name="listing_filter" value="latest" <?php if($filter == 'latest') echo 'checked'?>>
								<label for="latest">Latest</label> */?>
							</div>
						</li>
						
						<li>
							<?php //echo $selected_category; ?>
							<select name="category" class="form-control selectbox-category" id="category">
								<option value="0" selected>جميع الفئات</option>
								<?php foreach ($categories as $category) { ?>
									<option value="<?=$category->cat_id?>" ><?=$category->cat_name_arabic?></option>								
								<?php } ?>
							</select>
						</li>
					</ul>
				</form>
			</div>
			<!-- /container -->
		</div>
		<!-- /filters -->
		<?php //print_r($courseIDs); ?>
		<div class="container margin_60_35">
			
			<div class="row courses-row">
				<?php foreach($courses as $course) { ?>
				<div class="col-xl-4 col-lg-6 col-md-6">
					<div class="box_grid wow courses">
						<figure class="block-reveal">
							<div class="block-horizzontal"></div>							
								
							
							
								<?php if($course->course_banner) { ?>
									<?php /*<img src="<?=base_url($course->course_banner)?>" class="img-fluid" alt="">*/?>
									<div class="banner" style="background-image: url(<?=base_url($course->course_banner)?>);"></div>
								<?php } else { ?>
									<?php /*<img src="<?=base_url('site-assets/img/no-image.jpg')?>" class="img-fluid" alt="">*/ ?>
									<div class="banner" style="background-image: url(<?=base_url('site-assets/img/no-image.jpg')?>);"></div>
								<?php }?>
							
							<div class="price"><p>فردي</p><?=$course->course_sale_price?> BHD</div>
							<?php /*<div class="price tamkeen"><p>Tamkeen</p><?=$course->course_price?> BHD</div>*/?>
						</figure>
						<div class="wrapper">
							<small><?=$course->cat_name_arabic?></small>
							<h3><?=$course->course_title_arabic?></h3>
							<p><?=getExcerpt($course->course_description_arabic)?></p>
							
						</div>
						<ul>
							<li><i class="pe-7s-clock"></i><?=$course->course_duration_arabic?>  </li>
							<?php if(isStudent()) {
								
								//print_r($courseIDs);
								//echo "id=" . $course->course_id;
								if(!empty($courseIDs) && is_in_array($courseIDs, 'course_id', $course->course_id)) { ?>
									<li><a href="<?=base_url('ar/subscriptions/details/'. $course->course_id)?>">مشترك!</a></li>
								<?php } else { 
									if($course->sequence_id == 0) { ?>
										<?php if($course->students_count < $course->student_limit) { ?>
											<li><a href="<?=base_url('ar/courses/details/'. $course->course_id)?>">سجل الآن</a></li>
										<?php } else { ?>
											<li><a href="<?=base_url('ar/courses/details/'. $course->course_id)?>">سجل الآن</a></li>
										<?php } ?>
										
									<?php } else {
										if(!empty($courseIDs) && is_in_array($courseIDs, 'course_id', $course->sequence_id)) { ?>
											<li><a href="<?=base_url('ar/courses/details/'. $course->course_id)?>">سجل الآن</a></li>
										<?php }	else {?>	
											<li><a href="<?=base_url('ar/courses/details/'. $course->course_id)?>">سجل الآن</a></li>
										<?php } 
									}
								}									
							
							} else {?>
								<?php if($course->students_count < $course->student_limit) { ?>
									<li><a href="<?=base_url('ar/courses/details/'. $course->course_id)?>">سجل الآن</a></li>
								<?php } else { ?>
									<li><a href="<?=base_url('ar/courses/details/'. $course->course_id)?>">سجل الآن</a></li>
								<?php } ?>
							<?php } ?>

							
						</ul>
					</div>
				</div>
				<!-- /box_grid -->
				<?php } ?>
				
			</div>
			<!-- /row -->
			<?php /*<p class="text-center"><a href="#0" class="btn_1 rounded add_top_30">Load more</a></p> */?>
		</div>
		<!-- /container -->
		
	</main>
	<!--/main-->
	
	<?php $this->load->view('site/arabic/includes/footer'); ?>
	</div>
	<!-- page -->

	<?php $this->load->view('site/arabic/includes/scripts'); ?>
  
</body>
</html>

<script type="text/javascript">
	/*$('.listing-filter').change(function(e) {
		$('#form-filter').submit();
	});

	$('.selectbox-category').change(function(e) {
		$('#form-filter').submit();
	});

	*/
	$('.listing-filter').click(function(e) {
		var filter = $('input[name=listing_filter]:checked').val();
		var category = $('#category').val();
		//alert(filter);
		$.ajax({
			url: '<?=base_url('ar/courses/filters/')?>',
			data: {'listing_filter': filter, 'category': category},
			type: 'POST',

			success: function(response) {
				//console.log(response);
				$('.courses-row').html(response);
					  		
			},
			error: function(){
				toastr.error('Error !');
			}
				  	
		});
	});

	$('#category').change(function(e) {
		var filter = $('input[name=listing_filter]:checked').val();
		var category = $(this).val();
		//alert(filter);
		//alert(category);
		$.ajax({
			url: '<?=base_url('ar/courses/filters/')?>',
			data: {'listing_filter': filter, 'category': category},
			type: 'POST',

			success: function(response) {
				//console.log(response);
				$('.courses-row').html(response);
					  		
			},
			error: function(){
				toastr.error('Error !');
			}
				  	
		});
	});

</script>