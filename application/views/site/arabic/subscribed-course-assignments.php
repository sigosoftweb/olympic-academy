﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/arabic/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/arabic/includes/header'); 
	?>
	
	<main>
		

		<div class="bg_color_1">

			<nav class="subscribed-menu  sticky_horizontal">
				<div class="container">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
					  	<li class="nav-item">
					    	<a class="nav-link" id="description-tab" href="<?=base_url('ar/subscriptions/details/') . $course->course_id ?>">التفاصيل</a>
					  	</li>

					  	<li class="nav-item">
					    	<a class="nav-link" id="classes-tab" href="<?=base_url('ar/subscriptions/classes/') . $course->course_id ?>">الفصول</a>
					  	</li>
					  	
					  	<li class="nav-item">
					    	<a class="nav-link" id="exams-tab" href="<?=base_url('ar/subscriptions/exams/') . $course->course_id ?>">الامتحانات</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="attendance-tab" href="<?=base_url('ar/subscriptions/attendance/') . $course->course_id ?>">الحضور</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="trainers-tab" href="<?=base_url('ar/subscriptions/trainers/') . $course->course_id ?>">المدربون</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link active" id="assignments-tab" href="<?=base_url('ar/subscriptions/assignments/') . $course->course_id ?>">الواجبات</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('ar/subscriptions/certificates/') . $course->course_id ?>">شهادة</a>
					  	</li>
					</ul>
				</div>
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="tab-content" id="myTabContent">				  	


						  	<div class="tab-pane fade show active" id="assignments" role="tabpanel" aria-labelledby="assignments-tab">
						  		<div class="container margin_60_35">

						  			<?php 
						  			//print_r($assignments); ?>
						  			
							  		<div class="intro_title">
										<h2>الواجبات</h2>
										<ul>
											<li><?=$assignments_count->a_count?> الواجبات</li>
											
										</ul>
									</div>
									<div id="accordion_assignments" role="tablist" class="add_bottom_45">
										<?php 
										$i = 1;
										$first_child = true;
										foreach ($assignments as $assignment) { ?>
											<div class="card">
												<!-- .card-header starts -->
												<div class="card-header" role="tab" id="heading-<?=$i?>">
													<h5 class="mb-0">
														<?php if($first_child) { ?>
															<a data-toggle="collapse" href="#collapse-<?=$i?>" aria-expanded="true" aria-controls="collapse-<?=$i?>"><i class="indicator icon-minus"></i>
														<?php } else { ?>
															<a class="collapsed" data-toggle="collapse" href="#collapse-<?=$i?>" aria-expanded="false" aria-controls="collapse-<?=$i?>">
															<i class="indicator icon-plus"></i>

														<?php } ?>
														Assignment 
														<span class="float-left"><?=date('d-m-Y', strtotime($assignment->assignment_date)) ?></span>
															</a>
													</h5>
												</div>
												<!-- .card-header ends -->

												<?php if($first_child) { ?>
												<div id="collapse-<?=$i?>" class="collapse show" role="tabpanel" aria-labelledby="heading-<?=$i?>" data-parent="#accordion_assignments">
												<?php } else { ?>
												<div id="collapse-<?=$i?>" class="collapse" role="tabpanel" aria-labelledby="heading-<?=$i?>" data-parent="#accordion_assignments">
												<?php } ?>	
													<div class="card-body">
														<div>
															<p><?=$assignment->title_arabic?></p>
															<?php if($assignment->attachment) { ?>
																<p><a href="<?=base_url($assignment->attachment)?>" target="_blank">
																	<i class='icon-attach'></i>
																المرفقات</a></p>
															<?php } ?>
															
														</div>
														<div class="assignment-answer">											
															<?php if(!$assignment->csa_id) { ?>
																<h4>الجواب</h4>
																<form method="post" id="answer-form" enctype="multipart/form-data" action="<?=base_url('ar/subscriptions/post_assignment') ?>">
																	<div class="row">
																		<div class="col-12 pb-2">
																			<textarea name="answer" class="form-control" id="answer"></textarea>
																		</div>
																		<div class="col-md-6">
																			<input type="file" name="answer_attachment" class="form-control" id="answer-attachment">
																		</div>
																		<div class="col-md-6">
																			<button type="submit" class="btn-exam float-left">تقديم</button>
																		</div>
																	</div>
																	<input type="hidden" name="ca_id" value="<?=$assignment->assignment_id ?>">
																	<input type="hidden" name="course_id" value="<?=$course->course_id?>">
																</form>
															<?php } else { ?>
																<h4>الجواب <span class="float-left"><?=date('d-m-Y', strtotime($assignment->student_assignment_date)) ?></span></h4>
																<?php if($assignment->answer) { ?>
																	<p><?=$assignment->answer?></p>
																<?php }
																if($assignment->answer_attachment) { ?>
																<p><a href="<?=base_url($assignment->answer_attachment)?>" target="_blank"><i class='icon-attach'></i> المرفقات</a></p>
																<?php }
																
															} ?>
														</div>
														
														
														
													</div>
												</div>
											</div>
											<!-- /card -->
											<?php 
											$first_child = false;
										} ?>
											
									</div>
										<!-- /accordion --> 
								</div>
						  	</div> <!-- end of #lessons -->


						</div> <!-- end of .tab-content -->
					</div><!-- end of .col-lg-8 -->
					<aside class="col-lg-4 margin_60_35" id="sidebar">
						<div class="box_detail">
							<h3 class="title"><?=$course->course_title_arabic?></h3>
							<p><i class="pe-7s-clock"></i> <?=$course->course_duration_arabic?></p>
							<figure>							
									<?php if ($course->course_banner) { ?>
										<img src="<?=base_url($course->course_banner)?>" alt="" class="img-fluid">
									<?php } else {?>
										<img src="<?=base_url('site-assets/img/no-image.jpg')?>" alt="" class="img-fluid">
									<?php } ?>
									
							</figure>
												
						</div>
					</aside>
				</div>
			</div>
			
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php 
	$this->load->view('site/arabic/includes/footer'); 
	?>
	</div>
	<!-- page -->
	
	<?php 
	$this->load->view('site/arabic/includes/scripts'); 
	?>
	
	<script type="text/javascript">

		$('#answer-attachment').on('change', function () {
		    var file = $("#answer-attachment")[0].files[0];
		    var val = file.type;
		    var type = val.substr(val.indexOf("/") + 1);
		    if (type == 'doc' || type == 'docx' || type == 'pdf' || type == 'jpg' || type == 'jpeg' || type == 'png') {
		      
		    }
		    else {
		      $("#answer-attachment").val('');
			  toastr.error("File format not supported","");
		    }
		});

		$('#answer-form').on('submit', function(e){	

			e.preventDefault();
			var self = this;

			
			var answer = $('#answer').val();
			var attachment = $("#answer-attachment").val();
			
			//alert(attachment);
			if ( answer == '' && attachment == '') {
				toastr.error('Both empty!');				
			}	

			else {
				  
				  self.submit();
				}
			  
		});


	</script>
  
</body>
</html>