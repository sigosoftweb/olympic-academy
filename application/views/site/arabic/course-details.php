<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >
    
    <?php 
    $this->load->view('site/arabic/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/arabic/includes/header'); 
	?>
	
	<main>
		
			<section id="hero_in" class="courses">
				<div class="wrapper">
					<div class="container">
						<h1 class="fadeInUp"><span></span><?=$course->course_title_arabic?></h1>
					</div>
				</div>
			</section>
			<!--/hero_in-->
		

		<div class="bg_color_1">
			<nav class="subscribed-menu sticky_horizontal">
				<div class="container">					
					<ul class="nav nav-tabs" id="myTab" role="tablist">
					  	<li class="nav-item">
					    	<a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">التفاصيل</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="trainers-tab" data-toggle="tab" href="#trainers" role="tab" aria-controls="trainers" aria-selected="false">المدربون</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="calendar-tab" data-toggle="tab" href="#calendar" role="tab" aria-controls="calendar" aria-selected="false">التقويم</a>
					  	</li>
					  	
					</ul>
				</div>
			</nav>
			<div class="container ">
				<div class="row">
					<div class="col-lg-12">
						<div class="tab-content" id="myTabContent">

						  	<div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
						  		<div class="container margin_60_35">					
									<?php //echo $students_count; ?>
									<div class="row">
										<div class="col-lg-8">
											<h2>وصف موجز</h2>
											<?=$course->course_description_arabic?>
											<?php if($sections_count->scount) { ?>
											<div class="container margin_60_35">
												<div class="intro_title">
													<h2>التفاصيل</h2>
													<ul>
														<li><?=$sections_count->scount?> الدروس</li>						
													</ul>
												</div>
												<div id="accordion_lessons" role="tablist" class="add_bottom_45">
													<?php 
													$first_child = true;
													foreach ($course_sections as $course_section) { ?>
														<div class="card">
														<!-- .card-header starts -->
															<div class="card-header" role="tab" id="heading-<?=$course_section->cs_id?>">
																<h5 class="mb-0">
																	<?php if($first_child) { ?>
																		<a data-toggle="collapse" href="#collapse-<?=$course_section->cs_id?>" aria-expanded="true" aria-controls="collapse-<?=$course_section->cs_id?>"><i class="indicator icon-minus"></i>
																	<?php } else { ?>
																		<a class="collapsed" data-toggle="collapse" href="#collapse-<?=$course_section->cs_id?>" aria-expanded="false" aria-controls="collapse-<?=$course_section->cs_id?>">
																			<i class="indicator icon-plus"></i>

																	<?php } ?>
																	<?=$course_section->section_title_arabic?></a>
																</h5>
															</div>
															<!-- .card-header ends -->

															<?php if($first_child) { ?>
															<div id="collapse-<?=$course_section->cs_id?>" class="collapse show" role="tabpanel" aria-labelledby="heading-<?=$course_section->cs_id?>" data-parent="#accordion_lessons">
															<?php } else { ?>
															<div id="collapse-<?=$course_section->cs_id?>" class="collapse" role="tabpanel" aria-labelledby="heading-<?=$course_section->cs_id?>" data-parent="#accordion_lessons">
															<?php } ?>	
																<div class="card-body">
																	<div>
																		<?=$course_section->section_description_arabic?>
																	</div>
																		
																</div>
															</div>
														</div>
														<!-- /card -->
														<?php 
														$first_child = false;
													} ?>
															
												</div>
												<!-- /accordion --> 
											</div>						
											<?php } ?>
										</div>

										<aside class="col-lg-4" id="sidebar">
											<div class="box_detail">
												
												<h3 class="title"><?=$course->course_title_arabic?></h3>
												<p><i class="pe-7s-clock"></i> <?=$course->course_duration_arabic?></p>
												
												<figure>								
														<?php if ($course->course_banner) { ?>
															<img src="<?=base_url($course->course_banner)?>" alt="" class="img-fluid">
														<?php } else {?>
															<img src="<?=base_url('site-assets/img/no-image.jpg')?>" alt="" class="img-fluid">
														<?php } ?>
														
												</figure>
												<div class="price">
													<p>فردي</p>
													<?=$course->course_sale_price?> BHD							
												</div>
												<?php /*<div class="price">
													<p>Tamkeen</p>
													<?=$course->course_price?> BHD							
												</div>*/ ?>											
												<?php if($subscription_message){ ?>
												    <?=$subscription_message?>
												<?php }else{ ?>
												    <?php if($students_count < $course->student_limit) { ?>
    													<a href="<?=base_url('ar/student/subscribe/'. $course->course_id)?>" class="btn_1 full-width">اشترك</a>
    												<?php } else { ?>
    													<p>Seats Filled!</p>
    												<?php } ?>
												<?php } ?>
												
											</div>
										</aside>
									</div>
									<!-- /row -->
								</div>

								
							</div>	<!-- #description -->							

							<div class="tab-pane fade" id="trainers" role="tabpanel" aria-labelledby="trainers-tab">
								<div class="container margin_60_35">
									<div class="row">
										<div class="col-lg-8">
											<h2>المدربون</h2>
											<?php if(!empty($trainers)) { ?>
								  				<div class="row trainers mt-4">
								  				
								  				<?php foreach ($trainers as $trainer) { ?>
								  					
								  					<div class="col-lg-4 col-md-6 col-sm-6">
														<div class="box_grid text-center wow">
															<figure>
																<div class="block-horizzontal"></div>
																<a href="<?=base_url('ar/trainers/details/' . $trainer->trainer_id) ?>">
																<?php if($trainer->trainer_image) { ?>
																	<div class="box-image" style="background-image: url(<?=base_url($trainer->trainer_image)?>);"></div>
																<?php }
																else { ?>
																	<div class="box-image" style="background-image: url(<?=base_url('site-assets/img/user.png')?>);"></div>
																<?php } ?>
																</a>
															</figure>
															<div class="trainer-details">
																<a href="<?=base_url('ar/trainers/details/' . $trainer->trainer_id) ?>">
																	<h3><?=$trainer->arabic_first . ' '. $trainer->arabic_second?></h3>
																</a>
																<h5><?=$trainer->education_arabic?></h5>
																<p><?=$trainer->experience_arabic?></p>
															</div>
														</div>
													</div>
												<?php } ?>

												</div>		
											<?php } 
											else {?>
												<div class="text-center margin_60_35">
													<figure><img src="<?=base_url()?>site-assets/img/wizard_intro_icon.svg" alt=""></figure>
													<h3>Trainers not scheduled!</h3>
												</div>
											<?php } ?>

											
											<?php if(!empty($organisers)) { ?>
												<h2 class="pt-5">Organizers</h2>
								  				<div class="row trainers mt-4">
								  				
								  				<?php foreach ($organisers as $organiser) { ?>
								  					
								  					<div class="col-lg-4 col-md-6 col-sm-6">
														<div class="box_grid text-center">
															<figure>
																<?php if($organiser->organiser_image) { ?>
																	
																	<div class="box-image" style="background-image: url(<?=base_url($organiser->organiser_image)?>);"></div>
																<?php }
																else { ?>
																	<div class="box-image" style="background-image: url(<?=base_url('site-assets/img/user.png')?>);"></div>
																<?php } ?>
															</figure>
															<div class="trainer-details">
																<h3><?=$organiser->arabic_first . ' ' . $organiser->arabic_second ?></h3>
																<h5><?=$organiser->education_arabic?></h5>
																<p><?=$organiser->experience_arabic?></p>
															</div>
														</div>
													</div>
												<?php } ?>

												</div>		
											<?php } ?>
											
										</div>
										<aside class="col-lg-4" id="sidebar">
											<div class="box_detail">
												
												<h3 class="title"><?=$course->course_title_arabic?></h3>
												<p><i class="pe-7s-clock"></i> <?=$course->course_duration_arabic?></p>
												
												<figure>								
														<?php if ($course->course_banner) { ?>
															<img src="<?=base_url($course->course_banner)?>" alt="" class="img-fluid">
														<?php } else {?>
															<img src="<?=base_url('site-assets/img/no-image.jpg')?>" alt="" class="img-fluid">
														<?php } ?>
														
												</figure>
												<div class="price">
													<p>فردي</p>
													<?=$course->course_sale_price?> BHD							
												</div>
												<?php /*<div class="price">
													<p>Tamkeen</p>
													<?=$course->course_price?> BHD							
												</div>*/ ?>											
												<?php if($subscription_message){ ?>
												    <?=$subscription_message?>
												<?php }else{ ?>
												    <?php if($students_count < $course->student_limit) { ?>
    													<a href="<?=base_url('ar/student/subscribe/'. $course->course_id)?>" class="btn_1 full-width">اشترك</a>
    												<?php } else { ?>
    													<p>Seats Filled!</p>
    												<?php } ?>
												<?php } ?>
												
											</div>
										</aside>
									</div>
								</div>
							</div> <!-- #trainers -->

							<div class="tab-pane fade" id="calendar" role="tabpanel" aria-labelledby="calendar-tab">
						  		<div class="container margin_60_35 calendar">

									<h2>تقويم الدورة - <?=$course->course_title_arabic?></h2>
							
									<ul class="calendar-colors">
										<?php /*<li class="section-green"><i class="icon_easel"></i> Lesson</li>*/?>
										<li class="class-yellow"> صف عبر الإنترنت <i class="icon_mic_alt"></i></li>
										<li class="exam-blue"> الامتحان <i class="icon_pencil-edit"></i></li>
									</ul>

									<div class="box_general" id="calendar-box" style="clear: both;">	
								
										<div class="table-responsive">	
											<ul class="calendar-navigators">
												<li><a class="btn_1 cal-navs next" data-year= "<?=$current_calendar['year'] ?>" data-month="<?=$current_calendar['month'] - 1 ?>" data-course="<?=$course->course_id ?>" href="#"><i class="ti-angle-right" ></i></a></li>
										
												<li><a class="btn_1 cal-navs prev" data-year= "<?=$current_calendar['year'] ?>" data-month="<?=$current_calendar['month'] + 1 ?>" data-course="<?=$course->course_id ?>" href="#"><i class="ti-angle-left" ></i></a></li>
												
											</ul>			
											<?=$calendar?>
										</div>
									</div>
								</div>
							
							</div> <!-- #calendar -->
							
						</div> <!-- .tab-content -->					
						
					</div>
					<!-- /col -->				
					
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php 
	$this->load->view('site/arabic/includes/footer'); 
	?>
	</div>
	<!-- page -->
	
	<?php 
	$this->load->view('site/arabic/includes/scripts'); 
	?>

	<script type="text/javascript">


		$(document).on('click', '.cal-navs',function(e) {

			e.preventDefault();
			//alert("click");
			var course_id = $(this).attr('data-course');
			var year = $(this).attr('data-year');
			var month = $(this).attr('data-month');


			$.ajax({
				url: '<?=base_url('ar/courses/ajax_calendar/')?>',
				data: {'course_id': course_id, 'year': year, 'month': month},
				type: 'POST',

				success: function(response) {
					console.log(response);
					$('#calendar-box').html(response);
						  		
				},
				error: function(){
					toastr.error('Error !');
				}
					  	
			});
		});

		$('body').tooltip({
			selector: '[data-toggle="tooltip"]'
		});
	
	</script>
	
  
</body>
</html>