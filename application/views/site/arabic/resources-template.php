
							<h5><?= $course_title?></h5>
							<ul>
								<?php foreach ($resources as $resource) { ?>
									<li>
										<?php switch ($resource->cr_type) {
											case 'Document':
												echo "<i class='icon-doc-text-inv'></i>";
												break;
											case 'Video':
												echo "<i class='icon-video'></i>";
												break;
											case 'Image':
												echo "<i class='icon-picture'></i>";
												break;
											default:
												echo "<i class='icon-attach'></i>";
												break;
										} ?>
										<a href="<?=base_url($resource->cr_attachment) ?>" target="_blank">
											<span><?=$resource->cr_title ?></span>
										</a>
									</li>
								<?php } ?>
							
							</ul>

							