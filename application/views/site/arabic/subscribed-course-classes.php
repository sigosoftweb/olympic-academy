﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/arabic/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/arabic/includes/header'); 
	?>
	
	<main>
		

		<div class="bg_color_1">

			<nav class="subscribed-menu sticky_horizontal">
				<div class="container">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
					  	<li class="nav-item">
					    	<a class="nav-link" id="description-tab" href="<?=base_url('ar/subscriptions/details/') . $course->course_id ?>">التفاصيل</a>
					  	</li>

					  	<li class="nav-item">
					    	<a class="nav-link active" id="classes-tab" href="<?=base_url('ar/subscriptions/classes/') . $course->course_id ?>">الفصول</a>
					  	</li>
					  	
					  	<li class="nav-item">
					    	<a class="nav-link" id="exams-tab" href="<?=base_url('ar/subscriptions/exams/') . $course->course_id ?>">الامتحانات</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="attendance-tab" href="<?=base_url('ar/subscriptions/attendance/') . $course->course_id ?>">الحضور</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="trainers-tab" href="<?=base_url('ar/subscriptions/trainers/') . $course->course_id ?>">المدربون</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('ar/subscriptions/assignments/') . $course->course_id ?>">الواجبات</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('ar/subscriptions/certificates/') . $course->course_id ?>">شهادة</a>
					  	</li>
					</ul>
				</div>
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="tab-content" id="myTabContent">	  	

							<div class="tab-pane fade show active" id="classes" role="tabpanel" aria-labelledby="classes-tab">
						  		<div class="container margin_60_35">
						  			<?php //print_r($course_classes); 
						  			//echo '<br>';
						  			//print_r($batchIDs);?>
						  			<div class="intro_title">
										<h2>الفصول القادمة</h2>
										<ul>
											<li><?=$classes_count->c_count?> الفصول</li>
										</ul>
									</div>

						  			<div id="accordion_classes" role="tablist" class="add_bottom_45">
										<?php 
										$first_child = true;
										foreach ($course_classes as $course_class) { ?>
											<div class="card">
												<!-- .card-header starts -->
												<div class="card-header" role="tab" id="class-title-<?=$course_class->cc_id?>">
													<h5 class="mb-0">
														<?php if($first_child) { ?>
															<a data-toggle="collapse" href="#class-<?=$course_class->cc_id?>" aria-expanded="true" aria-controls="class-<?=$course_class->cc_id?>"><i class="indicator icon-minus"></i>
														<?php } else { ?>
															<a class="collapsed" data-toggle="collapse" href="#class-<?=$course_class->cc_id?>" aria-expanded="false" aria-controls="class-<?=$course_class->cc_id?>">
															<i class="indicator icon-plus"></i>

														<?php } ?>
														<?=$course_class->topic_arabic?>
														<span class="float-left"><?=$course_class->duration?> دقيقة</span>
														</a>
													</h5>
												</div>
												<!-- .card-header ends -->

												<?php if($first_child) { ?>
												<div id="class-<?=$course_class->cc_id?>" class="collapse show" role="tabpanel" aria-labelledby="class-title-<?=$course_class->cc_id?>" data-parent="#accordion_classes">
												<?php } else { ?>
												<div id="class-<?=$course_class->cc_id?>" class="collapse" role="tabpanel" aria-labelledby="class-title-<?=$course_class->cc_id?>" data-parent="#accordion_classes">
												<?php } ?>	
													<div class="card-body">
														<div>
															
														</div>
														
														<div class="list_lessons">
															<ul>										<li>Lesson <span><?=$course_class->section_title_arabic?></span></li>	
																<?php if(is_in_array($batchIDs, 'cb_id', $course_class->batch_id)) {?>
																	<li>Batch <span><?=$course_class->batch_name_arabic?></span></li>

																<?php } ?>			
																<li>Meeting Password<span><?=$course_class->meeting_password?></span></li>
																<li>التاريخ <span><?=date('d-m-Y', strtotime($course_class->starting_time))?></span></li>

																<li>Time <span><?=getArabicDate(date('h:i A', strtotime($course_class->starting_time)))?></span></li>
																
																	
															</ul>
															<div class="row">
																<div class="col-sm-6">
																	<div class="class-trainer">
																		<img src="<?=base_url($course_class->trainer_image)?>" alt="" class="img-fluid">
																		<h5><?=$course_class->arabic_first?></h5>
																		<p>Trainer</p>
																	</div>
																</div>
																<div class="col-sm-6 pt-4">
																	<a href="<?=$course_class->join_url?>" target="_blank" class="btn-exam float-left">Start</a>
																</div>
															</div>
														</div> <!-- end of .list_lessons -->
														
													</div>
												</div>
											</div>
											<!-- /card -->
											<?php 
											$first_child = false;
										} ?>
											
									</div>
									<!-- /accordion --> 
									<?php //print_r($completed_classes);?>

									<?php //if($completed_classes) { ?>
									<div class="margin_60_35">	
										<div class="intro_title">
											<h2>دروس مكتملة</h2>
											<ul>
												<li><?=$completed_count->c_count?> الفصول</li>
											</ul>
										</div>
										<div id="accordion_completed_classes" role="tablist" class="add_bottom_45">
											<?php 
											$first_child = true;
											foreach ($completed_classes as $completed_class) { ?>
												<div class="card">
													<!-- .card-header starts -->
													<div class="card-header" role="tab" id="mclass-title-<?=$completed_class->class_id?>">
														<h5 class="mb-0">
															<?php if($first_child) { ?>
																<a data-toggle="collapse" href="#mclass-<?=$completed_class->class_id?>" aria-expanded="true" aria-controls="mclass-<?=$completed_class->class_id?>"><i class="indicator icon-minus"></i>
															<?php } else { ?>
																<a class="collapsed" data-toggle="collapse" href="#mclass-<?=$completed_class->class_id?>" aria-expanded="false" aria-controls="mclass-<?=$completed_class->class_id?>">
																<i class="indicator icon-plus"></i>

															<?php } ?>
															<?=$completed_class->topic_arabic?>
															<span class="float-left"><?=$completed_class->duration?> دقيقة</span>
															</a>
														</h5>
													</div>
													<!-- .card-header ends -->

													<?php if($first_child) { ?>
													<div id="mclass-<?=$completed_class->class_id?>" class="collapse show" role="tabpanel" aria-labelledby="mclass-title-<?=$completed_class->class_id?>" data-parent="#accordion_completed_classes">
													<?php } else { ?>
													<div id="mclass-<?=$completed_class->class_id?>" class="collapse" role="tabpanel" aria-labelledby="mclass-title-<?=$completed_class->class_id?>" data-parent="#accordion_completed_classes">
													<?php } ?>	
														<div class="card-body">
															<div>
																
															</div>
															
															<div class="list_lessons">
																<ul>								<li>Lesson <span><?=$completed_class->section_title_arabic?></span></li>	
																	<li>Date <span><?=date('d-m-Y', strtotime($completed_class->starting_time))?></span></li>		
																	
																	<?php if($completed_class->attachment) { ?>
																	<li>
																		<?php
																		switch($completed_class->type) {
																			case 'Documents':
																				echo "<i class='icon-doc-text-inv'></i>";
																				break;
																			case 'Video':
																				echo "<i class='icon-video'></i>";
																				break;
																			case 'Image':
																				echo "<i class='icon-picture'></i>";
																				break;
																			default:
																				echo "<i class='icon-attach'></i>";
																				break;
																		}
																		?>

																		<a href="<?=base_url($completed_class->attachment)?>" target="_blank"><?=$completed_class->title_arabic?></a></li>
																	<?php } ?>
																		
																</ul>
																
															</div> <!-- end of .list_lessons -->
															
														</div>
													</div>
												</div>
												<!-- /card -->
												<?php 
												$first_child = false;
											} ?>
												
										</div>
										<!-- /accordion --> 
									</div>
									<?php //} ?>
						  		</div>
						  	</div>


						</div> <!-- end of .tab-content -->
					</div><!-- end of .col-lg-8 -->
					<aside class="col-lg-4 margin_60_35" id="sidebar">
						<div class="box_detail">
							<h3 class="title"><?=$course->course_title_arabic?></h3>
							<p><i class="pe-7s-clock"></i> <?=$course->course_duration_arabic?></p>
							<figure>							
									<?php if ($course->course_banner) { ?>
										<img src="<?=base_url($course->course_banner)?>" alt="" class="img-fluid">
									<?php } else {?>
										<img src="<?=base_url('site-assets/img/no-image.jpg')?>" alt="" class="img-fluid">
									<?php } ?>
									
							</figure>
												
						</div>
					</aside>
				</div>
			</div>
			
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php 
	$this->load->view('site/arabic/includes/footer'); 
	?>
	</div>
	<!-- page -->
	
	<?php 
	$this->load->view('site/arabic/includes/scripts'); 
	?>
	
  
</body>
</html>