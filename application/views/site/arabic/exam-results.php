<!DOCTYPE html>
<html lang="en">

<head>

    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php
    $this->load->view('site/arabic/includes/styles');
    ?>

</head>

<body>

	<div id="page">

	<?php $this->load->view('site/arabic/includes/header'); ?>

	<main>
		

		<div class="bg_color_1 exam-results">
			<div class="container margin_60_35">
				<h3 class="no-sidebar-page-title text-center">نتائج الامتحانات</h3>
				<h3 class="exam-title text-center pb-3"><?=$result->exam_name_arabic?></h3>
				<div class="row">
					<div class="col-lg-12">
						<div class="score-sheet">
							<div class="row score-row">
								<div class="col-8 title">
									<p>عدد الأسئلة</p>
								</div>
								<div class="col-4 ">
									<p><?=$result->questions?></p>
								</div>
							</div>
							<div class="row score-row">
								<div class="col-8 title">
									<p>حضر</p>
								</div>
								<div class="col-4 ">
									<p><?=$result->attended?></p>
								</div>
							</div>
							<div class="row score-row">
								<div class="col-8 title">
									<p>مغادرة</p>
								</div>
								<div class="col-4 ">
									<p><?=$result->left_questions?></p>
								</div>
							</div>
							<!--<div class="row score-row">-->
							<!--	<div class="col-8 title">-->
							<!--		<p>Correct Answers</p>-->
							<!--	</div>-->
							<!--	<div class="col-4">-->
							<!--		<p><?=$result->correct_answers?></p>-->
							<!--	</div>-->
							<!--</div>-->
							<!--<div class="row score-row">-->
							<!--	<div class="col-8 title">-->
							<!--		<p>Wrong Answers</p>-->
							<!--	</div>-->
							<!--	<div class="col-4">-->
							<!--		<p><?=$result->wrong_answers?></p>-->
							<!--	</div>-->
							<!--</div>-->
							<div class="row score-row">
								<div class="col-8 title">
									<p>الوقت المستغرق</p>
								</div>
								<div class="col-4 ">
									<p>
										<?=$result->time_taken?>
									</p>
								</div>
							</div>

							<?php if($result->show_result == '1'){ ?>
							    <div class="row score-row total-score">
    								<div class="col-12 title">
    									<h5><i class="ti-clipboard"></i>  مجموع الدرجات</h5>
    								</div>
    								<div class="col-12">
    									<h6><span><?=$result->mark?></span> / <?=$result->total_mark?></h6>
    								</div>
    							</div>
							<?php } ?>

						</div>
					</div>

				</div>
			</div>
		</div>

	</main>
	<!--/main-->

	<?php $this->load->view('site/arabic/includes/footer'); ?>
	</div>
	<!-- page -->

	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/arabic/includes/scripts'); ?>
	<script>

		$(document).ready(function() {

			var countDownDate = new Date($('#start-time').html()).getTime();
			var x = setInterval(function() {

				var now = new Date().getTime();

				var distance = countDownDate - now;

				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);




				$('.time-left-timer').html(('0' + hours).slice(-2) + " : " + ('0' + minutes).slice(-2) + " : " + ('0' + seconds).slice(-2));


				if (distance < 0) {
				    clearInterval(x);
				    $('.exam-timer').html('Expired');
			 	}

			}, 1000);
		});
	</script>

</body>
</html>
