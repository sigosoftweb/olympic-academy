﻿<!DOCTYPE html>
<html>

<head>
	<meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >       
    <?php 	$this->load->view('site/arabic/includes/styles'); 	?>
</head>

<body>
		
	<div id="page">
		
	<?php 
	$this->load->view('site/arabic/includes/header'); 
	?>
	
	<main>		

		<div id="home-slider" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<?php for ($i=0; $i < $slides_count->scount; $i++) { ?>
					<li data-target="#home-slider" data-slide-to="<?=$i?>" 
						<?php if($i==0) { echo 'class="active"'; }?> ></li>
				<?php } ?>
				
			</ol>
		 	<div class="carousel-inner">

		 		<?php 
		 		$first_slide = true;
		 		foreach ($slides as $slide) { ?>
		 			<div class="carousel-item 
		 			<?php if($first_slide) { echo 'active'; $first_slide = false; } ?> " >
		 				<div style="background-image: url(<?=base_url($slide->image)?>);"></div>
				      	<?php /*<img class="w-100" src="<?=base_url($slide->image)?>" alt="<?=$slide->name?>">*/?>
				    </div>
		 		<?php } ?>
			    
			    
		  	</div>
		  
		</div>

		<div class="features clearfix">
			<div class="container">
				<ul>
					<li><i class="pe-7s-study"></i>
						<h4>+200 المشارك</h4><span>مجموعة متنوعة من الدورات</span>
					</li>
					<li><i class="pe-7s-cup"></i>
						<h4>المحاضرين</h4><span>اعثر على المدرب المناسب لك</span>
					</li>
					<li><i class="pe-7s-target"></i>
						<h4>التركيز على الهدف</h4><span>زيادة خبرتك الشخصية</span>
					</li>
				</ul>
			</div>
		</div>
		<!-- /features -->
		<?php //print_r($popular_courses); ?>
		<?php /*<div class="container-fluid margin_70_70">
			<div class="main_title_2">
				<span><em></em></span>
				<h2>Popular Courses</h2>
				<p></p>
			</div>
			<div class="row">
				<?php 
				//print_r($popular_courses);
				foreach($popular_courses as $popular_course) { ?>
				<div class="col-xl-3 col-lg-4 col-sm-6">
					<div class="box_grid">
						<figure>
							
							<a href="<?=base_url('ar/courses/details/'. $popular_course->course_id)?>">
								
								<?php if($popular_course->course_banner) { ?>
									<img src="<?=base_url($popular_course->course_banner)?>" class="img-fluid" alt="">
								<?php } else { ?>
									<img src="<?=base_url('site-assets/img/no-image.jpg')?>" class="img-fluid" alt="">
								<?php }?>
								
							</a>
							<div class="price"><?=$popular_course->course_sale_price?> BHD</div>

						</figure>
						<div class="wrapper">
							<small><?=$popular_course->cat_name_arabic?></small>
							<h3><?=$popular_course->course_title_arabic?></h3>
							<p><?=getExcerpt($popular_course->course_description_arabic)?></p>
							
						</div>
						<ul>
							<li><i class="pe-7s-clock"></i><?=$popular_course->course_duration_arabic?></li>
							<?php if(isStudent()) {
								
								//print_r($courseIDs);
								//echo "id=" . $course->course_id;
								if(is_in_array($courseIDs, 'course_id', $popular_course->course_id)) { ?>
										<li><a href="<?=base_url('ar/subscriptions/details/'. $popular_course->course_id)?>">Subscribed!</a></li>
								<?php } else { ?>
										<li><a href="<?=base_url('ar/courses/details/'. $popular_course->course_id)?>">Enroll now</a></li>
								<?php }
							} else {?>
								<li><a href="<?=base_url('ar/courses/details/'. $popular_course->course_id)?>">Enroll now</a></li>
							<?php } ?>
							
							

						</ul>
					</div>
				</div>
				<!-- /item -->
				<?php } ?>


			</div>
			<!-- /carousel -->
			<div class="">
				<p class="btn_home_align"><a href="<?=base_url('ar/courses')?>" class="btn_1 rounded">View all courses</a></p>
			</div>
			<!-- /container -->
			
		</div>
		<!-- /container --> */?>
		<div class="bg_color_3">
			<div class="container margin_70_70">
				<div class="main_title_2">
					<span><em></em></span>
					<h2>الدورات</h2>
					<p></p>
				</div>
				<div class="row">
					<?php 
					foreach($courses as $course) { ?>
					<div class="col-xl-4 col-lg-4 col-sm-6">
						<div class="box_grid wow courses">
							<figure>
								<div class="block-horizzontal"></div>							
									<?php if($course->course_banner) { ?>
										<?php /*<img src="<?=base_url($course->course_banner)?>" class="img-fluid" alt="">*/?>
										<div class="banner" style="background-image: url(<?=base_url($course->course_banner)?>);"></div>
									<?php } else { ?>
										<?php /*<img src="<?=base_url('site-assets/img/no-image.jpg')?>" class="img-fluid" alt="">*/ ?>
										<div class="banner" style="background-image: url(<?=base_url('site-assets/img/no-image.jpg')?>);"></div>
									<?php }?>
									
								
								<div class="price"><p>فردي</p><?=$course->course_sale_price?> BHD</div>
								<?php /*<div class="price tamkeen"><p>Tamkeen</p><?=$course->course_price?> BHD</div>*/?>

							</figure>
							<div class="wrapper">
								<small><?=$course->cat_name_arabic?></small>
								<h3><?=$course->course_title_arabic?></h3>
								<p><?=getExcerpt($course->course_description_arabic)?></p>
								
							</div>
							<ul>
								<li><i class="pe-7s-clock"></i><?=$course->course_duration_arabic?></li>
								<?php if(isStudent()) {
									
									//print_r($courseIDs);
									//echo "id=" . $course->course_id;
									if(!empty($courseIDs) && is_in_array($courseIDs, 'course_id', $course->course_id)) { ?>
										<li><a href="<?=base_url('ar/subscriptions/details/'. $course->course_id)?>">مشترك!</a></li>
									<?php } else { 
										if($course->sequence_id == 0) { ?>
											<?php if($course->students_count < $course->student_limit) { ?>
												<li><a href="<?=base_url('ar/courses/details/'. $course->course_id)?>">سجل الآن</a></li>
											<?php } else { ?>
												<li><a href="<?=base_url('ar/courses/details/'. $course->course_id)?>">سجل الآن</a></li>
											<?php } ?>
											
										<?php } else {
											if(!empty($courseIDs) && is_in_array($courseIDs, 'course_id', $course->sequence_id)) { ?>
												<li><a href="<?=base_url('ar/courses/details/'. $course->course_id)?>">سجل الآن</a></li>
											<?php }	else {?>	
												<li><a href="<?=base_url('ar/courses/details/'. $course->course_id)?>">سجل الآن</a></li>
											<?php } 
										}
									}									
								
								} else {?>
									<?php if($course->students_count < $course->student_limit) { ?>
										<li><a href="<?=base_url('ar/courses/details/'. $course->course_id)?>">سجل الآن</a></li>
									<?php } else { ?>
										<li><a href="<?=base_url('ar/courses/details/'. $course->course_id)?>">سجل الآن</a></li>
									<?php } ?>
								<?php } ?>
								
								

							</ul>
						</div>
					</div>
					<!-- /item -->
					<?php } ?>


				</div>
				<!-- /carousel -->
				<div class="">
					<p class="btn_home_align"><a href="<?=base_url('ar/courses')?>" class="btn_1 rounded">مشاهدة جميع الدورات</a></p>
				</div>
				<!-- /container -->
				
			</div>
			<!-- /container -->
		</div>

		<?php /* <hr>
		<div class="container margin_30_95">
			<div class="main_title_2">
				<span><em></em></span>
				<h2>Categories Courses</h2>
				<p></p>
			</div>
			<div class="row">

				<?php foreach ($categories as $category) { ?>
					
				
				<div class="col-lg-4 col-md-6 wow" data-wow-offset="150">
					<a href="#" class="grid_item category-link">
						<?php if($category->cat_image) { 
							$bgimage = base_url($category->cat_image);
						} else { 
							$bgimage = base_url() . "site-assets/img/no-image.jpg";
						} ?>
						<figure class="block-reveal"style="background-image: url(<?=$bgimage?>)" >
							<div class="block-horizzontal"></div>
							
							
							<div class="info">
								<small><i class="ti-layers"></i><?=$category->course_count?> Courses</small>
								<h3><?=$category->cat_name?></h3>							
							</div>
						</figure>
						<form  method="post" action="<?=base_url('ar/courses')?>">
							<input type="hidden" name="category" value="<?=$category->cat_id?>"/>
							<input type="hidden" name="listing_filter" value="all"/>
						</form>
					</a>
				</div>
				<!-- /grid_item -->
				<?php } ?>
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->*/?>

		<?php /*<div class="call_section call_section_calendar">
			<div class="container clearfix">
				<div class="col-lg-5 col-md-6 float-right wow" data-wow-offset="250">
					<div class="block-reveal">
						<div class="block-vertical"></div>
						<div class="box_1">
							<h3>Course Calendar</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
							<a href="<?=base_url('ar/calendar') ?>" class="btn_1 rounded">Explore</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/call_section--> */?>
		<div class="bg_color_2">
			<div class="container margin_70_70">
				<div class="main_title_2">
					<span><em></em></span>
					<h2>تقويم الدورة</h2>
					<p></p>
				</div>
				<div class="row">
					<div class="col-12 text-center">
						<p>ت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم دولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت كياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي فوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي دولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم ايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, كونسيكتيتيور,أديبايسكاي فيلايت, سيد كيواي نون نيومكيوام ايايوس موداي تيمبورا انكايديونت يوت لابوري أيت دولار ماجنام ألايكيوام كيوايرات فوليوبتاتيم. يوت اينايم أد مينيما فينيام, كيواس نوستريوم أكسيركايتاشيم يلامكوربوريس سيوسكايبيت لابورايوسام, نايساي يوت ألايكيوايد أكس أيا كوموداي كونسيكيواتشر؟ كيوايس أيوتيم فيل أيوم أيوري ريبريهينديرايت كيواي ان إيا فوليوبتاتي فيلايت ايسسي كيوم نايهايل موليستايا كونسيكيواتيو,فيلايليوم كيواي دولوريم أيوم 	</p>
						<p class="btn_home_align"><a href="<?=base_url('ar/calendar')?>" class="btn_1 rounded">عرض التقويم</a></p>

					</div>
				</div>
			</div>
		</div>

		<div class="bg_color_4">
			<div class="container margin_70_70">
				<div class="main_title_2">
					<span><em></em></span>
					<h2>محاضرينا</h2>
					<p></p>
				</div>
				<?php //print_r($trainers); ?>
				<div class="row trainers">
					<?php 
					//print_r($trainers);
					foreach($trainers as $trainer) { ?>
					<div class="col-lg-3 col-sm-6">

						<div class="box_grid text-center wow">
							<figure>
								<div class="block-horizzontal"></div>
								<a href="<?=base_url('ar/trainers/details/' . $trainer->trainer_id) ?>">
								<?php if($trainer->trainer_image) { ?>
									<?php /*<img src="<?=base_url($trainer->trainer_image)?>" class="img-fluid" alt="<?=$trainer->trainer_name_english?>"> */?>
									<div class="box-image" style="background-image: url(<?=base_url($trainer->trainer_image)?>);"></div>
								<?php }
								else { ?>									
									<div class="box-image" style="background-image: url(<?=base_url('site-assets/img/user.png')?>);"></div>
								<?php } ?>
								</a>
							</figure>
							<div class="trainer-details">
								<a href="<?=base_url('ar/trainers/details/' . $trainer->trainer_id) ?>">
									<h3><?=$trainer->arabic_first . ' '. $trainer->arabic_second?></h3>
								</a>
								<h5><?=$trainer->education_arabic?></h5>
								<p><?=$trainer->experience_arabic?></p>
							</div>
						</div>
					</div>
					<!-- /item -->
					<?php } ?>


				</div>
				<!-- /carousel -->
				<div class="">
					<p class="btn_home_align"><a href="<?=base_url('ar/trainers')?>" class="btn_1 rounded">عرض جميع المدربين</a></p>
				</div>
			</div>

		
		
		</div>
			<!-- /bg_color_1 -->

		

		<?php if(isset($related_courses) && !empty($related_courses)) { ?>
			<?php //print_r($related_courses); ?>
			<div class="bg_color_2">
				<div class="container margin_70_70">
					<div class="main_title_2">
						<span><em></em></span>
						<h2>الدورات ذات الصلة</h2>
						<p></p>
					</div>
					<div class="row">
						<?php 
						foreach($related_courses as $related_course) { ?>
						<div class="col-xl-4 col-lg-4 col-sm-6">
							<div class="box_grid wow courses">
								<figure>
									<div class="block-horizzontal"></div>
									<a href="<?=base_url('ar/courses/details/'. $related_course->course_id)?>">
										
										<?php if($related_course->course_banner) { ?>
											
											<div class="banner" style="background-image: url(<?=base_url($related_course->course_banner)?>);"></div>
										<?php } else { ?>
											<div class="banner" style="background-image: url(<?=base_url('site-assets/img/no-image.jpg')?>);"></div>
										<?php }?>
										
									</a>
									<div class="price"><p>فردي</p><?=$course->course_sale_price?> BHD</div>
									<?php /*<div class="price tamkeen"><p>Tamkeen</p><?=$course->course_price?> BHD</div>*/?>

								</figure>
								<div class="wrapper">
									<small><?=$related_course->cat_name_arabic?></small>
									<h3><?=$related_course->course_title_arabic?></h3>
									<p><?=getExcerpt($related_course->course_description_arabic)?></p>
									
								</div>
								<ul>
									<li><i class="pe-7s-clock"></i><?=$related_course->course_duration_arabic?></li>
									<li><a href="<?=base_url('ar/courses/details/'. $related_course->course_id)?>">سجل الآن</a></li>									
								</ul>
							</div>
						</div>
						<!-- /item -->
						<?php } ?>


					</div>
					<!-- /carousel -->
					<div class="">
						<p class="btn_home_align"><a href="<?=base_url('ar/courses')?>" class="btn_1 rounded">مشاهدة جميع الدورات</a></p>
					</div>
					<!-- /container -->
					
				</div>
				<!-- /container -->
			</div>
		<?php } ?> 

		<div class="bg_color_3">
			<?php if($events) { ?>
				
					<div class="container margin_70_70">
						<div class="main_title_2">
							<span><em></em></span>
							<h2>الأحداث القادمة</h2>
							<p></p>
						</div>
						<div class="row">
							<?php foreach ($events as $event) { ?>							
								<div class="col-lg-6">
									<a class="box_news wow" href="<?=base_url('ar/events/details/').$event->event_id?>">
										<figure style="background-image: url(<?=base_url($event->image)?>)">
											<div class="block-horizzontal"></div>
											<!-- <img src="http://via.placeholder.com/500x333/ccc/fff/news_home_1.jpg" alt=""> -->
											<figcaption><strong><?=date('d', strtotime($event->start_time))?></strong><?=getArabicDate(date('M', strtotime($event->start_time)))?></figcaption>
										</figure>
										<ul>
											<li><?=date('d.m.Y', strtotime($event->start_time))?>
											
											<?php if(date('h:i a', strtotime($event->start_time))) { echo  getArabicDate(date(', h:i a', strtotime($event->start_time)));}?>
												
											</li>
											<li>
											<?php if($event->end_time != '0000-00-00 00:00:00') {
												echo " - ". getArabicDate(date('  d.m.Y, h:i a', strtotime($event->end_time)));

											}?>
											
											</li>
										</ul>
										<h4><?=$event->title_arabic?></h4>
										<p><?=getExcerpt($event->description_arabic)?></p>
									</a>
								</div>
							<?php } ?>
							
						</div>
						<!-- /row -->
						<p class="btn_home_align"><a href="<?=base_url('ar/events')?>" class="btn_1 rounded">عرض جميع الأحداث</a></p>
					</div>
					<!-- /container -->
				
			<?php } ?>
		</div>

		<div class="call_section">
			<div class="container clearfix">
				<div class="col-lg-5 col-md-6 float-left wow" data-wow-offset="250">
					<div class="block-reveal">
						<div class="block-vertical"></div>
						<div class="box_1">
							<h3>تمتع بمجتمع طلابي عظيم</h3>
							<p>سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم دولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت كياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي فوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي دولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم ايست,كيواي دولوريم ايبسيوم كيوا دولار سايت أميت, 			</p>			
							<a href="<?=base_url('ar/about') ?>" class="btn_1 rounded float-left">اقرأ المزيد</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/call_section-->
	</main>
	<!-- /main -->

	<?php 
	$this->load->view('site/arabic/includes/footer'); 
	?>
	</div>
	<!-- page -->
	
	<?php 
	$this->load->view('site/arabic/includes/scripts'); 
	?>

	<script type="text/javascript">
		$('.carousel').carousel({
		  interval: 5000
		});

		$('.category-link').click(function(e) {
			e.preventDefault();
			//alert($('.listing-filter').val());
			$(this).find('form').submit();
		});

		
	</script>

	<script type="text/javascript">


		$(document).on('click', '.cal-navs',function(e) {

			e.preventDefault();
			//alert("click");
			var year = $(this).attr('data-year');
			var month = $(this).attr('data-month');


			$.ajax({
				url: '<?=base_url('ar/calendar/ajax_calendar/')?>',
				data: {'year': year, 'month': month},
				type: 'POST',

				success: function(response) {
					console.log(response);
					$('#calendar-box').html(response);
						  		
				},
				error: function(){
					toastr.error('Error !');
				}
					  	
			});
		});

		$(document).on('click', '.month-navs',function(e) {

			e.preventDefault();
			//alert("click");
			var year = $(this).attr('data-year');
			var month = $(this).attr('data-month');


			$.ajax({
				url: '<?=base_url('ar/calendar/ajax_calendar/')?>',
				data: {'year': year, 'month': month},
				type: 'POST',

				success: function(response) {
					console.log(response);
					$('#calendar-box').html(response);
						  		
				},
				error: function(){
					toastr.error('Error !');
				}
					  	
			});
		});

		$('body').tooltip({
			selector: '[data-toggle="tooltip"]'
		});
	
	</script>
	
</body>
</html>