<!DOCTYPE html>
<html lang="en">

<head>

    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php
    $this->load->view('site/arabic/includes/styles');
    ?>

</head>

<body>

	<div id="page">

	<?php $this->load->view('site/arabic/includes/header'); ?>

	<main>
		

		<div class="bg_color_1">
			<div class="container margin_60_35">
				<h3 class="no-sidebar-page-title"><?=$exam->exam_name_arabic?></h3>
				<div class="row">
					<div class="col-lg-12">
						<?php $i = 1; foreach ($questions as $question) { ?>

							<div class="question">
								<h3>Question <?=$i?></h3>
								<h5>
								    <?php
								        if($question->formula_question_arabic == ''){
								            echo $question->formula_question;
								        }
								        else
								        {
								            echo $question->formula_question_arabic;
								        }
								    ?>
								    
								</h5>

									<div class="question-options">
										<ul>
											<li>
											    <input type="radio" <?php if($question->ans == 'a'){ ?>checked<?php } ?>>
											    <?php
            								        if($question->formula_a_arabic == '' || $question->formula_a_arabic == NULL){
            								            echo $question->formula_a;
            								        }
            								        else
            								        {
            								            echo $question->formula_a_arabic;
            								        }
            								    ?>
											</li>
											<li>
											    <input type="radio" <?php if($question->ans == 'b'){ ?>checked<?php } ?>> 
											    <?php
            								        if($question->formula_b_arabic == '' || $question->formula_b_arabic == NULL){
            								            echo $question->formula_b;
            								        }
            								        else
            								        {
            								            echo $question->formula_b_arabic;
            								        }
            								    ?>
											</li>
											<?php if ($question->formula_c != '') { ?>
												<li>
												    <input type="radio" <?php if($question->ans == 'c'){ ?>checked<?php } ?>>
												    <?php
                								        if($question->formula_c_arabic == '' || $question->formula_c_arabic == NULL){
                								            echo $question->formula_c;
                								        }
                								        else
                								        {
                								            echo $question->formula_c_arabic;
                								        }
                								    ?>
												</li>
											<?php } ?>
											<?php if ($question->formula_d != '') { ?>
												<li>
												    <input type="radio" <?php if($question->ans == 'd'){ ?>checked<?php } ?>>
												    <?php
                								        if($question->formula_d_arabic == '' || $question->formula_d_arabic == NULL){
                								            echo $question->formula_d;
                								        }
                								        else
                								        {
                								            echo $question->formula_d_arabic;
                								        }
                								    ?>
												</li>
											<?php } ?>
											<?php if ($question->formula_e_arabic != '') { ?>
												<li>
												    <input type="radio" <?php if($question->ans == 'e'){ ?>checked<?php } ?>> 
												    <?php
                								        if($question->formula_e_arabic == '' || $question->formula_e_arabic == NULL){
                								            echo $question->formula_e;
                								        }
                								        else
                								        {
                								            echo $question->formula_e_arabic;
                								        }
                								    ?>
												</li>
											<?php } ?>
										</ul>
										<?php if ($question->stu_ans != '') { ?>
											<p>Your answer : Option <?=$question->stu_ans?></p>
										<?php } ?>
									</div>
							</div>
						<?php $i++; } ?>
					</div>

				</div>
			</div>
		</div>

	</main>
	<!--/main-->

	<?php $this->load->view('site/arabic/includes/footer'); ?>
	</div>
	<!-- page -->

	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/arabic/includes/scripts'); ?>
</body>
</html>
