<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/arabic/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page">
		
	<?php $this->load->view('site/arabic/includes/header'); ?>
	
	<main>
		
		<section id="hero_in" class="general banner-calendar">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>التقويم</h1>
				</div>
			</div>
		</section>
			<!--/hero_in-->
		


		<div class="bg_color_2 ">
			<div class="container margin_60_35">
							
				<div class="container calendar year-calendar">						
					<div class="box_general" id="calendar-box" style="clear: both;">
						<ul class="year-calendar-navigators">
										
								
								<li><a class="btn_1 cal-navs next" data-year= "<?=$current_calendar['year'] - 1 ?>" href="#"><i class="ti-angle-right" ></i></a></li>
								<li><a class="btn_1 cal-navs prev" data-year= "<?=$current_calendar['year'] + 1 ?>" href="#"><i class="ti-angle-left" ></i></a></li>
							</ul>	
							<div class="row pt-3" style="clear: both;">
								<?php for ($i=1; $i <=12 ; $i++) { ?>
									<div class="col-lg-4 col-sm-6">
										<div class="table-responsive">	
															

													<?=$calendar[$i]?>
										</div>
									</div>
								<?php } ?>
								
							</div>
						
					</div>	
				</div>
			</div>
		</div>

	</main>
	<!--/main-->
	
	<?php $this->load->view('site/arabic/includes/footer'); ?>
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/arabic/includes/scripts'); ?>
	<script type="text/javascript">


		$(document).on('click', '.cal-navs',function(e) {

			e.preventDefault();
			//alert("click");
			var year = $(this).attr('data-year');


			$.ajax({
				url: '<?=base_url('ar/calendar/ajax_calendar/')?>',
				data: {'year': year},
				type: 'POST',

				success: function(response) {
					console.log(response);
					$('#calendar-box').html(response);
						  		
				},
				error: function(){
					toastr.error('Error !');
				}
					  	
			});
		});

		

		$('body').tooltip({
			selector: '[data-toggle="tooltip"]'
		});
	
	</script>
	
</body>
</html>