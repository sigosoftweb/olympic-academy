﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/arabic/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page">
		
	<?php $this->load->view('site/arabic/includes/header'); ?>
	
	<main>
		<section id="hero_in" class="courses">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Subscribed Courses</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		

		<div class="container margin_60_35">
			<?php if(!empty($courses)) {
				foreach($courses as $course) { ?>
				<div class="box_list wow">				
					<div class="row no-gutters">
						<div class="col-lg-5">
							<figure class="block-reveal">
								<div class="block-horizzontal"></div>
								<a href="<?=base_url('ar/subscriptions/details/'. $course->course_id)?>">
									<?php if($course->course_banner) { ?>
										<img src="<?=base_url($course->course_banner)?>" class="img-fluid" alt="">
									<?php } else { ?>
										<img src="<?=base_url('site-assets/img/no-image.jpg')?>" class="img-fluid" alt="">
									<?php }?>
								</a>
								<?php /*<div class="preview"><span>Preview course</span></div> */?>
							</figure>
						</div>
						<div class="col-lg-7">
							<div class="wrapper">
								
								<div class="price"><?=$course->course_sale_price?> BHD</div>
								<small><?=$course->cat_name?></small>
								<h3><?=$course->course_title?></h3>
								<p><?=getExcerpt($course->course_description, 0, 200)?></p>
								<?php /*<div class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></div> */?>
							</div>
							<ul>
								<li><i class="icon_clock_alt"></i> <?=$course->course_duration?></li>
								
								<li><a href="<?=base_url('ar/subscriptions/details/'. $course->course_id)?>">View Course</a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /box_list -->
				<?php } 

				if(isset($related_courses) && !empty($related_courses)) { ?>
					<?php //print_r($related_courses); ?>

					<div class="container-fluid margin_60_35">
						<div class="main_title_2">
							<span><em></em></span>
							<h2>Related Courses</h2>
							<p></p>
						</div>
						<div id="related-courses" class="owl-carousel owl-theme">
							<?php 
							foreach($related_courses as $related_course) { ?>
							<div class="item">
								<div class="box_grid">
									<figure>
										
										<a href="<?=base_url('ar/courses/details/'. $related_course->course_id)?>">
											
											<?php if($related_course->course_banner) { ?>
												<img src="<?=base_url($related_course->course_banner)?>" class="img-fluid" alt="">
											<?php } else { ?>
												<img src="<?=base_url('site-assets/img/no-image.jpg')?>" class="img-fluid" alt="">
											<?php }?>
											
										</a>
										<div class="price"><?=$related_course->course_sale_price?> BHD</div>

									</figure>
									<div class="wrapper">
										<small><?=$related_course->cat_name?></small>
										<h3><?=$related_course->course_title?></h3>
										<p><?=getExcerpt($related_course->course_description)?></p>
										
									</div>
									<ul>
										<li><i class="icon_clock_alt"></i><?=$related_course->course_duration?></li>
										<li><a href="<?=base_url('ar/courses/details/'. $related_course->course_id)?>">Enroll now</a></li>
									</ul>
								</div>
							</div>
							<!-- /item -->
							<?php } ?>


						</div>
						<!-- /carousel -->
						<div class="container">
							<p class="btn_home_align"><a href="<?=base_url('ar/courses')?>" class="btn_1 rounded">View all courses</a></p>
						</div>
						<!-- /container -->
						
					</div>
					<!-- /container -->
				<?php } 
			} else { ?>
				<div class="text-center margin_60_35">
					<figure class=""><img src="<?=base_url()?>site-assets/img/wizard_intro_icon.svg" alt=""></figure>
					<h3>No courses subscribed!</h3>
				</div>
			<?php } ?>
			
			<?php /*<p class="text-center add_top_60"><a href="#0" class="btn_1">Load more</a></p> */?>
		</div>
		<!-- /container -->
		
	</main>
	<!--/main-->
	
	<?php $this->load->view('site/arabic/includes/footer'); ?>
	</div>
	<!-- page -->

	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/arabic/includes/scripts'); ?>
  
</body>
</html>