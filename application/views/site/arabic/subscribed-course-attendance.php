﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/arabic/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/arabic/includes/header'); 
	?>
	
	<main>
		

		<div class="bg_color_1">

			<nav class="subscribed-menu  sticky_horizontal">
				<div class="container">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
					  	<li class="nav-item">
					    	<a class="nav-link" id="description-tab" href="<?=base_url('ar/subscriptions/details/') . $course->course_id ?>">التفاصيل</a>
					  	</li>

					  	<li class="nav-item">
					    	<a class="nav-link" id="classes-tab" href="<?=base_url('ar/subscriptions/classes/') . $course->course_id ?>">الفصول</a>
					  	</li>
					  	
					  	<li class="nav-item">
					    	<a class="nav-link" id="exams-tab" href="<?=base_url('ar/subscriptions/exams/') . $course->course_id ?>">الامتحانات</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link active" id="attendance-tab" href="<?=base_url('ar/subscriptions/attendance/') . $course->course_id ?>">الحضور</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="trainers-tab" href="<?=base_url('ar/subscriptions/trainers/') . $course->course_id ?>">المدربون</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('ar/subscriptions/assignments/') . $course->course_id ?>">الواجبات</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('ar/subscriptions/certificates/') . $course->course_id ?>">شهادة</a>
					  	</li>
					</ul>
				</div>
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="tab-content" id="myTabContent">

						  	<div class="tab-pane fade show active" id="attendance" role="tabpanel" aria-labelledby="attendance-tab">
						  		<div class="container margin_60_35">
						  			<div class="intro_title">
										<h2>الحضور</h2>
										<?php //print_r($attends); ?>
									</div>
						  			<div class="row">
						  				<div class="col-12">
						  					<?php if(!empty($attends)) { ?>
						  					<table class="table">
											  <thead class="thead-light">
											    <tr>
											      <th scope="col">#</th>
											      <th scope="col">التاريخ</th>
											      <th scope="col">الحضور</th>
											    </tr>
											  </thead>
											  <tbody>
											  	<?php 
											  	$i = 1;
											  	foreach ($attends as $attend) { ?>
											  		<tr>
												      <th scope="row"><?=$i?></th>
												      <td><?=date('d-m-Y', strtotime($attend->attend_date))?></td>
												      <td>
												      		<?php
												      		if($attend->present) {
												      			echo 'الحاضر';
												      		}
												      		else if($attend->absent) {
												      			echo 'غائب';
												      		}
												      		else if($attend->late) {
												      			echo 'في وقت متأخر';
												      		}
												      		else {
												      			echo '';
												      		}
												      		?>
												      </td>
												    </tr>
											  	<?php $i++; } ?>		    
											    
											  </tbody>
											</table>
											<?php } 
											else {?>
												<div class="text-center margin_60_35">
													<figure><img src="<?=base_url()?>site-assets/img/wizard_intro_icon.svg" alt=""></figure>
													<h3>غير مسجل!</h3>
												</div>
											<?php } ?>
						  				</div>
						  			</div>



						  		</div>
								
								
						  	</div> <!-- end of #attendance -->							


						</div> <!-- end of .tab-content -->
					</div><!-- end of .col-lg-8 -->
					<aside class="col-lg-4 margin_60_35" id="sidebar">
						<div class="box_detail">
							<h3 class="title"><?=$course->course_title_arabic?></h3>
							<p><i class="pe-7s-clock"></i> <?=$course->course_duration_arabic?></p>
							<figure>							
									<?php if ($course->course_banner) { ?>
										<img src="<?=base_url($course->course_banner)?>" alt="" class="img-fluid">
									<?php } else {?>
										<img src="<?=base_url('site-assets/img/no-image.jpg')?>" alt="" class="img-fluid">
									<?php } ?>
									
							</figure>
												
						</div>
					</aside>
				</div>
			</div>
			
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php 
	$this->load->view('site/arabic/includes/footer'); 
	?>
	</div>
	<!-- page -->
	
	<?php 
	$this->load->view('site/arabic/includes/scripts'); 
	?>
	
  
</body>
</html>