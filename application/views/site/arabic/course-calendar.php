﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >
    
    <?php 
    $this->load->view('site/arabic/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/arabic/includes/header'); 
	?>
	
	<main>
		<?php if(!isStudent()) { ?>
			<section id="hero_in" class="courses">
				<div class="wrapper">
					<div class="container">
						<h1 class="fadeInUp"><span></span><?=$course->course_title?></h1>
					</div>
				</div>
			</section>
			<!--/hero_in-->
		<?php } ?>

		<div class="bg_color_1">
			<nav class="subscribed-menu sticky_horizontal">
				<div class="container">
					
					<ul class="nav nav-tabs" id="myTab" role="tablist">
					  	<li class="nav-item">
					    	<a class="nav-link" href="<?=base_url('ar/courses/details/') . $course->course_id ?>">Details</a>
					  	</li>
					  	
					  	<li class="nav-item">
					    	<a class="nav-link" href="<?=base_url('ar/courses/trainers/') . $course->course_id ?>">Trainers</a>
					  	</li>

					  	<li class="nav-item">
					    	<a class="nav-link active" href="<?=base_url('ar/courses/calendar/') . $course->course_id ?>">Calendar</a>
					  	</li>
					  	
					</ul>
				</div>
			</nav>
			<div class="container ">
				<div class="row">
					<div class="col-lg-12">
							
						<div class="container margin_60_35 calendar">

							<h2>Course Calendar - <?=$course->course_title?></h2>
					
							<ul class="calendar-colors">
								<li class="section-green"><i class="icon_easel"></i> Lesson</li>
								<li class="class-yellow"><i class="icon_mic_alt"></i> Online Class</li>
								<li class="exam-blue"><i class="icon_pencil-edit"></i> Exam</li>
							</ul>

							<div class="box_general" style="clear: both;">	
						
								<div class="table-responsive">	
									<ul class="calendar-navigators">
										<li><a class="btn_1 next" href="<?=base_url('ar/courses/calendar/'). $course->course_id . '/' . $current_calendar['year'] . '/' . ($current_calendar['month'] - 1) ?>"><i class="ti-angle-right" ></i></a></li>
								
										<li><a class="btn_1 prev" href="<?=base_url('ar/courses/calendar/'). $course->course_id . '/' . $current_calendar['year'] . '/' . ($current_calendar['month'] + 1) ?>"><i class="ti-angle-left" ></i></a></li>
										
									</ul>			
									<?=$calendar?>
								</div>
							</div>
						</div>
					</div>
					<!-- /col -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
	</main>
	<!--/main-->
	
	<?php $this->load->view('site/arabic/includes/footer.php'); ?>
	<!--/footer-->

</div>
<!-- page -->


	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/arabic/includes/scripts.php'); ?>    

    
	
</body>
</html>