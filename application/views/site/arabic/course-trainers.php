﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >
    
    <?php 
    $this->load->view('site/arabic/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/arabic/includes/header'); 
	?>
	
	<main>
		<?php if(!isStudent()) { ?>
			<section id="hero_in" class="courses">
				<div class="wrapper">
					<div class="container">
						<h1 class="fadeInUp"><span></span><?=$course->course_title?></h1>
					</div>
				</div>
			</section>
			<!--/hero_in-->
		<?php } ?>

		<div class="bg_color_1">
			<nav class="subscribed-menu sticky_horizontal">
				<div class="container">
					
					<ul class="nav nav-tabs" id="myTab" role="tablist">
					  	<li class="nav-item">
					    	<a class="nav-link" href="<?=base_url('ar/courses/details/') . $course->course_id ?>">Details</a>
					  	</li>
					  	
					  	<li class="nav-item">
					    	<a class="nav-link active" href="<?=base_url('ar/courses/trainers/') . $course->course_id ?>">Trainers</a>
					  	</li>
					  	
					  	<li class="nav-item">
					    	<a class="nav-link" href="<?=base_url('ar/courses/calendar/') . $course->course_id ?>">Calendar</a>
					  	</li>
					</ul>
				</div>
			</nav>
			<div class="container ">
				<div class="row">
					<div class="col-lg-8">
							
						<div class="container margin_60_35">
							<div class="intro_title">
								<h2>Trainers</h2>
							</div>
							<?php if(!empty($trainers)) { ?>
						  	<div class="row trainers mt-4">
						  				
						  		<?php foreach ($trainers as $trainer) { ?>
						  					
						  			<div class="col-lg-4 col-md-6 col-sm-6">
										<div class="box_grid text-center">
											<figure>
												<?php if($trainer->trainer_image) { ?>
													<img src="<?=base_url($trainer->trainer_image)?>" class="img-fluid" alt="<?=$trainer->trainer_name_english?>">
														<?php }
												else { ?>
													<img src="<?=base_url('uploads/trainers/user.png')?>" class="img-fluid" alt="<?=$trainer->trainer_name_english?>">
														<?php } ?>
											</figure>
											<div class="trainer-details">
												<h3><?=$trainer->trainer_name_english . ' ' . $trainer->trainer_name_arabic?></h3>
												<h5><?=$trainer->trainer_education?></h5>
												<p><?=$trainer->trainer_experience?></p>
											</div>
										</div>
									</div>
								<?php } ?>

							</div>		
							<?php } 
							else {?>
								<div class="text-center margin_60_35">
									<figure><img src="<?=base_url()?>site-assets/img/wizard_intro_icon.svg" alt=""></figure>
									<h3>Trainers not scheduled!</h3>
								</div>
							<?php } ?>

							<?php if(!empty($organiser)) { ?>
							  		<div class="container margin_60_35">
							  			<div class="intro_title">
											<h2>Organizers</h2>
										</div>
										<div class="row trainers mt-4">
											<div class="col-lg-4 col-md-6 col-sm-6">
												<div class="box_grid text-center">
													<figure>
														<?php if($organiser->organiser_image) { ?>
															<img src="<?=base_url($organiser->organiser_image)?>" class="img-fluid" alt="<?=$organiser->organiser_first?>">
														<?php }
														else { ?>
															<img src="<?=base_url('site-assets/img/user.png')?>" class="img-fluid" alt="<?=$organiser->organiser_second?>">
														<?php } ?>
													</figure>
													<div class="trainer-details">
														<h3><?=$organiser->organiser_first . ' '. $organiser->organiser_second?></h3>
														
													</div>
												</div>
											</div>
										</div>
									</div>
							<?php } ?>
						</div>					
						
					</div>
					<!-- /col -->
					
					<aside class="col-lg-4" id="sidebar">
						<div class="box_detail">
							
							<h3 class="title"><?=$course->course_title?></h3>
							<p><i class="icon_clock_alt"></i> <?=$course->course_duration?></p>
							
							<figure>								
									<?php if ($course->course_banner) { ?>
										<img src="<?=base_url($course->course_banner)?>" alt="" class="img-fluid">
									<?php } else {?>
										<img src="<?=base_url('site-assets/img/no-image.jpg')?>" alt="" class="img-fluid">
									<?php } ?>
									
							</figure>
							<div class="price">
								<?=$course->course_sale_price?> BHD							
							</div>
							<?php if(isStudent()) { 
								if(is_in_array($courseIDs, 'course_id', $course->course_id)) { ?>
									<a href="<?=base_url('ar/subscriptions/details/'. $course->course_id)?>">Subscribed!</a>
								<?php } else { 
									if($course->sequence_id == 0) { ?>
										<a href="<?=base_url('ar/student/subscribe/'. $course->course_id)?>" class="btn_1 full-width">Subscribe</a>
									<?php } else {
										if(is_in_array($courseIDs, 'course_id', $course->sequence_id)) { ?>
											<a href="<?=base_url('ar/student/subscribe/'. $course->course_id)?>" class="btn_1 full-width">Subscribe</a>
										<?php }	else {?>	
											<p>You can subscribe this course only after subscribing the previous course<br>
												<a href="<?=base_url('ar/courses/details/'. $course->sequence_id)?>">View Previous  Course</a></p>
										<?php } 
									}
								}	
							} else { ?>
								<a href="<?=base_url('ar/student/subscribe/'. $course->course_id)?>" class="btn_1 full-width">Subscribe</a>
							<?php } ?>
							
							
						</div>
					</aside>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php 
	$this->load->view('site/arabic/includes/footer'); 
	?>
	</div>
	<!-- page -->
	
	<?php 
	$this->load->view('site/arabic/includes/scripts'); 
	?>
	
  
</body>
</html>