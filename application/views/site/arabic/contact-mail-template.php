<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Contact Us</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body style="color: #666; font-size: 14px; line-height: 1.5;">
    <div style="max-width: 650px; background-color: #f9f9f9; margin: 0 auto;" >
        <img style="margin:0px auto; padding: 10px 0; display: block; width:75px;" src="<?=base_url('site-assets/img/olympic.PNG') ?>">
        <div style="padding: 10px 20px 20px 20px;">
            
            <img style="width:100%;" src="<?=base_url('site-assets/img/mail-banner.jpg') ?>" alt="banner"/>
            <div style="padding: 0px 15px;"> 
                <h4 style="border-bottom: 1px solid #999; padding-bottom: 5px; font-size: 15px;">Message</h4>
                <p><?=$message ?></p>
                
                <p style="padding-top: 5px;"><strong>From</strong></p>
                <p style="padding-top: 0px;"><?=$firstname . " " . $lastname ?><br>
                Mob: <?=$mobile ?><br/>
                Email: <?=$email ?></p>  
            </div>       

        </div>
    </div>
</body>
</html>