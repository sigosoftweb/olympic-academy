﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/arabic/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/arabic/includes/header'); 
	?>
	
	<main>
		

		<div class="bg_color_1">

			<nav class="subscribed-menu sticky_horizontal">
				<div class="container">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
					  	<li class="nav-item">
					    	<a class="nav-link active" id="description-tab" href="<?=base_url('ar/subscriptions/details/') . $course->course_id ?>">التفاصيل</a>
					  	</li>

					  	<?php /*<li class="nav-item">
					    	<a class="nav-link" id="sections-tab" href="<?=base_url('ar/subscriptions/sections/') . $course->course_id ?>">Lessons</a>
					  	</li> */?>

					  	<li class="nav-item">
					    	<a class="nav-link" id="classes-tab" href="<?=base_url('ar/subscriptions/classes/') . $course->course_id ?>">الفصول</a>
					  	</li>
					  	
					  	<li class="nav-item">
					    	<a class="nav-link" id="exams-tab" href="<?=base_url('ar/subscriptions/exams/') . $course->course_id ?>">الامتحانات</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="attendance-tab" href="<?=base_url('ar/subscriptions/attendance/') . $course->course_id ?>">الحضور</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="trainers-tab" href="<?=base_url('ar/subscriptions/trainers/') . $course->course_id ?>">المدربون</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('ar/subscriptions/assignments/') . $course->course_id ?>">الواجبات</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('ar/subscriptions/certificates/') . $course->course_id ?>">شهادة</a>
					  	</li>
					</ul>
				</div>
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="tab-content" id="myTabContent">

						  	<div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
						  		<div class="container margin_60_35">
						  			<div class="intro_title">
							  			<h2>وصف موجز</h2>	
							  		</div>						
									<div class="row">
										<div class="col-lg-12">
											<?=$course->course_description_arabic?>
										</div>
									</div>
									<!-- /row -->
								</div>

								<?php if($sections_count->scount) { ?>
								<div class="container margin_60_35">
							  		<div class="intro_title">
										<h2>التفاصيل</h2>
										<ul>
											<li><?=$sections_count->scount?> الدروس</li>
											<?php /*<li>01:02:10</li> */?>
										</ul>
									</div>
									<div id="accordion_lessons" role="tablist" class="add_bottom_45">
										<?php 
										$first_child = true;
										foreach ($course_sections as $course_section) { ?>
											<div class="card">
												<!-- .card-header starts -->
												<div class="card-header" role="tab" id="heading-<?=$course_section->cs_id?>">
													<h5 class="mb-0">
														<?php if($first_child) { ?>
															<a data-toggle="collapse" href="#collapse-<?=$course_section->cs_id?>" aria-expanded="true" aria-controls="collapse-<?=$course_section->cs_id?>"><i class="indicator  icon-minus"></i>
														<?php } else { ?>
															<a class="collapsed" data-toggle="collapse" href="#collapse-<?=$course_section->cs_id?>" aria-expanded="false" aria-controls="collapse-<?=$course_section->cs_id?>">
															<i class="indicator icon-plus"></i>

														<?php } ?>
														<?=$course_section->section_title_arabic?></a>
													</h5>
												</div>
												<!-- .card-header ends -->

												<?php if($first_child) { ?>
												<div id="collapse-<?=$course_section->cs_id?>" class="collapse show" role="tabpanel" aria-labelledby="heading-<?=$course_section->cs_id?>" data-parent="#accordion_lessons">
												<?php } else { ?>
												<div id="collapse-<?=$course_section->cs_id?>" class="collapse" role="tabpanel" aria-labelledby="heading-<?=$course_section->cs_id?>" data-parent="#accordion_lessons">
												<?php } ?>	
													<div class="card-body">
														<div>
															<?=$course_section->section_description_arabic?>
														</div>
														
														<div class="list_lessons">
															<ul>
																<?php foreach ($course_section->attachments as $attachment) { ?>

																	<?php if($attachment->csa_attachment) { ?>
																		<li><a href="<?=base_url($attachment->csa_attachment)?>" target="_blank">
																			<?php
																			switch ($attachment->csa_file_type) {
																				case 'Documents':
																					echo "<i class='icon-doc-text-inv'></i>";
																					break;
																				case 'Video':
																					echo "<i class='icon-video'></i>";
																					break;
																				case 'Image':
																					echo "<i class='icon-picture'></i>";
																					break;
																				default:
																					echo "<i class='icon-attach'></i>";
																					break;
																			}

																			?>
																		المرفقات</a></li>
																	<?php } ?>

																	<?php if($attachment->csa_youtube) { ?>
																		<li><a href="<?=$attachment->csa_youtube?>" target="_blank"><i class="icon-youtube"></i> Video</a></li>
																	<?php } ?>

																	<?php if($attachment->csa_other) { ?>
																		<li><a href="<?=$attachment->csa_other?>" target="_blank"><i class="icon-attach"></i> Other</a></li>
																	<?php } ?>
																<?php } ?>		
															</ul>
														</div> <!-- end of .list_lessons -->
														
													</div>
												</div>
											</div>
											<!-- /card -->
											<?php 
											$first_child = false;
										} ?>
											
									</div>
										<!-- /accordion --> 
								</div>
								<?php } ?>
						  	</div> <!-- end of #description -->			  	


						</div> <!-- end of .tab-content -->
					</div><!-- end of .col-lg-8 -->
					<aside class="col-lg-4 margin_60_35" id="sidebar">
						<div class="box_detail">
							<h3 class="title"><?=$course->course_title_arabic?></h3>
							<p><i class="pe-7s-clock"></i> <?=$course->course_duration_arabic?></p>
							<figure>							
									<?php if ($course->course_banner) { ?>
										<img src="<?=base_url($course->course_banner)?>" alt="" class="img-fluid">
									<?php } else {?>
										<img src="<?=base_url('site-assets/img/no-image.jpg')?>" alt="" class="img-fluid">
									<?php } ?>
									
							</figure>
													
						</div>
					</aside>
				</div>
			</div>
			
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php 
	$this->load->view('site/arabic/includes/footer'); 
	?>
	</div>
	<!-- page -->
	
	<?php 
	$this->load->view('site/arabic/includes/scripts'); 
	?>
	
  
</body>
</html>