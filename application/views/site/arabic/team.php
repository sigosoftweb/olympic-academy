﻿

<!DOCTYPE html>
<html lang="en">

<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >
    
    <?php 
    $this->load->view('site/arabic/includes/styles');
    ?>  

</head>
<body>
	
	<div id="page">
		
	<?php $this->load->view('site/arabic/includes/header'); ?>
	
	<main>
		<section id="hero_in" class="general">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>فريقنا</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->					
		
		<div class="bg_color_3">
			<div class="container margin_120_95">
				<div class="row trainers">
					
						<?php foreach ($team as $tm) { ?>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="box_grid text-center wow">
								<figure>
									
									
									<?php if($tm->team_image) { ?>
										
										<div class="box-image" style="background-image: url(<?=base_url($tm->team_image)?>);"></div>
									<?php }
									else { ?>
										<div class="box-image" style="background-image: url(<?=base_url('site-assets/img/user.png')?>);"></div>
									<?php } ?>
									
								</figure>
								<div class="trainer-details">
									
									<h3><?=$tm->first_arabic . ' '. $tm->second_arabic?></h3>
									
									<h5><?=$tm->designation_arabic?></h5>
								</div>
							</div>
						</div>
						<?php } ?>
					
					
				</div>
			</div>

		</div>

		
	</main>
	<!--/main-->
	
	<?php $this->load->view('site/arabic/includes/footer'); ?>
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/arabic/includes/scripts'); ?>
	
</body>
</html>