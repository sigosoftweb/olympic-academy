<!DOCTYPE html>

<html>

<head>

    <meta charset="utf-8" />

    <title>Olympic Academy - Reset Password</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

</head>

<body style="font-family: sans-serif; color: #666; font-size: 14px; line-height: 1.5;">

    <div style="max-width: 650px; background-color: #f9f9f9; margin: 0 auto;" >

        

        <div style="padding: 10px 20px 20px 20px;">

            

            

            <div style="padding: 0px 15px;"> 

                <h4 style="border-bottom: 1px solid #999; padding-bottom: 5px; font-size: 17px;">Don't worry, we all forget sometimes</h4>

                <p>Hi <?=$name ?>,</p>

                <p>You've recently asked to reset the password for your <strong>Olympic Academy</strong> account</p>

                <p>To update your password, click the button below:</p>

                <p style="padding-top: 10px;">

                    <a href="<?=$reset_link ?>" target="_blank" style="background-color: #2196F3; color: #fff; padding: 10px 20px; text-decoration: none; border-radius: 5px;">Reset Password</a>

                </p>

                <p style="padding-top: 10px;">If you don't use this link within 30 minutes, it will expire. To get a new password reset link, visit <a href="<?=site_url('login/forgotpassword') ?>" target="_blank"><?=site_url('login/forgotpassword') ?></a></p>

                <p style="padding-top: 20px;">Thanks,<br/>

                Olympic Academy Team</p>  

            </div>       



        </div>

    </div>

</body>

</html>