<?php
	
	if (isset($this->session->userdata['olympicUser'])) {
	    $user = $this->session->userdata['olympicUser'];
	    if(isset($user)) {
	    	$userid = $user['userID'];
	    	$username = $user['userName'];
	    	//echo $username;
	    	//exit;
	    }
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/arabic/includes/styles');
    ?>   
    <link rel="stylesheet" href="<?=base_url()?>plugins/image-crop/croppie.css">   

</head>

<body>
	
	
	<div id="page" class="theia-exception">
		
	<?php $this->load->view('site/arabic/includes/header'); ?>
	
	<main>

		
		<div class="container-fluid margin_60_35 student-profile">
			<div class="row">
				<aside class="col-lg-3" id="sidebar">

					<div class="profile">
						<figure>
							<div>
								<?php if($student->stu_image) { ?>
									<img src="<?=base_url($student->stu_image)?>" alt="<?=$student->stu_name_english?>" class="rounded-circle img-fluid">
								<?php } else { ?>
									<img src="<?=base_url('site-assets/img/user.jpg')?>" alt="<?=$student->stu_name_english?>" class="rounded-circle img-fluid">
								<?php } ?>
								
							</div>
						</figure>
						<p><?=$student->first_arabic?></p>
						<ul class="nav flex-column flex-nowrap overflow-hidden">
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('ar/student/profile')?>">ملفي الشخصي</a>    </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('ar/subscriptions')?>">اشتراكاتي</a>
			                </li>
			                <li class="nav-item active">
			                    <a class="nav-link" href="<?=base_url('ar/subscriptions/pending')?>">الاشتراكات المعلقة</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('ar/subscriptions/rejected')?>">الاشتراكات المرفوضة</a>
			                </li>
			                <?php if(!empty($subscribed_courses)) { ?>
			                <li class="nav-item">
			                    <a class="nav-link collapsed" href="#submenu1" data-toggle="collapse" data-target="#submenu1">تقويم الدورة<span class="float-right"><i class="icon-down-dir"></i></span></a>
			                    <div class="collapse" id="submenu1" aria-expanded="false">
			                        <ul class="flex-column nav">
			                        	<?php foreach ($subscribed_courses as $subscribed_course) { ?>                   		
			                            <li class="nav-item"><a class="nav-link py-0" href="<?=base_url('ar/subscriptions/calendar/'). $subscribed_course->cid ?>"><?=$subscribed_course->course_title_arabic ?></a></li>
			                            <?php } ?>
			                        </ul>
			                    </div>
			                </li>
			            	<?php } ?>
			                <li class="nav-item"><a class="nav-link" href="<?=base_url('ar/student/payments')?>">مدفوعاتي</a></li>
			                <li class="nav-item">
			                	<a class="nav-link" href="<?=base_url('ar/student/notice')?>">إشعار 
			                		<?php if($notice_count->ncount) { ?>
				                		<span class="notice-count"><?=$notice_count->ncount?></span>
				                	<?php } ?>
			                	</a></li>
			                <li class="nav-item"><a class="nav-link" href="<?=base_url('ar/student/password/change')?>">تغيير كلمة المرور</a></li>
			            </ul>
					</div>
				</aside>
				<!--/aside -->

				<div class="col-lg-9">
					<h3 class="sidebar-page-title">الاشتراكات المعلقة</h3>
					<?php if(!empty($courses)) {
						foreach($courses as $course) { ?>
						<div class="box_list wow">				
							<div class="row no-gutters">
								<div class="col-lg-4">
									<figure class="block-reveal">
										<div class="block-horizzontal"></div>
										<a href="<?=base_url('ar/courses/details/'. $course->course_id)?>">
											<?php if($course->course_banner) { ?>
												<img src="<?=base_url($course->course_banner)?>" class="img-fluid" alt="">
											<?php } else { ?>
												<img src="<?=base_url('site-assets/img/no-image.jpg')?>" class="img-fluid" alt="">
											<?php }?>
										</a>
										<?php /*<div class="preview"><span>Preview course</span></div> */?>
									</figure>
								</div>
								<div class="col-lg-8">
									<div class="wrapper">
										
										<div class="price"><?=$course->course_sale_price?> BHD</div>
										<small><?=$course->cat_name_arabic?></small>
										<h3><?=$course->course_title_arabic?></h3>
										<p><?=getExcerpt($course->course_description_arabic, 0, 300)?></p>
										<?php /*<div class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></div> */?>
									</div>
									<ul>
										<li style="font-style:italic;  color: #f00;"><span>شتراكك قيد المراجعة. يمكنك الحصول على تفاصيل الدورة بمجرد موافقة المسؤول على طلبك.</span></li>
									</ul>
									
								</div>
							</div>
						</div>
						<!-- /box_list -->
						<?php } 

						
					} else { ?>
						<div class="text-center margin_60_35">
							<figure><img src="<?=base_url()?>site-assets/img/wizard_intro_icon.svg" alt=""></figure>
							<h3>Nothing!</h3>
						</div>
					<?php } ?>

				</div>
				<!-- /col -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->

	</main>
	<!--/main-->
	
	<?php $this->load->view('site/arabic/includes/footer'); ?>
	<!--/footer-->
	
	
	

</div>
<!-- page -->


	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/arabic/includes/scripts'); ?>
    

    
	
</body>
</html>