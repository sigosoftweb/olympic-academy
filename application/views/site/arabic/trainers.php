<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/arabic/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page">
		
	<?php $this->load->view('site/arabic/includes/header'); ?>
	
	<main>
	
		<section id="hero_in" class="general">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>محاضرينا</h1>
				</div>
			</div>
		</section>
			<!--/hero_in-->
	



		<div class="bg_color_2 trainers">
			<div class="container margin_60_35">
								
				<div class="row">
					
						<?php foreach ($trainers as $trainer) { ?>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="box_grid text-center wow">
								<figure>
									<div class="block-horizzontal"></div>
									<a href="<?=base_url('ar/trainers/details/' . $trainer->trainer_id) ?>">
									<?php if($trainer->trainer_image) { ?>
										
										<div class="box-image" style="background-image: url(<?=base_url($trainer->trainer_image)?>);"></div>
									<?php }
									else { ?>
										<div class="box-image" style="background-image: url(<?=base_url('site-assets/img/user.png')?>);"></div>
									<?php } ?>
									</a>
								</figure>
								<div class="trainer-details">
									<a href="<?=base_url('ar/trainers/details/' . $trainer->trainer_id) ?>">
										<h3><?=$trainer->arabic_first . ' '. $trainer->arabic_second?></h3>
									</a>
									<h5><?=$trainer->education_arabic?></h5>
									<p><?=$trainer->experience_arabic?></p>
								</div>
							</div>
						</div>
						<?php } ?>
					
					
				</div>
			</div>
		</div>

	</main>
	<!--/main-->
	
	<?php $this->load->view('site/arabic/includes/footer'); ?>
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/arabic/includes/scripts'); ?>
	
	
</body>
</html>