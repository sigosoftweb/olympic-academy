<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/arabic/includes/styles');    
    ?>  
    <!-- SPECIFIC CSS -->
    <link href="<?=base_url('site-assets/css/blog.css')?>" rel="stylesheet">
</head>

<body>
	
	<div id="page">
		
	<?php $this->load->view('site/arabic/includes/header'); ?>
	
	<main>
	
		<section id="hero_in" class="general banner-resources">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span> الموارد</h1>
				</div>
			</div>
		</section>
			<!--/hero_in-->
	



		<div class="bg_color_2 resources">
			<div class="container margin_60_35">
				
				<div class="row">				
						
					<div class="col-lg-8 ">
						<div class="course-resources">
							<?php if(!empty($resources)) { ?>
								<ul>
									<?php foreach ($resources as $resource) { ?>
										<li>
											<?php switch ($resource->cr_type) {
												case 'Document':
													echo "<i class='icon-doc-text-inv'></i>";
													break;
												case 'Video':
													echo "<i class='icon-video'></i>";
													break;
												case 'Image':
													echo "<i class='icon-picture'></i>";
													break;
												default:
													echo "<i class='icon-attach'></i>";
													break;
											} ?>
											<a href="<?=base_url($resource->cr_attachment) ?>" target="_blank">
												<span><?=$resource->cr_title_arabic ?></span>
											</a>
										</li>
									<?php } ?>
								
								</ul>
							<?php } else { ?>
								<div class="text-center margin_120_95 ">
									<figure><img src="<?=base_url()?>site-assets/img/wizard_intro_icon.svg" alt=""></figure>
									<h3 class="pb-5">No resources!</h3>
								</div>
							<?php } ?>
							
							
						</div>
					</div>

					<aside class="col-lg-4 pl-20">
						<?php if($courses) { ?>
							<div class="widget">
								<div class="widget-title">
									<h4>دوراتنا</h4>
								</div>
								<ul class="comments-list">
									<?php foreach ($courses as $course) { ?>				
										<li>
											<div class="alignleft">
												<a href="<?=base_url('ar/courses/details/').$course->course_id?>"><img src="<?=base_url($course->course_banner)?>" alt="Image"></a>
											</div>
											<small><?=$course->cat_name_arabic ?></small>
											<h3><a href="<?=base_url('ar/courses/details/').$course->course_id?>" title=""><?=$course->course_title_arabic?></a></h3>
										</li>
									<?php } ?>
								</ul>
							</div>
							<!-- /widget -->
							<?php } ?>
					</aside>
					
				</div>
			</div>
		</div>

	</main>
	<!--/main-->
	
	<?php $this->load->view('site/arabic/includes/footer'); ?>
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/arabic/includes/scripts'); ?>
	
	
</body>
</html>