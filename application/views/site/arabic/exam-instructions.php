<!DOCTYPE html>
<html lang="en">

<head>

    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php
    $this->load->view('site/arabic/includes/styles');
    ?>

</head>

<body>

	<div id="page">

	<?php $this->load->view('site/arabic/includes/header'); ?>

	<main>
		
		<?php
			$time = false;
			$start = false;
			$end = false;
			$ended = false;
			$now = date('Y-m-d H:i:s');
			if (strtotime($now) < strtotime($exam->from_time)) {
				$start = true;
			}
			else {
				if (strtotime($now) < strtotime($exam->to_time)) {
					$end = true;
				}
				else {
					$ended = true;
				}
			}
		?>
		<div class="bg_color_1">
			<div class="container margin_60_35">
				<h3 class="no-sidebar-page-title">تعليمات الامتحان</h3>
				<div class="row">
					<div class="col-lg-<?php if ($exam->time_status == '1') { ?>9<?php }else { ?>12<?php } ?>">
						<div class="instructions">
							<h4 class="mb-3">يرجى قراءة التعليمات أدناه قبل بدء الاختبار</h4>
							<ul>
								<?php foreach ($instructions as $instruction) { ?>
									<li><i class="icon-check-1"></i> <?=$instruction->instruction_arabic?></li>
								<?php } ?>
							</ul>
							<?php if($exam->time_status == '0'){ ?>
							    <div id="start-button">
    									<a href="<?=base_url('ar/exam/start/' . $exam_id . '/' . $course_id)?>" class="btn_1 rounded">بدء الامتحان</a>
    								</div>
							<?php }else{ ?>
    							<?php if ($start || $end) { ?>
    								<div id="start-button">
    									<a href="<?=base_url('ar/exam/start/' . $exam_id . '/' . $course_id)?>" class="btn_1 rounded">بدء الامتحان</a>
    								</div>
    							<?php } ?>
    						<?php } ?>
						</div>
					</div>
					<?php if ($exam->time_status == '1') { ?>
						<div class="col-lg-3">
							<div class="exam-sidebar">
								<p id="start-time" style="display: none;"><?=$exam->from_time?></p>
								<p id="end-time" style="display: none;"><?=$exam->to_time?></p>
								<div class="time-left">
									<?php if ($start) { ?>
										<h4>Exam will start in</h4>
										<h5 class="exam-timer">
											<span class="days-left-timer"></span><br>
											<span class="time-left-timer"></span>
										</h5>
									<?php } ?>
									<?php if ($end) { ?>
										<h4>Exam will end in</h4>
										<h5 class="exam-timer">
											<span class="days-left-timer"></span><br>
											<span class="time-left-timer"></span>
										</h5>
									<?php } ?>
									<?php if ($ended) { ?>
										<h4>Exam ended</h4>
									<?php } ?>
								</div>

							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>

	</main>
	<!--/main-->

	<?php $this->load->view('site/arabic/includes/footer'); ?>
	</div>
	<!-- page -->

	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/arabic/includes/scripts'); ?>

	<script>
		<?php if ($start) { ?>
			$(document).ready(function() {

				var countDownDate = new Date($('#start-time').html()).getTime();
				var x = setInterval(function() {

					var now = new Date().getTime();

					var distance = countDownDate - now;

					var days = Math.floor(distance / (1000 * 60 * 60 * 24));
					var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
					var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);

					if(days == 0) {
						$('.days-left-timer').html(' ');
					}
					else {
						$('.days-left-timer').html(days + 'D');
					}


					$('.time-left-timer').html(('0' + hours).slice(-2) + " : " + ('0' + minutes).slice(-2) + " : " + ('0' + seconds).slice(-2));


					if (distance < 0) {
					    clearInterval(x);
					    $('.exam-timer').html('Exam started');
						location.reload();
				 	}

				}, 1000);
			});
		<?php } ?>
		<?php if ($end) { ?>
			$(document).ready(function() {

				var countDownDate = new Date($('#end-time').html()).getTime();
				var x = setInterval(function() {

					var now = new Date().getTime();

					var distance = countDownDate - now;

					var days = Math.floor(distance / (1000 * 60 * 60 * 24));
					var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
					var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);

					if(days == 0) {
						$('.days-left-timer').html(' ');
					}
					else {
						$('.days-left-timer').html(days + 'D');
					}


					$('.time-left-timer').html(('0' + hours).slice(-2) + " : " + ('0' + minutes).slice(-2) + " : " + ('0' + seconds).slice(-2));


					if (distance < 0) {
					    clearInterval(x);
					    $('.exam-timer').html('');
						location.reload();
				 	}

				}, 1000);
			});
		<?php } ?>
	</script>
</body>
</html>
