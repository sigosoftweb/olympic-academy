<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/english/includes/styles');
    ?>      

</head>

<body id="register_bg">
	
	<nav id="menu" class="fake_menu"></nav>
	
	<div id="preloader">
		<div data-loader="circle-side"></div>
	</div>
	<!-- End Preload -->
	
	<div id="login">
		<aside>
			<figure class="logo">
				<a href="<?=base_url()?>">
					<img src="<?=base_url('site-assets/img/olympic.PNG')?>" width="75"  data-retina="true" alt="">
					
				</a>
			</figure>
			<form action="<?=site_url('en/register/student')?>" method="post" id="register-form" >
				<div class="form-group register">
					<span class="input">
						<?php /*<input type="text" class="input_field" name="stu_name_english" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required  autocomplete="off">*/ ?>
						<input type="text" class="input_field" name="stu_name_english" required  autocomplete="off">
						<label class="input_label">
						<span class="input__label-content">Full Name (English)</span>
					</label>
					</span>

					<span class="input">
					<?php /* <input type="text" class="input_field" name="stu_name_arabic"  autocomplete="off" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' > */ ?>
					<input type="text" class="input_field" name="stu_name_arabic"  autocomplete="off">
						<label class="input_label">
						<span class="input__label-content">Family Name (English)</span>
					</label>
					</span>		
					<span class="input">
						<input type="text" class="input_field" id="first-arabic" name="first_arabic" required  autocomplete="off">
						<label class="input_label">
						<span class="input__label-content">Full Name (Arabic)</span>
					</label>
					</span>

					<span class="input">
					    <input type="text" class="input_field" id="second-arabic" name="second_arabic"  autocomplete="off">
						<label class="input_label">
						    <span class="input__label-content">Family Name (Arabic)</span>
					    </label>
					</span>						
					
					<span class="select">
						<label class="select_label">
							<span class="input__label-content"></span>
						</label>
						<select name="stu_timezone" required class="selectbox">
							<option value="">Select Timezone</option>
							<?php foreach ($timezones as $timezone) { ?>
								<option value="<?= $timezone ?>"><?= $timezone ?></option>
							<?php } ?>
						</select>
										
					</span>

					<span class="input">
					<input type="text" name="stu_mobile" id="mobile" class="input_field"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" required  autocomplete="off">
						<label class="input_label">
						<span class="input__label-content">Mobile</span>
					</label>
					</span>

					<span class="input">
					<input type="email" name="stu_email" id="email" class="input_field" required  autocomplete="off">
						<label class="input_label">
						<span class="input__label-content">Email</span>
					</label>
					</span> 
					
					<span class="input">
					<input type="text" name="stu_cpr" id="cpr" class="input_field" required  autocomplete="off">
						<label class="input_label">
						<span class="input__label-content">CPR</span>
					</label>
					</span> 
					
					<?php /* <span class="input">
					    <input type="text" name="stu_dob" id="datepicker" class="input_field" required  autocomplete="off">
						<label class="input_label">
						    <span class="input__label-content">DOB</span>
					    </label>
					</span>  */ ?>

					<span class="input input-date">
					    <input type="date" name="stu_dob" class="input_field" required  autocomplete="off">
						<label class="input_label">
						    <span class="input__label-content">DOB</span>
					    </label>
					</span> 
					
					<span class="select">
						<label class="select_label">
							<span class="input__label-content"></span>
						</label>
						<select name="stu_gender" required class="selectbox">
							<option value="">Select Gender</option>
							<option value="Male">Male</option>
                            <option value="Female">Female</option>
                            <option value="Other">Other</option>
						</select>
										
					</span>
                    
                    <span class="input input-file">
					    <input type="file" name="image" class="input_field" accept="image/*" id="img0" onchange="preview_image(this)" autocomplete="off">
						<label class="input_label">
						    <span class="input__label-content">Photo</span>
					    </label>
					</span> 
                    
                    <span class="input">
					    <input type="text" name="stu_qualification" id="qualification" class="input_field" required  autocomplete="off">
						<label class="input_label">
						    <span class="input__label-content">Qualification</span>
					    </label>
					</span> 
                    
					<span class="input">
					<input type="password" name="stu_password" id="password" class="input_field" required  autocomplete="off">
						<label class="input_label">
						<span class="input__label-content">Your password</span>
					</label>
					</span>

					<span class="input">
					<input class="input_field" type="password" name="confirm_password" id="confirm-password" required  autocomplete="off">
						<label class="input_label">
						<span class="input__label-content">Confirm password</span>
					</label>
					</span>
					
					<div id="pass-info" class="clearfix"></div>
				</div>
				<input type="hidden" name="username_type" id="username-type">
				<button class="btn_1 rounded full-width add_top_30" id="btn-register" type="submit">Register to Olympic Academy</button>
				<p class="dummy-message"></p>
				<div class="text-center add_top_10">Already have an acccount? <strong><a href="<?=base_url('en/login')?>">Sign In</a></strong></div>
			</form>
			<div class="copy">© 2020 Olympic Academy</div>
		</aside>
	</div>
	<!-- /login -->
	
	<?php $this->load->view('site/english/includes/scripts'); ?>
	
	<!-- SPECIFIC SCRIPTS -->
	<?php /*<script src="<?=base_url('js/pw_strenght.js')?>"></script>*/?>

	<script type="text/javascript">
		$('#register-form').on('submit', function(e){	

			e.preventDefault();
			var self = this;

			var first_arabic = $('#first-arabic').val();
			var second_arabic = $('#second-arabic').val();
			
			var mobile = $('#mobile').val();
			var password = $('#password').val();
			
			var confirm_password = $('#confirm-password').val();

			var isArabic = /^([\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufbc1]|[\ufbd3-\ufd3f]|[\ufd50-\ufd8f]|[\ufd92-\ufdc7]|[\ufe70-\ufefc]|[\ufdf0-\ufdfd]|[ ])*$/g;
			var isArabic2 = /^([\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufbc1]|[\ufbd3-\ufd3f]|[\ufd50-\ufd8f]|[\ufd92-\ufdc7]|[\ufe70-\ufefc]|[\ufdf0-\ufdfd]|[ ])*$/g;
			if (!isArabic.test(first_arabic)) {
				toastr.error('First name given has non-arabic characters');
			}

			else if (second_arabic != '' && !isArabic2.test(second_arabic)) {
				toastr.error('Second name given has non-arabic characters');
			}
			
			/*else if ( mobile.length < 8 && mobile.length > 12  ) {
				toastr.error('Please add a valid mobile number');				
			}*/	
			
			else if(password.length < 6) {
				toastr.error('Password should contains atleast 6 characters');
			}

			else if(password != confirm_password) {
				toastr.error('Password and confirm password does not match');
			}
			  
			

			else {
				  var email = $('#email').val();	

				  //alert(email);
				  $.ajax({
				  	url: '<?=base_url('en/register/exists/')?>',
				  	data: {'email': email, 'mobile': mobile},
				  	type: 'POST',

				  	success: function(response) {
				  		//alert(response);

				  		if(response == 0) {
				  			$('#btn-register').prop('disabled', true);
				  			self.submit();
				  		}
				  		else if(response == 1) {
				  			toastr.error('Email already registered !');
				  		}
				  		else if(response == 2) {
				  			toastr.error('Mobile already registered !');
				  		}		  		
				  	},
				  	error: function(){
				  		toastr.error('Error !');
				  	}
				  	
				  });
				}
			  
		});


	</script>
      <script type="">
        function preview_image(id)
        {
          var id = id.id;
          var x = document.getElementById(id);
          var size = x.files[0].size;
          if (size > 5000000) 
          {
            toastr.error("Please select an image with size less than 5 mb.");   
            document.getElementById(id).value = "";
          }
          else 
          {
            var val = x.files[0].type;
            var type = val.substr(val.indexOf("/") + 1);
            s_type = ['jpeg','jpg','png'];
            var flag = 0;
            for (var i = 0; i < s_type.length; i++) 
            {
              if (s_type[i] == type) 
              {
                flag = flag + 1;
              }
            }
            if (flag == 0) 
            {
              toastr.error("This file format is not supported.");   
              document.getElementById(id).value = "";
            }
            else 
            {
              var reader = new FileReader();
              reader.onload = function()
              {
                var cn = id.substring(3);
                var preview = 'preview' + cn;
                var output = document.getElementById(preview);
                output.src = reader.result;
              }
              reader.readAsDataURL(x.files[0]);
            }
          }
        }
  </script>
</body>
</html>