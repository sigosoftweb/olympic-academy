<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>

    <?php 
    $this->load->view('site/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page">
		
	<?php $this->load->view('site/includes/header'); ?>
	
	<main>
		<section id="hero_in" class="courses exams">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Exam Results</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="bg_color_1 exam-results">
			<div class="container margin_60_35">	
				<h3 class="exam-title text-center pb-3">Exam Title</h3>			
				<div class="row">
					<div class="col-lg-12">
						<div class="score-sheet">
							<div class="row score-row">
								<div class="col-8 title">
									<p>No. of Questions</p>
								</div>
								<div class="col-4 ">
									<p>20</p>
								</div>
							</div>
							<div class="row score-row">
								<div class="col-8 title">
									<p>Attended</p>
								</div>
								<div class="col-4 ">
									<p>15</p>
								</div>
							</div>
							<div class="row score-row">
								<div class="col-8 title">
									<p>Left</p>
								</div>
								<div class="col-4 ">
									<p>5</p>
								</div>
							</div>
							<div class="row score-row">
								<div class="col-8 title">
									<p>Correct Answers</p>
								</div>
								<div class="col-4">
									<p>10</p>
								</div>
							</div>
							<div class="row score-row">
								<div class="col-8 title">
									<p>Wrong Answers</p>
								</div>
								<div class="col-4">
									<p>5</p>
								</div>
							</div>
							<div class="row score-row">
								<div class="col-8 title">
									<p>Time Taken</p>
								</div>
								<div class="col-4 ">
									<p>10 min</p>
								</div>
							</div>

							<div class="row score-row total-score">
								<div class="col-12 title">
									<h5><i class="ti-clipboard"></i>  Total Score</h5>
								</div>
								<div class="col-12">
									<h6><span>25</span> / 100</h6>
								</div>
							</div>

						</div>
					</div>
					
				</div>
			</div>
		</div>

	</main>
	<!--/main-->
	
	<?php $this->load->view('site/includes/footer'); ?>
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/includes/scripts'); ?>
	<script>
		
		$(document).ready(function() {
			
			var countDownDate = new Date($('#start-time').html()).getTime();
			var x = setInterval(function() {
			
				var now = new Date().getTime();

				var distance = countDownDate - now;

				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);

				

				
				$('.time-left-timer').html(('0' + hours).slice(-2) + " : " + ('0' + minutes).slice(-2) + " : " + ('0' + seconds).slice(-2));
				

				if (distance < 0) {
				    clearInterval(x);
				    $('.exam-timer').html('Expired');
			 	}

			}, 1000);
		});
	</script>
	
</body>
</html>