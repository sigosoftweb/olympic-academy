<!DOCTYPE html>
<html lang="en">

<head>

    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php
    $this->load->view('site/english/includes/styles');
    ?>

</head>

<body>

	<div id="page">

	<?php $this->load->view('site/english/includes/header'); ?>

	<main>
		<?php /*<section id="hero_in" class="courses exams">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span><?=$exam->exam_name?></h1>
				</div>
			</div>
		</section>
		<!--/hero_in--> */?>

		<div class="bg_color_1">
			<div class="container margin_60_35">
				<h3 class="no-sidebar-page-title"><?=$exam->exam_name?></h3>
				<div class="row">
					<div class="col-lg-8">
						<div class="question">
							<h3>Question <?=$question_number?> of <?=$exam->no_questions?></h3>
							<h5><?=$question->formula_question?></h5>
							<form method="post" action="<?=site_url('en/exam/answer')?>">
								<input type="hidden" name="sqa_id" value="<?=$question->sqa_id?>">
								<input type="hidden" name="next" value="<?=$next?>">
								<input type="hidden" name="que_id" value="<?=$question->que_id?>">
								<input type="hidden" name="exam_id" value="<?=$exam_id?>">
								<input type="hidden" name="se_id" value="<?=$question->se_id?>">
								<div class="question-options">
									<ul>
										<li><input type="radio" name="answer" value="a" <?php if($ans == 'a'){ ?>checked<?php } ?>> <?=$question->formula_a?></li>
										<li><input type="radio" name="answer" value="b" <?php if($ans == 'b'){ ?>checked<?php } ?>> <?=$question->formula_b?></li>
										<?php if ($question->formula_c != '') { ?>
											<li><input type="radio" name="answer" value="c" <?php if($ans == 'c'){ ?>checked<?php } ?>> <?=$question->formula_c?></li>
										<?php } ?>
										<?php if ($question->formula_d != '') { ?>
											<li><input type="radio" name="answer" value="d" <?php if($ans == 'd'){ ?>checked<?php } ?>> <?=$question->formula_d?></li>
										<?php } ?>
										<?php if ($question->formula_e != '') { ?>
											<li><input type="radio" name="answer" value="e" <?php if($ans == 'e'){ ?>checked<?php } ?>> <?=$question->formula_e?></li>
										<?php } ?>
								</div>
								<div class="float-right">
									<button type="submit" class="btn_1 rounded btn-finish">Submit</button>
								</div>
							</form>
							<div class="clearfix"></div>
							<div class="navigators float-right">
								<ul>
									<?php if ($previous) { ?>
										<li>
											<span class="prev"><a href="<?=site_url('en/exam/questions/' . $exam_id . '/' . $previous)?>"><i class="icon-left-bold"></i></a></span>
										</li>
									<?php }else { ?>
										<li class="disabled">
											<span class="prev"><i class="icon-left-bold"></i></span>
										</li>
									<?php } ?>

									<?php if ($next) { ?>
										<li>
											<span class="next"><a href="<?=site_url('en/exam/questions/' . $exam_id . '/' . $next)?>"><i class="icon-right-bold"></i></a></span>
										</li>
									<?php }else { ?>
										<li class="disabled">
											<span class="next"><i class="icon-right-bold"></i></span>
										</li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
					<aside class="col-lg-4">
						<div class="exam-sidebar">
							<div class="question-numbers">
								<h4>Questions</h4>
								<ul>
									<?php $i = 1; foreach ($questions as $ques) { ?>
										<a href="<?=site_url('en/exam/questions/' . $exam_id . '/' . $ques->que_id)?>"><li <?php if($question_number == $i){ ?>class="active"<?php } ?>><span><?=$i?></span></li></a>
									<?php $i++; } ?>
								</ul>
							</div>
							<div class="time-left">
								<p id="start-time" style="display: none;"><?=$exam->ending_time?></p>
								<h4>Time Left</h4>
								<h5 class="exam-timer">
									<span class="time-left-timer"></span>
								</h5>
							</div>
							<div class="finish my-2">
								<a href="<?=site_url('en/exam/finish/' . $exam->se_id)?>" class="btn_1 rounded btn-finish">Finish</a>
							</div>
						</div>
					</aside>
				</div>
			</div>
		</div>

	</main>
	<!--/main-->

	<?php $this->load->view('site/english/includes/footer'); ?>
	</div>
	<!-- page -->

	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/english/includes/scripts'); ?>
	<script>

		$(document).ready(function() {

			var countDownDate = new Date($('#start-time').html()).getTime();
			var x = setInterval(function() {

				var now = new Date().getTime();

				var distance = countDownDate - now;

				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);




				$('.time-left-timer').html(('0' + hours).slice(-2) + " : " + ('0' + minutes).slice(-2) + " : " + ('0' + seconds).slice(-2));


				if (distance < 0) {
				    clearInterval(x);
				    $('.exam-timer').html('Time up');
			 	}

			}, 1000);
		});
	</script>

</body>
</html>
