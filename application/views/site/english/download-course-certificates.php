<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Certificate</title>
      <style>
      @page  {margin: 0;}
      
      @page { size: A4 landscape }
      
      
         @font-face {
         font-family: fontastique;
         src: url(<?=base_url()?>assets/font/fontastique.ttf);
         }
         body{background: #ffffff; margin: 0;}
         .certificate{ padding: 10px;}
         .top{
         height: 250px;
         }
      .bottom,.middle{
         height: 250px;
         }
         .middle{
         text-align: center;
         }
         .middle p{
         font-size: 20px;
         margin-top: 20px;
         }
         .two{
         text-align: center;
         }
         .one{
         text-align: center;
         }
         .three{
         text-align: center;
         }
         .one img{
            width: 160px; 
         }
         .two img{
            width: 80px; 
         }
         .three img{
            width: 100px; 
         }
         .img-fluid{
         max-width: 100%;
         height: auto;
         }
         .bottom{

         }
        .four {
            width: 10%;
         }
         .four img{
         width: 150px;
         }
         .eight img{
         width: 70px;
         }
         .five,.six,.seven{
      
         }
         .five,.hr,.seven{
         border-top: 1px solid #000000;
         padding-top: 10px;
         margin: 0 10px;
         }
         h4,h2,p{
         font-family: fontastique;
         font-weight: 500;
         text-align: center;
         margin: 0;
         }
         h4{
         font-size: 20px;
         }
         p{
         margin-top: 5px;
         font-size: 16px;
         font-weight: 400;
         }
         h2{
         font-size: 26px;
         text-transform: uppercase;
         }
         .sign img{
            width: 300px;
         }
         .sign{
            text-align: center;
         }
      </style>
   </head>
   <body class="A4 landscape">
      <table class="certificate">
         <tr class="top">
            <td class="one"  colspan="2">
               <img src="<?=base_url()?>assets/img/one.png">
            </td>
            <td class="two">
               <img src="<?=base_url()?>assets/img/two.png">
            </td>
            <td class="three" colspan="2">
               <img src="<?=base_url()?>assets/img/three.png" >
            </td>
         </tr>
         <tr class="middle">
            <td colspan="5">
               <h2 style=" margin-top:50px">certificate</h2>
               <p>This is to certify that</p>
               <p><b>Mr/Mrs <?=$student?></b></p>
               <p><?=$certificate->content?></p>
            </td>
         </tr>
         <tr class="bottom">
            <td class="four" rowspan="2">
               <img src="<?=base_url()?>assets/img/four.png">
            </td>
            <td></td>
            <td class="sign">
               <img src="<?=base_url()?>assets/img/five.png">
            </td>
            <td></td>
            <td></td>
         </tr>
         <tr>
            <td class="five" width="25%;">
               <h4>Dr. Nabeel Taha Al Shehab</h4>
               <p>Director, Bahrain Olympic Academy NCCP Program Manager in Bahrain</p>
            </td>
            <td class="hr" width="25%">
               <h4>Lorraine Lafreniere</h4>
               <p>Chief Executive Officer, Coaching Association of Canada</p>
            </td>
            <td class="seven" width="25%">
               <h4>Mohammed H. Al-Nusuf</h4>
               <p>Secretary General, Bahrain Olympic Committeen</p>
            </td>
            <td class="eight" width="25%">
               <img src="<?=base_url()?>assets/img/six.png">
            </td>
         </tr>
      </table>
   </body>
</html>