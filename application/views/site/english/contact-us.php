﻿<!DOCTYPE html>
<html lang="en">

<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >
    
    <?php 
    $this->load->view('site/english/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page">
		
	<?php $this->load->view('site/english/includes/header'); ?>
	
	<main>
		<section id="hero_in" class="contacts">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Contact Us</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="contact_info">
			<div class="container">
				<ul class="clearfix">
					<li>
						<i class="pe-7s-map-marker"></i>
						<h4>Address</h4>
						<span><?=$contact->address?></span>
					</li>
					<li>
						<i class="pe-7s-mail-open-file"></i>
						<h4>Email address</h4>
						<span><?=$contact->email?><br><small>Sunday to Thursday 7:30am - 2pm</small></span>

					</li>
					<li>
						<i class="pe-7s-phone"></i>
						<h4>Contacts info</h4>
						<span><?=$contact->mobile?><br><small></small></span>
					</li>
				</ul>
			</div>
		</div>
		<!--/contact_info-->

		<div class="bg_color_1">
			<div class="container margin_120_95">
				<div class="row justify-content-between">
					
					<div class="col-lg-12">
						<h4>Send a message</h4>
						<div id="message-contact"></div>
						<form method="post" action="<?=base_url('en/contact/sendmail') ?>" id="contact-form" autocomplete="off">
							<div class="row">
								<div class="col-md-6">
									<span class="input">
										<input class="input_field" type="text" id="firstname" name="firstname" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required autocomplete="off">
										<label class="input_label">
											<span class="input__label-content">First Name</span>
										</label>
									</span>
								</div>
								<div class="col-md-6">
									<span class="input">
										<input class="input_field" type="text" id="lastname" name="lastname" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required autocomplete="off">
										<label class="input_label">
											<span class="input__label-content">Last Name</span>
										</label>
									</span>
								</div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-md-6">
									<span class="input">
										<input class="input_field" type="email" id="email" name="email" required autocomplete="off">
										<label class="input_label">
											<span class="input__label-content">Email</span>
										</label>
									</span>
								</div>
								<div class="col-md-6">
									<span class="input">
										<input class="input_field" type="text" id="mobile" name="mobile"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" required autocomplete="off">
										<label class="input_label">
											<span class="input__label-content">Mobile</span>
										</label>
									</span>
								</div>
							</div>
							<!-- /row -->
							<span class="input">
									<textarea class="input_field" id="message" name="message" style="height:150px;" autocomplete="off"></textarea>
									<label class="input_label">
										<span class="input__label-content">Message</span>
									</label>
							</span>
							<span class="input">
									<input class="input_field" type="text" id="verify-contact" name="verify_contact" autocomplete="off">
									<label class="input_label">
									<span class="input__label-content">Are you human? 3 + 1 =</span>
									</label>
							</span>
							<p class="add_top_30"><input type="submit" value="Submit" class="btn_1 rounded" id="submit-contact"></p>
						</form>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php $this->load->view('site/english/includes/footer'); ?>
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/english/includes/scripts'); ?>

	<script type="text/javascript">
		$('#contact-form').on('submit', function(e){	

			e.preventDefault();
			var self = this;

			
			var verify = $('#verify-contact').val();
			var mobile = $('#mobile').val();
			
			
			if ( mobile.length != 8 ) {
				toastr.error('Mobile number should contains 8 numbers');				
			}	
			
			else if ( verify != 4 ) {
				toastr.error('Give correct value for 3 + 1');				
			}			
			

			else {
				  
				self.submit();
				  		
			}
			  
		});


	</script>
	
</body>
</html>