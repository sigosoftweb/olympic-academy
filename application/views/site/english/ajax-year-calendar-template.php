
<ul class="year-calendar-navigators">
										
	<li><a class="btn_1 cal-navs prev" data-year= "<?=$current_calendar['year'] - 1 ?>" href="#"><i class="ti-angle-left" ></i></a></li>
	<li><a class="btn_1 cal-navs next" data-year= "<?=$current_calendar['year'] + 1 ?>" href="#"><i class="ti-angle-right" ></i></a></li>
</ul>	
<div class="row pt-3" style="clear: both;">
	<?php for ($i=1; $i <=12 ; $i++) { ?>
		<div class="col-lg-4 col-sm-6">
			<div class="table-responsive">	
				<?=$calendar[$i]?>
			</div>
		</div>
	<?php } ?>
</div>