<?php
	
	if (isset($this->session->userdata['olympicUser'])) {
	    $user = $this->session->userdata['olympicUser'];
	    if(isset($user)) {
	    	$userid = $user['userID'];
	    	$username = $user['userName'];
	    	//echo $username;
	    	//exit;
	    }
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/english/includes/styles');
    ?>   
    <link rel="stylesheet" href="<?=base_url()?>plugins/image-crop/croppie.css">   

</head>

<body>
	
	
	<div id="page" class="theia-exception">
		
	<?php $this->load->view('site/english/includes/header'); ?>
	
	<main>

		
		<div class="container-fluid margin_60_35 student-profile">
			<div class="row">
				<aside class="col-lg-3" id="sidebar">

					<div class="profile">
						<figure>
							<div>
								<?php if($student->stu_image) { ?>
									<img src="<?=base_url($student->stu_image)?>" alt="<?=$student->stu_name_english?>" class="rounded-circle img-fluid">
								<?php } else { ?>
									<img src="<?=base_url('site-assets/img/user.jpg')?>" alt="<?=$student->stu_name_english?>" class="rounded-circle img-fluid">
								<?php } ?>
								
							</div>
						</figure>
						<p><?=$student->stu_name_english?></p>
						<ul class="nav flex-column flex-nowrap overflow-hidden">
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('en/student/profile')?>">My Profile</a>    </li>
			                <li class="nav-item active">
			                    <a class="nav-link" href="<?=base_url('en/subscriptions')?>">My Subscriptions</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('en/subscriptions/pending')?>">Pending Subscriptions</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('en/subscriptions/rejected')?>">Rejected Subscriptions</a>
			                </li>
			                <?php if(!empty($subscribed_courses)) { ?>
			                <li class="nav-item">
			                    <a class="nav-link collapsed" href="#submenu1" data-toggle="collapse" data-target="#submenu1">Course Calendar<span class="float-right"><i class="icon-down-dir"></i></span></a>
			                    <div class="collapse" id="submenu1" aria-expanded="false">
			                        <ul class="flex-column nav">
			                        	<?php foreach ($subscribed_courses as $subscribed_course) { ?>                   		
			                            <li class="nav-item"><a class="nav-link py-0" href="<?=base_url('en/subscriptions/calendar/'). $subscribed_course->cid ?>"><?=$subscribed_course->course_title ?></a></li>
			                            <?php } ?>
			                        </ul>
			                    </div>
			                </li>
			            	<?php } ?>
			                <li class="nav-item"><a class="nav-link" href="<?=base_url('en/student/payments')?>">My Payments</a></li>
			                <li class="nav-item"><a class="nav-link" href="<?=base_url('en/student/notice')?>">Notice
			                		<?php if($notice_count->ncount) { ?>
				                		<span class="notice-count"><?=$notice_count->ncount?></span>
				                	<?php } ?>
			                </a></li>
			                <li class="nav-item"><a class="nav-link" href="<?=base_url('en/student/password/change')?>">Change Password</a></li>
			            </ul>
					</div>
				</aside>
				<!--/aside -->

				<div class="col-lg-9">
					<h3 class="sidebar-page-title">My Subscriptions</h3>
					<?php if(!empty($courses)) {
						foreach($courses as $course) { ?>
						<div class="box_list wow">				
							<div class="row no-gutters">
								<div class="col-lg-4">
									<figure class="block-reveal">
										<div class="block-horizzontal"></div>
										<a href="<?=base_url('en/subscriptions/details/'. $course->course_id)?>">
											<?php if($course->course_banner) { ?>
												<img src="<?=base_url($course->course_banner)?>" class="img-fluid" alt="">
											<?php } else { ?>
												<img src="<?=base_url('site-assets/img/no-image.jpg')?>" class="img-fluid" alt="">
											<?php }?>
										</a>
										<?php /*<div class="preview"><span>Preview course</span></div> */?>
									</figure>
								</div>
								<div class="col-lg-8">
									<div class="wrapper">
										
										<div class="price"><?=$course->course_sale_price?> BHD</div>
										<small><?=$course->cat_name?></small>
										<h3><?=$course->course_title?></h3>
										<p><?=getExcerpt($course->course_description, 0, 200)?></p>
										<?php /*<div class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></div> */?>
									</div>
									<?php if($course->sp_status == 'Approved'){ ?>
									<ul>
										<li><i class="icon_clock_alt"></i> <?=$course->course_duration?></li>
										
										<li><a href="<?=base_url('en/subscriptions/details/'. $course->course_id)?>">View Course</a></li>
										
									</ul>
									<?php if($course->grades>0) {?>
									 <h6>&nbsp;&nbsp;Grade : <b><?=$course->grade?></b></h6>
										<?php }?>
									<?php }elseif($course->sp_status == 'Waiting'){ ?>
									<ul>
										<li style="font-style:italic; color: #f00;"><p>Your subscription is under review. You can goto the course details once admin approves your request.</p></li>
									</ul>
									<?php }else{ ?>
									<ul>
										<li style="font-style:italic; color: #f00;"><p>Admin has rejected your request due to following reason,<br><?=$course->reject_reason?></p></li>
									</ul>
									<?php } ?>
								</div>
							</div>
						</div>
						<!-- /box_list -->
						<?php } 

						
					} else { ?>
						<div class="text-center margin_60_35">
							<figure><img src="<?=base_url()?>site-assets/img/wizard_intro_icon.svg" alt=""></figure>
							<h3>No courses subscribed!</h3>
						</div>
					<?php } ?>

				</div>
				<!-- /col -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->

	</main>
	<!--/main-->
	
	<?php $this->load->view('site/english/includes/footer'); ?>
	<!--/footer-->
	
	
	

</div>
<!-- page -->


	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/english/includes/scripts'); ?>
    

    
	
</body>
</html>