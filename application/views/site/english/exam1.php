<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>

    <?php 
    $this->load->view('site/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page">
		
	<?php $this->load->view('site/includes/header'); ?>
	
	<main>
		<section id="hero_in" class="courses exams">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Exam</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="bg_color_1">
			<div class="container margin_60_35">				
				<div class="row">
					<div class="col-lg-8">
						<div class="question">
							<h3>Question 1 of 10</h3>
							<h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat?</h5>
							<form>
								<div class="question-options">
									<ul>
										<li><input type="radio" name="question-options"> Option one</li>
										<li><input type="radio" name="question-options"> Option two</li>
										<li><input type="radio" name="question-options"> Option three</li>
										<li><input type="radio" name="question-options"> Option four</li>
										<li><input type="radio" name="question-options"> Option five</li>
								</div>
							</form>
							<div class="navigators float-right">
								<ul>
									<li class="disabled"><span class="prev"><i class="icon-left-bold"></i></span></li>
									<li><span class="next"><i class="icon-right-bold"></i></span></li>
								</ul>
							</div>
						</div>
					</div>
					<aside class="col-lg-4">
						<div class="exam-sidebar">
							<div class="question-numbers">
								<h4>Questions</h4>
								<ul>
									<li class="active"><span>1</span></li>
									<li><span>2</span></li>
									<li><span>3</span></li>
									<li><span>4</span></li>
									<li><span>5</span></li>
									<li><span>6</span></li>
									<li><span>7</span></li>
									<li><span>8</span></li>
									<li><span>9</span></li>
									<li><span>10</span></li>
									<li><span>94</span></li>
									<li><span>95</span></li>
									<li><span>96</span></li>
									<li><span>97</span></li>
									<li><span>98</span></li>
									<li><span>99</span></li>
									<li><span>100</span></li>
								</ul>
							</div>
							<div class="time-left">
								<p id="start-time" style="display: none;">2020-06-27 16:59:00</p>
								<h4>Time Left</h4>
								<h5 class="exam-timer">
									<span class="time-left-timer"></span>
								</h5>
							</div>
							<div class="finish my-2">
								<a href="#" class="btn_1 rounded btn-finish">Finish</a>
							</div>
						</div>
					</aside>
				</div>
			</div>
		</div>

	</main>
	<!--/main-->
	
	<?php $this->load->view('site/includes/footer'); ?>
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/includes/scripts'); ?>
	<script>
		
		$(document).ready(function() {
			
			var countDownDate = new Date($('#start-time').html()).getTime();
			var x = setInterval(function() {
			
				var now = new Date().getTime();

				var distance = countDownDate - now;

				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);

				

				
				$('.time-left-timer').html(('0' + hours).slice(-2) + " : " + ('0' + minutes).slice(-2) + " : " + ('0' + seconds).slice(-2));
				

				if (distance < 0) {
				    clearInterval(x);
				    $('.exam-timer').html('Expired');
			 	}

			}, 1000);
		});
	</script>
	
</body>
</html>