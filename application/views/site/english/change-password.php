﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >
    
    <?php 
    $this->load->view('site/english/includes/styles');
    ?>   
    <link rel="stylesheet" href="<?=base_url()?>plugins/image-crop/croppie.css">   

</head>

<body>
	
	
	<div id="page" class="theia-exception">
		
	<?php $this->load->view('site/english/includes/header'); ?>
	
	<main>

		
		<div class="container-fluid margin_60_35 student-profile">
			<div class="row">
				<aside class="col-lg-3" id="sidebar">

					<div class="profile">
						<figure>
							<div>
								<?php if($student->stu_image) { ?>
									<img src="<?=base_url($student->stu_image)?>" alt="<?=$student->stu_name_english?>" class="rounded-circle img-fluid">
								<?php } else { ?>
									<img src="<?=base_url('site-assets/img/user.jpg')?>" alt="<?=$student->stu_name_english?>" class="rounded-circle img-fluid">
								<?php } ?>
								
							</div>
						</figure>
						<p><?=$student->stu_name_english?></p>
						<ul class="nav flex-column flex-nowrap overflow-hidden">
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('en/student/profile')?>">My Profile</a>    </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('en/subscriptions')?>">My Subscriptions</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('en/subscriptions/pending')?>">Pending Subscriptions</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('en/subscriptions/rejected')?>">Rejected Subscriptions</a>
			                </li>
			                <?php if(!empty($subscribed_courses)) { ?>
			                <li class="nav-item">
			                    <a class="nav-link collapsed" href="#submenu1" data-toggle="collapse" data-target="#submenu1">Course Calendar<span class="float-right"><i class="icon-down-dir"></i></span></a>
			                    <div class="collapse" id="submenu1" aria-expanded="false">
			                        <ul class="flex-column nav">
			                        	<?php foreach ($subscribed_courses as $subscribed_course) { ?>                   		
			                            <li class="nav-item"><a class="nav-link py-0" href="<?=base_url('en/subscriptions/calendar/'). $subscribed_course->cid ?>"><?=$subscribed_course->course_title ?></a></li>
			                            <?php } ?>
			                        </ul>
			                    </div>
			                </li>
			            	<?php } ?>
			                <li class="nav-item"><a class="nav-link" href="<?=base_url('en/student/payments')?>">My Payments</a></li>
			                <li class="nav-item"><a class="nav-link" href="<?=base_url('en/student/notice')?>">Notice
			                		<?php if($notice_count->ncount) { ?>
				                		<span class="notice-count"><?=$notice_count->ncount?></span>
				                	<?php } ?>
			                </a></li>
			                <li class="nav-item active"><a class="nav-link" href="<?=base_url('en/student/password/change')?>">Change Password</a></li>
			            </ul>
					</div>
				</aside>
				<!--/aside -->

				<div class="col-lg-9">
					<h3 class="sidebar-page-title">Change Password</h3>
					<div class="change-password">
			
						<div class="box_teacher password-form-wrapper">
							<div class="">
								<form id="change-pswd-form" method="post" action="<?=base_url('en/student/changePassword')?>">
									<input type="hidden" name="stu_id" value="<?=$student->stu_id?>">
									
									<span class="input">
									<input type="password" name="current_password" class="input_field" required>
										<label class="input_label">
										<span class="input__label-content">Current password</span>
									</label>
									</span>	

									<span class="input">
									<input type="password" name="new_password" id="new-password" class="input_field" required>
										<label class="input_label">
										<span class="input__label-content">New password</span>
									</label>
									</span>	

									<span class="input">
									<input type="password" name="confirm_password" id="confirm-password" class="input_field" required>
										<label class="input_label">
										<span class="input__label-content">Confirm password</span>
									</label>
									</span>	

									<button type="submit" class="btn_1 rounded float-right">Submit</button>																
								</form>
							</div>
						</div>
				
					</div>
		
				</div>
				<!-- /col -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->

	</main>
	<!--/main-->
	
	<?php $this->load->view('site/english/includes/footer'); ?>
	<!--/footer-->
	
	

</div>
<!-- page -->


	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/english/includes/scripts'); ?>

    <script type="text/javascript">
    	

    	$('#change-pswd-form').on('submit', function(e){
			
			
			var new_password = $('#new-password').val();
			var confirm_password = $('#confirm-password').val();
			if(new_password.length < 6) {
				e.preventDefault();
				toastr.error('Password must contain atleast 6 characters');
			}
		  	
			 if (new_password != confirm_password ) {
				e.preventDefault();
				toastr.error('New password and confirm password does not match');				  
			}

			
		});

		
    </script>

    
	
</body>
</html>