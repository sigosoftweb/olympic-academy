﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>

    <?php $this->load->view('site/includes/styles'); ?>

</head>

<body>
	
	<div id="page">
		
	<?php $this->load->view('site/includes/header'); ?>
	
	<main>
		<section id="hero_in" class="courses">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Online courses</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="filters_listing sticky_horizontal">
			<div class="container">
				<form name="form_filter" id="form-filter" method="post" action="<?=base_url('site/courses')?>">
					<ul class="clearfix">
						<li>
							<div class="switch-field">
								<input type="radio" id="all" class="listing-filter" name="listing_filter" value="all" <?php if($filter == 'all') echo 'checked'?>>
								<label for="all">All</label>
								<input type="radio" id="popular" class="listing-filter" name="listing_filter" value="popular" <?php if($filter == 'popular') echo 'checked'?>>
								<label for="popular">Popular</label>
								<input type="radio" id="latest" class="listing-filter" name="listing_filter" value="latest" <?php if($filter == 'latest') echo 'checked'?>>
								<label for="latest">Latest</label>
							</div>
						</li>
						
						<li>
							<?php //echo $selected_category; ?>
							<select name="category" class="selectbox selectbox-category">
								<option value="all" <?php if($selected_category == 'all') echo 'selected' ?>>All</option>
								<?php foreach ($categories as $category) { ?>
									<option value="<?=$category->cat_id?>" <?php if($selected_category == $category->cat_id) echo 'selected' ?>><?=$category->cat_name?></option>								
								<?php } ?>
							</select>
						</li>
					</ul>
				</form>
			</div>
			<!-- /container -->
		</div>
		<!-- /filters -->

		<div class="container margin_60_35">

			<?php foreach($courses as $course) { ?>
			<div class="box_list wow">
				<div class="row no-gutters">
					<div class="col-lg-5">
						<figure class="block-reveal">
							<div class="block-horizzontal"></div>
							<a href="course-detail.html">
								<?php if($course->course_banner) { ?>
									<img src="<?=base_url($course->course_banner)?>" class="img-fluid" alt="">
								<?php } else { ?>
									<img src="<?=base_url('site-assets/img/no-image.jpg')?>" class="img-fluid" alt="">
								<?php }?>
							</a>
							<div class="preview"><span>Preview course</span></div>
						</figure>
					</div>
					<div class="col-lg-7">
						<div class="wrapper">
							<a href="#0" class="wish_bt"></a>
							<div class="price"><?=$course->course_sale_price?></div>
							<small><?=$course->cat_name?></small>
							<h3><?=$course->course_title?></h3>
							<p><?=getExcerpt($course->course_description,0,200)?></p>
							<div class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></div>
						</div>
						<ul>
							<li><i class="icon_clock_alt"></i> <?=$course->course_duration?></li>
							<li><i class="icon_like"></i> 890</li>
							<li><a href="course-detail.html">Enroll now</a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /box_list -->
			<?php } ?>
			<p class="text-center add_top_60"><a href="#0" class="btn_1">Load more</a></p>
		</div>
		<!-- /container -->
		<div class="bg_color_1">
			<div class="container margin_60_35">
				<div class="row">
					<div class="col-md-4">
						<a href="#0" class="boxed_list">
							<i class="pe-7s-help2"></i>
							<h4>Need Help? Contact us</h4>
							<p>Cum appareat maiestatis interpretaris et, et sit.</p>
						</a>
					</div>
					<div class="col-md-4">
						<a href="#0" class="boxed_list">
							<i class="pe-7s-wallet"></i>
							<h4>Payments and Refunds</h4>
							<p>Qui ea nemore eruditi, magna prima possit eu mei.</p>
						</a>
					</div>
					<div class="col-md-4">
						<a href="#0" class="boxed_list">
							<i class="pe-7s-note2"></i>
							<h4>Quality Standards</h4>
							<p>Hinc vituperata sed ut, pro laudem nonumes ex.</p>
						</a>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php $this->load->view('site/includes/footer'); ?>
	</div>
	<!-- page -->

	<?php $this->load->view('site/includes/scripts'); ?>
  
</body>
</html>

<script type="text/javascript">
	$('.listing-filter').change(function(e) {
		//alert($('.listing-filter').val());
		$('#form-filter').submit();
	});

	$('.selectbox-category').change(function(e) {
		//alert($(this).val());
		$('#form-filter').submit();
	});
</script>