<!DOCTYPE html>
<html>

<head>
	<meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >
       

    <?php 
	$this->load->view('site/english/includes/styles.php'); 
	?>
	
    

</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/english/includes/header'); 
	?>
	
	<main>
		<section id="hero_in" class="general">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Trainer Profile</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->
		<div class="container margin_60_35 trainer-detail">
			<div class="row">
				

				<div class="col-lg-12">
					<div class="box_teacher">
						<div class="profile text-center">
							<figure><img src="<?=base_url($trainer->trainer_image) ?>" alt="Teacher" class="img-fluid"></figure>
							<h3><?=$trainer->trainer_name_english . ' ' . $trainer->trainer_name_arabic ?></h3>
						</div>
						<div class="wrapper_indent">


							<?php if($trainer->trainer_education){ ?>
								<div class="indent_title_in">
									<i class="pe-7s-study"></i>
									<h5>Education</h5>
									<p><?=$trainer->trainer_education ?></p>
								</div>
							<?php } ?>
							
							<?php if($trainer->trainer_experience){ ?>
								<div class="indent_title_in">
									<i class="pe-7s-portfolio"></i>
									<h5>Experience</h5>
									<p><?=$trainer->trainer_experience ?></p>
								</div>
							<?php } ?>

							<?php if($trainer->trainer_email){ ?>
								<div class="indent_title_in">
									<i class="pe-7s-mail"></i>
									<h5>Email</h5>
									<p><?=$trainer->trainer_email ?></p>
								</div>
							<?php } ?>


							
							
						</div>
						
					</div>
				</div>
				<!-- /col -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</main>
	<!--/main-->
	
	<?php 
	$this->load->view('site/english/includes/footer'); 
	?>
	<!--/footer-->
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
    <?php 
	$this->load->view('site/english/includes/scripts'); 
	?>
	
</body>
</html>