﻿<?php
	
	if (isset($this->session->userdata['olympicUser'])) {
	    $user = $this->session->userdata['olympicUser'];
	    if(isset($user)) {
	    	$userid = $user['userID'];
	    	$username = $user['userName'];
	    	//echo $username;
	    	//exit;
	    }
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/english/includes/styles');
    ?>   
    <link rel="stylesheet" href="<?=base_url()?>plugins/image-crop/croppie.css">   

</head>

<body>
	
	
	<div id="page" class="theia-exception">
		
	<?php $this->load->view('site/english/includes/header'); ?>
	
	<main>

		
		<div class="container-fluid margin_60_35 student-profile">
			<div class="row">
				<aside class="col-lg-3" id="sidebar">

					<div class="profile">
						<figure>
							<div>
								<?php if($student->stu_image) { ?>
									<img src="<?=base_url($student->stu_image)?>" alt="<?=$student->stu_name_english?>" class="rounded-circle img-fluid">
								<?php } else { ?>
									<img src="<?=base_url('site-assets/img/user.jpg')?>" alt="<?=$student->stu_name_english?>" class="rounded-circle img-fluid">
								<?php } ?>
								
							</div>
						</figure>
						<p><?=$student->stu_name_english?></p>
						<ul class="nav flex-column flex-nowrap overflow-hidden">
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('en/student/profile')?>">My Profile</a>    </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('en/subscriptions')?>">My Subscriptions</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('en/subscriptions/pending')?>">Pending Subscriptions</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" href="<?=base_url('en/subscriptions/rejected')?>">Rejected Subscriptions</a>
			                </li>
			                <?php if(!empty($subscribed_courses)) { ?>
			                <li class="nav-item active">
			                    <a class="nav-link collapsed" href="#submenu1" data-toggle="collapse" data-target="#submenu1">Course Calendar<span class="float-right"><i class="icon-down-dir"></i></span></a>
			                    <div class="collapse show" id="submenu1" aria-expanded="false">
			                        <ul class="flex-column nav">
			                        	<?php foreach ($subscribed_courses as $subscribed_course) { ?>                   		
			                            <li class="nav-item <?php if($subscribed_course->course_title == $course->course_title) { ?> active <?php } ?>"><a class="nav-link py-0" href="<?=base_url('en/subscriptions/calendar/'). $subscribed_course->cid ?>"><?=$subscribed_course->course_title ?></a></li>
			                            <?php } ?>
			                        </ul>
			                    </div>
			                </li>
			            	<?php } ?>
			                <li class="nav-item"><a class="nav-link" href="<?=base_url('en/student/payments')?>">My Payments</a></li>
			                <li class="nav-item"><a class="nav-link" href="<?=base_url('en/student/notice')?>">Notice
			                		<?php if($notice_count->ncount) { ?>
				                		<span class="notice-count"><?=$notice_count->ncount?></span>
				                	<?php } ?>
			                </a></li>
			                <li class="nav-item"><a class="nav-link" href="<?=base_url('en/student/password/change')?>">Change Password</a></li>
			            </ul>
					</div>
				</aside>
				<!--/aside -->

				<div class="col-lg-9 calendar">
					<h3 class="sidebar-page-title">Course Calendar - <?=$course->course_title?></h3>
					
					<ul class="calendar-colors">
						<?php /*<li class="section-green"><i class="icon_easel"></i> Lesson</li> */?>
						<li class="class-yellow"><i class="icon_mic_alt"></i> Online Class</li>
						<li class="exam-blue"><i class="icon_pencil-edit"></i> Exam</li>
					</ul>

					<div class="box_general" style="clear: both;">	
						
						<div class="table-responsive">	
							<ul class="calendar-navigators">
								<?php /*<li class="btn_1 prev" data-year="<?=$current_calendar['year']?>" data-month="<?=$current_calendar['month']?>"><i class="ti-angle-left" ></i></li>
								<li class="btn_1 next"><i class="ti-angle-right"></i></li>*/

								?>
								<li><a class="btn_1 prev" href="<?=base_url('en/subscriptions/calendar/'). $course->course_id . '/' . $current_calendar['year'] . '/' . ($current_calendar['month'] - 1) ?>"><i class="ti-angle-left" ></i></a></li>
								<li><a class="btn_1 next" href="<?=base_url('en/subscriptions/calendar/'). $course->course_id . '/' . $current_calendar['year'] . '/' . ($current_calendar['month'] + 1) ?>"><i class="ti-angle-right" ></i></a></li>
							</ul>			
							<?=$calendar?>
						</div>
					</div>
				</div>
				<!-- /col -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->

	</main>
	<!--/main-->
	
	<?php $this->load->view('site/english/includes/footer'); ?>
	<!--/footer-->
	
	<div class="modal fade" id="modal-password" tabindex="-1" role="dialog" aria-labelledby="pswd-modal-title" aria-hidden="true">
  		<div class="modal-dialog" role="document">
    		<div class="modal-content">
      			<div class="modal-header">
        			<h5 class="modal-title" id="pswd-modal-title">Modal title</h5>
        			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          				<span aria-hidden="true">&times;</span>
        			</button>
      			</div>
      			<div class="modal-body">
                </div><!-- end of .modal-body -->
            </div>
        </div>
    </div><!-- end of #modal-password -->	
	

</div>
<!-- page -->


	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/english/includes/scripts'); ?>
    <script src="<?=base_url()?>site-assets/plugins/image-crop/croppie.js"></script>

    <?php /*<script type="text/javascript">
    	$('.calendar-navigators .prev').on('click', function(e){
    		var year = $(this).attr('data-year');
    		var month = $(this).attr('data-month') - 1;
    		alert(month);
    		$.ajax({
				  	url: '<?=base_url('en/subscriptions/display_calendar/'). $course->course_id?>',
				  	data: {'year': year, 'month': month},
				  	type: 'POST',

				  	success: function(response) {
				  		//alert(response);

				  		$('.table-responsive').html(response);
				  	},
				  	error: function(){
				  		toastr.error('Error !');
				  		window.location(<?=base_url('en/subscriptions/calendar/'). $course->course_id ?>)
				  	}
				  	
				  });
    	}); */?>
    	
		
    </script>

    
	
</body>
</html>