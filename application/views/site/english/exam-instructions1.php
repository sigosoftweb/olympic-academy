<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>

    <?php 
    $this->load->view('site/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page">
		
	<?php $this->load->view('site/includes/header'); ?>
	
	<main>
		<section id="hero_in" class="courses">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Exam Instructions</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="bg_color_1">
			<div class="container margin_60_35">	
				<div class="row">	
					<div class="col-lg-9">		
						<div class="instructions">
							<h4 class="mb-3">Please read the instructions below before starting the exam</h4>
							<ul>
								<li><i class="icon-check-1"></i> Lorem ipsum dolor sit amet</li>
								<li><i class="icon-check-1"></i> consectetur adipiscing elit, sed do eiusmod</li>
								<li><i class="icon-check-1"></i> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</li>
								<li><i class="icon-check-1"></i> quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</li>
								<li><i class="icon-check-1"></i> consequat. Duis aute irure dolor</li>
								<li><i class="icon-check-1"></i> in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla</li>
								<li><i class="icon-check-1"></i> pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
								<li><i class="icon-check-1"></i> quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</li>
								<li><i class="icon-check-1"></i> consequat. Duis aute irure dolor</li>
								<li><i class="icon-check-1"></i> in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla</li>
								<li><i class="icon-check-1"></i> pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
							</ul>
							<a href="<?=base_url('exam')?>" class="btn_1 rounded">Start Exam</a>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="exam-sidebar">
							
							<div class="time-left">
								<p id="start-time" style="display: none;">2020-06-27 16:59:00</p>
								<h4>Time Left</h4>
								<h5 class="exam-timer">
									<span class="days-left-timer"></span><br>
									<span class="time-left-timer"></span>
								</h5>
								
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>

	</main>
	<!--/main-->
	
	<?php $this->load->view('site/includes/footer'); ?>
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/includes/scripts'); ?>
	
	<script>
		
		$(document).ready(function() {
			
			var countDownDate = new Date($('#start-time').html()).getTime();
			var x = setInterval(function() {
			
				var now = new Date().getTime();

				var distance = countDownDate - now;

				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);

				if(days == 0) {
					$('.days-left-timer').html(' ');
				}
				else {
					$('.days-left-timer').html(days + 'D');
				}

				
				$('.time-left-timer').html(('0' + hours).slice(-2) + " : " + ('0' + minutes).slice(-2) + " : " + ('0' + seconds).slice(-2));
				

				if (distance < 0) {
				    clearInterval(x);
				    $('.exam-timer').html('Expired');
			 	}

			}, 1000);
		});
	</script>
</body>
</html>