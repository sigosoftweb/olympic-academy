<div class="table-responsive">	
	<ul class="calendar-navigators">
										
		<li><a class="btn_1 cal-navs prev" data-year= "<?=$current_calendar['year'] ?>" data-month="<?=$current_calendar['month'] - 1 ?>" data-course="<?=$course->course_id ?>" href="#"><i class="ti-angle-left" ></i></a></li>
		<li><a class="btn_1 cal-navs next" data-year= "<?=$current_calendar['year'] ?>" data-month="<?=$current_calendar['month'] + 1 ?>" data-course="<?=$course->course_id ?>" href="#"><i class="ti-angle-right" ></i></a></li>
	</ul>			
	<?=$calendar?>
</div>