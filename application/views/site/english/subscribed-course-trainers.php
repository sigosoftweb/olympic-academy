﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/english/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/english/includes/header'); 
	?>
	
	<main>
		

		<div class="bg_color_1">

			<nav class="subscribed-menu  sticky_horizontal">
				<div class="container">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
					  	<li class="nav-item">
					    	<a class="nav-link" id="description-tab" href="<?=base_url('en/subscriptions/details/') . $course->course_id ?>">Details</a>
					  	</li>


					  	<li class="nav-item">
					    	<a class="nav-link" id="classes-tab" href="<?=base_url('en/subscriptions/classes/') . $course->course_id ?>">Classes</a>
					  	</li>
					  	
					  	<li class="nav-item">
					    	<a class="nav-link" id="exams-tab" href="<?=base_url('en/subscriptions/exams/') . $course->course_id ?>">Exams</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="attendance-tab" href="<?=base_url('en/subscriptions/attendance/') . $course->course_id ?>">Attendance</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link active" id="trainers-tab" href="<?=base_url('subscriptions/trainers/') . $course->course_id ?>">Trainers</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('en/subscriptions/assignments/') . $course->course_id ?>">Assignments</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('en/subscriptions/certificates/') . $course->course_id ?>">Certificates</a>
					  	</li>
					</ul>
				</div>
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="tab-content" id="myTabContent">

						  	<div class="tab-pane fade show active" id="trainers" role="tabpanel" aria-labelledby="trainers-tab">
						  		<div class="container margin_60_35">
						  			<div class="intro_title">
										<h2>Trainers</h2>
										<?php //print_r($trainers); ?>
									</div>
									<?php if(!empty($trainers)) { ?>
						  				<div class="row trainers mt-4">
						  				
						  				<?php foreach ($trainers as $trainer) { ?>
						  					
						  					<div class="col-lg-4 col-md-6 col-sm-6">
												<div class="box_grid text-center wow">
													<figure>
														<div class="block-horizzontal"></div>
														<a href="<?=base_url('en/trainers/details/' . $trainer->trainer_id) ?>">
														<?php if($trainer->trainer_image) { ?>
															<div class="box-image" style="background-image: url(<?=base_url($trainer->trainer_image)?>);"></div>

														<?php }
														else { ?>															
															<div class="box-image" style="background-image: url(<?=base_url('site-assets/img/user.png')?>);"></div>
														<?php } ?>
														</a>
													</figure>
													<div class="trainer-details">
														<a href="<?=base_url('en/trainers/details/' . $trainer->trainer_id) ?>">
															<h3><?=$trainer->trainer_name_english . ' '. $trainer->trainer_name_arabic?></h3>
														</a>
														<h5><?=$trainer->trainer_education?></h5>
														<p><?=$trainer->trainer_experience?></p>
													</div>
												</div>
											</div>
										<?php } ?>

										</div>		
									<?php } 
									else {?>
										<div class="text-center margin_60_35">
											<figure><img src="<?=base_url()?>site-assets/img/wizard_intro_icon.svg" alt=""></figure>
											<h3>Trainers not scheduled!</h3>
										</div>
									<?php } ?>	


						  		</div>

						  		<?php if(!empty($organisers)) { ?>
							  		<div class="container margin_60_35">
							  			<div class="intro_title">
											<h2>Organizers</h2>
										</div>
										<div class="row trainers mt-4">
											<?php foreach ($organisers as $organiser) { ?>
												<div class="col-lg-4 col-md-6 col-sm-6">
													<div class="box_grid text-center">
														<figure>
															<?php if($organiser->organiser_image) { ?>
																<div class="box-image" style="background-image: url(<?=base_url($organiser->organiser_image)?>);"></div>
															<?php }
															else { ?>
																<div class="box-image" style="background-image: url(<?=base_url('site-assets/img/user.png')?>);"></div>
															<?php } ?>
														</figure>
														<div class="trainer-details">
															<h3><?=$organiser->organiser_first . ' '. $organiser->organiser_second?></h3>
															<h5><?=$organiser->organiser_education?></h5>
															<p><?=$organiser->organiser_experience?></p>
														</div>
													</div>
												</div>
											<?php } ?>
										</div>
									</div>
								<?php } ?>
								
						  	</div> <!-- end of #attendance -->							


						</div> <!-- end of .tab-content -->
					</div><!-- end of .col-lg-8 -->
					<aside class="col-lg-4 margin_60_35" id="sidebar">
						<div class="box_detail">
							<h3 class="title"><?=$course->course_title?></h3>
							<p><i class="icon_clock_alt"></i> <?=$course->course_duration?></p>
							<figure>							
									<?php if ($course->course_banner) { ?>
										<img src="<?=base_url($course->course_banner)?>" alt="" class="img-fluid">
									<?php } else {?>
										<img src="<?=base_url('site-assets/img/no-image.jpg')?>" alt="" class="img-fluid">
									<?php } ?>
									
							</figure>
												
						</div>
					</aside>
				</div>
			</div>
			
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php 
	$this->load->view('site/english/includes/footer'); 
	?>
	</div>
	<!-- page -->
	
	<?php 
	$this->load->view('site/english/includes/scripts'); 
	?>
	
  
</body>
</html>