<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>

    <?php 
    $this->load->view('site/includes/styles.php');
    ?>      

</head>

<body id="register_bg">
	
	<nav id="menu" class="fake_menu"></nav>
	
	<div id="preloader">
		<div data-loader="circle-side"></div>
	</div>
	<!-- End Preload -->
	
	<div id="login">
		<aside>
			<figure class="logo">
				<a href="index.html">
					<img src="<?=base_url('site-assets/img/logo.png')?>" width="150" height="45" data-retina="true" alt="">
					
				</a>
			</figure>
			<form autocomplete="off" action="<?=site_url('site/register/student')?>" method="post" id="register-form" enctype="multipart/form-data">
				<div class="form-group">

					<span class="input">
						<input type="text" class="input_field" name="stu_name_english" required>
						<label class="input_label">
						<span class="input__label-content">Name (English)</span>
					</label>
					</span>

					<span class="input">
					<input type="text" class="input_field" id="name-arabic" name="stu_name_arabic">
						<label class="input_label">
						<span class="input__label-content">Name (Arabic)</span>
					</label>
					</span>

					<!--<span class="input">
					<input type="text" class="input_field" name="stu_cpr">
						<label class="input_label">
						<span class="input__label-content">CPR</span>
					</label>
					</span>

					<span class="input">
					<input type="date" name="stu_dob" class="input_field">
						<label class="input_label">
						<span class="input__label-content">Date of Birth</span>
					</label>
					</span>

					<span class="input">
					<input type="text" name="stu_education" class="input_field">
						<label class="input_label">
						<span class="input__label-content">Education</span>
					</label>
					</span>-->

					

					<span class="input">
					<input type="text" name="stu_mobile" id="mobile-number" class="input_field"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>
						<label class="input_label">
						<span class="input__label-content">Mobile</span>
					</label>
					</span>

					<span class="input">
					<input type="email" name="stu_email" class="input_field" required>
						<label class="input_label">
						<span class="input__label-content">Email</span>
					</label>
					</span>

					<span class="input">
					<input type="password" name="password" id="password" class="input_field" required>
						<label class="input_label">
						<span class="input__label-content">Your password</span>
					</label>
					</span>

					<span class="input">
					<input class="input_field" type="password" name="confirm_password" id="confirm-password" required>
						<label class="input_label">
						<span class="input__label-content">Confirm password</span>
					</label>
					</span>
					
					<div id="pass-info" class="clearfix"></div>
				</div>
				
				<button class="btn_1 rounded full-width add_top_30" type="submit">Register to Olympic Academy</button>
				<div class="text-center add_top_10">Already have an acccount? <strong><a href="<?=base_url('site/login')?>">Sign In</a></strong></div>
			</form>
			<div class="copy">© 2020 Olympic Academy</div>
		</aside>
	</div>
	<!-- /login -->
	
	<?php $this->load->view('site/includes/scripts.php'); ?>
	
	<!-- SPECIFIC SCRIPTS -->
	<?php /*<script src="<?=base_url('js/pw_strenght.js')?>"></script>*/?>

	<script type="text/javascript">
		$('#register-form').on('submit', function(e){
			  
			  var isArabic = /^([\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufbc1]|[\ufbd3-\ufd3f]|[\ufd50-\ufd8f]|[\ufd92-\ufdc7]|[\ufe70-\ufefc]|[\ufdf0-\ufdfd]|[ ])*$/g; 
			  var mobile = $('#mobile-number').val();
			  var password = $('#password').val();
			  var arabic = $('#name-arabic').val();
			  if (mobile.length != 8 || password.length < 7) {
				  if ( mobile.length != 8 ) {
					  toastr.error('Mobile number should contains 8 numbers');
				  }
				  else {
					  toastr.error('Password should contains atleast 6 characters');
				  }
				  e.preventDefault();
			  }
			  if (!isArabic.test(arabic)){
			          toastr.error("Field contains Non-arabic letters","");	
			          e.preventDefault();	      
			  } 
			  
		});
	</script>
  
</body>
</html>