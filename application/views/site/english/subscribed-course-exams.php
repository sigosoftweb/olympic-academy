﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/english/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/english/includes/header'); 
	?>
	
	<main>
		

		<div class="bg_color_1">

			<nav class="subscribed-menu  sticky_horizontal">
				<div class="container">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
					  	<li class="nav-item">
					    	<a class="nav-link" id="home-tab" href="<?=base_url('en/subscriptions/details/') . $course->course_id ?>">Details</a>
					  	</li>

					  	<li class="nav-item">
					    	<a class="nav-link" id="classes-tab" href="<?=base_url('en/subscriptions/classes/') . $course->course_id ?>">Classes</a>
					  	</li>
					  	
					  	<li class="nav-item">
					    	<a class="nav-link active" id="contact-tab" href="<?=base_url('en/subscriptions/exams/') . $course->course_id ?>">Exams</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="contact-tab" href="<?=base_url('en/subscriptions/attendance/') . $course->course_id ?>">Attendance</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="trainers-tab" href="<?=base_url('en/subscriptions/trainers/') . $course->course_id ?>">Trainers</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('en/subscriptions/assignments/') . $course->course_id ?>">Assignments</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('en/subscriptions/certificates/') . $course->course_id ?>">Certificates</a>
					  	</li>
					</ul>
				</div>
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="tab-content" id="myTabContent">

						  	<div class="tab-pane fade show active" id="exams" role="tabpanel" aria-labelledby="exams-tab">
						  		<div class="container margin_60_35">
						  			<div class="intro_title">
										<h2>Upcoming Exams</h2>
										<ul>
											
												<?php if (count($exams) > 1) { ?>
													<li><?=count($exams)?> exams</li>
												<?php }else { ?>
													<li><?=count($exams)?> exam</li>
												<?php } ?>
											
										</ul>
									</div>
						  			<div id="accordion_exams_upcoming" role="tablist" class="add_bottom_45">
										<?php 
										$first_child = true;
										$i=1;
										foreach ($exams as $exam) { ?>
											<div class="card">
												<div class="card-header" role="tab" id="up-exam-title-<?=$i?>">
													<h5 class="mb-0">
														<?php if($first_child) { ?>
														<a data-toggle="collapse" href="#up-exam-<?=$i?>" aria-expanded="true" aria-controls="up-exam-<?=$i?>">
															<i class="indicator ti-minus"></i>
														<?php } 
														else {?>
															<a class="collapsed" data-toggle="collapse" href="#up-exam-<?=$i?>" aria-expanded="false" aria-controls="up-exam-<?=$i?>">
															<i class="indicator ti-plus"></i>
														<?php } ?>
															<?=$exam->exam_name?>
															<span class="float-right"><?=$exam->exam_time?> minutes</span>
														</a>
													</h5>
												</div>


												<div id="up-exam-<?=$i?>" class="collapse <?php if($first_child) { echo 'show'; }?>" role="tabpanel" aria-labelledby="up-exam-title-<?=$i?>" data-parent="#accordion_exams_upcoming">

													<div class="card-body">
														<div class="list_lessons">
															<ul>
																<?php if ($exam->cs_id != '0') { ?>
																	<li>From <span><?=$exam->section_title?></span></li>
																<?php } ?>
																<li>Questions<span><?=$exam->no_questions?></span></li>
																<?php if ($exam->time_status == '1') { ?>
																	<li>Date<span><?=date('d/m/Y',strtotime($exam->from_time . ' ' . $timezone))?></span></li>
																	<li>Time<span><?=date('h:i A',strtotime($exam->from_time . ' ' . $timezone))?> - <?=date('h:i A',strtotime($exam->to_time . ' ' . $timezone))?></span></li>
																<?php } ?>
																
															</ul>
															
															<a href="<?=base_url('en/exam/instructions/' . $exam->exam_id .'/' . $course->course_id)?>" class="btn-exam float-right my-2">Start Exam</a>
														</div>
													</div>
												</div>
											</div>
											<?php 
											$i++;
											$first_child = false;
										} ?>
									</div>



						  		</div>
								<div class="container margin_60_35">
						  			<div class="intro_title">
										<h2>Attended Exams</h2>
										<ul>
											<?php if (count($attended) > 1) { ?>
												<li><?=count($attended)?> exams</li>
											<?php }else { ?>
												<li><?=count($attended)?> exam</li>
											<?php } ?>
										</ul>
									</div>
						  			<div id="accordion_exams_attended" role="tablist" class="add_bottom_45">
										<?php 
										$first_child = true;
										$i=1;
										foreach ($attended as $exam) { ?>
											<div class="card">
												<div class="card-header" role="tab" id="atexam-title-<?=$i?>">
													<h5 class="mb-0">
													<?php if($first_child) { ?>
														<a data-toggle="collapse" href="#atexam-<?=$i?>" aria-expanded="true" aria-controls="atexam-<?=$i?>">
															<i class="indicator ti-minus"></i>
													<?php } 
													else {?>
														<a class="collapsed" data-toggle="collapse" href="#atexam-<?=$i?>" aria-expanded="false" aria-controls="atexam-<?=$i?>">
															<i class="indicator ti-plus"></i>
													<?php } ?>
															<?=$exam->exam_name?>
															<span class="float-right"><?=$exam->exam_time?> minutes</span>
														</a>
													</h5>
												</div>

												<div id="atexam-<?=$i?>" class="collapse 
													<?php if($first_child) { echo 'show'; } ?> " role="tabpanel" aria-labelledby="atexam-title-<?=$i?>" data-parent="#accordion_exams_attended">

													<div class="card-body">
														<div class="list_lessons">
															<ul>
																<?php if ($exam->cs_id != '0') { ?>
																	<li>From <span><?=$exam->section_title?></span></li>
																<?php } ?>
																<li>Questions<span><?=$exam->no_questions?></span></li>
																<?php if ($exam->time_status == '1') { ?>
																	<li>Date<span><?=date('d/m/Y',strtotime($exam->from_time))?></span></li>
																	<li>Time<span><?=date('h:i A',strtotime($exam->from_time))?> - <?=date('h:i A',strtotime($exam->to_time))?></span></li>
																<?php } ?>
																
															</ul>
															
															<a href="<?=base_url('en/exam/result/' . $exam->se_id )?>" class="btn-exam float-right my-2">View result</a>
															<!--<a href="<?=base_url('en/exam/view/' . $exam->se_id )?>" class="btn-exam float-right my-2 mr-2">View questions</a>-->
																	
																
														</div>
													</div>
												</div>
											</div>
											<?php 
											$i++;
											$first_child = false;
										} ?>
									</div>



						  		</div>
								<div class="container margin_60_35">
						  			<div class="intro_title">
										<h2>Unattempted Exams</h2>
										<ul>
											<?php if (count($unattempted) > 1) { ?>
												<li><?=count($unattempted)?> exams</li>
											<?php }else { ?>
												<li><?=count($unattempted)?> exam</li>
											<?php } ?>
										</ul>
									</div>
						  			<div id="accordion_exams_unattempted" role="tablist" class="add_bottom_45">
										<?php 
										$first_child = true;
										$i=1;
										foreach ($unattempted as $exam) { ?>
											<div class="card">
												<div class="card-header" role="tab" id="unexam-title-<?=$i?>">
													<h5 class="mb-0">
														<?php if($first_child) { ?>
														<a data-toggle="collapse" href="#unexam-<?=$i?>" aria-expanded="true" aria-controls="unexam-<?=$i?>">
															<i class="indicator ti-minus"></i>
														<?php }
														else { ?>
														<a class="collapsed" data-toggle="collapse" href="#unexam-<?=$i?>" aria-expanded="false" aria-controls="unexam-<?=$i?>">
															<i class="indicator ti-plus"></i>
														<?php } ?>
															<?=$exam->exam_name?>
															<span class="float-right"><?=$exam->exam_time?> minutes</span>
														</a>
													</h5>
												</div>

												<div id="unexam-<?=$i?>" class="collapse show" role="tabpanel" aria-labelledby="unexam-title-<?=$i?>" data-parent="#accordion_exams_unattempted">
													<div class="card-body">
														<div class="list_lessons">
															<ul>
																<?php if ($exam->cs_id != '0') { ?>
																	<li>From <span><?=$exam->section_title?></span></li>
																<?php } ?>
																<li>Questions<span><?=$exam->no_questions?></span></li>
																<?php if ($exam->time_status == '1') { ?>
																	<li>Date<span><?=date('d/m/Y',strtotime($exam->from_time))?></span></li>
																	<li>Time<span><?=date('h:i A',strtotime($exam->from_time))?> - <?=date('h:i A',strtotime($exam->to_time))?></span></li>
																<?php } ?>
																
															</ul>
															<a href="<?=base_url('en/exam/unattempted/' . $exam->exam_id )?>" class="btn-exam float-right my-2">View questions</a>
														</div>
													</div>
												</div>
											</div>
										<?php } ?>
									</div>
						  		</div>
						  	</div> <!-- end of #exams -->							


						</div> <!-- end of .tab-content -->
					</div><!-- end of .col-lg-8 -->
					<aside class="col-lg-4 margin_60_35" id="sidebar">
						<div class="box_detail">
							<h3 class="title"><?=$course->course_title?></h3>
							<p><i class="icon_clock_alt"></i> <?=$course->course_duration?></p>
							<figure>							
									<?php if ($course->course_banner) { ?>
										<img src="<?=base_url($course->course_banner)?>" alt="" class="img-fluid">
									<?php } else {?>
										<img src="<?=base_url('site-assets/img/no-image.jpg')?>" alt="" class="img-fluid">
									<?php } ?>
									
							</figure>
													
						</div>
					</aside>
				</div>
			</div>
			
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php 
	$this->load->view('site/english/includes/footer'); 
	?>
	</div>
	<!-- page -->
	
	<?php 
	$this->load->view('site/english/includes/scripts'); 
	?>
	
  
</body>
</html>