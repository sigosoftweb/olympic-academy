<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >
    <link href="https://goSellJSLib.b-cdn.net/v1.4.1/css/gosell.css" rel="stylesheet" />
    <?php 
    $this->load->view('site/english/includes/styles');
    ?>  
    <script type="text/javascript" src="https://goSellJSLib.b-cdn.net/v1.4.1/js/gosell.js"></script>
</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/english/includes/header'); 
	?>
	
	<main>
        <div id="root"></div>
		<div class="container margin_60_35">

			<div class="row">
				<div class="col-lg-4 text-center align-center">
					<div class="box_grid wow" style="min-height:500px;">
						<figure class="block-reveal">
							<div class="block-horizzontal"></div>
							
							<a href="<?=base_url('en/courses/details/'. $course->course_id)?>">
								<?php if($course->course_banner) { ?>
									<img src="<?=base_url($course->course_banner)?>" class="img-fluid" alt="">
								<?php } else { ?>
									<img src="<?=base_url('site-assets/img/no-image.jpg')?>" class="img-fluid" alt="">
								<?php }?>
							</a>
							<div class="price"><?=$course->course_sale_price?> BHD</div>
							<?php /*<div class="preview"><span>Preview course</span></div> */?>
						</figure>
						<div class="wrapper">
							<h3><?=$course->course_title?></h3>
							
						</div>
						
					</div>
					<h1></h1>
					<?php /*<a id="btn-subscribe" href="<?=base_url('en/student/subscribe_process/'. $course->course_id)?>" class="btn_1 rounded full-width add_top_30">Confirm Subscribe</a>*/?>
					<a href="#" class="btn_1 rounded full-width add_top_30" onclick="goSell.openLightBox()">Confirm Subscribe</a>
				</div>
			</div>



		</div>

		<div id="subscribe-modal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Subscribe</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    
                <div class="modal-body">                        
                    <p>Your subscription is under review. You can goto the course details only when admin approves your request.</p>
                    <a href="<?=base_url('en/subscriptions') ?>" class="btn_1 rounded float-right">OK</a>
                </div>
                
                    
            </div>
        </div>
    </div>

	</main>
	<!--/main-->
	
	<?php $this->load->view('site/english/includes/footer'); ?>

	

	</div>
	<!-- page -->

	<?php $this->load->view('site/english/includes/scripts'); ?>
    <script>

    goSell.config({
      containerID:"root",
      gateway:{
        publicKey:"pk_test_9gn0dc1rvTaKWlhsiMuJ7V6x",
        language:"en",
        contactInfo:true,
        supportedCurrencies:"all",
        supportedPaymentMethods: "all",
        saveCardOption:false,
        customerCards: true,
        notifications:'standard',
        callback:(response) => {
            console.log('response', response);
        },
        onClose: () => {
            console.log("onClose Event");
        },
        backgroundImg: {
          url: 'imgURL',
          opacity: '0.5'
        },
        labels:{
            cardNumber:"Card Number",
            expirationDate:"MM/YY",
            cvv:"CVV",
            cardHolder:"Name on Card",
            actionButton:"Pay"
        },
        style: {
            base: {
              color: '#535353',
              lineHeight: '18px',
              fontFamily: 'sans-serif',
              fontSmoothing: 'antialiased',
              fontSize: '16px',
              '::placeholder': {
                color: 'rgba(0, 0, 0, 0.26)',
                fontSize:'15px'
              }
            },
            invalid: {
              color: 'red',
              iconColor: '#fa755a '
            }
        }
      },
      customer:{
        first_name: "<?=$student->stu_name_english?>",
        middle_name: " ",
        last_name: "<?=$student->stu_name_arabic?>",
        email: "<?=$student->stu_email?>",
        phone: {
            country_code: "973",
            number: "<?=$student->stu_mobile?>"
        }
      },
      order:{
        amount: <?=$course->course_sale_price?>,
        currency:"BHD",
        items:[{
          id:1,
          name:'<?=$course->course_title?>',
          description: 'item1 desc',
          quantity: '1',
          amount_per_unit:'<?=$course->course_sale_price?>',
          discount: {
            type: 'P',
            value: '0%'
          },
          total_amount: '<?=$course->course_sale_price?>'
        }],
        shipping:null,
        taxes: null
      },
     transaction:{
       mode: 'charge',
       charge:{
          saveCard: false,
          threeDSecure: true,
          description: "Test Description",
          statement_descriptor: "Sample",
          reference:{
            transaction: "txn_0001",
            order: "ord_0001"
          },
          metadata:{},
          receipt:{
            email: false,
            sms: true
          },
          redirect: "<?=site_url('student/redirectURL')?>",
          post: "<?=site_url('payment/postURL')?>",
        }
     }
    });

    </script>
	
  
</body>
</html>
