﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>

    <?php 
    $this->load->view('site/includes/styles.php');
    ?>  

</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/includes/header.php'); 
	?>
	
	<main>
		<section id="hero_in" class="courses">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span><?=$course->course_title?></h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		<div class="bg_color_1">
			<nav class="secondary_nav sticky_horizontal">
				<div class="container">
					<ul class="clearfix">
						<li><a href="#description" class="active">Description</a></li>
						<li><a href="#lessons">Lessons</a></li>
						<?php /*<li><a href="#reviews">Reviews</a></li> */?>
					</ul>
				</div>
			</nav>
			<div class="container margin_60_35">
				<div class="row">
					<div class="col-lg-8">
						
						<section id="description">
							<h2>Description</h2>							
							<div class="row">
								<div class="col-lg-12">
									<?=$course->course_description?>
								</div>
							</div>
							<!-- /row -->
						</section>
						<!-- /section -->
						<?php //print_r($course_sections);?>
						<section id="lessons">
							<div class="intro_title">
								<h2>Sections</h2>
								<ul>
									<li><?=$sections_count->scount?> sections</li>
									<?php /*<li>01:02:10</li> */?>
								</ul>
							</div>
							<div id="accordion_lessons" role="tablist" class="add_bottom_45">
								<?php 
								$first_child = true;
								foreach ($course_sections as $course_section) { ?>
								<div class="card">
									<!-- .card-header starts -->
									<div class="card-header" role="tab" id="heading-<?=$course_section->cs_id?>">
										<h5 class="mb-0">
											<?php if($first_child) { ?>
												<a data-toggle="collapse" href="#collapse-<?=$course_section->cs_id?>" aria-expanded="true" aria-controls="collapse-<?=$course_section->cs_id?>"><i class="indicator ti-minus"></i>
											<?php } else { ?>
												<a class="collapsed" data-toggle="collapse" href="#collapse-<?=$course_section->cs_id?>" aria-expanded="false" aria-controls="collapse-<?=$course_section->cs_id?>">
												<i class="indicator ti-plus"></i>

											<?php } ?>
											<?=$course_section->section_title?></a>
										</h5>
									</div>
									<!-- .card-header ends -->

									<?php if($first_child) { ?>
									<div id="collapse-<?=$course_section->cs_id?>" class="collapse show" role="tabpanel" aria-labelledby="heading-<?=$course_section->cs_id?>" data-parent="#accordion_lessons">
									<?php } else { ?>
									<div id="collapse-<?=$course_section->cs_id?>" class="collapse" role="tabpanel" aria-labelledby="heading-<?=$course_section->cs_id?>" data-parent="#accordion_lessons">
									<?php } ?>	
										<div class="card-body">
											<div>
												<?=$course_section->section_description?>
											</div>
											
											<div class="list_lessons">
												<ul>
												<?php if($course_section->section_attachment) { ?>
													<li><a href="<?=base_url($course_section->section_attachment)?>" target="_blank"
														<?php
														switch ($course_section->section_file_type) {
															case 'Documents':
																echo 'class="txt_doc"';
																break;
															case 'Image':
																echo '';
																break;
															case 'Video':
																echo 'class="video"';
																break;
															default:
																		//echo '<i class="fas fa-paperclip"></i>';
																break;
														}

														?>
														>Attachment</a></li>
												<?php } ?>

												<?php if($course_section->section_youtube) { ?>
													<li><a href="<?=$course_section->section_youtube?>" target="_blank"><i class="fab fa-youtube"></i> Video</a></li>
												<?php } ?>

												<?php if($course_section->section_other) { ?>
													<li><a href="<?=$course_section->section_other?>" target="_blank"><i class="fas fa-paperclip"></i> Other</a></li>
												<?php } ?>
														
												</ul>
											</div> <!-- end of .list_lessons -->
											
										</div>
									</div>
								</div>
								<!-- /card -->
								<?php 
								$first_child = false;
								} ?>
								
							</div>
							<!-- /accordion --> 
						</section>
						<!-- /section -->
						
						<?php /*<section id="reviews">
							<h2>Reviews</h2>
							<div class="reviews-container">
								<div class="row">
									<div class="col-lg-3">
										<div id="review_summary">
											<strong>4.7</strong>
											<div class="rating">
												<i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
											</div>
											<small>Based on 4 reviews</small>
										</div>
									</div>
									<div class="col-lg-9">
										<div class="row">
											<div class="col-lg-10 col-9">
												<div class="progress">
													<div class="progress-bar" role="progressbar" style="width: 90%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
											</div>
											<div class="col-lg-2 col-3"><small><strong>5 stars</strong></small></div>
										</div>
										<!-- /row -->
										<div class="row">
											<div class="col-lg-10 col-9">
												<div class="progress">
													<div class="progress-bar" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
											</div>
											<div class="col-lg-2 col-3"><small><strong>4 stars</strong></small></div>
										</div>
										<!-- /row -->
										<div class="row">
											<div class="col-lg-10 col-9">
												<div class="progress">
													<div class="progress-bar" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
											</div>
											<div class="col-lg-2 col-3"><small><strong>3 stars</strong></small></div>
										</div>
										<!-- /row -->
										<div class="row">
											<div class="col-lg-10 col-9">
												<div class="progress">
													<div class="progress-bar" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
											</div>
											<div class="col-lg-2 col-3"><small><strong>2 stars</strong></small></div>
										</div>
										<!-- /row -->
										<div class="row">
											<div class="col-lg-10 col-9">
												<div class="progress">
													<div class="progress-bar" role="progressbar" style="width: 0" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
											</div>
											<div class="col-lg-2 col-3"><small><strong>1 stars</strong></small></div>
										</div>
										<!-- /row -->
									</div>
								</div>
								<!-- /row -->
							</div>

							<hr>

							<div class="reviews-container">

								<div class="review-box clearfix">
									<figure class="rev-thumb"><img src="img/avatar1.jpg" alt="">
									</figure>
									<div class="rev-content">
										<div class="rating">
											<i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
										</div>
										<div class="rev-info">
											Admin – April 03, 2016:
										</div>
										<div class="rev-text">
											<p>
												Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis
											</p>
										</div>
									</div>
								</div>
								<!-- /review-box -->
								<div class="review-box clearfix">
									<figure class="rev-thumb"><img src="img/avatar2.jpg" alt="">
									</figure>
									<div class="rev-content">
										<div class="rating">
											<i class="icon-star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
										</div>
										<div class="rev-info">
											Ahsan – April 01, 2016:
										</div>
										<div class="rev-text">
											<p>
												Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis
											</p>
										</div>
									</div>
								</div>
								<!-- /review-box -->
								<div class="review-box clearfix">
									<figure class="rev-thumb"><img src="img/avatar3.jpg" alt="">
									</figure>
									<div class="rev-content">
										<div class="rating">
											<i class="icon-star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
										</div>
										<div class="rev-info">
											Sara – March 31, 2016:
										</div>
										<div class="rev-text">
											<p>
												Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis
											</p>
										</div>
									</div>
								</div>
								<!-- /review-box -->
							</div>
							<!-- /review-container -->
						</section>
						<!-- /section --> */ ?>
					</div>
					<!-- /col -->
					
					<aside class="col-lg-4" id="sidebar">
						<div class="box_detail">
							<figure>							
									<?php if ($course->course_banner) { ?>
										<img src="<?=base_url($course->course_banner)?>" alt="" class="img-fluid">
									<?php } else {?>
										<img src="<?=base_url('site-assets/img/no-image.jpg')?>" alt="" class="img-fluid">
									<?php } ?>
									
							</figure>
							<div class="price">
								<?=$course->amount?> AED
								
							</div>
							
							
						</div>
					</aside>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php 
	$this->load->view('site/includes/footer.php'); 
	?>
	</div>
	<!-- page -->
	
	<?php 
	$this->load->view('site/includes/scripts.php'); 
	?>
	
  
</body>
</html>