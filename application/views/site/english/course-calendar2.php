<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

	<?php $this->load->view('site/includes/styles'); ?>

	<!-- SPECIFIC CSS -->
	<link href="<?=base_url('site-assets/css/fullcalendar.print.css')?>" rel="stylesheet" media="print">

    

</head>

<body>
	
	<div id="page">
		
	<?php $this->load->view('site/includes/header'); ?>
	
	<main>
		

		<div class="container margin_60_35 course-calendar">
			<h3 class="no-sidebar-page-title">Course Calendar</h3>
			<div class="row">
				<div class="col-md-9">
					<div class="box_general">						
						<?=$calendar?>
					</div>
				</div>
				<div class="col-md-3">
					<div id="external-events">
											
						<div class='external-event'><i class="icon_mic_alt"></i> Online Class</div>
						<div class='external-event'><i class="icon_easel"></i> Lesson</div>
						<div class='external-event'><i class="icon_pencil-edit"></i> Exam</div>
						
					</div>
				</div>
			</div>
			<!--/row-->
		</div>
		<!-- /container -->
	</main>
	<!--/main-->
	
	<?php $this->load->view('site/includes/footer'); ?>
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/includes/scripts'); ?>
	
	
	
</body>
</html>