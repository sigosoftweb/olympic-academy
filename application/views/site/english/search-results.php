<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/english/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page">
		
	<?php $this->load->view('site/english/includes/header'); ?>
	
	<main>
		<section class="hero_single version_2">
			<div class="wrapper">
				<div class="container">
					<h3>What would you learn?</h3>					
					<form method="get" action="<?=base_url('en/search') ?>">
						<div id="custom-search-input">
							<div class="input-group">
								<input type="text" name="q" class="search-query" placeholder="">
								<input type="submit" class="btn_search" value="Search">
							</div>
						</div>
					</form>
				</div>
			</div>
		</section>
		<!-- /hero_single -->

		<div class="bg_color_1">
			<div class="container margin_60_35">				
				
						<div class="row">
							<?php 
							if(!empty($courses)) {
							foreach($courses as $course) { ?>
							<div class="col-xl-4 col-lg-6 col-md-6">
								<div class="box_grid wow">
									<figure class="block-reveal">
										<div class="block-horizzontal"></div>
							
										<a href="<?=base_url('en/courses/details/'. $course->course_id)?>">
										<?php if($course->course_banner) { ?>
											<img src="<?=base_url($course->course_banner)?>" class="img-fluid" alt="">
										<?php } else { ?>
											<img src="<?=base_url('site-assets/img/no-image.jpg')?>" class="img-fluid" alt="">
										<?php }?>
										</a>
										<div class="price"><?=$course->course_sale_price?> AED</div>
									</figure>
									<div class="wrapper">
										<small><?=$course->cat_name?></small>
										<h3><?=$course->course_title?></h3>
										<p><?=getExcerpt($course->course_description)?></p>
							
									</div>
									<ul>
										<li><i class="icon_clock_alt"></i> <?=$course->course_duration?></li>
										<?php if(isStudent()) {
								
								
										if(is_in_array($courseIDs, 'course_id', $course->course_id)) { ?>
											<li><a href="<?=base_url('en/subscriptions/details/'. $course->course_id)?>">Subscribed!</a></li>
										<?php } else { ?>
											<li><a href="<?=base_url('en/courses/details/'. $course->course_id)?>">Enroll now</a></li>
										<?php }
										} else {?>
											<li><a href="<?=base_url('en/courses/details/'. $course->course_id)?>">Enroll now</a></li>
										<?php } ?>

							
									</ul>
								</div>
							</div>
							<!-- /box_grid -->
							<?php } 
							}
							else { ?>
								<div class="col-12 text-center">
									<figure><img src="<?=base_url()?>site-assets/img/wizard_intro_icon.svg" alt=""></figure>
									<h3>No results found!</h3>
								</div>
							<?php } ?>							
						</div>
					
				
			</div>
		</div>

	</main>
	<!--/main-->
	
	<?php $this->load->view('site/english/includes/footer'); ?>
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/english/includes/scripts'); ?>
	<script>
		
		$(document).ready(function() {
			
			var countDownDate = new Date($('#start-time').html()).getTime();
			var x = setInterval(function() {
			
				var now = new Date().getTime();

				var distance = countDownDate - now;

				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);

				

				
				$('.time-left-timer').html(('0' + hours).slice(-2) + " : " + ('0' + minutes).slice(-2) + " : " + ('0' + seconds).slice(-2));
				

				if (distance < 0) {
				    clearInterval(x);
				    $('.exam-timer').html('Expired');
			 	}

			}, 1000);
		});
	</script>
	
</body>
</html>