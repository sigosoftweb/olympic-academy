﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >
    <?php 
    $this->load->view('site/english/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/english/includes/header'); 
	?>
	
	<main>
		

		<div class="bg_color_1">

			<nav class="subscribed-menu  sticky_horizontal">
				<div class="container">
					<ul class="nav nav-tabs" id="myTab" role="tablist">

					  	<li class="nav-item">
					    	<a class="nav-link" id="description-tab" href="<?=base_url('en/subscriptions/details/') . $course->course_id ?>">Details</a>
					  	</li>

					  	<li class="nav-item">
					    	<a class="nav-link active" id="sections-tab" href="<?=base_url('en/subscriptions/sections/') . $course->course_id ?>">Lessons</a>
					  	</li>

					  	<li class="nav-item">
					    	<a class="nav-link" id="classes-tab" href="<?=base_url('en/subscriptions/classes/') . $course->course_id ?>">Classes</a>
					  	</li>
					  	
					  	<li class="nav-item">
					    	<a class="nav-link" id="exams-tab" href="<?=base_url('en/subscriptions/exams/') . $course->course_id ?>">Exams</a>
					  	</li>

					  	<li class="nav-item">
					    	<a class="nav-link" id="attendance-tab" href="<?=base_url('en/subscriptions/attendance/') . $course->course_id ?>">Attendance</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="trainers-tab" href="<?=base_url('en/subscriptions/trainers/') . $course->course_id ?>">Trainers</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('en/subscriptions/assignments/') . $course->course_id ?>">Assignments</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('en/subscriptions/certificates/') . $course->course_id ?>">Certificates</a>
					  	</li>
					</ul>
				</div>
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="tab-content" id="myTabContent">				  	


						  	<div class="tab-pane fade show active" id="lessons" role="tabpanel" aria-labelledby="lessons-tab">
						  		<div class="container margin_60_35">
							  		<div class="intro_title">
										<h2>Lessons</h2>
										<ul>
											<li><?=$sections_count->scount?> lessons</li>
											<?php /*<li>01:02:10</li> */?>
										</ul>
									</div>
									<div id="accordion_lessons" role="tablist" class="add_bottom_45">
										<?php 
										$first_child = true;
										foreach ($course_sections as $course_section) { ?>
											<div class="card">
												<!-- .card-header starts -->
												<div class="card-header" role="tab" id="heading-<?=$course_section->cs_id?>">
													<h5 class="mb-0">
														<?php if($first_child) { ?>
															<a data-toggle="collapse" href="#collapse-<?=$course_section->cs_id?>" aria-expanded="true" aria-controls="collapse-<?=$course_section->cs_id?>"><i class="indicator ti-minus"></i>
														<?php } else { ?>
															<a class="collapsed" data-toggle="collapse" href="#collapse-<?=$course_section->cs_id?>" aria-expanded="false" aria-controls="collapse-<?=$course_section->cs_id?>">
															<i class="indicator ti-plus"></i>

														<?php } ?>
														<?=$course_section->section_title?></a>
													</h5>
												</div>
												<!-- .card-header ends -->

												<?php if($first_child) { ?>
												<div id="collapse-<?=$course_section->cs_id?>" class="collapse show" role="tabpanel" aria-labelledby="heading-<?=$course_section->cs_id?>" data-parent="#accordion_lessons">
												<?php } else { ?>
												<div id="collapse-<?=$course_section->cs_id?>" class="collapse" role="tabpanel" aria-labelledby="heading-<?=$course_section->cs_id?>" data-parent="#accordion_lessons">
												<?php } ?>	
													<div class="card-body">
														<div>
															<?=$course_section->section_description?>
														</div>
														
														<div class="list_lessons">
															<ul>
															<?php if($course_section->section_attachment) { ?>
																<li><a href="<?=base_url($course_section->section_attachment)?>" target="_blank">
																	<?php
																	switch ($course_section->section_file_type) {
																		case 'Documents':
																			echo "<i class='icon-doc-text-inv'></i>";
																			break;
																		case 'Video':
																			echo "<i class='icon-video'></i>";
																			break;
																		case 'Image':
																			echo "<i class='icon-picture'></i>";
																			break;
																		default:
																			echo "<i class='icon-attach'></i>";
																			break;
																	}

																	?>
																Attachment</a></li>
															<?php } ?>

															<?php if($course_section->section_youtube) { ?>
																<li><a href="<?=$course_section->section_youtube?>" target="_blank"><i class="icon-youtube"></i> Video</a></li>
															<?php } ?>

															<?php if($course_section->section_other) { ?>
																<li><a href="<?=$course_section->section_other?>" target="_blank"><i class="icon-attach"></i> Other</a></li>
															<?php } ?>
																	
															</ul>
														</div> <!-- end of .list_lessons -->
														
													</div>
												</div>
											</div>
											<!-- /card -->
											<?php 
											$first_child = false;
										} ?>
											
									</div>
										<!-- /accordion --> 
								</div>
						  	</div> <!-- end of #lessons -->


						</div> <!-- end of .tab-content -->
					</div><!-- end of .col-lg-8 -->
					<aside class="col-lg-4 margin_60_35" id="sidebar">
						<div class="box_detail">
							<h3 class="title"><?=$course->course_title?></h3>
							<p><i class="icon_clock_alt"></i> <?=$course->course_duration?></p>
							<figure>							
									<?php if ($course->course_banner) { ?>
										<img src="<?=base_url($course->course_banner)?>" alt="" class="img-fluid">
									<?php } else {?>
										<img src="<?=base_url('site-assets/img/no-image.jpg')?>" alt="" class="img-fluid">
									<?php } ?>
									
							</figure>
													
						</div>
					</aside>
				</div>
			</div>
			
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php 
	$this->load->view('site/english/includes/footer'); 
	?>
	</div>
	<!-- page -->
	
	<?php 
	$this->load->view('site/english/includes/scripts'); 
	?>
	
  
</body>
</html>