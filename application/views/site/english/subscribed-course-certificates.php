﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/english/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/english/includes/header'); 
	?>
	
	<main>
		

		<div class="bg_color_1">

			<nav class="subscribed-menu  sticky_horizontal">
				<div class="container">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
					  	<li class="nav-item">
					    	<a class="nav-link" id="home-tab" href="<?=base_url('en/subscriptions/details/') . $course->course_id ?>">Details</a>
					  	</li>

					  	<li class="nav-item">
					    	<a class="nav-link" id="classes-tab" href="<?=base_url('en/subscriptions/classes/') . $course->course_id ?>">Classes</a>
					  	</li>
					  	
					  	<li class="nav-item">
					    	<a class="nav-link active" id="contact-tab" href="<?=base_url('en/subscriptions/exams/') . $course->course_id ?>">Exams</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="contact-tab" href="<?=base_url('en/subscriptions/attendance/') . $course->course_id ?>">Attendance</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="trainers-tab" href="<?=base_url('en/subscriptions/trainers/') . $course->course_id ?>">Trainers</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('en/subscriptions/assignments/') . $course->course_id ?>">Assignments</a>
					  	</li>
					  	<li class="nav-item">
					    	<a class="nav-link" id="assignments-tab" href="<?=base_url('en/subscriptions/certificates/') . $course->course_id ?>">Certificates</a>
					  	</li>
					</ul>
				</div>
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="tab-content" id="myTabContent">

						  	<div class="tab-pane fade show active" id="exams" role="tabpanel" aria-labelledby="exams-tab">
						  		<div class="container margin_60_35">
						  			<div class="intro_title">
										<h2>Certificates</h2>
										<ul>
												<?php if (count($certificates) > 1) { ?>
													<li><?=count($certificates)?> Certificates</li>
												<?php }else { ?>
													<li><?=count($certificates)?> Certificate</li>
												<?php } ?>
											
										</ul>
									</div>
						  			<div id="accordion_exams_upcoming" role="tablist" class="add_bottom_45">
										<?php 
										$i=1;
										foreach ($certificates as $certificate) { ?>
											<div class="card">
												<div class="card-header" role="tab" id="up-exam-title-<?=$i?>">
													<h5 class="mb-0">
															<?=$certificate->title?>
															<span class="float-right">	<a href="<?= base_url('en/subscriptions/certificate_download/' . $certificate->sc_id) ?>" target="_blank" class="confirm-button btn btn-primary btn-border-theme float-lg-right">Download Certificate</a></span>
													</h5>
												</div>
											</div>
											<?php 
											$i++;
										} ?>
									</div>

						  		</div>
						  	</div> <!-- end of #exams -->							


						</div> <!-- end of .tab-content -->
					</div><!-- end of .col-lg-8 -->
					<aside class="col-lg-4 margin_60_35" id="sidebar">
						<div class="box_detail">
							<h3 class="title"><?=$course->course_title?></h3>
							<p><i class="icon_clock_alt"></i> <?=$course->course_duration?></p>
							<figure>							
									<?php if ($course->course_banner) { ?>
										<img src="<?=base_url($course->course_banner)?>" alt="" class="img-fluid">
									<?php } else {?>
										<img src="<?=base_url('site-assets/img/no-image.jpg')?>" alt="" class="img-fluid">
									<?php } ?>
									
							</figure>
													
						</div>
					</aside>
				</div>
			</div>
			
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php 
	$this->load->view('site/english/includes/footer'); 
	?>
	</div>
	<!-- page -->
	
	<?php 
	$this->load->view('site/english/includes/scripts'); 
	?>
	
  
</body>
</html>