<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/english/includes/styles');
    ?>  

    <!-- SPECIFIC CSS -->
    <link href="<?=base_url('site-assets/css/blog.css')?>" rel="stylesheet">


</head>

<body>
	
	<div id="page">
		
	<?php $this->load->view('site/english/includes/header'); ?>
	
	<main>
		<?php if(!isStudent()) { ?>
			<section id="hero_in" class="general">
				<div class="wrapper">
					<div class="container">
						<h1 class="fadeInUp"><span></span><?=$event->title?></h1>
					</div>
				</div>
			</section>
			<!--/hero_in-->
		<?php } ?>

		<div class="container margin_60_35">
			<?php if(isStudent()) { ?>
				<h3 class="no-sidebar-page-title"><?=$event->title?></h3>
			<?php }	?>		
			<div class="row">	
				<div class="col-lg-9">
					<div class="row no-gutters event single-detail">
									
						<div class="col-lg-12">
							<figure style="background-image: url(<?=base_url($event->image)?>)">								
								<figcaption><strong><?=date('d', strtotime($event->start_time))?></strong><?=date('M', strtotime($event->start_time))?></figcaption>
							</figure>
						</div>
						<div class="col-lg-12 ">
							<div class="event-details">
								<ul>
									<li><?=date('d.m.Y', strtotime($event->start_time))?>
													
									<?php if(date('h:i a', strtotime($event->start_time))) { echo  date(', h:i a', strtotime($event->start_time));}?>
														
									</li>
									<li>
									<?php if($event->end_time != '0000-00-00 00:00:00') {
										echo " - ". date(' d.m.Y, h:i a', strtotime($event->end_time));

									}?>
													
									</li>
								</ul>
								<?php /*<h4><?=$event->title?></h4>*/?>
								<p><?=$event->description?></p>
							</div>
						</div>
									
					</div>
				</div>

				<aside class="col-lg-3">
					<?php if($other_events) { ?>
					<div class="widget">
						<div class="widget-title">
							<h4>Other Events</h4>
						</div>
						<ul class="comments-list">
							<?php foreach ($other_events as $other_event) { ?>				
								<li>
									<div class="alignleft">
										<a href="<?=base_url('en/events/details/').$other_event->event_id?>"><img src="<?=base_url($other_event->image)?>" alt="Image"></a>
									</div>
									<small><?=date('d.m.Y', strtotime($other_event->start_time))?></small>
									<h3><a href="<?=base_url('en/events/details/').$other_event->event_id?>" title=""><?=$other_event->title?></a></h3>
								</li>
							<?php } ?>
						</ul>
					</div>
					<!-- /widget -->
					<?php } ?>
					
				</aside>
			</div>
				
		</div>
		<!-- /container -->
	</main>
	<!--/main-->
	
	<?php $this->load->view('site/english/includes/footer'); ?>
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/english/includes/scripts'); ?>

</body>
</html>