﻿<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >
    
    <?php 
    $this->load->view('site/includes/styles.php');
    ?>  

</head>

<body>
	
	<div id="page" class="theia-exception">
		
	<?php 
	$this->load->view('site/includes/header.php'); 
	?>
	
	<main>
		<?php if(!isStudent()) { ?>
			<section id="hero_in" class="courses">
				<div class="wrapper">
					<div class="container">
						<h1 class="fadeInUp"><span></span><?=$course->course_title?></h1>
					</div>
				</div>
			</section>
			<!--/hero_in-->
		<?php } ?>

		<div class="bg_color_1">
			<nav class="subscribed-menu sticky_horizontal">
				<div class="container">
					
					<ul class="nav nav-tabs" id="myTab" role="tablist">
					  	<li class="nav-item">
					    	<a class="nav-link active" href="<?=base_url('courses/details/') . $course->course_id ?>">Details</a>
					  	</li>					  	
					  	<li class="nav-item">
					    	<a class="nav-link" href="<?=base_url('courses/trainers/') . $course->course_id ?>">Trainers</a>
					  	</li>

					  	<li class="nav-item">
					    	<a class="nav-link" href="<?=base_url('courses/calendar/') . $course->course_id ?>">Calendar</a>
					  	</li>
					  	
					</ul>
				</div>
			</nav>
			<div class="container ">
				<div class="row">
					<div class="col-lg-8">
						
						<div class="container margin_60_35">
							<div class="intro_title">
								<h2>Brief Description</h2>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<?=$course->course_description?>
								</div>
							</div>
							<!-- /row -->
						</div>
							

						<?php if($sections_count->scount) { ?>
						<div class="container margin_60_35">
							<div class="intro_title">
								<h2>Details</h2>
								<ul>
									<li><?=$sections_count->scount?> lessons</li>
										
								</ul>
							</div>
							<div id="accordion_lessons" role="tablist" class="add_bottom_45">
								<?php 
								$first_child = true;
								foreach ($course_sections as $course_section) { ?>
									<div class="card">
									<!-- .card-header starts -->
										<div class="card-header" role="tab" id="heading-<?=$course_section->cs_id?>">
											<h5 class="mb-0">
												<?php if($first_child) { ?>
													<a data-toggle="collapse" href="#collapse-<?=$course_section->cs_id?>" aria-expanded="true" aria-controls="collapse-<?=$course_section->cs_id?>"><i class="indicator ti-minus"></i>
												<?php } else { ?>
													<a class="collapsed" data-toggle="collapse" href="#collapse-<?=$course_section->cs_id?>" aria-expanded="false" aria-controls="collapse-<?=$course_section->cs_id?>">
														<i class="indicator ti-plus"></i>

												<?php } ?>
												<?=$course_section->section_title?></a>
											</h5>
										</div>
										<!-- .card-header ends -->

										<?php if($first_child) { ?>
										<div id="collapse-<?=$course_section->cs_id?>" class="collapse show" role="tabpanel" aria-labelledby="heading-<?=$course_section->cs_id?>" data-parent="#accordion_lessons">
										<?php } else { ?>
										<div id="collapse-<?=$course_section->cs_id?>" class="collapse" role="tabpanel" aria-labelledby="heading-<?=$course_section->cs_id?>" data-parent="#accordion_lessons">
										<?php } ?>	
											<div class="card-body">
												<div>
													<?=$course_section->section_description?>
												</div>
													
											</div>
										</div>
									</div>
									<!-- /card -->
									<?php 
									$first_child = false;
								} ?>
										
							</div>
							<!-- /accordion --> 
						</div>						
						<?php } ?>
					</div>
					<!-- /col -->
					
					<aside class="col-lg-4" id="sidebar">
						<div class="box_detail">
							
							<h3 class="title"><?=$course->course_title?></h3>
							<p><i class="icon_clock_alt"></i> <?=$course->course_duration?></p>
							
							<figure>								
									<?php if ($course->course_banner) { ?>
										<img src="<?=base_url($course->course_banner)?>" alt="" class="img-fluid">
									<?php } else {?>
										<img src="<?=base_url('site-assets/img/no-image.jpg')?>" alt="" class="img-fluid">
									<?php } ?>
									
							</figure>
							<div class="price">
								<?=$course->course_sale_price?> BHD							
							</div>
							<?php if(isStudent()) { 
								if(is_in_array($courseIDs, 'course_id', $course->course_id)) { ?>
									<a href="<?=base_url('subscriptions/details/'. $course->course_id)?>">Subscribed!</a>
								<?php } else { 
									if($course->sequence_id == 0) { ?>
										<a href="<?=base_url('student/subscribe/'. $course->course_id)?>" class="btn_1 full-width">Subscribe</a>
									<?php } else {
										if(is_in_array($courseIDs, 'course_id', $course->sequence_id)) { ?>
											<a href="<?=base_url('student/subscribe/'. $course->course_id)?>" class="btn_1 full-width">Subscribe</a>
										<?php }	else {?>	
											<p>You can subscribe this course only after subscribing the previous course<br>
												<a href="<?=base_url('courses/details/'. $course->sequence_id)?>">View Previous  Course</a></p>
										<?php } 
									}
								}	
							} else { ?>
								<a href="<?=base_url('student/subscribe/'. $course->course_id)?>" class="btn_1 full-width">Subscribe</a>
							<?php } ?>
							
							
							
						</div>
					</aside>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php 
	$this->load->view('site/includes/footer.php'); 
	?>
	</div>
	<!-- page -->
	
	<?php 
	$this->load->view('site/includes/scripts.php'); 
	?>
	
  
</body>
</html>