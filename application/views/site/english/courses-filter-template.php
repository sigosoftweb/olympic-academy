﻿

				<?php foreach($courses as $course) { ?>
				<div class="col-xl-4 col-lg-6 col-md-6">
					<div class="box_grid wow courses">
						<figure class="block-reveal">
							<div class="block-horizzontal"></div>
								
								<?php if($course->course_banner) { ?>
									<?php /*<img src="<?=base_url($course->course_banner)?>" class="img-fluid" alt="">*/?>
									<div class="banner" style="background-image: url(<?=base_url($course->course_banner)?>);"></div>
								<?php } else { ?>
									<?php /*<img src="<?=base_url('site-assets/img/no-image.jpg')?>" class="img-fluid" alt="">*/ ?>
									<div class="banner" style="background-image: url(<?=base_url('site-assets/img/no-image.jpg')?>);"></div>
								<?php }?>
							
							<div class="price"><p>Individual</p><?=$course->course_sale_price?> BHD</div>
							<?php /*<div class="price tamkeen"><p>Tamkeen</p><?=$course->course_price?> BHD</div>*/?>
						</figure>
						<div class="wrapper">
							<small><?=$course->cat_name?></small>
							<h3><?=$course->course_title?></h3>
							<p><?=getExcerpt($course->course_description)?></p>
							<?php /*<div class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></div> */?>
						</div>
						<ul>
							<li><i class="icon_clock_alt"></i> <?=$course->course_duration?></li>
							<?php if(isStudent()) {
								
								//print_r($courseIDs);
								//echo "id=" . $course->course_id;
								if(!empty($courseIDs) && is_in_array($courseIDs, 'course_id', $course->course_id)) { ?>
									<li><a href="<?=base_url('en/subscriptions/details/'. $course->course_id)?>">Subscribed!</a></li>
								<?php } else { 
									if($course->sequence_id == 0) { ?>
										<?php if($course->students_count < $course->student_limit) { ?>
											<li><a href="<?=base_url('en/courses/details/'. $course->course_id)?>">Enroll now</a></li>
										<?php } else { ?>
											<li><a href="<?=base_url('en/courses/details/'. $course->course_id)?>">Enroll Now</a></li>
										<?php } ?>
										
									<?php } else {
										if(!empty($courseIDs) && is_in_array($courseIDs, 'course_id', $course->sequence_id)) { ?>
											<li><a href="<?=base_url('en/courses/details/'. $course->course_id)?>">Enroll now</a></li>
										<?php }	else {?>	
											<li><a href="<?=base_url('en/courses/details/'. $course->course_id)?>">Enroll Now</a></li>
										<?php } 
									}
								}									
							
							} else {?>
								<?php if($course->students_count < $course->student_limit) { ?>
									<li><a href="<?=base_url('en/courses/details/'. $course->course_id)?>">Enroll now</a></li>
								<?php } else { ?>
									<li><a href="<?=base_url('en/courses/details/'. $course->course_id)?>">Seats Filled</a></li>
								<?php } ?>
							<?php } ?>

							
						</ul>
					</div>
				</div>
				<!-- /box_grid -->
				<?php } ?>

			