﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/english/includes/styles.php');
    ?>     

</head>

<body id="login_bg">
	
	<nav id="menu" class="fake_menu"></nav>
	
	<div id="preloader">
		<div data-loader="circle-side"></div>
	</div>
	<!-- End Preload -->
	
	<div id="login">
		<aside>
			<figure class="logo">
					<a href="<?=base_url()?>">
						<img src="<?=base_url('site-assets/img/olympic.PNG')?>" width="75"  data-retina="true" alt="">	
					</a>
			</figure>
			  <form id="login-form"  autocomplete="off" method="post" action="<?=base_url('en/login/checkStudent')?>">
				
				<div class="form-group">
					<span class="input">
					<input class="input_field" type="text" autocomplete="off" name="username"  required>
						<label class="input_label">
						<span class="input__label-content">Email / Mobile</span>
					</label>
					</span>

					<span class="input">
					<input class="input_field" type="password"  autocomplete="off" name="password" required>
						<label class="input_label">
						<span class="input__label-content">Password</span>
					</label>
					</span>
					<small><a href="<?= base_url('en/login/forgotpassword') ?>">Forgot password?</a></small>
				</div>
				<button type="submit" class="btn_1 rounded full-width add_top_60">Login</button>
				<div class="text-center add_top_10">New to Olympic Academy? <strong><a href="<?=base_url('en/register')?>">Sign up!</a></strong></div>
			</form>
			<div class="copy">© 2020 Olympic Academy</div>
		</aside>
	</div>
	<!-- /login -->
		
	<?php $this->load->view('site/english/includes/scripts.php'); ?>
  
</body>
</html>