﻿

<!DOCTYPE html>
<html lang="en">

<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >
    
    <?php 
    $this->load->view('site/english/includes/styles');
    ?>  

</head>
<body>
	
	<div id="page">
		
	<?php $this->load->view('site/english/includes/header'); ?>
	
	<main>
		<section id="hero_in" class="general">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>About Us</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		
			<div class="container margin_120_95">
				<div class="main_title_2">
					<span><em></em></span>
					<h2>Bahrain Olympic Academy</h2>
				</div>
				<div class="row text-center">
					<div class="col-lg-12"> 
						<p>The Bahrain Olympic Academy is considered as the next step forward as well as an upgrade of the former "<strong>Bahrain Sports Institute</strong>", which was established back in 1981 as an initiative of His Majesty King Hamad bin Isa Al Khalifa, the king of the Kingdom of Bahrain. At that time, his Majesty was the Crown Prince and Chairman of the Supreme Council for Youth and Sports. The Bahrain Sports Institute was later renamed to "<strong>The Sports Development Center</strong>".  In 1990, the name changed to "<strong>Sports Training & Development Centre</strong>", and in 2017 it became the "<strong>Bahrain Olympic Academy</strong>".</p>

						<p>The scientific status that was created and developed in the Bahrain Olympic Academy, with its outstanding performance, was the platform it worked with since 2000. The Bahrain Olympic Academy continued to select many high-level training programs, and worked on creating elite and distinguished sports cadres. It worked on the development of many certification programs that contributed to the development and preparation of coaches, athletes and sports leaders. It is the  window of opportunity to all levels of the sports organizational and cultural entities, and the aim is to spread the sport culture and raise performance in all sectors. To promote the level of sport in the Kingdom of Bahrain and commensurate with the economic vision 2030 to make the Kingdom of Bahrain in the ranks of the world countries at the level of sports.</p>
					</div>
				</div>
			</div>
			
		

		<div class="bg_color_3">
		<div class="container margin_120_95">
			<div class="main_title_2">
				<span><em></em></span>
				<h2>Why choose Us</h2>
				<p></p>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<a class="box_feat" href="#">
						<i class="pe-7s-diamond"></i>
						<h3>Qualified teachers</h3>
						<p></p>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="box_feat" href="#">
						<i class="pe-7s-display2"></i>
						<h3>Equiped class rooms</h3>
						<p></p>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="box_feat" href="#">
						<i class="pe-7s-science"></i>
						<h3>Advanced teaching</h3>
						<p></p>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="box_feat" href="#">
						<i class="pe-7s-rocket"></i>
						<h3>Adavanced study plans</h3>
						<p></p>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="box_feat" href="#">
						<i class="pe-7s-target"></i>
						<h3>Focus on target</h3>
						<p></p>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="box_feat" href="#">
						<i class="pe-7s-graph1"></i>
						<h3>focus on success</h3>
						<p></p>
					</a>
				</div>
			</div>
			<!--/row-->
		</div>
		<!-- /container -->
		</div>

		
	</main>
	<!--/main-->
	
	<?php $this->load->view('site/english/includes/footer'); ?>
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/english/includes/scripts'); ?>
	
</body>
</html>