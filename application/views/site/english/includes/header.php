﻿	<?php

	if (isset($this->session->userdata['olympicUser'])) {
	    $user = $this->session->userdata['olympicUser'];
	    if(isset($user)) {
	    	$userid = $user['userID'];
	    	$username = $user['userName'];
	    	//echo $username;
	    	//exit;
	    }
	}
	?>
	<header class="header menu_2 sticky">
		<div id="preloader"><div data-loader="circle-side"></div></div><!-- /Preload -->
		<div id="logo">
			<a href="<?=base_url()?>">
				<?php /* <img src="<?=base_url()?>site-assets/img/logo.png" width="150" height="45" data-retina="true" alt=""> */?>
				<img src="<?=base_url()?>site-assets/img/olympic.PNG" width="75" alt="Logo"/>
				
			</a>
		</div>
		<ul id="top_menu" class="<?php if(isset($user)) { echo 'logged'; }?>">
			<li>
				<?php if(isset($user)) { ?>
					<?php /*<a href="<?=base_url('student/profile')?>"><i class="ti-user"></i> <span class="hidden_tablet"><?=$user['userName']?></span></a>*/?>
					<div class="user dropdown show">
					  	<a class="btn dropdown-toggle" href="#" role="button" id="user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    <i class="ti-user"></i> <span class="hidden_tablet"><?=$user['userName']?></span>
					  	</a>

					  	<div class="dropdown-menu" aria-labelledby="user-dropdown">
					    	<a class="dropdown-item" href="<?=base_url('en/student/profile')?>">My Account</a>
					    	<div class="dropdown-divider"></div>							
								
					    	<a class="dropdown-item" href="<?=base_url('en/login/logout')?>">Logout</a>
					  	</div>
					</div> 
				<?php } else { ?>
					<a href="<?=base_url('en/login')?>" class="btn_1 rounded">
						<i class="ti-lock mr-1"></i>Login
					</a>
				<?php } ?>
				
			</li>

			<li><a href="#0" class="search-overlay-menu-btn">Search</a></li>
			
		</ul>
		<!-- /top_menu -->
		<a href="#menu" class="btn_mobile">
			<div class="hamburger hamburger--spin" id="hamburger">
				<div class="hamburger-box">
					<div class="hamburger-inner"></div>
				</div>
			</div>
		</a>
		<nav id="menu" class="main-menu">
			<ul>
				<?php if(isStudent()) { 				
					
				} ?>	
				
				<li><span><a href="<?=base_url()?>">Home</a></span></li>
				<li><span><a href="#">About</a></span>
					<ul>
						<li><a href="<?=base_url('en/about')?>">About BOA</a></li>
						<li><a href="<?=base_url('en/about/team')?>">Our Team</a></li>
					</ul>
				</li>
				<li><span><a href="<?=base_url('en/courses')?>">Courses</a></span></li>
				<li><span><a href="<?=base_url('en/trainers')?>">Trainers</a></span></li>
				<li><span><a href="<?=base_url('en/calendar')?>">Calendar</a></span></li>	
				<li><span><a href="<?=base_url('en/courses/resources')?>">Resources</a></span></li>	
				<li><span><a href="<?=base_url('en/contact')?>">Contact</a></span></li>

				
				
			</ul>
		</nav>
		<!-- Search Menu -->
		<div class="search-overlay-menu">
			<span class="search-overlay-close"><span class="closebt"><i class="ti-close"></i></span></span>
			<form role="search" id="searchform" method="get" action="<?=base_url('en/search') ?>">
				<input value="" name="q" type="search" placeholder="Search...">
				<button type="submit"><i class="icon_search"></i>
				</button>
			</form>
		</div><!-- End Search Menu -->

		<div class="site-language">
			<select class="selectbox" id="select-language">
				<option value="Arabic">Arabic</option>
				<option value="English" selected>English</option>					
			</select>
		</div>
	</header>
	<!-- /header -->
	
