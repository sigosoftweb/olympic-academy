﻿	<?php
	$ci = & get_instance();
    $ci->load->model('site/english/Model_Contact');
    $contact = $ci->Model_Contact->get_contact();
    
    ?>

	<footer>
		<div class="container pt-5 pb-4">
			<?php if(!isStudent()) { ?>
			<div class="row">
				<div class="col-lg-5 col-md-12 p-r-5">
					<p class="logo">
						<img src="<?=base_url()?>site-assets/img/olympic.PNG" width="149" height="auto" data-retina="true" alt="">						
					</p>
					<p>The Bahrain Olympic Academy is considered as the next step forward as well as an upgrade of the former "Bahrain Sports Institute", which was established back in 1981 as an initiative of His Majesty King Hamad bin Isa Al Khalifa, the king of the Kingdom of Bahrain.</p>
					<div class="follow_us">
						<ul>
							<li>Follow us</li>
							<li><a href="#0"><i class="ti-facebook"></i></a></li>
							<li><a href="#0"><i class="ti-twitter-alt"></i></a></li>
							<li><a href="#0"><i class="ti-google"></i></a></li>
							<li><a href="#0"><i class="ti-pinterest"></i></a></li>
							<li><a href="#0"><i class="ti-instagram"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 ml-lg-auto">
					<h5>Useful links</h5>
					<ul class="links">
						<li><a href="<?=base_url('en/about')?>">About</a></li>
						<li><a href="<?=base_url('en/courses')?>">Courses</a></li>
						<li><a href="<?=base_url('en/trainers')?>">Our Trainers</a></li>
						<li><a href="<?=base_url('en/events')?>">Events</a></li>
						<li><a href="<?=base_url('en/calendar')?>">Calendar</a></li>
						<li><a href="<?=base_url('en/courses/resources')?>">Resources</a></li>
						<li><a href="<?=base_url('en/login')?>">Login</a></li>
						<li><a href="<?=base_url('en/register')?>">Register</a></li>
						<li><a href="<?=base_url('en/teacher')?>">Trainers Login</a></li>
						<li><a href="<?=base_url('en/contact')?>">Contact Us</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-6">
					<h5>Contact Us</h5>
					<ul class="contacts">
						<li><i class="pe-7s-map-marker" style="margin-right: 10px;"></i> <?=$contact->address?></li>
						<li><a href="tel://+973 34477598"><i class="ti-mobile"></i> <?= $contact->mobile?></a></li>
						<li><a href="mailto:info@olympicacademy.com"><i class="ti-email"></i> <?= $contact->email ?></a></li>
					</ul>
					
				</div>
			</div>
			<!--/row-->
			<hr>
		<?php } ?>
			<div class="row">
				<div class="col-md-8">
					<ul id="additional_links">
						<li><a href="#0">Terms and Conditions</a></li>
					</ul>
				</div>
				<div class="col-md-4">
					<div id="copy">© 2020 Olympic Academy</div>
				</div>
			</div>
		</div>
	</footer>
	<!--/footer-->
