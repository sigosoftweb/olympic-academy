﻿    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php /* <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" > */ ?>

    <?php /*<link href="<?=base_url()?>site-assets/css/font.css?family=Poppins:300,400,500,600,700,800" rel="stylesheet"/>*/?>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link href="<?=base_url()?>site-assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?=base_url()?>site-assets/css/style.css" rel="stylesheet"/>
    <link href="<?=base_url()?>site-assets/css/vendors.css" rel="stylesheet"/>
    <link href="<?=base_url()?>site-assets/css/icon_fonts/css/all_icons.min.css" rel="stylesheet"/>
    <?php /*<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" > */?>
    <link href="<?=base_url()?>site-assets/plugins/toaster/toaster.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>site-assets/css/custom.css" rel="stylesheet"/>
