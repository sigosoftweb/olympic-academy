﻿<!DOCTYPE html>
<html lang="en">

<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >
    
    <?php 
    $this->load->view('site/english/includes/styles');
    ?>  

</head>

<body>
	
	<div id="page">
		
	<?php $this->load->view('site/english/includes/header'); ?>
	
	<main>
		<section id="hero_in" class="contacts">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Contact Us</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->

		

		<div class="bg_color_1">
			<div class="container margin_70_70">
				<div class="row text-center">
					
					<div class="col-lg-12 thank-you">
						<h3><i class="pe-7s-check"></i></h3>
						<h4>Thank You!</h4>
						<p>Thank you for mailing us. Our executive will get back to you soon.</p>
						
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->
	</main>
	<!--/main-->
	
	<?php $this->load->view('site/english/includes/footer'); ?>
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/english/includes/scripts'); ?>

	
	
</body>
</html>