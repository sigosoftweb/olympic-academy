<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OLYMPIC ACADEMY</title>
    <link rel="shortcut icon" href="<?=base_url()?>site-assets/img/favicon.png" >

    <?php 
    $this->load->view('site/english/includes/styles');
    ?>  

    

</head>

<body>
	
	<div id="page">
		
	<?php $this->load->view('site/english/includes/header'); ?>
	
	<main>
		<?php if(!isStudent()) { ?>

			<section id="hero_in" class="general">
				<div class="wrapper">
					<div class="container">
						<h1 class="fadeInUp"><span></span>Events</h1>
					</div>
				</div>
			</section>
			<!--/hero_in-->
		<?php } ?>

		<div class="container margin_60_35">			
			<?php if(isStudent()) { ?>
				<h3 class="no-sidebar-page-title">Events</h3>
			<?php }	?>	

			<?php 
			if(!empty($events)) {
				foreach ($events as $event) { ?>							
					<div class="row event no-gutters">
									
						<div class="col-lg-4">
							<figure style="background-image: url(<?=base_url($event->image)?>)">									
								<figcaption><strong><?=date('d', strtotime($event->start_time))?></strong><?=date('M', strtotime($event->start_time))?></figcaption>
							</figure>
						</div>
						<div class="col-lg-8 ">
							<div class="event-details">
								<ul>
									<li><?=date('d.m.Y', strtotime($event->start_time))?>
													
									<?php if(date('h:i a', strtotime($event->start_time))) { echo  date(', h:i a', strtotime($event->start_time));}?>
														
									</li>
									<li>
									<?php if($event->end_time != '0000-00-00 00:00:00') {
										echo " - ". date(' d.m.Y, h:i a', strtotime($event->end_time));

									}?>
													
									</li>
								</ul>
								<h4><?=$event->title?></h4>
								<p><?=getExcerpt($event->description, 0, 250)?></p>
								<a href="<?=base_url('en/events/details/') . $event->event_id?>" class="btn-purple-rounded float-right">View Event</a>
							</div>
						</div>
									
					</div>
				<?php } 
			} else {?>
				<div class="text-center margin_60_35 ">
					<figure><img src="<?=base_url()?>site-assets/img/wizard_intro_icon.svg" alt=""></figure>
					<h3 class="pb-5">No new events!</h3>
				</div>
			<?php }?>					
				
		</div>
		<!-- /container -->
	</main>
	<!--/main-->
	
	<?php $this->load->view('site/english/includes/footer'); ?>
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
    <?php $this->load->view('site/english/includes/scripts'); ?>

</body>
</html>