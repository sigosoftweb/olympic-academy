<!DOCTYPE html>
<html>
<head>
    <title>Olympic | Page 404</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8" />
    <link rel="icon" type="image/ico" href="<?=base_url()?>assets/images/favicon.ico" />
    <link href="<?=base_url()?>assets/css/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/vendor/bootstrap-checkbox.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/vendor/bootstrap/bootstrap-dropdown-multilevel.css">
    <link href="<?=base_url()?>assets/css/minimal.css" rel="stylesheet">
  </head>
  <body class="bg-solid-6">
    <div id="wrap">
      <div class="row">
        <div id="content" class="col-md-12 full-page error">
          <div class="search">
            <input type="text" class="form-control" placeholder="Search...">
          </div>
          <div class="inside-block">
            <img src="<?=base_url()?>assets/images/logo-big.png" alt class="logo">
            <h1 class="error">Error <strong>404</strong></h1>
            <p class="lead"><span class="overline">something's</span> not right here</p>
            <p>the page you are looking for cannot be found</p>
            <div class="controls">
              <button class="btn btn-cyan"><i class="fa fa-refresh"></i> Try Again</button>
              <button class="btn btn-greensea"><i class="fa fa-home"></i> Return to home</button>
            </div>
          </div>
        </div>
      </div>
    </div>
   </body>
</html>
