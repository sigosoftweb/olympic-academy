<!DOCTYPE html>
<html>
   <head>
    <title><?=$title?></title>
	  <?php $this->load->view('subadmin/includes/includes.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('subadmin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-university" style="line-height: 48px;padding-left: 2px;"></i> Organizations</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Organizations</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                    <div class="col-md-12">
						         <section class="tile transparent">
		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">
                           <?php if($priority->add=='1') {?>
								             <button type="button" class="btn btn-default btn-lg margin-bottom-20" data-toggle="modal" data-target="#add-organization">Add organization</button>
                           <?php };?>   
		                     </div>
		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom" id="datatable">
								               <thead>
  		                           <tr>
  		                             <th width="40%">Organization</th>
  		                             <th width="5%">Status</th>
  		                             <?php if($priority->edit=='1') {?> 
                                    <th width="5%">Edit</th>
                                   <?php };?> 
  		                           </tr>
  		                         </thead>
  		                         <tbody>
              									 <?php foreach ($organizations as $organization) { ?>
              									 	<tr>
              									 		<td><?=$organization->organization?></td>
              											<td>
              												<?php if ($organization->status) { ?>
              													Active
                                        <?php if($priority->block=='1') {?>
                                          <br>
                													<a href="<?=site_url('subadmin/organizations/status/' . $organization->org_id . '/0')?>" class="btn btn-danger btn-xs margin-bottom-20">Block</a>
              												  <?php } }else { ?>
              													Blocked
                                        <?php if($priority->block=='1') {?><br>
              													<a href="<?=site_url('subadmin/organizations/status/' . $organization->org_id . '/1')?>" class="btn btn-success btn-xs margin-bottom-20">Activate</a>
              												<?php } } ?>
              											</td>
                                    <?php if($priority->edit=='1') {?> 
              											   <td><button type="button" class="btn btn-primary btn-xs margin-bottom-20" onclick="edit(<?=$organization->org_id?>)">Edit</button></td>
                                    <?php };?>   
              									 	</tr>
              									 <?php } ?>
  		                         </tbody>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <div class="modal fade" id="add-organization" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
              <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> organization</h3>
            </div>
            <div class="modal-body">
              <form role="form" id="add-form" action="<?=site_url('subadmin/organizations/add')?>" method="post">

                <div class="form-group">
                  <label for="placeholderInput">organization</label>
                  <input type="text" name="organization" class="form-control" placeholder="organization name..." onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
                </div>

            </div>
            <div class="modal-footer">
              <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
              <button type="submit" class="btn btn-green" id="submit-button">Save changes</button>
            </div>
			</form>
          </div>
        </div>
      </div>
	  <div class="modal fade" id="edit-organization" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
              <h3 class="modal-title" id="modalConfirmLabel"><strong>Edit</strong> organization</h3>
            </div>
            <div class="modal-body">
              <form role="form" id="edit-form" action="<?=site_url('subadmin/organizations/edit')?>" method="post">
				  <input type="hidden" name="org_id" id="org_id">
                <div class="form-group">
                  <label for="placeholderInput">organization</label>
                  <input type="text" name="organization" class="form-control" id="organization" placeholder="organization name..." onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
                </div>

            </div>
            <div class="modal-footer">
              <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
              <button type="submit" class="btn btn-green" id="submit-button-edit">Update</button>
            </div>
			</form>
          </div>
        </div>
      </div>
      <section class="videocontent" id="video"></section>
      <?php $this->load->view('subadmin/includes/scripts.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-script.php'); ?>
	  <script>
	  	$('#add-form').on('submit', function(e){
		  e.preventDefault();
		  $('#submit-button').attr('disabled',true);
		  document.getElementById("add-form").submit();
	  	});
		function edit(org_id)
	    {
	        $('#org_id').val(org_id);
	        $.ajax({
	          method: "POST",
	          url: "<?=site_url('subadmin/organizations/getDetails');?>",
	          data : { org_id : org_id },
	          dataType : "json",
	          success : function( data ){
	              $('#organization').val(data.organization);
	              $('#edit-organization').modal('show');
	          }
	        });
	    }
	  </script>
   </body>
</html>
