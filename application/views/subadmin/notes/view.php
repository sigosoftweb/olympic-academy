<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('subadmin/includes/includes.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-css.php'); ?>
	  <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/datepicker/css/bootstrap-datetimepicker.css">
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('subadmin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-graduation-cap" style="line-height: 48px;padding-left: 2px;"></i> Notice</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Notice</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">


		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">
								 <button type="button" class="btn btn-default btn-lg margin-bottom-20" data-toggle="modal" data-target="#add-education">Add notice</button>
		                     </div>
		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom" id="datatable">
								   <thead>
  		                           <tr>
  		                             <th width="10%">Notice</th>
									 <th width="30%">Description</th>
									 <th width="10%">Start time</th>
									 <th width="10%">End time</th>
									 <th width="10%">Type</th>
  		                             <th width="5%">Options</th>
  		                           </tr>
  		                         </thead>
  		                         <tbody>
									 <?php foreach ($notes as $note) { ?>
									 	<tr>
									 		<td><?=$note->title?></td>
											<td><?=$note->description?></td>
											<td><?=date('d/m/Y h:i A',strtotime($note->start_time))?></td>
											<td><?=date('d/m/Y h:i A',strtotime($note->end_time))?></td>
											<td><?=$note->type?></td>
											<td>
												<button type="button" class="btn btn-primary btn-xs margin-bottom-20" onclick="edit(<?=$note->note_id?>)">Edit</button>
												<a class="btn btn-danger btn-xs" href="<?=site_url('subadmin/notes/delete/' . $note->note_id . '/1')?>">Delete</a>
											</td>
									 	</tr>
									 <?php } ?>
  		                         </tbody>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <div class="modal fade" id="add-education" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
              <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> Notice</h3>
            </div>
            <div class="modal-body">
              <form role="form" id="add-form" action="<?=site_url('subadmin/notes/addNote')?>" method="post">

                <div class="form-group">
                  <label for="placeholderInput">Notice</label>
                  <input type="text" name="title" class="form-control" placeholder="Education name..." onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
                </div>
				<div class="form-group">
				  <label>Description *</label>
				  <textarea name="description" class="form-control" rows="5" cols="80" required></textarea>
				</div>
				<div class="form-group">
				  <label>Start time *</label>
				  <input type="text" class="form-control" id="datepicker" name="start_time" autocomplete="off" required>
				</div>
				<div class="form-group">
				  <label>End time *</label>
				  <input type="text" class="form-control" id="datepicker-edit" name="end_time" autocomplete="off" required>
				</div>
				<div class="form-group">
				  <label>Note type</label>
				  <select class="form-control" name="type">
					  <option value="Teacher">Teacher</option>
					  <option value="Student">Student</option>
				  </select>
				</div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
              <button type="submit" class="btn btn-green" id="submit-button">Add</button>
            </div>
			</form>
          </div>
        </div>
      </div>
	  <div class="modal fade" id="edit-education" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
              <h3 class="modal-title" id="modalConfirmLabel"><strong>Edit</strong> Notice</h3>
            </div>
            <div class="modal-body">
              <form role="form" id="edit-form" action="<?=site_url('subadmin/notes/editNote')?>" method="post">
				  <input type="hidden" name="note_id" id="note_id">
				  <div class="form-group">
                      <label for="placeholderInput">Notice</label>
                      <input type="text" name="title" id="title" class="form-control" placeholder="Education name..." onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
                  </div>
	  			  <div class="form-group">
	  				  <label>Description *</label>
	  				  <textarea name="description" id="description" class="form-control" rows="5" cols="80" required></textarea>
	  			  </div>
	  			  <div class="form-group">
	  				  <label>Start time *</label>
	  				  <input type="text" class="form-control" id="datepicker-e" name="start_time" autocomplete="off" required>
	  			  </div>
	  		      <div class="form-group">
	  				  <label>End time *</label>
	  				  <input type="text" class="form-control" id="datepicker-e-edit" name="end_time" autocomplete="off" required>
	  			  </div>
	  			  <div class="form-group">
	  				  <label>Note type</label>
	  				  <select class="form-control" name="type" id="type">
	  					  <option value="Teacher">Teacher</option>
	  					  <option value="Student">Student</option>
	  				  </select>
	  			  </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
              <button type="submit" class="btn btn-green" id="submit-button-edit">Update</button>
            </div>
			</form>
          </div>
        </div>
      </div>
      <?php $this->load->view('subadmin/includes/scripts.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-script.php'); ?>
	  <script src="<?=base_url()?>assets/js/vendor/datepicker/bootstrap-datetimepicker.min.js"></script>
	  <script>
	  $('#edit-form').on('submit', function(e){
		  e.preventDefault();
		  $('#submit-button-edit').attr('disabled',true);

		  var start_time = $('#datepicker-e').val();
		  var end_time = $('#datepicker-e-edit').val();

		  start = changeFormat(start_time);
		  end = changeFormat(end_time);
		  if (new Date(start) < new Date(end)) {
			  document.getElementById("edit-form").submit();
		  }
		  else {
			  toastr.error('Start time must be less than end time',"");
			  $('#submit-button-edit').attr('disabled',false);
		  }

	  });
	  $('#add-form').on('submit', function(e){
		  e.preventDefault();
		  $('#submit-button').attr('disabled',true);

		  var start_time = $('#datepicker').val();
		  var end_time = $('#datepicker-edit').val();

		  start = changeFormat(start_time);
		  end = changeFormat(end_time);
		  if (new Date(start) < new Date(end)) {
			  document.getElementById("add-form").submit();
		  }
		  else {
			  toastr.error('Start time must be less than end time',"");
			  $('#submit-button').attr('disabled',false);
		  }

	  });
	  function changeFormat(date)
	  {
		  arr = date.split(" ");
		  if (arr[2] == 'PM') {
			  tm = arr[1].split(":");
			  if (tm[0] == 12) {
				  hour = tm[0];
			  }
			  else {
				  hour = +12 + +tm[0];
			  }
			  time = hour + ':' + tm[1] + ':00';
		  }
		  else {
			  time = arr[1] + ':00';
		  }
		  return arr[0].split("/").reverse().join("-") + ' ' + time;
	    }
		function edit(note_id)
	    {
	        $('#note_id').val(note_id);
	        $.ajax({
	          method: "POST",
	          url: "<?=site_url('subadmin/notes/getNote');?>",
	          data : { note_id : note_id },
	          dataType : "json",
	          success : function( data ){
	              $('#title').val(data.title);
				  $('#description').val(data.description);
				  $('#datepicker-e').val(data.start_time);
				  $('#datepicker-e-edit').val(data.end_time);
				  $('#type').val(data.type);

	              $('#edit-education').modal('show');
	          }
	        });
	    }
		$(function(){
			$('#datepicker').datetimepicker({
				format:'DD/MM/YYYY hh:mm A',
			  icons: {
				time: "fa fa-clock-o",
				date: "fa fa-calendar",
				up: "fa fa-arrow-up",
				down: "fa fa-arrow-down"
			  }
			});
			$('#datepicker-edit').datetimepicker({
				format:'DD/MM/YYYY hh:mm A',
			  icons: {
				time: "fa fa-clock-o",
				date: "fa fa-calendar",
				up: "fa fa-arrow-up",
				down: "fa fa-arrow-down"
			  }
			});
			$('#datepicker-e').datetimepicker({
				format:'DD/MM/YYYY hh:mm A',
			  icons: {
				time: "fa fa-clock-o",
				date: "fa fa-calendar",
				up: "fa fa-arrow-up",
				down: "fa fa-arrow-down"
			  }
			});
			$('#datepicker-e-edit').datetimepicker({
				format:'DD/MM/YYYY hh:mm A',
			  icons: {
				time: "fa fa-clock-o",
				date: "fa fa-calendar",
				up: "fa fa-arrow-up",
				down: "fa fa-arrow-down"
			  }
			});
			$("#datepicker").on("dp.show",function (e) {
			  var newtop = $('.bootstrap-datetimepicker-widget').position().top - 45;
			  $('.bootstrap-datetimepicker-widget').css('top', newtop + 'px');
			});
		});
	  </script>
   </body>
</html>
