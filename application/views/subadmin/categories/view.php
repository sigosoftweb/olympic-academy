<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('subadmin/includes/includes.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('subadmin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-university" style="line-height: 48px;padding-left: 2px;"></i> Categories</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Categories</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">


		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">
								 <button type="button" class="btn btn-default btn-lg margin-bottom-20" data-toggle="modal" data-target="#add-category">Add category</button>
		                     </div>
		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom" id="datatable">
								   <thead>
  		                           <tr>
  		                             <th width="30%">Image</th>
  		                             <th width="30%">category</th>
  		                             <th width="5%">Status</th>
  		                             <th width="5%">Edit</th>
  		                           </tr>
  		                         </thead>
  		                         <tbody>
									 <?php foreach ($categories as $category) { ?>
									 	<tr>
									 	    <td><img src="<?=base_url() . $category->cat_image?>" height="100px"></td>
									 		<td><?=$category->cat_name?></td>
											<td>
												<?php if ($category->cat_status) { ?>
													Active<br>
													<a href="<?=site_url('subadmin/categories/status/' . $category->cat_id . '/0')?>" class="btn btn-danger btn-xs margin-bottom-20">Block</a>
												<?php }else { ?>
													Blocked<br>
													<a href="<?=site_url('subadmin/categories/status/' . $category->cat_id . '/1')?>" class="btn btn-success btn-xs margin-bottom-20">Activate</a>
												<?php } ?>
											</td>
											<td><button type="button" class="btn btn-primary btn-xs margin-bottom-20" onclick="edit(<?=$category->cat_id?>)">Edit</button></td>
									 	</tr>
									 <?php } ?>
  		                         </tbody>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <div class="modal fade" id="add-category" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
              <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> category</h3>
            </div>
            <div class="modal-body">
              <form role="form" id="add-form" action="<?=site_url('subadmin/categories/add')?>" method="post" enctype="multipart/form-data">

                <div class="form-group">
                  <label for="placeholderInput">category</label>
                  <input type="text" name="category" class="form-control" placeholder="category name..." onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
                </div>
                <div class="form-group">
                  <label for="placeholderInput">Image</label>
                  <input type="file" name="image" class="form-control" id="upload" required>
                </div>
                
            </div>
            <div class="modal-footer">
              <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
              <button type="submit" class="btn btn-green" id="submit-button">Save changes</button>
            </div>
			</form>
          </div>
        </div>
      </div>
	  <div class="modal fade" id="edit-category" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
              <h3 class="modal-title" id="modalConfirmLabel"><strong>Edit</strong> category</h3>
            </div>
            <div class="modal-body">
              <form role="form" id="edit-form" action="<?=site_url('subadmin/categories/edit')?>" method="post" enctype="multipart/form-data">
				  <input type="hidden" name="cat_id" id="cat_id">
                <div class="form-group">
                  <label for="placeholderInput">category</label>
                  <input type="text" name="category" class="form-control" id="category" placeholder="category name..." onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
                </div>
                <div class="form-group">
                  <label for="placeholderInput">Image ( Choose image if you wish to change the current image )</label>
                  <input type="file" name="image" class="form-control" id="upload-edit">
                </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
              <button type="submit" class="btn btn-green" id="submit-button-edit">Update</button>
            </div>
			</form>
          </div>
        </div>
      </div>
      <section class="videocontent" id="video"></section>
      <?php $this->load->view('subadmin/includes/scripts.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-script.php'); ?>
	  <script>
	  	$('#add-form').on('submit', function(e){
		  e.preventDefault();
		  $('#submit-button').attr('disabled',true);
		  document.getElementById("add-form").submit();
	  	});
		function edit(cat_id)
	    {
	        $('#cat_id').val(cat_id);
	        $.ajax({
	          method: "POST",
	          url: "<?=site_url('subadmin/categories/getDetails');?>",
	          data : { cat_id : cat_id },
	          dataType : "json",
	          success : function( data ){
	              $('#category').val(data.cat_name);
	              $('#edit-category').modal('show');
	          }
	        });
	    }
	    $('#upload').on('change', function () {
		  var file = $("#upload")[0].files[0];
		  var val = file.type;
		  var type = val.substr(val.indexOf("/") + 1);
		  if (type == 'png' || type == 'jpg' || type == 'jpeg') {
			return true;
		  }
		  else {
			document.getElementById("upload").value = "";
			toastr.error("Failed to upload image, the format is not supported","");
		  }
		});
		$('#upload-edit').on('change', function () {
		  var file = $("#upload")[0].files[0];
		  var val = file.type;
		  var type = val.substr(val.indexOf("/") + 1);
		  if (type == 'png' || type == 'jpg' || type == 'jpeg') {
			return true;
		  }
		  else {
			document.getElementById("upload").value = "";
			toastr.error("Failed to upload image, the format is not supported","");
		  }
		});
	  </script>
   </body>
</html>
