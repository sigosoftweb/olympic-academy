<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('subadmin/includes/includes.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('subadmin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-graduation-cap" style="line-height: 48px;padding-left: 2px;"></i> Exam instructions</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Exam instructions</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">


		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">
								 
		                     </div>
		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom">
								   <thead>
  		                           <tr>
  		                             <th>Instruction</th>
  		                           </tr>
  		                         </thead>
  		                         <tbody>
									 <?php foreach ($instructions as $instruction) { ?>
										 <tr>
										 	<td><?=$instruction->instruction?></td>
										 </tr>
									 <?php } ?>
  		                         </tbody>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <section class="videocontent" id="video"></section>
      <?php $this->load->view('subadmin/includes/scripts.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-script.php'); ?>

   </body>
</html>
