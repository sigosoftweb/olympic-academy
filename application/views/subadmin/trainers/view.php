<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('subadmin/includes/includes.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('subadmin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-users" style="line-height: 48px;padding-left: 2px;"></i> Trainers</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Trainers</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">


		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">
		                        <?php if($priority->add=='1') {?>		
								 <a class="btn btn-default btn-lg margin-bottom-20" href="<?=site_url('subadmin/trainers/add')?>"><span>Add trainer</span></a>
								<?php };?> 
		                     </div>
		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom" id="user_data">
								   <thead>
  		                           <tr>
									 <th width="10%">Image</th>
  		                             <th width="10%">English name</th>
									 <th width="10%">Contact</th>
  		                             <th width="10%">Cpr</th>
  		                             <th width="10%">Education</th>
									 <th width="10%">Experience</th>
									 <th width="10%">Address</th>
									 <th width="10%">Status</th>
									 <?php if($priority->edit=='1') {?> 
									   <th width="5%">Edit</th>
									 <?php };?>  
									 <!-- <th width="5%">Password</th> -->
  		                           </tr>
  		                         </thead>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <div class="modal fade" id="change-password" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
              <h3 class="modal-title" id="modalConfirmLabel"><strong>Change</strong> Password</h3>
            </div>
            <div class="modal-body">
              <form role="form" id="change-form" action="<?=site_url('subadmin/trainers/change')?>" method="post">
				  <input type="hidden" name="trainer_id" id="trainer_id">
                <div class="form-group">
                  <label for="placeholderInput">New Password</label>
                  <input type="text" class="form-control" id="pass-1" required>
                </div>
				<div class="form-group">
                  <label for="placeholderInput">Confirm Password</label>
                  <input type="text" name="password" class="form-control" id="pass-2" required>
                </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
              <button type="submit" class="btn btn-green" id="submit-button">Save changes</button>
            </div>
			</form>
          </div>
        </div>
      </div>
      <?php $this->load->view('subadmin/includes/scripts.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-script.php'); ?>
	  <script type="text/javascript">
	    $(document).ready(function(){
	      var dataTable = $('#user_data').DataTable({
	        "processing":true,
	        "serverSide":true,
	        "order":[],
	        "ajax":{
	          url:"<?=site_url('subadmin/trainers/get')?>",
	          type:"POST"
	        },
	        "columnDefs":[
	          {
	            "target":[0,3,4],
	            "orderable":true
	          }
	        ]
	      });
	    });
		$('#change-form').on('submit', function(e){
		  e.preventDefault();
		  $('#submit-button').attr('disabled',true);
		  var pass1 = $('#pass-1').val();
		  var pass2 = $('#pass-2').val();
		  if (pass1 == pass2) {
			  document.getElementById("change-form").submit();
		  }
		  else {
			  toastr.error('The New password and Confirmation password does not match..!');
			  $('#submit-button').attr('disabled',false);
		  }
	  	});
		function changePassword(trainer_id)
	    {
			$('#trainer_id').val(trainer_id);
	        $('#change-password').modal('show');
	    }
	  </script>
   </body>
</html>
