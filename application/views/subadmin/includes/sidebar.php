<?php $user  = $this->session->userdata['subadmin']; 
   $user_id  =  $user['user_id'];
   $ci       = &get_instance();
   $ci->load->Model('Common');
   $student  = $ci->Common->get_details('subadmin_previleges',array('m_id'=>'1','subadmin_id'=>$user_id))->row();
   $category = $ci->Common->get_details('subadmin_previleges',array('subadmin_id'=>$user_id,'m_id'=>'4'))->row();
   $course   = $ci->Common->get_details('subadmin_previleges',array('subadmin_id'=>$user_id,'m_id'=>'2'))->row();
   $trainers = $ci->Common->get_details('subadmin_previleges',array('subadmin_id'=>$user_id,'m_id'=>'3'))->row();
   $sections = $ci->Common->get_details('subadmin_previleges',array('subadmin_id'=>$user_id,'m_id'=>'22'))->row();
   $class    = $ci->Common->get_details('subadmin_previleges',array('subadmin_id'=>$user_id,'m_id'=>'24'))->row();
   $organization = $ci->Common->get_details('subadmin_previleges',array('subadmin_id'=>$user_id,'m_id'=>'6'))->row();
   $events = $ci->Common->get_details('subadmin_previleges',array('subadmin_id'=>$user_id,'m_id'=>'25'))->row();
   $notice = $ci->Common->get_details('subadmin_previleges',array('subadmin_id'=>$user_id,'m_id'=>'26'))->row();
   $finance = $ci->Common->get_details('subadmin_previleges',array('subadmin_id'=>$user_id,'m_id'=>'27'))->row();
   $sports = $ci->Common->get_details('subadmin_previleges',array('subadmin_id'=>$user_id,'m_id'=>'5'))->row();
   $education = $ci->Common->get_details('subadmin_previleges',array('subadmin_id'=>$user_id,'m_id'=>'32'))->row();
   $sliders   = $ci->Common->get_details('subadmin_previleges',array('subadmin_id'=>$user_id,'m_id'=>'33'))->row();
?>
<div class="navbar navbar-default navbar-fixed-top navbar-transparent-black mm-fixed-top" role="navigation" id="navbar">
   <div class="navbar-header col-md-2">
      <a class="navbar-brand" href="<?=site_url('subadmin/dashboard')?>">
      <strong>OLYMPIC</strong>ACADEMY
      </a>
      <div class="sidebar-collapse">
         <a href="#"><i class="fa fa-bars"></i></a>
      </div>
   </div>
   <div class="navbar-collapse">
      <ul class="nav navbar-nav refresh">
         <li class="divided">
            <a href="#" class="page-refresh"><i class="fa fa-refresh"></i></a>
         </li>
      </ul>
      <ul class="nav navbar-nav quick-actions">
         <li class="dropdown divided user" id="current-user">
            <div class="profile-photo">
               <img src="assets/images/profile-photo.jpg" alt />
            </div>
            <a class="dropdown-toggle options" data-toggle="dropdown" href="#">Olymbic academy <i class="fa fa-caret-down"></i></a>
            <ul class="dropdown-menu arrow settings">
               <li>
                  <a href="<?=site_url('subadmin_login/logout')?>"><i class="fa fa-power-off"></i> Logout</a>
               </li>
            </ul>
         </li>
      </ul>
      <ul class="nav navbar-nav side-nav" id="sidebar">
      <li class="collapsed-content">
         <ul>
            <li class="search">

            </li>
         </ul>
      </li>
      <li class="navigation" id="navigation">
         <a href="#" class="sidebar-toggle" data-toggle="#navigation">Navigation <i class="fa fa-angle-up"></i></a>
         <ul class="menu">
            <li>
               <a href="<?=site_url('subadmin/dashboard')?>"><i class="fa fa-tachometer"></i> Dashboard</a>
            </li>
            <?php if($category->all!='0') {?>
   			   <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-square-o"></i> Categories <b class="fa fa-plus dropdown-plus"></b></a>
                  <ul class="dropdown-menu">
                    <?php if($education->view=='1') {?> 
                        <li>
                           <a href="<?=site_url('subadmin/educations')?>"><i class="fa fa-caret-right"></i> Educations</a>
                        </li>
                     <?php };?> 

                     <?php if($sports->view=='1') {?> 
                        <li>
                           <a href="<?=site_url('subadmin/sports')?>"><i class="fa fa-caret-right"></i> Sports</a>
                        </li>
                     <?php };?>   

                     <?php if($organization->view=='1') {?> 
      				      <li>
                           <a href="<?=site_url('subadmin/organizations')?>"><i class="fa fa-caret-right"></i> Work organizations</a>
                        </li>
                     <?php };?>

                     <?php if($category->view=='1') {?>  
      				      <li>
                           <a href="<?=site_url('subadmin/categories')?>"><i class="fa fa-caret-right"></i> Course categories</a>
                        </li>
                     <?php };?>   
                  </ul>
               </li>
            <?php };?>
               
            <?php if($student->all!='0') {?>
   			 <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Students <b class="fa fa-plus dropdown-plus"></b></a>
                  <ul class="dropdown-menu">
                   <?php if($student->view=='1') {?>  
                     <li>
                        <a href="<?=site_url('subadmin/students')?>"><i class="fa fa-caret-right"></i> Manage students</a>
                     </li>
                   <?php };?>  
                   <?php if($student->add=='1') {?>
                     <li>
                        <a href="<?=site_url('subadmin/students/add')?>"><i class="fa fa-caret-right"></i> Add student</a>
                     </li>
                   <?php };?>  
                  </ul>
               </li>
            <?php };?>  

            <?php if($trainers->all!='0') {?> 
   			   <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-users"></i> Trainers <b class="fa fa-plus dropdown-plus"></b></a>
                  <ul class="dropdown-menu">
                    <?php if($trainers->view=='1') {?>  
                     <li>
                        <a href="<?=site_url('subadmin/trainers')?>"><i class="fa fa-caret-right"></i> Manage trainers</a>
                     </li>
                    <?php };?> 

                    <?php if($trainers->add=='1') {?>
                     <li>
                        <a href="<?=site_url('subadmin/trainers/add')?>"><i class="fa fa-caret-right"></i> Add trainer</a>
                     </li>
                    <?php };?>  
                  </ul>
               </li>
            <?php };?>
            
            <?php if($course->all!='0') {?>   
   			   <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-users"></i> Courses <b class="fa fa-plus dropdown-plus"></b></a>
                  <ul class="dropdown-menu">
                    <?php if($course->view=='1') {?> 
                        <li>
                           <a href="<?=site_url('subadmin/courses')?>"><i class="fa fa-caret-right"></i> Manage course</a>
                        </li>
                     <?php };?>  
                     
                     <?php if($sections->all!='0') {?> 
                        <li>
                           <a href="<?=site_url('subadmin/courses/sections')?>"><i class="fa fa-caret-right"></i> Manage sections</a>
                        </li>
                     <?php };?>   

                     <?php if($class->all!='0') {?>
      				      <li>
                           <a href="<?=site_url('subadmin/courses/classes')?>"><i class="fa fa-caret-right"></i> Manage online class</a>
                        </li>
                     <?php };?>   
                  </ul>
               </li>
            <?php };?>  
            
            <?php if($sliders->all!='0') {?>  
             <li>
               <a href="<?=site_url('subadmin/sliders')?>"><i class="fa fa-picture-o"></i> Sliders</a>
            </li>
            <?php };?> 
            
            <?php if($events->all!='0') {?>  
   			   <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i> Events <b class="fa fa-plus dropdown-plus"></b></a>
                  <ul class="dropdown-menu">
                     <li>
                        <a href="<?=site_url('subadmin/events/upcoming')?>"><i class="fa fa-caret-right"></i> Upcoming events</a>
                     </li>
                     <li>
                        <a href="<?=site_url('subadmin/events/completed')?>"><i class="fa fa-caret-right"></i> Completed events</a>
                     </li>
                  </ul>
               </li>
            <?php };?>   

            <?php if($notice->all!='0') {?>   
   			   <li>
                  <a href="<?=site_url('subadmin/notes')?>"><i class="fa fa-comments-o"></i> Notice</a>
               </li>
            <?php };?>   
			   
            <?php if($finance->all!='0') {?>   
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-dollar"></i> Finance management <b class="fa fa-plus dropdown-plus"></b></a>
                  <ul class="dropdown-menu">
                     <li>
                        <a href="<?=site_url('subadmin/finance')?>"><i class="fa fa-caret-right"></i> All transactions</a>
                     </li>
                     <li>
                        <a href="<?=site_url('subadmin/finance/filter')?>"><i class="fa fa-caret-right"></i> Filter</a>
                     </li>
                  </ul>
               </li>
            <?php };?>
         </ul>
   </div>
</div>
