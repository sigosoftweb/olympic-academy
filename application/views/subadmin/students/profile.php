<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('subadmin/includes/includes.php'); ?>
      <?php $this->load->view('subadmin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('subadmin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i><?=$student->stu_name_english?></h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Student profile</li>
                     </ol>
                  </div>
               </div>
			   <div class="main">



           <!-- row -->
            <div class="row">




              <!-- col 4 -->
              <div class="col-md-4">


                <!-- tile -->
                <section class="tile transparent">

                  <div class="tile-widget color transparent-white rounded-top-corners">
                    <div class="user-card">
                      <h3><?=$student->stu_name_english?></h3>
                      <ul class="profile-controls inline">
                        <li class="avatar">
							<?php if ($student->stu_image == '') { ?>
								<img src="<?=base_url() . 'uploads/students/user.png'?>" alt="" class="img-circle" width="100px">
							<?php }else { ?>
								<img src="<?=base_url() . $student->stu_image?>" alt="" class="img-circle" width="200px">
							<?php } ?>
						</li>
                      </ul>
                    </div>

                  </div>
                  <!-- /tile widget -->

                  <!-- tile body -->
                  <div class="tile-body color transparent-black textured rounded-bottom-corners">
                    <ul class="inline divided social-feed">
                      <li>
                        <h4>126</h4>
                        Tweets
                      </li>
                      <li>
                        <h4>324</h4>
                        Following
                      </li>
                    </ul>
                  </div>
                  <!-- /tile body -->



                </section>

                <section class="tile">

                  <div class="tile-header">
                  <h1><strong>Subscribed</strong> Packages</h1>
                    <div class="controls">
                      <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a>
                      <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
                      <a href="#" class="remove"><i class="fa fa-times"></i></a>
                    </div>
                  </div>
                  <!-- /tile header -->


                  <!-- tile body -->
                  <div class="tile-body">
                    <ul class="project-list">
                      <li>
                        <div class="col-md-3 project-name">
                          Minimal
                        </div>
                        <div class="col-md-9">
                          <div class="progress-info">
                            <div class="percent">20%</div>
                          </div>
                          <div class="progress">
                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                              <span class="sr-only">20% Complete</span>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="col-md-3 project-name">
                          Minoral
                        </div>
                        <div class="col-md-9">
                          <div class="progress-info">
                            <div class="percent">36%</div>
                          </div>
                          <div class="progress">
                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 36%">
                              <span class="sr-only">36% Complete</span>
                            </div>
                          </div>
                        </div>
                      </li>

                    </ul>
                  </div>
                </section>
              </div>

              <div class="col-md-8">

                <section class="tile transparent">

                  <div class="tile-widget nopadding color transparent-black rounded-top-corners">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabdrop">
                      <li class="active"><a href="#feed-tab" data-toggle="tab">Profile</a></li>
                      <li><a href="#tasks-tab" data-toggle="tab">Subscribed packages</a></li>
                      <li><a href="#settings-tab" data-toggle="tab">Payments</a></li>
                    </ul>
                    <!-- / Nav tabs -->
                  </div>
                  <!-- /tile widget -->

                  <!-- tile body -->
                  <div class="tile-body tab-content rounded-bottom-corners">

                    <!-- Tab panes -->
                    <div id="feed-tab" class="tab-pane fade in active">
						<div class="row">
                          <div class="form-group col-md-12 legend">
                            <h4><strong>Student</strong> Profile</h4>
                            <p></p>
                          </div>
                        </div>
						<form action="<?=site_url('subadmin/students/updateProfile')?>" method="post" id="edit-form" enctype="multipart/form-data">
  						  <input type="hidden" name="stu_id" value="<?=$student->stu_id?>">
  						  <div class="row">
  							 <div class="form-group col-sm-6">
  								<label for="exampleInputCountry">Name english</label>
  								<input type="text" class="form-control" name="stu_name_english" value="<?=$student->stu_name_english?>" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
  							 </div>
  							 <div class="form-group col-sm-6">
  								<label for="exampleInputCountry">Name arabic</label>
  								<input type="text" class="form-control" id="name-arabic" value="<?=$student->stu_name_arabic?>" name="stu_name_arabic">
  							 </div>
  						  </div>
  						  <div class="row">
  							 <div class="form-group col-sm-6">
  								<label for="exampleInputCity">CPR</label>
  								<input type="text" class="form-control" name="stu_cpr" value="<?=$student->stu_cpr?>">
  							 </div>
  							 <div class="form-group col-sm-6">
  								<label for="exampleInputAddress">Date Of Birth</label>
  								<input type="date" name="stu_dob" class="form-control" value="<?=$student->stu_dob?>">
  							 </div>
  							 <div class="form-group col-sm-6">
  								<label for="exampleInputCountry">Education</label>
  								<input type="text" name="stu_education" class="form-control" value="<?=$student->stu_education?>">
  							 </div>
  							 <div class="form-group col-sm-6">
  								<label for="exampleInputCity">Address</label>
  								<textarea name="stu_contact" rows="3" cols="80" class="form-control"><?=$student->stu_contact?></textarea>
  							 </div>
  							 <div class="form-group col-sm-6">
  								<label for="exampleInputAddress">Mobile number</label>
  								<input type="text" name="stu_mobile" id="mobile-number" class="form-control" value="<?=$student->stu_mobile?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>
  							 </div>
  							 <div class="form-group col-sm-6">
  								<label for="exampleInputCountry">Email address</label>
  								<input type="email" name="stu_email" class="form-control" value="<?=$student->stu_email?>">
  							 </div>
  						  </div>
  						  <div class="row">
  							 <div class="form-group col-sm-6">
  								<label for="exampleInputCity">Student image ( Choose image if you wish to change the image )</label>
  								<input type="file" name="image" id="upload" class="form-control">
  							 </div>
  						  </div>
  						  <div class="row">
  							 <div class="form-group col-sm-6">
  								 <button type="submit" class="btn btn-primary" id="submit-button-edit">Update</button>
  							 </div>
  						  </div>
  					  </form>

                    </div>

                    <div id="tasks-tab" class="tab-pane fade in">
						<div class="row">
                          <div class="form-group col-md-12 legend">
							  <button type="button" class="btn btn-primary margin-bottom-20 pull-right" data-toggle="modal" data-target="#subscribe-package">Subscribe package</button>
                            <h4><strong>Subscribed</strong> Packages</h4>
                            <p></p>
                          </div>
                        </div>
						<div class="row">
							<div class="col-md-12">
								<table class="table table-datatable table-custom">
									<thead>
										<th width="50%">Course</th>
										<th width="30%">Subscribed at</th>
										<th width="10%">View</th>
									</thead>
									<tbody>
										<?php foreach ($courses as $course) { ?>
											<tr>
												<td><?=$course->course_title?></td>
												<td><?=date('d/m/Y h:i A',strtotime($course->date))?></td>
												<td>View</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
						<div class="modal fade" id="subscribe-package" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
						  <div class="modal-dialog">
							<div class="modal-content">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
								<h3 class="modal-title" id="modalConfirmLabel"><strong>Subscribe</strong> Course</h3>
							  </div>
							  <div class="modal-body">
								<form role="form" id="subscribe-form" action="<?=site_url('subadmin/students/subscribeCourse')?>" method="post">
								   <input type="hidden" name="stu_id" value="<?=$student->stu_id?>">
								   <div class="row">
									   <div class="form-group col-sm-12 col-md-12">
										<label for="exampleInputCountry">Choose course</label>
										<select class="form-control" id="course_id" name="course_id">
											<option value="">-- Select course -- </option>
										  <?php foreach ($available as $cour) { ?>
											  <option value="<?=$cour->course_id?>"><?=$cour->course_title?></option>
										  <?php } ?>
										</select>
									 </div>
								    <div class="form-group col-sm-12 col-md-12">
									    <label for="exampleInputCountry">Package price</label>
									    <input type="text" class="form-control" id="package-price" readonly>
								    </div>
								    <div class="form-group col-sm-12 col-md-12">
										<label for="exampleInputCountry">Paid amount</label>
										<input type="text" class="form-control" name="amount" required>
								 	</div>
								   </div>

							  </div>
							  <div class="modal-footer">
								<button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
								<button type="submit" class="btn btn-green" id="submit-button">Save changes</button>
							  </div>
							 </form>
							</div>
						  </div>
						</div>
					</div>

                    <div id="settings-tab" class="tab-pane fade in">


						<div class="row">
  						<div class="form-group col-md-12 legend">
  						  <h4><strong>Students</strong> Payments</h4>
  						  <p></p>
  						</div>
  					  </div>
  					  <div class="row">
  						  <div class="col-md-12">
  							  <table class="table table-datatable table-custom">
  								  <thead>
  									  <th width="50%">Course</th>
  									  <th width="30%">Date</th>
									  <th width="30%">Course price</th>
  									  <th width="10%">Paid amount</th>
  								  </thead>
  								  <tbody>
  									  <?php foreach ($courses as $course) { ?>
  										  <tr>
  											  <td><?=$course->course_title?></td>
  											  <td><?=date('d/m/Y h:i A',strtotime($course->date))?></td>
  											  <td><?=$course->course_sale_price?></td>
											  <td><?=$course->amount?></td>
  										  </tr>
  									  <?php } ?>
  								  </tbody>
  							  </table>
  						  </div>
  					  </div>


                    </div>
                    <!-- / Tab panes -->

                  </div>
                  <!-- /tile body -->



                </section>
                <!-- /tile -->



              </div>
              <!-- /col 8 -->



            </div>
            <!-- /row -->




          </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('subadmin/includes/scripts.php'); ?>
      <?php $this->load->view('subadmin/includes/table-script.php'); ?>
	  <script type="text/javascript">
		  $('#edit-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button').attr('disabled',true);

			  var mobile = $('#mobile-number').val();
			  var arabic = $('#name-arabic').val();
			  if (mobile.length != 8) {
				  toastr.error('Mobile number should contains 8 numbers');
				  $('#submit-button-edit').attr('disabled',false);
			  }
			  else {
				  var isArabic = /^([\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufbc1]|[\ufbd3-\ufd3f]|[\ufd50-\ufd8f]|[\ufd92-\ufdc7]|[\ufe70-\ufefc]|[\ufdf0-\ufdfd]|[ ])*$/g;
			      if (isArabic.test(arabic)){
			           document.getElementById("edit-form").submit();
			      }
			      else {
			          toastr.error("Field contains Non-arabic letters","");
			          $('#submit-button-edit').attr('disabled',false);
			      }
			  }
		  });
		  $('#upload').on('change', function () {
		    var file = $("#upload")[0].files[0];
		    var val = file.type;
		    var type = val.substr(val.indexOf("/") + 1);
		    if (type == 'png' || type == 'jpg' || type == 'jpeg') {
		      var reader = new FileReader();
		        reader.onload = function (e) {
		          $uploadCrop.croppie('bind', {
		            url: e.target.result
		          }).then(function(){
		            console.log('jQuery bind complete');
		          });
		        }
		        reader.readAsDataURL(this.files[0]);
		    }
		    else {
		      document.getElementById("upload").value = "";
			  toastr.error("Failed to upload image, the format is not supported","");
		    }
		  });
		  $('#course_id').on('change', function() {
	        var course_id = this.value;
	        if (course_id == '') {
	            $('#package-price').val('');
	        }
	        else {
	            $.ajax({
	                method: "POST",
	                url: "<?php echo site_url('subadmin/students/getCourse');?>",
	                dataType : "json",
	                data : { course_id : course_id },
	                success : function( data ){
	                    $('#package-price').val(data.course_sale_price);
	                }
	            });
	        }
	    });
	  </script>
   </body>
</html>
