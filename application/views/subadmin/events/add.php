<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('subadmin/includes/includes.php'); ?>
      <?php $this->load->view('subadmin/includes/table-css.php'); ?>
	  <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/datepicker/css/bootstrap-datetimepicker.css">
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('subadmin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i>Add Event</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Add Event</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
                              <form action="<?=site_url('subadmin/events/addEvent')?>" method="post" id="add-form" enctype="multipart/form-data">
								  <div class="row">
				                    <div class="col-md-12">
				                      <div class="form-group">
				                        <label>Event title * </label>
				                        <input type="text" maxlength="200" placeholder="Enter title here" name="title" class="form-control" required>
				                      </div>
				                    </div>
									<div class="col-md-6">
				                      <div class="form-group">
				                        <label>Start time *</label>
				                        <input type="text" class="form-control" id="datepicker" name="start_time" autocomplete="off" required>
				                      </div>
				                    </div>
									<div class="col-md-6">
				                      <div class="form-group">
				                        <label>End time *</label>
				                        <input type="text" class="form-control" id="datepicker-edit" name="end_time" autocomplete="off" required>
				                      </div>
				                    </div>
				                    <div class="col-md-12">
				                      <div class="form-group">
				                        <label>Description *</label>
				                        <textarea name="description" class="form-control" rows="5" cols="80" required></textarea>
				                      </div>
				                    </div>
				                    <div class="col-md-6">
				                        <label>Choose image</label>
				                        <input type="file" class="form-control" name="image" id="upload" required>
				                    </div>
				                    <div class="col-md-12 mt-4">
				                      <button type="submit" class="btn btn-success waves-light waves-effect w-md pull-right" id="submit-button">Add</button>
				                    </div>
				                  </div>
                              </form>
                           </div>
                        </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('subadmin/includes/scripts.php'); ?>
      <?php $this->load->view('subadmin/includes/table-script.php'); ?>
	  <script src="<?=base_url()?>assets/js/vendor/datepicker/bootstrap-datetimepicker.min.js"></script>
	  <script type="text/javascript">
		  $('#add-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button').attr('disabled',true);

			  var start_time = $('#datepicker').val();
			  var end_time = $('#datepicker-edit').val();

              start = changeFormat(start_time);
              end = changeFormat(end_time);
              if (new Date(start) < new Date(end)) {
                  document.getElementById("add-form").submit();
              }
              else {
                  toastr.error('Start time must be less than end time',"");
                  $('#submit-button').attr('disabled',false);
              }

		  });
		  function changeFormat(date)
          {
			  arr = date.split(" ");
              if (arr[2] == 'PM') {
                  tm = arr[1].split(":");
                  if (tm[0] == 12) {
                      hour = tm[0];
                  }
                  else {
                      hour = +12 + +tm[0];
                  }
                  time = hour + ':' + tm[1] + ':00';
              }
              else {
                  time = arr[1] + ':00';
              }
              return arr[0].split("/").reverse().join("-") + ' ' + time;
          }
		  $('#upload').on('change', function () {
		    var file = $("#upload")[0].files[0];
		    var val = file.type;
		    var type = val.substr(val.indexOf("/") + 1);
		    if (type == 'png' || type == 'jpg' || type == 'jpeg') {
		      return true;
		    }
		    else {
		      document.getElementById("upload").value = "";
			  toastr.error("Failed to upload image, the format is not supported","");
		    }
		  });
		  $(function(){
			  $('#datepicker').datetimepicker({
				  format:'DD/MM/YYYY hh:mm A',
		        icons: {
		          time: "fa fa-clock-o",
		          date: "fa fa-calendar",
		          up: "fa fa-arrow-up",
		          down: "fa fa-arrow-down"
		        }
		      });
			  $('#datepicker-edit').datetimepicker({
				  format:'DD/MM/YYYY hh:mm A',
		        icons: {
		          time: "fa fa-clock-o",
		          date: "fa fa-calendar",
		          up: "fa fa-arrow-up",
		          down: "fa fa-arrow-down"
		        }
		      });
		      $("#datepicker").on("dp.show",function (e) {
		        var newtop = $('.bootstrap-datetimepicker-widget').position().top - 45;
		        $('.bootstrap-datetimepicker-widget').css('top', newtop + 'px');
		      });
		  });
	  </script>

   </body>
</html>
