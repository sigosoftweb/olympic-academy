<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('subadmin/includes/includes.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('subadmin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-graduation-cap" style="line-height: 48px;padding-left: 2px;"></i> Upcoming events</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Upcoming events</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">
		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">
								 <button type="button" class="btn btn-default btn-lg margin-bottom-20" onclick="window.location='<?=site_url("subadmin/events/add");?>'">Add event</button>
		                     </div>
		                   </div>
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
								 <table id="datatable" class="table table-datatable table-custom">
                      <thead>
                        <tr>
                          <th width="10%">Image</th>
                          <th width="20%">Title</th>
                          <th width="40%">Description</th>
                          <th width="10%">Start</th>
						              <th width="10%">End</th>
                          <th width="5%">Status</th>
                          <th width="5%">Options</th>
                        </tr>
                      </thead>
  					<tbody>
  						<?php foreach ($events as $event) { ?>
  							<tr>
  								<td><img src="<?=base_url() . $event->image?>" width="100px"></td>
  								<td><?=$event->title?></td>
  								<td><?=$event->description?></td>
  								<td><?=$event->start_time?></td>
								<td><?=$event->end_time?></td>
								<td>
									<?php if ($event->status) { ?>
										Active<br>
										<a href="<?=site_url('subadmin/events/status/u/' . $event->event_id . '/0')?>" class="btn btn-danger btn-xs margin-bottom-20">Block</a>
									<?php }else { ?>
										Blocked<br>
										<a href="<?=site_url('subadmin/events/status/u/' . $event->event_id . '/1')?>" class="btn btn-success btn-xs margin-bottom-20">Activate</a>
									<?php } ?>
								</td>
  								<td>
                  <?php if ($priority->edit=='1') { ?>  
									  <a href="<?=site_url('subadmin/events/edit/' . $event->event_id)?>" class="btn btn-primary btn-sm " onclick="edit(<?=$event->event_id?>)">Edit</a>
                  <?php };?>  
                  <?php if ($priority->delete=='1') { ?>
                    <a href="<?=site_url('subadmin/events/delete/' . $event->event_id)?>" class="btn btn-danger btn-sm ">Delete</a>
                  <?php };?>  
								</td>
  							</tr>
  						<?php } ?>
  					</tbody>
                    </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('subadmin/includes/scripts.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-script.php'); ?>
   </body>
</html>
