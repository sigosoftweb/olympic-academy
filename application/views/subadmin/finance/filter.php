<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('subadmin/includes/includes.php'); ?>
      <?php $this->load->view('subadmin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('subadmin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-users" style="line-height: 48px;padding-left: 2px;"></i>Filter transactions</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Filter transactions</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
                              <form action="<?=site_url('subadmin/finance/filter')?>" method="post" id="add-form">
								  <div class="row">
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">From date</label>
	                                    <input type="date" class="form-control" name="from" required>
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">To date</label>
	                                    <input type="date" class="form-control" name="to">
	                                 </div>
									 <div class="form-group col-sm-4">
										 <button type="submit" class="btn btn-primary" id="submit-button" style="margin-top:30px;">Filter</button>
	                                 </div>
	                              </div>
                              </form>
                           </div>
                        </section>
                     </div>
                  </div>
               </div>
			   <?php if ($param) { ?>
				   <div class="main">
	                  <div class="row">
	                     <div class="col-md-12">
	                        <section class="tile color transparent-black">
	                           <div class="tile-body">
								   <div class="table-responsive">
	  		                       <table  class="table table-datatable table-custom" id="datatable">
	  								   <thead>
	    		                           <tr>
	  									 	 <th width="10%">Student</th>
	    		                             <th width="10%">Contact</th>
	  									  	 <th width="10%">Course</th>
	    		                             <th width="10%">Subscribed at</th>
	    		                             <th width="10%">Amount</th>
	  									 	 <th width="10%">Payment ID</th>
	    		                           </tr>
	    		                         </thead>
										 <tbody>
										 	<?php foreach ($transactions as $trans) { ?>
										 		<tr>
										 			<td><?=$trans->stu_name_english . '<br>' . $trans->stu_name_arabic?></td>
													<td><?=$trans->stu_mobile . '<br>' . $trans->stu_email?></td>
													<td><?=$trans->course_title?></td>
													<td><?=date('d/m/Y h:i A',strtotime($trans->date))?></td>
													<td><?=$trans->amount?></td>
													<td><?=$trans->payment_id?></td>
										 		</tr>
										 	<?php } ?>
										 </tbody>
	  		                       </table>
	  		                     </div>
	                           </div>
	                        </section>
	                     </div>
	                  </div>
	               </div>
			   <?php } ?>
            </div>
         </div>
      </div>
      <?php $this->load->view('subadmin/includes/scripts.php'); ?>
      <?php $this->load->view('subadmin/includes/table-script.php'); ?>
   </body>
</html>
