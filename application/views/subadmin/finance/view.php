<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('subadmin/includes/includes.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('subadmin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-users" style="line-height: 48px;padding-left: 2px;"></i> All transactions</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>All transactions</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">


		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>

		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom" id="user_data">
								   <thead>
  		                           <tr>
									 <th width="10%">Student</th>
  		                             <th width="10%">Contact</th>
									 <th width="10%">Course</th>
  		                             <th width="10%">Subscribed at</th>
  		                             <th width="10%">Amount</th>
									 <th width="10%">Payment</th>
  		                           </tr>
  		                         </thead>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <?php $this->load->view('subadmin/includes/scripts.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-script.php'); ?>
	  <script type="text/javascript">
	    $(document).ready(function(){
	      var dataTable = $('#user_data').DataTable({
	        "processing":true,
	        "serverSide":true,
	        "order":[],
	        "ajax":{
	          url:"<?=site_url('subadmin/finance/get')?>",
	          type:"POST"
	        },
	        "columnDefs":[
	          {
	            "target":[0,3,4],
	            "orderable":true
	          }
	        ]
	      });
	    });
	  </script>
   </body>
</html>
