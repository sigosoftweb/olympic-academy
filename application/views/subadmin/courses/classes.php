<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('subadmin/includes/includes.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('subadmin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i> All classes</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>All classes</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">

		                   <div class="tile-header transparent">
		                     <span class="note"></span>

		                   </div>

		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom" id="user_data">
								   <thead>
  		                           <tr>
  		                             <th width="10%">Topic</th>
									 <th width="10%">Course</th>
									 <th width="10%">Starting time</th>
									 <th width="10%">Status</th>
									 <th width="5%">View</th>
  		                           </tr>
  		                         </thead>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <?php $this->load->view('subadmin/includes/scripts.php'); ?>
	  <?php $this->load->view('subadmin/includes/table-script.php'); ?>
	  <script type="text/javascript">
	    $(document).ready(function(){
	      var dataTable = $('#user_data').DataTable({
	        "processing":true,
	        "serverSide":true,
	        "order":[],
	        "ajax":{
	          url:"<?=site_url('subadmin/courses/getClasses')?>",
	          type:"POST"
	        },
	        "columnDefs":[
	          {
	            "target":[0,3,4],
	            "orderable":true
	          }
	        ]
	      });
	    });
	  </script>
   </body>
</html>
