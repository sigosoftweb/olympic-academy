<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('admin/includes/includes.php'); ?>
      <?php $this->load->view('admin/includes/table-css.php'); ?>

      <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/chosen/css/chosen.min.css">
      <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/chosen/css/chosen-bootstrap.css">
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i>Course : <?=$course->course_title?></h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Add Course</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
							   <form class="new-added-form" method="POST" action="<?=site_url('admin/exams/addQ')?>" id="add-form" enctype="multipart/form-data">
									<div class="row">
										<div class="col-xl-12 col-lg-12 col-12">
                                            <h5>Question <?=$questions+1?>/<?=$exam->no_questions?></h5>
                                        </div>
										<div class="col-md-3">
											<label class="text-dark-medium">Upload Image</label>
                                            <input type="file" class="form-control-file" name="image" placeholder="Default Input" id="upload">
										</div>
									</div>
                                    <div class="row" style="margin-top:20px;">
                                        <input type="hidden" name="exam_id" value="<?=$exam->exam_id?>">
                                        <div class="col-xl-12 col-lg-12 col-12 form-group">
                                            <div id="editorContainer1">
                                                <span>Question</span>
										        <div class="tinyclass myeditablediv" id="formula-question"></div>
										        <input type="hidden" id="question_src" name="formula_question">
								            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-12 form-group">
                                            <label>A<span style="color: red;">*</span></label>
                                            <div id="editorContainer1">
										        <div class="tinyclass myeditablediv1" id="formula-a"></div>
										        <input type="hidden" id="formula_a" name="formula_a">
								            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-12 form-group">
                                            <label>B<span style="color: red;">*</span></label>
                                            <div id="editorContainer2">
										        <div class="tinyclass myeditablediv1" id="formula-b"></div>
										        <input type="hidden" id="formula_b" name="formula_b">
								            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-12 form-group">
                                            <label>C</label>
                                            <div id="editorContainer3">
										        <div class="tinyclass myeditablediv1" id="formula-c"></div>
										        <input type="hidden" id="formula_c" name="formula_c">
								            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-12 form-group">
                                            <label>D</label>
                                            <div id="editorContainer4">
										        <div class="tinyclass myeditablediv1" id="formula-d"></div>
										        <input type="hidden" id="formula_d" name="formula_d">
								            </div>
                                        </div>
										<div class="col-xl-12 col-lg-12 col-12 form-group">
                                            <label>E</label>
                                            <div id="editorContainer5">
										        <div class="tinyclass myeditablediv1" id="formula-e"><p></p></div>
										        <input type="hidden" id="formula_e" name="formula_e">
								            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-3 col-3 form-group">
                                            <label>Answer</label>
                                            <select class="form-control" name="answer" id="answer" required>
                                                <option value="">-- Please select answer --</option>
                                                <option value="a">A</option>
                                                <option value="b">B</option>
                                                <option value="c">C</option>
                                                <option value="d">D</option>
												<option value="e">E</option>
                                            </select>
                                        </div>
                                        <div class="col-12 form-group mg-t-8 float-right">
                                            <button type="submit" id="submit-button" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark float-right">Add</button>
                                        </div>
                                    </div>
                                </form>
                           </div>
                        </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
      <?php $this->load->view('admin/includes/table-script.php'); ?>
      <script src="<?=base_url()?>assets/js/vendor/chosen/chosen.jquery.min.js"></script>
   </body>
</html>
