<?php $user = $this->session->userdata['teacher']; ?>
<div class="navbar navbar-default navbar-fixed-top navbar-transparent-black mm-fixed-top" role="navigation" id="navbar">
   <div class="navbar-header col-md-2">
      <a class="navbar-brand" href="<?=site_url('trainer/dashboard')?>">
      <strong>OLYMPIC</strong>ACADEMY
      </a>
      <div class="sidebar-collapse">
         <a href="#"><i class="fa fa-bars"></i></a>
      </div>
   </div>
   <div class="navbar-collapse">
      <ul class="nav navbar-nav refresh">
         <li class="divided">
            <a href="#" class="page-refresh"><i class="fa fa-refresh"></i></a>
         </li>
      </ul>
      <ul class="nav navbar-nav quick-actions">
         <li class="dropdown divided user" id="current-user">
            <div class="profile-photo">
               <img src="assets/images/profile-photo.jpg" alt />
            </div>
            <a class="dropdown-toggle options" data-toggle="dropdown" href="#"><?=$user['name'];?><i class="fa fa-caret-down"></i></a>
            <ul class="dropdown-menu arrow settings">
               <li>
                  <a href="<?=site_url('trainer/profile')?>">Profile</a>
               </li>
               <li>
                  <a data-toggle="modal" data-target="#edit-password" class="dropdown-item notify-item">Change Password</a>
               </li>
               <li>
                  <a href="<?=site_url('teacher/logout')?>"><i class="fa fa-power-off"></i> Logout</a>
               </li>
            </ul>
         </li>
      </ul>
      <ul class="nav navbar-nav side-nav" id="sidebar">
      <li class="collapsed-content">
         <ul>
            <li class="search">

            </li>
         </ul>
      </li>
      <li class="navigation" id="navigation">
         <a href="#" class="sidebar-toggle" data-toggle="#navigation">Navigation <i class="fa fa-angle-up"></i></a>
         <ul class="menu">
            <li>
               <a href="<?=site_url('trainer/dashboard')?>"><i class="fa fa-tachometer"></i> Dashboard</a>
            </li>
          
   		    <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-users"></i> Courses <b class="fa fa-plus dropdown-plus"></b></a>
              <ul class="dropdown-menu">
                 <li>
                    <a href="<?=site_url('trainer/courses')?>"><i class="fa fa-caret-right"></i> Manage course</a>
                 </li>
               <!--   <li>
                    <a href="<?=site_url('trainer/courses/sections')?>"><i class="fa fa-caret-right"></i> Manage sections</a>
                 </li>
  				       <li>
                       <a href="<?=site_url('trainer/courses/class')?>"><i class="fa fa-caret-right"></i> Manage online class</a>
                 </li> -->
              </ul>
            </li>
            
            <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-square-o"></i> Categories <b class="fa fa-plus dropdown-plus"></b></a>
               <ul class="dropdown-menu">
				  <li>
                     <a href="<?=site_url('trainer/categories')?>"><i class="fa fa-caret-right"></i> Course categories</a>
                  </li>
               </ul>
            </li>
         </ul>
      </li>     
   </div>



</div>
    
  <div class="modal fade" id="edit-password" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
           <h3 class="modal-title" id="modalConfirmLabel"><strong>Change</strong> Password</h3>
         </div>
         <div class="modal-body">
           <form class="form-horizontal" id="form-password" method="POST" action="<?=site_url('trainer/profile/changePassword')?>">
             <div class="form-group">
               <label for="placeholderInput">Old Password</label>
               <input type="password" class="form-control" id="old_password" name="old_password" placeholder="Old Password" required>
               <input type="hidden" name="user_id" id="user" value="<?php echo $user['user_id'];?>" class="form-control" required>
             </div>
             <div class="form-group">
               <label for="placeholderInput">New Password</label>
               <input type="password" class="form-control" name="password" id="new_password" placeholder="Password" required>
             </div>
         </div>
         <div class="modal-footer">
           <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
           <button type="submit" class="btn btn-green" id="submit-button">Save changes</button>
         </div>
          </form>
       </div>
     </div>
   </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
    $('#form-password').on('submit', function(e){
      e.preventDefault();
     // CODE HERE
     password = $('#old_password').val();
     id       = $('#user').val();
     // alert(password);
      $.ajax({
       method: "POST",
       url: "<?php echo site_url('trainer/profile/checkPassword');?>",
       dataType : "json",
       data : { id       : id,
               password : password
              },
       success : function( data ){
         old          = data.trainer_password;
         current      = data.old_password;
         // alert(old);
         
         if (old == current) 
         {
           document.getElementById("form-password").submit();
           return true;
         }
         else 
         {
           toastr.error('Old password incorrect');
           return false;
         }
       }
       
     });
   });
 </script>