<script src="<?=base_url()?>assets/js/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/js/vendor/datatables/ColReorderWithResize.js"></script>
<script src="<?=base_url()?>assets/js/vendor/datatables/colvis/dataTables.colVis.min.js"></script>
<script src="<?=base_url()?>assets/js/vendor/datatables/tabletools/ZeroClipboard.js"></script>
<script src="<?=base_url()?>assets/js/vendor/datatables/tabletools/dataTables.tableTools.min.js"></script>
<script src="<?=base_url()?>assets/js/vendor/datatables/dataTables.bootstrap.js"></script>
<script src="<?=base_url()?>assets/js/vendor/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript">
$(function(){
  $.fn.dataTableExt.oStdClasses.sPaging = 'dataTables_paginate paging_bootstrap paging_custom';

  $('div.dataTables_filter input').addClass('form-control');
  $('div.dataTables_length select').addClass('form-control');

  jQuery.fn.dataTableExt.oSort['string-case-asc']  = function(x,y) {
	  return ((x < y) ? -1 : ((x > y) ?  1 : 0));
  };

  jQuery.fn.dataTableExt.oSort['string-case-desc'] = function(x,y) {
	  return ((x < y) ?  1 : ((x > y) ? -1 : 0));
  };

  $("#basicDataTable tbody tr").click( function( e ) {
	if ( $(this).hasClass('row_selected') ) {
	  $(this).removeClass('row_selected');
	}
	else {
	  oTable01.$('tr.row_selected').removeClass('row_selected');
	  $(this).addClass('row_selected');
	}

	if ($('#basicDataTable tr.row_selected').length > 0) {
	  $('#deleteRow').stop().fadeIn(300);
	} else {
	  $('#deleteRow').stop().fadeOut(300);
	}
  });

  /* Build the DataTable with third column using our custom sort functions */
  var settings = {
	"processing":true,
	"serverSide":true,
	"order":[],
	"ajax":{
	  url:"<?=site_url('customers/get')?>",
	  type:"POST"
	},
	"columnDefs":[
	  {
		"target":[0,3,4],
		"orderable":true
	  }
	],
	dom: 'lBfrtip',
	buttons: [
		{
			extend:'pdfHtml5',
			text:'Pdf',
			exportOptions: {
			  columns: [ 1, 2, 3, 4, 5 ]
			},
			orientation:'landscape',
			customize: function (doc) {
			  doc.content[1].table.widths =
				  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
			}
		},
		{
			extend: 'print',
			text: 'Print',
			exportOptions: {
			  columns: [ 1, 2, 3, 4, 5 ]
			},
		},
		{ extend: 'csv',text: 'Csv' },
	],
	lengthMenu: [[25, 100, -1], [25, 100, "All"]],
	pageLength: 25,
	customize: function (doc) {
		doc.content[1].table.widths =
			Array(doc.content[1].table.body[0].length + 1).join('*').split('');
	  }
  };
  var oTable01 = $('#basicDataTable').dataTable(settings);

});

</script>
