<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('teacher/includes/includes.php'); ?>
	  <?php $this->load->view('teacher/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('teacher/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-university" style="line-height: 48px;padding-left: 2px;"></i> Class attachments</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Class attachments</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">


		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">
								 <button type="button" class="btn btn-default btn-lg margin-bottom-20" data-toggle="modal" data-target="#add-organization">Add attachment</button>
		                     </div>
		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom" id="datatable">
								   <thead>
  		                           <tr>
  		                             <th width="10%">Title</th>
  		                             <th width="10%">Type</th>
  		                             <th width="10%">Attchment</th>
									 <th width="10%">Delete</th>
  		                           </tr>
  		                         </thead>
  		                         <tbody>
									 <?php foreach ($attachments as $attach) { ?>
									 	<tr>
									 		<td><?=$attach->title?></td>
											<td><?=$attach->type?></td>
											<td><a href="<?=base_url() . $attach->attachment?>" target="_blank" class="btn btn-primary btn-xs margin-bottom-20">View</a></td>
											<td><a href="<?=site_url('trainer/courses/deleteClassAttachment/' . $attach->cca_id)?>" class="btn btn-danger btn-xs margin-bottom-20">Delete</a></td>
									 	</tr>
									 <?php } ?>
  		                         </tbody>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <div class="modal fade" id="add-organization" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
              <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> attachment</h3>
            </div>
            <div class="modal-body">
              <form role="form" id="add-form" action="<?=site_url('trainer/courses/addClassAttachment')?>" method="post" enctype="multipart/form-data">
				  <input type="hidden" name="cc_id" value="<?=$cc_id?>">
                <div class="form-group">
                  <label for="placeholderInput">Title</label>
                  <input type="text" name="title" class="form-control" placeholder="Title" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
                </div>
				<div class="form-group">
				  <label for="exampleInputCountry">Attachment type</label>
				  <select class="form-control" name="type">
					  <option value="Image">Image</option>
					  <option value="Documents">Documents</option>
					  <option value="Video">Video</option>
				  </select>
				</div>
				<div class="form-group">
				  <label for="exampleInputCountry">Choose attachment</label>
				  <input type="file" name="file" id="upload" class="form-control">
				</div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
              <button type="submit" class="btn btn-green" id="submit-button">Save changes</button>
            </div>
			</form>
          </div>
        </div>
      </div>

      <section class="videocontent" id="video"></section>
      <?php $this->load->view('teacher/includes/scripts.php'); ?>
	  <?php $this->load->view('teacher/includes/table-script.php'); ?>
	  <script>
	  	$('#add-form').on('submit', function(e){
		  e.preventDefault();
		  $('#submit-button').attr('disabled',true);
		  document.getElementById("add-form").submit();
	  	});
		$('#upload').on('change', function () {
		  var file = $("#upload")[0].files[0];
		  var val = file.type;
		  var type = val.substr(val.indexOf("/") + 1);
		  if (type == 'png' || type == 'jpg' || type == 'jpeg' || type == 'pdf' || type == 'mp4') {
			  return true;
		  }
		  else {
			document.getElementById("upload").value = "";
			toastr.error("Failed to upload image, the format is not supported","");
		  }
		});
	  </script>
   </body>
</html>
