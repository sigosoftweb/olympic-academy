<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('teacher/includes/includes.php'); ?>
	  <?php $this->load->view('teacher/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('teacher/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i> Courses</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Courses</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">

		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">
								 <a class="btn btn-default btn-lg margin-bottom-20" href="<?=site_url('trainer/courses/add')?>"><span>Add course</span></a>
		                     </div>
		                   </div>
		                   <div class="tile-body color transparent-black rounded-corners">
		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom" id="user_data">
								  <thead>
  		                           <tr>
									 <th width="10%">Image</th>
  		                             <th width="10%">Title</th>
									 <th width="10%">Description</th>
  		                             <th width="10%">Starting date</th>
									 <th width="10%">Duration</th>
  		                             <th width="5%">Sale price</th>
									 <th width="5%">Price</th>
									 <th width="5%">Status</th>
									 <th width="5%">Details</th>
  		                           </tr>
  		                         </thead>
  		                         <tbody>
		                       	<?php foreach($courses as $course) {?>
		                       		<tr>
		                       			<td>
		                       				<?php if($course->course_banner==!'') {?>
		                       				  <img src="<?=base_url().$course->course_banner?>" height="100px">
		                       				<?php } else {?>  
		                       					<img src="<?=base_url()?>uploads/courses/default.png" height="100px">
		                       				<?php };?>
		                       			</td>
		                       			<td><?=$course->course_title?></td>
		                       			<td><?=$course->course_description?></td>
		                       			<td><?=$course->course_starting_date?></td>
		                       			<td><?=$course->course_duration?></td>
		                       			<td><?=$course->course_sale_price?></td>
		                       			<td><?=$course->course_price?></td>
		                       			<td><?php if ($course->course_status) 
		                       			    {
												$status = 'Published<br><a class="btn btn-danger btn-xs margin-bottom-20" href="' . site_url('trainer/courses/status/'. $course->course_id . '/0') . '">Block</a>';
											}
											else 
											{
												$status = 'Draft<br><a class="btn btn-success btn-xs margin-bottom-20" href="' . site_url('trainer/courses/status/'. $course->course_id . '/1') . '">Activate</a>';
										    }
										    echo $status;
										    ?>											
										</td>
		                       			<td><a class="btn btn-primary btn-xs margin-bottom-20" href="<?=site_url('trainer/courses/details/1/'.$course->course_id)?>">Details</a></td>
		                       		</tr>
		                       	<?php };?>
		                       </tbody>
		                       </table>

		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('teacher/includes/scripts.php'); ?>
	  <?php $this->load->view('teacher/includes/table-script.php'); ?>
	  <script type="text/javascript">
        $(document).ready(function() {
            $('#user_data').DataTable({
           "ordering": false
        });
        });

    </script>
   </body>
</html>
