<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('teacher/includes/includes.php'); ?>
	  <?php $this->load->view('teacher/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('teacher/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-graduation-cap" style="line-height: 48px;padding-left: 2px;"></i> View questions</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>View questions</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">


		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">

		                     </div>
		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
								 <table class="table table-bordered">
				                <thead>
				                  <tr>
				                     <th width="5%">No.</th>
				                     <th width="95%">Question</th>
				                     <th width="5%">Change</th>
				                  </tr>
				                </thead>
				                <tbody>
				                    <?php $i = 1; foreach ($questions as $que) { ?>
				                        <tr>
				                          <td>
				                              <?=$i?>
				                          </td>
				                          <td>
				                              <span>Question</span>
				  							<?php if ($que->attachment == '1') { ?>
				  							    <img src="<?=base_url() . $que->image?>" width="200px" alt=""><br>
				  							<?php } ?>
				                              <?=$que->formula_question?>

				                              <span>Option A</span>
				                              <?=$que->formula_a?>

				                              <span>Option B</span>
				                              <?=$que->formula_b?>

				                              <?php if($que->formula_c != ''){ ?>
				                                  <span>Option C</span>
				                                  <?=$que->formula_c?>
				                              <?php } ?>

				                              <?php if($que->formula_d != ''){ ?>
				                                  <span>Option D</span>
				                                  <?=$que->formula_d?>
				                              <?php } ?>

				                              <?php if($que->formula_e != ''){ ?>
				                                  <span>Option E</span>
				                                  <?=$que->formula_e?>
				                              <?php } ?>

				                              <span>Answer : </span> <?=$que->ans?><br>
				                              <span>Question mark : </span> <?=$que->correct?><br>
				                              <span>Negative mark : </span> <?=$que->negative?>
				                          </td>
				                          <td>
				                              <a class="btn btn-primary" href="<?=site_url('trainer/exams/editQues/' . $que->que_id)?>">Edit</a><br>
				                              <a class="btn btn-danger" onclick="return confirm('Are you sure to delete?')" href="<?=site_url('trainer/exams/deleteQuestion/' . $que->que_id)?>">Delete</a>
				                          </td>
				                        </tr>
				                    <?php $i++; } ?>
				                </tbody>
				              </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <section class="videocontent" id="video"></section>
      <?php $this->load->view('teacher/includes/scripts.php'); ?>
	  <?php $this->load->view('teacher/includes/table-script.php'); ?>

   </body>
</html>
