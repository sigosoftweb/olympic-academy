<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('teacher/includes/includes.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('teacher/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-tachometer" style="line-height: 48px;padding-left: 2px;"></i> Dashboard</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Dashboard</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
				   <div class="row cards">

 	                <div class="card-container col-lg-3 col-sm-6 col-sm-12">
 	                  <div class="card card-redbrown hover">
 	                    <div class="front">

 	                      <div class="media">
 	                        <span class="pull-left">
 	                          <i class="fa fa-users media-object"></i>
 	                        </span>

 	                        <div class="media-body">
 	                          <small>New Users</small>
 	                          <h2 class="media-heading animate-number" data-value="3659" data-animation-duration="1500">0</h2>
 	                        </div>
 	                      </div>

 	                      <div class="progress-list">
 	                        <div class="details">
 	                          <div class="title">This month plan %</div>
 	                        </div>
 	                        <div class="status pull-right bg-transparent-black-1">
 	                          <span class="animate-number" data-value="83" data-animation-duration="1500">0</span>%
 	                        </div>
 	                        <div class="clearfix"></div>
 	                        <div class="progress progress-little progress-transparent-black">
 	                          <div class="progress-bar animate-progress-bar" data-percentage="83%"></div>
 	                        </div>
 	                      </div>

 	                    </div>
 	                    <div class="back">
 	                      <a href="#">
 	                        <i class="fa fa-bar-chart-o fa-4x"></i>
 	                        <span>Check Summary</span>
 	                      </a>
 	                    </div>
 	                  </div>
 	                </div>


 	                <div class="card-container col-lg-3 col-sm-6 col-sm-12">
 	                  <div class="card card-blue hover">
 	                    <div class="front">

 	                      <div class="media">
 	                        <span class="pull-left">
 	                          <i class="fa fa-shopping-cart media-object"></i>
 	                        </span>

 	                        <div class="media-body">
 	                          <small>New Orders</small>
 	                          <h2 class="media-heading animate-number" data-value="19214" data-animation-duration="1500">0</h2>
 	                        </div>
 	                      </div>

 	                      <div class="progress-list">
 	                        <div class="details">
 	                          <div class="title">This month plan %</div>
 	                        </div>
 	                        <div class="status pull-right bg-transparent-black-1">
 	                          <span class="animate-number" data-value="100" data-animation-duration="1500">0</span>%
 	                        </div>
 	                        <div class="clearfix"></div>
 	                        <div class="progress progress-little progress-transparent-black">
 	                          <div class="progress-bar animate-progress-bar" data-percentage="100%"></div>
 	                        </div>
 	                      </div>

 	                    </div>
 	                    <div class="back">
 	                      <a href="#">
 	                        <i class="fa fa-bar-chart-o fa-4x"></i>
 	                        <span>Check Summary</span>
 	                      </a>
 	                    </div>
 	                  </div>
 	                </div>



 	                <div class="card-container col-lg-3 col-sm-6 col-sm-12">
 	                  <div class="card card-greensea hover">
 	                    <div class="front">

 	                      <div class="media">
 	                        <span class="pull-left">
 	                          <i class="fa fa-usd media-object"></i>
 	                        </span>

 	                        <div class="media-body">
 	                          <small>Sales</small>
 	                          <h2 class="media-heading animate-number" data-value="169541" data-animation-duration="1500">0</h2>
 	                        </div>
 	                      </div>

 	                      <div class="progress-list">
 	                        <div class="details">
 	                          <div class="title">This month plan %</div>
 	                        </div>
 	                        <div class="status pull-right bg-transparent-black-1">
 	                          <span class="animate-number" data-value="42" data-animation-duration="1500">0</span>%
 	                        </div>
 	                        <div class="clearfix"></div>
 	                        <div class="progress progress-little progress-transparent-black">
 	                          <div class="progress-bar animate-progress-bar" data-percentage="42%"></div>
 	                        </div>
 	                      </div>

 	                    </div>
 	                    <div class="back">
 	                      <a href="#">
 	                        <i class="fa fa-bar-chart-o fa-4x"></i>
 	                        <span>Check Summary</span>
 	                      </a>
 	                    </div>
 	                  </div>
 	                </div>


 	                <div class="card-container col-lg-3 col-sm-6 col-xs-12">
 	                  <div class="card card-slategray hover">
 	                    <div class="front">

 	                      <div class="media">
 	                        <span class="pull-left">
 	                          <i class="fa fa-eye media-object"></i>
 	                        </span>

 	                        <div class="media-body">
 	                          <small>Visits</small>
 	                          <h2 class="media-heading animate-number" data-value="9634" data-animation-duration="1500">0</h2>
 	                        </div>
 	                      </div>

 	                      <div class="progress-list">
 	                        <div class="details">
 	                          <div class="title">This month plan %</div>
 	                        </div>
 	                        <div class="status pull-right bg-transparent-black-1">
 	                          <span class="animate-number" data-value="25" data-animation-duration="1500">0</span>%
 	                        </div>
 	                        <div class="clearfix"></div>
 	                        <div class="progress progress-little progress-transparent-black">
 	                          <div class="progress-bar animate-progress-bar" data-percentage="25%"></div>
 	                        </div>
 	                      </div>

 	                    </div>
 	                    <div class="back">
 	                      <a href="#">
 	                        <i class="fa fa-bar-chart-o fa-4x"></i>
 	                        <span>Check Summary</span>
 	                      </a>
 	                    </div>
 	                  </div>
 	                </div>


 	              </div>
               </div>
            </div>
         </div>
      </div>
      <section class="videocontent" id="video"></section>
      <?php $this->load->view('teacher/includes/scripts.php'); ?>
   </body>
</html>
