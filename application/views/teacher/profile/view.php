<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('teacher/includes/includes.php'); ?>
      <?php $this->load->view('teacher/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('teacher/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-users" style="line-height: 48px;padding-left: 2px;"></i>My Profile</h2>
                    
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Profile</li>
                     </ol>
                     <ol class="breadcrumb float-right">
	                    <a href="<?=site_url('trainer/profile/edit/'.$profile->trainer_id)?>"><button type="button" class="btn btn-gradient btn-rounded waves-light waves-effect w-md">Edit Profile</button></a>
	                 </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
                           	  <img src="<?=base_url() . $profile->trainer_image?>" width="10%" height="10%">
                              <table class="table ms-profile-information">
	                            <tbody>
	                              <tr>
	                                <th scope="row">Name</th>
	                                <td>: <?=$profile->trainer_name_english?></td>
	                              </tr>
	                              <tr>
	                                <th scope="row">Email Address</th>
	                                <td>: <?=$profile->trainer_email?></td>
	                              </tr>
	                              <tr>
	                                <th scope="row">Phone Number</th>
	                                <td>: <?=$profile->trainer_mobile?></td>
	                              </tr>
	                              <tr>
	                                <th scope="row">CPR</th>
	                                <td>: <?=$profile->trainer_cpr?></td>
	                              </tr>
	                              <tr>
	                                <th scope="row">Education</th>
	                                <td>: <?=$profile->trainer_education?></td>
	                              </tr>
	                              <tr>
	                                <th scope="row">Experience</th>
	                                <td>: <?=$profile->trainer_experience?></td>
	                              </tr>
	                              <tr>
	                                <th scope="row">Address</th>
	                                <td>: <?=$profile->trainer_contact?></td>
	                              </tr>

	                            </tbody>
	                          </table>   
                           </div>
                        </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('teacher/includes/scripts.php'); ?>
      <?php $this->load->view('teacher/includes/table-script.php'); ?>
	  <script type="text/javascript">
		  $('#add-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button').attr('disabled',true);

			  var mobile = $('#mobile-number').val();
			  var password = $('#password').val();
			  var arabic = $('#name-arabic').val();
			  if (mobile.length != 8 || password.length < 7) {
				  if ( mobile.length != 8 ) {
					  toastr.error('Mobile number should contains 8 numbers');
				  }
				  else {
					  toastr.error('Password should contains atleast 6 characters');
				  }
				  $('#submit-button').attr('disabled',false);
			  }
			  else {
				  var isArabic = /^([\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufbc1]|[\ufbd3-\ufd3f]|[\ufd50-\ufd8f]|[\ufd92-\ufdc7]|[\ufe70-\ufefc]|[\ufdf0-\ufdfd]|[ ])*$/g;
			      if (isArabic.test(arabic)){
			           document.getElementById("add-form").submit();
			      }
			      else {
			          toastr.error("Field contains Non-arabic letters","");
			          $('#submit-button').attr('disabled',false);
			      }
			  }
		  });
		  $('#upload').on('change', function () {
		    var file = $("#upload")[0].files[0];
		    var val = file.type;
		    var type = val.substr(val.indexOf("/") + 1);
		    if (type == 'png' || type == 'jpg' || type == 'jpeg') {
		      var reader = new FileReader();
		        reader.onload = function (e) {
		          $uploadCrop.croppie('bind', {
		            url: e.target.result
		          }).then(function(){
		            console.log('jQuery bind complete');
		          });
		        }
		        reader.readAsDataURL(this.files[0]);
		    }
		    else {
		      document.getElementById("upload").value = "";
			  toastr.error("Failed to upload image, the format is not supported","");
		    }
		  });
	  </script>
   </body>
</html>
