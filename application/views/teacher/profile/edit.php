<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('teacher/includes/includes.php'); ?>
      <?php $this->load->view('teacher/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('teacher/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-users" style="line-height: 48px;padding-left: 2px;"></i>Edit Profile</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Edit Profile</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
                              <form action="<?=site_url('trainer/profile/editData')?>" method="post" id="edit-form" enctype="multipart/form-data">
								  <input type="hidden" name="trainer_id" value="<?=$trainer->trainer_id?>">
								  <div class="row">
	                                 <div class="form-group col-sm-6">
	                                    <label for="exampleInputCountry">Name english</label>
	                                    <input type="text" class="form-control" name="trainer_name_english" value="<?=$trainer->trainer_name_english?>" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
	                                 </div>
	                                 <div class="form-group col-sm-6">
	                                    <label for="exampleInputCountry">Name arabic</label>
	                                    <input type="text" class="form-control" id="name-arabic" value="<?=$trainer->trainer_name_arabic?>" name="trainer_name_arabic">
	                                 </div>
	                              </div>
	                              <div class="row">
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCity">CPR</label>
	                                    <input type="text" class="form-control" name="trainer_cpr" value="<?=$trainer->trainer_cpr?>">
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Education</label>
	                                    <input type="text" name="trainer_education" class="form-control" value="<?=$trainer->trainer_education?>">
	                                 </div>
									 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Experience</label>
	                                    <input type="text" name="trainer_experience" class="form-control" value="<?=$trainer->trainer_experience?>">
	                                 </div>
	                              </div>
	                              <div class="row">
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCity">Address</label>
	                                    <textarea name="trainer_contact" rows="3" cols="80" class="form-control"><?=$trainer->trainer_contact?></textarea>
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputAddress">Mobile number</label>
	                                    <input type="text" name="trainer_mobile" id="mobile-number" class="form-control" value="<?=$trainer->trainer_mobile?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>
	                                 </div>
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCountry">Email address</label>
	                                    <input type="email" name="trainer_email" class="form-control" value="<?=$trainer->trainer_email?>">
	                                 </div>
	                              </div>
	                              <div class="row">
	                                 <div class="form-group col-sm-4">
	                                    <label for="exampleInputCity">Image ( Choose image if you wish to change the image )</label>
	                                    <input type="file" name="image" id="upload" class="form-control">
	                                 </div>
	                              </div>
	                              <div class="row">
	                                 <div class="form-group col-sm-4">
										 <button type="submit" class="btn btn-primary" id="submit-button-edit">Update</button>
	                                 </div>
	                              </div>
                              </form>
                           </div>
                        </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('teacher/includes/scripts.php'); ?>
      <?php $this->load->view('teacher/includes/table-script.php'); ?>
	  <script type="text/javascript">
		  $('#edit-form').on('submit', function(e){
			  e.preventDefault();
			  $('#submit-button-edit').attr('disabled',true);

			  var mobile = $('#mobile-number').val();
			  var arabic = $('#name-arabic').val();
			  if (mobile.length != 8) {
				  toastr.error('Mobile number should contains 8 numbers');
				  $('#submit-button-edit').attr('disabled',false);
			  }
			  else {
				  var isArabic = /^([\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufbc1]|[\ufbd3-\ufd3f]|[\ufd50-\ufd8f]|[\ufd92-\ufdc7]|[\ufe70-\ufefc]|[\ufdf0-\ufdfd]|[ ])*$/g;
			      if (isArabic.test(arabic)){
			           document.getElementById("edit-form").submit();
			      }
			      else {
			          toastr.error("Field contains Non-arabic letters","");
			          $('#submit-button-edit').attr('disabled',false);
			      }
			  }
		  });
		  $('#upload').on('change', function () {
		    var file = $("#upload")[0].files[0];
		    var val = file.type;
		    var type = val.substr(val.indexOf("/") + 1);
		    if (type == 'png' || type == 'jpg' || type == 'jpeg') {
		      $(".upload-div").css("display", "block");
		      $("#submit-button").css("display", "none");
		      var reader = new FileReader();
		        reader.onload = function (e) {
		          $uploadCrop.croppie('bind', {
		            url: e.target.result
		          }).then(function(){
		            console.log('jQuery bind complete');
		          });
		        }
		        reader.readAsDataURL(this.files[0]);
		    }
		    else {
		      document.getElementById("upload").value = "";
			  toastr.error("Failed to upload image, the format is not supported","");
		    }
		  });
	  </script>
   </body>
</html>
