<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('admin/includes/includes.php'); ?>
	  <?php $this->load->view('admin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-graduation-cap" style="line-height: 48px;padding-left: 2px;"></i> Completed events</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Completed events</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">

		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">
								 <button type="button" class="btn btn-default btn-lg margin-bottom-20" onclick="window.location='<?=site_url("admin/events/add");?>'">Add event</button>
		                     </div>
		                   </div>
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
								 <table id="user_data1" class="table table-datatable table-custom my-table">
			                      <thead>
			                        <tr>
			                          <th width="10%">Image</th>
			                          <th width="20%">Title</th>
			                          <th width="40%">Description</th>
			                          <th width="10%">Start</th>
									  <th width="10%">End</th>
			                          <th width="5%">Status</th>
			                          <th width="5%">Options</th>
			                        </tr>
			                      </thead>
			  					<tbody>
			  						<?php foreach ($events as $event) { ?>
			  							<tr>
			  								<td><img src="<?=base_url() . $event->image?>" width="100px"></td>
			  								<td><?=$event->title?></td>
			  								<td><?=$event->description?></td>
			  								<td><?=$event->start_time?></td>
											<td><?=$event->end_time?></td>
			  								<td>
												<?php if ($event->status) { ?>
													<a href="<?=site_url('admin/events/status/c/' . $event->event_id . '/0')?>" class="btn btn-danger btn-xs">Block</a><br>Active
												<?php }else { ?>
													<a href="<?=site_url('admin/events/status/c/' . $event->event_id . '/1')?>" class="btn btn-success btn-xs">Activate</a><br>Blocked
												<?php } ?>
			  								</td>
			  								<td>
												<a href="<?=site_url('admin/events/edit/' . $event->event_id)?>" class="btn btn-primary btn-sm " onclick="edit(<?=$event->event_id?>)">Edit</a>
			                                    <a href="<?=site_url('admin/events/delete/' . $event->event_id)?>" class="btn btn-danger btn-sm ">Delete</a>
											</td>
			  							</tr>
			  						<?php } ?>
			  					</tbody>
			                    </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
	  <?php $this->load->view('admin/includes/table-script.php'); ?>
	  <script>
	      $(document).ready(function(){
        var tbl = $('.my-table');
        var settings = {
          "columnDefs":[
            {
              "target":[0,3,4],
              "orderable":true
            }
          ],
          dom: 'lBfrtip',
          buttons: [
              {
                  extend:'pdfHtml5',
                  text:'Pdf',
                  exportOptions: {
                    columns: [ 1, 2, 3, 4 ]
                  },
                  orientation:'landscape',
                  customize: function (doc) {
                    doc.content[1].table.widths =
                        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                  }
              },
              {
                  extend: 'print',
                  text: 'Print',
                  exportOptions: {
                    columns: [ 1, 2, 3, 4 ]
                  },
              },
              { extend: 'csv',text: 'Csv' },
          ],
          lengthMenu: [[25, 100, -1], [25, 100, "All"]],
          pageLength: 25,
          customize: function (doc) {
              doc.content[1].table.widths =
                  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        };
      var dataTable = $('#user_data1').DataTable(settings);

    });
	  </script>
   </body>
</html>
