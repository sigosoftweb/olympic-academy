<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('admin/includes/includes.php'); ?>
      <?php $this->load->view('admin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i><?=$exam_name?> - Exam result</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Add Course</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
							   <table class="table table-striped table-bordered" style="width :100%;">
					              <thead>
					                <th width="10%">Name</th>
					                   <th width="10%">Questions</th>
					                   <th width="10%">Answered</th>
					                   <th width="10%">Skipped</th>
					                   <th width="10%">Correct<br>Answers</th>
					                   <th width="10%">Wrong<br>Answers</th>
					                   <th width="10%">Mark</th>
					                   <th width="10%">Total</th>
					                   <th width="10%">Time taken</th>
					              </thead>
								  <tbody>
								  	<?php foreach ($leaders as $leader) { ?>
								  		<tr>
								  			<td><?=$leader->stu_name_english . ' ' . $leader->stu_name_arabic?></td>
											<td><?=$leader->questions?></td>
											<td><?=$leader->attended?></td>
											<td><?=$leader->left_questions?></td>
											<td><?=$leader->correct_answers?></td>
											<td><?=$leader->wrong_answers?></td>
											<td><?=$leader->mark?></td>
											<td><?=$leader->total_mark?></td>
											<td><?=$leader->time_taken?></td>
								  		</tr>
								  	<?php } ?>
								  </tbody>
					            </table>
                           </div>
                        </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
      <?php $this->load->view('admin/includes/table-script.php'); ?>
   </body>
</html>
