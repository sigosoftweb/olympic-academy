<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
      <?php $this->load->view('admin/includes/includes.php'); ?>
      <?php $this->load->view('admin/includes/table-css.php'); ?>

      <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/chosen/css/chosen.min.css">
      <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/chosen/css/chosen-bootstrap.css">
	  <link rel="stylesheet" href="<?=base_url()?>assets/js/vendor/datepicker/css/bootstrap-datetimepicker.css">
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-user" style="line-height: 48px;padding-left: 2px;"></i>Course : <?=$course->course_title?></h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Add Course</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
                        <section class="tile color transparent-black">
                           <div class="tile-body">
							   <form class="new-added-form" method="POST" action="<?=site_url('admin/exams/editExam')?>" id="add-form">
								   <input type="hidden" name="course_id" value="<?=$course->course_id?>">
								   <input type="hidden" name="exam_id" value="<?=$exam->exam_id?>">
                                   <input type="hidden" name="question_number" value="<?=$question_number?>">
                                     <div class="row">
                                         <div class="col-md-6 form-group">
                                             <label>Exam name *</label>
                                             <input type="text" class="form-control" name="name" value="<?=$exam->exam_name?>" required>
                                         </div>
                                         <div class="col-md-3 form-group">
                                             <label>Number of questions *</label>
                                             <input type="number" min="0" max="1000" class="form-control" name="no_of_questions" id="no_of_questions" value="<?=$exam->no_questions?>" required>
                                         </div>
                                         <div class="col-md-3 form-group">
                                             <label>Time (In minutes) *</label>
                                             <input type="number" min="0" max="1000" class="form-control" name="exam_time" value="<?=$exam->exam_time?>" required>
                                         </div>
                                         <?php if ($status) { ?>
                                            <div class="col-md-6 form-group">
                                             <label>Batch</label>
                                             <select class="form-control" name="cb_id">
												 <option value="0">For all batch</option>
												 <?php foreach ($batches as $batch) { ?>
												 	<option value="<?=$batch->cb_id?>" <?php if($exam->cb_id == $batch->cb_id){?>selected<?php } ?>><?=$batch->batch_name?></option>
												 <?php } ?>
                                             </select>
                                            </div>
                                         <?php }else { ?>
                                            <div class="col-md-6 form-group">
                                             <label>Batch ( Since students already attended the exam you cannot change batch )</label>
                                             <input type="text" class="form-control" value="<?=$batch_name?>">
                                             <input type="hidden" name="cb_id" value="<?=$exam->cb_id?>">
                                         </div>
                                         <?php }?>
                                         <?php if ($status) { ?>
                                            <div class="col-md-6 form-group">
                                             <label>Sections</label>
                                             <select class="form-control" name="cs_id">
												 <option value="0">No section</option>
												 <?php foreach ($sections as $section) { ?>
												 	<option value="<?=$section->cs_id?>" <?php if($exam->cs_id == $section->cs_id){?>selected<?php } ?>><?=$section->section_title?></option>
												 <?php } ?>
                                             </select>
                                         </div>
                                         <?php }else { ?>
                                            <div class="col-md-6 form-group">
                                             <label>Batch ( Since students already attended the exam you cannot change section )</label>
                                             <input type="text" class="form-control" value="<?=$section_name?>">
                                             <input type="hidden" name="cs_id" value="<?=$exam->cs_id?>">
                                         </div>
                                         <?php }?>
										 
                                         <?php if ($status) { ?>
                                            <div class="col-lg-12 col-12 form-group">
                                                <label>Set time</label>
                                                <label class="radio-inline"><input type="radio" name="time" id="timeTrue" value="1" <?php if($exam->time_status == '1'){ ?>checked<?php } ?>>&nbsp; Yes</label>
                                                <label class="radio-inline"><input type="radio" name="time" id="timeFalse" value="0" <?php if($exam->time_status == '0'){ ?>checked<?php } ?>>&nbsp;  No  </label>
                                            </div>
                                            <div id="time-div" <?php if($exam->time_status == '1'){ ?> style="display:block;" <?php }else{ ?>style="display:none;"<?php } ?>>
                                                <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Starting time</label>
                                                                <input type="text" class="form-control" id="datepicker" name="from_time" <?php if($exam->time_status == '1'){ ?> value="<?=date('d/m/Y h:i A',strtotime($exam->from_time))?>" <?php } ?> autocomplete="off">
                                                            </div>
                                                        </div>
                                            </div>
                                        <?php }else { ?>
                                            <div class="col-md-6">
                                                <h6 style="color:red">Since students already attended the exam you cannot set time for this exam</h6>
                                            </div>
                                        <?php } ?>
                                         
                                         <div class="col-md-12 form-group">
                                             <button type="submit" class="btn btn-primary" id="submit-button">Edit</button>
                                         </div>
                                     </div>
                                 </form>
                           </div>
                        </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
      <?php $this->load->view('admin/includes/table-script.php'); ?>
      <script src="<?=base_url()?>assets/js/vendor/chosen/chosen.jquery.min.js"></script>
	  <script src="<?=base_url()?>assets/js/vendor/datepicker/bootstrap-datetimepicker.min.js"></script>
	  <script type="text/javascript">
		  $('#add-form').on('submit', function(e){
			  e.preventDefault();
              $('#submit-button').attr('disabled',true);
              var number = $('#no_of_questions').val();
                var current = <?=$question_number?>;
                if (number < current) {
                    toastr.error('Already added ' + current + ' number of questions in this exam. For decrementing the number of questions please delete questions and try again',"");
                    $('#submit-button').attr('disabled',false);
                }
                else {
                    $('#submit-button').attr('disabled',false);
                    document.getElementById("add-form").submit();
                }
		  });
		  function changeFormat(date)
          {
			  arr = date.split(" ");
              if (arr[2] == 'PM') {
                  tm = arr[1].split(":");
                  if (tm[0] == 12) {
                      hour = tm[0];
                  }
                  else {
                      hour = +12 + +tm[0];
                  }
                  time = hour + ':' + tm[1] + ':00';
              }
              else {
                  time = arr[1] + ':00';
              }
              return arr[0].split("/").reverse().join("-") + ' ' + time;
          }
		  $(document).on('change','input:radio[name="time"]',function(){
            if (document.getElementById('timeTrue').checked) {
                $("#datepicker").prop('required',true);
                $('#time-div').css('display','block');
            }
            if (document.getElementById('timeFalse').checked) {
                $("#datepicker").prop('required',false);
                $('#time-div').css('display','none');
            }
        });
		  $(function(){
			  $(".chosen-select").chosen({disable_search_threshold: 10});
			  $('#datepicker').datetimepicker({
				  format:'DD/MM/YYYY HH:mm A',
		          icons: {
		              time: "fa fa-clock-o",
		              date: "fa fa-calendar",
		              up: "fa fa-arrow-up",
		              down: "fa fa-arrow-down"
		          }
		      });
		      $("#datepicker").on("dp.show",function (e) {
		        var newtop = $('.bootstrap-datetimepicker-widget').position().top - 45;
		        $('.bootstrap-datetimepicker-widget').css('top', newtop + 'px');
		      });
		  });
		  function addRow()
          {
	          var col1 = "<tr><td><input type='text' class='form-control' name='instructions[]' required></td>";
	          var col2 = "<td><a class='btn btn-link' onclick='deleteRow(this);'><i style='font-size:25px; color:red;' class='fa fa-minus-circle'></i></a></td></tr>";
	          var row = col1 + col2;
	          $('#options').append(row);
          }
          function deleteRow(row)
          {
          	  $(row).closest('tr').remove();
          }
	  </script>
   </body>
</html>
