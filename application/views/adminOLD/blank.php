<!DOCTYPE html>
<html>
   <head>
      <title>Minimal 1.3 - Blank Page</title>
      <?php include 'includes/includes.php'; ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php include 'includes/sidebar.php'; ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-file-o" style="line-height: 48px;padding-left: 2px;"></i> Blank Page <span>// Place subtitle here...</span></h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>You are here</li>
                        <li><a href="index.html">Minimal</a></li>
                        <li><a href="#">Example Pages</a></li>
                        <li class="active">Blank Page</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">

                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <section class="videocontent" id="video"></section>
      <?php include 'includes/scripts.php'; ?>
   </body>
</html>
