<script src="<?=base_url()?>assets/js/jquery.js"></script>
<script src="<?=base_url()?>assets/js/vendor/bootstrap/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/js/vendor/bootstrap/bootstrap-dropdown-multilevel.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/vendor/mmenu/js/jquery.mmenu.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/vendor/sparkline/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/vendor/nicescroll/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/vendor/animate-numbers/jquery.animateNumbers.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/vendor/videobackground/jquery.videobackground.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/vendor/blockui/jquery.blockUI.js"></script>
<script src="<?=base_url()?>assets/js/vendor/momentjs/moment-with-langs.min.js"></script>
<script src="<?=base_url()?>assets/js/minimal.min.js"></script>
<script src="<?=base_url()?>assets/plugins/toaster/toaster.min.js"></script>
<script type="text/javascript">
	toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "newestOnTop": false,
	  "progressBar": true,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": false,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": "5000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}
	<?php
		$alert = $this->session->flashdata('alert_type');
		if ($alert) { ?>
			toastr.<?=$this->session->flashdata('alert_type')?>("<?=$this->session->flashdata('alert_message')?>","<?php echo $this->session->flashdata('alert_title')?>");
		<?php }
	?>
</script>
