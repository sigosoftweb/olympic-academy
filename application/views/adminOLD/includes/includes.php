<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8" />
<link rel="icon" type="image/ico" href="<?=base_url()?>assets/images/favicon.png" />
<link href="<?=base_url()?>assets/css/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?=base_url()?>assets/css/vendor/animate/animate.css">
<link type="text/css" rel="stylesheet" media="all" href="<?=base_url()?>assets/js/vendor/mmenu/css/jquery.mmenu.all.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/css/vendor/bootstrap-checkbox.css">
<link rel="stylesheet" href="<?=base_url()?>assets/css/vendor/bootstrap/bootstrap-dropdown-multilevel.css">
<link href="<?=base_url()?>assets/css/minimal.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/toaster/toaster.min.css">
<link href="<?=base_url()?>assets/css/custom.css" rel="stylesheet">