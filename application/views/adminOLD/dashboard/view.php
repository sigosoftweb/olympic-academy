<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('admin/includes/includes.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-tachometer" style="line-height: 48px;padding-left: 2px;"></i> Dashboard</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Dashboard</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
				   <div class="row cards">

 	                <div class="card-container col-lg-3 col-sm-6 col-sm-12">
 	                  <div class="card card-greensea hover">
 	                    <div class="front">

 	                      <div class="media">
 	                        <span class="pull-left">
 	                          <i class="fa fa-user media-object"></i>
 	                        </span>

 	                        <div class="media-body">
 	                          <small>Students</small>
 	                          <h2 class="media-heading animate-number" data-value="<?=$count_student?>" data-animation-duration="1500">0</h2>
 	                        </div>
 	                      </div>
 	                    </div>
 	                  </div>
 	                </div>


 	                <div class="card-container col-lg-3 col-sm-6 col-sm-12">
 	                  <div class="card card-blue hover">
 	                    <div class="front">
 	                      <div class="media">
 	                        <span class="pull-left">
 	                          <i class="fa fa-users media-object"></i>
 	                        </span>
 	                        <div class="media-body">
 	                          <small>Trainers</small>
 	                          <h2 class="media-heading animate-number" data-value="<?=$count_trainers?>" data-animation-duration="1500">0</h2>
 	                        </div>
 	                      </div>
 	                    </div>
 	                  </div>
 	                </div>

 	                <div class="card-container col-lg-3 col-sm-6 col-sm-12">
 	                  <div class="card card-greensea hover">
 	                    <div class="front">
 	                      <div class="media">
 	                        <span class="pull-left">
 	                          <i class="fa fa-book media-object"></i>
 	                        </span>
 	                        <div class="media-body">
 	                          <small>Courses</small>
 	                          <h2 class="media-heading animate-number" data-value="<?=$courses?>" data-animation-duration="1500">0</h2>
 	                        </div>
 	                      </div>
 	                    </div>
 	                  </div>
 	                </div>

 	                <div class="card-container col-lg-3 col-sm-6 col-xs-12">
 	                  <div class="card card-slategray hover">
 	                    <div class="front">
 	                      <div class="media">
 	                        <span class="pull-left">
 	                          <i class="fa fa-calendar-o media-object"></i>
 	                        </span>
 	                        <div class="media-body">
 	                          <small>Events</small>
 	                          <h2 class="media-heading animate-number" data-value="<?=$events?>" data-animation-duration="1500">0</h2>
 	                        </div>
 	                      </div>
 	                    </div>
 	                  </div>
 	                </div>

 	              </div>
				  <div class="row cards">

				   <div class="card-container col-lg-3 col-sm-6 col-sm-12">
					 <div class="card card-greensea hover">
					   <div class="front">

						 <div class="media">
						   <span class="pull-left">
							 <i class="fa fa-file-text media-object"></i>
						   </span>

						   <div class="media-body">
							 <small>Exams</small>
							 <h2 class="media-heading animate-number" data-value="<?=$exams?>" data-animation-duration="1500">0</h2>
						   </div>
						 </div>
					   </div>
					 </div>
				   </div>


				   <div class="card-container col-lg-3 col-sm-6 col-sm-12">
					 <div class="card card-blue hover">
					   <div class="front">
						 <div class="media">
						   <span class="pull-left">
							 <i class="fa fa-money media-object"></i>
						   </span>
						   <div class="media-body">
							 <small>Today's earnings</small>
							 <h2 class="media-heading animate-number" data-value="<?=$today?>" data-animation-duration="1500">0</h2>
						   </div>
						 </div>
					   </div>
					 </div>
				   </div>

				   <div class="card-container col-lg-3 col-sm-6 col-sm-12">
					 <div class="card card-greensea hover">
					   <div class="front">
						 <div class="media">
						   <span class="pull-left">
							 <i class="fa fa-money media-object"></i>
						   </span>
						   <div class="media-body">
							 <small>Monthly earnings</small>
							 <h2 class="media-heading animate-number" data-value="<?=$monthly?>" data-animation-duration="1500">0</h2>
						   </div>
						 </div>
					   </div>
					 </div>
				   </div>

				   <div class="card-container col-lg-3 col-sm-6 col-xs-12">
					 <div class="card card-slategray hover">
					   <div class="front">
						 <div class="media">
						   <span class="pull-left">
							 <i class="fa fa-money media-object"></i>
						   </span>
						   <div class="media-body">
							 <small>Lifetime earning</small>
							 <h2 class="media-heading animate-number" data-value="<?=$lifetime?>" data-animation-duration="1500">0</h2>
						   </div>
						 </div>
					   </div>
					 </div>
				   </div>

				 </div>
				 <div class="row">
				 	<div class="col-md-6">
						<section class="tile transparent recent-activity">
		                  <div class="tile-header transparent">
		                    <h1><strong>New</strong> Students</h1>
		                  </div>
		                  <div class="tile-body tab-content nopadding rounded-bottom-corners">
		                    <ul id="users-tab" class="tab-pane fade in active">
								<?php foreach ($students as $student) { ?>
									<li>
										<?php if ($student->stu_image != '') { ?>
											<img src="<?=base_url() . $student->stu_image?>" class="w35" alt>
										<?php }else { ?>
											<img src="<?=base_url() . 'uploads/students/user.png'?>" class="w35" alt>
										<?php } ?>

			                          <span class="user font-slab"><?=$student->stu_name_english?></span>
			                          <span class="subject"><strong><?=$student->stu_mobile?></strong></span>
			                          <span class="time"><i class="fa fa-clock-o"></i> <?=date('d/m/Y h:i A',strtotime($student->timestamp))?></span>
			                        </li>
								<?php } ?>
		                    </ul>
		                  </div>
		                </section>
				 	</div>
					<div class="col-md-6">
						<section class="tile transparent recent-activity">
		                  <div class="tile-header transparent">
		                    <h1><strong>New</strong> Trainers</h1>
		                  </div>
		                  <div class="tile-body tab-content nopadding rounded-bottom-corners">
		                    <ul id="users-tab" class="tab-pane fade in active">
								<?php foreach ($trainers as $trainer) { ?>
									<li>
										<?php if ($trainer->trainer_image != '') { ?>
											<img src="<?=base_url() . $trainer->trainer_image?>" class="w35" alt>
										<?php }else { ?>
											<img src="<?=base_url() . 'uploads/students/user.png'?>" class="w35" alt>
										<?php } ?>

			                          <span class="user font-slab"><?=$trainer->trainer_name_english?></span>
			                          <span class="subject"><strong><?=$trainer->trainer_mobile?></strong></span>
			                          <span class="time"><i class="fa fa-clock-o"></i> <?=date('d/m/Y h:i A',strtotime($trainer->timestamp))?></span>
			                        </li>
								<?php } ?>
		                    </ul>
		                  </div>
		                </section>
				 	</div>
					<div class="col-md-6">
						<section class="tile transparent recent-activity">
		                  <div class="tile-header transparent">
		                    <h1><strong>Last</strong> Transactions</h1>
		                  </div>
		                  <div class="tile-body tab-content nopadding rounded-bottom-corners">
		                    <ul id="users-tab" class="tab-pane fade in active">
								<?php foreach ($transactions as $trans) { ?>
									<li>
										<?php if ($trans->stu_image != '') { ?>
											<img src="<?=base_url() . $student->stu_image?>" class="w35" alt>
										<?php }else { ?>
											<img src="<?=base_url() . 'uploads/students/user.png'?>" class="w35" alt>
										<?php } ?>
			                          <span class="subject"><?=$trans->stu_name_english?></span> -
									  <span class="subject"><?=$trans->course_title?></span> -
			                          <span class="subject"><strong><?=$trans->amount . ' BHD'?></strong></span>

			                        </li>
								<?php } ?>
		                    </ul>
		                  </div>
		                </section>
				 	</div>
				 </div>
               </div>
            </div>
         </div>
      </div>
      <section class="videocontent" id="video"></section>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
   </body>
</html>
