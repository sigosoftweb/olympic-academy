<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('admin/includes/includes.php'); ?>
	  <?php $this->load->view('admin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-university" style="line-height: 48px;padding-left: 2px;"></i> Sub admin Previleges</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Sub admin Previleges</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						         <section class="tile transparent">
		                   <!-- tile header -->
		                   <div class="tile-header transparent">		        
		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom" id="datatable">
								              <thead>
  		                           <tr>
  		                             <th width="40%">Items</th>
  		                             <th width="10%">View</th>
                                   <th width="10%">Add</th>
                                   <th width="10%">Edit</th>
                                   <th width="10%">Bolck/unblock</th>
  		                             <th width="10%">Delete</th>
                                   <th width="10%">All</th>
  		                           </tr>
  		                         </thead>
  		                         <tbody>
              									 <?php foreach ($modules as $module) { ?>
              									 	<tr>
              									 		<td><?=$module->item?></td>

              											<td>
              												<?php if ($module->view) { ?>
              													Active<br>
              													<a href="<?=site_url('admin/subadmin/disable/' . $module->sp_id . '/v')?>" class="btn btn-danger btn-xs margin-bottom-20">Block</a>
              												<?php }else { ?>
              													Blocked<br>
              													<a href="<?=site_url('admin/subadmin/enable/' . $module->sp_id . '/v')?>" class="btn btn-success btn-xs margin-bottom-20">Activate</a>
              												<?php } ?>
              											</td>

                                    <td>
                                      <?php if ($module->add) { ?>
                                        Active<br>
                                        <a href="<?=site_url('admin/subadmin/disable/' . $module->sp_id . '/a')?>" class="btn btn-danger btn-xs margin-bottom-20">Block</a>
                                      <?php }else { ?>
                                        Blocked<br>
                                        <a href="<?=site_url('admin/subadmin/enable/' . $module->sp_id . '/a')?>" class="btn btn-success btn-xs margin-bottom-20">Activate</a>
                                      <?php } ?>
                                    </td>

                                    <td>
                                      <?php if ($module->edit) { ?>
                                        Active<br>
                                        <a href="<?=site_url('admin/subadmin/disable/' . $module->sp_id . '/e')?>" class="btn btn-danger btn-xs margin-bottom-20">Block</a>
                                      <?php }else { ?>
                                        Blocked<br>
                                        <a href="<?=site_url('admin/subadmin/enable/' . $module->sp_id . '/e')?>" class="btn btn-success btn-xs margin-bottom-20">Activate</a>
                                      <?php } ?>
                                    </td>

                                    <td>
                                      <?php if ($module->block) { ?>
                                        Active<br>
                                        <a href="<?=site_url('admin/subadmin/disable/' . $module->sp_id . '/b')?>" class="btn btn-danger btn-xs margin-bottom-20">Block</a>
                                      <?php }else { ?>
                                        Blocked<br>
                                        <a href="<?=site_url('admin/subadmin/enable/' . $module->sp_id . '/b')?>" class="btn btn-success btn-xs margin-bottom-20">Activate</a>
                                      <?php } ?>
                                    </td>

                                    <td>
                                      <?php if ($module->delete) { ?>
                                        Active<br>
                                        <a href="<?=site_url('admin/subadmin/disable/' . $module->sp_id . '/d')?>" class="btn btn-danger btn-xs margin-bottom-20">Block</a>
                                      <?php }else { ?>
                                        Blocked<br>
                                        <a href="<?=site_url('admin/subadmin/enable/' . $module->sp_id . '/d')?>" class="btn btn-success btn-xs margin-bottom-20">Activate</a>
                                      <?php } ?>
                                    </td>

                                    <td>
                                      <?php if ($module->all=='1') { ?>
                                        Active<br>
                                        <a href="<?=site_url('admin/subadmin/disable/' . $module->sp_id . '/w')?>" class="btn btn-danger btn-xs margin-bottom-20">Block</a>
                                      <?php }elseif($module->all=='0') { ?>
                                        Blocked<br>
                                        <a href="<?=site_url('admin/subadmin/enable/' . $module->sp_id . '/w')?>" class="btn btn-success btn-xs margin-bottom-20">Activate</a>
                                       
                                      <?php } else { ?>
                                       None<br>
                                       <a href="<?=site_url('admin/subadmin/disable/' . $module->sp_id . '/w')?>" class="btn btn-danger btn-xs margin-bottom-20">Block</a><br>
                                       <a href="<?=site_url('admin/subadmin/enable/' . $module->sp_id . '/w')?>" class="btn btn-success btn-xs margin-bottom-20">Activate</a>
                                      <?php };?>  
                                    </td>
              											
              									 	</tr>
              									 <?php } ?>
  		                         </tbody>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <section class="videocontent" id="video"></section>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
	  <?php $this->load->view('admin/includes/table-script.php'); ?>
	  <script>
	  	$('#add-form').on('submit', function(e){
		  e.preventDefault();
		  $('#submit-button').attr('disabled',true);
		  document.getElementById("add-form").submit();
	  	});
		function edit(org_id)
	    {
	        $('#org_id').val(org_id);
	        $.ajax({
	          method: "POST",
	          url: "<?=site_url('admin/subadmin/getDetails');?>",
	          data : { org_id : org_id },
	          dataType : "json",
	          success : function( data ){
	              $('#organization').val(data.organization);
	              $('#edit-organization').modal('show');
	          }
	        });
	    }
	  </script>
   </body>
</html>
