<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('admin/includes/includes.php'); ?>
	  <?php $this->load->view('admin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-users" style="line-height: 48px;padding-left: 2px;"></i> Sub admin</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Sub admin</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">
		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">
								 <a class="btn btn-default btn-lg margin-bottom-20" href="<?=site_url('admin/subadmin/add')?>"><span>Add admin</span></a>
		                     </div>
		                   </div>
		                   <!-- /tile header -->
		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom" id="user_data">
								   <thead>
  		                           <tr>
									 <th width="10%">Image</th>
  		                             <th width="10%">English name</th>
									 <th width="10%">Contact</th>
  		                             <th width="10%">Cpr</th>
									 <th width="10%">Address</th>
									 <th width="10%">Previleges</th>
									 <th width="10%">Status</th>
									 <th width="5%">Edit</th>
									 <th width="5%">Password</th>
  		                           </tr>
  		                         </thead>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <div class="modal fade" id="change-password" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
              <h3 class="modal-title" id="modalConfirmLabel"><strong>Change</strong> Password</h3>
            </div>
            <div class="modal-body">
              <form role="form" id="change-form" action="<?=site_url('admin/subadmin/change')?>" method="post">
				  <input type="hidden" name="admin_id" id="admin_id">
                <div class="form-group">
                  <label for="placeholderInput">New Password</label>
                  <input type="text" class="form-control" id="pass-1" required>
                </div>
				<div class="form-group">
                  <label for="placeholderInput">Confirm Password</label>
                  <input type="text" name="password" class="form-control" id="pass-2" required>
                </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
              <button type="submit" class="btn btn-green" id="submit-button">Save changes</button>
            </div>
			</form>
          </div>
        </div>
      </div>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
	  <?php $this->load->view('admin/includes/table-script.php'); ?>
	  <script type="text/javascript">
	    $(document).ready(function(){
        var tbl = $('.my-table');
        var settings = {
          "processing":true,
          "serverSide":true,
          "order":[],
          "ajax":{
            url:"<?=site_url('admin/subadmin/get')?>",
            type:"POST"
          },
          "columnDefs":[
            {
              "target":[0,3,4],
              "orderable":true
            }
          ],
          dom: 'lBfrtip',
          buttons: [
              {
                  extend:'pdfHtml5',
                  text:'Pdf',
                  exportOptions: {
                    columns: [ 1, 2, 3, 4 ]
                  },
                  orientation:'landscape',
                  customize: function (doc) {
                    doc.content[1].table.widths =
                        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                  }
              },
              {
                  extend: 'print',
                  text: 'Print',
                  exportOptions: {
                    columns: [ 1, 2, 3, 4 ]
                  },
              },
              { extend: 'csv',text: 'Csv' },
          ],
          lengthMenu: [[25, 100, -1], [25, 100, "All"]],
          pageLength: 25,
          customize: function (doc) {
              doc.content[1].table.widths =
                  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        };
      var dataTable = $('#user_data').DataTable(settings);

    });
		$('#change-form').on('submit', function(e){
		  e.preventDefault();
		  $('#submit-button').attr('disabled',true);
		  var pass1 = $('#pass-1').val();
		  var pass2 = $('#pass-2').val();
		  if (pass1 == pass2) {
			  document.getElementById("change-form").submit();
		  }
		  else {
			  toastr.error('The New password and Confirmation password does not match..!');
			  $('#submit-button').attr('disabled',false);
		  }
	  	});
		function changePassword(admin_id)
	    {
			$('#admin_id').val(admin_id);
	        $('#change-password').modal('show');
	    }
	  </script>
   </body>
</html>
