<!DOCTYPE html>
<html>
   <head>
      <title><?=$title?></title>
	  <?php $this->load->view('admin/includes/includes.php'); ?>
	  <?php $this->load->view('admin/includes/table-css.php'); ?>
   </head>
   <body class="solid-bg-6">
      <div class="mask">
         <div id="loader"></div>
      </div>
      <div id="wrap">
         <div class="row">
            <?php $this->load->view('admin/includes/sidebar.php'); ?>
            <div id="content" class="col-md-12">
               <div class="pageheader">
                  <h2><i class="fa fa-graduation-cap" style="line-height: 48px;padding-left: 2px;"></i> Sliders</h2>
                  <div class="breadcrumbs">
                     <ol class="breadcrumb">
                        <li>Olymbic</li>
                        <li>Sliders</li>
                     </ol>
                  </div>
               </div>
               <div class="main">
                  <div class="row">
                     <div class="col-md-12">
						 <section class="tile transparent">


		                   <!-- tile header -->
		                   <div class="tile-header transparent">
		                     <span class="note"></span>
		                     <div class="controls tile-body">
								 <button type="button" class="btn btn-default btn-lg margin-bottom-20" data-toggle="modal" data-target="#add-education">Add slider</button>
		                     </div>
		                   </div>
		                   <!-- /tile header -->

		                   <!-- tile body -->
		                   <div class="tile-body color transparent-black rounded-corners">

		                     <div class="table-responsive">
		                       <table  class="table table-datatable table-custom" id="datatable">
								   <thead>
  		                           <tr>
  		                             <th width="40%">Slider</th>
									 <th width="20%">Name</th>
  		                             <th width="5%">Status</th>
  		                             <th width="5%">Delete</th>
  		                           </tr>
  		                         </thead>
  		                         <tbody>
									 <?php foreach ($sliders as $slider) { ?>
									 	<tr>
											<td><img src="<?=base_url() . $slider->image?>" height="100px"></td>
									 		<td><?=$slider->name?></td>
											<td>
												<?php if ($slider->status) { ?>
													<a href="<?=site_url('admin/sliders/status/0/' . $slider->slider_id )?>" class="btn btn-danger btn-xs margin-bottom-20">Block</a><br>Active
												<?php }else { ?>
													<a href="<?=site_url('admin/sliders/status/1/' . $slider->slider_id )?>" class="btn btn-success btn-xs margin-bottom-20">Activate</a><br>Blocked
												<?php } ?>
											</td>
											<td><a href="<?=site_url('admin/sliders/delete/' . $slider->slider_id)?>" class="btn btn-danger btn-xs margin-bottom-20">Delete</a></td>
									 	</tr>
									 <?php } ?>
  		                         </tbody>
		                       </table>
		                     </div>

		                   </div>
		                 </section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <div class="modal fade" id="add-education" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
              <h3 class="modal-title" id="modalConfirmLabel"><strong>Add</strong> Slider</h3>
            </div>
            <div class="modal-body">
              <form role="form" id="add-form" action="<?=site_url('admin/sliders/add')?>" method="post" enctype="multipart/form-data">

                <div class="form-group">
                  <label for="placeholderInput">Slider name</label>
                  <input type="text" name="name" class="form-control" placeholder="Education name..." onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' required>
                </div>
				<div class="form-group">
                  <label for="placeholderInput">Image ( 1600px * 600px )</label>
                  <input type="file" class="form-control" id="upload" name="image">
                </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-red" data-dismiss="modal" aria-hidden="true">Close</button>
              <button type="submit" class="btn btn-green" id="submit-button">Add slider</button>
            </div>
			</form>
          </div>
        </div>
      </div>

      <section class="videocontent" id="video"></section>
      <?php $this->load->view('admin/includes/scripts.php'); ?>
	  <?php $this->load->view('admin/includes/table-script.php'); ?>
	  <script>
	  	$('#add-form').on('submit', function(e){
		  e.preventDefault();
		  $('#submit-button').attr('disabled',true);
		  document.getElementById("add-form").submit();
	  	});
		$('#upload').on('change', function () {
		  var file = $("#upload")[0].files[0];
		  var val = file.type;
		  var type = val.substr(val.indexOf("/") + 1);
		  if (type == 'png' || type == 'jpg' || type == 'jpeg') {
			return true;
		  }
		  else {
			document.getElementById("upload").value = "";
			toastr.error("Failed to upload image, the format is not supported","");
		  }
		});
	  </script>
   </body>
</html>
