<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Timezone  {

    public function __construct() {
        date_default_timezone_set('Asia/Bahrain');
    }
}