<?php


$lang['cal_january']    = "يناير";
$lang['cal_february']   = "فبراير";
$lang['cal_march']      = "مارس";
$lang['cal_april']      = "أبريل";
$lang['cal_mayl']       = "مايو";
$lang['cal_june']       = "يونيو";
$lang['cal_july']       = "يوليو";
$lang['cal_august']     = "أغسطس";
$lang['cal_september']  = "سبتمبر";
$lang['cal_october']    = "أكتوبر";
$lang['cal_november']   = "تشرين الثاني";
$lang['cal_december']   = "ديسمبر";

$lang['cal_sun'] = 'الاحد';
$lang['cal_mon'] = 'الإثنين';
$lang['cal_tue'] = 'الثلاثاء';
$lang['cal_wed'] = 'الأربعاء';
$lang['cal_thu'] = 'الخميس';
$lang['cal_fri'] = 'الجمعة';
$lang['cal_sat'] = 'السبت';