<?php
function admin() {
    $ci = & get_instance();
    $ci->load->library('session');
    $admin = $ci->session->userdata('admin');
    if (isset($admin)) {
        return true;
    } else {
        return false;
    }
}

function subadmin() {
    $ci = & get_instance();
    $ci->load->library('session');
    $subadmin = $ci->session->userdata('subadmin');
    if (isset($subadmin)) {
        return true;
    } else {
        return false;
    }
}

function getExcerpt($str, $startPos=0, $maxLength=100) {
	if(strlen($str) > $maxLength) {
		$excerpt   = substr($str, $startPos, $maxLength-3);
		$lastSpace = strrpos($excerpt, ' ');
		$excerpt   = substr($excerpt, 0, $lastSpace);
		$excerpt  .= '...';
	} else {
		$excerpt = $str;
	}
	
	return $excerpt;
}

function is_in_array($array, $key, $key_value){
      $within_array = false;
      foreach( $array as $k=>$v ){
        if( is_array($v) ){
            $within_array = is_in_array($v, $key, $key_value);
            if( $within_array == true ){
                break;
            }
        } else {
                if( $v == $key_value && $k == $key ){
                        $within_array = true;
                        break;
                }
        }
      }
      return $within_array;
}

function isUser() {
    $ci =& get_instance();
	$ci->load->library('session');
	$user = $ci->session->userdata('olympicUser');
    if (isset($user)) {
      return true;
    }
    else {
      return false;
    }
}

function isStudent() {
	$ci =& get_instance();
	$ci->load->library('session');
	$user = $ci->session->userdata('olympicUser');
    if (isset($user)) {
    	if($user['userType'] == 'student')
    		return true;
    	else
    		return false;      
    }
    else {
      return false;
    }
}

function teacher() {
    $ci = & get_instance();
    $ci->load->library('session');
    $teacher = $ci->session->userdata('teacher');
    if (isset($teacher)) {
        return true;
    } else {
        return false;
    }
}

function getArabicDate($dt) {
    $dt_english = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'AM', 'PM');
    $dt_arabic = array('يناير', 'فبراير', 'مارس', 'أبريل', 'مايو', 'يونيو', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر', 'صباحًا', 'مساءً');
    return str_ireplace($dt_english, $dt_arabic, $dt);
}

?>
