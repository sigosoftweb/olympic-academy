<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subadmin_login extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('Model_login','login');
	}
	public function index()
	{
		$this->load->view('login/subadmin');
	}
	public function loginCheck()
	{
		$email    = $this->security->xss_clean($this->input->post('email'));
		$password = $pass = $this->security->xss_clean($this->input->post('password'));
		$get = $this->login->get_details('sub_admin',array('email' => $email, 'password' => md5($password),'status'=>'1'));
		if ($get->num_rows() > 0) {
			$user     = $get->row();
			$session  = [
							'user_id'   => $user->sa_id,
							'name'      => $user->name_english,
							'user_type' => 'subadmin',
						];
			$this->session->set_userdata('subadmin',$session);

			$remember = $this->security->xss_clean($this->input->post('remember'));
			if ($rem) {
				$hour = time() + 3600 * 24 * 30;
				setcookie('subadmin_username', $user_name, $hour);
				setcookie('subadmin_password', $pass, $hour);
			}
			else {
				$hour = time() - 3600 * 24 * 30;
				setcookie('subadmin_username', "", $hour);
				setcookie('subadmin_username', "", $hour);
			}
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Logged in Successfully..!');
			redirect('subadmin/dashboard');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Login failed');
			redirect('subadmin_login');
		}
	}
	public function logout()
	{
		setcookie('subadmin_username');
		setcookie('subadmin_username');
		$this->session->unset_userdata('subadmin');
		redirect('subadmin_login');
	}
}
?>
