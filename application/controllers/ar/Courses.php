<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Courses extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('site/arabic/Model_Courses','courses');
        $this->load->model('site/arabic/Model_Students','students');
        $this->load->model('site/arabic/Model_Categories','categories');
    }



    public function index() {        

        $data['courses'] = $this->courses->get_Courses();          
        $data['filter'] = 'all';
        $data['selected_category'] = 'all';
        //$data['students_count'] = $this->courses->get_studentsCount();
        if(isStudent()) {
            $user = $this->session->userdata['olympicUser'];
            $user_id = $user['userID'];
            $data['courseIDs'] = $this->students->get_subscribedCourseIDs($user_id);
        }

        $data['categories'] = $this->categories->get_categories_course_count();
        $this->load->view('site/arabic/courses',$data);

    }    



    public function filters() {
        $data['filter']  = $filter = $this->security->xss_clean($this->input->post('listing_filter'));
        $data['selected_category'] = $selected_category = $this->security->xss_clean($this->input->post('category'));

        if($filter == 'popular' && $selected_category != 0) {
            $data['courses'] = $this->courses->get_popularCourses_byCategory($selected_category);
        }

        if($filter == 'popular' && $selected_category == 0) {
            $data['courses'] = $this->courses->get_popularCourses();
        }         

        if($filter == 'all' && $selected_category != 0) {
            $data['courses'] = $this->courses->get_Courses_byCategory($selected_category);            
        }

        if($filter == 'all' && $selected_category == 0) {
            $data['courses'] = $this->courses->get_Courses();               
        }

        if(isStudent()) {
            $user = $this->session->userdata['olympicUser'];
            $user_id = $user['userID'];
            $data['courseIDs'] = $this->students->get_subscribedCourseIDs($user_id);
        }    

        $data['categories'] = $this->categories->get_categories_course_count();
        $result = $this->load->view('site/arabic/courses-filter-template',$data, TRUE);
        echo $result;

    } 



    public function details($course_id) {
        $data['course'] = $this->courses->get_courseDetails($course_id);
        if(!empty($data['course'])) {

            if(isStudent()) {
                $user = $this->session->userdata['olympicUser'];
                $user_id = $user['userID'];              
                $get = $this->students->getSubscribedCourse($course_id, $user_id);

                if($get->num_rows() > 0)  {

                    $crs = $get->row();
                    if($crs->sp_status == 'Approved')
                    {
                        redirect('ar/subscriptions/details/' . $course_id);
                    }

                    elseif($crs->sp_status == 'Waiting'){

                        $data['subscription_message'] = 'Your subscription is under review. You can goto the course details once admin approves your request.';

                    } else{

                        $data['subscription_message'] = 'Admin has rejected your request due to following reason,<br>' . $crs->reject_reason;
                    }

                } else  {

                    $data['subscription_message'] = false;

                }

                //$data['courseIDs'] = $this->students->get_subscribedCourseIDs($user_id);

            }

            else {

                $data['subscription_message'] = false;

            }

            //$data['sequence'] = $this->courses->get_courseDetails($data['course']->sequence_id);

            $data['sections_count'] = $this->courses->get_sectionsCount($course_id);
            $data['course_sections'] = $this->courses->get_courseSections($course_id);
            $data['trainers'] = $this->courses->get_courseTrainers($course_id);
            $data['organisers'] = $this->courses->get_courseOrganisers($course_id);
            $data['students_count'] = $this->courses->get_studentsCount($course_id);



            //print_r($data['students_count']);
            //echo '<br>' . $data['course']->student_limit;
            //exit;



            $prefs = array(

                'start_day'    => 'sunday',
                'month_type'   => 'long',
                'day_type'     => 'short',              
                'show_other_days'=> TRUE,              
                'template' => array(
                        'table_open'           => '<table class="table table-bordered">',
                        //'cal_cell_start'       => '<td class="day">',
                        //'cal_cell_start_today' => '<td class="today">'
                        'heading_row_start' => '<tr class="month">',
                        'week_row_start' => '<tr class="week">',
                        'cal_row_start' => '<tr class="day">',
                        'cal_cell_content' => '<div>{content}</div>',
                        'cal_cell_no_content' => '<div>{day}</div>',
                        'cal_cell_content_today' => '<div class="highlight">{content}</div>',
                        'cal_cell_no_content_today' => '<div class="highlight">{day}</div>',
                        'cal_cell_start_other' => '<td class="other-month"><div>',

                )

                );

            $this->load->library('calendar', $prefs);       
            $year = date('Y');
            $month = date('n');
            $data['calendar'] = $this->display_calendar($course_id, $year, $month);
            $data['current_calendar'] = array();
            $data['current_calendar']['year'] = $year;
            $data['current_calendar']['month'] = $month;       

            $this->load->view('site/arabic/course-details',$data);

        }

        else {

            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Loading Failed!');
            redirect('ar/courses');

        }

    }


    public function display_calendar($course_id, $year, $month) {    

        $calendar_data = array();

        /*--------------------Classes-----------------*/
        $classes = $this->courses->get_upcomingClassesMonth($course_id, $year, $month);
        foreach ($classes as $class) {  

            $day = intval(date("d", strtotime($class->starting_time)));               
            if(array_key_exists($day, $calendar_data)) {
                $calendar_data[$day] .= '<p class="class-yellow" data-toggle="tooltip" title="'. $class->topic_arabic.'"><i class="icon_mic_alt"></i>' . getArabicDate(date('h:i a' ,strtotime($class->starting_time))) . '</p>';
            } else {
                $calendar_data[$day] = '<span>'.$day.'</span><p class="class-yellow" data-toggle="tooltip" title="' .$class->topic_arabic.'"><i class="icon_mic_alt"></i>' . getArabicDate(date('h:i a' ,strtotime($class->starting_time))) . '</p>';
            }               

        }          

        /*--------------------------Exams-------------------------*/            

        $exams = $this->courses->get_upcomingExamsMonth($course_id, $year, $month); 
        $today = date('Y-m-d H:i:s');
        foreach ($exams as $exam) {
            $day = intval(date("d", strtotime($exam->from_time)));
            $valid_exam = FALSE;
            if ($exam->time_status == '1') {
                    if (strtotime($today) < strtotime($exam->to_time)) {
                        $valid_exam = TRUE;
                    }
            } else {
                $valid_exam = TRUE;
            }

            if($valid_exam) {               

                if(array_key_exists($day, $calendar_data)) {

                    $calendar_data[$day] .= '<p data-toggle="tooltip" title="'.$exam->exam_name_arabic.'" class="exam-blue" ><i class="icon_pencil-edit"></i>' . getArabicDate(date('h:i a' ,strtotime($exam->from_time))) . '</p>';

                } else {
                    $calendar_data[$day] = '<span>'.$day.'</span><p data-toggle="tooltip" title="'.$exam->exam_name_arabic.'"  class="exam-blue"><i class="icon_pencil-edit"></i>' . getArabicDate(date('h:i a' ,strtotime($exam->from_time))) . '</p>';
                }
            }            
        }



        /*---------------------------sections---------------*/

        /*$sections = $this->courses->get_upcomingSectionsMonth($course_id, $year, $month);

        foreach ($sections as $section) {
            $day = intval(date("d", strtotime($section->section_start_time)));
            if(array_key_exists($day, $calendar_data)) {

                $calendar_data[$day] .= '<p class="section-green" data-toggle="tooltip" title="'.$section->section_title.'"><a href="'. base_url('courses/details/'). $course_id . '"><i class="icon_easel"></i>' . date('h:i a' ,strtotime($section->section_start_time)) . '</a></p>';

            } else {

                $calendar_data[$day] = '<span>'.$day.'</span><p class="section-green" data-toggle="tooltip" title="'.$section->section_title.'"><a href="'. base_url('courses/details/'). $course_id . '"><i class="icon_easel"></i>' . date('h:i a' ,strtotime($section->section_start_time)) . '</a></p>';
            }                

        }*/


            //print_r($calendar_data);
            //exit;     
        $this->lang->load('calendar', 'arabic');
        $calendar = $this->calendar->generate($year, $month, $calendar_data);     

        return $calendar;       

    }



    public function ajax_calendar() {

        $course_id = $this->security->xss_clean($this->input->post('course_id'));
        $year = $this->security->xss_clean($this->input->post('year'));
        $month = $this->security->xss_clean($this->input->post('month'));
        $prefs = array(

                'start_day'    => 'sunday',
                'month_type'   => 'long',
                'day_type'     => 'short',              
                'show_other_days'=> TRUE,               

                'template' => array(

                        'table_open'           => '<table class="table table-bordered">',
                        //'cal_cell_start'       => '<td class="day">',
                        //'cal_cell_start_today' => '<td class="today">'
                        'heading_row_start' => '<tr class="month">',
                        'week_row_start' => '<tr class="week">',
                        'cal_row_start' => '<tr class="day">',
                        'cal_cell_content' => '<div>{content}</div>',
                        'cal_cell_no_content' => '<div>{day}</div>',
                        'cal_cell_content_today' => '<div class="highlight">{content}</div>',
                        'cal_cell_no_content_today' => '<div class="highlight">{day}</div>',
                        'cal_cell_start_other' => '<td class="other-month"><div>',

                )

                );


        $this->load->library('calendar', $prefs);
        $valid = $this->calendar->adjust_date($month, $year); 
        $data['calendar'] = $this->display_calendar($course_id, $valid['year'], $valid['month']);
        $data['current_calendar'] = array();
        $data['current_calendar']['year'] = $valid['year'];
        $data['current_calendar']['month'] = $valid['month']; 
        $data['course'] = $this->courses->get_courseDetails($course_id);
        $result = $this->load->view('site/arabic/ajax-calendar-template', $data, TRUE);
        echo $result;

    }



    /*public function resources() {
        if(isStudent()) {
            $user = $this->session->userdata['olympicUser'];
            $user_id = $user['userID'];
            $data['courses'] = $this->students->get_allUnsubscribedCourses($user_id, 6);

        } else {
            $data['courses'] = $this->courses->get_Courses(6);
        }
        $results = $this->courses->get_resourcesCourseIDs();
        $resources = '';
        foreach ($results as $result) {
            $temp['resources'] = $this->courses->get_courseResources($result->course_id);
            $course = $this->courses->get_courseDetails($result->course_id);
            $temp['course_title'] = $course->course_title;
            $resources = $resources . $this->load->view('site/arabic/resources-template', $temp, TRUE);
        }

        
        $data['resources_list'] = $resources;   
        $this->load->view('site/arabic/resources', $data);

    }*/


    public function resources() {
        if(isStudent()) {
            $user = $this->session->userdata['olympicUser'];
            $user_id = $user['userID'];
            $data['courses'] = $this->students->get_allUnsubscribedCourses($user_id, 6);

        } else {
            $data['courses'] = $this->courses->get_Courses(6);
        }

        $data['resources'] = $this->courses->get_allResources();     
        $this->load->view('site/arabic/resources', $data);

    }   

}



?>