<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('site/arabic/Model_Students','students');
	}
	public function index()
	{
		$data['timezones'] =  DateTimeZone::listIdentifiers(DateTimeZone::ALL);
		$this->load->view('site/arabic/register', $data);
	}

	public function exists() {	
		$email = $this->security->xss_clean($this->input->post('email'));
		$mobile = $this->security->xss_clean($this->input->post('mobile'));

		$email_get = $this->students->get_details('students',array('stu_email' => $email));
		$mobile_get = $this->students->get_details('students',array('stu_mobile' => $mobile));

		if ($email_get->num_rows() > 0) {
			echo 1;			
		}
		else if($mobile_get->num_rows() > 0) {
			echo 2;
		}
		else {
			echo 0;
		}
		
	}

	public function student() {
		$array = array(
			'stu_name_english' => $this->security->xss_clean($this->input->post('stu_name_english')),
			'stu_name_arabic' => $this->security->xss_clean($this->input->post('stu_name_arabic')),
			'first_arabic' => $this->security->xss_clean($this->input->post('first_arabic')),
			'second_arabic' => $this->security->xss_clean($this->input->post('second_arabic')),
			'stu_timezone' => $this->security->xss_clean($this->input->post('stu_timezone')),
			'stu_mobile' => $this->security->xss_clean($this->input->post('stu_mobile')),
			'stu_email' => $this->security->xss_clean($this->input->post('stu_email')),
			'stu_password' => md5($this->security->xss_clean($this->input->post('stu_password'))),
			'stu_dob' => $this->security->xss_clean($this->input->post('stu_dob')),
			'stu_cpr' => $this->security->xss_clean($this->input->post('stu_cpr')),
			'stu_education' => $this->security->xss_clean($this->input->post('stu_qualification')),
			'stu_gender' => $this->security->xss_clean($this->input->post('stu_gender'))
		);
		
        
		$file       = $_FILES['image'];
		if($file['name']!='')
		{
            $tar1        = "uploads/students/";
			$rand1       = date('Ymd').mt_rand(1001,9999);
			$tar_file1   = $tar1 . $rand1.'.png';
			move_uploaded_file($cover['tmp_name'], $tar_file1);
		}
		else
		{
			$tar_file1  = '';
		}
        $array['stu_image'] = $tar_file1;
        
		if ($user_id = $this->students->insert('students',$array)) {

			$session = [
						'userID' => $user_id,
						'userName'    => $array['stu_name_english'],
						'userNameArabic' => $array['first_arabic'],
						'userType' => 'student',
        				'userTimezone' => $array['stu_timezone']
			           ];
			$this->session->set_userdata('olympicUser',$session);

			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Registered!');
			if($this->session->userdata('last_page')) {                
                $url = $this->session->userdata('last_page');
                $this->session->unset_userdata('last_page');
                redirect($url);
            }
            else {
            	redirect('ar/student/profile');
            }
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Registration Failed!');
			redirect('ar/register');
		}
		
	}
	
}
?>
