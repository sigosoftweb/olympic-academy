<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('site/arabic/Model_Courses','courses');
			
	}
	public function index()	{
		
		
		$prefs = array(
                'start_day'    => 'sunday',
                'start_days' => array('الاحد' => 0, 'الإثنين' => 1, 'الثلاثاء' => 2, 'الأربعاء' => 3, 'الخميس'  => 4, 'الجمعة' =>5, 'السبت' => 6),
                'month_type'   => 'long',
                'day_type'     => 'short',
                
                'show_other_days'=> FALSE,
                
                'template' => array(
                        'table_open'           => '<table class="table table-bordered">',
                        //'cal_cell_start'       => '<td class="day">',
                        //'cal_cell_start_today' => '<td class="today">'
                        'heading_row_start' => '<tr class="month">',
                        'week_row_start' => '<tr class="week">',
                        'cal_row_start' => '<tr class="day">',
                        'cal_cell_content' => '<div>{content}</div>',
                        'cal_cell_no_content' => '<div>{day}</div>',
                        'cal_cell_content_today' => '<div class="highlight">{content}</div>',
                        'cal_cell_no_content_today' => '<div class="highlight">{day}</div>',
                        'cal_cell_start_other' => '<td class="other-month"><div>',
                	)
                /*'translated_day_names' => array('الإثنين', 'الثلاثاء', 'الأربعاء', 'الخميس', 'الجمعة', 'السبت', 'الاحد'),
                /'translated_month_names' => array(  '01' => 'يناير',
                                                    '02' => 'فبراير',
                                                    '03' => 'مارس',
                                                    '04' => 'أبريل',
                                                    '05' => 'مايو',
                                                    '06' => 'يونيو',
                                                    '07' => 'يوليو',
                                                    '08' => 'أغسطس',
                                                    '09' => 'سبتمبر',
                                                    '10' => 'أكتوبر',
                                                    '11' => 'تشرين الثاني (نوفمبر)',
                                                    '12' => 'ديسمبر'
                                            )*/
                );
				
		

        
		$this->load->library('calendar', $prefs);

		$year = date('Y');
        $month = date('n');
        //$data['calendar'] = $this->display_calendar($year, $month);
        //$data['current_calendar'] = array();
        $data['current_calendar']['year'] = $year;
        //$data['current_calendar']['month'] = $month;

        for ($i=1; $i <=12 ; $i++) { 
            $data['calendar'][$i] = $this->display_calendar($year, $i);
        }
        
		$this->load->view('site/arabic/year-calendar', $data);
        //$this->parser->parse('site/arabic/year-calendar',$data); 
	}


	public function display_calendar($year, $month) {
       
           
        $calendar_data = array();
        

        /*---------------------------sections---------------*/
        $courses = $this->courses->get_allCoursesMonth($year, $month);

        foreach ($courses as $course) {

            $day = intval(date("d", strtotime($course->course_starting_date)));
            /*$str = explode(" ", $course->course_title_arabic);
            if(array_key_exists($day, $calendar_data)) {
                $calendar_data[$day] .= '<p class="section-green" data-toggle="tooltip" title="' . $course->course_title_arabic . '"><a href="'. base_url('ar/courses/details/'). $course->course_id . '"><i class="icon_easel"></i>' . substr($str[0], 0 ,8)  . '..' . '</a></p>';
            }
            else {
                $calendar_data[$day] = '<span>'.$day.'</span><p class="section-green" data-toggle="tooltip" title="' . $course->course_title_arabic . '"><a href="'. base_url('ar/courses/details/'). $course->course_id . '"><i class="icon_easel"></i>' . substr($str[0], 0 ,8) . '..' . '</a></p>';
            }*/

            if(array_key_exists($day, $calendar_data)) {
                $calendar_data[$day] .= '<p class="section-green" data-toggle="tooltip" title="' . $course->course_title_arabic . '"><a href="'. base_url('ar/courses/details/'). $course->course_id . '"><i class="icon_easel"></i></a></p>';
            }
            else {
                $calendar_data[$day] = '<span>'.$day.'</span><p class="section-green" data-toggle="tooltip" title="' . $course->course_title_arabic . '"><a href="'. base_url('ar/courses/details/'). $course->course_id . '"><i class="icon_easel"></i></a></p>';
            }                    
        }

            //print_r($calendar_data);
            //exit;

        $this->lang->load('calendar', 'arabic');
        $calendar = $this->calendar->generate($year, $month, $calendar_data);
        

        return $calendar;
        
    }

    public function ajax_calendar() {
        $year = $this->security->xss_clean($this->input->post('year'));
        $prefs = array(
                'start_day'    => 'sunday',
                'month_type'   => 'long',
                'day_type'     => 'short',
                
                'show_other_days'=> TRUE,
                
                'template' => array(
                        'table_open'           => '<table class="table table-bordered">',
                        //'cal_cell_start'       => '<td class="day">',
                        //'cal_cell_start_today' => '<td class="today">'
                        'heading_row_start' => '<tr class="month">',
                        'week_row_start' => '<tr class="week">',
                        'cal_row_start' => '<tr class="day">',
                        'cal_cell_content' => '<div>{content}</div>',
                        'cal_cell_no_content' => '<div>{day}</div>',
                        'cal_cell_content_today' => '<div class="highlight">{content}</div>',
                        'cal_cell_no_content_today' => '<div class="highlight">{day}</div>',
                        'cal_cell_start_other' => '<td class="other-month"><div>',
                )
                );


        $this->load->library('calendar', $prefs);

        $data['current_calendar']['year'] = $year;

        for ($i=1; $i <=12 ; $i++) { 
            $data['calendar'][$i] = $this->display_calendar($year, $i);
        }        

        $result = $this->load->view('site/arabic/ajax-year-calendar-template', $data, TRUE);

        echo $result;
    }
}
?>