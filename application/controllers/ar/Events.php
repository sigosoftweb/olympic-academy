<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('site/arabic/Model_Events','events');
	}

	public function index() {
		$data['events'] = $this->events->get_Events();
		$this->load->view('site/arabic/events', $data);
	}

	public function details($id) {
		$data['event'] = $this->events->get_eventDetails($id);
		if(!empty($data['event'])) {
			$data['other_events'] = $this->events->get_otherEvents($id);
			$this->load->view('site/arabic/event-details', $data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Loading Failed!');
            redirect('ar/events');
		}
	}

	
}
?>