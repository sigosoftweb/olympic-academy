<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exam extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			if (!isStudent()) {
	            redirect('ar/login');
	        }
			$this->load->model('site/arabic/Model_Exams','exam');
	}
	public function index() {
		$this->load->view('site/arabic/exam');
	}
	public function instructions($exam_id,$course_id)
	{
		$user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];

		$batch = $this->exam->getStudentBatch($course_id,$user_id);
		if ($batch->num_rows() > 0) {
			$cb_id = $batch->row()->cb_id;
		}
		else {
			$cb_id = 0;
		}

		$check = $this->exam->checkExam($course_id,$cb_id,$user_id,$exam_id);
		if ($check->num_rows() > 0) {
			$data['exam'] = $check->row();
			$data['exam_id'] = $exam_id;
			$data['course_id'] = $course_id;
			$data['instructions'] = $this->exam->getExamInstructiosns($exam_id);
			$this->load->view('site/arabic/exam-instructions',$data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', '');
			$this->session->set_flashdata('alert_message', 'Loading failed');
			redirect('ar/subscriptions');
		}
	}
	public function start($exam_id,$course_id)
	{
		$user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];

		$batch = $this->exam->getStudentBatch($course_id,$user_id);
		if ($batch->num_rows() > 0) {
			$cb_id = $batch->row()->cb_id;
		}
		else {
			$cb_id = 0;
		}

		$check = $this->exam->checkExam($course_id,$cb_id,$user_id,$exam_id);
		if ($check->num_rows() > 0) {
			$exam = $check->row();
			$current = date('Y-m-d H:i:s');
			if ($exam->time_status == '1') {
				if (strtotime($current) < strtotime($exam->from_time)) {
					$this->session->set_flashdata('alert_type', 'error');
					$this->session->set_flashdata('alert_title', '');
					$this->session->set_flashdata('alert_message', 'Be patient, Exam will start soon.');
					redirect('ar/exam/instructions/' . $exam_id . '/' . $course_id);
				}
				else {
					if (strtotime($current) > strtotime($exam->to_time)) {
						$this->session->set_flashdata('alert_type', 'error');
						$this->session->set_flashdata('alert_title', '');
						$this->session->set_flashdata('alert_message', 'Sorry, exam is completed.');
						redirect('ar/subscriptions/details/' . $course_id);
					}
					else {
						$ending_time = $exam->to_time;
					}
				}
			}
			else {
				$ending_time = date('Y-m-d H:i:s',strtotime($current . '+' . $exam->exam_time .' minutes'));
			}

			$student_exam = array(
				'exam_id' => $exam_id,
				'stu_id' => $user_id,
				'questions' => $exam->no_questions,
				'attended' => '0',
				'left_questions' => $exam->no_questions,
				'correct_answers' => '0',
				'wrong_answers' => '0',
				'mark' => '0',
				'percentage' => '0',
				'grade' => 'Poor',
				'time_taken' => '00:00:00',
				'date' => date('Y-m-d'),
				'time' => date('h:i A'),
				'started' => '1',
				'completed' => '0',
				'starting_time' => $current,
				'ending_time' => $ending_time
			);
			if ($se_id = $this->exam->insert('students_exams',$student_exam)) {
				$questions = $this->exam->getExamQuestions($exam_id);
				$question_array = array(
					'stu_id' => $user_id,
					'ans' => '',
					'exam_id' => $exam_id,
					'se_id' => $se_id,
				);
				$i = 0;
				foreach ($questions as $que) {
					if ($i == 0) {
						$que_id = $que->que_id;
						$i++;
					}
					$question_array['que_id'] = $que->que_id;
					$this->exam->insert('students_question_answers',$question_array);
				}
				redirect('ar/exam/questions/' . $exam_id . '/' . $que_id);
			}
			else {
				redirect('ar/subscriptions');
			}
		}
		else {
			redirect('ar/subscriptions');
		}
	}
	public function questions($exam_id,$que_id)
	{
		$user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];

		$get = $this->exam->getStudentExam($exam_id,$user_id);
		if ($get->num_rows() > 0) {
			$data['exam'] = $exam = $get->row();
			$se_id = $exam->se_id;
			$check = $this->exam->getQuestion($que_id,$se_id);
			if ($check->num_rows() > 0) {
				$data['exam_id'] = $exam_id;
				$data['questions'] = $questions = $this->exam->getStudentsExamQuestions($se_id);
				$i = 1;
				$question_number = 0;
				$ans = '';
				foreach ($questions as $que) {
					if ($que->que_id == $que_id) {
						$question_number = $i;
						$ans = $que->ans;
						break;
					}
					$i++;
				}
				$data['ans'] = $ans;
				$data['question_number'] = $question_number;
				$data['question'] = $check->row();
				$data['next'] = $this->exam->next($exam_id,$que_id);
				$data['previous'] = $this->exam->previous($exam_id,$que_id);

				$this->load->view('site/arabic/exam',$data);
			}
			else {
				redirect('ar/subscriptions');
			}
		}
		else {
			redirect('ar/subscriptions');
		}
	}
	public function answer()
	{
		$sqa_id = $this->security->xss_clean($this->input->post('sqa_id'));
		$next = $this->security->xss_clean($this->input->post('next'));
		$que_id = $this->security->xss_clean($this->input->post('que_id'));
		$exam_id = $this->security->xss_clean($this->input->post('exam_id'));
		$answer = $this->security->xss_clean($this->input->post('answer'));
		$se_id = $this->security->xss_clean($this->input->post('se_id'));

		$get = $this->exam->get_details('students_exams',array('se_id' => $se_id));
		if ($get->num_rows() > 0) {
			$exam = $get->row();
			$current = date('Y-m-d H:i:s');
			$end_time = $exam->ending_time;
			if (strtotime($current) > strtotime($end_time)) {
				$this->finish($se_id);
			}
			else {
				$this->exam->update('sqa_id',$sqa_id,'students_question_answers',array('ans' => $answer));
				if ($next) {
					redirect('ar/exam/questions/' . $exam_id . '/' . $next);
				}
				else {
					redirect('ar/exam/questions/' . $exam_id . '/' . $que_id);
				}
			}
		}
		else {
			redirect('ar/subscriptions');
		}
	}
	public function finish($se_id)
	{
		$user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];

		$get = $this->exam->getExam($se_id,$user_id);
		if ($get->num_rows() > 0) {
			$exam = $get->row();
			$current = date('Y-m-d H:i:s');
			$end_time = $exam->ending_time;
			$start = $exam->starting_time;
			if (strtotime($current) > strtotime($end_time)) {
				$end = $exam->ending_time;
			}
			else {
				$end = $current;
			}

			$datetime1 = new DateTime($end);
			$datetime2 = new DateTime($start);
			$interval = $datetime1->diff($datetime2);
			$time_taken = $interval->format('%H:%i:%s');

			$answers = $this->exam->getStudentsAnswers($se_id);
			$attended = 0;
			$left = 0;
			$correct = 0;
			$wrong = 0;
			$mark = 0;
			$negative_mark = 0;
			$total = 0;
			foreach ($answers as $answer) {
				$total = $total + $answer->correct;
				if ($answer->stu_ans == '') {
					$left++;
				}
				else {
					$attended++;
					if ($answer->stu_ans == $answer->ans) {
						$correct++;
						$mark = $mark + $answer->correct;
					}
					else {
						$wrong++;
						$negative_mark = $negative_mark + $answer->negative;
					}
				}
			}

			$array = array(
				'attended' => $attended,
				'left_questions' => $left,
				'correct_answers' => $correct,
				'wrong_answers' => $wrong,
				'mark' => $mark - $negative_mark,
				'total_mark' => $total,
				'time_taken' => $time_taken,
				'completed' => '1',
				'ending_time' => $end
			);

			$this->exam->update('se_id',$se_id,'students_exams',$array);

			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', '');
			$this->session->set_flashdata('alert_message', 'Exam completed successfully');

			redirect('ar/exam/result/' . $se_id);
		}
		else {
			redirect('ar/subscriptions');
		}
	}
	public function result($se_id)
	{
		$user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];

		$get = $this->exam->getExamResult($se_id,$user_id);
		if ($get->num_rows() > 0) {
			$exam = $get->row();
			if ($exam->completed == '0') {
				$current = date('Y-m-d H:i:s');
				$end_time = $exam->ending_time;
				$start = $exam->starting_time;
				if (strtotime($current) > strtotime($end_time)) {
					$end = $exam->ending_time;
				}
				else {
					$end = $current;
				}

				$datetime1 = new DateTime($end);
				$datetime2 = new DateTime($start);
				$interval = $datetime1->diff($datetime2);
				$time_taken = $interval->format('%H:%i:%s');

				$answers = $this->exam->getStudentsAnswers($se_id);
				$attended = 0;
				$left = 0;
				$correct = 0;
				$wrong = 0;
				$mark = 0;
				$negative_mark = 0;
				$total = 0;
				foreach ($answers as $answer) {
					$total = $total + $answer->correct;
					if ($answer->stu_ans == '') {
						$left++;
					}
					else {
						$attended++;
						if ($answer->stu_ans == $answer->ans) {
							$correct++;
							$mark = $mark + $answer->correct;
						}
						else {
							$wrong++;
							$negative_mark = $negative_mark + $answer->negative;
						}
					}
				}

				$array = array(
					'attended' => $attended,
					'left_questions' => $left,
					'correct_answers' => $correct,
					'wrong_answers' => $wrong,
					'mark' => $mark - $negative_mark,
					'total_mark' => $total,
					'time_taken' => $time_taken,
					'completed' => '1',
					'ending_time' => $end
				);

				$this->exam->update('se_id',$se_id,'students_exams',$array);

				$data['result'] = $this->exam->getExamResult($se_id,$user_id);
			}
			else {
				$data['result'] = $exam;
			}
			$this->load->view('site/arabic/exam-results',$data);
		}
		else {
			redirect('ar/subscriptions');
		}
	}
	public function view($se_id)
	{
		$user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];

		$get = $this->exam->getExamResult($se_id,$user_id);
		if ($get->num_rows() > 0) {
			$data['exam'] = $get->row();
			$data['questions'] = $this->exam->getAllQuestions($se_id);
			$this->load->view('site/arabic/questions',$data);
		}
		else {
			redirect('ar/subscriptions');
		}
	}
	public function unattempted($exam_id)
	{
		$user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];

		$get = $this->exam->get_details('exams',array('exam_id' => $exam_id , 'exam_status' => '1' , 'registration_status' => '1'));
		if ($get->num_rows() > 0) {
			$data['exam'] = $get->row();
			$data['questions'] = $this->exam->get_details('questions',array('exam_id' => $exam_id))->result();
			$this->load->view('site/arabic/unattempted_questions',$data);
		}
		else {
			redirect('ar/subscriptions');
		}
	}
}
?>
