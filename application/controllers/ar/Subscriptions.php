<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subscriptions extends CI_Controller {

    public function __construct() {

        parent::__construct();

        if (!isStudent()) {
            redirect('ar/login');
        }

        $this->load->model('site/arabic/Model_Courses','courses');
        $this->load->model('site/arabic/Model_Students','students');
        $this->load->model('site/arabic/Model_Exams','exam');
        $this->load->model('Common');
    }



    public function index() {  	

        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];    
        $result = $this->students->get_details('students',array('stu_id' => $user_id));

        if ($result->num_rows() > 0) {
            $data['student']=$result->row();
            //print_r($data['student']);
            //exit;

            $data['notice_count'] = $this->students->get_noticeCount($user_id);
            $data['subscribed_courses'] = $this->students->get_subscribedCourseList($user_id);
            $courses         = $this->students->get_subscribedCourses($user_id);
            foreach($courses as $course)
            {
                $grade_check = $this->Common->get_details('students_grade',array('stu_id'=>$user_id,'course_id'=>$course->course_id));
		        if($grade_check->num_rows()>0)
		        {
		            $course->grade  = $grade_check->row()->grade;
		        }
		        else
		        {
		            $course->grade  = '';
		        }
		        $course->grades  = $grade_check->num_rows();
            }
            $data['courses'] =  $courses;           

            if(!empty($data['courses'])) {
                $data['related_courses'] = $this->students->get_relatedCourses($user_id);
                //$data['related_courses'] = array();
            }            

            $this->load->view('site/arabic/subscriptions',$data);
        }

        else {

            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to load your profile!');
            redirect('ar/login');

        }

    }

    public function pending() {   

        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];    
        $result = $this->students->get_details('students',array('stu_id' => $user_id));

        if ($result->num_rows() > 0) {
            $data['student']=$result->row();
            //print_r($data['student']);
            //exit;

            $data['notice_count'] = $this->students->get_noticeCount($user_id);
            $data['subscribed_courses'] = $this->students->get_subscribedCourseList($user_id);
            $data['courses'] = $this->students->get_pendingSubscriptions($user_id);            


            $this->load->view('site/arabic/pending-subscriptions',$data);
        }

        else {

            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to load your profile!');
            redirect('ar/login');

        }

    }

    public function rejected() {   

        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];    
        $result = $this->students->get_details('students',array('stu_id' => $user_id));

        if ($result->num_rows() > 0) {
            $data['student']=$result->row();
            //print_r($data['student']);
            //exit;

            $data['notice_count'] = $this->students->get_noticeCount($user_id);
            $data['subscribed_courses'] = $this->students->get_subscribedCourseList($user_id);
            $data['courses'] = $this->students->get_rejectedSubscriptions($user_id);            

            $this->load->view('site/arabic/rejected-subscriptions',$data);
        }

        else {

            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to load your profile!');
            redirect('ar/login');

        }

    }



    public function details ($course_id) {

        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID']; 
        $data['course'] = $this->students->get_subscribedCourseDetails($course_id, $user_id);

        if(!empty($data['course']))  {   

            $data['sections_count'] = $this->courses->get_sectionsCount($course_id);
            $course_sections = $this->courses->get_courseSections($course_id); 
            
            foreach ($course_sections as $course_section) {
                $course_section->attachments = $this->courses->get_courseSectionAttachments($course_section->cs_id);
            }  
            $data['course_sections'] = $course_sections;  
            $this->load->view('site/arabic/subscribed-course-details',$data);

        }

        else {

            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Loading Failed!');
            redirect('ar/subscriptions');

        }

    }



    public function sections ($course_id) {

        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID']; 
        $data['course'] = $this->students->get_subscribedCourseDetails($course_id, $user_id);

        if(!empty($data['course']))  {    

            $data['sections_count'] = $this->courses->get_sectionsCount($course_id);
            $data['course_sections'] = $this->courses->get_courseSections($course_id);        
            $this->load->view('site/arabic/subscribed-course-sections',$data);

        }

        else {

            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Loading Failed!');
            redirect('ar/subscriptions');

        }

    }



    public function classes($course_id) {

        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID']; 
        $data['course'] = $this->students->get_subscribedCourseDetails($course_id, $user_id);

        if(!empty($data['course']))  {          

            $data['course_classes'] = $this->students->get_upcomingCourseClasses($course_id, $user_id);
            $data['batchIDs'] = $this->students->get_studentBatches($user_id);
            $data['classes_count'] = $this->students->get_upcomingClassesCount($course_id,$user_id);

            $data['completed_classes'] = $this->students->get_completedCourseClasses($course_id,$user_id);
            $data['completed_count'] = $this->students->get_completedClassesCount($course_id, $user_id);

            $this->load->view('site/arabic/subscribed-course-classes',$data);

        }

        else {

            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Loading Failed!');
            redirect('ar/subscriptions');

        }

    }



    public function exams($course_id) {

        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID']; 
        $data['course'] = $this->students->get_subscribedCourseDetails($course_id, $user_id);


        if(!empty($data['course']))  {             

            // EXAM MODULE            

            $batch = $this->exam->getStudentBatch($course_id,$user_id);

            if ($batch->num_rows() > 0) {
                $cb_id = $batch->row()->cb_id;
            }

            else {
                $cb_id = 0;
            }

            $exams = $this->exam->getExams($course_id,$cb_id,$user_id);
            $exam_array = array();
            $today = date('Y-m-d H:i:s');

            foreach ($exams as $exam) {

                if ($exam->time_status == '1') {
                        if (strtotime($today) < strtotime($exam->to_time)) {
                            $exam_array[] = $exam;
                        }
                    }

                    else {
                        $exam_array[] = $exam;
                    }

            }

            $data['exams'] = $exam_array;
            $data['attended'] = $this->exam->getAttendedExams($user_id);
            $data['unattempted'] = $this->exam->getUnattemptedExams($course_id,$cb_id,$user_id);               

            $this->load->view('site/arabic/subscribed-course-exams',$data);

        }

        else {

            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Loading Failed!');
            redirect('ar/subscriptions');

        }

    }



    public function attendance($course_id) {

        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID']; 
        $data['course'] = $this->students->get_subscribedCourseDetails($course_id, $user_id);

        if(!empty($data['course']))  { 
            $data['attends'] = $this->students->get_courseAttendance($user_id, $course_id);
            $this->load->view('site/arabic/subscribed-course-attendance',$data);
        }

        else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Loading Failed!');
            redirect('ar/subscriptions');
        }

    }

    public function assignments($course_id) {

        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID']; 
        $data['course'] = $this->students->get_subscribedCourseDetails($course_id, $user_id);

        if(!empty($data['course']))  { 

            $batch = $this->exam->getStudentBatch($course_id,$user_id);
            if ($batch->num_rows() > 0) {
                $cb_id = $batch->row()->cb_id;
            }

            else {
                $cb_id = 0;
            }

            //echo $cb_id;
            //exit;

            $data['assignments_count'] = $this->students->get_courseAssignmentsCount($course_id, $cb_id, $user_id);
            $data['assignments'] = $this->students->get_courseAssignments($course_id, $cb_id, $user_id);            

            $this->load->view('site/arabic/subscribed-course-assignments',$data);
        }

        else {

            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Loading Failed!');
            redirect('ar/subscriptions');
        }

    }



    public function post_assignment() {

        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID']; 
        $course_id = $this->security->xss_clean($this->input->post('course_id'));
        $array = array(

            'answer' => $this->security->xss_clean($this->input->post('answer')),
            'ca_id' => $this->security->xss_clean($this->input->post('ca_id')),
            'stu_id' => $user_id            

        );



        $file = $_FILES['answer_attachment'];

            if ($file['size'] > 0) {
                $tar = "uploads/students-assignments/";
                $rand=date('Ymd').mt_rand(1001,9999);
                $tar_file = $tar . $rand . basename($file['name']);

                if(move_uploaded_file($file["tmp_name"], $tar_file))  {
                    $array['answer_attachment'] = $tar_file;
                }

                else {
                    $array['answer_attachment'] = '';
                }

            }

            else {
                $array['answer_attachment'] = '';
            }

        if($array['answer_attachment'] == '' && $array['answer']=='') {

            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed!');

        }

        else {

            if ($csa_id = $this->students->insert('course_students_assignments',$array)) {

                $this->session->set_flashdata('alert_type', 'success');
                $this->session->set_flashdata('alert_title', 'Success');
                $this->session->set_flashdata('alert_message', 'Assignment posted!');

            }

            else {

                $this->session->set_flashdata('alert_type', 'error');
                $this->session->set_flashdata('alert_title', 'Error');
                $this->session->set_flashdata('alert_message', 'Error!');                

            }

        }

        redirect('ar/subscriptions/assignments/'.$course_id);

    }



    /*public function get_assignment() {

        $csa_id = $this->security->xss_clean($this->input->post('csa_id'));
        $row = $this->students->get_studentAssignment($csa_id);

        if(!empty($row)) {
            return json_encode($row);
        }

        else {
            return 0;
        }

    }*/



    public function trainers($course_id) {

        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID']; 
        $data['course'] = $this->students->get_subscribedCourseDetails($course_id, $user_id);

        if(!empty($data['course'])) {

            $data['trainers'] = $this->courses->get_courseTrainers($course_id);
            $data['organisers'] = $this->courses->get_courseOrganisers($course_id);
            $this->load->view('site/arabic/subscribed-course-trainers',$data);

        }

        else {

            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Loading Failed!');
            redirect('ar/courses');

        }

    }



    public function calendar($course_id, $year=NULL, $month=NULL) {



        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID']; 
        $data['course'] = $this->students->get_subscribedCourseDetails($course_id, $user_id);


        if(!empty($data['course'])) {

            $prefs = array(

                'start_day'    => 'sunday',
                'month_type'   => 'long',
                'day_type'     => 'short',               
                'show_other_days'=> TRUE,               
                'template' => array(

                        'table_open'           => '<table class="table table-bordered">',
                        //'cal_cell_start'       => '<td class="day">',
                        //'cal_cell_start_today' => '<td class="today">'
                        'heading_row_start' => '<tr class="month">',
                        'week_row_start' => '<tr class="week">',
                        'cal_row_start' => '<tr class="day">',
                        'cal_cell_content' => '<div>{content}</div>',
                        'cal_cell_no_content' => '<div>{day}</div>',
                        'cal_cell_content_today' => '<div class="highlight">{content}</div>',
                        'cal_cell_no_content_today' => '<div class="highlight">{day}</div>',
                        'cal_cell_start_other' => '<td class="other-month"><div>',

                )

                );


            $this->load->library('calendar', $prefs);        

            if($year == NULL && $month == NULL) {

                $this->display_calendar($course_id, date('Y'), date('n'));
                //$this->display_calendar($course_id, 2020, 6);

            }

            else {

                $valid = $this->calendar->adjust_date($month, $year);
                $this->display_calendar($course_id, $valid['year'], $valid['month']);

            }

        }

        else {

            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Loading Failed!');
            redirect('ar/courses');

        }        

    }



    public function display_calendar($course_id, $year, $month) {



        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];

        $result = $this->students->get_details('students',array('stu_id' => $user_id));

        if ($result->num_rows() > 0) { 

            $data['student']=$result->row();    
            $data['notice_count'] = $this->students->get_noticeCount($user_id);
            $data['subscribed_courses'] = $this->students->get_subscribedCourseList($user_id);

            $calendar_data = array();

            /*--------------------Classes-----------------*/

            $classes = $this->students->get_upcomingClassesMonth($course_id, $user_id, $year, $month); 
            foreach ($classes as $class) {   

                $day = intval(date("d", strtotime($class->starting_time)));               
                if(array_key_exists($day, $calendar_data)) {

                    $calendar_data[$day] .= '<p class="class-yellow" data-toggle="tooltip" title="'. $class->topic_arabic.'"><a href="'. $class->join_url. '" target="_blank">' . getArabicDate(date('h:i a' ,strtotime($class->starting_time))) . ' <i class="icon_mic_alt"></i></a></p>';
                }

                else {

                    $calendar_data[$day] = '<span>'.$day.'</span><p class="class-yellow" data-toggle="tooltip" title="' .$class->topic_arabic.'"><a href="'. $class->join_url. '" target="_blank">' . getArabicDate(date('h:i a' ,strtotime($class->starting_time))) . ' <i class="icon_mic_alt"></i></a></p>';

                }              

            }            



            /*--------------------------Exams-------------------------*/

            $batch = $this->exam->getStudentBatch($course_id,$user_id);

            if ($batch->num_rows() > 0) {
                $cb_id = $batch->row()->cb_id;
            }

            else {
                $cb_id = 0;
            }

            $exams = $this->students->get_upcomingExamsMonth($course_id, $cb_id, $user_id, $year, $month); 
            $today = date('Y-m-d H:i:s');
            foreach ($exams as $exam) {
                $day = intval(date("d", strtotime($exam->from_time)));
                $valid_exam = FALSE;
                if ($exam->time_status == '1') {
                        if (strtotime($today) < strtotime($exam->to_time)) {
                            $valid_exam = TRUE;
                        }
                }

                else {
                        $valid_exam = TRUE;
                }



                if($valid_exam) {                

                    if(array_key_exists($day, $calendar_data)) {

                        $calendar_data[$day] .= '<p data-toggle="tooltip" title="'.$exam->exam_name_arabic.'" class="exam-blue" ><a href="'.base_url('ar/exam/instructions/') . $exam->exam_id .'/' . $course_id.'">' . getArabicDate(date('h:i a' ,strtotime($exam->from_time))) . ' <i class="icon_pencil-edit"></i></a></p>';
                    }

                    else {

                        $calendar_data[$day] = '<span>'.$day.'</span><p data-toggle="tooltip" title="'.$exam->exam_name_arabic.'"  class="exam-blue"><a href="'.base_url('ar/exam/instructions/') . $exam->exam_id .'/' . $course_id.'">' . getArabicDate(date('h:i a' ,strtotime($exam->from_time))) . ' <i class="icon_pencil-edit"></i></a></p>';
                    }
                }  
                else {
                    if(array_key_exists($day, $calendar_data)) {

                        $calendar_data[$day] .= '<p data-toggle="tooltip" title="'.$exam->exam_name_arabic.'" class="exam-blue" >' . getArabicDate(date('h:i a' ,strtotime($exam->from_time))) . ' <i class="icon_pencil-edit"></i></p>';
                    }

                    else {

                        $calendar_data[$day] = '<span>'.$day.'</span><p data-toggle="tooltip" title="'.$exam->exam_name_arabic.'"  class="exam-blue">' . getArabicDate(date('h:i a' ,strtotime($exam->from_time))) . ' <i class="icon_pencil-edit"></i></p>';
                    }
                }         

            }



            /*---------------------------sections---------------*/

            /*$sections = $this->students->get_upcomingSectionsMonth($course_id, $year, $month);

            foreach ($sections as $section) {

                $day = intval(date("d", strtotime($section->section_start_time)));

                if(array_key_exists($day, $calendar_data)) {

                    $calendar_data[$day] .= '<p class="section-green" data-toggle="tooltip" title="'.$section->section_title.'"><a href="'. base_url('ar/subscriptions/sections/'). $course_id . '"><i class="icon_easel"></i>' . date('h:i a' ,strtotime($section->section_start_time)) . '</a></p>';

                }
                else {

                    $calendar_data[$day] = '<span>'.$day.'</span><p class="section-green" data-toggle="tooltip" title="'.$section->section_title.'"><a href="'. base_url('ar/subscriptions/sections/'). $course_id . '"><i class="icon_easel"></i>' . date('h:i a' ,strtotime($section->section_start_time)) . '</a></p>';

                }                

            } */

            //print_r($calendar_data);
            //exit;       


            $this->lang->load('calendar', 'arabic');
            $data['calendar'] = $this->calendar->generate($year, $month, $calendar_data);
            $data['course'] = $this->courses->get_details('courses',array('course_id' => $course_id))->row();

            $data['current_calendar'] = array();
            $data['current_calendar']['year'] = $year;
            $data['current_calendar']['month'] = $month;

            //$data['calendar'] = $this->calendar->generate($cal_data);
            $this->load->view('site/arabic/calendar', $data);

        }

        else {

            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to load your profile!');
            redirect('ar/login');

        }

    }
    
    public function certificates($course_id) 
    {
        $user    = $this->session->userdata['olympicUser'];
        $user_id = $user['userID']; 
        
        $data['course'] = $this->students->get_subscribedCourseDetails($course_id, $user_id);
        if(!empty($data['course']))
        {
            $data['certificates'] = $this->Common->get_details('students_certificates',array('stu_id'=>$user_id,'course_id'=>$course_id))->result();
            $this->load->view('site/arabic/subscribed-course-certificates',$data);
        }
        else 
        {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Loading Failed!');
            redirect('ar/courses');
        }
    }
    
    public function certificate_download($id) 
    {
        $this->load->library('pdf');
        $user    = $this->session->userdata['olympicUser'];
        $user_id = $user['userID']; 
        
        $certificate         = $this->Common->get_details('students_certificates',array('sc_id'=>$id))->row();
        $data['certificate'] = $certificate;
        if(!empty($data['certificate']))
        {
           $student         =  $this->Common->get_details('students',array('stu_id'=>$certificate->stu_id))->row();
           $data['student'] = $student->stu_name_english;
            $html     = $this->load->view('site/arabic/download-course-certificates', $data, true);
			$filename = 'certificate-OLMPC' . $id;
    		$this->pdf->createPDF($html, $filename, false);
        //   $this->load->view('site/english/download-course-certificates', $data);
        }
        else 
        {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Loading Failed!');
            redirect('ar/courses');
        }
    }
    

}



?>