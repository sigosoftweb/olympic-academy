<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('site/arabic/Model_Team','team');
	}
	public function index()
	{
		$this->load->view('site/arabic/about');
	}

	public function team()
	{
		$data['team'] = $this->team->get_team();
		$this->load->view('site/arabic/team', $data);
	}
}
?>