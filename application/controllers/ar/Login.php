<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('site/arabic/Model_Students','students');
			$this->load->model('Common');
	}
	public function index()
	{
		if (isStudent()) {
	        redirect('ar/home');	
		}
		$this->load->view('site/arabic/login');
	}
	public function checkStudent()
	{
		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean(trim($this->input->post('password')));
		//echo "Email:" . $email . ", Password:" . $password;
		//exit;
		if(is_numeric($username)) {
			$get = $this->students->get_details('students',array('stu_mobile' => $username, 'stu_password' => md5($password)));
		}

		else {
			$get = $this->students->get_details('students',array('stu_email' => $username, 'stu_password' => md5($password)));
		}
		
		
		if ($get->num_rows() > 0) {
			$user = $get->row();
			
			if($user->stu_registration == 'Approved')
			{
			    if($user->stu_status == '1')
			    {
			    	$timezone = date_default_timezone_get();
			    	if($user->stu_timezone) {
			    		$timezone = $user->stu_timezone;
			    	}
			        $session = [
        				'userID' => $user->stu_id,
        				'userName'    => $user->stu_name_english,
        				'userNameArabic' => $user->first_arabic,
        				'userType' => 'student',
        				'userTimezone' => $timezone
        			];
        			$this->session->set_userdata('olympicUser',$session);
        
        			
        			$this->session->set_flashdata('alert_type', 'success');
        			$this->session->set_flashdata('alert_title', 'Success');
        			$this->session->set_flashdata('alert_message', 'Logged in Successfully!');
        			
        			if($this->session->userdata('last_page')) {                
                        $url = $this->session->userdata('last_page');
                        $this->session->unset_userdata('last_page');
                        redirect($url);
                    }
                    else {
                    	redirect('ar/student/profile');
                    }
			    }
			    else
			    {
			        $this->session->set_flashdata('alert_type', 'error');
        			$this->session->set_flashdata('alert_title', 'Failed');
        			$this->session->set_flashdata('alert_message', 'You are temporarily blocked. Tell us if you think we made a mistake.');
        			redirect('ar/login');
			    }
			}
			else
			{
			    $this->session->set_flashdata('alert_type', 'error');
    			$this->session->set_flashdata('alert_title', 'Failed');
    			if($user->stu_registration == 'Waiting')
    			{
    			    $this->session->set_flashdata('alert_message', 'Your account has been created, but you cannot login until it has been verified.');
    			}
    			else
    			{
    			    $this->session->set_flashdata('alert_message', 'Your account is rejected by admin.');
    			}
    			
    			redirect('ar/login');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Invalid login credentials, please try again');
			redirect('ar/login');
		}
	}

	public function forgotpassword() {
		$this->load->view('site/arabic/forgot-password');
	}

	function getSignature()
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 70; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
		return $randomString;
	}


	function resetlink() {
		$username   = $this->security->xss_clean($this->input->post('username'));
		
		if(is_numeric($username)) {

			$check = $this->Common->get_details('students',array('stu_mobile' => $username));
		}
		else {
			$check = $this->Common->get_details('students',array('stu_email' => $username));
		}

		
		if($check->num_rows() > 0) {
			$result = $check->row();
			//print_r($result );
			//exit;
			$signature = $this->getSignature();	          
            
	        $array        = [
    			               'email'       => $result->stu_email,
    			               'signature'   => $signature
    		                ];
		
	        
			if($reset_id=$this->Common->insert('password_reset',$array)) {

				$reset_link = site_url('ar/login/resetpassword?signature=' . $signature. '&email=' . $result->stu_email);
				$this->sendMailResetLink($result->stu_email, $result->stu_name_english, $reset_link);
				
				
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Error!');
				redirect('ar/login');
			}
			
		}

		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Not registered!');
			redirect('ar/login');
		}
	}



	public function sendMailResetLink($email, $name, $reset_link){
        $config = array(
            'mailtype' => 'html',
            'charset'  => 'utf-8',
            'priority' => '1'
             );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->set_crlf("\r\n");

        $this->email->from('info@sigosoft.com'); 
        

        $this->email->to($email); 
        $subject = "Olympic Academy - Password Reset";
        $this->email->subject($subject);

        $data['reset_link'] = $reset_link;
        $data['name'] = $name;

        $body = $this->load->view('site/arabic/reset-link-mail',$data,TRUE);
        $this->email->message($body); 

        if ($this->email->send()) {
        	$this->session->set_flashdata('alert_type', 'success');
    		$this->session->set_flashdata('alert_title', 'Success');
    		$this->session->set_flashdata('alert_message', 'Reset link sent to your mail. It will be valid for 15 minutes only.');
            
        } else {
            $this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Error!');
            
        }
        redirect('ar/login');
    }

	function resetpassword() {
		$email = $this->security->xss_clean($this->input->get('email'));
		$signature = $this->security->xss_clean($this->input->get('signature'));

		$verified = $this->students->verifyReset($email, $signature);
		$data['email'] = $email;
		if($verified) {
			$this->load->view('site/arabic/reset-password', $data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Not a valid reset link or time expired!');
			redirect('ar/login');
		}
	}

	function changePassword() {
		$email = $this->security->xss_clean($this->input->post('email'));
		$password = $this->security->xss_clean($this->input->post('password'));
		$array = 	[
    					'stu_password' => md5($password)
    				];
    	if($this->students->update('stu_email',$email,'students',$array)) {
    		$this->session->set_flashdata('alert_type', 'success');
    		$this->session->set_flashdata('alert_title', 'Success');
    		$this->session->set_flashdata('alert_message', 'Password updated successfully!');
    	}
    	else {
    		$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Error! Try Again');
			
    	}
    	redirect('ar/login');
	}
	public function logout()
	{
		
		$this->session->unset_userdata('olympicUser');
		redirect('ar/home');
	}
}
?>
