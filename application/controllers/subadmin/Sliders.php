<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sliders extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('subadmin/Model_sliders','slider');
			if (!subadmin()) {
				redirect('subadmin_login');
			}
	}
	public function index()
	{   
	    $admin    = $this->session->userdata['subadmin'];
		$admin_id = $admin['user_id'];
		
	    $data['title']      = 'Olymbic | Sliders';
		$data['sliders']    = $this->slider->get_details('sliders',array())->result();
		$data['priority']   = $this->slider->get_details('subadmin_previleges',array('m_id'=>'33','subadmin_id'=>$admin_id))->row();
		$this->load->view('subadmin/sliders/view',$data);
	}
	public function add()
	{
		$slider = $this->security->xss_clean($this->input->post('name'));

		$file = $_FILES['image'];
		$tar = "uploads/sliders/";
		$rand=date('Ymd').mt_rand(1001,9999);
		$tar_file = $tar . $rand . basename($file['name']);
		if(move_uploaded_file($file["tmp_name"], $tar_file))
		{
			$array = [
    			'name' => $slider,
    			'image' => $tar_file
    		];
    		if ($this->slider->insert('sliders',$array)) {
    			$this->session->set_flashdata('alert_type', 'success');
    			$this->session->set_flashdata('alert_title', 'Success');
    			$this->session->set_flashdata('alert_message', 'New slider added..!');

    			redirect('subadmin/sliders');
    		}
    		else {
    			$this->session->set_flashdata('alert_type', 'error');
    			$this->session->set_flashdata('alert_title', 'Failed');
    			$this->session->set_flashdata('alert_message', 'Failed to add slider..!');

    			redirect('subadmin/sliders/add');
    		}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to upload image..!');

			redirect('subadmin/sliders');
		}
	}

	public function delete($slider_id)
	{
		$check = $this->slider->get_details('sliders',array('slider_id' => $slider_id));
		if ($check->num_rows() > 0) {
			$slider = $check->row();
			if ($this->slider->delete('sliders',array('slider_id' => $slider_id))) {
				$remove_path = FCPATH . $slider->image;
				unlink($remove_path);

				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'slider deleted successfully..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to remove slider..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to remove slider..!');
		}
		redirect('subadmin/sliders');
	}

	public function status($status,$slider_id)
	{
		$array = array(
			'status' => $status
		);
		$this->slider->update('slider_id',$slider_id,'sliders',$array);
		if ($status == '1') {
			$message = 'Slider activated';
		}
		else {
			$message = 'Slider deactivated';
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		$this->session->set_flashdata('alert_message', $message);
		redirect('subadmin/sliders');
	}
}
?>
