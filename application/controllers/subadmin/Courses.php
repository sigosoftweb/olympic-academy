<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/JWT.php';
class Courses extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!subadmin()) {
            redirect('subadmin_login');
        }
		$this->load->model('subadmin/Model_courses','course');
		$this->load->model('subadmin/Model_sections','section');
		$this->load->model('subadmin/Model_classes','classes');
    }
    public function index() {
        $admin      = $this->session->userdata['subadmin'];
		$admin_id   = $admin['user_id'];
		
        $data['title'] = 'Olymbic | Courses';
        $data['priority'] = $this->course->get_details('subadmin_previleges',array('subadmin_id'=>$admin_id,'m_id'=>'2'))->row();
        $this->load->view('subadmin/courses/view', $data);
    }
	public function add()
	{
		$data['title'] = 'Olymbic | Add Courses';
		$data['trainers'] = $this->course->getTrainers();
		$data['categories'] = $this->course->getCategories();
        $this->load->view('subadmin/courses/add', $data);
	}
	public function edit($course_id)
	{
		$data['title'] = 'Olymbic | Edit Courses';
		$get = $this->course->get_details('courses',array('course_id' => $course_id));
		if ($get->num_rows() > 0) {
			$data['course'] = $get->row();
			$this->load->view('subadmin/courses/edit', $data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Error');
			$this->session->set_flashdata('alert_message', 'Failed to load course..!');
		}
        $this->load->view('subadmin/courses/add', $data);
	}
	public function get()
	{
		$result = $this->course->make_datatables();
		$data = array();
		foreach ($result as $res) {
			$sub_array = array();
			if ($res->course_banner != '') {
				$sub_array[] = '<img src="' . base_url() . $res->course_banner . '" height="100px">';
			}
			else {
				$sub_array[] = '<img src="' . base_url() . 'uploads/courses/default.png" height="50px">';
			}
			$sub_array[] = $res->course_title;
			$sub_array[] = $res->course_description;
			$sub_array[] = date('d/m/Y',strtotime($res->course_starting_date));

			$sub_array[] = $res->course_duration;
			$sub_array[] = $res->course_sale_price;
			$sub_array[] = $res->course_price;
			if ($res->course_status) {
				$status = 'Published<br><a class="btn btn-danger btn-xs margin-bottom-20" href="' . site_url('subadmin/courses/status/'. $res->course_id . '/0') . '">Draft</a>';
			}
			else {
				$status = 'Draft<br><a class="btn btn-success btn-xs margin-bottom-20" href="' . site_url('subadmin/courses/status/'. $res->course_id . '/1') . '">Publish</a>';
			}
			$sub_array[] = $status;
			$sub_array[] = '<a class="btn btn-primary btn-xs margin-bottom-20" href="' . site_url('subadmin/courses/details/1/'. $res->course_id) . '">Details</a>';

			$data[] = $sub_array;
		}

		$output = array(
			"draw"   => intval($_POST['draw']),
			"recordsTotal" => $this->course->get_all_data(),
			"recordsFiltered" => $this->course->get_filtered_data(),
			"data" => $data
		);
		echo json_encode($output);
	}
	public function details($cat,$course_id)
	{
		$data['title'] = 'Olymbic | Course details';
		$data['priority'] = $this->course->get_details('priority',array('item'=>'Course'))->row();
		$data['t_priority'] = $this->course->get_details('priority',array('item'=>'Trainers'))->row();
		$data['b_priority'] = $this->course->get_details('priority',array('item'=>'Batches'))->row();
		$data['s_priority'] = $this->course->get_details('priority',array('item'=>'Sections'))->row();
		$data['c_priority'] = $this->course->get_details('priority',array('item'=>'Class'))->row();
		$data['e_priority'] = $this->course->get_details('priority',array('item'=>'Exams'))->row();


		$get = $this->course->get_details('courses',array('course_id' => $course_id));
		if ($get->num_rows() > 0) {
			$data['course_id'] = $course_id;
			$data['active'] = $cat;
			$data['course_name'] = $get->row()->course_title;
			if ($cat == '1') {
				$data['course'] = $get->row();
				$data['categories'] = $this->course->getCategories();
			}
			elseif ($cat == '2') {
				$data['teachers'] = $this->course->getAvailableTeachers($course_id);
				$data['trainers'] = $this->course->getCourseTeachers($course_id);
			}
			elseif ($cat == '3') {
				$data['sections'] = $this->course->getSections($course_id);
			}
			elseif ($cat == '4') {
				$data['batches'] = $this->course->getBatches($course_id);
			}
			elseif ($cat == '5') {
				$data['batches'] = $this->course->getCourseBatches($course_id);
				$data['sections'] = $this->course->getCourseSections($course_id);
				$data['trainers'] = $this->course->getCourseTrainers($course_id);
				$data['classes'] = $this->course->getClasses($course_id);
			}elseif ($cat == '6') {
				$data['students'] = $this->course->getSubscribedStudents($course_id);
			}elseif ($cat == '7') {
				$data['exams'] = array();
			}elseif ($cat == '8') {
				$data['batches'] = $this->course->getCourseBatches($course_id);
				if (isset($_POST['cb_id'])) {
					$data['param'] = true;
					$data['cb_id'] = $cb_id = $this->security->xss_clean($this->input->post('cb_id'));
					$data['date'] = $date = $this->security->xss_clean($this->input->post('date'));
					$get = $this->course->get_details('attendance',array('cb_id' => $cb_id , 'date' => $date));
					if ($get->num_rows() > 0) {
						$data['attendance'] = true;
						$data['students'] = $this->course->getBatchAttendance($cb_id,$date);
					}
					else {
						$data['attendance'] = false;
						$data['students'] = $this->course->getBatchStudents($cb_id);
					}
				}
				else {
					$data['date'] = date('Y-m-d');
					$data['cb_id'] = 0;
					$data['param'] = false;
				}
			};
	        $this->load->view('subadmin/courses/details', $data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Error');
			$this->session->set_flashdata('alert_message', 'Failed to load course..!');
			redirect('subadmin/courses');
		}
	}
	public function addCourse()
	{
		$array = array(
			'course_title' => $this->security->xss_clean($this->input->post('title')),
			'course_description' => $this->security->xss_clean($this->input->post('description')),
			'cat_id' => $this->security->xss_clean($this->input->post('cat_id')),
			'course_starting_date' => $this->security->xss_clean($this->input->post('starting_date')),
			'course_duration' => $this->security->xss_clean($this->input->post('duration')),
			'course_sale_price' => $this->security->xss_clean($this->input->post('sale_price')),
			'course_price' => $this->security->xss_clean($this->input->post('price')),
		);
		$image = $_FILES['image'];
		if ($file['size'] > 0) {
			$tar = "uploads/courses/";
			$rand=date('Ymd').mt_rand(1001,9999);
			$tar_file = $tar . $rand . basename($file['name']);
			if(move_uploaded_file($file["tmp_name"], $tar_file))
			{
				$array['course_banner'] = $tar_file;
			}
			else {
				$array['course_banner'] = '';
			}
		}
		else {
			$array['course_banner'] = '';
		}
		if ($course_id = $this->course->insert('courses',$array)) {
			$trainers = $this->security->xss_clean($this->input->post('trainers'));
			foreach ($trainers as $trainer_id) {
				$trainer = array(
					'course_id' => $course_id,
					'trainer_id' => $trainer_id
				);
				$this->course->insert('course_trainers',$trainer);
			}
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'New course added..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add course..!');
		}
		redirect('subadmin/courses');
	}
	public function editCourse()
	{
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$get = $this->course->get_details('courses',array('course_id' => $course_id));
		if ($get->num_rows() > 0) {
			$array = array(
				'course_title' => $this->security->xss_clean($this->input->post('title')),
				'course_description' => $this->security->xss_clean($this->input->post('description')),
				'cat_id' => $this->security->xss_clean($this->input->post('cat_id')),
				'course_starting_date' => $this->security->xss_clean($this->input->post('starting_date')),
				'course_duration' => $this->security->xss_clean($this->input->post('duration')),
				'course_sale_price' => $this->security->xss_clean($this->input->post('sale_price')),
				'course_price' => $this->security->xss_clean($this->input->post('price')),
			);
			$file = $_FILES['image'];
			if ($file['size'] > 0) {
				$tar = "uploads/courses/";
				$rand=date('Ymd').mt_rand(1001,9999);
				$tar_file = $tar . $rand . basename($file['name']);
				if(move_uploaded_file($file["tmp_name"], $tar_file))
				{
					$array['course_banner'] = $tar_file;
				}
			}
			if ($this->course->update('course_id',$course_id,'courses',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Course updated..!');
				redirect('subadmin/courses/details/1/' . $course_id);
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to update course..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load course..!');
		}
		redirect('subadmin/courses');
	}
	public function status($course_id,$status)
	{
		$array = array(
			'course_status' => $status
		);
		$this->course->update('course_id',$course_id,'courses',$array);
		if ($status == '1') {
			$this->session->set_flashdata('alert_message', 'Course activated..!');
		}
		else {
			$this->session->set_flashdata('alert_message', 'Course deactivated..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('subadmin/courses');
	}
	public function statusTeacher($ct_id,$status)
	{
		$get = $this->course->get_details('course_trainers',array('ct_id' => $ct_id));
		if ($get->num_rows() > 0) {
			$course_id = $get->row()->course_id;
			$array = array(
				'ct_status' => $status
			);
			$this->course->update('ct_id',$ct_id,'course_trainers',$array);
			if ($status == '1') {
				$this->session->set_flashdata('alert_message', 'Trainer is activated in this course..!');
			}
			else {
				$this->session->set_flashdata('alert_message', 'Trainer is deactivated in this course..!');
			}
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			redirect('subadmin/courses/details/2/' . $course_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load course..!');
			redirect('subadmin/courses');
		}
	}
	public function addTrainers()
	{
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$get = $this->course->get_details('courses',array('course_id' => $course_id));
		if ($get->num_rows() > 0) {
			$trainers = $this->security->xss_clean($this->input->post('trainers'));
			foreach ($trainers as $trainer_id) {
				$trainer = array(
					'course_id' => $course_id,
					'trainer_id' => $trainer_id
				);
				$this->course->insert('course_trainers',$trainer);
			}
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Teachers added to this course..!');
			redirect('subadmin/courses/details/2/' . $course_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load course..!');
			redirect('subadmin/courses');
		}
	}

	public function addSection()
	{
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$get = $this->course->get_details('courses',array('course_id' => $course_id));
		if ($get->num_rows() > 0) {
			$array = array(
				'section_title' => $this->security->xss_clean($this->input->post('title')),
				'section_description' => $this->security->xss_clean($this->input->post('description')),
				'section_file_type' => $this->security->xss_clean($this->input->post('type')),
				'section_youtube' => $this->security->xss_clean($this->input->post('youtube')),
				'section_other' => $this->security->xss_clean($this->input->post('other')),
				'course_id' => $course_id,
			);
			$file = $_FILES['file'];
			if ($file['size'] > 0) {
				$tar = "uploads/sections/";
				$rand = date('Ymd').mt_rand(1001,9999);
				$tar_file = $tar . $rand . basename($file['name']);
				if(move_uploaded_file($file["tmp_name"], $tar_file))
				{
					$array['section_attachment'] = $tar_file;
				}
				else {
					$array['section_attachment'] = '';
				}
			}
			else {
				$array['section_attachment'] = '';
			}
			if ($this->course->insert('course_sections',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'New course added..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to add course..!');
			}
			redirect('subadmin/courses/details/3/' . $course_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load course..!');
			redirect('subadmin/courses');
		}
	}
	public function editSection()
	{
		$cs_id = $this->security->xss_clean($this->input->post('cs_id'));
		$get = $this->course->get_details('course_sections',array('cs_id' => $cs_id));
		if ($get->num_rows() > 0) {
			$section = $get->row();
			$course_id = $section->course_id;
			$array = array(
				'section_title' => $this->security->xss_clean($this->input->post('title')),
				'section_description' => $this->security->xss_clean($this->input->post('description')),
				'section_file_type' => $this->security->xss_clean($this->input->post('type')),
				'section_youtube' => $this->security->xss_clean($this->input->post('youtube')),
				'section_other' => $this->security->xss_clean($this->input->post('other')),
			);
			$file = $_FILES['file'];
			if ($file['size'] > 0) {
				$tar = "uploads/sections/";
				$rand = date('Ymd').mt_rand(1001,9999);
				$tar_file = $tar . $rand . basename($file['name']);
				if(move_uploaded_file($file["tmp_name"], $tar_file))
				{
					if ($section->section_attachment != '') {
						$unlink = FCPATH . $section->section_attachment;
						unlink($unlink);
					}
					$array['section_attachment'] = $tar_file;
				}
			}
			if ($this->course->update('cs_id',$cs_id,'course_sections',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Section updated..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to update section..!');
			}
			redirect('subadmin/courses/details/3/' . $course_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load section..!');
			redirect('subadmin/courses');
		}
	}
	public function statusSection($cs_id,$status)
	{
		$get = $this->course->get_details('course_sections',array('cs_id' => $cs_id));
		if ($get->num_rows() > 0) {
			$course_id = $get->row()->course_id;
			$array = array(
				'section_status' => $status
			);
			$this->course->update('cs_id',$cs_id,'course_sections',$array);
			if ($status == '1') {
				$this->session->set_flashdata('alert_message', 'Section activated');
			}
			else {
				$this->session->set_flashdata('alert_message', 'Section deactivated');
			}
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			redirect('subadmin/courses/details/3/' . $course_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load course..!');
			redirect('subadmin/courses');
		}
	}
	public function getSection()
	{
		$cs_id = $this->security->xss_clean($this->input->post('cs_id'));
		$section = $this->course->get_details('course_sections',array('cs_id' => $cs_id))->row();
		print_r(json_encode($section));
	}
	public function deleteSection($cs_id)
	{
		$get = $this->course->get_details('course_sections',array('cs_id' => $cs_id));
		if ($get->num_rows() > 0) {
			$section = $get->row();
			$course_id = $section->course_id;
			if ($this->course->delete('course_sections',array('cs_id' => $cs_id))) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Section deleted..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to delete section..!');
			}
			redirect('subadmin/courses/details/3/' . $course_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load section..!');
			redirect('subadmin/courses');
		}
	}
	public function addBatch()
	{
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$get = $this->course->get_details('courses',array('course_id' => $course_id));
		if ($get->num_rows() > 0) {
			$array = array(
				'batch_name' => $this->security->xss_clean($this->input->post('batch_name')),
				'course_id' => $course_id,
			);
			if ($this->course->insert('course_batches',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'New batch added..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to add new batch..!');
			}
			redirect('subadmin/courses/details/4/' . $course_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load course..!');
			redirect('subadmin/courses');
		}
	}
	public function editBatch()
	{
		$cb_id = $this->security->xss_clean($this->input->post('cb_id'));
		$get = $this->course->get_details('course_batches',array('cb_id' => $cb_id));
		if ($get->num_rows() > 0) {
			$batch = $get->row();
			$array = array(
				'batch_name' => $this->security->xss_clean($this->input->post('batch_name'))
			);
			if ($this->course->update('cb_id',$cb_id,'course_batches',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Batch updated..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to update batch..!');
			}
			redirect('subadmin/courses/details/4/' . $batch->course_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load batch..!');
			redirect('subadmin/courses');
		}
	}
	public function getBatch()
	{
		$cb_id = $this->security->xss_clean($this->input->post('cb_id'));
		$batch = $this->course->get_details('course_batches',array('cb_id' => $cb_id))->row();
		print_r(json_encode($batch));
	}
	public function addClass()
	{
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$get = $this->course->get_details('courses',array('course_id' => $course_id));
		if ($get->num_rows() > 0) {
			$time = $this->security->xss_clean($this->input->post('starting_time'));
			$time = date('Y-m-d H:i:s',strtotime($time));
			$array = array(
				'topic' => $this->security->xss_clean($this->input->post('topic')),
				'cs_id' => $this->security->xss_clean($this->input->post('cs_id')),
				'cb_id' => $this->security->xss_clean($this->input->post('cb_id')),
				'trainer_id' => $this->security->xss_clean($this->input->post('trainer_id')),
				'starting_time' => $time,
				'duration' => $this->security->xss_clean($this->input->post('duration')),
				'host_name' => $this->security->xss_clean($this->input->post('host_name')),
				'alternative_host' => $this->security->xss_clean($this->input->post('alternative_host')),
				'meeting_password' => $this->security->xss_clean($this->input->post('meeting_password')),
				'course_id' => $course_id,
				'created_by' => 'admin',
				'admin_id' => '1',
			);

			$return = $this->createMeeting($array['topic'],$time,$array['duration'],$array['meeting_password'],$array['alternative_host']);
			$response = json_decode($return,TRUE);
			if (isset($response['id'])) {
				$array['meeting_id'] = $response['id'];
				$array['start_url'] = $response['start_url'];
				$array['join_url'] = $response['join_url'];

				if ($this->course->insert('course_classes',$array)) {
					$this->session->set_flashdata('alert_type', 'success');
					$this->session->set_flashdata('alert_title', 'Success');
					$this->session->set_flashdata('alert_message', 'New class added..!');
				}
				else {
					$this->session->set_flashdata('alert_type', 'error');
					$this->session->set_flashdata('alert_title', 'Failed');
					$this->session->set_flashdata('alert_message', 'Failed to add class..!');
				}
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', $response['message']);
			}
			redirect('subadmin/courses/details/5/' . $course_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load course..!');
			redirect('subadmin/courses');
		}
	}
	public function createMeeting($topic,$time,$duration,$password,$alternative_hosts)
	{
		$tm1 = date('Y-m-d',strtotime($time));
		$tm2 = date('H:i:s',strtotime($time));
		$timestamp = $tm1 . 'T' . $tm2 . 'Z';
		$payload = '{
		  "topic": "' . $topic . '",
		  "type": 2,
		  "start_time": "' . $timestamp . '",
		  "duration": ' . $duration . ',
		  "schedule_for": "",
		  "timezone": "",
		  "password": " ' . $password . '",
		  "agenda": "string",
		  "recurrence": {
		    "type": "1",
		    "repeat_interval": "0",
		    "weekly_days": "0",
		    "monthly_day": "0",
		    "monthly_week": "0",
		    "monthly_week_day": "0",
		    "end_times": "0",
		    "end_date_time": "0"
		  },
		  "settings": {
		    "host_video": true,
		    "participant_video": false,
		    "cn_meeting": false,
		    "in_meeting": false,
		    "join_before_host": true,
		    "mute_upon_entry": true,
		    "watermark": false,
		    "use_pmi": false,
		    "approval_type": 2,
		    "registration_type": 1,
		    "audio": "both",
		    "auto_recording": "none",
		    "enforce_login": false,
		    "enforce_login_domains": "",
		    "alternative_hosts": "' . $alternative_hosts . '",
		    "global_dial_in_countries": null,
		    "registrants_email_notification": false
		  }
		}';
		$token = $this->generateToken();

		$service_url = 'https://api.zoom.us/v2/users/athul@gfx-it.com/meetings';
    	$curl = curl_init($service_url);

		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	        'Authorization: Bearer ' . $token,
	        'Content-Type: application/json'
	    ));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

		if ($err) {
		  return $err;
		} else {
		  return $response;
		}
	}
	public function editClass()
	{
		$cc_id = $this->security->xss_clean($this->input->post('cc_id'));
		$get = $this->course->get_details('course_classes',array('cc_id' => $cc_id));
		if ($get->num_rows() > 0) {
			$course_id = $get->row()->course_id;
			$time = $this->security->xss_clean($this->input->post('starting_time'));
			$time = date('Y-m-d H:i:s',strtotime($time));
			$array = array(
				'topic' => $this->security->xss_clean($this->input->post('topic')),
				'cs_id' => $this->security->xss_clean($this->input->post('cs_id')),
				'cb_id' => $this->security->xss_clean($this->input->post('cb_id')),
				'trainer_id' => $this->security->xss_clean($this->input->post('trainer_id')),
				'starting_time' => $time,
				'duration' => $this->security->xss_clean($this->input->post('duration')),
				'host_name' => $this->security->xss_clean($this->input->post('host_name')),
				'alternative_host' => $this->security->xss_clean($this->input->post('alternative_host')),
				'meeting_password' => $this->security->xss_clean($this->input->post('meeting_password')),
			);

			$return = $this->updateMeeting($array['topic'],$time,$array['duration'],$array['meeting_password'],$array['alternative_host'],$get->row()->meeting_id);
			$response = json_decode($return,TRUE);
			if (isset($response['message'])) {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', $response['message']);
			}
			else {
				if ($this->course->update('cc_id',$cc_id,'course_classes',$array)) {
					$this->session->set_flashdata('alert_type', 'success');
					$this->session->set_flashdata('alert_title', 'Success');
					$this->session->set_flashdata('alert_message', 'Class updated..!');
				}
				else {
					$this->session->set_flashdata('alert_type', 'error');
					$this->session->set_flashdata('alert_title', 'Failed');
					$this->session->set_flashdata('alert_message', 'Failed to update class..!');
				}
			}
			redirect('subadmin/courses/details/5/' . $course_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load course..!');
			redirect('subadmin/courses');
		}
	}
	public function updateMeeting($topic,$time,$duration,$password,$alternative_hosts,$meeting_id)
	{
		$tm1 = date('Y-m-d',strtotime($time));
		$tm2 = date('H:i:s',strtotime($time));
		$timestamp = $tm1 . 'T' . $tm2 . 'Z';

		$payload = '{
  			"agenda": "",
  			"duration": ' . $duration . ',
  			"password": "' . $password . '",
  			"settings": {
	    		"alternative_hosts": "' . $alternative_hosts . '",
	    		"approval_type": 0,
	    		"global_dial_in_countries": null,
	    		"host_video": false,
	    		"in_meeting": false,
	    		"join_before_host": true,
	    		"mute_upon_entry": false,
	    		"participant_video": false,
	    		"registrants_confirmation_email": false,
	    		"use_pmi": false,
	    		"waiting_room": false,
	    		"watermark": false
  			},
  			"start_time": "' . $timestamp . '",
  			"timezone": "",
  			"topic": "' . $topic . '",
  			"type": 2
		}';

		$token = $this->generateToken();

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.zoom.us/v2/meetings/" . $meeting_id,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "PATCH",
		  CURLOPT_POSTFIELDS => $payload,
		  CURLOPT_HTTPHEADER => array(
		    "authorization: Bearer " . $token,
		    "content-type: application/json"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
		if ($err) {
		  return $err;
		} else {
		  return true;
		}
	}
	public function getClass()
	{
		$cc_id = $this->security->xss_clean($this->input->post('cc_id'));
		$class = $this->course->get_details('course_classes',array('cc_id' => $cc_id))->row();
		$class->starting_time = date('d/m/Y h:i A',strtotime($class->starting_time));
		print_r(json_encode($class));
	}
	public function deleteClass($cc_id)
	{
		$get = $this->course->get_details('course_classes',array('cc_id' => $cc_id));
		if ($get->num_rows() > 0) {
			$section = $get->row();
			$course_id = $section->course_id;
			if ($this->course->delete('course_classes',array('cc_id' => $cc_id))) {
				if ($section->meeting_id != '') {
					$this->deleteMeeting($section->meeting_id);
				}
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Class deleted..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to delete class..!');
			}
			redirect('subadmin/courses/details/5/' . $course_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load class..!');
			redirect('subadmin/courses');
		}
	}
	public function deleteMeeting($meeting_id)
	{
		$token = $this->generateToken();
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.zoom.us/v2/meetings/" . $meeting_id,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "DELETE",
		  CURLOPT_HTTPHEADER => array(
		    "authorization: Bearer " . $token
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		return true;
	}
	public function getUnassignedStudents()
	{
		$cb_id = $this->input->post('cb_id');
		$course_id = $this->input->post('course_id');
		$students = $this->course->getUnassignedStudents($cb_id,$course_id);
		$string = '';
		foreach ($students as $student) {
			$string = $string . '<option value="' . $student->stu_id . '">' . $student->stu_name_english . '</option>';
		}
		$data = array(
			'students' => $string
		);
		print_r(json_encode($data));
	}
	public function addBatchStudents()
	{
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$get = $this->course->get_details('courses',array('course_id' => $course_id));
		if ($get->num_rows() > 0) {
			$students = $this->security->xss_clean($this->input->post('students'));
			$course_batch_id = $this->security->xss_clean($this->input->post('course_batch_id'));
			foreach ($students as $stu_id) {
				$array = array(
					'cb_id' => $course_batch_id,
					'course_id' => $course_id,
					'stu_id' => $stu_id
				);
				$this->course->insert('course_batch_students',$array);
			}

			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Student added to batch..!');
			redirect('subadmin/courses/details/4/' . $course_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load course..!');
			redirect('subadmin/courses');
		}
	}
	public function getBatchStudents()
	{
		$cb_id = $this->input->post('cb_id');
		$students = $this->course->getBatchStudents($cb_id);
		$table = '<table class="table table-datatable table-custom"><thead><th>Name</th><th>Mobile</th><th>Email</th></thead><tbody>';
		foreach ($students as $student) {
			$string = '<tr><td>' . $student->stu_name_english . '</td><td>' . $student->stu_mobile . '</td><td>' . $student->stu_email . '</td></tr>';
			$table = $table . $string;
		}
		$table = $table . '</tbody></table>';
		$array = array(
			'table' => $table
		);
		print_r(json_encode($array));
	}
	public function getBatchStudentsRemove()
	{
		$cb_id = $this->input->post('cb_id');
		$students = $this->course->getBatchStudents($cb_id);
		$string = '';
		foreach ($students as $student) {
			$string = $string . '<option value="' . $student->stu_id . '">' . $student->stu_name_english . '</option>';
		}
		$array = array(
			'student' => $string
		);
		print_r(json_encode($array));
	}
	public function removeBatchStudents()
	{
		$cb_id = $this->input->post('cb_id');
		$course_id = $this->input->post('course_id');
		$students = $this->input->post('students');
		foreach ($students as $student) {
			$this->course->delete('course_batch_students',array('cb_id' => $cb_id , 'stu_id' => $student));
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		$this->session->set_flashdata('alert_message', 'Students removed from batch..!');
		redirect('subadmin/courses/details/4/' . $course_id);
	}
	public function completeCourse($cc_id)
	{
		$get = $this->course->get_details('course_classes',array('cc_id' => $cc_id));
		if ($get->num_rows() > 0) {
			$section = $get->row();
			$course_id = $section->course_id;
			$array = array(
				'class_status' => 'completed'
			);
			if ($this->course->update('cc_id',$cc_id,'course_classes',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Course completed..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to delete class..!');
			}
			redirect('subadmin/courses/details/5/' . $course_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load class..!');
			redirect('subadmin/courses');
		}
	}

	public function sections()
	{
		$data['title'] = 'All sections';
		$this->load->view('subadmin/courses/sections',$data);
	}
	public function classes()
	{
		$data['title'] = 'All classes';
		$this->load->view('subadmin/courses/classes',$data);
	}
	public function getSections()
	{
		$result = $this->section->make_datatables();
		$data = array();
		foreach ($result as $res) {
			$sub_array = array();

			$sub_array[] = $res->section_title;
			$sub_array[] = $res->section_description;
			$sub_array[] = $res->course_title;
			$sub_array[] = '<a class="btn btn-primary btn-xs margin-bottom-20" href="' . site_url('subadmin/courses/details/3/'. $res->course_id) . '">View</a>';
			$data[] = $sub_array;
		}
		$output = array(
			"draw"   => intval($_POST['draw']),
			"recordsTotal" => $this->section->get_all_data(),
			"recordsFiltered" => $this->section->get_filtered_data(),
			"data" => $data
		);
		echo json_encode($output);
	}
	public function getClasses()
	{
		$today = date('Y-m-d H:i:s');
		$result = $this->classes->make_datatables();
		$data = array();
		foreach ($result as $res) {
			$sub_array = array();

			$sub_array[] = $res->topic;
			$sub_array[] = $res->course_title;
			$sub_array[] = date('d/m/Y h:i A',strtotime($res->starting_time));
			if (strtotime($today) > strtotime($res->starting_time)) {
				$sub_array[] = '<span style="font-weight:bold;">Class completed</span>';
			}
			else {
				$sub_array[] = '<span style="color:green;font-weight:bold;">Class pending</span>';
			}
			$sub_array[] = '<a class="btn btn-primary btn-xs margin-bottom-20" href="' . site_url('subadmin/courses/details/4/'. $res->course_id) . '">View</a>';
			$data[] = $sub_array;
		}
		$output = array(
			"draw"   => intval($_POST['draw']),
			"recordsTotal" => $this->classes->get_all_data(),
			"recordsFiltered" => $this->classes->get_filtered_data(),
			"data" => $data
		);
		echo json_encode($output);
	}
	public function addCertificate()
	{
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$get = $this->course->get_details('courses',array('course_id' => $course_id));
		if ($get->num_rows() > 0) {
			$array = array(
				'title' => $this->security->xss_clean($this->input->post('title')),
				'stu_id' => $this->security->xss_clean($this->input->post('stu_id')),
				'course_id' => $course_id,
				'date' => date('Y-m-d H:i:s')
			);
			$file = $_FILES['file'];
			if ($file['size'] > 0) {
				$tar = "uploads/certificates/";
				$rand = date('Ymd').mt_rand(1001,9999);
				$tar_file = $tar . $rand . basename($file['name']);
				if(move_uploaded_file($file["tmp_name"], $tar_file))
				{
					$array['certificate'] = $tar_file;
				}
			}
			if ($this->course->insert('students_certificates',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'New certificate added..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to upload certificate..!');
			}
			redirect('subadmin/courses/details/6/' . $course_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load course..!');
			redirect('subadmin/courses');
		}
	}
	public function deleteCertificate($sc_id)
	{
		$get = $this->course->get_details('students_certificates',array('sc_id' => $sc_id));
		if ($get->num_rows() > 0) {
			$certificate = $get->row();
			$course_id = $certificate->course_id;
			if ($this->course->delete('students_certificates',array('sc_id' => $sc_id))) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Certificate deleted..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to delete certificate..!');
			}
			redirect('subadmin/courses/details/6/' . $course_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load section..!');
			redirect('subadmin/courses');
		}
	}
	public function getCertificate()
	{
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$stu_id = $this->security->xss_clean($this->input->post('stu_id'));

		$certificates = $this->course->get_details('students_certificates',array('course_id' => $course_id , 'stu_id' => $stu_id))->result();
		$table = '<table class="table table-datatable table-custom"><thead><th>Title</th><th>Certificate</th><th>Delete</th></thead><tbody>';
		foreach ($certificates as $certificate) {
			$td1 = '<tr><td>' . $certificate->title . '</td>';
			$td2 = '<td><a class="btn btn-primary btn-xs" target="_blank" href="' . base_url() . $certificate->certificate . '">View</a></td>';
			$td3 = '<td><a class="btn btn-danger btn-xs" href="' . site_url('subadmin/courses/deleteCertificate/' . $certificate->sc_id) . '">Delete</a></td></tr>';
			$table = $table . $td1 . $td2 . $td3;
		}
		$table = $table . '</tbody></table>';
		$array = array(
			'table' => $table
		);
		print_r(json_encode($array));
	}
	public function class($cc_id)
	{
		$get = $this->course->get_details('course_classes',array('cc_id' => $cc_id));
		if ($get->num_rows() > 0) {
			$data['attachments'] = $this->course->get_details('course_class_attachments',array('cc_id' => $cc_id))->result();
			$data['cc_id'] = $cc_id;
			$this->load->view('subadmin/courses/class_attachments',$data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load class..!');
			redirect('subadmin/courses');
		}
	}
	public function addClassAttachment()
	{
		$cc_id = $this->security->xss_clean($this->input->post('cc_id'));
		$get = $this->course->get_details('course_classes',array('cc_id' => $cc_id));
		if ($get->num_rows() > 0) {
			$array = array(
				'title' => $this->security->xss_clean($this->input->post('title')),
				'type' => $this->security->xss_clean($this->input->post('type')),
				'cc_id' => $cc_id,
				'course_id' => $get->row()->course_id,
				'timestamp' => date('Y-m-d H:i:s')
			);
			$file = $_FILES['file'];
			if ($file['size'] > 0) {
				$tar = "uploads/class/";
				$rand = date('Ymd').mt_rand(1001,9999);
				$tar_file = $tar . $rand . basename($file['name']);
				if(move_uploaded_file($file["tmp_name"], $tar_file))
				{
					$array['attachment'] = $tar_file;
					if ($this->course->insert('course_class_attachments',$array)) {
						$this->session->set_flashdata('alert_type', 'success');
						$this->session->set_flashdata('alert_title', 'Success');
						$this->session->set_flashdata('alert_message', 'New certificate added..!');
					}
					else {
						$this->session->set_flashdata('alert_type', 'error');
						$this->session->set_flashdata('alert_title', 'Failed');
						$this->session->set_flashdata('alert_message', 'Failed to upload certificate..!');
					}
				}
				else {
					$this->session->set_flashdata('alert_type', 'error');
					$this->session->set_flashdata('alert_title', 'Failed');
					$this->session->set_flashdata('alert_message', 'Failed to upload attachment..!');
				}
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to upload attachment..!');
			}
			redirect('subadmin/courses/class/' . $cc_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load class..!');
			redirect('subadmin/courses');
		}
	}
	public function deleteClassAttachment($cca_id)
	{
		$get = $this->course->get_details('course_class_attachments',array('cca_id' => $cca_id));
		if ($get->num_rows() > 0) {
			$class = $get->row();
			if ($this->course->delete('course_class_attachments',array('cca_id' => $cca_id))) {
				if($class->attachment != '')
				{
					$unlink = FCPATH . $class->attachment;
					unlink($unlink);
				}
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Class attachment deleted..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to delete class attachment..!');
			}
			redirect('subadmin/courses/class/' . $class->cc_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load section..!');
			redirect('subadmin/courses');
		}
	}
	public function generateToken()
	{
		$key = 'fA5eKhl2TNiG3vGxXvMScg';
        $secret = 'vnN6RFYYUWhMyKFlVZ2DUdxl3Qxn9ym5R5h7';
        $token = array(
                "iss" => $key,
                "exp" => time() + 3600
        );
        $tok = JWT::encode( $token, $secret );
		return $tok;
	}
	public function addAttendance()
	{
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$date = $this->security->xss_clean($this->input->post('date'));
		$cb_id = $this->security->xss_clean($this->input->post('cb_id'));
		$student_count = $this->security->xss_clean($this->input->post('student_count'));
		$get = $this->course->get_details('attendance',array('cb_id' => $cb_id , 'date' => $date));
		if ($get->num_rows() > 0) {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Error');
			$this->session->set_flashdata('alert_message', 'Attendance already added..!');
		}
		else {
			$array = array(
				'cb_id' => $cb_id,
				'date' => $date
			);
			$att_id = $this->course->insert('attendance',$array);
			$i = 0;
			while ( $i < $student_count ) {
				$stu_id = $this->input->post('stu_id' . $i);
				$attendance = $this->input->post('attendance' . $i);
				$present = false;
				$absent = false;
				$late = false;
				if ($attendance == 'present') {
					$present = true;
				}elseif ($attendance == 'absent') {
					$absent = true;
				}
				else {
					$late = true;
				}
				$attend = array(
					'stu_id' => $stu_id,
					'att_id' => $att_id,
					'date' => $date,
					'present' => $present,
					'absent' => $absent,
					'late' => $late
				);
				$this->course->insert('course_attendance',$attend);
				$i++;
			}
			redirect('subadmin/courses/details/8/' . $course_id);
		}

	}
	public function editAttendance()
	{
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$student_count = $this->security->xss_clean($this->input->post('student_count'));

		$i = 0;
		while ( $i < $student_count ) {
			$ca_id = $this->input->post('ca_id' . $i);
			$attendance = $this->input->post('attendance' . $i);
			$present = false;
			$absent = false;
			$late = false;
			if ($attendance == 'present') {
				$present = true;
			}elseif ($attendance == 'absent') {
				$absent = true;
			}
			else {
				$late = true;
			}
			$attend = array(
				'present' => $present,
				'absent' => $absent,
				'late' => $late
			);
			$this->course->update('ca_id',$ca_id,'course_attendance',$attend);
			$i++;
		}
		redirect('subadmin/courses/details/8/' . $course_id);
	}
}
?>
