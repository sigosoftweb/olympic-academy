<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bridal extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('Common');
			if (!admin()) {
				redirect('app');
			}
	}
	public function index()
	{
		$data['images'] = $this->Common->get_details('bridal',array())->result();
		$this->load->view('admin/bridal/view',$data);
	}

	public function add()
	{
		$this->load->view('admin/bridal/add');
	}

	public function addImage()
	{
		$name = $this->security->xss_clean($this->input->post('name'));

		$file = $_FILES['image'];
		$tar = "uploads/bridal/";
		$rand=date('Ymd').mt_rand(1001,9999);
		$tar_file = $tar . $rand . basename($file['name']);
		if(move_uploaded_file($file["tmp_name"], $tar_file))
		{
			$array = [
			'name' => $name,
			'image' => $tar_file
    		];
    		if ($this->Common->insert('bridal',$array)) {
    			$this->session->set_flashdata('alert_type', 'success');
    			$this->session->set_flashdata('alert_title', 'Success');
    			$this->session->set_flashdata('alert_message', 'New image added..!');

    			redirect('admin/bridal');
    		}
    		else {
    			$this->session->set_flashdata('alert_type', 'error');
    			$this->session->set_flashdata('alert_title', 'Failed');
    			$this->session->set_flashdata('alert_message', 'Failed to insert data..!');

    			redirect('admin/bridal');
    		}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add image..!');

			redirect('admin/bridal');
		}
	}

	public function delete()
	{
		$image_id = $this->input->post('image_id');
		$check = $this->Common->get_details('bridal',array('image_id' => $image_id));
		if ($check->num_rows() > 0) {
			$bridal = $check->row();
			if ($this->Common->delete('bridal',array('image_id' => $image_id))) {
				$remove_path = FCPATH . $bridal->image;
				unlink($remove_path);

				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'image deleted successfully..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to remove image..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to remove image..!');
		}
		redirect('admin/bridal');
	}
}
?>
