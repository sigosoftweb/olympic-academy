<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Finance extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!subadmin()) {
            redirect('subadmin_login');
        }
		$this->load->model('subadmin/Model_finance','finance');
    }
    public function index() {
        $data['title'] = 'Olymbic | Students';
        $this->load->view('subadmin/finance/view', $data);
    }
	public function get()
	{   
		$admin    = $this->session->userdata['subadmin'];
		$admin_id = $admin['user_id'];
		$priority = $this->finance->get_details('subadmin_previleges',array('m_id'=>'27','subadmin_id'=>$admin_id))->row();

		$result = $this->finance->make_datatables();
		$data = array();
		foreach ($result as $res) {
			$sub_array = array();

			$sub_array[] = $res->stu_name_english . '<br>' . $res->stu_name_arabic;
			$sub_array[] = $res->stu_mobile . '<br>' . $res->stu_email;
			$sub_array[] = $res->course_title;
			$sub_array[] = date('d/m/Y',strtotime($res->date));

			$sub_array[] = $res->amount;
			$sub_array[] = $res->payment_id;
			$data[] = $sub_array;
		}

		$output = array(
			"draw"   => intval($_POST['draw']),
			"recordsTotal" => $this->finance->get_all_data(),
			"recordsFiltered" => $this->finance->get_filtered_data(),
			"data" => $data
		);
		echo json_encode($output);
	}
	public function filter()
	{
		$data['title'] = 'filter transactions';
		if (isset($_POST['from'])) {
			$data['param'] = true;
			$from_time = $this->security->xss_clean($this->input->post('from'));
			$to_time = $this->security->xss_clean($this->input->post('to'));
			$from = $from_time . ' 00:00:00';
			$to = $to_time . ' 00:00:00';
			$data['transactions'] = $this->finance->getTransactions($from,$to);
		}
		else {
			$data['param'] = false;
			$from_time = false;
			$to_time = false;
			$data['transactions'] = array();
		}
		$data['from'] = $from_time;
		$data['to'] = $to_time;
		$this->load->view('subadmin/finance/filter',$data);
	}
}
?>
