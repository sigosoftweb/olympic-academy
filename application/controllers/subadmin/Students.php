<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Students extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!subadmin()) {
            redirect('subadmin_login');
        }
		$this->load->model('subadmin/Model_students','student');
    }
    public function index() {
    	$admin    = $this->session->userdata['subadmin'];
		$admin_id = $admin['user_id'];

        $data['title'] = 'Olymbic | Students';
        $data['priority'] = $this->student->get_details('subadmin_previleges',array('m_id'=>'1','subadmin_id'=>$admin_id))->row();
        $this->load->view('subadmin/students/view', $data);
    }
	public function add()
	{
		$data['title'] = 'Olymbic | Add Students';
        $this->load->view('subadmin/students/add', $data);
	}
	public function edit($stu_id)
	{
		$get = $this->student->get_details('students',array('stu_id' => $stu_id));
		if ($get->num_rows() > 0) {
			$data['title'] = 'Olymbic | Edit Students';
			$data['student'] = $get->row();
			$this->load->view('subadmin/students/edit',$data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load student');
			redirect('subadmin/students');
		}
	}
	public function get()
	{  
		$admin    = $this->session->userdata['subadmin'];
		$admin_id = $admin['user_id'];
		
		$result = $this->student->make_datatables();
		$data = array();
		foreach ($result as $res) {
			$sub_array = array();
			$priority  = $this->student->get_details('subadmin_previleges',array('m_id'=>'1','subadmin_id'=>$admin_id))->row();
			if ($res->stu_image != '') 
			{
				$sub_array[] = '<img src="' . base_url() . $res->stu_image . '" width="100px" height="100px">';
			}
			else 
			{
				$sub_array[] = '<img src="' . base_url() . 'uploads/students/user.png" width="100px" height="100px">';
			}
			$sub_array[] = $res->stu_name_english . '<br>' . $res->stu_name_arabic;
			$sub_array[] = $res->stu_mobile . '<br>' . $res->stu_email;
			$sub_array[] = $res->stu_cpr;
			if ($res->stu_dob == NULL || $res->stu_dob == '0000-00-00') {
				$sub_array[] = '';
			}
			else {
				$sub_array[] = date('d/m/Y',strtotime($res->stu_dob));
			}

			$sub_array[] = $res->stu_education;
			$sub_array[] = $res->stu_contact;
			if ($res->stu_status) 
			{
				if($priority->block=='1') 
				{
				    $status = 'Active<br><a class="btn btn-danger btn-xs margin-bottom-20" href="' . site_url('subadmin/students/status/'.$res->stu_id . '/0') . '">Block</a>';
				 }
				 else
				 {
				 	$status = 'Active';
				 }   
			}
			else 
			{
				if($priority->block=='1') 
				{
				   $status = 'Blocked<br><a class="btn btn-success btn-xs margin-bottom-20" href="' . site_url('subadmin/students/status/'.$res->stu_id . '/1') . '">Activate</a>';
				}
				else
				{
					$status = 'Blocked';
				}   
			}
			$sub_array[] = $status;

			if($priority->edit=='1')
			{
				$sub_array[] = '<a class="btn btn-primary btn-xs" href="' . site_url('subadmin/students/edit/'.$res->stu_id) . '">Edit</a><br>
							<a class="btn btn-primary btn-xs" style="margin-top:10px;" href="' . site_url('subadmin/students/profile/'.$res->stu_id) . '">Profile</a>';
			}
			else
			{
				$sub_array[] = '<a class="btn btn-primary btn-xs" style="margin-top:10px;" href="' . site_url('subadmin/students/profile/'.$res->stu_id) . '">Profile</a>';
			}
			// $sub_array[] = '<a class="btn btn-primary btn-xs margin-bottom-20" onclick="changePassword('. $res->stu_id .')">Change<br>password</s>';

			$data[] = $sub_array;
		}

		$output = array(
			"draw"   => intval($_POST['draw']),
			"recordsTotal" => $this->student->get_all_data(),
			"recordsFiltered" => $this->student->get_filtered_data(),
			"data" => $data
		);
		echo json_encode($output);
	}
	public function addStudent()
	{
		$array = array(
					'stu_name_english' => $this->security->xss_clean($this->input->post('stu_name_english')),
					'stu_name_arabic' => $this->security->xss_clean($this->input->post('stu_name_arabic')),
					'stu_cpr' => $this->security->xss_clean($this->input->post('stu_cpr')),
					'stu_dob' => $this->security->xss_clean($this->input->post('stu_dob')),
					'stu_mobile' => $this->security->xss_clean($this->input->post('stu_mobile')),
					'stu_email' => $this->security->xss_clean($this->input->post('stu_email')),
					'stu_education' => $this->security->xss_clean($this->input->post('stu_education')),
					'stu_contact' => $this->security->xss_clean($this->input->post('stu_contact')),
					'stu_password' => md5($this->security->xss_clean($this->input->post('stu_password')))
		           );
		$file = $_FILES['image'];
		if ($file['size'] > 0) {
			$tar = "uploads/students/";
			$rand=date('Ymd').mt_rand(1001,9999);
			$tar_file = $tar . $rand . basename($file['name']);
			if(move_uploaded_file($file["tmp_name"], $tar_file))
			{
				$array['stu_image'] = $tar_file;
			}
			else {
				$array['stu_image'] = '';
			}
		}
		else {
			$array['stu_image'] = '';
		}

		if ($this->student->insert('students',$array)) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'New student added..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add student..!');
		}
		redirect('subadmin/students');
	}
	public function editStudent()
	{
		$stu_id = $this->security->xss_clean($this->input->post('stu_id'));
		$get = $this->student->get_details('students',array('stu_id' => $stu_id));
		if ($get->num_rows() > 0) {
			$student = $get->row();
			$array = array(
				'stu_name_english' => $this->security->xss_clean($this->input->post('stu_name_english')),
				'stu_name_arabic' => $this->security->xss_clean($this->input->post('stu_name_arabic')),
				'stu_cpr' => $this->security->xss_clean($this->input->post('stu_cpr')),
				'stu_mobile' => $this->security->xss_clean($this->input->post('stu_mobile')),
				'stu_email' => $this->security->xss_clean($this->input->post('stu_email')),
				'stu_dob' => $this->security->xss_clean($this->input->post('stu_dob')),
				'stu_education' => $this->security->xss_clean($this->input->post('stu_education')),
				'stu_contact' => $this->security->xss_clean($this->input->post('stu_contact')),
			);

			$file = $_FILES['image'];
			if ($file['size'] > 0) {
				$tar = "uploads/students/";
				$rand=date('Ymd').mt_rand(1001,9999);
				$tar_file = $tar . $rand . basename($file['name']);
				if(move_uploaded_file($file["tmp_name"], $tar_file))
				{
					$array['stu_image'] = $tar_file;
					if ($student->stu_image != '') {
						$unliink = FCPATH . $student->stu_image;
						unlink($unliink);
					}
				}
			}

			if ($this->student->update('stu_id',$stu_id,'students',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Student details updated..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to update student..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load student..!');
		}
		redirect('subadmin/students');
	}
	public function status($stu_id,$status)
	{
		$array = array(
			'stu_status' => $status
		);
		$this->student->update('stu_id',$stu_id,'students',$array);
		if ($status == '1') {
			$this->session->set_flashdata('alert_message', 'student activated..!');
		}
		else {
			$this->session->set_flashdata('alert_message', 'student deactivated..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('subadmin/students');
	}
	public function getDetails()
	{
		$stu_id = $this->security->xss_clean($this->input->post('stu_id'));
		$student = $this->student->get_details('students',array('stu_id' => $stu_id))->row();
		print_r(json_encode($student));
	}
	public function change()
	{
		$stu_id = $this->security->xss_clean($this->input->post('stu_id'));
		$password = $this->security->xss_clean($this->input->post('password'));
		$array = array(
			'stu_password' => md5($password)
		);
		if ($this->student->update('stu_id',$stu_id,'students',$array)) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Student password has been successfully reset');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to update student password');
		}
		redirect('subadmin/students');
	}
	public function profile($stu_id)
	{
		$get = $this->student->get_details('students',array('stu_id' => $stu_id));
		if ($get->num_rows() > 0) {
			$data['title'] = 'Olymbic | Student Profile';
			$data['student'] = $get->row();
			$data['courses'] = $this->student->getStudentsPackages($stu_id);
			$data['available'] = $this->student->getAvailableCourses($stu_id);
			$this->load->view('subadmin/students/profile',$data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load student');
			redirect('subadmin/students');
		}
	}
	public function updateProfile()
	{
		$stu_id = $this->security->xss_clean($this->input->post('stu_id'));
		$get = $this->student->get_details('students',array('stu_id' => $stu_id));
		if ($get->num_rows() > 0) {
			$student = $get->row();
			$array = array(
				'stu_name_english' => $this->security->xss_clean($this->input->post('stu_name_english')),
				'stu_name_arabic' => $this->security->xss_clean($this->input->post('stu_name_arabic')),
				'stu_cpr' => $this->security->xss_clean($this->input->post('stu_cpr')),
				'stu_mobile' => $this->security->xss_clean($this->input->post('stu_mobile')),
				'stu_email' => $this->security->xss_clean($this->input->post('stu_email')),
				'stu_dob' => $this->security->xss_clean($this->input->post('stu_dob')),
				'stu_education' => $this->security->xss_clean($this->input->post('stu_education')),
				'stu_contact' => $this->security->xss_clean($this->input->post('stu_contact')),
			);

			$file = $_FILES['image'];
			if ($file['size'] > 0) {
				$tar = "uploads/students/";
				$rand=date('Ymd').mt_rand(1001,9999);
				$tar_file = $tar . $rand . basename($file['name']);
				if(move_uploaded_file($file["tmp_name"], $tar_file))
				{
					$array['stu_image'] = $tar_file;
					if ($student->stu_image != '') {
						$unliink = FCPATH . $student->stu_image;
						unlink($unliink);
					}
				}
			}

			if ($this->student->update('stu_id',$stu_id,'students',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Student details updated..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to update student..!');
			}
			redirect('subadmin/students/profile/' . $stu_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load student..!');
			redirect('subadmin/students');
		}
	}
	public function subscribeCourse()
	{
		$stu_id = $this->security->xss_clean($this->input->post('stu_id'));
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$get = $this->student->get_details('courses',array('course_id' => $course_id));
		if ($get->num_rows() > 0) {
			$array = array(
				'stu_id' => $stu_id,
				'course_id' => $course_id,
				'date' => date('Y-m-d H:i:s'),
				'payment_id' => '',
				'amount' => $this->security->xss_clean($this->input->post('amount'))
			);
			if ($this->student->insert('students_packages',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Course subscribed');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to subscribe course, please try again..!');
			}
			redirect('subadmin/students/profile/' . $stu_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load course..!');
			redirect('subadmin/students');
		}
	}
	public function getCourse()
	{
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$course = $this->student->get_details('courses',array('course_id' => $course_id))->row();
		print_r(json_encode($course));
	}
}
?>
