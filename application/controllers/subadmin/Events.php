<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Events extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('subadmin/Model_events','events');
			if (!subadmin()) {
				redirect('subadmin_login');
			}
	}
	public function index()
	{
		$this->upcoming();
	}
	public function upcoming()
	{
		$data['events'] = $this->events->getUpcomingEvents();
		$this->load->view('subadmin/events/upcoming',$data);
	}
	public function completed()
	{
		$data['events'] = $this->events->getCompletedEvents();
		$this->load->view('subadmin/events/completed',$data);
	}
	public function add()
	{
		$this->load->view('subadmin/events/add');
	}
	public function addEvent()
	{
		$title = $this->security->xss_clean($this->input->post('title'));
		$description = $this->security->xss_clean($this->input->post('description'));
		$start_time = $this->security->xss_clean($this->input->post('start_time'));
		$end_time = $this->security->xss_clean($this->input->post('end_time'));

		$start_time = str_replace("/","-",$start_time);
		$end_time = str_replace("/","-",$end_time);
		$start = date('Y-m-d H:i:s',strtotime($start_time));
		$end = date('Y-m-d H:i:s',strtotime($end_time));

		$file = $_FILES['image'];
		$tar = "uploads/events/";
		$rand=date('Ymd').mt_rand(1001,9999);
		$tar_file = $tar . $rand . basename($file['name']);
		if(move_uploaded_file($file["tmp_name"], $tar_file))
		{
			$array = [
				'title' => $title,
				'description' => $description,
				'start_time' => $start,
				'end_time' => $end,
				'image' => $tar_file
			];

			if ($this->events->insert('events',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'New event added..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to add events..!');
			}

			$now = date('Y-m-d H:i:s');
			if( strtotime($end) > strtotime($now) )
			{
				redirect('subadmin/events/upcoming');
			}
			else {
				redirect('subadmin/events/completed');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to upload image..!');
		}
		redirect('subadmin/events');
	}
	public function editEvent()
	{
		$event_id = $this->security->xss_clean($this->input->post('event_id'));
		$get = $this->events->get_details('events',array('event_id' => $event_id));
		if ($get->num_rows() > 0) {
			$event = $get->row();
			$title = $this->security->xss_clean($this->input->post('title'));
			$description = $this->security->xss_clean($this->input->post('description'));
			$start_time = $this->security->xss_clean($this->input->post('start_time'));
			$end_time = $this->security->xss_clean($this->input->post('end_time'));

			$start_time = str_replace("/","-",$start_time);
			$end_time = str_replace("/","-",$end_time);
			$start = date('Y-m-d H:i:s',strtotime($start_time));
			$end = date('Y-m-d H:i:s',strtotime($end_time));

			$array = [
				'title' => $title,
				'description' => $description,
				'start_time' => $start,
				'end_time' => $end,
			];

			$file = $_FILES['image'];
			if ($file['size'] > 0) {
				$tar = "uploads/events/";
				$rand=date('Ymd').mt_rand(1001,9999);
				$tar_file = $tar . $rand . basename($file['name']);
				if(move_uploaded_file($file["tmp_name"], $tar_file))
				{
					if ($event->image != '') {
						$unlink = FCPATH . $event->image;
						unlink($unlink);
					}
					$array['image'] = $tar_file;
				}
			}
			$this->events->update('event_id',$event_id,'events',$array);

			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'New event added..!');

			$now = date('Y-m-d H:i:s');
			if( strtotime($end) > strtotime($now) )
			{
				redirect('subadmin/events/upcoming');
			}
			else {
				redirect('subadmin/events/completed');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load event..!');
			redirect('subadmin/events');
		}
	}

	public function edit($events_id)
	{
		$event = $this->events->get_details('events',array('event_id' => $events_id));
		if ($event->num_rows() > 0) {
			$data['event'] = $event->row();
			$this->load->view('subadmin/events/edit',$data);
		}
		else {
			redirect('subadmin/events');
		}
	}

	public function delete($events_id)
	{
		$get = $this->events->get_details('events',array('event_id' => $events_id));
		if ($get->num_rows() > 0) {
			$event = $get->row();
			if($this->events->delete('events',array('event_id' => $events_id)))
			{
				$remove_path = FCPATH . $event->image;
				unlink($remove_path);

				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Event deleted successfully..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to remove Event..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load Event..!');
		}
		redirect('subadmin/events');
	}
	public function status($type,$event_id,$status)
	{
		$array = array(
			'status' => $status
		);
		$this->events->update('event_id',$event_id,'events',$array);

		if ($status == '1') {
			$message = 'Event activated';
		}
		else {
			$message = 'Event blocked';
		}

		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		$this->session->set_flashdata('alert_message', $message);
		if ($type == 'c') {
			redirect('subadmin/events/completed');
		}
		else {
			redirect('subadmin/events/upcoming');
		}
	}
}
?>
