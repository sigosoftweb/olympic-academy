<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Trainers extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!subadmin()) {
            redirect('subadmin_login');
        }
		$this->load->model('subadmin/Model_trainers','trainer');
    }
    public function index() {
    	$admin    = $this->session->userdata['subadmin'];
		$admin_id = $admin['user_id'];

        $data['title'] = 'Olymbic | Trainers';
        $data['priority'] = $this->trainer->get_details('subadmin_previleges',array('m_id'=>'3','subadmin_id'=>$admin_id))->row();
        $this->load->view('subadmin/trainers/view', $data);
    }
	public function add()
	{
		$data['title'] = 'Olymbic | Add trainers';
        $this->load->view('subadmin/trainers/add', $data);
	}
	public function edit($trainer_id)
	{
		$get = $this->trainer->get_details('trainers',array('trainer_id' => $trainer_id));
		if ($get->num_rows() > 0) {
			$data['title'] = 'Olymbic | Edit trainers';
			$data['trainer'] = $get->row();
			$this->load->view('subadmin/trainers/edit',$data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load trainer');
			redirect('subadmin/trainers');
		}
	}
	public function get()
	{   
		$admin    = $this->session->userdata['subadmin'];
		$admin_id = $admin['user_id'];

		$result = $this->trainer->make_datatables();
		$data = array();
		$priority  = $this->trainer->get_details('subadmin_previleges',array('m_id'=>'3','subadmin_id'=>$admin_id))->row();
		foreach ($result as $res) 
		{
			$sub_array = array();
			if ($res->trainer_image != '') 
			{
				$sub_array[] = '<img src="' . base_url() . $res->trainer_image . '" width="100px" height="100px">';
			}
			else 
			{
				$sub_array[] = '<img src="' . base_url() . 'uploads/trainers/user.png" width="100px" height="100px">';
			}
			$sub_array[] = $res->trainer_name_english . '<br>' . $res->trainer_name_arabic;
			$sub_array[] = $res->trainer_mobile . '<br>' . $res->trainer_email;
			$sub_array[] = $res->trainer_cpr;
			$sub_array[] = $res->trainer_education;
			$sub_array[] = $res->trainer_experience;
			$sub_array[] = $res->trainer_contact;
			if ($res->trainer_status) {
				if($priority->block=='1') 
				{
					$status = 'Active<br><a class="btn btn-danger btn-xs margin-bottom-20" href="' . site_url('subadmin/trainers/status/'.$res->trainer_id . '/0') . '">Block</a>';
				}
				else
				{
					$status = 'Active';
				}
			}
			else {
				if($priority->block=='1') 
				{
					$status = 'Blocked<br><a class="btn btn-success btn-xs margin-bottom-20" href="' . site_url('subadmin/trainers/status/'.$res->trainer_id . '/1') . '">Activate</a>';
				}
				else
				{
					$status = 'Blocked';
				}
				
			}
			$sub_array[] = $status;
			if($priority->edit=='1')
			{
				$sub_array[] = '<a class="btn btn-primary btn-xs margin-bottom-20" href="' . site_url('subadmin/trainers/edit/'.$res->trainer_id) . '">Edit</a>';
			}
			
			// $sub_array[] = '<a class="btn btn-primary btn-xs margin-bottom-20" onclick="changePassword('. $res->trainer_id .')">Change<br>password</s>';

			$data[] = $sub_array;
		}

		$output = array(
			"draw"   => intval($_POST['draw']),
			"recordsTotal" => $this->trainer->get_all_data(),
			"recordsFiltered" => $this->trainer->get_filtered_data(),
			"data" => $data
		);
		echo json_encode($output);
	}
	public function addTrainer()
	{
		$array = array(
			'trainer_name_english' => $this->security->xss_clean($this->input->post('trainer_name_english')),
			'trainer_name_arabic' => $this->security->xss_clean($this->input->post('trainer_name_arabic')),
			'trainer_cpr' => $this->security->xss_clean($this->input->post('trainer_cpr')),
			'trainer_mobile' => $this->security->xss_clean($this->input->post('trainer_mobile')),
			'trainer_email' => $this->security->xss_clean($this->input->post('trainer_email')),
			'trainer_education' => $this->security->xss_clean($this->input->post('trainer_education')),
			'trainer_experience' => $this->security->xss_clean($this->input->post('trainer_experience')),
			'trainer_contact' => $this->security->xss_clean($this->input->post('trainer_contact')),
			'trainer_password' => md5($this->security->xss_clean($this->input->post('trainer_password')))
		);
		$file = $_FILES['image'];
		if ($file['size'] > 0) {
			$tar = "uploads/trainers/";
			$rand=date('Ymd').mt_rand(1001,9999);
			$tar_file = $tar . $rand . basename($file['name']);
			if(move_uploaded_file($file["tmp_name"], $tar_file))
			{
				$array['trainer_image'] = $tar_file;
			}
			else {
				$array['trainer_image'] = '';
			}
		}
		else {
			$array['trainer_image'] = '';
		}

		if ($this->trainer->insert('trainers',$array)) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'New trainer added..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add trainer..!');
		}
		redirect('subadmin/trainers');
	}
	public function editTrainer()
	{
		$trainer_id = $this->security->xss_clean($this->input->post('trainer_id'));
		$get = $this->trainer->get_details('trainers',array('trainer_id' => $trainer_id));
		if ($get->num_rows() > 0) {
			$trainer = $get->row();
			$array = array(
				'trainer_name_english' => $this->security->xss_clean($this->input->post('trainer_name_english')),
				'trainer_name_arabic' => $this->security->xss_clean($this->input->post('trainer_name_arabic')),
				'trainer_cpr' => $this->security->xss_clean($this->input->post('trainer_cpr')),
				'trainer_mobile' => $this->security->xss_clean($this->input->post('trainer_mobile')),
				'trainer_email' => $this->security->xss_clean($this->input->post('trainer_email')),
				'trainer_education' => $this->security->xss_clean($this->input->post('trainer_education')),
				'trainer_experience' => $this->security->xss_clean($this->input->post('trainer_experience')),
				'trainer_contact' => $this->security->xss_clean($this->input->post('trainer_contact')),
			);

			$file = $_FILES['image'];
			if ($file['size'] > 0) {
				$tar = "uploads/trainers/";
				$rand=date('Ymd').mt_rand(1001,9999);
				$tar_file = $tar . $rand . basename($file['name']);
				if(move_uploaded_file($file["tmp_name"], $tar_file))
				{
					$array['trainer_image'] = $tar_file;
					if ($trainer->trainer_image != '') {
						$unliink = FCPATH . $trainer->trainer_image;
						unlink($unliink);
					}
				}
			}

			if ($this->trainer->update('trainer_id',$trainer_id,'trainers',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'trainer details updated..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to update trainer..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load trainer..!');
		}
		redirect('subadmin/trainers');
	}
	public function status($trainer_id,$status)
	{
		$array = array(
			'trainer_status' => $status
		);
		$this->trainer->update('trainer_id',$trainer_id,'trainers',$array);
		if ($status == '1') {
			$this->session->set_flashdata('alert_message', 'trainer activated..!');
		}
		else {
			$this->session->set_flashdata('alert_message', 'trainer deactivated..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('subadmin/trainers');
	}
	public function getDetails()
	{
		$trainer_id = $this->security->xss_clean($this->input->post('trainer_id'));
		$trainer = $this->trainer->get_details('trainers',array('trainer_id' => $trainer_id))->row();
		print_r(json_encode($trainer));
	}
	public function change()
	{
		$trainer_id = $this->security->xss_clean($this->input->post('trainer_id'));
		$password = $this->security->xss_clean($this->input->post('password'));
		$array = array(
			'trainer_password' => md5($password)
		);
		if ($this->trainer->update('trainer_id',$trainer_id,'trainers',$array)) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'trainer password has been successfully reset');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to update trainer password');
		}
		redirect('subadmin/trainers');
	}
}
?>
