<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Categories extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!subadmin()) {
            redirect('subadmin_login');
        }
		$this->load->model('subadmin/Model_categories','category');
    }
    public function index() {

    	$admin    = $this->session->userdata['subadmin'];
		$admin_id = $admin['user_id'];

        $data['title'] = 'Olymbic | Catgories';
		$data['categories'] = $this->category->getAllCategories();
		$data['priority'] = $this->category->get_details('subadmin_previleges',array('m_id'=>'4','subadmin_id'=>$admin_id))->row();
        $this->load->view('subadmin/categories/view', $data);
    }
	public function add()
	{
		$category = $this->security->xss_clean($this->input->post('category'));
		$file = $_FILES['image'];
		$tar = "uploads/categories/";
		$rand=date('Ymd').mt_rand(1001,9999);
		$tar_file = $tar . $rand . basename($file['name']);
		if(move_uploaded_file($file["tmp_name"], $tar_file))
		{
			$array = [
    			'cat_name' => $category,
    			'cat_image' => $tar_file
    		];
    		if ($this->category->insert('categories',$array)) {
    			$this->session->set_flashdata('alert_type', 'success');
    			$this->session->set_flashdata('alert_title', 'Success');
    			$this->session->set_flashdata('alert_message', 'New category added..!');
    		}
    		else {
    			$this->session->set_flashdata('alert_type', 'error');
    			$this->session->set_flashdata('alert_title', 'Failed');
    			$this->session->set_flashdata('alert_message', 'Failed to add category..!');
    		}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to upload image..!');
		}
		redirect('subadmin/categories');
	}
	public function edit()
	{
		$cat_id = $this->security->xss_clean($this->input->post('cat_id'));
		$category = $this->security->xss_clean($this->input->post('category'));
		
		$array = [
    		'cat_name' => $category
    	];
		
		$file = $_FILES['image'];
		if($file['size'] > 0)
		{
		    $tar = "uploads/categories/";
    		$rand=date('Ymd').mt_rand(1001,9999);
    		$tar_file = $tar . $rand . basename($file['name']);
    		if(move_uploaded_file($file["tmp_name"], $tar_file))
    		{
    		    $array['cat_image'] = $tar_file;
    		}
		}
		
		if ($this->category->update('cat_id',$cat_id,'categories',$array)) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Logged in Successfully..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add category..!');
		}
		redirect('subadmin/categories');
	}
	public function status($cat_id,$status)
	{
		$array = array(
			'cat_status' => $status
		);
		$this->category->update('cat_id',$cat_id,'categories',$array);
		if ($status == '1') {
			$this->session->set_flashdata('alert_message', 'category activated..!');
		}
		else {
			$this->session->set_flashdata('alert_message', 'category deactivated..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('subadmin/categories');
	}
	public function getDetails()
	{
		$cat_id = $this->security->xss_clean($this->input->post('cat_id'));
		$category = $this->category->get_details('categories',array('cat_id' => $cat_id))->row();
		print_r(json_encode($category));
	}
}
?>
