<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Exams extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!admin()) {
            redirect('login');
        }
        $this->load->model('admin/Model_exams', 'exam');
    }
    public function add($course_id) {
        $data['title'] = 'Add exam';
        $get = $this->exam->get_details('courses', array('course_id' => $course_id));
        if ($get->num_rows() > 0) {
            $data['course'] = $get->row();
            $data['sections'] = $this->exam->getSections($course_id);
            $data['batches'] = $this->exam->getBatches($course_id);
            $this->load->view('admin/exams/add', $data);
        } else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Error');
            $this->session->set_flashdata('alert_message', 'Failed to load course..!');
            redirect('admin/courses');
        }
    }
    public function addExam() {
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
        $name = $this->security->xss_clean($this->input->post('name'));
        $cb_id = $this->security->xss_clean($this->input->post('cb_id'));
		$cs_id = $this->security->xss_clean($this->input->post('cs_id'));
        $no_of_questions = $this->security->xss_clean($this->input->post('no_of_questions'));
        $exam_time = $this->security->xss_clean($this->input->post('exam_time'));
        $time = $this->security->xss_clean($this->input->post('time'));
        $instructions = $this->security->xss_clean($this->input->post('instructions'));
		if ($cb_id == '0' && $cs_id == '0') {
			$type = 'course';
		}
		else {
			if ($cb_id != '0') {
				$type = 'batch';
			}
			else {
				$type = 'section';
			}
		}
        $array = ['exam_name' => $name, 'type' => $type, 'course_id' => $course_id, 'cb_id' => $cb_id, 'cs_id' => $cs_id, 'no_questions' => $no_of_questions, 'exam_time' => $exam_time, 'time_status' => $time ];
        if ($time == '1') {
            $from = $this->security->xss_clean($this->input->post('from_time'));
			$from = str_replace("/","-",$from);
			$from = date('Y-m-d H:i:s',strtotime($from));
            $end = date('Y-m-d H:i:s', strtotime($from . ' +' . $exam_time . ' minutes'));
            $array['from_time'] = $from;
            $array['to_time'] = $end;
        }
        if ($exam_id = $this->exam->insert('exams', $array)) {
            foreach ($instructions as $instruction) {
                $instruction_array = ['exam_id' => $exam_id, 'instruction' => $instruction, 'timestamp' => date('Y-m-d H:i:s') ];
                $this->exam->insert('exam_instructions', $instruction_array);
            }
            $this->session->set_flashdata('alert_type', 'success');
            $this->session->set_flashdata('alert_title', 'Success');
            $this->session->set_flashdata('alert_message', 'Exam added');
            redirect('admin/exams/questions/' . $exam_id);
        } else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to add exam..!');
            redirect('admin/exams');
        }
    }
	public function questions($exam_id)
    {
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
            $data['questions'] = $question = $this->exam->get_details('questions',array('exam_id' => $exam_id))->num_rows();
            $data['exam'] = $exam = $get->row();
            if ($question == $exam->no_questions) {
                $array = [
                    'registration_status' => '1',
                    'exam_status' => '1'
                ];
                $this->exam->update('exam_id',$exam_id,'exams',$array);
                redirect('admin/exams');
            }
            else {
                $this->load->view('admin/exams/questions',$data);
            }
        }
        else {
            redirect('admin/exams');
        }
    }
	public function get()
    {
		$course_id = $this->input->post('course_id');
        $result = $this->exam->make_datatables($course_id);
        $data   = array();
        foreach ($result as $res) {
            $sub_array   = array();
            $sub_array[] = $res->exam_name;
            $sub_array[] = $res->batch_name;
			$sub_array[] = $res->section_title;
            $sub_array[] = $res->no_questions;
            $sub_array[] = $res->exam_time .' minutes';

            if ($res->time_status == '1') {
                $sub_array[] = 'From : ' . date('d/m/Y h:i A',strtotime($res->from_time)) . '<br>To : ' . date('d/m/Y h:i A',strtotime($res->to_time));
            }
            else {
                $sub_array[] = 'Time not set';
            }
            $sub_array[] = '<a class="btn btn-primary btn-xs"  href="' . site_url('admin/exams/edit/' . $res->exam_id) . '">Edit</a>';
            $sub_array[] = '<a class="btn btn-primary btn-xs"  href="' . site_url('admin/exams/question/' . $res->exam_id) . '">Questions</a>';
            $sub_array[] = '<a class="btn btn-primary btn-xs"  href="' . site_url('admin/exams/instructions/' . $res->exam_id) . '">Instruction</a>';
            if ($res->exam_status == '1') {
                $sub_array[] = '<a class="btn btn-danger btn-xs"  href="' . site_url('admin/exams/status/' . $res->exam_id .'/0') . '">Block</a>';
            }
            else {
                $sub_array[] = '<a class="btn btn-success btn-xs"  href="' . site_url('admin/exams/status/' . $res->exam_id .'/1') . '">Activate</a>';
            }
            $sub_array[] = '<a class="btn btn-danger btn-xs" onclick="return confirm(`Are you sure to delete?`)"  href="' . site_url('admin/exams/deleteExam/' . $res->exam_id) . '">Delete</a>';

            $data[]      = $sub_array;
        }
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $this->exam->get_all_data($course_id),
            "recordsFiltered" => $this->exam->get_filtered_data($course_id),
            "data" => $data
        );
        echo json_encode($output);
    }
	public function status($exam_id,$status)
    {
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
            $exam = $get->row();
            if ($exam->registration_status == '0') {
                $this->session->set_flashdata('alert_type', 'error');
    			$this->session->set_flashdata('alert_title', 'Failed');
    			$this->session->set_flashdata('alert_message', 'Registration of exam still pending');
            }
            else {
                $array = [
                    'exam_status' => $status
                ];
                $this->exam->update('exam_id',$exam_id,'exams',$array);
                $this->session->set_flashdata('alert_type', 'success');
    			$this->session->set_flashdata('alert_title', 'Success');
                if ($status == '0') {
                    $this->session->set_flashdata('alert_message', 'Exam deactivated..!');
                }
                else {
                    $this->session->set_flashdata('alert_message', 'Exam activated..!');
                }
            }
			redirect('admin/courses/details/7/' . $exam->course_id);
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load exam..!');
			redirect('admin/courses');
        }
    }
	public function instructions($exam_id)
    {
		$data['title'] = 'Exam instructions';
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
            $data['instructions']  = $this->exam->get_details('exam_instructions',array('exam_id' => $exam_id))->result();
            $data['exam']  = $get->row();
            $this->load->view('admin/exams/instructions',$data);
        }
        else {
            redirect('admin/courses');
        }
    }
	public function deleteExam($exam_id)
    {
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
			$exam = $get->row();
            $this->exam->delete('exams',array('exam_id' => $exam_id));
            $this->exam->delete('questions',array('exam_id' => $exam_id));
            // $this->exam->delete('students_question_answers',array('exam_id' => $exam_id));

            $this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Exam and all data related to this exam was deleted');
            redirect('admin/courses/details/7/' . $exam->course_id);
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to delete exam..!');
            redirect('admin/courses');
        }
    }
	public function question($exam_id)
    {
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
            $data['questions'] = $question = $this->exam->get_details('questions',array('exam_id' => $exam_id))->result();
            $data['exam'] = $exam = $get->row();
            $this->load->view('admin/exams/view_questions',$data);
        }
        else {
            redirect('admin/exams');
        }
    }
}
?>
