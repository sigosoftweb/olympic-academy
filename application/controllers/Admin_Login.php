<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Login extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('Model_login','login');
	}
	public function index()
	{
		$this->load->view('login/admin');
	}
	public function loginCheck()
	{
		$email = $this->security->xss_clean($this->input->post('email'));
		$password = $pass = $this->security->xss_clean($this->input->post('password'));
		$get = $this->login->get_details('admin',array('username' => $email, 'password' => md5($password)));
		if ($get->num_rows() > 0) {
			$user = $get->row();
			$session = [
				'user_id' => $user->admin_id,
				'name'    => $user->name,
				'user_type' => 'admin'
			];
			$this->session->set_userdata('admin',$session);

			$remember = $this->security->xss_clean($this->input->post('remember'));
			if ($rem) {
				$hour = time() + 3600 * 24 * 30;
				setcookie('olympic_username', $user_name, $hour);
				setcookie('olympic_password', $pass, $hour);
			}
			else {
				$hour = time() - 3600 * 24 * 30;
				setcookie('olympic_username', "", $hour);
				setcookie('olympic_password', "", $hour);
			}
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Logged in Successfully..!');
			redirect('admin/dashboard');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Login failed');
			redirect('login');
		}
	}
	public function logout()
	{
		setcookie('olympic_username');
		setcookie('olympic_password');
		$this->session->unset_userdata('admin');
		redirect('Admin_Login');
	}
}
?>
