<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagenotfound extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
	}
	public function index()
	{
		if ($this->session->userdata('admin')) {
			$this->load->view('pageadmin');
		}
		else {
			$this->load->view('pagecommon');
		}
	}
}
?>
