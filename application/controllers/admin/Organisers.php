<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class organisers extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!admin()) {
            redirect('admin');
        }
		$this->load->model('admin/Model_organisers','organiser');
    }
    public function index() {
        $data['title'] = 'Olymbic | organisers';
        $this->load->view('admin/organisers/view', $data);
    }
	public function add()
	{
		$data['title'] = 'Olymbic | Add organisers';
        $this->load->view('admin/organisers/add', $data);
	}
	public function edit($organiser_id)
	{
		$get = $this->organiser->get_details('organisers',array('organiser_id' => $organiser_id));
		if ($get->num_rows() > 0) {
			$data['title'] = 'Olymbic | Edit organisers';
			$data['organiser'] = $get->row();
			$this->load->view('admin/organisers/edit',$data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load organiser');
			redirect('admin/organisers');
		}
	}
	public function get()
	{
		$result = $this->organiser->make_datatables();
		$data = array();
		foreach ($result as $res) {
			$sub_array = array();
			if ($res->organiser_image != '') {
				$sub_array[] = '<img src="' . base_url() . $res->organiser_image . '" width="100px" height="100px">';
			}
			else {
				$sub_array[] = '<img src="' . base_url() . 'uploads/organisers/user.png" width="100px" height="100px">';
			}
			$sub_array[] = $res->organiser_first . '<br>' . $res->organiser_second;
			$sub_array[] = $res->organiser_mobile . '<br>' . $res->organiser_email;
			$sub_array[] = $res->organiser_cpr;
			$sub_array[] = $res->organiser_education;
			$sub_array[] = $res->organiser_experience;
			$sub_array[] = $res->organiser_contact;
			if ($res->organiser_status) {
				$status = 'Active<br><a class="btn btn-danger btn-xs margin-bottom-20" href="' . site_url('admin/organisers/status/'.$res->organiser_id . '/0') . '">Block</a>';
			}
			else {
				$status = 'Blocked<br><a class="btn btn-success btn-xs margin-bottom-20" href="' . site_url('admin/organisers/status/'.$res->organiser_id . '/1') . '">Activate</a>';
			}
			$sub_array[] = $status;
			$sub_array[] = '<a class="btn btn-primary btn-xs margin-bottom-20" href="' . site_url('admin/organisers/edit/'.$res->organiser_id) . '">Edit</a>';

			$data[] = $sub_array;
		}

		$output = array(
			"draw"   => intval($_POST['draw']),
			"recordsTotal" => $this->organiser->get_all_data(),
			"recordsFiltered" => $this->organiser->get_filtered_data(),
			"data" => $data
		);
		echo json_encode($output);
	}
	public function addOrganiser()
	{
		$array = array(
			'organiser_first' => $this->security->xss_clean($this->input->post('organiser_first')),
			'organiser_second' => $this->security->xss_clean($this->input->post('organiser_second')),
			'arabic_first' => $this->security->xss_clean($this->input->post('first_arabic')),
			'arabic_second' => $this->security->xss_clean($this->input->post('second_arabic')),
			'organiser_cpr' => $this->security->xss_clean($this->input->post('organiser_cpr')),
			'organiser_mobile' => $this->security->xss_clean($this->input->post('organiser_mobile')),
			'organiser_email' => $this->security->xss_clean($this->input->post('organiser_email')),
			'organiser_education' => $this->security->xss_clean($this->input->post('organiser_education')),
			'organiser_experience' => $this->security->xss_clean($this->input->post('organiser_experience')),
			'education_arabic' => $this->security->xss_clean($this->input->post('organiser_education_arabic')),
			'experience_arabic' => $this->security->xss_clean($this->input->post('organiser_experience_arabic')),
			'organiser_contact' => $this->security->xss_clean($this->input->post('organiser_contact')),
		);
		$image = $this->input->post('image');
		if ($image != '')
		{
			$img = substr($image, strpos($image, ",") + 1);
			$url = FCPATH.'uploads/organisers/';
			$rand = $plan_name.date('Ymd').mt_rand(1001,9999);
			$userpath = $url.$rand.'.png';
			$path = "uploads/organisers/".$rand.'.png';
			file_put_contents($userpath,base64_decode($img));

            $array['organiser_image'] = $path;
		}
        else {
            $array['organiser_image'] = '';
        }

		if ($this->organiser->insert('organisers',$array)) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'New organiser added..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add organiser..!');
		}
		redirect('admin/organisers');
	}
	public function editorganiser()
	{
		$organiser_id = $this->security->xss_clean($this->input->post('organiser_id'));
		$get = $this->organiser->get_details('organisers',array('organiser_id' => $organiser_id));
		if ($get->num_rows() > 0) {
			$organiser = $get->row();
			$array = array(
				'organiser_first' => $this->security->xss_clean($this->input->post('organiser_first')),
				'organiser_second' => $this->security->xss_clean($this->input->post('organiser_second')),
				'arabic_first' => $this->security->xss_clean($this->input->post('first_arabic')),
				'arabic_second' => $this->security->xss_clean($this->input->post('second_arabic')),
				'organiser_cpr' => $this->security->xss_clean($this->input->post('organiser_cpr')),
				'organiser_mobile' => $this->security->xss_clean($this->input->post('organiser_mobile')),
				'organiser_email' => $this->security->xss_clean($this->input->post('organiser_email')),
				'organiser_education' => $this->security->xss_clean($this->input->post('organiser_education')),
				'organiser_experience' => $this->security->xss_clean($this->input->post('organiser_experience')),
				'education_arabic' => $this->security->xss_clean($this->input->post('organiser_education_arabic')),
				'experience_arabic' => $this->security->xss_clean($this->input->post('organiser_experience_arabic')),
				'organiser_contact' => $this->security->xss_clean($this->input->post('organiser_contact')),
			);

			$image = $this->input->post('image');
			if ($image != '')
			{
				$img = substr($image, strpos($image, ",") + 1);
				$url = FCPATH.'uploads/organisers/';
				$rand = $plan_name.date('Ymd').mt_rand(1001,9999);
				$userpath = $url.$rand.'.png';
				$path = "uploads/organisers/".$rand.'.png';
				file_put_contents($userpath,base64_decode($img));

	            $array['organiser_image'] = $path;
			}

			if ($this->organiser->update('organiser_id',$organiser_id,'organisers',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'organiser details updated..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to update organiser..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load organiser..!');
		}
		redirect('admin/organisers');
	}
	public function status($organiser_id,$status)
	{
		$array = array(
			'organiser_status' => $status
		);
		$this->organiser->update('organiser_id',$organiser_id,'organisers',$array);
		if ($status == '1') {
			$this->session->set_flashdata('alert_message', 'organiser activated..!');
		}
		else {
			$this->session->set_flashdata('alert_message', 'organiser deactivated..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('admin/organisers');
	}
	public function getDetails()
	{
		$organiser_id = $this->security->xss_clean($this->input->post('organiser_id'));
		$organiser = $this->organiser->get_details('organisers',array('organiser_id' => $organiser_id))->row();
		print_r(json_encode($organiser));
	}
}
?>
