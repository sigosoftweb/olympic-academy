<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('admin/Model_reports','reports');
			$this->load->model('Common');
			if (!admin()) {
				redirect('admin');
			}
	}
	public function index()
	{
		$data['title'] = 'Olympic | Resources';
		$data['cources'] = $this->reports->getCources();
		$this->load->view('admin/reports/select',$data);
	}

	public function report()
	{
	    $start        = $this->input->post('from');
		$end          = $this->input->post('to');
		$course_id    = $this->input->post('course_id');
		$start_date   = date('Y-m-d',strtotime($start));
		$end_date     = date('Y-m-d',strtotime($end));
		if($start!=='' && $end!='')
		{
		    $subscribed_count   = $this->reports->getSubscribedCount($course_id,$start_date,$end_date);
    		$data['subscribed'] = $subscribed_count;
    		
    		$earnings           = $this->reports->getEarning($course_id,$start_date,$end_date);
    		$data['earnings']   = $earnings;
    		
    		$online_payment     = $this->reports->getOnlinePayments($course_id,$start_date,$end_date);
    		$data['online']     = $online_payment;
    		
    		$tamkeen_payment    = $this->reports->getTamkeenPayments($course_id,$start_date,$end_date);
    		$data['tamkeen']    = $tamkeen_payment;
    		
    		$cash_payment       = $this->reports->getCashPayments($course_id,$start_date,$end_date);
    		$data['cash']       = $cash_payment;
    		
    		$students           = $this->reports->getSubscribedStudents($course_id,$start_date,$end_date);
    		$data['students']   = $students;
    		
    		$data['from']       = $start;
            $data['to']         = $end;
            $data['date']       = date('d-M-Y',strtotime($start)).' To '.date('d-M-Y',strtotime($end));
		}
		else
		{
		    $subscribed_count   = $this->reports->getSubscribedCountAll($course_id);
    		$data['subscribed'] = $subscribed_count;
    		
    		$earnings           = $this->reports->getEarningAll($course_id);
    		$data['earnings']   = $earnings;
    		
    		$online_payment     = $this->reports->getOnlinePaymentsAll($course_id);
    		$data['online']     = $online_payment;
    		
    		$tamkeen_payment    = $this->reports->getTamkeenPaymentsAll($course_id);
    		$data['tamkeen']    = $tamkeen_payment;
    		
    		$cash_payment       = $this->reports->getCashPaymentsAll($course_id);
    		$data['cash']       = $cash_payment;
    		
    		$students           = $this->reports->getSubscribedStudentsAll($course_id);
    		$data['students']   = $students;
    		
    		$data['from']       = $start;
            $data['to']         = $end;
            $data['date']       = 'All Data';
		}
// 		print_r($students);
        $details            = $this->Common->get_details('courses',array('course_id'=>$course_id))->row();
        $data['course']     = $details;
        $data['title']      = 'Report';
        $data['course_id']  = $course_id;
        $this->load->view('admin/reports/report',$data);
	}
	
	public function reportPrint()
	{
	    $this->load->library('pdf');
	    $start        = $this->input->post('from');
		$end          = $this->input->post('to');
		$course_id    = $this->input->post('course_id');
		$start_date   = date('Y-m-d',strtotime($start));
		$end_date     = date('Y-m-d',strtotime($end));
		if($start!=='' && $end!='')
		{
		    $subscribed_count   = $this->reports->getSubscribedCount($course_id,$start_date,$end_date);
    		$data['subscribed'] = $subscribed_count;
    		
    		$earnings           = $this->reports->getEarning($course_id,$start_date,$end_date);
    		$data['earnings']   = $earnings;
    		
    		$online_payment     = $this->reports->getOnlinePayments($course_id,$start_date,$end_date);
    		$data['online']     = $online_payment;
    		
    		$tamkeen_payment    = $this->reports->getTamkeenPayments($course_id,$start_date,$end_date);
    		$data['tamkeen']    = $tamkeen_payment;
    		
    		$cash_payment       = $this->reports->getCashPayments($course_id,$start_date,$end_date);
    		$data['cash']       = $cash_payment;
    		
    		$students           = $this->reports->getSubscribedStudents($course_id,$start_date,$end_date);
    		$data['students']   = $students;
    		
    		$data['from']       = $start;
            $data['to']         = $end;
            $data['date']       = date('d-M-Y',strtotime($start)).' To '.date('d-M-Y',strtotime($end));
		}
		else
		{
		    $subscribed_count   = $this->reports->getSubscribedCountAll($course_id);
    		$data['subscribed'] = $subscribed_count;
    		
    		$earnings           = $this->reports->getEarningAll($course_id);
    		$data['earnings']   = $earnings;
    		
    		$online_payment     = $this->reports->getOnlinePaymentsAll($course_id);
    		$data['online']     = $online_payment;
    		
    		$tamkeen_payment    = $this->reports->getTamkeenPaymentsAll($course_id);
    		$data['tamkeen']    = $tamkeen_payment;
    		
    		$cash_payment       = $this->reports->getCashPaymentsAll($course_id);
    		$data['cash']       = $cash_payment;
    		
    		$students           = $this->reports->getSubscribedStudentsAll($course_id);
    		$data['students']   = $students;
    		
    		$data['from']       = $start;
            $data['to']         = $end;
            $data['date']       = 'All Data';
		}
// 		print_r($students);
        $details            = $this->Common->get_details('courses',array('course_id'=>$course_id))->row();
        $data['course']     = $details;
        $data['title']      = 'Report';
        $data['course_id']  = $course_id;
        // $this->load->view('admin/reports/report',$data);
        $html            = $this->load->view('admin/reports/download-reports', $data, true);
		$filename        = 'report-OLMPC' . $course_id;
		$this->pdf->createPDF($html, $filename, false);
	}
}
?>
