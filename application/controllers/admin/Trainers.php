<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Trainers extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!admin()) {
            redirect('admin');
        }
		$this->load->model('admin/Model_trainers','trainer');
    }
    public function index() {
        $data['title'] = 'Olymbic | Trainers';
        $this->load->view('admin/trainers/view', $data);
    }
	public function add()
	{
		$data['title'] = 'Olymbic | Add trainers';
        $this->load->view('admin/trainers/add', $data);
	}
	public function edit($trainer_id)
	{
		$get = $this->trainer->get_details('trainers',array('trainer_id' => $trainer_id));
		if ($get->num_rows() > 0) {
			$data['title'] = 'Olymbic | Edit trainers';
			$data['trainer'] = $get->row();
			$this->load->view('admin/trainers/edit',$data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load trainer');
			redirect('admin/trainers');
		}
	}
	public function get()
	{
		$result = $this->trainer->make_datatables();
		$data = array();
		foreach ($result as $res) {
			$sub_array = array();
			if ($res->trainer_image != '') {
				$sub_array[] = '<img src="' . base_url() . $res->trainer_image . '" width="100px" height="100px">';
			}
			else {
				$sub_array[] = '<img src="' . base_url() . 'uploads/trainers/user.png" width="100px" height="100px">';
			}
			$sub_array[] = $res->trainer_name_english . '<br>' . $res->trainer_name_arabic;
			$sub_array[] = $res->trainer_mobile . '<br>' . $res->trainer_email;
			$sub_array[] = $res->trainer_gender;
			$sub_array[] = $res->trainer_education;
			$sub_array[] = $res->trainer_experience;
			$sub_array[] = $res->trainer_contact;
			if ($res->trainer_status) {
				$status = '<a class="btn btn-danger btn-xs" href="' . site_url('admin/trainers/status/'.$res->trainer_id . '/0') . '" onclick="return block()">Block</a><br>Active';
			}
			else {
				$status = '<a class="btn btn-success btn-xs" href="' . site_url('admin/trainers/status/'.$res->trainer_id . '/1') . '" onclick="return unblock()">Activate</a><br>Blocked';
			}
			$sub_array[] = $status;
			$sub_array[] = '<a class="btn btn-primary btn-xs" href="' . site_url('admin/trainers/edit/'.$res->trainer_id) . '">Edit</a>';
			$sub_array[] = '<a class="btn btn-primary btn-xs" onclick="changePassword('. $res->trainer_id .')">Change<br>password</s>';

			$data[] = $sub_array;
		}

		$output = array(
			"draw"   => intval($_POST['draw']),
			"recordsTotal" => $this->trainer->get_all_data(),
			"recordsFiltered" => $this->trainer->get_filtered_data(),
			"data" => $data
		);
		echo json_encode($output);
	}
	public function addTrainer()
	{
		$array = array(
			'trainer_name_english' => $this->security->xss_clean($this->input->post('trainer_name_english')),
			'trainer_name_arabic' => $this->security->xss_clean($this->input->post('trainer_name_arabic')),
			'arabic_first' => $this->security->xss_clean($this->input->post('first_arabic')),
			'arabic_second' => $this->security->xss_clean($this->input->post('second_arabic')),
			'trainer_gender' => $this->security->xss_clean($this->input->post('trainer_gender')),
			'trainer_cpr' => $this->security->xss_clean($this->input->post('trainer_cpr')),
			'trainer_mobile' => $this->security->xss_clean($this->input->post('trainer_mobile')),
			'trainer_email' => $this->security->xss_clean($this->input->post('trainer_email')),
			'trainer_education' => $this->security->xss_clean($this->input->post('trainer_education')),
			'trainer_experience' => $this->security->xss_clean($this->input->post('trainer_experience')),
			'education_arabic' => $this->security->xss_clean($this->input->post('trainer_education_arabic')),
			'experience_arabic' => $this->security->xss_clean($this->input->post('trainer_experience_arabic')),
			'trainer_contact' => $this->security->xss_clean($this->input->post('trainer_contact')),
			'trainer_password' => md5($this->security->xss_clean($this->input->post('trainer_password')))
		);
		$image = $this->input->post('image');
		if ($image != '')
		{
			$img = substr($image, strpos($image, ",") + 1);
			$url = FCPATH.'uploads/trainers/';
			$rand = $plan_name.date('Ymd').mt_rand(1001,9999);
			$userpath = $url.$rand.'.png';
			$path = "uploads/trainers/".$rand.'.png';
			file_put_contents($userpath,base64_decode($img));

            $array['trainer_image'] = $path;
		}
        else {
            $array['trainer_image'] = '';
        }

		if ($this->trainer->insert('trainers',$array)) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'New trainer added..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add trainer..!');
		}
		redirect('admin/trainers');
	}
	public function editTrainer()
	{
		$trainer_id = $this->security->xss_clean($this->input->post('trainer_id'));
		$get = $this->trainer->get_details('trainers',array('trainer_id' => $trainer_id));
		if ($get->num_rows() > 0) {
			$trainer = $get->row();
			$array = array(
				'trainer_name_english' => $this->security->xss_clean($this->input->post('trainer_name_english')),
				'trainer_name_arabic' => $this->security->xss_clean($this->input->post('trainer_name_arabic')),
				'trainer_gender' => $this->security->xss_clean($this->input->post('trainer_gender')),
				'trainer_cpr' => $this->security->xss_clean($this->input->post('trainer_cpr')),
				'trainer_mobile' => $this->security->xss_clean($this->input->post('trainer_mobile')),
				'trainer_email' => $this->security->xss_clean($this->input->post('trainer_email')),
				'trainer_education' => $this->security->xss_clean($this->input->post('trainer_education')),
				'trainer_experience' => $this->security->xss_clean($this->input->post('trainer_experience')),
				'trainer_contact' => $this->security->xss_clean($this->input->post('trainer_contact')),
			);

			$image = $this->input->post('image');
			if ($image != '')
			{
				$img = substr($image, strpos($image, ",") + 1);
				$url = FCPATH.'uploads/trainers/';
				$rand = $plan_name.date('Ymd').mt_rand(1001,9999);
				$userpath = $url.$rand.'.png';
				$path = "uploads/trainers/".$rand.'.png';
				file_put_contents($userpath,base64_decode($img));

	            $array['trainer_image'] = $path;
			}

			if ($this->trainer->update('trainer_id',$trainer_id,'trainers',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'trainer details updated..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to update trainer..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load trainer..!');
		}
		redirect('admin/trainers');
	}
	public function status($trainer_id,$status)
	{
		$array = array(
			'trainer_status' => $status
		);
		$this->trainer->update('trainer_id',$trainer_id,'trainers',$array);
		if ($status == '1') {
			$this->session->set_flashdata('alert_message', 'trainer activated..!');
		}
		else {
			$this->session->set_flashdata('alert_message', 'trainer deactivated..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('admin/trainers');
	}
	public function getDetails()
	{
		$trainer_id = $this->security->xss_clean($this->input->post('trainer_id'));
		$trainer = $this->trainer->get_details('trainers',array('trainer_id' => $trainer_id))->row();
		print_r(json_encode($trainer));
	}
	public function change()
	{
		$trainer_id = $this->security->xss_clean($this->input->post('trainer_id'));
		$password = $this->security->xss_clean($this->input->post('password'));
		$array = array(
			'trainer_password' => md5($password)
		);
		if ($this->trainer->update('trainer_id',$trainer_id,'trainers',$array)) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'trainer password has been successfully reset');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to update trainer password');
		}
		redirect('admin/trainers');
	}
	public function addValidation()
	{
		$mobile = $this->security->xss_clean($this->input->post('mobile'));
		$email = $this->security->xss_clean($this->input->post('email'));
		$mCheck = $this->trainer->get_details('trainers',array('trainer_mobile' => $mobile))->num_rows();
		if ($mCheck > 0) {
			$mob = false;
		}
		else {
			$mob = true;
		}
		if ($email != '') {
			$eCheck = $this->trainer->get_details('trainers',array('trainer_email' => $email))->num_rows();
			if ($eCheck > 0) {
				$ema = false;
			}
			else {
				$ema = true;
			}
		}
		else {
			$ema = true;
		}
		if ($ema && $mob) {
			$message = '';
			$status = true;
		}
		else {
			$status = false;
			if(!$ema && !$mob)
			{
				$message = 'Mobile number and Email address already registered';
			}
			else {
				if (!$ema) {
					$message = 'Email address already registered';
				}
				if (!$mob) {
					$message = 'Mobile number already registered';
				}
			}
		}
		$return = array(
			'status' => $status,
			'message' => $message
		);
		print_r(json_encode($return));
	}
	public function editValidation()
	{
		$trainer_id = $this->security->xss_clean($this->input->post('trainer_id'));
		$mobile = $this->security->xss_clean($this->input->post('mobile'));
		$email = $this->security->xss_clean($this->input->post('email'));
		$mCheck = $this->trainer->get_details('trainers',array('trainer_mobile' => $mobile, 'trainer_id !=' => $trainer_id))->num_rows();
		if ($mCheck > 0) {
			$mob = false;
		}
		else {
			$mob = true;
		}
		if ($email != '') {
			$eCheck = $this->trainer->get_details('trainers',array('trainer_email' => $email,'trainer_id !=' => $trainer_id))->num_rows();
			if ($eCheck > 0) {
				$ema = false;
			}
			else {
				$ema = true;
			}
		}
		else {
			$ema = true;
		}
		if ($ema && $mob) {
			$message = '';
			$status = true;
		}
		else {
			$status = false;
			if(!$ema && !$mob)
			{
				$message = 'Mobile number and Email address already registered';
			}
			else {
				if (!$ema) {
					$message = 'Email address already registered';
				}
				if (!$mob) {
					$message = 'Mobile number already registered';
				}
			}
		}
		$return = array(
			'status' => $status,
			'message' => $message
		);
		print_r(json_encode($return));
	}
}
?>
