<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('Common');
			if (!admin()) {
				redirect('admin');
			}
	}
	public function index()
	{
		$data['title'] = 'Settings | Olympic academy';
		$data['admin'] = $this->Common->get_details('admin',array('admin_id' => '1'))->row();
		$setting = $this->Common->get_details('settings',array('id' => '1'));
		if ($setting->num_rows() > 0) {
			$data['setting'] = $setting->row();
		}
		else {
			$data['setting'] = false;
		}
		$this->load->view('admin/settings/view',$data);
	}
	public function updatePassword()
	{
		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));
		$array = array(
			'username' => $username,
			'password' => md5($password)
		);
		if ($this->Common->update('admin_id','1','admin',$array)) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Admin credentials updated..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to update admin credentials..!');
		}
		redirect('admin/settings');
	}
	public function updateSettings()
	{
		$array = array(
			'address' => $this->security->xss_clean($this->input->post('address')),
			'address_arabic' => $this->security->xss_clean($this->input->post('address_arabic')),
			'email' => $this->security->xss_clean($this->input->post('email')),
			'mobile' => $this->security->xss_clean($this->input->post('mobile')),
		);
		if ($this->Common->update('id','1','settings',$array)) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Contact info updated..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to update contact info..!');
		}
		redirect('admin/settings');
	}
}
?>
