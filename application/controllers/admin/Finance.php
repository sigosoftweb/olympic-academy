<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Finance extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!admin()) {
            redirect('admin');
        }
		$this->load->model('admin/Model_finance','finance');
    }
    public function index() {
        $data['title'] = 'Olymbic | Students';
        $this->load->view('admin/finance/view', $data);
    }
	public function get()
	{
		$result = $this->finance->make_datatables();
		$data = array();
		foreach ($result as $res) {
			$sub_array = array();

			$sub_array[] = $res->stu_name_english . '<br>' . $res->stu_name_arabic;
			$sub_array[] = $res->stu_mobile . '<br>' . $res->stu_email;
			$sub_array[] = $res->course_title;
			$sub_array[] = date('d/m/Y',strtotime($res->date));

			$sub_array[] = $res->amount;
			$sub_array[] = $res->payment_id;
			$sub_array[] = $res->payment_type;
			$data[] = $sub_array;
		}

		$output = array(
			"draw"   => intval($_POST['draw']),
			"recordsTotal" => $this->finance->get_all_data(),
			"recordsFiltered" => $this->finance->get_filtered_data(),
			"data" => $data
		);
		echo json_encode($output);
	}
	public function filter()
	{
		$data['title'] = 'filter transactions';
		if (isset($_POST['from'])) {
			$data['param'] = true;
			$from_time = $this->security->xss_clean($this->input->post('from'));
			$to_time = $this->security->xss_clean($this->input->post('to'));
			$payment_type = $this->security->xss_clean($this->input->post('payment_type'));
			$from = $from_time . ' 00:00:00';
			$to = $to_time . ' 23:59:59';
			$data['transactions'] = $this->finance->getTransactions($from,$to,$payment_type);
		}
		else {
			$data['param'] = false;
			$from_time = false;
			$to_time = false;
			$payment_type = 'all';
			$data['transactions'] = array();
		}
		$data['from'] = $from_time;
		$data['to'] = $to_time;
		$data['payment_type'] = $payment_type;
		$this->load->view('admin/finance/filter',$data);
	}
	public function export_date_filter($from_time,$to_time,$payment)
	{
	    $from = $from_time . ' 00:00:00';
		$to = $to_time . ' 23:59:59';
	    $data['transactions'] = $this->finance->getTransactions($from,$to,$payment);
	    $data['from'] = $from_time;
	    $data['to'] = $to_time;
	    $data['payment_type'] = $payment;
	    
	    $mpdf = new \Mpdf\Mpdf();
		$pdfFilePath = "Report.pdf";
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->shrink_tables_to_fit = 1;
		$html = $this->load->view('admin/finance/export_date_filter',$data,true);
		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath,'D');
	}
	public function course()
	{
		$data['title'] = 'Course filter';
		$data['courses'] = $this->finance->getAllCourses();
		if (isset($_POST['course_id'])) {
			$data['param'] = true;
			$course_id = $this->security->xss_clean($this->input->post('course_id'));
			$from_time = $this->security->xss_clean($this->input->post('from'));
			$to_time = $this->security->xss_clean($this->input->post('to'));
			$payment_type = $this->security->xss_clean($this->input->post('payment_type'));

			if ($from_time != '') {
				$from = $from_time . ' 00:00:00';
			}
			else {
				$from = false;
			}

			if ($to_time != '') {
				$to = $to_time . ' 23:59:59';
			}
			else {
				$to = false;
			}

			$data['transactions'] = $this->finance->getCourseFilter($course_id,$from,$to,$payment_type);
		}
		else {
			$data['param'] = false;
			$from_time = false;
			$to_time = false;
			$data['transactions'] = array();
			$payment_type = 'all';
			$course_id = '0';
		}
		$data['from'] = $from_time;
		$data['to'] = $to_time;
		$data['payment_type'] = $payment_type;
		$data['course_id'] = $course_id;
		
		$this->load->view('admin/finance/course_filter',$data);
	}
	public function export_course_filter($course_id,$from_time,$to_time,$payment)
	{
	    if ($from_time != '0') {
			$from = $from_time . ' 00:00:00';
		}
		else {
			$from = false;
		}

		if ($to_time != '0') {
			$to = $to_time . ' 23:59:59';
		}
		else {
			$to = false;
		}
		
	    $data['transactions'] = $this->finance->getCourseFilter($course_id,$from,$to,$payment);
	    $data['from'] = $from_time;
	    $data['to'] = $to_time;
	    $data['payment_type'] = $payment;
	    $data['course'] = $this->finance->getCourse($course_id);
	    
	    $mpdf = new \Mpdf\Mpdf();
		$pdfFilePath = "Report.pdf";
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->shrink_tables_to_fit = 1;
		$html = $this->load->view('admin/finance/export_course_filter',$data,true);
		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath,'D');
	}
}
?>
