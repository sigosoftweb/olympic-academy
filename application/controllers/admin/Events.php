<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Events extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('admin/Model_events','events');
			if (!admin()) {
				redirect('admin');
			}
	}
	public function index()
	{
		$this->upcoming();
	}
	public function upcoming()
	{
	    $data['title'] = 'Events';
		$data['events'] = $this->events->getUpcomingEvents();
		$this->load->view('admin/events/upcoming',$data);
	}
	public function completed()
	{
	    $data['title'] = 'Events';
		$data['events'] = $this->events->getCompletedEvents();
		$this->load->view('admin/events/completed',$data);
	}
	public function add()
	{
		$data['title'] = 'Add Event';
		$this->load->view('admin/events/add');
	}
	public function addEvent()
	{
		$title        = $this->security->xss_clean($this->input->post('title'));
		$description  = $this->security->xss_clean($this->input->post('description'));
		$title_arabic = $this->security->xss_clean($this->input->post('title_arabic'));
		$description_arabic = $this->security->xss_clean($this->input->post('description_arabic'));
		$start_time  = $this->security->xss_clean($this->input->post('start_time'));
		$end_time    = $this->security->xss_clean($this->input->post('end_time'));

		$start_time = str_replace("/","-",$start_time);
		$end_time   = str_replace("/","-",$end_time);
		$start      = date('Y-m-d H:i:s',strtotime($start_time));
		$end        = date('Y-m-d H:i:s',strtotime($end_time));

		$image       = $this->input->post('image');
		$img         = substr($image, strpos($image, ",") + 1);

		$url      = FCPATH.'uploads/events/';
		$rand     = $title.date('Ymd').mt_rand(1001,9999);
		$userpath = $url.$rand.'.png';
		$path     = "uploads/events/".$rand.'.png';
		if(file_put_contents($userpath,base64_decode($img)))
		{
			$array = [
				'title' => $title,
				'description' => $description,
				'title_arabic' => $title_arabic,
				'description_arabic' => $description_arabic,
				'start_time' => $start,
				'end_time' => $end,
				'image' => $path
			];

			if ($this->events->insert('events',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'New event added..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to add events..!');
			}

			$now = date('Y-m-d H:i:s');
			if( strtotime($end) > strtotime($now) )
			{
				redirect('admin/events/upcoming');
			}
			else {
				redirect('admin/events/completed');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to upload image..!');
		}
		redirect('admin/events');
	}
	public function editEvent()
	{
		$event_id = $this->security->xss_clean($this->input->post('event_id'));
		$get = $this->events->get_details('events',array('event_id' => $event_id));
		if ($get->num_rows() > 0) {
			$event = $get->row();
			$title = $this->security->xss_clean($this->input->post('title'));
			$description = $this->security->xss_clean($this->input->post('description'));
			$title_arabic = $this->security->xss_clean($this->input->post('title_arabic'));
			$description_arabic = $this->security->xss_clean($this->input->post('description_arabic'));
			$start_time = $this->security->xss_clean($this->input->post('start_time'));
			$end_time = $this->security->xss_clean($this->input->post('end_time'));

			$start_time = str_replace("/","-",$start_time);
			$end_time = str_replace("/","-",$end_time);
			$start = date('Y-m-d H:i:s',strtotime($start_time));
			$end = date('Y-m-d H:i:s',strtotime($end_time));
            
            $image       = $this->input->post('image');
            if($image!='')
            {
				$img      = substr($image, strpos($image, ",") + 1);

				$url      = FCPATH.'uploads/events/';
				$rand     = $title.date('Ymd').mt_rand(1001,9999);
				$userpath = $url.$rand.'.png';
				$path     = "uploads/events/".$rand.'.png';
				file_put_contents($userpath,base64_decode($img));
                
                if ($event->image != '') {
						$unlink = FCPATH . $event->image;
						unlink($unlink);
					}

				$array = [
							'title' => $title,
							'description' => $description,
							'title_arabic' => $title_arabic,
							'description_arabic' => $description_arabic,
							'start_time' => $start,
							'end_time' => $end,
							'image'    => $path
						];
            }
			else
			{
				$array = [
							'title' => $title,
							'description' => $description,
							'title_arabic' => $title_arabic,
							'description_arabic' => $description_arabic,
							'start_time' => $start,
							'end_time' => $end,
						];
			}
           
			$this->events->update('event_id',$event_id,'events',$array);

			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'New event added..!');

			$now = date('Y-m-d H:i:s');
			if( strtotime($end) > strtotime($now) )
			{
				redirect('admin/events/upcoming');
			}
			else {
				redirect('admin/events/completed');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load event..!');
			redirect('admin/events');
		}
	}

	public function edit($events_id)
	{
		$data['title'] = 'Edit Event';
		$event = $this->events->get_details('events',array('event_id' => $events_id));
		if ($event->num_rows() > 0) {
			$data['event'] = $event->row();
			$this->load->view('admin/events/edit',$data);
		}
		else {
			redirect('admin/events');
		}
	}

	public function delete($events_id)
	{
		$get = $this->events->get_details('events',array('event_id' => $events_id));
		if ($get->num_rows() > 0) {
			$event = $get->row();
			if($this->events->delete('events',array('event_id' => $events_id)))
			{
				$remove_path = FCPATH . $event->image;
				unlink($remove_path);

				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Event deleted successfully..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to remove Event..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load Event..!');
		}
		redirect('admin/events');
	}
	public function status($type,$event_id,$status)
	{
		$array = array(
			'status' => $status
		);
		$this->events->update('event_id',$event_id,'events',$array);

		if ($status == '1') {
			$message = 'Event activated';
		}
		else {
			$message = 'Event blocked';
		}

		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		$this->session->set_flashdata('alert_message', $message);
		if ($type == 'c') {
			redirect('admin/events/completed');
		}
		else {
			redirect('admin/events/upcoming');
		}
	}
}
?>
