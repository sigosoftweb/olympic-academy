<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Subscriptions extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!admin()) {
            redirect('admin');
        }
		$this->load->model('admin/Model_subscriptions','sub');
    }
    public function index() {
		$this->approved();
    }
	public function approved()
	{
		$data['title'] = 'Olymbic | Subscriptions';
        $this->load->view('admin/subscriptions/approved', $data);
	}
	public function pending()
	{
		$data['title'] = 'Olymbic | Subscriptions';
        $this->load->view('admin/subscriptions/pending', $data);
	}
	public function rejected()
	{
		$data['title'] = 'Olymbic | Subscriptions';
        $this->load->view('admin/subscriptions/rejected', $data);
	}
	public function getApproved()
	{
		$result = $this->sub->make_datatables('Approved');
		$data = array();
		foreach ($result as $res) {
			$sub_array = array();

			$sub_array[] = $res->stu_name_english . '<br>' . $res->stu_name_arabic;
			$sub_array[] = $res->stu_mobile . '<br>' . $res->stu_email;
			$sub_array[] = $res->course_title;
			$sub_array[] = date('d/m/Y',strtotime($res->date));

			$sub_array[] = $res->amount;

			$data[] = $sub_array;
		}

		$output = array(
			"draw"   => intval($_POST['draw']),
			"recordsTotal" => $this->sub->get_all_data('Approved'),
			"recordsFiltered" => $this->sub->get_filtered_data('Approved'),
			"data" => $data
		);
		echo json_encode($output);
	}
	public function getPending()
	{
		$result = $this->sub->make_datatables('Waiting');
		$data = array();
		foreach ($result as $res) {
			$sub_array = array();

			$sub_array[] = $res->stu_name_english . '<br>' . $res->stu_name_arabic;
			$sub_array[] = $res->stu_mobile . '<br>' . $res->stu_email;
			$sub_array[] = $res->course_title;
			$sub_array[] = date('d/m/Y',strtotime($res->date));

			$sub_array[] = $res->amount;
			$sub_array[] = '<a class="btn btn-success btn-xs" onclick="return confirmAction(0)" href="' . site_url('admin/subscriptions/approve/'.$res->sp_id) . '" style="margin-bottom:5px;">Approve</a><br>
			<a class="btn btn-danger btn-xs" onclick="reject(' . $res->sp_id . ')" href="#">Reject</a>';
			$data[] = $sub_array;
		}

		$output = array(
			"draw"   => intval($_POST['draw']),
			"recordsTotal" => $this->sub->get_all_data('Waiting'),
			"recordsFiltered" => $this->sub->get_filtered_data('Waiting'),
			"data" => $data
		);
		echo json_encode($output);
	}
	public function getRejected()
	{
		$result = $this->sub->make_datatables('Rejected');
		$data = array();
		foreach ($result as $res) {
			$sub_array = array();

			$sub_array[] = $res->stu_name_english . '<br>' . $res->stu_name_arabic;
			$sub_array[] = $res->stu_mobile . '<br>' . $res->stu_email;
			$sub_array[] = $res->course_title;
			$sub_array[] = date('d/m/Y',strtotime($res->date));

			$sub_array[] = $res->amount;
			$sub_array[] = $res->reject_reason;
			$sub_array[] = '<a class="btn btn-success btn-xs" onclick="return confirmAction(0)" href="' . site_url('admin/subscriptions/approve/'.$res->sp_id) . '">Approve</a>';
			$data[] = $sub_array;
		}

		$output = array(
			"draw"   => intval($_POST['draw']),
			"recordsTotal" => $this->sub->get_all_data('Rejected'),
			"recordsFiltered" => $this->sub->get_filtered_data('Rejected'),
			"data" => $data
		);
		echo json_encode($output);
	}
	public function approve($sp_id)
	{
		$get = $this->sub->get_details('students_packages',array('sp_id' => $sp_id));
		if ($get->num_rows() > 0) {
			$array = array(
				'sp_status' => 'Approved'
			);
			$this->sub->update('sp_id',$sp_id,'students_packages',$array);

			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Subscription accepted');
			redirect('admin/subscriptions/pending');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to accept subscription');
			redirect('admin/subscriptions/pending');
		}
	}
	public function reject()
	{
		$sp_id = $this->security->xss_clean($this->input->post('sp_id'));
		$reason = $this->security->xss_clean($this->input->post('reason'));
		$get = $this->sub->get_details('students_packages',array('sp_id' => $sp_id));
		if ($get->num_rows() > 0) {
			$array = array(
				'sp_status' => 'Rejected',
				'reject_reason' => $reason
			);
			$this->sub->update('sp_id',$sp_id,'students_packages',$array);

			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Subscription rejected');
			redirect('admin/subscriptions/pending');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to reject subscription');
			redirect('admin/subscriptions/pending');
		}
	}
}
?>
