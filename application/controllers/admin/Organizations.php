<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Organizations extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!admin()) {
            redirect('admin');
        }
		$this->load->model('admin/Model_organizations','organization');
    }
    public function index() {
        $data['title'] = 'Olymbic | Organizations';
		$data['organizations'] = $this->organization->getAllorganizations();
        $this->load->view('admin/organizations/view', $data);
    }
	public function add()
	{
		$organization = $this->security->xss_clean($this->input->post('organization'));
		if ($this->organization->insert('organizations',array('organization' => $organization))) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'New organization added..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add organization..!');
		}
		redirect('admin/organizations');
	}
	public function edit()
	{
		$org_id = $this->security->xss_clean($this->input->post('org_id'));
		$organization = $this->security->xss_clean($this->input->post('organization'));
		if ($this->organization->update('org_id',$org_id,'organizations',array('organization' => $organization))) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Changes made Successfully..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add organization..!');
		}
		redirect('admin/organizations');
	}
	public function status($org_id,$status)
	{
		$array = array(
			'status' => $status
		);
		$this->organization->update('org_id',$org_id,'organizations',$array);
		if ($status == '1') {
			$this->session->set_flashdata('alert_message', 'organization activated..!');
		}
		else {
			$this->session->set_flashdata('alert_message', 'organization deactivated..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('admin/organizations');
	}
	public function getDetails()
	{
		$org_id = $this->security->xss_clean($this->input->post('org_id'));
		$organization = $this->organization->get_details('organizations',array('org_id' => $org_id))->row();
		print_r(json_encode($organization));
	}
}
?>
