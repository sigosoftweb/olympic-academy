<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sliders extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('admin/Model_sliders','slider');
			if (!admin()) {
				redirect('admin');
			}
	}
	public function index()
	{
	    $data['title'] = 'Olympic | Sliders';
		$data['sliders'] = $this->slider->get_details('sliders',array())->result();
		$this->load->view('admin/sliders/view',$data);
	}
	public function add()
	{
	    $data['title'] = 'Olympic | Add Slider';
		$this->load->view('admin/sliders/add',$data);
	}
	public function addSlider()
	{
		$slider = $this->security->xss_clean($this->input->post('name'));

		$image = $this->input->post('image');
		if ($image != '')
		{
			$img = substr($image, strpos($image, ",") + 1);
			$url = FCPATH.'uploads/trainers/';
			$rand = $plan_name.date('Ymd').mt_rand(1001,9999);
			$userpath = $url.$rand.'.png';
			$path = "uploads/trainers/".$rand.'.png';
			file_put_contents($userpath,base64_decode($img));

			$array = [
    			'name' => $slider,
    			'image' => $path
    		];
    		if ($this->slider->insert('sliders',$array)) {
    			$this->session->set_flashdata('alert_type', 'success');
    			$this->session->set_flashdata('alert_title', 'Success');
    			$this->session->set_flashdata('alert_message', 'New slider added..!');

    			redirect('admin/sliders');
    		}
    		else {
    			$this->session->set_flashdata('alert_type', 'error');
    			$this->session->set_flashdata('alert_title', 'Failed');
    			$this->session->set_flashdata('alert_message', 'Failed to add slider..!');

    			redirect('admin/sliders/add');
    		}
		}
        else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to upload image..!');

			redirect('admin/sliders');
        }
	}

	public function delete($slider_id)
	{
		$check = $this->slider->get_details('sliders',array('slider_id' => $slider_id));
		if ($check->num_rows() > 0) {
			$slider = $check->row();
			if ($this->slider->delete('sliders',array('slider_id' => $slider_id))) {
				$remove_path = FCPATH . $slider->image;
				unlink($remove_path);

				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'slider deleted successfully..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to remove slider..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to remove slider..!');
		}
		redirect('admin/sliders');
	}

	public function status($status,$slider_id)
	{
		$array = array(
			'status' => $status
		);
		$this->slider->update('slider_id',$slider_id,'sliders',$array);
		if ($status == '1') {
			$message = 'Slider activated';
		}
		else {
			$message = 'Slider deactivated';
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		$this->session->set_flashdata('alert_message', $message);
		redirect('admin/sliders');
	}
}
?>
