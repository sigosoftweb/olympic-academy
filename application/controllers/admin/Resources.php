<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resources extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('admin/Model_resources','resources');
			if (!admin()) {
				redirect('admin');
			}
	}
	public function index()
	{
		$data['title'] = 'Olympic | Resources';
		$data['resources'] = $this->resources->getResources();
		$this->load->view('admin/resources/view',$data);
	}

	public function addResource()
	{
		$array = array(
			'cr_title' => $this->security->xss_clean($this->input->post('title')),
			'cr_title_arabic' => $this->security->xss_clean($this->input->post('title_arabic')),
			'cr_type' => $this->security->xss_clean($this->input->post('type')),
		);
		$file = $_FILES['file'];
		if ($file['size'] > 0) {
			$tar = "uploads/resources/";
			$rand = date('Ymd').mt_rand(1001,9999);
			$tar_file = $tar . $rand . basename($file['name']);
			if(move_uploaded_file($file["tmp_name"], $tar_file))
			{
				$array['cr_attachment'] = $tar_file;
				if ($this->resources->insert('course_resources',$array)) {
					$this->session->set_flashdata('alert_type', 'success');
					$this->session->set_flashdata('alert_title', 'Success');
					$this->session->set_flashdata('alert_message', 'New resource added..!');
				}
				else {
					$this->session->set_flashdata('alert_type', 'error');
					$this->session->set_flashdata('alert_title', 'Failed');
					$this->session->set_flashdata('alert_message', 'Failed to add resource..!');
				}
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to upload resource..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Please choose a document');
		}
		redirect('admin/resources');
	}
	public function statusResource($cr_id,$status)
	{
		$get = $this->resources->get_details('course_resources',array('cr_id' => $cr_id));
		if ($get->num_rows() > 0) {
			$array = array(
				'cr_status' => $status
			);
			$this->resources->update('cr_id',$cr_id,'course_resources',$array);
			if ($status == '1') {
				$this->session->set_flashdata('alert_message', 'Resource activated');
			}
			else {
				$this->session->set_flashdata('alert_message', 'Resource deactivated');
			}
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load course..!');
		}
		redirect('admin/resources');
	}
	public function deleteResource($cr_id)
	{
		$get = $this->resources->get_details('course_resources',array('cr_id' => $cr_id));
		if ($get->num_rows() > 0) {
			$resource = $get->row();
			if ($this->resources->delete('course_resources',array('cr_id' => $cr_id))) {
				$unlink = FCPATH . $resource->cr_attachment;
				unlink($unlink);

				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Resource deleted..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to delete resource..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load resource..!');
		}
		redirect('admin/resources');
	}
}
?>
