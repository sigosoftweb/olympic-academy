<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Exams extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!admin()) {
            redirect('admin');
        }
        $this->load->model('admin/Model_exams', 'exam');
    }
    public function add($course_id) {
        $data['title'] = 'Add exam';
        $get = $this->exam->get_details('courses', array('course_id' => $course_id));
        if ($get->num_rows() > 0) {
            $data['course'] = $get->row();
            $data['sections'] = $this->exam->getSections($course_id);
            $data['batches'] = $this->exam->getBatches($course_id);
            $this->load->view('admin/exams/add', $data);
        } else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Error');
            $this->session->set_flashdata('alert_message', 'Failed to load course..!');
            redirect('admin/courses');
        }
    }
    public function edit($exam_id)
    {
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
            $date = date('Y-m-d');
            $data['exam'] = $exam = $get->row();
            $data['course'] = $this->exam->get_details('courses', array('course_id' => $exam->course_id))->row();
            $data['sections'] = $this->exam->getSections($exam->course_id);
            $data['batches'] = $this->exam->getBatches($exam->course_id);
            $data['batch_name'] = '';
            $data['section_name'] = '';
            $data['question_number'] = $this->exam->get_details('questions',array('exam_id'=>$exam_id))->num_rows();
            $check = $this->exam->get_details('students_question_answers',array('exam_id' => $exam_id))->num_rows();
            if ($check > 0) {
                $data['status'] = false;
            }
            else {
                $data['status'] = true;
            }
            $this->load->view('admin/exams/edit',$data);
        }
        else {
            redirect('admin/exams');
        }
    }
    public function duplicate($course_id) {
        $data['title'] = 'Duplicate exam';
        $get = $this->exam->get_details('courses', array('course_id' => $course_id));
        if ($get->num_rows() > 0) {
            $data['course'] = $get->row();
            $data['sections'] = $this->exam->getSections($course_id);
            $data['batches'] = $this->exam->getBatches($course_id);
			$data['exams'] = $this->exam->getAllExams();
            $this->load->view('admin/exams/duplicate', $data);
        } else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Error');
            $this->session->set_flashdata('alert_message', 'Failed to load course..!');
            redirect('admin/courses');
        }
    }
    public function addExam() {
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
        $name = $this->security->xss_clean($this->input->post('name'));
		$name_arabic = $this->security->xss_clean($this->input->post('name_arabic'));
        $cb_id = $this->security->xss_clean($this->input->post('cb_id'));
		$cs_id = $this->security->xss_clean($this->input->post('cs_id'));
        $no_of_questions = $this->security->xss_clean($this->input->post('no_of_questions'));
        $exam_time = $this->security->xss_clean($this->input->post('exam_time'));
        $time = $this->security->xss_clean($this->input->post('time'));
        $instructions = $this->security->xss_clean($this->input->post('instructions'));
		$instructions_arabic = $this->security->xss_clean($this->input->post('instructions_arabic'));
        $show_result = $this->security->xss_clean($this->input->post('show_result'));
		if ($cb_id == '0' && $cs_id == '0') {
			$type = 'course';
		}
		else {
			if ($cb_id != '0') {
				$type = 'batch';
			}
			else {
				$type = 'section';
			}
		}
        $array = ['exam_name' => $name, 'exam_name_arabic' => $name_arabic, 'type' => $type, 'course_id' => $course_id, 'cb_id' => $cb_id, 'cs_id' => $cs_id, 'no_questions' => $no_of_questions, 'exam_time' => $exam_time, 'time_status' => $time, 'show_result' => $show_result ];
        if ($time == '1') {
            $from = $this->security->xss_clean($this->input->post('from_time'));
			$from = str_replace("/","-",$from);
			$from = date('Y-m-d H:i:s',strtotime($from));
            $end = date('Y-m-d H:i:s', strtotime($from . ' +' . $exam_time . ' minutes'));
            $array['from_time'] = $from;
            $array['to_time'] = $end;
        }
        if ($exam_id = $this->exam->insert('exams', $array)) {
			$k = 0;
            foreach ($instructions as $instruction) {
                $instruction_array = ['exam_id' => $exam_id, 'instruction' => $instruction, 'instruction_arabic' => $instructions_arabic[$k], 'timestamp' => date('Y-m-d H:i:s') ];
                $this->exam->insert('exam_instructions', $instruction_array);
				$k++;
            }
            $this->session->set_flashdata('alert_type', 'success');
            $this->session->set_flashdata('alert_title', 'Success');
            $this->session->set_flashdata('alert_message', 'Exam added');
            redirect('admin/exams/questions/' . $exam_id);
        } else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to add exam..!');
            redirect('admin/exams');
        }
    }
    public function editExam() {
		$exam_id = $this->security->xss_clean($this->input->post('exam_id'));
		$question_number  = $this->security->xss_clean($this->input->post('question_number'));

        $name = $this->security->xss_clean($this->input->post('name'));
        $cb_id = $this->security->xss_clean($this->input->post('cb_id'));
		$cs_id = $this->security->xss_clean($this->input->post('cs_id'));
        $no_of_questions = $this->security->xss_clean($this->input->post('no_of_questions'));
        $exam_time = $this->security->xss_clean($this->input->post('exam_time'));
        $time = $this->security->xss_clean($this->input->post('time'));
        $instructions = $this->security->xss_clean($this->input->post('instructions'));
        $show_result = $this->security->xss_clean($this->input->post('show_result'));
		if ($cb_id == '0' && $cs_id == '0') {
			$type = 'course';
		}
		else {
			if ($cb_id != '0') {
				$type = 'batch';
			}
			else {
				$type = 'section';
			}
		}
        $array = ['exam_name' => $name, 'type' => $type, 'cb_id' => $cb_id, 'cs_id' => $cs_id, 'no_questions' => $no_of_questions, 'exam_time' => $exam_time, 'time_status' => $time, 'show_result' => $show_result ];
        if ($time == '1') {
            $from = $this->security->xss_clean($this->input->post('from_time'));
			$from = str_replace("/","-",$from);
			$from = date('Y-m-d H:i:s',strtotime($from));
            $end = date('Y-m-d H:i:s', strtotime($from . ' +' . $exam_time . ' minutes'));
            $array['from_time'] = $from;
            $array['to_time'] = $end;
        }
        else{
            $array['time_status'] = '0';
            $array['from_time'] = '';
            $array['to_time'] = '';
        }
        if ($question_number < $no_of_questions) {
            $array['registration_status'] = '0';
            $array['exam_status'] = '0';
        }
        if ($this->exam->update('exam_id',$exam_id,'exams', $array)) {
            $this->session->set_flashdata('alert_type', 'success');
            $this->session->set_flashdata('alert_title', 'Success');
            $this->session->set_flashdata('alert_message', 'Exam added');
            redirect('admin/exams/questions/' . $exam_id);
        } else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to add exam..!');
            redirect('admin/courses');
        }
    }
	public function duplicateExam() {
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$dup_id = $this->security->xss_clean($this->input->post('exam_id'));
		$get = $this->exam->get_details('exams',array('exam_id' => $dup_id));
		if ($get->num_rows() > 0) {
			$exam = $get->row();
			$name = $this->security->xss_clean($this->input->post('name'));
			$name_arabic = $this->security->xss_clean($this->input->post('name_arabic'));
	        $cb_id = $this->security->xss_clean($this->input->post('cb_id'));
			$cs_id = $this->security->xss_clean($this->input->post('cs_id'));
	        $exam_time = $this->security->xss_clean($this->input->post('exam_time'));
	        $time = $this->security->xss_clean($this->input->post('time'));
	        $instructions = $this->security->xss_clean($this->input->post('instructions'));
	        $instructions_arabic = $this->security->xss_clean($this->input->post('instructions_arabic'));
			$no_of_questions = $exam->no_questions;
			if ($cb_id == '0' && $cs_id == '0') {
				$type = 'course';
			}
			else {
				if ($cb_id != '0') {
					$type = 'batch';
				}
				else {
					$type = 'section';
				}
			}
	        $array = ['exam_name' => $name, 'exam_name_arabic' => $name_arabic, 'type' => $type, 'course_id' => $course_id, 'cb_id' => $cb_id, 'cs_id' => $cs_id, 'no_questions' => $no_of_questions, 'exam_time' => $exam_time, 'time_status' => $time, 'exam_status' => '1', 'registration_status' => '1' ];
	        if ($time == '1') {
	            $from = $this->security->xss_clean($this->input->post('from_time'));
				$from = str_replace("/","-",$from);
				$from = date('Y-m-d H:i:s',strtotime($from));
	            $end = date('Y-m-d H:i:s', strtotime($from . ' +' . $exam_time . ' minutes'));
	            $array['from_time'] = $from;
	            $array['to_time'] = $end;
	        }
	        if ($exam_id = $this->exam->insert('exams', $array)) {
	            $k = 0;
	            foreach ($instructions as $instruction) {
	                $instruction_array = ['exam_id' => $exam_id, 'instruction' => $instruction, 'instruction_arabic' => $instructions_arabic[$k], 'timestamp' => date('Y-m-d H:i:s') ];
	                $this->exam->insert('exam_instructions', $instruction_array);
	                $k++;
	            }
				$this->exam->duplicate($exam_id,$dup_id);

	            $this->session->set_flashdata('alert_type', 'success');
	            $this->session->set_flashdata('alert_title', 'Success');
	            $this->session->set_flashdata('alert_message', 'Exam added');
	            redirect('admin/courses/details/7/' . $course_id);
	        } else {
	            $this->session->set_flashdata('alert_type', 'error');
	            $this->session->set_flashdata('alert_title', 'Failed');
	            $this->session->set_flashdata('alert_message', 'Failed to add exam..!');
	            redirect('admin/courses');
	        }
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load exam..!');
			redirect('admin/courses');
		}
    }
	public function questions($exam_id)
    {
        $data['title'] = 'Add question';
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
            $data['questions'] = $question = $this->exam->get_details('questions',array('exam_id' => $exam_id))->num_rows();
            $data['exam'] = $exam = $get->row();
            if ($question == $exam->no_questions) {
                $array = [
                    'registration_status' => '1',
                    'exam_status' => '1'
                ];
                $this->exam->update('exam_id',$exam_id,'exams',$array);
                redirect('admin/courses/details/7/' . $exam->course_id);
            }
            else {
                $this->load->view('admin/exams/questions',$data);
            }
        }
        else {
            redirect('admin/exams');
        }
    }
	public function addQ($param=0)
    {
        $exam_id  = $this->security->xss_clean($this->input->post('exam_id'));

		$formula_question  = $this->input->post('formula_question');
        $formula_a  = $this->input->post('formula_a');
        $formula_b  = $this->input->post('formula_b');
        $formula_c  = $this->input->post('formula_c');
        $formula_d  = $this->input->post('formula_d');
		$formula_e  = $this->input->post('formula_e');

		$formula_question_arabic  = $this->input->post('formula_question_arabic');
        $formula_a_arabic         = $this->input->post('formula_a_arabic');
        $formula_b_arabic         = $this->input->post('formula_b_arabic');
        $formula_c_arabic         = $this->input->post('formula_c_arabic');
        $formula_d_arabic         = $this->input->post('formula_d_arabic');
		$formula_e_arabic         = $this->input->post('formula_e_arabic');

		$correct  = $this->input->post('correct');
		$negative  = $this->input->post('negative');

        $answer  = $this->security->xss_clean($this->input->post('answer'));

        $array = [
			'formula_question' => $formula_question,
            'formula_a' => $formula_a,
            'formula_b' => $formula_b,
            'formula_c' => $formula_c,
            'formula_d' => $formula_d,
			'formula_e' => $formula_e,
			'formula_question_arabic' => $formula_question_arabic,
            'formula_a_arabic' => $formula_a_arabic,
            'formula_b_arabic' => $formula_b_arabic,
            'formula_c_arabic' => $formula_c_arabic,
            'formula_d_arabic' => $formula_d_arabic,
			'formula_e_arabic' => $formula_e_arabic,
            'ans' => $answer,
            'correct' => $correct,
            'negative' => $negative,
            'exam_id' => $exam_id
        ];

		$image =  $_FILES['image'];
		 if ($image['size'] > 0) {

            $tar = "uploads/questions/";
            $rand = date('Ymd') . mt_rand(1001, 9999);
            $tar_file = $tar . $rand . '.png';
            move_uploaded_file($image['tmp_name'], $tar_file);

			$array['attachment'] = '1';
			$array['image'] = $tar_file;
		}

        if ($this->exam->insert('questions',$array)) {
            $this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Question added');
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add exam..!');
        }
        if ($param == '1') {
            redirect('admin/exams/question/' . $exam_id);
        }
        else {
            redirect('admin/exams/questions/' . $exam_id);
        }
    }
    public function get()
    {
		$course_id = $this->input->post('course_id');
        $result = $this->exam->make_datatables($course_id);
        $data   = array();
        foreach ($result as $res) {
            $sub_array   = array();
            $sub_array[] = $res->exam_name;
            $sub_array[] = $res->batch_name;
			$sub_array[] = $res->section_title;
            $sub_array[] = $res->no_questions;
            $sub_array[] = $res->exam_time .' minutes';

            if ($res->time_status == '1') {
                $sub_array[] = 'From : ' . date('d/m/Y h:i A',strtotime($res->from_time)) . '<br>To : ' . date('d/m/Y h:i A',strtotime($res->to_time));
            }
            else {
                $sub_array[] = 'Time not set';
            }
            if($res->registration_status != '1')
            {
                $sub_array[] = '<a class="btn btn-primary btn-xs"  href="' . site_url('admin/exams/questions/' . $res->exam_id) . '">Complete</a><br>Exam registration is not completed';
            }
            else
            {
                $sub_array[] = '<a class="btn btn-primary btn-xs"  href="' . site_url('admin/exams/edit/' . $res->exam_id) . '">Edit</a>';
            }
            $sub_array[] = '<a class="btn btn-primary btn-xs"  href="' . site_url('admin/exams/question/' . $res->exam_id) . '">Questions</a>';
            $sub_array[] = '<a class="btn btn-primary btn-xs" style="margin-bottom:5px;" href="' . site_url('admin/exams/instructions/' . $res->exam_id) . '">Instruction</a><br>
                            <a class="btn btn-primary btn-xs"  href="' . site_url('admin/exams/leaderboard/' . $res->exam_id) . '">Leaderboard</a>
                            ';
            if ($res->exam_status == '1') {
                $sub_array[] = '<a class="btn btn-danger btn-xs"  href="' . site_url('admin/exams/status/' . $res->exam_id .'/0') . '">Block</a>';
            }
            else {
                $sub_array[] = '<a class="btn btn-success btn-xs"  href="' . site_url('admin/exams/status/' . $res->exam_id .'/1') . '">Activate</a>';
            }
            $sub_array[] = '<a class="btn btn-danger btn-xs" onclick="return confirm(`Are you sure to delete?`)"  href="' . site_url('admin/exams/deleteExam/' . $res->exam_id) . '">Delete</a>';

            $data[]      = $sub_array;
        }
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $this->exam->get_all_data($course_id),
            "recordsFiltered" => $this->exam->get_filtered_data($course_id),
            "data" => $data
        );
        echo json_encode($output);
    }
	public function status($exam_id,$status)
    {
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
            $exam = $get->row();
            if ($exam->registration_status == '0') {
                $this->session->set_flashdata('alert_type', 'error');
    			$this->session->set_flashdata('alert_title', 'Failed');
    			$this->session->set_flashdata('alert_message', 'Registration of exam still pending');
            }
            else {
                $array = [
                    'exam_status' => $status
                ];
                $this->exam->update('exam_id',$exam_id,'exams',$array);
                $this->session->set_flashdata('alert_type', 'success');
    			$this->session->set_flashdata('alert_title', 'Success');
                if ($status == '0') {
                    $this->session->set_flashdata('alert_message', 'Exam deactivated..!');
                }
                else {
                    $this->session->set_flashdata('alert_message', 'Exam activated..!');
                }
            }
			redirect('admin/courses/details/7/' . $exam->course_id);
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load exam..!');
			redirect('admin/courses');
        }
    }
	public function instructions($exam_id)
    {
		$data['title'] = 'Exam instructions';
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
            $data['instructions']  = $this->exam->get_details('exam_instructions',array('exam_id' => $exam_id))->result();
            $data['exam']  = $get->row();
            $this->load->view('admin/exams/instructions',$data);
        }
        else {
            redirect('admin/courses');
        }
    }
	public function deleteExam($exam_id)
    {
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
			$exam = $get->row();
            $this->exam->delete('exams',array('exam_id' => $exam_id));
            $this->exam->delete('questions',array('exam_id' => $exam_id));
            // $this->exam->delete('students_question_answers',array('exam_id' => $exam_id));

            $this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Exam and all data related to this exam was deleted');
            redirect('admin/courses/details/7/' . $exam->course_id);
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to delete exam..!');
            redirect('admin/courses');
        }
    }
	public function question($exam_id)
    {
        $data['title'] = 'Questions';
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
            $data['questions'] = $question = $this->exam->get_details('questions',array('exam_id' => $exam_id))->result();
            $data['exam'] = $exam = $get->row();
            $this->load->view('admin/exams/view_questions',$data);
        }
        else {
            redirect('admin/exams');
        }
    }
    public function deleteQuestion($que_id)
    {
        $get = $this->exam->get_details('questions',array('que_id' => $que_id));
        if ($get->num_rows() > 0) {
            $exam_id = $get->row()->exam_id;
            $this->exam->delete('questions',array('que_id' => $que_id));
            // $this->exam->delete('students_question_answers',array('que_id' => $que_id));
            $array = [
                'registration_status' => '0',
                'exam_status' => '0'
            ];
            $this->exam->update('exam_id',$exam_id,'exams',$array);
            $this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Question deleted from the list..!');
            redirect('admin/exams/question/' . $exam_id);
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to remove question..!');
            redirect('admin/courses');
        }
    }
    public function editQues($que_id)
	{
		$get = $this->exam->get_details('questions',array('que_id' => $que_id));
		if ($get->num_rows() > 0) {
			$data['que'] = $get->row();
			$this->load->view('admin/exams/question_edit',$data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add exam..!');
			redirect('admin/exams');
		}
	}
	public function editQuestion()
    {
        $exam_id  = $this->security->xss_clean($this->input->post('exam_id'));
        $que_id  = $this->security->xss_clean($this->input->post('que_id'));

		$formula_question  = $this->input->post('formula_question');
        $formula_a  = $this->input->post('formula_a');
        $formula_b  = $this->input->post('formula_b');
        $formula_c  = $this->input->post('formula_c');
        $formula_d  = $this->input->post('formula_d');
		$formula_e  = $this->input->post('formula_e');

		$formula_question_arabic  = $this->input->post('formula_question_arabic');
        $formula_a_arabic         = $this->input->post('formula_a_arabic');
        $formula_b_arabic         = $this->input->post('formula_b_arabic');
        $formula_c_arabic         = $this->input->post('formula_c_arabic');
        $formula_d_arabic         = $this->input->post('formula_d_arabic');
		$formula_e_arabic         = $this->input->post('formula_e_arabic');

		$correct  = $this->input->post('correct');
		$negative  = $this->input->post('negative');

        $answer  = $this->security->xss_clean($this->input->post('answer'));

        $array = [
            'exam_id' => $exam_id,
            'que_id' => $que_id,
			'formula_question' => $formula_question,
            'formula_a' => $formula_a,
            'formula_b' => $formula_b,
            'formula_c' => $formula_c,
            'formula_d' => $formula_d,
			'formula_e' => $formula_e,
			'formula_question_arabic' => $formula_question_arabic,
            'formula_a_arabic' => $formula_a_arabic,
            'formula_b_arabic' => $formula_b_arabic,
            'formula_c_arabic' => $formula_c_arabic,
            'formula_d_arabic' => $formula_d_arabic,
			'formula_e_arabic' => $formula_e_arabic,
			'correct' => $correct,
			'negative' => $negative,
            'ans' => $answer
        ];


        $image =  $_FILES['image'];
        if ($image['size'] > 0)
        {
            $tar = "uploads/questions/";
            $rand = date('Ymd') . mt_rand(1001, 9999);
            $tar_file = $tar . $rand . '.png';
            move_uploaded_file($image['tmp_name'], $tar_file);

        	$array['attachment'] = '1';
        	$array['image'] = $tar_file;

        	$old_image = $this->input->post('old_image');
        	if ($old_image != '') {
        		$delete_path = FCPATH . $old_image;
        		unlink($old_image);
        	}
		}

        if ($this->exam->update('que_id',$que_id,'questions',$array)) {
            $this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Question updated');
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to update question..!');
        }
        redirect('admin/exams/question/' . $exam_id);
    }

    public function addInstruction()
    {

        $exam_id     = $this->security->xss_clean($this->input->post('exam_id'));
        $instruction = $this->security->xss_clean($this->input->post('instruction'));
		$instruction_arabic = $this->security->xss_clean($this->input->post('instruction_arabic'));

        $array      = [
                         'exam_id'    => $exam_id,
                         'instruction'=> $instruction,
						 'instruction_arabic' => $instruction_arabic,
                         'added_by'   => 'a'
                      ];
        if ($this->exam->insert('exam_instructions',$array)) {
            $this->session->set_flashdata('alert_type', 'success');
            $this->session->set_flashdata('alert_title', 'Success');
            $this->session->set_flashdata('alert_message', 'Instruction added...');
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to add instruction..!');
        }
        redirect('admin/exams/instructions/' . $exam_id);
    }

    public function editInstruction()
    {

        $inst_id    = $this->security->xss_clean($this->input->post('inst_id'));
        $exam_id    = $this->security->xss_clean($this->input->post('exam_id'));
        $instruction= $this->security->xss_clean($this->input->post('instruction'));
		$instruction_arabic = $this->security->xss_clean($this->input->post('instruction_arabic'));

        $array      = [
                         'instruction'=> $instruction,
						 'instruction_arabic' => $instruction_arabic,
                         'edited_by'  => 'a'
					 ];
        if ($this->exam->update('ei_id',$inst_id,'exam_instructions',$array)) {
            $this->session->set_flashdata('alert_type', 'success');
            $this->session->set_flashdata('alert_title', 'Success');
            $this->session->set_flashdata('alert_message', 'Instruction updated...');
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to update instruction..!');
        }
        redirect('admin/exams/instructions/' . $exam_id);
    }
    public function getInstructionDetails()
    {
        $inst_id = $this->security->xss_clean($this->input->post('inst_id'));
        $data = $this->exam->get_details('exam_instructions',array('ei_id' => $inst_id))->row();
        print_r(json_encode($data));
    }

    public function deleteInstruction($id)
    {

        $instruction= $this->exam->get_details('exam_instructions',array('ei_id'=>$id))->row();
        $exam_id    = $instruction->exam_id;

        if ($this->exam->delete('exam_instructions',array('ei_id'=>$id))) {
            $this->session->set_flashdata('alert_type', 'success');
            $this->session->set_flashdata('alert_title', 'Success');
            $this->session->set_flashdata('alert_message', 'Instruction deleted...');
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to delete instruction..!');
        }
        redirect('admin/exams/instructions/' . $exam_id);
    }
    public function leaderboard($exam_id)
	{
	    $data['title'] = 'Leaderboard';
		$get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
		if ($get->num_rows() > 0) {
			$exam = $get->row();
			$data['leaders'] = $this->exam->getLeaderBoard($exam_id);
			$data['exam_name'] = $exam->exam_name;
            $this->load->view('admin/exams/leaderboard',$data);
		}
		else {
			redirect('admin/courses');
		}
	}
	public function deleteEntry($se_id)
	{
		$get = $this->exam->getExamBySe_id($se_id);
		if ($get->num_rows() > 0) {
			$exam = $get->row();
			$this->exam->delete('students_question_answers',array('se_id' => $se_id));
			$this->exam->delete('students_exams',array('se_id' => $se_id));

			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Entry deleted');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load exam..!');
			redirect('admin/courses');
		}
	}
	public function updateMark()
	{
	    $se_id    = $this->security->xss_clean($this->input->post('se_id'));
		$get = $this->exam->getExamBySe_id($se_id);
		if ($get->num_rows() > 0) {
			$array = array(
				'mark' => $this->security->xss_clean($this->input->post('mark'))
			);
			$this->exam->update('se_id',$se_id,'students_exams',$array);

			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Student mark updated');
			redirect('admin/exams/leaderboard/' . $get->row()->exam_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to update mark');
			redirect('admin/courses');
		}
	}
}
?>
