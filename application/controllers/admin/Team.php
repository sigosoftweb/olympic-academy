<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Team extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!admin()) {
            redirect('admin');
        }
		$this->load->model('admin/Model_team','team');
    }
    public function index() {
        $data['title'] = 'Olymbic | Team';
		$data['team'] = $this->team->getTeams();
        $this->load->view('admin/team/view', $data);
    }
	public function add()
	{
		$data['title'] = 'Olymbic | Add team';
        $this->load->view('admin/team/add', $data);
	}
	public function edit($team_id)
	{
		$get = $this->team->get_details('team',array('team_id' => $team_id));
		if ($get->num_rows() > 0) {
			$data['title'] = 'Olymbic | Edit team';
			$data['team'] = $get->row();
			$this->load->view('admin/team/edit',$data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load team');
			redirect('admin/team');
		}
	}
	public function addTeam()
	{
		$array = array(
			'team_first' => $this->security->xss_clean($this->input->post('team_first')),
			'team_second' => $this->security->xss_clean($this->input->post('team_second')),
			'team_designation' => $this->security->xss_clean($this->input->post('team_designation')),
			'first_arabic' => $this->security->xss_clean($this->input->post('first_arabic')),
			'second_arabic' => $this->security->xss_clean($this->input->post('second_arabic')),
			'designation_arabic' => $this->security->xss_clean($this->input->post('designation_arabic')),
		);
		$image = $this->input->post('image');
		if ($image != '')
		{
			$img = substr($image, strpos($image, ",") + 1);
			$url = FCPATH.'uploads/team/';
			$rand = $plan_name.date('Ymd').mt_rand(1001,9999);
			$userpath = $url.$rand.'.png';
			$path = "uploads/team/".$rand.'.png';
			file_put_contents($userpath,base64_decode($img));

            $array['team_image'] = $path;
		}
        else {
            $array['team_image'] = '';
        }

		if ($team_id = $this->team->insert('team',$array)) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'New team member added..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add team members..!');
		}
		redirect('admin/team');
	}
	public function editteam()
	{
		$team_id = $this->security->xss_clean($this->input->post('team_id'));
		$get = $this->team->get_details('team',array('team_id' => $team_id));
		if ($get->num_rows() > 0) {
			$team = $get->row();
			$array = array(
				'team_first' => $this->security->xss_clean($this->input->post('team_first')),
				'team_second' => $this->security->xss_clean($this->input->post('team_second')),
				'team_designation' => $this->security->xss_clean($this->input->post('team_designation')),
				'first_arabic' => $this->security->xss_clean($this->input->post('first_arabic')),
				'second_arabic' => $this->security->xss_clean($this->input->post('second_arabic')),
				'designation_arabic' => $this->security->xss_clean($this->input->post('designation_arabic')),
			);

			$image = $this->input->post('image');
			if ($image != '')
			{
				$img = substr($image, strpos($image, ",") + 1);
				$url = FCPATH.'uploads/team/';
				$rand = $plan_name.date('Ymd').mt_rand(1001,9999);
				$userpath = $url.$rand.'.png';
				$path = "uploads/team/".$rand.'.png';
				file_put_contents($userpath,base64_decode($img));

	            $array['team_image'] = $path;

				$unlink = FCPATH . $team->team_image;
				unlink($unlink);
			}

			if ($this->team->update('team_id',$team_id,'team',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Details updated..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to update detail..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load team..!');
		}
		redirect('admin/team');
	}
	public function status($team_id,$status)
	{
		$array = array(
			'team_status' => $status
		);
		$this->team->update('team_id',$team_id,'team',$array);
		if ($status == '1') {
			$this->session->set_flashdata('alert_message', 'Member activated..!');
		}
		else {
			$this->session->set_flashdata('alert_message', 'Member deactivated..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('admin/team');
	}
	public function delete($team_id)
	{
		$get = $this->team->get_details('team',array('team_id' => $team_id));
		if ($get->num_rows() > 0) {
			$team = $get->row();
			if ($this->team->delete('team',array('team_id'=> $team_id))) {
				$unlink = FCPATH . $team->team_image;
				unlink($unlink);

				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Member deleted');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to delete member..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load member..!');
		}
		redirect('admin/team');
	}
}
?>
