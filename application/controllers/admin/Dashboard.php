<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!admin()) {
            redirect('admin');
        }
		$this->load->model('admin/Model_dashboard','dashboard');
    }
    public function index() {
        $data['title'] = 'Olymbic | Dashboard';
		$data['count_student'] = $this->dashboard->getStudentCount();
		$data['count_trainers'] = $this->dashboard->getTrainersCount();
		$data['courses'] = $this->dashboard->getCourseCount();
		$data['events'] = $this->dashboard->getEventsCount();
		$data['exams'] = $this->dashboard->getExamCount();
		$data['today'] = $this->dashboard->getTodayEarning();
		$data['monthly'] = $this->dashboard->getMonthlyEarning();
		$data['lifetime'] = $this->dashboard->getLifetimeEarning();
		
		$data['todayTamkeen'] = $this->dashboard->getTodayEarningTamkeen();
		$data['monthlyTamkeen'] = $this->dashboard->getMonthlyEarningTamkeen();
		$data['lifetimeTamkeen'] = $this->dashboard->getLifetimeEarningTamkeen();

		$data['students'] = $this->dashboard->getStudents();
		$data['trainers'] = $this->dashboard->getTrainers();
		$data['transactions'] = $this->dashboard->getTransactions();

        $this->load->view('admin/dashboard/view', $data);
    }
}
?>
