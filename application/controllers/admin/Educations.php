<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Educations extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!admin()) {
            redirect('admin');
        }
		$this->load->model('admin/Model_educations','education');
    }
    public function index() {
        $data['title'] = 'Olymbic | Educations';
		$data['educations'] = $this->education->getAllEducations();
        $this->load->view('admin/educations/view', $data);
    }
	public function add()
	{
		$education = $this->security->xss_clean($this->input->post('education'));
		if ($this->education->insert('educations',array('education' => $education))) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'New education added..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add education..!');
		}
		redirect('admin/educations');
	}
	public function edit()
	{
		$edu_id = $this->security->xss_clean($this->input->post('edu_id'));
		$education = $this->security->xss_clean($this->input->post('education'));
		if ($this->education->update('edu_id',$edu_id,'educations',array('education' => $education))) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Logged in Successfully..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add education..!');
		}
		redirect('admin/educations');
	}
	public function status($edu_id,$status)
	{
		$array = array(
			'status' => $status
		);
		$this->education->update('edu_id',$edu_id,'educations',$array);
		if ($status == '1') {
			$this->session->set_flashdata('alert_message', 'Education activated..!');
		}
		else {
			$this->session->set_flashdata('alert_message', 'Education deactivated..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('admin/educations');
	}
	public function getDetails()
	{
		$edu_id = $this->security->xss_clean($this->input->post('edu_id'));
		$education = $this->education->get_details('educations',array('edu_id' => $edu_id))->row();
		print_r(json_encode($education));
	}
}
?>
