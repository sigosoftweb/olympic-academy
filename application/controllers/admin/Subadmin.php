<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Subadmin extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!admin()) {
            redirect('admin');
        }
		$this->load->model('admin/Model_subadmin','sub');
    }
    public function index() {
        $data['title'] = 'Olymbic | Subadmin';
        $this->load->view('admin/subadmin/view', $data);
    }
	public function add()
	{
		$data['title'] = 'Olymbic | Add subadmin';
        $this->load->view('admin/subadmin/add', $data);
	}
	public function edit($id)
	{
		$get = $this->sub->get_details('sub_admin',array('sa_id' => $id));
		if ($get->num_rows() > 0) 
		{
			$data['title'] = 'Olymbic | Edit Subadmin';
			$data['admin'] = $get->row();
			$this->load->view('admin/subadmin/edit',$data);
		}
		else 
		{
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load data');
			redirect('admin/subadmin');
		}
	}

	public function get()
	{
		$result = $this->sub->make_datatables();
		$data = array();
		foreach ($result as $res) {
			$sub_array = array();
			if ($res->photo != '') 
			{
				$sub_array[] = '<img src="' . base_url() . $res->photo . '" width="100px" height="100px">';
			}
			else 
			{
				$sub_array[] = '<img src="' . base_url() . 'uploads/subadmin/user.png" width="100px" height="100px">';
			}
			$sub_array[] = $res->name_english . '<br>' . $res->name_arabic;
			$sub_array[] = $res->email . '<br>' . $res->phone;
			$sub_array[] = $res->cpr;
			$sub_array[] = $res->address;
			$sub_array[] = '<a class="btn btn-primary btn-xs margin-bottom-20" href="' . site_url('admin/subadmin/previlges/'.$res->sa_id) . '">Previleges</a>';
			if ($res->status) {
				$status = 'Active<br><a class="btn btn-danger btn-xs margin-bottom-20" href="' . site_url('admin/subadmin/status/'.$res->sa_id . '/0') . '" onclick="return block()">Block</a>';
			}
			else {
				$status = 'Blocked<br><a class="btn btn-success btn-xs margin-bottom-20" href="' . site_url('admin/subadmin/status/'.$res->sa_id . '/1') . '" onclick="return unblock()">Activate</a>';
			}
			$sub_array[] = $status;
			$sub_array[] = '<a class="btn btn-primary btn-xs margin-bottom-20" href="' . site_url('admin/subadmin/edit/'.$res->sa_id) . '">Edit</a>';
			$sub_array[] = '<a class="btn btn-primary btn-xs margin-bottom-20" onclick="changePassword('. $res->sa_id .')">Change<br>password</s>';

			$data[] = $sub_array;
		}

		$output = array(
			"draw"   => intval($_POST['draw']),
			"recordsTotal" => $this->sub->get_all_data(),
			"recordsFiltered" => $this->sub->get_filtered_data(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function addData()
	{  
		$modules  = $this->sub->get_details('modules',array('status'=>'1'))->result();
		$array    = array(
			'name_english' => $this->security->xss_clean($this->input->post('name_english')),
			'name_arabic' => $this->security->xss_clean($this->input->post('name_arabic')),
			'cpr'     => $this->security->xss_clean($this->input->post('cpr')),
			'phone'   => $this->security->xss_clean($this->input->post('phone')),
			'email'   => $this->security->xss_clean($this->input->post('email')),
			'address' => $this->security->xss_clean($this->input->post('address')),
			'password' => md5($this->security->xss_clean($this->input->post('password')))
		);
		$file = $_FILES['image'];
		if ($file['size'] > 0) {
			$tar = "uploads/subadmin/";
			$rand=date('Ymd').mt_rand(1001,9999);
			$tar_file = $tar . $rand . basename($file['name']);
			if(move_uploaded_file($file["tmp_name"], $tar_file))
			{
				$array['photo'] = $tar_file;
			}
			else {
				$array['photo'] = '';
			}
		}
		else {
			$array['photo'] = '';
		}

		if ($id= $this->sub->insert('sub_admin',$array)) 
		{
			foreach($modules as $module)
			{
				$m_array   = [
					            'subadmin_id' => $id,
					            'm_id'        => $module->m_id,
					            'view'        => '1',
					            'add'         => '1',
					            'edit'        => '1',
					            'block'       => '1',
					            'delete'      => '1',
					            'all'         => '1',
					            'timestamp'   => date('Y-m-d H:i:s')
				             ];
				$this->sub->insert('subadmin_previleges',$m_array);             
			}
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'New admin added..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add admin..!');
		}
		redirect('admin/subadmin');
	}
	public function editData()
	{
		$admin_id = $this->security->xss_clean($this->input->post('admin_id'));
		$get      = $this->sub->get_details('sub_admin',array('sa_id' => $admin_id));
		if ($get->num_rows() > 0) 
		{
			$admin = $get->row();
			$array = array(
				'name_english' => $this->security->xss_clean($this->input->post('name_english')),
				'name_arabic' => $this->security->xss_clean($this->input->post('name_arabic')),
				'cpr'     => $this->security->xss_clean($this->input->post('cpr')),
				'phone'   => $this->security->xss_clean($this->input->post('phone')),
				'email'   => $this->security->xss_clean($this->input->post('email')),
				'address' => $this->security->xss_clean($this->input->post('address')),
			);

			$file = $_FILES['image'];
			if ($file['size'] > 0) {
				$tar = "uploads/subadmin/";
				$rand=date('Ymd').mt_rand(1001,9999);
				$tar_file = $tar . $rand . basename($file['name']);
				if(move_uploaded_file($file["tmp_name"], $tar_file))
				{
					$array['photo'] = $tar_file;
					if ($admin->photo != '') {
						$unliink = FCPATH . $admin->photo;
						unlink($unliink);
					}
				}
			}
            
            $check = $this->sub->get_details('sub_admin',array('email'=>$array['email'],'sa_id!='=>$admin_id));
            if($check->num_rows()==0)
            {
            	if ($this->sub->update('sa_id',$admin_id,'sub_admin',$array)) 
            	{
					$this->session->set_flashdata('alert_type', 'success');
					$this->session->set_flashdata('alert_title', 'Success');
					$this->session->set_flashdata('alert_message', 'Admin details updated..!');
				}
				else 
				{
					$this->session->set_flashdata('alert_type', 'error');
					$this->session->set_flashdata('alert_title', 'Failed');
					$this->session->set_flashdata('alert_message', 'Failed to update admin..!');
				}
				redirect('admin/subadmin');
            }
            else
            {
            	    $this->session->set_flashdata('alert_type', 'error');
					$this->session->set_flashdata('alert_title', 'Failed');
					$this->session->set_flashdata('alert_message', 'Email already exists');
					redirect('admin/subadmin/edit/'.$admin_id);
            }
			
		}
		else 
		{
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load admin..!');
			redirect('admin/subadmin');
		}
		
	}
	public function status($id,$status)
	{
		$array  = array(
						'status' => $status
					   );
		$this->sub->update('sa_id',$id,'sub_admin',$array);
		if ($status == '1') {
			$this->session->set_flashdata('alert_message', 'Admin activated..!');
		}
		else {
			$this->session->set_flashdata('alert_message', 'Admin deactivated..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('admin/subadmin');
	}
	public function getDetails()
	{
		$admin_id = $this->security->xss_clean($this->input->post('admin_id'));
		$admin = $this->sub->get_details('sub_admin',array('sa_id' => $admin_id))->row();
		print_r(json_encode($admin));
	}
	public function change()
	{
		$admin_id = $this->security->xss_clean($this->input->post('admin_id'));
		$password = $this->security->xss_clean($this->input->post('password'));
		$array = array(
							'password' => md5($password)
						);
		if ($this->sub->update('sa_id',$admin_id,'sub_admin',$array)) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Admin password has been successfully reset');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to update admin password');
		}
		redirect('admin/subadmin');
	}

	public function checkAdmin() 
	{
        $email = $this->input->post('email');
        $check_mail  = $this->sub->get_details('sub_admin', array('email' => $email));
        if($check_mail->num_rows() > 0) 
        {
            $data = '1';
        }
        else
        {
        	$data = '0';
        }
        print_r($data);
    }

    public function previlges($id) 
    {
        $data['title']   = 'Olymbic | Previleges';
        $modules         = $this->sub->getAllPrevileges($id);
        foreach($modules as $module)
        {
        	$module->item = $this->sub->get_details('modules',array('m_id'=>$module->m_id))->row()->item;
        }
		$data['modules'] = $modules;
        $this->load->view('admin/subadmin/previleges', $data);
    }

    public function disable($id,$status)
	{   
		$module   = $this->sub->get_details('subadmin_previleges',array('sp_id'=>$id))->row();
		$admin_id = $module->subadmin_id;
		if($status=='v')
		{
			if($module->add=='0' && $module->edit=='0' && $module->block=='0' && $module->delete=='0')
			{
				$array = array(
				             'view' => '0',
				             'all'  => '0'
			              );
			}
			else
			{
				$array = array(
				             'view' => '0',
				             'all'  => '2'
			              );
			}
			
	    }
	    elseif($status=='a')
	    {
	    	if($module->view=='0' && $module->edit=='0' && $module->block=='0' && $module->delete=='0')
			{
	    	    $array = array(
				                 'add' => '0',
				                 'all' => '0'
			                  );
	    	}
	    	else
	    	{
               $array = array(
				                 'add' => '0',
				                 'all' => '2'
			                  );
	    	}    
	    }
	    elseif($status=='e')
	    {
	    	if($module->view=='0' && $module->add=='0' && $module->block=='0' && $module->delete=='0')
			{
		    	$array = array(
					             'edit' => '0',
					             'all'  => '0'
				              );
		    }
		    else
		    {
                $array = array(
					             'edit' => '0',
					             'all'  => '2'
				              );
		    }	
	    }
	    elseif($status=='b')
	    {
	    	if($module->view=='0' && $module->add=='0' && $module->edit=='0' && $module->delete=='0')
	    	{
	    		$array = array(
					             'block' => '0',
					             'all'   => '0'
				              );
	    	}	
	    	else
	    	{
	    		$array = array(
					             'block' => '0',
					             'all'   => '2'
				              );
	    	}	    	
	    }		
	    elseif($status=='d')
	    {
	    	if($module->view=='0' && $module->add=='0' && $module->block=='0' && $module->edit=='0')
	    	{
		    	$array = array(
					             'delete' => '0',
					             'all'    => '0'
				              );
		    }
		    else
		    {
               $array = array(
					             'delete' => '0',
					             'all'    => '2'
				              );
		    } 	
	    }
	    elseif($status=='w')
	    {
	    	$array = array(
				             'all' => '0',
				             'add' => '0',
				             'edit'=> '0',
				             'view'=> '0',
				             'block'=> '0',
				             'delete'=> '0'
			              );
	    }

		if ($this->sub->update('sp_id',$id,'subadmin_previleges',$array)) 
		{   
			$this->session->set_flashdata('alert_message', 'Data blocked..!');
		}
		else 
		{
			$this->session->set_flashdata('alert_message', 'Failed to blocked..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('admin/subadmin/previlges/'.$admin_id);
	}

	public function enable($id,$status)
	{
		$module   = $this->sub->get_details('subadmin_previleges',array('sp_id'=>$id))->row();
		$admin_id = $module->subadmin_id;
		if($status=='v')
		{  
			if($module->add=='1' && $module->edit=='1' && $module->block=='1' && $module->delete=='1')
			{
				$array = array(
				                'view'  => '1',
				                'all'   => '1',
			                  );
			}
			else
			{
				$array = array(
				                'view'  => '1',
				                'all'   => '2',
			                  );
			}
			
	    }
	    elseif($status=='a')
	    {   
	    	if($module->view=='1' && $module->edit=='1' && $module->block=='1' && $module->delete=='1')
	    	{
                   $array = array(
				             'add' => '1',
				             'all' => '1',
			              );
	    	}
	    	else
	    	{
	    		$array = array(
				             'add' => '1',
				             'all' => '2',
			              );
	    	}
	    	
	    }
	    elseif($status=='e')
	    {  
	    	if($module->view=='1' && $module->add=='1' && $module->block=='1' && $module->delete=='1')
	    	{
	    		$array = array(
				             'edit' => '1',
				             'all'  => '2',
			              );
	    	}
	    	else
	    	{
	    		$array = array(
				             'edit' => '1',
				             'all'  => '2',
			              );
	    	}
	    	
	    }
	    elseif($status=='b')
	    {  
	    	if($module->view=='1' && $module->add=='1' && $module->edit=='1' && $module->delete=='1')
	    	{
	    		$array = array(
				             'block' => '1',
				             'all'   => '1',
			              );
	    	}
	    	else
	    	{
	    		$array = array(
				             'block' => '1',
				             'all'   => '2',
			              );
	    	}
	    	
	    }		
	    elseif($status=='d')
	    {
	    	if($module->view=='1' && $module->add=='1' && $module->edit=='1' && $module->block=='1')
	    	{
	    	    $array = array(
				             'delete' => '1',
				             'all'    => '1',
			              );
	    	}
	    	else
	    	{
	    		 $array = array(
				             'delete' => '1',
				             'all'    => '2',
			              );
	    	}    
	    }
	    elseif($status=='w')
	    {
	    	$array = array(
				             'all'   => '1',
				             'add'   => '1',
				             'edit'  => '1',
				             'view'  => '1',
				             'block' => '1',
				             'delete'=> '1'
			              );
	    }

		if ($this->sub->update('sp_id',$id,'subadmin_previleges',$array)) 
		{   
			$this->session->set_flashdata('alert_message', 'Data activated..!');
		}
		else 
		{
			$this->session->set_flashdata('alert_message', 'Failed to activate..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('admin/subadmin/previlges/'.$admin_id);
	}
}
?>
