<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Students extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!admin()) {
            redirect('admin');
        }
		$this->load->model('admin/Model_students','student');
    }
    public function index() {
        $data['title'] = 'Olymbic | Students';
        $this->load->view('admin/students/view', $data);
    }
	public function add()
	{
		$data['title'] = 'Olymbic | Add Students';
		$data['courses'] = $this->student->getAvailableCourses(0);
		$data['timezones'] =  DateTimeZone::listIdentifiers(DateTimeZone::ALL);
        $this->load->view('admin/students/add', $data);
	}
	public function approvals()
	{
		$data['title'] = 'Olympic | Approve students';
		$data['students'] = $this->student->get_details('students',array('stu_registration' => 'Waiting'))->result();
		$this->load->view('admin/students/approvals',$data);
	}
	public function rejected()
	{
		$data['title'] = 'Olympic | Approve students';
		$data['students'] = $this->student->get_details('students',array('stu_registration' => 'Rejected'))->result();
		$this->load->view('admin/students/rejected',$data);
	}
	public function approve($stu_id)
	{
		$get = $this->student->get_details('students',array('stu_id' => $stu_id));
		if ($get->num_rows() > 0) {
			$array = array(
				'stu_registration' => 'Approved'
			);
			$this->student->update('stu_id',$stu_id,'students',$array);

			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Student Approved');
			redirect('admin/students/approvals');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load student');
			redirect('admin/students');
		}
	}
	public function reject($stu_id)
	{
		$get = $this->student->get_details('students',array('stu_id' => $stu_id));
		if ($get->num_rows() > 0) {
			$array = array(
				'stu_registration' => 'Rejected'
			);
			$this->student->update('stu_id',$stu_id,'students',$array);

			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Student Rejected');
			redirect('admin/students/approvals');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load student');
			redirect('admin/students');
		}
	}
	public function edit($stu_id)
	{
		$get = $this->student->get_details('students',array('stu_id' => $stu_id));
		if ($get->num_rows() > 0) {
			$data['title'] = 'Olymbic | Edit Students';
			$data['student'] = $get->row();
			$data['timezones'] =  DateTimeZone::listIdentifiers(DateTimeZone::ALL);
			$this->load->view('admin/students/edit',$data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load student');
			redirect('admin/students');
		}
	}
	public function get()
	{
		$result = $this->student->make_datatables();
		$data = array();
		foreach ($result as $res) {
			$sub_array = array();
			if ($res->stu_image != '') {
				$sub_array[] = '<img src="' . base_url() . $res->stu_image . '" width="100px" height="100px">';
			}
			else {
				$sub_array[] = '<img src="' . base_url() . 'uploads/students/user.png" width="100px" height="100px">';
			}
			$sub_array[] = $res->stu_name_english . '<br>' . $res->stu_name_arabic;
			$sub_array[] = $res->stu_mobile . '<br>' . $res->stu_email;
			$sub_array[] = $res->stu_gender;
			if ($res->stu_dob == NULL || $res->stu_dob == '0000-00-00') {
				$sub_array[] = '';
			}
			else {
				$sub_array[] = date('d/m/Y',strtotime($res->stu_dob));
			}

			$sub_array[] = $res->stu_education;
			$sub_array[] = $res->stu_timezone;
			// $sub_array[] = $res->stu_contact;
			if($res->stu_cv != '')
            {
                $sub_array[] = '<a href="' . base_url() . $res->stu_cv . '" target="_blank">View</a>';
            }
            else
            {
                $sub_array[] = 'Not uploaded';
            }
			
			if ($res->stu_status) {
				$status = '<a class="btn btn-danger btn-xs" href="' . site_url('admin/students/status/'.$res->stu_id . '/0') . '" onclick="return block()">Block</a><br>Active';
			}
			else {
				$status = '<a class="btn btn-success btn-xs" href="' . site_url('admin/students/status/'.$res->stu_id . '/1') . '" onclick="return unblock()">Activate</a><br>Blocked';
			}
			$sub_array[] = $status;
			$sub_array[] = '<a class="btn btn-primary btn-xs" style="margin-top:5px;" href="' . site_url('admin/students/profile/'.$res->stu_id) . '">Profile</a>';
			$sub_array[] = '<a class="btn btn-primary btn-xs" onclick="changePassword('. $res->stu_id .')">Change<br>password</s>';

			$data[] = $sub_array;
		}

		$output = array(
			"draw"   => intval($_POST['draw']),
			"recordsTotal" => $this->student->get_all_data(),
			"recordsFiltered" => $this->student->get_filtered_data(),
			"data" => $data
		);
		echo json_encode($output);
	}
	public function addStudent()
	{
		$array = array(
			'stu_name_english' => $this->security->xss_clean($this->input->post('stu_name_english')),
			'stu_name_arabic' => $this->security->xss_clean($this->input->post('stu_name_arabic')),
			'first_arabic' => $this->security->xss_clean($this->input->post('first_arabic')),
			'second_arabic' => $this->security->xss_clean($this->input->post('second_arabic')),
			'stu_gender' => $this->security->xss_clean($this->input->post('stu_gender')),
			'stu_timezone' => $this->security->xss_clean($this->input->post('stu_timezone')),
			'stu_cpr' => $this->security->xss_clean($this->input->post('stu_cpr')),
			'stu_dob' => $this->security->xss_clean($this->input->post('stu_dob')),
			'stu_mobile' => $this->security->xss_clean($this->input->post('stu_mobile')),
			'stu_email' => $this->security->xss_clean($this->input->post('stu_email')),
			'stu_education' => $this->security->xss_clean($this->input->post('stu_education')),
			'stu_contact' => $this->security->xss_clean($this->input->post('stu_contact')),
			'stu_password' => md5($this->security->xss_clean($this->input->post('stu_password')))
		);
		$image = $this->input->post('image');
		if ($image != '')
		{
			$img = substr($image, strpos($image, ",") + 1);
			$url = FCPATH.'uploads/students/';
			$rand = $plan_name.date('Ymd').mt_rand(1001,9999);
			$userpath = $url.$rand.'.png';
			$path = "uploads/students/".$rand.'.png';
			file_put_contents($userpath,base64_decode($img));

            $array['stu_image'] = $path;
		}
        else {
            $array['stu_image'] = '';
        }

		$file = $_FILES['cv'];
		if ($file['size'] > 0) {
			$tar = "uploads/students-cv/";
			$rand=date('Ymd').mt_rand(1001,9999);
			$tar_file = $tar . $rand . basename($file['name']);
			if(move_uploaded_file($file["tmp_name"], $tar_file))
			{
				$array['stu_cv'] = $tar_file;
			}
			else {
				$array['stu_cv'] = '';
			}
		}
		else {
			$array['stu_cv'] = '';
		}

		if ($stu_id = $this->student->insert('students',$array)) {

		    $course_id = $this->security->xss_clean($this->input->post('course_id'));
			if ($course_id != '0') {
				$get = $this->student->get_details('courses',array('course_id' => $course_id));
				if ($get->num_rows() > 0) {
					$array = array(
						'stu_id' => $stu_id,
						'course_id' => $course_id,
						'date' => date('Y-m-d H:i:s'),
						'payment_id' => '',
						'sp_status' => 'Approved',
						'amount' => $this->security->xss_clean($this->input->post('amount')),
						'payment_type' => $this->security->xss_clean($this->input->post('payment_type')),
					);
					$this->student->insert('students_packages',$array);
				}
			}

			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'New student added..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add student..!');
		}
		redirect('admin/students');
	}
	public function editStudent()
	{
		$stu_id = $this->security->xss_clean($this->input->post('stu_id'));
		$get = $this->student->get_details('students',array('stu_id' => $stu_id));
		if ($get->num_rows() > 0) {
			$student = $get->row();
			$array = array(
				'stu_name_english' => $this->security->xss_clean($this->input->post('stu_name_english')),
				'stu_name_arabic' => $this->security->xss_clean($this->input->post('stu_name_arabic')),
				'first_arabic' => $this->security->xss_clean($this->input->post('first_arabic')),
				'second_arabic' => $this->security->xss_clean($this->input->post('second_arabic')),
				'stu_gender' => $this->security->xss_clean($this->input->post('stu_gender')),
				'stu_timezone' => $this->security->xss_clean($this->input->post('stu_timezone')),
				'stu_cpr' => $this->security->xss_clean($this->input->post('stu_cpr')),
				'stu_mobile' => $this->security->xss_clean($this->input->post('stu_mobile')),
				'stu_email' => $this->security->xss_clean($this->input->post('stu_email')),
				'stu_dob' => $this->security->xss_clean($this->input->post('stu_dob')),
				'stu_education' => $this->security->xss_clean($this->input->post('stu_education')),
				'stu_contact' => $this->security->xss_clean($this->input->post('stu_contact')),
			);

			
			$image = $this->input->post('image');
			if ($image!='')
			{
				$img = substr($image, strpos($image, ",") + 1);
				$url = FCPATH.'uploads/students/';
				$rand = date('Ymd').mt_rand(1001,9999);
				$userpath = $url.$rand.'.png';
				$path = "uploads/students/".$rand.'.png';
				file_put_contents($userpath,base64_decode($img));

	            $array['stu_image'] = $path;
			}

			if ($this->student->update('stu_id',$stu_id,'students',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Student details updated..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to update student..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load student..!');
		}
		redirect('admin/students');
	}
	public function status($stu_id,$status)
	{
		$array = array(
			'stu_status' => $status
		);
		$this->student->update('stu_id',$stu_id,'students',$array);
		if ($status == '1') {
			$this->session->set_flashdata('alert_message', 'student activated..!');
		}
		else {
			$this->session->set_flashdata('alert_message', 'student deactivated..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('admin/students');
	}
	public function getDetails()
	{
		$stu_id = $this->security->xss_clean($this->input->post('stu_id'));
		$student = $this->student->get_details('students',array('stu_id' => $stu_id))->row();
		print_r(json_encode($student));
	}
	public function change()
	{
		$stu_id = $this->security->xss_clean($this->input->post('stu_id'));
		$password = $this->security->xss_clean($this->input->post('password'));
		$array = array(
			'stu_password' => md5($password)
		);
		if ($this->student->update('stu_id',$stu_id,'students',$array)) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Student password has been successfully reset');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to update student password');
		}
		redirect('admin/students');
	}
	public function profile($stu_id)
	{
		$get = $this->student->get_details('students',array('stu_id' => $stu_id));
		if ($get->num_rows() > 0) {
			$data['title'] = 'Olymbic | Student Profile';
			$data['student'] = $get->row();
			$data['courses'] = $this->student->getStudentsPackages($stu_id);
			$data['available'] = $this->student->getAvailableCourses($stu_id);
			$data['timezones'] =  DateTimeZone::listIdentifiers(DateTimeZone::ALL);
			$this->load->view('admin/students/profile',$data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load student');
			redirect('admin/students');
		}
	}
	public function updateProfile()
	{
		$stu_id = $this->security->xss_clean($this->input->post('stu_id'));
		$get = $this->student->get_details('students',array('stu_id' => $stu_id));
		if ($get->num_rows() > 0) {
			$student = $get->row();
			$array = array(
				'stu_name_english' => $this->security->xss_clean($this->input->post('stu_name_english')),
				'stu_name_arabic' => $this->security->xss_clean($this->input->post('stu_name_arabic')),
				'first_arabic' => $this->security->xss_clean($this->input->post('first_arabic')),
				'second_arabic' => $this->security->xss_clean($this->input->post('second_arabic')),
				'stu_cpr' => $this->security->xss_clean($this->input->post('stu_cpr')),
				'stu_timezone' => $this->security->xss_clean($this->input->post('stu_timezone')),
				'stu_mobile' => $this->security->xss_clean($this->input->post('stu_mobile')),
				'stu_email' => $this->security->xss_clean($this->input->post('stu_email')),
				'stu_dob' => $this->security->xss_clean($this->input->post('stu_dob')),
				'stu_education' => $this->security->xss_clean($this->input->post('stu_education')),
				'stu_contact' => $this->security->xss_clean($this->input->post('stu_contact')),
			);

			$image = $this->input->post('image');
			if ($image!='')
			{
				$img = substr($image, strpos($image, ",") + 1);
				$url = FCPATH.'uploads/students/';
				$rand = date('Ymd').mt_rand(1001,9999);
				$userpath = $url.$rand.'.png';
				$path = "uploads/students/".$rand.'.png';
				file_put_contents($userpath,base64_decode($img));

	            $array['stu_image'] = $path;

	            if ($student->stu_image != '') {
						$unliink = FCPATH . $student->stu_image;
						unlink($unliink);
					}
			}


			$file = $_FILES['cv'];
    		if ($file['size'] > 0) {
    			$tar = "uploads/students-cv/";
    			$rand=date('Ymd').mt_rand(1001,9999);
    			$tar_file = $tar . $rand . basename($file['name']);
    			if(move_uploaded_file($file["tmp_name"], $tar_file))
    			{
    				$array['stu_cv'] = $tar_file;
    				$old_cv = $student->stu_cv;
    				if($old_cv != '')
    				{
    				    unlink($old_cv);
    				}
    			}
    		}

			if ($this->student->update('stu_id',$stu_id,'students',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Student details updated..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to update student..!');
			}
			redirect('admin/students/profile/' . $stu_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load student..!');
			redirect('admin/students');
		}
	}
	public function subscribeCourse()
	{
		$stu_id = $this->security->xss_clean($this->input->post('stu_id'));
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$get = $this->student->get_details('courses',array('course_id' => $course_id));
		if ($get->num_rows() > 0) {
			$array = array(
				'stu_id' => $stu_id,
				'course_id' => $course_id,
				'date' => date('Y-m-d H:i:s'),
				'payment_id' => '',
				'sp_status' => 'Approved',
				'amount' => $this->security->xss_clean($this->input->post('amount'))
			);
			if ($this->student->insert('students_packages',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Course subscribed');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to subscribe course, please try again..!');
			}
			redirect('admin/students/profile/' . $stu_id);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load course..!');
			redirect('admin/students');
		}
	}
	public function getCourse()
	{
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$course = $this->student->get_details('courses',array('course_id' => $course_id))->row();
		print_r(json_encode($course));
	}
	public function addValidation()
	{
		$mobile = $this->security->xss_clean($this->input->post('mobile'));
		$email = $this->security->xss_clean($this->input->post('email'));
		$mCheck = $this->student->get_details('students',array('stu_mobile' => $mobile))->num_rows();
		if ($mCheck > 0) {
			$mob = false;
		}
		else {
			$mob = true;
		}
		if ($email != '') {
			$eCheck = $this->student->get_details('students',array('stu_email' => $email))->num_rows();
			if ($eCheck > 0) {
				$ema = false;
			}
			else {
				$ema = true;
			}
		}
		else {
			$ema = true;
		}
		if ($ema && $mob) {
			$message = '';
			$status = true;
		}
		else {
			$status = false;
			if(!$ema && !$mob)
			{
				$message = 'Mobile number and Email address already registered';
			}
			else {
				if (!$ema) {
					$message = 'Email address already registered';
				}
				if (!$mob) {
					$message = 'Mobile number already registered';
				}
			}
		}
		$return = array(
			'status' => $status,
			'message' => $message
		);
		print_r(json_encode($return));
	}
	public function editValidation()
	{
		$stu_id = $this->security->xss_clean($this->input->post('stu_id'));
		$mobile = $this->security->xss_clean($this->input->post('mobile'));
		$email = $this->security->xss_clean($this->input->post('email'));
		$mCheck = $this->student->get_details('students',array('stu_mobile' => $mobile, 'stu_id !=' => $stu_id))->num_rows();
		if ($mCheck > 0) {
			$mob = false;
		}
		else {
			$mob = true;
		}
		if ($email != '') {
			$eCheck = $this->student->get_details('students',array('stu_email' => $email,'stu_id !=' => $stu_id))->num_rows();
			if ($eCheck > 0) {
				$ema = false;
			}
			else {
				$ema = true;
			}
		}
		else {
			$ema = true;
		}
		if ($ema && $mob) {
			$message = '';
			$status = true;
		}
		else {
			$status = false;
			if(!$ema && !$mob)
			{
				$message = 'Mobile number and Email address already registered';
			}
			else {
				if (!$ema) {
					$message = 'Email address already registered';
				}
				if (!$mob) {
					$message = 'Mobile number already registered';
				}
			}
		}
		$return = array(
			'status' => $status,
			'message' => $message
		);
		print_r(json_encode($return));
	}
}
?>
