<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Previleges extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!admin()) {
            redirect('admin');
        }
		$this->load->model('admin/Model_previleges','pre');
		$this->load->model('Common');
    }
    public function index() 
    {
        $data['title']   = 'Olymbic | Previleges';
        $modules         = $this->pre->getAllModules();
		$data['modules'] = $modules;
        $this->load->view('admin/previleges/view', $data);
    }

	public function disable($id,$status)
	{   
		$module   = $this->Common->get_details('priority',array('pr_id'=>$id))->row();
		if($status=='v')
		{
			if($module->add=='0' && $module->edit=='0' && $module->block=='0' && $module->delete=='0')
			{
				$array = array(
				             'view' => '0',
				             'all'  => '0'
			              );
			}
			else
			{
				$array = array(
				             'view' => '0',
				             'all'  => '2'
			              );
			}
			
	    }
	    elseif($status=='a')
	    {
	    	if($module->view=='0' && $module->edit=='0' && $module->block=='0' && $module->delete=='0')
			{
	    	    $array = array(
				                 'add' => '0',
				                 'all' => '0'
			                  );
	    	}
	    	else
	    	{
               $array = array(
				                 'add' => '0',
				                 'all' => '2'
			                  );
	    	}    
	    }
	    elseif($status=='e')
	    {
	    	if($module->view=='0' && $module->add=='0' && $module->block=='0' && $module->delete=='0')
			{
		    	$array = array(
					             'edit' => '0',
					             'all'  => '0'
				              );
		    }
		    else
		    {
                $array = array(
					             'edit' => '0',
					             'all'  => '2'
				              );
		    }	
	    }
	    elseif($status=='b')
	    {
	    	if($module->view=='0' && $module->add=='0' && $module->edit=='0' && $module->delete=='0')
	    	{
	    		$array = array(
					             'block' => '0',
					             'all'   => '0'
				              );
	    	}	
	    	else
	    	{
	    		$array = array(
					             'block' => '0',
					             'all'   => '2'
				              );
	    	}	    	
	    }		
	    elseif($status=='d')
	    {
	    	if($module->view=='0' && $module->add=='0' && $module->block=='0' && $module->edit=='0')
	    	{
		    	$array = array(
					             'delete' => '0',
					             'all'    => '0'
				              );
		    }
		    else
		    {
               $array = array(
					             'delete' => '0',
					             'all'    => '2'
				              );
		    } 	
	    }
	    elseif($status=='w')
	    {
	    	$array = array(
				             'all' => '0',
				             'add' => '0',
				             'edit'=> '0',
				             'view'=> '0',
				             'block'=> '0',
				             'delete'=> '0'
			              );
	    }

		if ($this->Common->update('pr_id',$id,'priority',$array)) 
		{   
			$this->session->set_flashdata('alert_message', 'Data blocked..!');
		}
		else 
		{
			$this->session->set_flashdata('alert_message', 'Failed to blocked..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('admin/previleges');
	}

	public function enable($id,$status)
	{
		$module   = $this->Common->get_details('priority',array('pr_id'=>$id))->row();
		if($status=='v')
		{  
			if($module->add=='1' && $module->edit=='1' && $module->block=='1' && $module->delete=='1')
			{
				$array = array(
				                'view'  => '1',
				                'all'   => '1',
			                  );
			}
			else
			{
				$array = array(
				                'view'  => '1',
				                'all'   => '2',
			                  );
			}
			
	    }
	    elseif($status=='a')
	    {   
	    	if($module->view=='1' && $module->edit=='1' && $module->block=='1' && $module->delete=='1')
	    	{
                   $array = array(
				             'add' => '1',
				             'all' => '1',
			              );
	    	}
	    	else
	    	{
	    		$array = array(
				             'add' => '1',
				             'all' => '2',
			              );
	    	}
	    	
	    }
	    elseif($status=='e')
	    {  
	    	if($module->view=='1' && $module->add=='1' && $module->block=='1' && $module->delete=='1')
	    	{
	    		$array = array(
				             'edit' => '1',
				             'all'  => '2',
			              );
	    	}
	    	else
	    	{
	    		$array = array(
				             'edit' => '1',
				             'all'  => '2',
			              );
	    	}
	    	
	    }
	    elseif($status=='b')
	    {  
	    	if($module->view=='1' && $module->add=='1' && $module->edit=='1' && $module->delete=='1')
	    	{
	    		$array = array(
				             'block' => '1',
				             'all'   => '1',
			              );
	    	}
	    	else
	    	{
	    		$array = array(
				             'block' => '1',
				             'all'   => '2',
			              );
	    	}
	    	
	    }		
	    elseif($status=='d')
	    {
	    	if($module->view=='1' && $module->add=='1' && $module->edit=='1' && $module->block=='1')
	    	{
	    	    $array = array(
				             'delete' => '1',
				             'all'    => '1',
			              );
	    	}
	    	else
	    	{
	    		 $array = array(
				             'delete' => '1',
				             'all'    => '2',
			              );
	    	}    
	    }
	    elseif($status=='w')
	    {
	    	$array = array(
				             'all'   => '1',
				             'add'   => '1',
				             'edit'  => '1',
				             'view'  => '1',
				             'block' => '1',
				             'delete'=> '1'
			              );
	    }

		if ($this->Common->update('pr_id',$id,'priority',$array)) 
		{   
			$this->session->set_flashdata('alert_message', 'Data activated..!');
		}
		else 
		{
			$this->session->set_flashdata('alert_message', 'Failed to activate..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('admin/previleges');
	}

	public function status($id,$status)
	{

		$array = array(
			             'all' => '2'
		              );

		if ($this->Common->update('pr_id',$id,'priority',$array)) 
		{
			$this->session->set_flashdata('alert_message', 'Data activated..!');
		}
		else 
		{
			$this->session->set_flashdata('alert_message', 'Failed to activate..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('admin/previleges');
	}

	public function getDetails()
	{
		$cat_id = $this->security->xss_clean($this->input->post('cat_id'));
		$category = $this->category->get_details('categories',array('cat_id' => $cat_id))->row();
		print_r(json_encode($category));
	}
}
?>
