<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Notes extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('admin/Model_notes','notes');
			if (!admin()) {
				redirect('admin');
			}
	}
	public function index()
	{
		$data['title'] = 'Notes';
		$data['notes'] = $this->notes->getNotes();
		$this->load->view('admin/notes/view',$data);
	}
	public function add()
	{
		$this->load->view('admin/notes/add');
	}
	public function addNote()
	{
		$title = $this->security->xss_clean($this->input->post('title'));
		$description = $this->security->xss_clean($this->input->post('description'));
		$title_arabic = $this->security->xss_clean($this->input->post('title_arabic'));
		$description_arabic = $this->security->xss_clean($this->input->post('description_arabic'));
		$start_time = $this->security->xss_clean($this->input->post('start_time'));
		$end_time = $this->security->xss_clean($this->input->post('end_time'));
		$type = $this->security->xss_clean($this->input->post('type'));

		$start_time = str_replace("/","-",$start_time);
		$end_time = str_replace("/","-",$end_time);
		$start = date('Y-m-d H:i:s',strtotime($start_time));
		$end = date('Y-m-d H:i:s',strtotime($end_time));

		$array = [
			'title' => $title,
			'description' => $description,
			'title_arabic' => $title_arabic,
			'description_arabic' => $description_arabic,
			'start_time' => $start,
			'end_time' => $end,
			'type' => $type
		];

		if ($this->notes->insert('notes',$array)) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'New note added..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add note..!');
		}
		redirect('admin/notes');
	}
	public function editNote()
	{
		$note_id = $this->security->xss_clean($this->input->post('note_id'));
		$get = $this->notes->get_details('notes',array('note_id' => $note_id));
		if ($get->num_rows() > 0) {
			$title = $this->security->xss_clean($this->input->post('title'));
			$description = $this->security->xss_clean($this->input->post('description'));
			$title_arabic = $this->security->xss_clean($this->input->post('title_arabic'));
			$description_arabic = $this->security->xss_clean($this->input->post('description_arabic'));
			$start_time = $this->security->xss_clean($this->input->post('start_time'));
			$end_time = $this->security->xss_clean($this->input->post('end_time'));
			$type = $this->security->xss_clean($this->input->post('type'));

			$start_time = str_replace("/","-",$start_time);
			$end_time = str_replace("/","-",$end_time);
			$start = date('Y-m-d H:i:s',strtotime($start_time));
			$end = date('Y-m-d H:i:s',strtotime($end_time));

			$array = [
				'title' => $title,
				'description' => $description,
				'title_arabic' => $title_arabic,
				'description_arabic' => $description_arabic,
				'start_time' => $start,
				'end_time' => $end,
				'type' => $type
			];

			$this->notes->update('note_id',$note_id,'notes',$array);

			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Note updated..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load note..!');
		}
		redirect('admin/notes');
	}

	public function getNote()
	{
		$note_id = $this->security->xss_clean($this->input->post('note_id'));
		$note = $this->notes->get_details('notes',array('note_id' => $note_id))->row();
		$note->start_time = date('d/m/Y h:i A',strtotime($note->start_time));
		$note->end_time = date('d/m/Y h:i A',strtotime($note->end_time));
		print_r(json_encode($note));
	}

	public function delete($note_id)
	{
		$get = $this->notes->get_details('notes',array('note_id' => $note_id));
		if ($get->num_rows() > 0) {
			if($this->notes->delete('notes',array('note_id' => $note_id)))
			{
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Note deleted successfully..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to remove Note..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load Note..!');
		}
		redirect('admin/notes');
	}
}
?>
