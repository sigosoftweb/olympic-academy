<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Sports extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!admin()) {
            redirect('admin');
        }
		$this->load->model('admin/Model_sports','sport');
    }
    public function index() {
        $data['title'] = 'Olymbic | sports';
		$data['sports'] = $this->sport->getAllsports();
        $this->load->view('admin/sports/view', $data);
    }
	public function add()
	{
		$sport = $this->security->xss_clean($this->input->post('sport'));
		if ($this->sport->insert('sports',array('sport' => $sport))) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'New sport added..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add sport..!');
		}
		redirect('admin/sports');
	}
	public function edit()
	{
		$sports_id = $this->security->xss_clean($this->input->post('sports_id'));
		$sport = $this->security->xss_clean($this->input->post('sport'));
		if ($this->sport->update('sports_id',$sports_id,'sports',array('sport' => $sport))) {
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Logged in Successfully..!');
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add sport..!');
		}
		redirect('admin/sports');
	}
	public function status($sports_id,$status)
	{
		$array = array(
			'status' => $status
		);
		$this->sport->update('sports_id',$sports_id,'sports',$array);
		if ($status == '1') {
			$this->session->set_flashdata('alert_message', 'sport activated..!');
		}
		else {
			$this->session->set_flashdata('alert_message', 'sport deactivated..!');
		}
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_title', 'Success');
		redirect('admin/sports');
	}
	public function getDetails()
	{
		$sports_id = $this->security->xss_clean($this->input->post('sports_id'));
		$sport = $this->sport->get_details('sports',array('sports_id' => $sports_id))->row();
		print_r(json_encode($sport));
	}
}
?>
