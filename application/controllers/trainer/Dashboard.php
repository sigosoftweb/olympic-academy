<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!teacher()) {
            redirect('teacher');
        }
    }
    public function index() {
        $data['title'] = 'Olymbic | Dashboard';
        $this->load->view('teacher/dashboard/view', $data);
    }
}
?>
