<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Exams extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!teacher()) {
            redirect('teacher');
        }
        $this->load->model('teacher/Model_exams', 'exam');
    }
    public function add($course_id) {
        $data['title'] = 'Add exam';
        $get = $this->exam->get_details('courses', array('course_id' => $course_id));
        if ($get->num_rows() > 0) {
            $data['course'] = $get->row();
            $data['sections'] = $this->exam->getSections($course_id);
            $data['batches'] = $this->exam->getBatches($course_id);
            $this->load->view('teacher/exams/add', $data);
        } else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Error');
            $this->session->set_flashdata('alert_message', 'Failed to load course..!');
            redirect('trainer/courses');
        }
    }
    public function edit($exam_id)
    {
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
            $date = date('Y-m-d');
            $data['exam'] = $exam = $get->row();
            $data['course'] = $this->exam->get_details('courses', array('course_id' => $exam->course_id))->row();
            $data['sections'] = $this->exam->getSections($exam->course_id);
            $data['batches'] = $this->exam->getBatches($exam->course_id);
            $data['batch_name'] = '';
            $data['section_name'] = '';
            $data['question_number'] = $this->exam->get_details('questions',array('exam_id'=>$exam_id))->num_rows();
            $check = $this->exam->get_details('students_question_answers',array('exam_id' => $exam_id))->num_rows();
            if ($check > 0) {
                $data['status'] = false;
            }
            else {
                $data['status'] = true;
            }
            $this->load->view('teacher/exams/edit',$data);
        }
        else {
            redirect('trainer/exams');
        }
    }
    public function addExam() {
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
        $name = $this->security->xss_clean($this->input->post('name'));
        $cb_id = $this->security->xss_clean($this->input->post('cb_id'));
		$cs_id = $this->security->xss_clean($this->input->post('cs_id'));
        $no_of_questions = $this->security->xss_clean($this->input->post('no_of_questions'));
        $exam_time = $this->security->xss_clean($this->input->post('exam_time'));
        $time = $this->security->xss_clean($this->input->post('time'));
        $instructions = $this->security->xss_clean($this->input->post('instructions'));
		if ($cb_id == '0' && $cs_id == '0') {
			$type = 'course';
		}
		else {
			if ($cb_id != '0') {
				$type = 'batch';
			}
			else {
				$type = 'section';
			}
		}
        $array = ['exam_name' => $name, 'type' => $type, 'course_id' => $course_id, 'cb_id' => $cb_id, 'cs_id' => $cs_id, 'no_questions' => $no_of_questions, 'exam_time' => $exam_time, 'time_status' => $time ];
        if ($time == '1') {
            $from = $this->security->xss_clean($this->input->post('from_time'));
			$from = str_replace("/","-",$from);
			$from = date('Y-m-d H:i:s',strtotime($from));
            $end = date('Y-m-d H:i:s', strtotime($from . ' +' . $exam_time . ' minutes'));
            $array['from_time'] = $from;
            $array['to_time'] = $end;
        }
        if ($exam_id = $this->exam->insert('exams', $array)) {
            foreach ($instructions as $instruction) {
                $instruction_array = ['exam_id' => $exam_id, 'instruction' => $instruction, 'timestamp' => date('Y-m-d H:i:s') ];
                $this->exam->insert('exam_instructions', $instruction_array);
            }
            $this->session->set_flashdata('alert_type', 'success');
            $this->session->set_flashdata('alert_title', 'Success');
            $this->session->set_flashdata('alert_message', 'Exam added');
            redirect('trainer/exams/questions/' . $exam_id);
        } else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to add exam..!');
            redirect('trainer/exams');
        }
    }
    public function editExam() {
		$exam_id = $this->security->xss_clean($this->input->post('exam_id'));
		$question_number  = $this->security->xss_clean($this->input->post('question_number'));
		
        $name = $this->security->xss_clean($this->input->post('name'));
        $cb_id = $this->security->xss_clean($this->input->post('cb_id'));
		$cs_id = $this->security->xss_clean($this->input->post('cs_id'));
        $no_of_questions = $this->security->xss_clean($this->input->post('no_of_questions'));
        $exam_time = $this->security->xss_clean($this->input->post('exam_time'));
        $time = $this->security->xss_clean($this->input->post('time'));
        $instructions = $this->security->xss_clean($this->input->post('instructions'));
		if ($cb_id == '0' && $cs_id == '0') {
			$type = 'course';
		}
		else {
			if ($cb_id != '0') {
				$type = 'batch';
			}
			else {
				$type = 'section';
			}
		}
        $array = ['exam_name' => $name, 'type' => $type, 'cb_id' => $cb_id, 'cs_id' => $cs_id, 'no_questions' => $no_of_questions, 'exam_time' => $exam_time, 'time_status' => $time ];
        if ($time == '1') {
            $from = $this->security->xss_clean($this->input->post('from_time'));
			$from = str_replace("/","-",$from);
			$from = date('Y-m-d H:i:s',strtotime($from));
            $end = date('Y-m-d H:i:s', strtotime($from . ' +' . $exam_time . ' minutes'));
            $array['from_time'] = $from;
            $array['to_time'] = $end;
        }
        else{
            $array['time_status'] = '0';
            $array['from_time'] = '';
            $array['to_time'] = '';
        }
        if ($question_number < $no_of_questions) {
            $array['registration_status'] = '0';
            $array['exam_status'] = '0';
        }
        if ($this->exam->update('exam_id',$exam_id,'exams', $array)) {
            $this->session->set_flashdata('alert_type', 'success');
            $this->session->set_flashdata('alert_title', 'Success');
            $this->session->set_flashdata('alert_message', 'Exam added');
            redirect('trainer/exams/questions/' . $exam_id);
        } else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to add exam..!');
            redirect('trainer/courses');
        }
    }
	public function questions($exam_id)
    {
        $data['title'] = 'Add question';
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
            $data['questions'] = $question = $this->exam->get_details('questions',array('exam_id' => $exam_id))->num_rows();
            $data['exam'] = $exam = $get->row();
            if ($question == $exam->no_questions) {
                $array = [
                    'registration_status' => '1',
                    'exam_status' => '1'
                ];
                $this->exam->update('exam_id',$exam_id,'exams',$array);
                redirect('trainer/courses/details/7/' . $exam->course_id);
            }
            else {
                $this->load->view('teacher/exams/questions',$data);
            }
        }
        else {
            redirect('trainer/exams');
        }
    }
    public function addQ($param=0)
    {
        $exam_id  = $this->security->xss_clean($this->input->post('exam_id'));
		
		$formula_question  = $this->input->post('formula_question');
        $formula_a  = $this->input->post('formula_a');
        $formula_b  = $this->input->post('formula_b');
        $formula_c  = $this->input->post('formula_c');
        $formula_d  = $this->input->post('formula_d');
		$formula_e  = $this->input->post('formula_e');
		
		$correct  = $this->input->post('correct');
		$negative  = $this->input->post('negative');
		
        $answer  = $this->security->xss_clean($this->input->post('answer'));

        $array = [
			'formula_question' => $formula_question,
            'formula_a' => $formula_a,
            'formula_b' => $formula_b,
            'formula_c' => $formula_c,
            'formula_d' => $formula_d,
			'formula_e' => $formula_e,
            'ans' => $answer,
            'correct' => $correct,
            'negative' => $negative,
            'exam_id' => $exam_id
        ];

		$image =  $_FILES['image'];
		 if ($image['size'] > 0) {
		     
            $tar = "uploads/questions/";
            $rand = date('Ymd') . mt_rand(1001, 9999);
            $tar_file = $tar . $rand . '.png';
            move_uploaded_file($image['tmp_name'], $tar_file);

			$array['attachment'] = '1';
			$array['image'] = $tar_file;
		}

        if ($this->exam->insert('questions',$array)) {
            $this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Question added');
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add exam..!');
        }
        if ($param == '1') {
            redirect('trainer/exams/question/' . $exam_id);
        }
        else {
            redirect('trainer/exams/questions/' . $exam_id);
        }
    }
    public function get()
    {
		$course_id = $this->input->post('course_id');
        $result = $this->exam->make_datatables($course_id);
        $data   = array();
        foreach ($result as $res) {
            $sub_array   = array();
            $sub_array[] = $res->exam_name;
            $sub_array[] = $res->batch_name;
			$sub_array[] = $res->section_title;
            $sub_array[] = $res->no_questions;
            $sub_array[] = $res->exam_time .' minutes';

            if ($res->time_status == '1') {
                $sub_array[] = 'From : ' . date('d/m/Y h:i A',strtotime($res->from_time)) . '<br>To : ' . date('d/m/Y h:i A',strtotime($res->to_time));
            }
            else {
                $sub_array[] = 'Time not set';
            }
            if($res->registration_status != '1')
            {
                $sub_array[] = 'Exam registration is not completed<br><a class="btn btn-primary btn-xs"  href="' . site_url('trainer/exams/questions/' . $res->exam_id) . '">Complete</a>';
            }
            else
            {
                $sub_array[] = '<a class="btn btn-primary btn-xs"  href="' . site_url('trainer/exams/edit/' . $res->exam_id) . '">Edit</a>';
            }
            $sub_array[] = '<a class="btn btn-primary btn-xs"  href="' . site_url('trainer/exams/question/' . $res->exam_id) . '">Questions</a>';
            $sub_array[] = '<a class="btn btn-primary btn-xs"  href="' . site_url('trainer/exams/instructions/' . $res->exam_id) . '">Instruction</a>';
            if ($res->exam_status == '1') {
                $sub_array[] = '<a class="btn btn-danger btn-xs"  href="' . site_url('trainer/exams/status/' . $res->exam_id .'/0') . '">Block</a>';
            }
            else {
                $sub_array[] = '<a class="btn btn-success btn-xs"  href="' . site_url('trainer/exams/status/' . $res->exam_id .'/1') . '">Activate</a>';
            }
            $sub_array[] = '<a class="btn btn-danger btn-xs" onclick="return confirm(`Are you sure to delete?`)"  href="' . site_url('trainer/exams/deleteExam/' . $res->exam_id) . '">Delete</a>';

            $data[]      = $sub_array;
        }
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $this->exam->get_all_data($course_id),
            "recordsFiltered" => $this->exam->get_filtered_data($course_id),
            "data" => $data
        );
        echo json_encode($output);
    }
	public function status($exam_id,$status)
    {
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
            $exam = $get->row();
            if ($exam->registration_status == '0') {
                $this->session->set_flashdata('alert_type', 'error');
    			$this->session->set_flashdata('alert_title', 'Failed');
    			$this->session->set_flashdata('alert_message', 'Registration of exam still pending');
            }
            else {
                $array = [
                    'exam_status' => $status
                ];
                $this->exam->update('exam_id',$exam_id,'exams',$array);
                $this->session->set_flashdata('alert_type', 'success');
    			$this->session->set_flashdata('alert_title', 'Success');
                if ($status == '0') {
                    $this->session->set_flashdata('alert_message', 'Exam deactivated..!');
                }
                else {
                    $this->session->set_flashdata('alert_message', 'Exam activated..!');
                }
            }
			redirect('trainer/courses/details/7/' . $exam->course_id);
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load exam..!');
			redirect('trainer/courses');
        }
    }
	public function instructions($exam_id)
    {
		$data['title'] = 'Exam instructions';
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
            $data['instructions']  = $this->exam->get_details('exam_instructions',array('exam_id' => $exam_id))->result();
            $data['exam']  = $get->row();
            $this->load->view('teacher/exams/instructions',$data);
        }
        else {
            redirect('trainer/courses');
        }
    }
	public function deleteExam($exam_id)
    {
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
			$exam = $get->row();
            $this->exam->delete('exams',array('exam_id' => $exam_id));
            $this->exam->delete('questions',array('exam_id' => $exam_id));
            // $this->exam->delete('students_question_answers',array('exam_id' => $exam_id));

            $this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Exam and all data related to this exam was deleted');
            redirect('trainer/courses/details/7/' . $exam->course_id);
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to delete exam..!');
            redirect('trainer/courses');
        }
    }
	public function question($exam_id)
    {
        $get = $this->exam->get_details('exams',array('exam_id' => $exam_id));
        if ($get->num_rows() > 0) {
            $data['questions'] = $question = $this->exam->get_details('questions',array('exam_id' => $exam_id))->result();
            $data['exam'] = $exam = $get->row();
            $this->load->view('teacher/exams/view_questions',$data);
        }
        else {
            redirect('trainer/exams');
        }
    }
    public function deleteQuestion($que_id)
    {
        $get = $this->exam->get_details('questions',array('que_id' => $que_id));
        if ($get->num_rows() > 0) {
            $exam_id = $get->row()->exam_id;
            $this->exam->delete('questions',array('que_id' => $que_id));
            // $this->exam->delete('students_question_answers',array('que_id' => $que_id));
            $array = [
                'registration_status' => '0',
                'exam_status' => '0'
            ];
            $this->exam->update('exam_id',$exam_id,'exams',$array);
            $this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Question deleted from the list..!');
            redirect('trainer/exams/question/' . $exam_id);
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to remove question..!');
            redirect('trainer/courses');
        }
    }
    public function editQues($que_id)
	{
		$get = $this->exam->get_details('questions',array('que_id' => $que_id));
		if ($get->num_rows() > 0) {
			$data['que'] = $get->row();
			$this->load->view('teacher/exams/question_edit',$data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to add exam..!');
			redirect('trainer/exams');
		}
	}
	public function editQuestion()
    {
        $exam_id  = $this->security->xss_clean($this->input->post('exam_id'));
        $que_id  = $this->security->xss_clean($this->input->post('que_id'));
		
		$formula_question  = $this->input->post('formula_question');
        $formula_a  = $this->input->post('formula_a');
        $formula_b  = $this->input->post('formula_b');
        $formula_c  = $this->input->post('formula_c');
        $formula_d  = $this->input->post('formula_d');
		$formula_e  = $this->input->post('formula_e');
		
		$correct  = $this->input->post('correct');
		$negative  = $this->input->post('negative');
		
        $answer  = $this->security->xss_clean($this->input->post('answer'));

        $array = [
            'exam_id' => $exam_id,
            'que_id' => $que_id,
			'formula_question' => $formula_question,
            'formula_a' => $formula_a,
            'formula_b' => $formula_b,
            'formula_c' => $formula_c,
            'formula_d' => $formula_d,
			'formula_e' => $formula_e,
			'correct' => $correct,
			'negative' => $negative,
            'ans' => $answer
        ];


        $image =  $_FILES['image'];
        if ($image['size'] > 0) 
        {
            $tar = "uploads/questions/";
            $rand = date('Ymd') . mt_rand(1001, 9999);
            $tar_file = $tar . $rand . '.png';
            move_uploaded_file($image['tmp_name'], $tar_file);
        
        	$array['attachment'] = '1';
        	$array['image'] = $tar_file;
        			
        	$old_image = $this->input->post('old_image');
        	if ($old_image != '') {
        		$delete_path = FCPATH . $old_image;
        		unlink($old_image);
        	}
		}

        if ($this->exam->update('que_id',$que_id,'questions',$array)) {
            $this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Question updated');
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to update question..!');
        }
        redirect('trainer/exams/question/' . $exam_id);
    }

    public function addInstruction()
    {
        $trainer    = $this->session->userdata['teacher'];
        $trainer_id = $trainer['user_id'];

        $exam_id    = $this->security->xss_clean($this->input->post('exam_id'));
        $instruction= $this->security->xss_clean($this->input->post('instruction'));

        $array      = [
                         'exam_id'    => $exam_id,
                         'instruction'=> $instruction,
                         'added_by'   => 't',
                         'trainer_id' => $trainer_id
                      ];
        if ($this->exam->insert('exam_instructions',$array)) {
            $this->session->set_flashdata('alert_type', 'success');
            $this->session->set_flashdata('alert_title', 'Success');
            $this->session->set_flashdata('alert_message', 'Instruction added...');
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to add instruction..!');
        }
        redirect('trainer/exams/instructions/' . $exam_id);              
    }

    public function editInstruction()
    {
        $trainer    = $this->session->userdata['teacher'];
        $trainer_id = $trainer['user_id'];

        $inst_id    = $this->security->xss_clean($this->input->post('inst_id'));
        $exam_id    = $this->security->xss_clean($this->input->post('exam_id'));
        $instruction= $this->security->xss_clean($this->input->post('instruction'));

        $array      = [
                         'instruction'=> $instruction,
                         'edited_by'  => 't',
                         'trainer_id' => $trainer_id
                      ];
        if ($this->exam->update('ei_id',$inst_id,'exam_instructions',$array)) {
            $this->session->set_flashdata('alert_type', 'success');
            $this->session->set_flashdata('alert_title', 'Success');
            $this->session->set_flashdata('alert_message', 'Instruction updated...');
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to update instruction..!');
        }
        redirect('trainer/exams/instructions/' . $exam_id);              
    }
    public function getInstructionDetails()
    {
        $inst_id = $this->security->xss_clean($this->input->post('inst_id'));
        $data = $this->exam->get_details('exam_instructions',array('ei_id' => $inst_id))->row();
        print_r(json_encode($data));
    }

    public function deleteInstruction($id)
    {

        $instruction= $this->exam->get_details('exam_instructions',array('ei_id'=>$id))->row();
        $exam_id    = $instruction->exam_id;

        if ($this->exam->delete('exam_instructions',array('ei_id'=>$id))) {
            $this->session->set_flashdata('alert_type', 'success');
            $this->session->set_flashdata('alert_title', 'Success');
            $this->session->set_flashdata('alert_message', 'Instruction deleted...');
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to delete instruction..!');
        }
        redirect('trainer/exams/instructions/' . $exam_id);              
    }
}
?>
