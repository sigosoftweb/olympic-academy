<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('Common');
			if (!teacher()) {
				redirect('teacher');
			}
	}
	public function index()
	{   
		$trainer    = $this->session->userdata['teacher'];
		$trainer_id = $trainer['user_id'];

        $data['title'] = 'Olymbic | Profile';
		$data['profile'] = $this->Common->get_details('trainers',array('trainer_id'=>$trainer_id))->row();
		$this->load->view('teacher/profile/view',$data);
	}

	public function edit()
	{
		$trainer    = $this->session->userdata['teacher'];
		$trainer_id = $trainer['user_id'];

        $data['title'] = 'Olymbic | Profile';
		$data['trainer'] = $this->Common->get_details('trainers',array('trainer_id'=>$trainer_id))->row();
		$this->load->view('teacher/profile/edit',$data);
	}

	public function editData()
	{
		$trainer_id = $this->security->xss_clean($this->input->post('trainer_id'));
		$get   = $this->Common->get_details('trainers',array('trainer_id' => $trainer_id));
		if ($get->num_rows() > 0) {
			$trainer = $get->row();
			$array = array(
				'trainer_name_english' => $this->security->xss_clean($this->input->post('trainer_name_english')),
				'trainer_name_arabic' => $this->security->xss_clean($this->input->post('trainer_name_arabic')),
				'trainer_cpr' => $this->security->xss_clean($this->input->post('trainer_cpr')),
				'trainer_mobile' => $this->security->xss_clean($this->input->post('trainer_mobile')),
				'trainer_email' => $this->security->xss_clean($this->input->post('trainer_email')),
				'trainer_education' => $this->security->xss_clean($this->input->post('trainer_education')),
				'trainer_experience' => $this->security->xss_clean($this->input->post('trainer_experience')),
				'trainer_contact' => $this->security->xss_clean($this->input->post('trainer_contact')),
			);

			$file = $_FILES['image'];
			if ($file['size'] > 0) {
				$tar = "uploads/trainers/";
				$rand=date('Ymd').mt_rand(1001,9999);
				$tar_file = $tar . $rand . basename($file['name']);
				if(move_uploaded_file($file["tmp_name"], $tar_file))
				{
					$array['trainer_image'] = $tar_file;
					if ($trainer->trainer_image != '') {
						$unliink = FCPATH . $trainer->trainer_image;
						unlink($unliink);
					}
				}
			}

			if ($this->Common->update('trainer_id',$trainer_id,'trainers',$array)) {
				$this->session->set_flashdata('alert_type', 'success');
				$this->session->set_flashdata('alert_title', 'Success');
				$this->session->set_flashdata('alert_message', 'Profile updated..!');
			}
			else {
				$this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Failed to update profile..!');
			}
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to load profile..!');
		}
		redirect('trainer/profile');
	}

	public function checkPassword()
	{
		$id            = $_POST['id'];
		$old_password  = md5($_POST['password']);
		$data          = $this->Common->get_details('trainers',array('trainer_id' => $id))->row();
		$data->old_password = $old_password;
		
		print_r(json_encode($data));
	}
	
	public function changePassword()
	{
		$user_id    = $this->security->xss_clean($this->input->post('user_id'));
		$password   = md5($this->input->post('password'));
// 		print_r($user_id);
		$password_array    = [
        			          'trainer_password'   => $password
        		             ];
	   if($this->Common->update('trainer_id',$user_id,'trainers',$password_array))
	   {
	        $this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Password changed successfully..!');
	   }
	   else
	   {
	        $this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Failed to update password..!');
	   }
	   redirect('trainer/dashboard');      
	}
}
?>
