<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Search extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('site/english/Model_Courses','courses');  
        $this->load->model('site/english/Model_Students','students');
      
    }

    public function index() {

        $search_str = $this->security->xss_clean($this->input->get('q'));


        $data['courses'] = $this->courses->search($search_str);   

        if(isStudent()) {
            $user = $this->session->userdata['olympicUser'];
            $user_id = $user['userID'];
            $data['courseIDs'] = $this->students->get_subscribedCourseIDs($user_id);
        }       

    	
        $this->load->view('site/english/search-results', $data);
    }



    
}

?>