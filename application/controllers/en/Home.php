<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('site/english/Model_Slider', 'slider');
        $this->load->model('site/english/Model_Courses','courses');
        $this->load->model('site/english/Model_Categories','categories');
        $this->load->model('site/english/Model_Events','events');
        $this->load->model('site/english/Model_Students','students');
        $this->load->model('site/english/Model_Trainers','trainers');
    }

    public function index() {
        $data['slides'] = $this->slider->get_slides();
        $data['slides_count'] = $this->slider->get_slidesCount();

    	//$data['popular_courses'] = $this->courses->get_popularCourses(8);
        $data['courses'] = $this->courses->get_Courses(6);

        $data['categories'] = $this->categories->get_categories_course_count();

        //$data['events'] = $this->events->get_upcomingEvents(4);
        $data['events'] = $this->events->get_Events(4);

        $data['trainers'] = $this->trainers->get_Trainers(8);
        
        if(isStudent()) {
            $user = $this->session->userdata['olympicUser'];
            $user_id = $user['userID']; 
            $count = $this->students->get_subscribedCourseCount($user_id);
            //print_r($count);
            //exit;
            if($count->c_count) {
                $data['related_courses'] = $this->students->get_relatedCourses($user_id, 3);
                $data['courseIDs'] = $this->students->get_subscribedCourseIDs($user_id);
                
            }
            
        }

        
        
    	$this->load->view('site/english/home',$data);


    }

    


    

    
}

?>