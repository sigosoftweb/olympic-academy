<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payment extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Common');
    }
    public function postURLOLD()
    {
        $transactionJsonRaw = file_get_contents('php://input');
        
        $headers = getallheaders();
        $header = json_encode($headers);
        
		$this->Common->insert('tap_payment',array('payment' => $transactionJsonRaw , 'header' => $header));
    }
    public function postURL()
    {
        $transactionJsonRaw = file_get_contents('php://input');
        $headers = getallheaders();
        
        $array = json_decode($transactionJsonRaw,True);
        
        if($this->validateResponse($array))
        {
            if($this->validateHash($headers['hashstring'],$array))
            {
                $transaction = array(
                    'amount' => $array['amount'],
                    'currency' => $array['currency'],
                    'payment_reference' => $array['reference']['payment'],
                    'gateway_reference' => $array['reference']['gateway'],
                    'response_code' => $array['response']['code'],
                    'error_message' => $array['response']['message'],
                    'tap_id' => $array['id'],
                );
                $this->Common->insert('transactions',$transaction);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    public function validateResponse($response)
    {
        if(!is_array($response) || !array_key_exists('id', $response) || !array_key_exists('amount', $response) || !array_key_exists('currency', $response) || !array_key_exists('reference', $response) ||
            !array_key_exists('payment', $response['reference']) || !array_key_exists('gateway', $response['reference']) || !array_key_exists('status', $response) || !array_key_exists('transaction', $response) || 
            !array_key_exists('created', $response['transaction']) || !array_key_exists('response', $response) || !array_key_exists('code', $response['response'])
        )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public function validateHash($hash,$response)
    {
        $SecretAPIKey = 'sk_test_i6kdqaLznIfuZcPD0SoHsKMG';
        $amount = $this->formatAmount($response['amount']);
        $postedDataHash = "x_id{$response['id']}x_amount{$amount}x_currency{$response['currency']}x_gateway_reference{$response['reference']['gateway']}x_payment_reference{$response['reference']['payment']}x_status{$response['status']}x_created{$response['transaction']['created']}";
        return $hash == hash_hmac('sha256', $postedDataHash, $SecretAPIKey);
    }
    public function formatAmount($amount)
    {
        return number_format((float)$amount, 3, '.', '');
    }
}

?>
