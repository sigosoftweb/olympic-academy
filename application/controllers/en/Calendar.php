<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('site/english/Model_Courses','courses');
			
	}
	public function index()	{
		
		
		$prefs = array(
                'start_day'    => 'sunday',
                'month_type'   => 'long',
                'day_type'     => 'short',
                
                'show_other_days'=> FALSE,
                
                'template' => array(
                        'table_open'           => '<table class="table table-bordered">',
                        //'cal_cell_start'       => '<td class="day">',
                        //'cal_cell_start_today' => '<td class="today">'
                        'heading_row_start' => '<tr class="month">',
                        'week_row_start' => '<tr class="week">',
                        'cal_row_start' => '<tr class="day">',
                        'cal_cell_content' => '<div>{content}</div>',
                        'cal_cell_no_content' => '<div>{day}</div>',
                        'cal_cell_content_today' => '<div class="highlight">{content}</div>',
                        'cal_cell_no_content_today' => '<div class="highlight">{day}</div>',
                        'cal_cell_start_other' => '<td class="other-month"><div>',
                	)
                );
				
		


		$this->load->library('calendar', $prefs);

		$year = date('Y');
        $month = date('n');
        //$data['calendar'] = $this->display_calendar($year, $month);
        //$data['current_calendar'] = array();
        $data['current_calendar']['year'] = $year;
        //$data['current_calendar']['month'] = $month;

        for ($i=1; $i <=12 ; $i++) { 
            $data['calendar'][$i] = $this->display_calendar($year, $i);
        }
        
		$this->load->view('site/english/year-calendar', $data);
	}


	public function display_calendar($year, $month) {
       
           
        $calendar_data = array();
        

        /*---------------------------sections---------------*/
        $courses = $this->courses->get_allCoursesMonth($year, $month);

        foreach ($courses as $course) {

            $day = intval(date("d", strtotime($course->course_starting_date)));
            /*$str = explode(" ", $course->course_title);
            if(array_key_exists($day, $calendar_data)) {
                $calendar_data[$day] .= '<p class="section-green" data-toggle="tooltip" title="' . $course->course_title . '"><a href="'. base_url('en/courses/details/'). $course->course_id . '"><i class="icon_easel"></i>' . substr($str[0], 0 ,8)  . '..' . '</a></p>';
            }
            else {
                $calendar_data[$day] = '<span>'.$day.'</span><p class="section-green" data-toggle="tooltip" title="' . $course->course_title . '"><a href="'. base_url('en/courses/details/'). $course->course_id . '"><i class="icon_easel"></i>' . substr($str[0], 0 ,8) . '..' . '</a></p>';
            }*/

            if(array_key_exists($day, $calendar_data)) {
                $calendar_data[$day] .= '<p class="section-green" data-toggle="tooltip" title="' . $course->course_title . '"><a href="'. base_url('en/courses/details/'). $course->course_id . '"><i class="icon_easel"></i></a></p>';
            }
            else {
                $calendar_data[$day] = '<span>'.$day.'</span><p class="section-green" data-toggle="tooltip" title="' . $course->course_title . '"><a href="'. base_url('en/courses/details/'). $course->course_id . '"><i class="icon_easel"></i></a></p>';
            }                    
        }

            //print_r($calendar_data);
            //exit;

        

        $calendar = $this->calendar->generate($year, $month, $calendar_data);
        

        return $calendar;
        
    }

    public function ajax_calendar() {
        $year = $this->security->xss_clean($this->input->post('year'));
        $prefs = array(
                'start_day'    => 'sunday',
                'month_type'   => 'long',
                'day_type'     => 'short',
                
                'show_other_days'=> TRUE,
                
                'template' => array(
                        'table_open'           => '<table class="table table-bordered">',
                        //'cal_cell_start'       => '<td class="day">',
                        //'cal_cell_start_today' => '<td class="today">'
                        'heading_row_start' => '<tr class="month">',
                        'week_row_start' => '<tr class="week">',
                        'cal_row_start' => '<tr class="day">',
                        'cal_cell_content' => '<div>{content}</div>',
                        'cal_cell_no_content' => '<div>{day}</div>',
                        'cal_cell_content_today' => '<div class="highlight">{content}</div>',
                        'cal_cell_no_content_today' => '<div class="highlight">{day}</div>',
                        'cal_cell_start_other' => '<td class="other-month"><div>',
                )
                );


        $this->load->library('calendar', $prefs);

        $data['current_calendar']['year'] = $year;

        for ($i=1; $i <=12 ; $i++) { 
            $data['calendar'][$i] = $this->display_calendar($year, $i);
        }        

        $result = $this->load->view('site/english/ajax-year-calendar-template', $data, TRUE);

        echo $result;
    }
}
?>