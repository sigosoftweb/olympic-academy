<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('Common');
		$this->load->model('site/english/Model_Contact', 'contact');				

	}

	public function index()
	{
		$data['contact'] = $this->contact->get_contact();
		$this->load->view('site/english/contact-us', $data);
	}

	public function thankyou()
	{
		$this->load->view('site/english/thank-you');
	}

	public function test() {
		$this->load->view('site/english/mail-template');
	}

	public function sendmail() {

		$array = array(
			'firstname' => $this->security->xss_clean($this->input->post('firstname')),
			'lastname' => $this->security->xss_clean($this->input->post('lastname')),
			'mobile' => $this->security->xss_clean($this->input->post('mobile')),
			'email' => $this->security->xss_clean($this->input->post('email')),
			'message' => $this->security->xss_clean($this->input->post('message'))
		);
		

		if ($contact_id = $this->Common->insert('contact',$array)) {

			$config = array(
				            'mailtype' => 'html',
				            'charset'  => 'utf-8',
				            'priority' => '1'
				        );
	        $this->load->library('email');
	        $this->email->initialize($config);
	        $this->email->set_newline("\r\n");
	        $this->email->set_crlf("\r\n");

	        $this->email->from($array['email']); 
	        

	        $this->email->to('boa@boc.bh'); 
	        $subject = "Enquiry from website";
	        $this->email->subject($subject);

	        $data['firstname'] = $array['firstname'];
	        $data['lastname'] = $array['lastname'];
	        $data['mobile'] = $array['mobile'];
	        $data['email'] = $array['email'];
	        $data['message'] = $array['message'];

	        $body = $this->load->view('site/english/contact-mail-template',$data,TRUE);
	        $this->email->message($body); 

	        if ($this->email->send()) {
	        	redirect('en/contact/thankyou');
	            
	        } else {
	            $this->session->set_flashdata('alert_type', 'error');
				$this->session->set_flashdata('alert_title', 'Failed');
				$this->session->set_flashdata('alert_message', 'Error!');

	            redirect('en/contact');
	        }

		}

		else {
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Error!');
			redirect('en/contact');
		}

		
        
	}
}
?>