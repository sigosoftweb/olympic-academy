<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trainers extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('site/english/Model_Trainers','trainers');
	}

	public function index() {
		$data['trainers'] = $this->trainers->get_Trainers();
		$this->load->view('site/english/trainers', $data);
	}

	public function details($id) {
		$data['trainer'] = $this->trainers->get_trainerDetails($id);
		if(!empty($data['trainer'])) {
			
			$this->load->view('site/english/trainer-details', $data);
		}
		else {
			$this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Loading Failed!');
            redirect('en/trainers');
		}
	}

	
}
?>