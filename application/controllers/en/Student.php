<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Student extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (!isStudent()) {
            $this->session->set_userdata('last_page', current_url());

            redirect('en/login');
        }


        $this->load->model('Common');
        $this->load->model('site/english/Model_Courses','courses');
        $this->load->model('site/english/Model_Categories','categories');
        $this->load->model('site/english/Model_Students','students');
    }

    public function index() {  	


    }

    public function profile() {
        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];

        $result = $this->students->get_details('students',array('stu_id' => $user_id));

        if ($result->num_rows() > 0) {
            $data['student'] = $result->row();
            $data['notice_count'] = $this->students->get_noticeCount($user_id);
            $data['subscribed_courses'] = $this->students->get_subscribedCourseList($user_id);
            $data['timezones'] =  DateTimeZone::listIdentifiers(DateTimeZone::ALL);
            $this->load->view('site/english/profile-student',$data);
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to load your profile!');
            redirect('en/login');
        }

    }

    public function edit_profile() {

        $stu_id = $this->security->xss_clean($this->input->post('stu_id'));
        $get = $this->students->get_details('students',array('stu_id' => $stu_id));

        if ($get->num_rows() > 0) {

            $student = $get->row();
            $array = array(
                'stu_name_english' => $this->security->xss_clean($this->input->post('stu_name_english')),
                'stu_name_arabic' => $this->security->xss_clean($this->input->post('stu_name_arabic')),
                'first_arabic' => $this->security->xss_clean($this->input->post('first_arabic')),
                'second_arabic' => $this->security->xss_clean($this->input->post('second_arabic')),
                'stu_timezone' => $this->security->xss_clean($this->input->post('stu_timezone')),
                'stu_gender' => $this->security->xss_clean($this->input->post('stu_gender')),
                'stu_cpr' => $this->security->xss_clean($this->input->post('stu_cpr')),
                'stu_mobile' => $this->security->xss_clean($this->input->post('stu_mobile')),
                'stu_email' => $this->security->xss_clean($this->input->post('stu_email')),
                'stu_dob' => $this->security->xss_clean($this->input->post('stu_dob')),
                'stu_education' => $this->security->xss_clean($this->input->post('stu_education')),
                'stu_contact' => $this->security->xss_clean($this->input->post('stu_contact')),
            );

            $file = $_FILES['stu_image'];
            if ($file['size'] > 0) {
                $tar = "uploads/students/";
                $rand=date('Ymd').mt_rand(1001,9999);
                $tar_file = $tar . $rand . basename($file['name']);
                if(move_uploaded_file($file["tmp_name"], $tar_file))
                {
                    $array['stu_image'] = $tar_file;
                }
                else {
                    $array['stu_image'] = '';
                }
            }
            else {
                $array['stu_image'] = '';
            }


            $file_cv = $_FILES['stu_cv'];
            if ($file_cv['size'] > 0) {
                $tar = "uploads/students-cv/";
                $rand=date('Ymd').mt_rand(1001,9999);
                $tar_file = $tar . $rand . basename($file_cv['name']);
                if(move_uploaded_file($file_cv["tmp_name"], $tar_file))
                {
                    $array['stu_cv'] = $tar_file;
                }
                else {
                    $array['stu_cv'] = '';
                }
            }
            else {
                $array['stu_cv'] = '';
            }


            if($array['stu_dob'] == '') {
                $array['stu_dob'] = NULL;
            }

            
            if ($this->students->update('stu_id',$stu_id,'students',$array)) {

                $session = [
                        'userID' => $stu_id,
                        'userName'    => $array['stu_name_english'],
                        'userNameArabic' => $array['first_arabic'],
                        'userType' => 'student',
                        'userTimezone' => $array['stu_timezone']
                       ];
                $this->session->set_userdata('olympicUser',$session);

                $this->session->set_flashdata('alert_type', 'success');
                $this->session->set_flashdata('alert_title', 'Success');
                $this->session->set_flashdata('alert_message', 'Profile updated!');
                
            }
            else {
                $this->session->set_flashdata('alert_type', 'error');
                $this->session->set_flashdata('alert_title', 'Failed');
                $this->session->set_flashdata('alert_message', 'Failed to update your profile!');
            }
            redirect('en/student/profile');
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to load your profile!');
            redirect('en/home');
        }
        
    }

    public function password() {
        $this->change();
    }

    public function change() {
        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];

        $result = $this->students->get_details('students',array('stu_id' => $user_id));

        if ($result->num_rows() > 0) {
            $data['student']=$result->row();
            $data['notice_count'] = $this->students->get_noticeCount($user_id);
            $data['subscribed_courses'] = $this->students->get_subscribedCourseList($user_id);
            $this->load->view('site/english/change-password',$data);
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to load your profile!');
            redirect('en/login');
        }
    }

    public function changePassword()
    {
        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];
        $current_password = $this->security->xss_clean($this->input->post('current_password'));
        $count = $this->students->get_details('students',array('stu_id' => $user_id , 'stu_password' => md5($current_password)))->num_rows();
        if($count > 0)
        {
            $password = $this->security->xss_clean($this->input->post('new_password'));
            $array = [
                'stu_password' => md5($password)
            ];
            $this->students->update('stu_id',$user_id,'students',$array);
            $this->session->set_flashdata('alert_type', 'success');
            $this->session->set_flashdata('alert_title', 'Password updated successfully');
        }
        else
        {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Invalid password, please try again');
            
        }
        redirect('en/student/profile');
    }

    public function subscribe($course_id) {
        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];

        $data['student'] = $this->students->get_details('students',array('stu_id' => $user_id))->row();
        $data['course'] = $this->courses->get_courseDetails($course_id);
        
        $session = [
        	'type' => 'subscription',
        	'course_id' => $course_id
        ];
        $this->session->set_userdata('course_subscription',$session);

        if(!empty($data['course'])) {
            $user = $this->session->userdata['olympicUser'];
            $user_id = $user['userID'];

            $data['students_count'] = $this->courses->get_studentsCount($course_id);
            $get = $this->students->getSubscribedCourse($course_id, $user_id);

            if($get->num_rows()) {

                $result = $get->row();
                $this->session->set_flashdata('alert_type', 'error');
                $this->session->set_flashdata('alert_title', '');
                $this->session->set_flashdata('alert_message', 'Already Subscribed!');
                if($result->sp_status=='Approved') {                    
                    redirect('en/subscriptions/details/' . $course_id);                    
                }
                else {
                    redirect('en/subscriptions');
                }
                
            }

            else {
                if($data['course']->sequence_id) {
                    if($this->students->is_subscribed($data['course']->sequence_id, $user_id)){
                        $this->load->view('site/english/subscribe', $data);
                    }
                    else {
                        $this->session->set_flashdata('alert_type', 'error');
                        $this->session->set_flashdata('alert_title', 'Failed');
                        $this->session->set_flashdata('alert_message', 'You can subscribe this course only after subscribing the previous course!');
                        
                        redirect('en/courses/details/'.$course_id);
                    }
                }
                else if($data['students_count'] >= $data['course']->student_limit) {
                    $this->session->set_flashdata('alert_type', 'error');
                    $this->session->set_flashdata('alert_title', 'Failed');
                    $this->session->set_flashdata('alert_message', 'Students limit reached!');
                        
                    redirect('en/courses/details/'.$course_id);
                }
                else {
                    $this->load->view('site/english/subscribe', $data);
                }
            }
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to load course..!');
            redirect('en/courses');
        }

    }
    public function redirectURL()
    {  
        $token    = $this->input->get('token');
        $tap_id   = $this->input->get('tap_id');
        
        $subscription = $this->session->userdata['course_subscription'];
        $course_id = $subscription['course_id'];
        
        $trans = $this->Common->get_details('transactions',array('tap_id' => $tap_id));
        if($trans->num_rows() > 0)
        {
            $transaction = $trans->row();
            if($transaction->response_code == '000')
            {
                if($course_id)
                {
                    $user = $this->session->userdata['olympicUser'];
                    $user_id = $user['userID'];
                    
                    $get = $this->courses->get_courseDetails($course_id);
                    if ($get) {
                        $sub = array(
                                'course_id' => $course_id,
                                'stu_id' => $user_id,                    
                                'amount' => $get->course_sale_price
                        );
                        if($this->students->insert('students_packages',$sub)) {
                            $this->Common->update('transactions',array('transaction_status' => 'Completed'));
                            $this->session->set_flashdata('alert_type', 'success');
                            $this->session->set_flashdata('alert_title', 'Success');
                            $this->session->set_flashdata('alert_message', 'Subscribed successfully!');
                            redirect('en/subscriptions');
                        }
                        else {
                            $message = 'Your last transaction was failed, please contact our support team in case of balance reduction from your account.';
                            $this->session->set_flashdata('alert_type', 'error');
                            $this->session->set_flashdata('alert_title', 'Failed');
                            $this->session->set_flashdata('alert_message', $message);
                            redirect('en/courses/details/', $course_id);
                        }             
                        
                    }
                    else {
                        $message = 'Your last transaction was failed, please contact our support team in case of balance reduction from your account.';
                        $this->session->set_flashdata('alert_type', 'error');
                        $this->session->set_flashdata('alert_title', 'Failed');
                        $this->session->set_flashdata('alert_message', $message);
                        redirect('en/courses');
                    }
                }
                else
                {
                    $message = 'Your last transaction was failed, please contact our support team in case of balance reduction from your account.';
                    $this->session->set_flashdata('alert_type', 'error');
                    $this->session->set_flashdata('alert_title', 'Failed');
                    $this->session->set_flashdata('alert_message', $message);
                    redirect('en/courses');
                }
            }
            else
            {
                $message = 'Your last transaction was failed due to ' . $transaction->error_message . ', please try again.';
                $this->session->set_flashdata('alert_type', 'error');
                $this->session->set_flashdata('alert_title', 'Failed');
                $this->session->set_flashdata('alert_message', $message);
                redirect('en/courses');
            }
        }
        else
        {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to subscribe course.');
            redirect('en/courses');
        }
    }
    /*public function subscribe_process($course_id) {
        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];

        $get = $this->courses->get_courseDetails($course_id);
        if ($get) {
            
            $sub = array(
                    'course_id' => $course_id,
                    'stu_id' => $user_id,                    
                    'amount' => $get->course_sale_price
            );
            if($this->students->insert('students_packages',$sub)) {
                $this->session->set_flashdata('alert_type', 'success');
                $this->session->set_flashdata('alert_title', 'Success');
                $this->session->set_flashdata('alert_message', 'Subscribed successfully!');
                redirect('en/subscriptions');
            }

            else {
                $this->session->set_flashdata('alert_type', 'error');
                $this->session->set_flashdata('alert_title', 'Failed');
                $this->session->set_flashdata('alert_message', 'Subscription failed!');
                redirect('en/courses/details/2', $course_id);
            }             
            
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to load course..!');
            redirect('en/courses');
        }
           
    }*/

    public function subscribe_process() {

        $course_id = $this->security->xss_clean($this->input->post('course_id'));       

        $course = $this->courses->get_courseDetails($course_id);
        if (!empty($course)) {

            $user = $this->session->userdata['olympicUser'];
            $user_id = $user['userID'];
            
            $get = $this->students->getSubscribedCourse($course_id, $user_id);

            if($get->num_rows()) {
                echo 2;
            }
            else {
                $sub = array(
                    'course_id' => $course_id,
                    'stu_id' => $user_id,                    
                    'amount' => $course->course_sale_price
                );
                if($this->students->insert('students_packages',$sub)) {
                    echo 1;
                }

                else {
                    echo 0;
                }        
            }
                 
            
        }
        else {
            echo 0;
        }
           
    }


    public function payments() 
    {
        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];

        $result = $this->students->get_details('students',array('stu_id' => $user_id));

        if ($result->num_rows() > 0) {
            $data['student']=$result->row();
            $data['notice_count'] = $this->students->get_noticeCount($user_id);
            $data['subscribed_courses'] = $this->students->get_subscribedCourseList($user_id);
            $data['payments'] = $this->students->get_payments($user_id);
            //print_r($data['payments']);
            //exit;
            $this->load->view('site/english/payments', $data);
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to load your profile!');
            redirect('en/login');
        }
    }

    public function notice() {
        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];

        $result = $this->students->get_details('students',array('stu_id' => $user_id));

        if ($result->num_rows() > 0) {
            $data['student']=$result->row();
            $data['notice'] = $notices = $this->students->get_notice();
            //print_r($notices);
            //exit;
            foreach ($notices as $notice) {
                if(!$this->students->is_noticeRead($user_id, $notice->note_id)) {
                    $array = array(
                        'note_id' => $notice->note_id,
                        'stu_id' => $user_id,
                        'read_status' => 1                
                    );
                    $ns_id = $this->students->insert('notes_students',$array);
                }
            }            
            

            
            $data['subscribed_courses'] = $this->students->get_subscribedCourseList($user_id);
            
            //print_r($data['notice']);
            //exit;
            $this->load->view('site/english/notice', $data);
        }
        else {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Failed to load your profile!');
            redirect('en/login');
        }
    }

    public function payment()
    {    
        $this->load->view('site/english/payment-gateway');
    }
    public function test()
    {
        $user = $this->session->userdata['olympicUser'];
        $user_id = $user['userID'];

        $course_id = 2;
        $get = $this->students->get_details('students',array('stu_id' => $user_id));
        if ($get->num_rows() > 0) 
        {
            $data['student'] = $get->row();
            $course = $this->students->get_details('courses',array('course_id' => $course_id));
            if($course->num_rows() > 0)
            {
                $data['course'] = $course->row();
                $this->load->view('site/english/payment-test',$data);
            }
            else
            {
                $this->session->set_flashdata('alert_type', 'error');
                $this->session->set_flashdata('alert_title', 'Failed');
                $this->session->set_flashdata('alert_message', 'Failed to load course.');
                redirect('en/courses');
            }
        }
        else
        {
            $this->session->set_flashdata('alert_type', 'error');
            $this->session->set_flashdata('alert_title', 'Failed');
            $this->session->set_flashdata('alert_message', 'Session expired, please login again to continue');
            redirect('en/login');
        }
    }
    public function test_success()
    {   
        $data['message']  = 'success';
        $data['token']    = $this->input->get('token');
        $data['tap_id']   = $this->input->get('tap_id');
        $this->load->view('site/english/test_success',$data);
    }
}

?>