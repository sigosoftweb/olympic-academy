<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher extends CI_Controller {
	public function __construct()
	{
			parent::__construct();
			$this->load->model('Common');
	}
    public function index()
	{
		$this->load->view('login/teacher');
	}
	public function loginCheck()
	{
		$email = $this->security->xss_clean($this->input->post('email'));
		$password = $this->security->xss_clean($this->input->post('password'));
		$get = $this->Common->get_details('trainers',array('trainer_email' => $email, 'trainer_password' => md5($password)));
		if ($get->num_rows() > 0) 
		{
			$user = $get->row();
			$session = [
				'user_id' => $user->trainer_id,
				'name'    => $user->trainer_name_english,
				'user_type' => 'teacher'
			];
			$this->session->set_userdata('teacher',$session);

			$remember = $this->security->xss_clean($this->input->post('remember'));
			if ($rem) 
			{
				$hour = time() + 3600 * 24 * 30;
				setcookie('teacher_username', $user_name, $hour);
				setcookie('teacher_password', $pass, $hour);
			}
			else 
			{
				$hour = time() - 3600 * 24 * 30;
				setcookie('teacher_username', "", $hour);
				setcookie('teacher_password', "", $hour);
			}
			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_title', 'Success');
			$this->session->set_flashdata('alert_message', 'Logged in Successfully..!');
			redirect('trainer/dashboard');
		}
		else 
		{
			$this->session->set_flashdata('alert_type', 'error');
			$this->session->set_flashdata('alert_title', 'Failed');
			$this->session->set_flashdata('alert_message', 'Login failed');
			redirect('teacher');
		}
	}
	public function logout()
	{
		setcookie('teacher_username');
		setcookie('teacher_password');
		$this->session->unset_userdata('teacher');
		redirect('teacher');
	}

	

}
?>

