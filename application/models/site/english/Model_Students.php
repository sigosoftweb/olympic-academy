<?php
class Model_Students extends CI_Model {

    public function insert($table, $data) {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    

    public function get_details($table, $cond) {
        $result = $this->db->get_where($table, $cond);
        return $result;
    }

    
    public function update($base_id, $id, $table, $data) {
        $this->db->where($base_id, $id);
        $this->db->update($table, $data);
        return true;
    }

    public function get_studentLogin($email, $password) {
        
        $this->db->select('stu_id, stu_name_english,');
        $this->db->from('students');
        $this->db->where('stu_email', $email);
        $this->db->where('stu_password', $password);

        return $this->db->get();
    }

    public function verifyReset($email, $signature) {
        $this->db->select('reset_id');
        $this->db->from('password_reset');
        $this->db->where('email', $email);
        $this->db->where('signature', $signature);
        $this->db->where('TIME_TO_SEC(timediff(now(),timestamp)) <=', 1800);//30 minutes expiry
        return $this->db->get()->num_rows();
    }

    public function get_subscribedCourses($id) {
        $this->db->select('courses.*, cat_name, amount,sp_status,reject_reason');
        $this->db->from('courses');
        $this->db->join('categories','courses.cat_id=categories.cat_id'); 
        $this->db->join('students_packages','courses.course_id=students_packages.course_id');
        $this->db->order_by('students_packages.date','desc');
        $this->db->where('students_packages.stu_id', $id);
        $this->db->where('course_status', 1);
        return $this->db->get()->result();
    }

    public function get_pendingSubscriptions($id) {
        $this->db->select('courses.*, cat_name, amount,sp_status,reject_reason');
        $this->db->from('courses');
        $this->db->join('categories','courses.cat_id=categories.cat_id'); 
        $this->db->join('students_packages','courses.course_id=students_packages.course_id');
        $this->db->order_by('students_packages.date','desc');
        $this->db->where('students_packages.stu_id', $id);
        $this->db->where('course_status', 1);
        $this->db->where('sp_status', 'Waiting');
        return $this->db->get()->result();
    }

    public function get_rejectedSubscriptions($id) {
        $this->db->select('courses.*, cat_name, amount,sp_status,reject_reason');
        $this->db->from('courses');
        $this->db->join('categories','courses.cat_id=categories.cat_id'); 
        $this->db->join('students_packages','courses.course_id=students_packages.course_id');
        $this->db->order_by('students_packages.date','desc');
        $this->db->where('students_packages.stu_id', $id);
        $this->db->where('course_status', 1);
        $this->db->where('sp_status', 'Rejected');
        return $this->db->get()->result();
    }

    public function get_subscribedCourseList($stu_id) {
        $this->db->select('courses.course_id as cid, course_title');
        $this->db->from('courses');
        $this->db->join('students_packages','courses.course_id=students_packages.course_id');
        $this->db->order_by('students_packages.date','desc');
        $this->db->where('students_packages.stu_id', $stu_id);
        $this->db->where('course_status', 1);
        $this->db->where('sp_status', 'Approved');
        return $this->db->get()->result();
    }

    public function get_subscribedCourseDetails($course_id, $stu_id) {
        $this->db->select('courses.*, amount,sp_status,reject_reason');
        $this->db->from('courses');
        $this->db->join('students_packages', 'students_packages.course_id = courses.course_id');
        $this->db->where('students_packages.course_id',$course_id);
        $this->db->where('students_packages.stu_id',$stu_id);
        $this->db->where('course_status', 1);
        $this->db->where('sp_status', 'Approved');
        return $this->db->get()->row();
    }

    public function get_subscribedCourseCount($user_id) {
        $this->db->select('count(course_id) as c_count');
        $this->db->from('students_packages');
        $this->db->where('stu_id', $user_id);
        return $this->db->get()->row();
    }

    public function get_subscribedCourseIDs($user_id) {
        $this->db->select('course_id');
        $this->db->from('students_packages');
        $this->db->where('stu_id', $user_id);
        //$this->db->where('sp_status', 'Approved');
        return $this->db->get()->result_array();
    }

    public function is_subscribed($course_id, $user_id) {
        $this->db->select('sp_id, sp_status');
        $this->db->from('students_packages');
        $this->db->where('stu_id', $user_id);
        $this->db->where('course_id',$course_id);
        $this->db->where('sp_status','Approved');
        return $this->db->get()->num_rows();
    }
    
    public function getSubscribedCourse($course_id, $user_id) {
        $this->db->select('sp_id,sp_status,reject_reason');
        $this->db->from('students_packages');
        $this->db->where('stu_id', $user_id);
        $this->db->where('course_id',$course_id);
        return $this->db->get();
    }

    public function get_allUnsubscribedCourses($user_id, $limit) {
        $this->db->select('courses.*, cat_name');
        $this->db->from('courses');
        $this->db->join('categories','courses.cat_id=categories.cat_id');  
        $this->db->where('course_status', 1);
        $this->db->where('course_id NOT IN (select course_id from students_packages where stu_id = ' . $user_id . ')', NULL, TRUE);
        $this->db->limit($limit);
        return $this->db->get()->result();
        
    }

    

    public function get_studentBatches($stu_id) {
        $this->db->select('cb_id');
        $this->db->from('course_batch_students');
        $this->db->where('stu_id', $stu_id);
        return $this->db->get()->result_array();
    }

    public function get_subscribedCategories($stu_id) {
        $this->db->distinct()->select('cat_id');
        $this->db->from('students_packages');
        $this->db->join('courses', 'students_packages.course_id = courses.course_id');
        $this->db->where('stu_id', $stu_id);
        return $this->db->get()->result_array();
    }

    public function get_relatedCourses($stu_id, $limit = NULL) {

        $categories = $this->get_subscribedCategories($stu_id);
        //$subscribed_courses = $this->get_subscribedCourseIDs($stu_id);

        $catIDs = array_map(function($value){
            return $value['cat_id'];
        }, $categories);

        /*$courseIDs = array_map(function($value){
            return $value['course_id'];
        }, $subscribed_courses);*/
    
        $this->db->select('courses.*, cat_name');
        $this->db->from('courses');
        $this->db->join('categories','courses.cat_id=categories.cat_id'); 
        $this->db->where_in('courses.cat_id', $catIDs);
        //$this->db->where_not_in('course_id', $courseIDs);  
        $this->db->where('course_id NOT IN (select course_id from students_packages where stu_id = '. $stu_id . ' )', NULL, FALSE);
        $this->db->where('course_status', 1); 
        $this->db->limit($limit);

        return $this->db->get()->result();

    }

    public function get_payments($stu_id) {
        $this->db->select('students_packages.course_id as cid, course_title, course_banner, date, amount');
        $this->db->from('students_packages');
        $this->db->join('courses', 'students_packages.course_id = courses.course_id');
        $this->db->where('stu_id', $stu_id);
        $this->db->where('course_status', 1);
        return $this->db->get()->result();

    }

    public function get_noticeCount($user_id) {
        $this->db->select('count(note_id) as ncount');
        $this->db->from('notes');
        $this->db->where('note_id NOT IN (select note_id from notes_students where stu_id = ' . $user_id . ' AND read_status = 1)',NULL,FALSE);
        $this->db->where('type', 'Student');
        $this->db->where('end_time >=', date('Y-m-d H:i:s'));
        return $this->db->get()->row();

    }
    	
    public function get_notice() {
        $this->db->select('note_id, title, description');
        $this->db->from('notes');
        $this->db->where('type', 'Student');
        $this->db->where('end_time >=', date('Y-m-d H:i:s'));
        $this->db->order_by('note_id', 'desc');
        return $this->db->get()->result();

    }

    public function is_noticeRead($user_id, $note_id) {
        $this->db->select('ns_id');
        $this->db->from('notes_students');
        $this->db->where('stu_id', $user_id);
        $this->db->where('note_id', $note_id);
        return $this->db->get()->num_rows();
    }

    public function get_courseAttendance($stu_id, $course_id) {
        $this->db->select('course_attendance.date as attend_date, present, absent, late');
        $this->db->from('course_attendance');
        $this->db->join('attendance', 'course_attendance.att_id = attendance.att_id');
        $this->db->join('course_batches', 'attendance.cb_id = course_batches.cb_id');
        $this->db->where('stu_id', $stu_id);
        $this->db->where('course_id', $course_id);
        return $this->db->get()->result();
    }

    public function get_courseAssignments( $course_id, $cb_id, $stu_id) {
        $this->db->select('course_assignments.ca_id as assignment_id, course_assignments.cb_id as batch_id, csa_id, stu_id, title, attachment, answer, answer_attachment, course_assignments.timestamp as assignment_date, temp.timestamp as student_assignment_date');
        $this->db->from('course_assignments');
        $this->db->join('course_batches', 'course_assignments.cb_id = course_batches.cb_id','left');
        $this->db->join('(select * from course_students_assignments where stu_id ='. $stu_id. ') temp', 'temp.ca_id = course_assignments.ca_id','left');
        $this->db->where('course_assignments.course_id', $course_id);
        $this->db->group_start();
            if ($cb_id == '0') {
                $this->db->or_where('course_assignments.cb_id', '0');
            }
            else {
                $this->db->where('course_assignments.cb_id',$cb_id);
                $this->db->or_where('course_assignments.cb_id','0');
            }
        $this->db->group_end();
        return $this->db->get()->result();
    }

    public function get_courseAssignmentsCount($course_id, $cb_id, $stu_id) {
        $this->db->select('count(course_assignments.ca_id) as a_count');
        $this->db->from('course_assignments');
        $this->db->join('course_batches', 'course_assignments.cb_id = course_batches.cb_id','left');
        $this->db->join('(select * from course_students_assignments where stu_id ='. $stu_id. ') temp', 'temp.ca_id = course_assignments.ca_id','left');
        $this->db->where('course_assignments.course_id', $course_id);
        $this->db->group_start();
            if ($cb_id == '0') {
                $this->db->or_where('course_assignments.cb_id', '0');
            }
            else {
                $this->db->where('course_assignments.cb_id',$cb_id);
                $this->db->or_where('course_assignments.cb_id','0');
            }
        $this->db->group_end();

        return $this->db->get()->row();
    }

    public function get_studentAssignment($csa_id) {
        $this->db->select('answer, answer_attachment');
        $this->db->from('course_students_assignments');
        $this->db->where('csa_id', $csa_id);
        return $this->db->get()->result_array();
    }

    public function get_upcomingCourseClasses($course_id, $stu_id) {
    $this->db->select('cc_id, course_batches.cb_id as batch_id, batch_name, course_sections.cs_id as section_id, section_title, trainer_name_english, trainer_image, topic, starting_time, duration, meeting_password, join_url');
    $this->db->from('course_classes');
    $this->db->join('trainers', 'trainers.trainer_id = course_classes.trainer_id', 'left');
    $this->db->join('course_sections', 'course_classes.cs_id = course_sections.cs_id', 'left');
    $this->db->join('course_batches', 'course_classes.cb_id = course_batches.cb_id'); 
    $this->db->join('course_batch_students', 'course_classes.cb_id = course_batch_students.cb_id');
    $this->db->where('course_classes.course_id',$course_id);
    $this->db->where('stu_id',$stu_id);
    $this->db->where('class_status','pending');
    $this->db->where('starting_time >=',date('Y-m-d H:i:s'));
    $this->db->order_by('starting_time');
    return $this->db->get()->result();
  }



  public function get_upcomingClassesCount($course_id, $stu_id) {
    $this->db->select('count(cc_id) as c_count');
    $this->db->from('course_classes');
    $this->db->join('course_batches', 'course_classes.cb_id = course_batches.cb_id'); 
    $this->db->join('course_batch_students', 'course_batches.cb_id = course_batch_students.cb_id');
    $this->db->where('course_classes.course_id',$course_id);
    $this->db->where('stu_id',$stu_id);
    $this->db->where('class_status','pending');
    $this->db->where('starting_time >=',date('Y-m-d H:i:s'));
    return $this->db->get()->row();
  }

  public function get_completedCourseClasses($course_id, $stu_id) {
    
    $this->db->select('course_classes.cc_id as class_id, course_sections.cs_id as section_id, section_title, trainer_name_english, trainer_image, topic, starting_time, duration, type, title, attachment');
    $this->db->from('course_classes');
    $this->db->join('trainers', 'trainers.trainer_id = course_classes.trainer_id', 'left');
    $this->db->join('course_sections', 'course_classes.cs_id = course_sections.cs_id', 'left');
      
    $this->db->join('course_batches', 'course_classes.cb_id = course_batches.cb_id'); 
    $this->db->join('course_batch_students', 'course_classes.cb_id = course_batch_students.cb_id');
    $this->db->join('course_class_attachments', 'course_classes.cc_id = course_class_attachments.cc_id','left'); 
    $this->db->where('course_classes.course_id',$course_id);
    $this->db->where('stu_id',$stu_id);
    //$this->db->where('class_status','completed');
    $this->db->where('TIME_TO_SEC(timediff(now(),starting_time)) >=', 'duration*60');
    $this->db->order_by('starting_time');
    return $this->db->get()->result();
  }



  public function get_completedClassesCount($course_id, $stu_id) {
    $this->db->select('count(cc_id) as c_count');
    $this->db->from('course_classes');
    $this->db->join('course_batches', 'course_classes.cb_id = course_batches.cb_id'); 
    $this->db->join('course_batch_students', 'course_batches.cb_id = course_batch_students.cb_id');
    $this->db->where('course_classes.course_id',$course_id);
    $this->db->where('stu_id',$stu_id);
    //$this->db->where('class_status','completed');
    $this->db->where('TIME_TO_SEC(timediff(now(),starting_time)) >=', 'duration*60');
    return $this->db->get()->row();
  }

  
  public function get_upcomingClassesMonth($course_id, $stu_id, $year, $month) {
    $this->db->select('cc_id, topic, starting_time, join_url');
    $this->db->from('course_classes');
    $this->db->join('course_batches', 'course_classes.cb_id = course_batches.cb_id'); 
    $this->db->join('course_batch_students', 'course_batches.cb_id = course_batch_students.cb_id');
    $this->db->where('course_classes.course_id',$course_id);
    $this->db->where('stu_id',$stu_id);
    $this->db->where('MONTH(starting_time)', $month);
    $this->db->where('YEAR(starting_time)', $year);
    return $this->db->get()->result();
  }
  

  public function get_upcomingSectionsMonth($course_id, $year, $month) {
    $this->db->select('cs_id, section_title, section_start_time');
    $this->db->from('course_sections');
    $this->db->where('course_id', $course_id);
    $this->db->where('MONTH(section_start_time)', $month);
    $this->db->where('YEAR(section_start_time)', $year);
    return $this->db->get()->result();
  }



  public function get_upcomingExamsMonth($course_id, $cb_id, $stu_id, $year, $month) {
        $this->db->select('exam_id, exam_name, time_status, from_time, to_time');
        $this->db->from('exams');
        $this->db->where('exams.course_id',$course_id);
        $this->db->where('exam_status','1');
        $this->db->where('registration_status','1');
        $this->db->group_start();
        if ($cb_id == '0') {
            $this->db->where('cb_id','0');
        }
        else {
            $this->db->where('cb_id',$cb_id);
            $this->db->or_where('cb_id','0');
        }
        $this->db->group_end();
        //$this->db->where('exams.exam_id NOT IN (select exam_id from students_exams where stu_id = ' . $stu_id . ')',NULL,FALSE);
        $this->db->where('MONTH(from_time)', $month);
        $this->db->where('YEAR(from_time)', $year);
        return $this->db->get()->result();
    }
    
}
?>
