<?php

class Model_Categories extends CI_Model
{

  

  public function get_categories_course_count()
  {
    $this->db->select('categories.*, count(course_id) as course_count');
    $this->db->from('categories');
    //$this->db->join('courses','courses.cat_id=categories.cat_id', 'left'); 
    $this->db->join('courses','courses.cat_id=categories.cat_id'); 
    $this->db->group_by('courses.cat_id');
    $this->db->where('cat_status', '1');
    
    //$this->db->limit(10);
    return $this->db->get()->result();
  }
}
?>