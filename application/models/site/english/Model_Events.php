<?php

class Model_Events extends CI_Model
{

  public function get_upcomingEvents($limit= NULL)
  {
    $this->db->select('event_id, title, description, start_time, end_time, image');
    $this->db->from('events');   
    $this->db->where('status', '1');
    $this->db->order_by('start_time');
    $this->db->where('end_time >=',date('Y-m-d H:i:s'));
    $this->db->limit($limit);
    return $this->db->get()->result();
  }

  public function get_Events($limit= NULL)
  {
    $this->db->select('event_id, title, description, start_time, end_time, image');
    $this->db->from('events');   
    $this->db->where('status', '1');
    $this->db->order_by('start_time', 'DESC');
    $this->db->limit($limit);
    return $this->db->get()->result();
  }

  public function get_eventDetails($id) {
    $this->db->select('event_id, title, description, start_time, end_time, image');
    $this->db->from('events');
    $this->db->where('event_id',$id);
    return $this->db->get()->row();
  }

  public function get_otherEvents($id) {
  	$this->db->select('event_id, title, description, start_time, end_time, image');
    $this->db->from('events');   
    $this->db->where('status', '1');
    $this->db->where('event_id !=',$id);
    $this->db->order_by('start_time');
    $this->db->limit(6);
    return $this->db->get()->result();
  }


}
?>