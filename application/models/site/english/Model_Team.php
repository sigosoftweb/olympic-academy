<?php

class Model_Team extends CI_Model
{

  public function get_team($limit = NULL)
  {
    $this->db->select('*');
    $this->db->from('team');   
    $this->db->where('team_status', 1);
    $this->db->limit($limit);
    return $this->db->get()->result();
  }

 


}
?>