<?php

class Model_Contact extends CI_Model

{

  public function get_contact() {

    $this->db->select('address, address_arabic, email, mobile');
    $this->db->from('settings');
    return $this->db->get()->row();

  }

}

?>