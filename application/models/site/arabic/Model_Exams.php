<?php
	class Model_Exams extends CI_Model
	{
		public function insert($table,$data)
	  	{
	    	$this->db->insert($table,$data);
	    	$insert_id = $this->db->insert_id();
	    	return  $insert_id;
	  	}
	  	public function get_details($table,$cond)
	  	{
	    	$res = $this->db->get_where($table,$cond);
	    	return $res;
	  	}
	  	public function update($base_id,$id,$table,$data) {
	    	$this->db->where($base_id, $id);
	    	$this->db->update($table, $data);
	    	return true;
	  	}
	  	public function delete($table,$cond)
	  	{
	    	$this->db->delete($table, $cond);
	    	return true;
	  	}
  		function getExams($course_id,$cb_id,$stu_id)
  		{
    		$this->db->select('exams.*,section_title, section_title_arabic');
    		$this->db->from('exams');
			$this->db->where('exams.course_id',$course_id);
			$this->db->where('exam_status','1');
			$this->db->where('registration_status','1');
			$this->db->group_start();
				if ($cb_id == '0') {
					$this->db->where('cb_id','0');
				}
				else {
					$this->db->where('cb_id',$cb_id);
					$this->db->or_where('cb_id','0');
				}
			$this->db->group_end();
			$this->db->where('exams.exam_id NOT IN (select exam_id from students_exams where stu_id = ' . $stu_id . ')',NULL,FALSE);
			$this->db->join('course_sections','exams.cs_id=course_sections.cs_id','left');
    		return $this->db->get()->result();
  		}
		function checkExam($course_id,$cb_id,$stu_id,$exam_id)
  		{
    		$this->db->select('exams.*,section_title');
    		$this->db->from('exams');
			$this->db->where('exams.course_id',$course_id);
			$this->db->where('exam_status','1');
			$this->db->where('registration_status','1');
			$this->db->where('exams.exam_id',$exam_id);
			$this->db->group_start();
				if ($cb_id == '0') {
					$this->db->where('cb_id','0');
				}
				else {
					$this->db->where('cb_id',$cb_id);
					$this->db->or_where('cb_id','0');
				}
			$this->db->group_end();
			$this->db->where('exams.exam_id NOT IN (select exam_id from students_exams where stu_id = ' . $stu_id . ')',NULL,FALSE);
			$this->db->join('course_sections','exams.cs_id=course_sections.cs_id','left');
    		return $this->db->get();
  		}
		function getStudentBatch($course_id,$user_id)
		{
			$this->db->select('cb_id');
    		$this->db->from('course_batch_students');
			$this->db->where('course_id',$course_id);
			$this->db->where('stu_id',$user_id);
			return $this->db->get();
		}
		function getExamInstructiosns($exam_id)
		{
			$this->db->select('instruction, instruction_arabic');
    		$this->db->from('exam_instructions');
			$this->db->where('exam_id',$exam_id);
			return $this->db->get()->result();
		}
		function getExamQuestions($exam_id)
		{
			$this->db->select('que_id');
    		$this->db->from('questions');
			$this->db->where('exam_id',$exam_id);
			return $this->db->get()->result();
		}
		function getStudentExam($exam_id,$user_id)
		{
			$this->db->select('exams.*,se_id,ending_time');
    		$this->db->from('students_exams');
			$this->db->where('students_exams.exam_id',$exam_id);
			$this->db->where('stu_id',$user_id);
			$this->db->where('completed','0');
			$this->db->join('exams','exams.exam_id=students_exams.exam_id');
			return $this->db->get();
		}
		function getStudentsExamQuestions($se_id)
		{
			$this->db->select('que_id,ans');
    		$this->db->from('students_question_answers');
			$this->db->where('se_id',$se_id);
			return $this->db->get()->result();
		}
		function getQuestion($que_id,$se_id)
		{
			$this->db->select('questions.*,se_id,sqa_id');
    		$this->db->from('students_question_answers');
			$this->db->join('questions','questions.que_id=students_question_answers.que_id');
			$this->db->where('students_question_answers.que_id',$que_id);
			$this->db->where('se_id',$se_id);
			return $this->db->get();
		}
		function next($exam_id,$que_id)
		{
			$this->db->select('que_id');
    		$this->db->from('questions');
			$this->db->where('exam_id',$exam_id);
			$this->db->where('que_id >',$que_id);
			$this->db->limit(1);
			$res = $this->db->get();
			if($res->num_rows() > 0)
			{
				return $res->row()->que_id;
			}
			else {
				return false;
			}
		}
		function previous($exam_id,$que_id)
		{
			$this->db->select('que_id');
    		$this->db->from('questions');
			$this->db->where('exam_id',$exam_id);
			$this->db->where('que_id <',$que_id);
			$this->db->order_by('que_id','desc');
			$this->db->limit(1);
			$res = $this->db->get();
			if($res->num_rows() > 0)
			{
				return $res->row()->que_id;
			}
			else {
				return false;
			}
		}
		function getExam($se_id,$user_id)
		{
			$this->db->select('*');
    		$this->db->from('students_exams');
			$this->db->where('se_id',$se_id);
			$this->db->where('stu_id',$user_id);
			return $this->db->get();
		}
		function getStudentsAnswers($se_id)
		{
			$this->db->select('questions.ans,correct,negative,students_question_answers.ans as stu_ans');
    		$this->db->from('students_question_answers');
			$this->db->where('se_id',$se_id);
			$this->db->join('questions','questions.que_id=students_question_answers.que_id');
			return $this->db->get()->result();
		}
		function getAttendedExams($user_id)
		{
			$this->db->select('exams.*,se_id, section_title, section_title_arabic');
    		$this->db->from('students_exams');
			$this->db->where('stu_id',$user_id);
			$this->db->join('exams','exams.exam_id=students_exams.exam_id');
			$this->db->join('course_sections','exams.cs_id=course_sections.cs_id','left');
			$this->db->order_by('se_id','desc');
			return $this->db->get()->result();
		}
		function getExamResult($se_id,$user_id)
		{
			$this->db->select('students_exams.*,exam_name, exam_name_arabic, show_result');
    		$this->db->from('students_exams');
			$this->db->where('se_id',$se_id);
			$this->db->where('stu_id',$user_id);
			$this->db->join('exams','exams.exam_id=students_exams.exam_id');
			return $this->db->get();
		}
		function getAllQuestions($se_id)
		{
			$this->db->select('questions.*,students_question_answers.ans as stu_ans');
    		$this->db->from('students_question_answers');
			$this->db->where('se_id',$se_id);
			$this->db->join('questions','questions.que_id=students_question_answers.que_id');
			return $this->db->get()->result();
		}
		function getUnattemptedExams($course_id,$cb_id,$stu_id)
  		{
			$now = date('Y-m-d H:i:s');
    		$this->db->select('exams.*,section_title, section_title_arabic');
    		$this->db->from('exams');
			$this->db->where('exams.course_id',$course_id);
			$this->db->where('exam_status','1');
			$this->db->where('registration_status','1');
			$this->db->where('time_status','1');
			$this->db->where('to_time <',$now);
			$this->db->group_start();
				if ($cb_id == '0') {
					$this->db->where('cb_id','0');
				}
				else {
					$this->db->where('cb_id',$cb_id);
					$this->db->or_where('cb_id','0');
				}
			$this->db->group_end();
			$this->db->where('exams.exam_id NOT IN (select exam_id from students_exams where stu_id = ' . $stu_id . ')',NULL,FALSE);
			$this->db->join('course_sections','exams.cs_id=course_sections.cs_id','left');
    		return $this->db->get()->result();
  		}
	}
?>
