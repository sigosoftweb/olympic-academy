<?php

class Model_Slider extends CI_Model
{

  public function get_slides()
  {
    $this->db->select('name, image');
    $this->db->from('sliders');   
    $this->db->where('status', '1') ;
    return $this->db->get()->result();
  }

  public function get_slidesCount()
  {
    $this->db->select('count(slider_id) as scount');
    $this->db->from('sliders');   
    $this->db->where('status', '1') ;    
    return $this->db->get()->row();
  }


}
?>