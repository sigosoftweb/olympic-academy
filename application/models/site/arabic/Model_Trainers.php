<?php

class Model_Trainers extends CI_Model
{

  public function get_Trainers($limit = NULL)
  {
    $this->db->select('*');
    $this->db->from('trainers');   
    $this->db->where('trainer_status', 1);
    $this->db->limit($limit);
    return $this->db->get()->result();
  }

  public function get_trainerDetails($id)
  {
    $this->db->select('*');
    $this->db->from('trainers');   
    $this->db->where('trainer_status', 1);
    $this->db->where('trainer_id', $id);
    return $this->db->get()->row();
  }


}
?>