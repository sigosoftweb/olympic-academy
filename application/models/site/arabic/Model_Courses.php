<?php

class Model_Courses extends CI_Model
{

  public function get_details($table, $cond) {
      $result = $this->db->get_where($table, $cond);
      return $result;
  }

  public function get_Courses($limit = NULL)
  {
    $this->db->select('courses.course_id, course_title, course_title_arabic, course_description_arabic, course_duration, course_duration_arabic, course_description, course_starting_date, course_sale_price, course_price, course_banner, sequence_id, cat_name, cat_name_arabic, student_limit, students_count');
    $this->db->from('courses');
    $this->db->join('categories','courses.cat_id=categories.cat_id'); 
    $this->db->join('(select course_id, count(sp_id) as students_count from students_packages group by course_id) SP ', 'SP.course_id = courses.course_id', 'left');
    //$this->db->group_by('students_packages.course_id'); 
    $this->db->where('course_status', 1);
    $this->db->limit($limit);
    return $this->db->get()->result();
  }

  public function get_Courses_byCategory($cat_id, $limit = NULL)
  {
    $this->db->select('courses.course_id, course_title, course_title_arabic, course_description_arabic, course_duration, course_duration_arabic, course_description, course_starting_date, course_sale_price, course_price, course_banner, sequence_id, cat_name, cat_name_arabic, student_limit, students_count');
    $this->db->from('courses');
    $this->db->join('categories','courses.cat_id=categories.cat_id'); 
    $this->db->join('(select course_id, count(sp_id) as students_count from students_packages group by course_id) SP ', 'SP.course_id = courses.course_id', 'left');  
    $this->db->where('course_status', 1); 
    $this->db->limit($limit);
    $this->db->where('courses.cat_id', $cat_id);
    return $this->db->get()->result();
  }



  public function get_latestCourses($limit = NULL)
  {
    $this->db->select('courses.*, cat_name, cat_name_arabic');
    $this->db->from('courses');
    $this->db->join('categories','courses.cat_id=categories.cat_id');
    $this->db->where('course_status', 1);    
    $this->db->order_by('timestamp','desc');
    $this->db->limit($limit);
    return $this->db->get()->result();
  }

  public function get_latestCourses_byCategory($cat_id, $limit = NULL)
  {
    $this->db->select('courses.*, cat_name, cat_name_arabic');
    $this->db->from('courses');
    $this->db->join('categories','courses.cat_id=categories.cat_id');    
    $this->db->order_by('timestamp','desc');
    $this->db->limit($limit);
    $this->db->where('course_status', 1);
    $this->db->where('courses.cat_id',$cat_id);
    return $this->db->get()->result();
  }

  public function get_popularCourses($limit = NULL)
  {
    $this->db->select('courses.*, cat_name, cat_name_arabic, count(stu_id) as students_count');
    $this->db->from('courses');
    $this->db->join('categories','courses.cat_id=categories.cat_id'); 
    $this->db->join('students_packages','courses.course_id=students_packages.course_id','left');
    $this->db->order_by('students_count','desc');
    $this->db->group_by('students_packages.course_id');
    $this->db->limit($limit);
    $this->db->where('course_status', 1);
    return $this->db->get()->result();
  }

  public function get_popularCourses_byCategory($cat_id, $limit = NULL)
  {
    $this->db->select('courses.*, cat_name, cat_name_arabic, count(stu_id) as students_count');
    $this->db->from('courses');
    $this->db->join('categories','courses.cat_id=categories.cat_id'); 
    $this->db->join('students_packages','courses.course_id=students_packages.course_id','left');
    $this->db->order_by('students_count','desc');
    $this->db->group_by('students_packages.course_id');
    $this->db->limit($limit);
    $this->db->where('courses.cat_id',$cat_id);
    $this->db->where('course_status', 1);
    return $this->db->get()->result();
  }

  public function get_courseDetails($id) {
    $this->db->select('courses.*, cat_name, cat_name_arabic');
    $this->db->from('courses');
    $this->db->join('categories','courses.cat_id=categories.cat_id');      
    $this->db->where('course_id',$id);
    $this->db->where('course_status', 1);
    return $this->db->get()->row();
  }

  public function get_studentsCount($course_id) {
        $this->db->select('sp_id');
        $this->db->from('students_packages');
        $this->db->where('course_id', $course_id);
        return $this->db->get()->num_rows();
    }

  public function get_courseTrainers($id) {
    $this->db->select('trainers.trainer_id, trainer_name_english, trainer_name_arabic, arabic_first, arabic_second, trainer_image, trainer_education, education_arabic, trainer_experience, experience_arabic');
    $this->db->from('course_trainers');
    $this->db->join('trainers', 'course_trainers.trainer_id = trainers.trainer_id');
    $this->db->where('course_id', $id);
    $this->db->where('trainer_status', 1);
    return $this->db->get()->result();
  }

  public function get_courseOrganisers($course_id) {
    $this->db->select('organiser_first, arabic_first, organiser_second, arabic_second, organiser_image, organiser_education, organiser_experience, education_arabic, experience_arabic');
    $this->db->from('course_organisers');
    $this->db->join('organisers', 'course_organisers.organiser_id = organisers.organiser_id');
    $this->db->where('course_id', $course_id);
    $this->db->where('organiser_status', 1);
    return $this->db->get()->result();
  }

  public function get_sectionsCount($id) {
    $this->db->select('count(cs_id) as scount');
    $this->db->from('course_sections');
    $this->db->where('course_id',$id);
    $this->db->where('section_status', 1);
    return $this->db->get()->row();
  }
  

  public function get_courseSections($id) {
    $this->db->select('course_sections.*');
    $this->db->from('course_sections');
    $this->db->where('course_id',$id);
    $this->db->where('section_status', 1);
    return $this->db->get()->result();
  }

  public function get_courseSectionAttachments($cs_id) {
    $this->db->select('csa_id, csa_attachment, csa_file_type, csa_youtube, csa_other');
    $this->db->from('course_section_attachments');    
    $this->db->where('cs_id',$cs_id);
    return $this->db->get()->result();
  }
  

  public function search($str) {
    $this->db->select('courses.*, cat_name, cat_name_arabic');
    $this->db->from('courses');
    $this->db->join('categories','courses.cat_id=categories.cat_id');  
    $this->db->where('course_status', 1);
    $this->db->like('course_title', $str);
    $this->db->or_like('course_title_arabic', $str);    
    //$this->db->limit(10);
    return $this->db->get()->result();
  }

  public function get_upcomingClassesMonth($course_id, $year, $month) {
    $this->db->select('cc_id, topic, topic_arabic, starting_time');
    $this->db->from('course_classes');    
    $this->db->where('course_id',$course_id);
    $this->db->where('MONTH(starting_time)', $month);
    $this->db->where('YEAR(starting_time)', $year);
    return $this->db->get()->result();
  }
  

  public function get_upcomingSectionsMonth($course_id, $year, $month) {
    $this->db->select('cs_id, section_title, section_title_arabic, section_start_time');
    $this->db->from('course_sections');
    $this->db->where('course_id', $course_id);
    $this->db->where('section_status', 1);
    $this->db->where('MONTH(section_start_time)', $month);
    $this->db->where('YEAR(section_start_time)', $year);
    return $this->db->get()->result();
  }



  public function get_upcomingExamsMonth($course_id, $year, $month) {
        $this->db->select('exam_id, exam_name, exam_name_arabic, time_status, from_time, to_time');
        $this->db->from('exams');
        $this->db->where('exams.course_id',$course_id);
        $this->db->where('exam_status','1');        
        $this->db->where('MONTH(from_time)', $month);
        $this->db->where('YEAR(from_time)', $year);
        return $this->db->get()->result();
  }

  public function get_allCoursesMonth($year, $month) {
      $this->db->select('course_id, course_title, course_title_arabic, course_starting_date, course_duration, course_duration_arabic');
      $this->db->from('courses');
      $this->db->where('course_status', 1);
      $this->db->where('MONTH(course_starting_date)', $month);
      $this->db->where('YEAR(course_starting_date)', $year);
      return $this->db->get()->result();
  }



  public function get_allCoursesSectionsMonth($year, $month) {
      $this->db->select('cs_id, course_sections.course_id as cid, course_title, section_title, section_start_time');
      $this->db->from('course_sections');
      $this->db->join('courses', 'course_sections.course_id = courses.course_id');
      $this->db->where('section_status', 1);
      $this->db->where('MONTH(section_start_time)', $month);
      $this->db->where('YEAR(section_start_time)', $year);
      return $this->db->get()->result();
  }

  public function get_allCoursesClassesMonth($year, $month) {
    $this->db->select('cc_id, course_classes.course_id as cid, course_title, topic, starting_time');
    $this->db->from('course_classes');    
    $this->db->join('courses', 'course_classes.course_id = courses.course_id');
    $this->db->where('MONTH(starting_time)', $month);
    $this->db->where('YEAR(starting_time)', $year);
    return $this->db->get()->result();
  }

  public function get_allCoursesExamsMonth($year, $month) {
        $this->db->select('exam_id, exams.course_id as cid, course_title, exam_name, time_status, from_time, to_time');
        $this->db->from('exams');
        $this->db->join('courses', 'exams.course_id = courses.course_id');
        $this->db->where('exam_status','1');        
        $this->db->where('MONTH(from_time)', $month);
        $this->db->where('YEAR(from_time)', $year);
        return $this->db->get()->result();
  }

  public function get_resourcesCourseIDs() {
    $this->db->select('course_id');
    $this->db->from('course_resources');
    $this->db->order_by('course_id');
    $this->db->group_by('course_id');
    return $this->db->get()->result();

  }

  public function get_courseResources($course_id) {
    $this->db->select('cr_id, cr_title, cr_type, cr_attachment');
    $this->db->from('course_resources');
    $this->db->order_by('cr_id');
    $this->db->where('course_id', $course_id);
    return $this->db->get()->result();
  }

  public function get_allResources() {
    $this->db->select('cr_id, cr_title, cr_title_arabic, cr_type, cr_attachment');
    $this->db->from('course_resources');
    $this->db->order_by('cr_id');
    return $this->db->get()->result();
  }

  

}
?>