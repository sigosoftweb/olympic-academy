<?php
	class Model_dashboard extends CI_Model {
	    function getStudentCount()
		{
			$this->db->select('stu_id');
			$this->db->from('students');
			return $this->db->get()->num_rows();
		}
		function getTrainersCount()
		{
			$this->db->select('trainer_id');
			$this->db->from('trainers');
			return $this->db->get()->num_rows();
		}
		function getCourseCount()
		{
			$this->db->select('course_id');
			$this->db->from('courses');
			return $this->db->get()->num_rows();
		}
		function getEventsCount()
		{
			$this->db->select('event_id');
			$this->db->from('events');
			return $this->db->get()->num_rows();
		}
		function getExamCount()
		{
			$this->db->select('*');
			$this->db->from('exams');
			$this->db->where('registration_status','1');
			return $this->db->get()->num_rows();
		}
		function getTodayEarning()
		{
			$start = date('Y-m-d') . ' 00:00:00';
			$end = date('Y-m-d') . ' 23:59:59';
			$this->db->select_sum('amount');
			$this->db->from('students_packages');
			$this->db->where('date >=',$start);
			$this->db->where('date <=',$end);
			$sum = $this->db->get()->row()->amount;
			if($sum == '')
			{
				return 0;
			}
			else {
				return $sum;
			}
		}
		function getMonthlyEarning()
		{
			$date = date('Y-m-d');
			$start = date('Y-m-01', strtotime($date)) . ' 00:00:00';
			$end = date('Y-m-t', strtotime($date)) . ' 23:59:59';

			$this->db->select_sum('amount');
			$this->db->from('students_packages');
			$this->db->where('date >=',$start);
			$this->db->where('date <=',$end);
			$sum = $this->db->get()->row()->amount;
			if($sum == '')
			{
				return 0;
			}
			else {
				return $sum;
			}
		}
		function getLifetimeEarning()
		{
			$this->db->select_sum('amount');
			$this->db->from('students_packages');
			$sum = $this->db->get()->row()->amount;
			if($sum == '')
			{
				return 0;
			}
			else {
				return $sum;
			}
		}

		function getStudents()
		{
			$this->db->select('*');
			$this->db->from('students');
			$this->db->order_by('stu_id','desc');
			$this->db->limit(10);
			return $this->db->get()->result();
		}
		function getTrainers()
		{
			$this->db->select('*');
			$this->db->from('trainers');
			$this->db->order_by('trainer_id','desc');
			$this->db->limit(10);
			return $this->db->get()->result();
		}
		function getTransactions()
		{
			$this->db->select('students_packages.*,stu_name_english,stu_name_arabic,stu_mobile,stu_email,course_title,stu_image');
			$this->db->from('students_packages');
			$this->db->join('students','students_packages.stu_id=students.stu_id');
			$this->db->join('courses','courses.course_id=students_packages.course_id');
			$this->db->order_by('sp_id','desc');
			$this->db->limit(10);
			return $this->db->get()->result();
		}
	}
?>
