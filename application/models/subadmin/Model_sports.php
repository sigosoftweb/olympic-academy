<?php
class Model_sports extends CI_Model {
    public function insert($table, $data) {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    public function get_details($table, $cond) {
        $res = $this->db->get_where($table, $cond);
        return $res;
    }
    public function update($base_id, $id, $table, $data) {
        $this->db->where($base_id, $id);
        $this->db->update($table, $data);
        return true;
    }
    public function delete($table, $cond) {
        $this->db->delete($table, $cond);
        return true;
    }
	public function getAllSports()
	{
		return $this->db->select('sports_id,sport,status')->from('sports')->order_by('sports_id','desc')->get()->result();
	}
}
?>
