<?php
class Model_students extends CI_Model {
    public function insert($table, $data) {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    public function get_details($table, $cond) {
        $res = $this->db->get_where($table, $cond);
        return $res;
    }
    public function update($base_id, $id, $table, $data) {
        $this->db->where($base_id, $id);
        $this->db->update($table, $data);
        return true;
    }
    public function delete($table, $cond) {
        $this->db->delete($table, $cond);
        return true;
    }
	function make_query(){
      $this->db->select('*');
      $this->db->from('students');
      if (isset($_POST["search"]["value"])) {
		  $search = $_POST["search"]["value"];
          $this->db->where("(stu_name_english LIKE '%" . $search .  "%' OR stu_name_arabic LIKE '%" . $search . "%')",NULL,false);
      }
      if (isset($_POST["order"])) {
        $this->db->order_by($_POST['order']['0']['column'],$_POST['order']['0']['dir']);
      }
      else {
        $this->db->order_by("stu_id","desc");
      }
    }
    function make_datatables()
    {
      $this->make_query();
      if ($_POST["length"] != -1) {
        $this->db->limit($_POST["length"],$_POST["start"]);
      }
      $query = $this->db->get();
      return $query->result();
    }
    function get_filtered_data()
    {
      $this->make_query();
      $query = $this->db->get();
      return $query->num_rows();
    }
    function get_all_data()
    {
      $this->db->select("*");
      $this->db->from("students");
      return $this->db->count_all_results();
    }
	function getStudentsPackages($stu_id)
	{
		$this->db->select("students_packages.*,courses.*");
        $this->db->from("students_packages");
        $this->db->where('stu_id',$stu_id);
		$this->db->join('courses','courses.course_id=students_packages.course_id');
        return $this->db->get()->result();
	}
	public function getAvailableCourses($stu_id)
	{
		$q =$this->db->select('t1.*')
            ->from('courses AS t1')
            ->where('t1.course_status','1')
            ->where('t1.course_id NOT IN (select course_id from students_packages where stu_id = ' . $stu_id . ')',NULL,FALSE)
            ->order_by('t1.course_id','desc')->get()->result();
		return $q;
	}
}
?>
