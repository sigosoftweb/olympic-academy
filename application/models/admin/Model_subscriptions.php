<?php
class Model_subscriptions extends CI_Model {
    public function insert($table, $data) {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    public function get_details($table, $cond) {
        $res = $this->db->get_where($table, $cond);
        return $res;
    }
    public function update($base_id, $id, $table, $data) {
        $this->db->where($base_id, $id);
        $this->db->update($table, $data);
        return true;
    }
    public function delete($table, $cond) {
        $this->db->delete($table, $cond);
        return true;
    }
	function make_query($param){
      $this->db->select('students_packages.*,stu_name_english,stu_name_arabic,stu_mobile,stu_email,course_title');
      $this->db->from('students_packages');
	  $this->db->join('students','students.stu_id=students_packages.stu_id','left');
	  $this->db->join('courses','courses.course_id=students_packages.course_id','left');
	  $this->db->where('sp_status',$param);
      if (isset($_POST["search"]["value"])) {
		  $search = $_POST["search"]["value"];
          $this->db->where("(stu_name_english LIKE '%" . $search .  "%' OR stu_mobile LIKE '%" . $search . "%' OR course_title LIKE '%" . $search . "%')",NULL,false);
      }
      if (isset($_POST["order"])) {
        $this->db->order_by($_POST['order']['0']['column'],$_POST['order']['0']['dir']);
      }
      else {
        $this->db->order_by("sp_id","desc");
      }
    }
    function make_datatables($param)
    {
      $this->make_query($param);
      if ($_POST["length"] != -1) {
        $this->db->limit($_POST["length"],$_POST["start"]);
      }
      $query = $this->db->get();
      return $query->result();
    }
    function get_filtered_data($param)
    {
      $this->make_query($param);
      $query = $this->db->get();
      return $query->num_rows();
    }
    function get_all_data($param)
    {
      $this->db->select("*");
      $this->db->from("students_packages");
	  $this->db->where('sp_status',$param);
      return $this->db->count_all_results();
    }
}
?>
