<?php
class Model_finance extends CI_Model {
    public function insert($table, $data) {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    public function get_details($table, $cond) {
        $res = $this->db->get_where($table, $cond);
        return $res;
    }
    public function update($base_id, $id, $table, $data) {
        $this->db->where($base_id, $id);
        $this->db->update($table, $data);
        return true;
    }
    public function delete($table, $cond) {
        $this->db->delete($table, $cond);
        return true;
    }
	function make_query(){
      $this->db->select('students_packages.*,stu_name_english,stu_name_arabic,stu_mobile,stu_email,course_title');
      $this->db->from('students_packages');
	  $this->db->join('students','students.stu_id=students_packages.stu_id','left');
	  $this->db->join('courses','courses.course_id=students_packages.course_id','left');
      if (isset($_POST["search"]["value"])) {
		  $search = $_POST["search"]["value"];
          $this->db->where("(stu_name_english LIKE '%" . $search .  "%' OR stu_mobile LIKE '%" . $search . "%' OR course_title LIKE '%" . $search . "%')",NULL,false);
      }
      if (isset($_POST["order"])) {
        $this->db->order_by($_POST['order']['0']['column'],$_POST['order']['0']['dir']);
      }
      else {
        $this->db->order_by("sp_id","desc");
      }
    }
    function make_datatables()
    {
      $this->make_query();
      if ($_POST["length"] != -1) {
        $this->db->limit($_POST["length"],$_POST["start"]);
      }
      $query = $this->db->get();
      return $query->result();
    }
    function get_filtered_data()
    {
      $this->make_query();
      $query = $this->db->get();
      return $query->num_rows();
    }
    function get_all_data()
    {
      $this->db->select("*");
      $this->db->from("students_packages");
      return $this->db->count_all_results();
    }
	function getTransactions($from,$to,$payment_type)
	{
		$this->db->select('students_packages.*,stu_name_english,stu_name_arabic,stu_mobile,stu_email,course_title');
        $this->db->from('students_packages');
		if ($from) {
			$this->db->where('date >=',$from);
		}
		if ($to) {
			$this->db->where('date <=',$to);
		}
		if($payment_type != 'all')
		{
		    $this->db->where('payment_type',$payment_type);
		}
  	    $this->db->join('students','students.stu_id=students_packages.stu_id','left');
  	    $this->db->join('courses','courses.course_id=students_packages.course_id','left');
		return $this->db->get()->result();
	}
	function getAllCourses()
	{
	    $this->db->select('course_id,course_title');
		$this->db->from('courses');
		$this->db->where('course_status','1');
		$this->db->order_by('course_id','desc');
		return $this->db->get()->result();
	}
	function getCourseFilter($course_id,$from,$to,$payment_type)
	{
		$this->db->select('students_packages.*,stu_name_english,stu_name_arabic,stu_mobile,stu_email,course_title');
        $this->db->from('students_packages');
		if ($from) {
			$this->db->where('date >=',$from);
		}
		if ($to) {
			$this->db->where('date <=',$to);
		}
		$this->db->where('students_packages.course_id',$course_id);
		if ($payment_type != 'all') {
			$this->db->where('payment_type',$payment_type);
		}
  	    $this->db->join('students','students.stu_id=students_packages.stu_id','left');
  	    $this->db->join('courses','courses.course_id=students_packages.course_id','left');
		return $this->db->get()->result();
	}
	function getCourse($course_id)
	{
	    $this->db->select('course_id,course_title');
		$this->db->from('courses');
		$this->db->where('course_id',$course_id);
		$res = $this->db->get();
		if($res->num_rows() > 0)
		{
		    return $res->row()->course_title;
		}
		else
		{
		    return '';
		}
	}
}
?>
