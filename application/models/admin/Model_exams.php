<?php
class Model_exams extends CI_Model {
    public function insert($table, $data) {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    public function get_details($table, $cond) {
        $res = $this->db->get_where($table, $cond);
        return $res;
    }
    public function update($base_id, $id, $table, $data) {
        $this->db->where($base_id, $id);
        $this->db->update($table, $data);
        return true;
    }
    public function delete($table, $cond) {
        $this->db->delete($table, $cond);
        return true;
    }
	function getSections($course_id)
	{
		return $this->db->select('cs_id,section_title')->from('course_sections')->where('course_id',$course_id)->order_by('cs_id','desc')->get()->result();
	}
	function getBatches($course_id)
	{
		return $this->db->select('*')->from('course_batches')->where('course_id',$course_id)->order_by('cb_id','desc')->get()->result();
	}
	function make_query($course_id)
  {
    $this->db->select('exams.*,batch_name,section_title');
    $this->db->from('exams');
	$this->db->join('courses','courses.course_id=exams.course_id','left');
	$this->db->join('course_batches','course_batches.cb_id=exams.cb_id','left');
	$this->db->join('course_sections','course_sections.cs_id=exams.cs_id','left');
	$this->db->where('exams.course_id',$course_id);
    if (isset($_POST["search"]["value"]))
    {
      $this->db->like("exam_name",$_POST["search"]["value"]);
    }
    if (isset($_POST["order"]))
    {
      $this->db->order_by($_POST['order']['0']['column'],$_POST['order']['0']['dir']);
    }
    else
    {
      $this->db->order_by("exam_id","desc");
    }
  }

  function make_datatables($course_id)
  {
    $this->make_query($course_id);
    if ($_POST["length"] != -1)
    {
      $this->db->limit($_POST["length"],$_POST["start"]);
    }
    $query = $this->db->get();
    return $query->result();
  }

  function get_filtered_data($course_id)
  {
    $this->make_query($course_id);
    $query = $this->db->get();
    return $query->num_rows();
  }

  function get_all_data($course_id)
  {
    $date  = date('Y-m-d');
    $this->db->select("*");
    $this->db->from("exams");
	$this->db->where('exams.course_id',$course_id);
    return $this->db->count_all_results();
  }
    public function getLeaderBoard($exam_id)
    {
        $this->db->select('students.stu_id,stu_name_english,stu_name_arabic, stu_mobile, students_exams.*');
        $this->db->from('students_exams');
        $this->db->where('exam_id',$exam_id);
        $this->db->join('students','students.stu_id=students_exams.stu_id');
        $this->db->order_by('mark','desc');
        return $this->db->get()->result();
    }
    function getAllExams()
    {
        $this->db->select('exam_id,exam_name');
        $this->db->from("exams");
	    $this->db->where('exam_status','1');
	    $this->db->where('registration_status','1');
        return $this->db->get()->result();
    }
    public function duplicate($ex_id,$exam_id)
	{
		$table = 'exam' . $ex_id;
		$query = 'CREATE TEMPORARY TABLE ' . $table . ' SELECT * FROM questions WHERE exam_id = ' . $exam_id . ';';
		$this->db->query($query);
		$query1 = 'UPDATE ' . $table . ' SET que_id=NULL,exam_id=' . $ex_id .';';
		$this->db->query($query1);
		$query2 = 'INSERT INTO questions SELECT * FROM ' . $table . ' WHERE exam_id = ' . $ex_id . ';';
		$this->db->query($query2);
		return true;
	}
	public function getExamBySe_id($se_id)
	{
		$this->db->select('se_id,course_id,students_exams.exam_id');
        $this->db->from('students_exams');
		$this->db->join('exams','students_exams.exam_id=exams.exam_id');
	    $this->db->where('se_id',$se_id);
        return $this->db->get();
	}
}
?>
