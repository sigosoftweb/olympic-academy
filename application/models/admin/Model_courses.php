<?php
class Model_courses extends CI_Model {
    public function insert($table, $data) {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    public function get_details($table, $cond) {
        $res = $this->db->get_where($table, $cond);
        return $res;
    }
    public function update($base_id, $id, $table, $data) {
        $this->db->where($base_id, $id);
        $this->db->update($table, $data);
        return true;
    }
    public function delete($table, $cond) {
        $this->db->delete($table, $cond);
        return true;
    }
	public function getTrainers()
	{
		return $this->db->select('trainer_id,trainer_name_english')->from('trainers')->where('trainer_status','1')->get()->result();
	}
	public function getCategories()
	{
		return $this->db->select('cat_id,cat_name')->from('categories')->where('cat_status','1')->get()->result();
	}
	public function getCourseTeachers($course_id)
	{
		$this->db->select('trainers.*,ct_id,ct_status');
		$this->db->from('course_trainers');
		$this->db->where('course_id',$course_id);
		$this->db->join('trainers','trainers.trainer_id=course_trainers.trainer_id','left');
		return $this->db->get()->result();
	}
	function make_query(){
      $this->db->select('*');
      $this->db->from('courses');
      if (isset($_POST["search"]["value"])) {
		  $search = $_POST["search"]["value"];
          $this->db->where("(course_title LIKE '%" . $search .  "%')",NULL,false);
      }
      if (isset($_POST["order"])) {
        $this->db->order_by($_POST['order']['0']['column'],$_POST['order']['0']['dir']);
      }
      else {
        $this->db->order_by("course_id","desc");
      }
    }
    function make_datatables()
    {
      $this->make_query();
      if ($_POST["length"] != -1) {
        $this->db->limit($_POST["length"],$_POST["start"]);
      }
      $query = $this->db->get();
      return $query->result();
    }
    function get_filtered_data()
    {
      $this->make_query();
      $query = $this->db->get();
      return $query->num_rows();
    }
    function get_all_data()
    {
      $this->db->select("*");
      $this->db->from("courses");
      return $this->db->count_all_results();
    }
	public function getAvailableTeachers($course_id)
	{
		$q =$this->db->select(array(
            't1.trainer_id',
            't1.trainer_name_english'))
            ->from('trainers AS t1')
            ->where('t1.trainer_status','1')
            ->where('t1.trainer_id NOT IN (select trainer_id from course_trainers where course_id = ' . $course_id . ')',NULL,FALSE)
            ->order_by('t1.trainer_id','desc')->get()->result();
		return $q;
	}
	function getSections($course_id)
	{
		return $this->db->select('*')->from('course_sections')->where('course_id',$course_id)->order_by('section_start_time','desc')->get()->result();
	}
	function getBatches($course_id)
	{
		return $this->db->select('*')->from('course_batches')->where('course_id',$course_id)->order_by('cb_id','desc')->get()->result();
	}
	public function getCourseTrainers($course_id)
	{
		$this->db->select('trainers.trainer_id,trainer_name_english');
		$this->db->from('course_trainers');
		$this->db->where('course_id',$course_id);
		$this->db->where('ct_status','1');
		$this->db->join('trainers','trainers.trainer_id=course_trainers.trainer_id','left');
		return $this->db->get()->result();
	}
	function getCourseSections($course_id)
	{
		return $this->db->select('cs_id,section_title')->from('course_sections')->where('course_id',$course_id)->where('section_status','1')->order_by('cs_id','desc')->get()->result();
	}
	function getCourseBatches($course_id)
	{
		return $this->db->select('cb_id,batch_name')->from('course_batches')->where('course_id',$course_id)->order_by('cb_id','desc')->get()->result();
	}
	public function getClasses($course_id)
	{
		$this->db->select('course_classes.*,batch_name,trainer_name_english,section_title');
		$this->db->from('course_classes');
		$this->db->where('course_classes.course_id',$course_id);
		$this->db->join('trainers','trainers.trainer_id=course_classes.trainer_id','left');
		$this->db->join('course_batches','course_batches.cb_id=course_classes.cb_id','left');
		$this->db->join('course_sections','course_sections.cs_id=course_classes.cs_id','left');
		return $this->db->get()->result();
	}
	function getSubscribedStudents($course_id)
	{
		$this->db->select('students.*');
		$this->db->from('students_packages');
		$this->db->where('course_id',$course_id);
		$this->db->join('students','students.stu_id=students_packages.stu_id','left');
		return $this->db->get()->result();
	}
	function getUnassignedStudents($cb_id,$course_id)
	{
		$q =$this->db->select('students.*')
            ->from('students_packages AS t1')
			->join('students','students.stu_id=t1.stu_id')
            ->where('t1.course_id',$course_id)
            ->where('t1.stu_id NOT IN (select stu_id from course_batch_students where course_id = ' . $course_id . ')',NULL,FALSE)
            ->order_by('t1.stu_id','desc')->get()->result();
		return $q;
	}
	function getBatchStudents($cb_id)
	{
		$this->db->select('students.*');
		$this->db->from('course_batch_students');
		$this->db->where('cb_id',$cb_id);
		$this->db->join('students','students.stu_id=course_batch_students.stu_id','left');
		return $this->db->get()->result();
	}
	function getBatchAttendance($cb_id,$date)
	{
		$this->db->select('stu_name_english,course_attendance.*');
		$this->db->from('attendance');
		$this->db->where('cb_id',$cb_id);
		$this->db->where('attendance.date',$date);
		$this->db->join('course_attendance','attendance.att_id=course_attendance.att_id');
		$this->db->join('students','students.stu_id=course_attendance.stu_id','left');
		return $this->db->get()->result();
	}
	function getAssignments($course_id)
	{
		$this->db->select('course_assignments.*,batch_name');
		$this->db->from('course_assignments');
		$this->db->where('course_assignments.course_id',$course_id);
		$this->db->join('course_batches','course_assignments.cb_id=course_batches.cb_id','left');
		$this->db->order_by('ca_id','desc');
		return $this->db->get()->result();
	}
	function getStudentsAssignment($ca_id)
	{
		$this->db->select('course_students_assignments.*,stu_name_arabic,stu_name_english');
		$this->db->from('course_students_assignments');
		$this->db->where('ca_id',$ca_id);
		$this->db->join('students','course_students_assignments.stu_id=students.stu_id');
		$this->db->order_by('csa_id','desc');
		return $this->db->get()->result();
	}
	function getOrganisers()
	{
	    $this->db->select('organiser_id,organiser_first,organiser_second');
		$this->db->from('organisers');
		$this->db->where('organiser_status','1');
		$this->db->order_by('organiser_id','desc');
		return $this->db->get()->result();
	}
	function getAllCourses()
	{
	    $this->db->select('course_id,course_title');
		$this->db->from('courses');
		$this->db->where('course_status','1');
		$this->db->order_by('course_id','desc');
		return $this->db->get()->result();
	}
	function getCourse($cb_id)
	{
		$course = $this->db->select('batch_name,course_title')
		->from('course_batches')
		->where('cb_id',$cb_id)
		->join('courses','courses.course_id=course_batches.course_id','left')
		->get();

		return $course;
	}
	function getResources($course_id)
	{
		$resources = $this->db->select('*')
		->from('course_resources')
		->where('course_id',$course_id)
		->order_by('cr_id','desc')
		->get()->result();

		return $resources;
	}
	function getCourseOrganisers($course_id)
	{
		$organisers = $this->db->select('organiser_id')
		->from('course_organisers')
		->where('course_id',$course_id)
		->get()->result();

		return $organisers;
	}
}
?>
