<?php
class Model_trainers extends CI_Model {
    public function insert($table, $data) {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    public function get_details($table, $cond) {
        $res = $this->db->get_where($table, $cond);
        return $res;
    }
    public function update($base_id, $id, $table, $data) {
        $this->db->where($base_id, $id);
        $this->db->update($table, $data);
        return true;
    }
    public function delete($table, $cond) {
        $this->db->delete($table, $cond);
        return true;
    }
	function make_query(){
      $this->db->select('*');
      $this->db->from('trainers');
      if (isset($_POST["search"]["value"])) {
		  $search = $_POST["search"]["value"];
          $this->db->where("(trainer_name_english LIKE '%" . $search .  "%' OR trainer_name_arabic LIKE '%" . $search . "%')",NULL,false);
      }
      if (isset($_POST["order"])) {
        $this->db->order_by($_POST['order']['0']['column'],$_POST['order']['0']['dir']);
      }
      else {
        $this->db->order_by("trainer_id","desc");
      }
    }
    function make_datatables()
    {
      $this->make_query();
      if ($_POST["length"] != -1) {
        $this->db->limit($_POST["length"],$_POST["start"]);
      }
      $query = $this->db->get();
      return $query->result();
    }
    function get_filtered_data()
    {
      $this->make_query();
      $query = $this->db->get();
      return $query->num_rows();
    }
    function get_all_data()
    {
      $this->db->select("*");
      $this->db->from("trainers");
      return $this->db->count_all_results();
    }
}
?>
