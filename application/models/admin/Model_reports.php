<?php
class Model_reports extends CI_Model {

    public function getCources()
    {
        $this->db->select('*');
        $this->db->from('courses');
        $this->db->where('course_status','1');
        return $this->db->get()->result();
    }
 
    function getSubscribedCount($course_id,$start_date,$end_date)
	{
		$this->db->select('students_packages.*');
		$this->db->from('students_packages');
		$this->db->where('course_id',$course_id);
		$this->db->where('DATE(students_packages.date)>=',$start_date);
		$this->db->where('DATE(students_packages.date)<=',$end_date);
		return $this->db->get()->num_rows();
	}
	
	function getEarning($course_id,$start_date,$end_date)
	{
	    $this->db->select_sum('amount');
		$this->db->from('students_packages');
		$this->db->where('course_id',$course_id);
		$this->db->where('DATE(date)>=',$start_date);
		$this->db->where('DATE(date)<=',$end_date);
		$this->db->where('sp_status','Approved');
		$sum = $this->db->get()->row()->amount;
		if($sum == '')
		{
			return 0;
		}
		else {
			return $sum;
		}
	}
	
	function getOnlinePayments($course_id,$start_date,$end_date)
	{
	    $this->db->select_sum('amount');
		$this->db->from('students_packages');
		$this->db->where('course_id',$course_id);
		$this->db->where('DATE(date)>=',$start_date);
		$this->db->where('DATE(date)<=',$end_date);
		$this->db->where('payment_type','Online');
		$this->db->where('sp_status','Approved');
		$sum = $this->db->get()->row()->amount;
		if($sum == '')
		{
			return 0;
		}
		else {
			return $sum;
		}
	}
	
	function getTamkeenPayments($course_id,$start_date,$end_date)
	{
	    $this->db->select_sum('amount');
		$this->db->from('students_packages');
		$this->db->where('course_id',$course_id);
		$this->db->where('DATE(date)>=',$start_date);
		$this->db->where('DATE(date)<=',$end_date);
		$this->db->where('payment_type','Tamkeen');
		$this->db->where('sp_status','Approved');
		$sum = $this->db->get()->row()->amount;
		if($sum == '')
		{
			return 0;
		}
		else {
			return $sum;
		}
	}
	
	function getCashPayments($course_id,$start_date,$end_date)
	{
	    $this->db->select_sum('amount');
		$this->db->from('students_packages');
		$this->db->where('course_id',$course_id);
		$this->db->where('DATE(date)>=',$start_date);
		$this->db->where('DATE(date)<=',$end_date);
		$this->db->where('payment_type','By cash');
		$this->db->where('sp_status','Approved');
		$sum = $this->db->get()->row()->amount;
		if($sum == '')
		{
			return 0;
		}
		else {
			return $sum;
		}
	}
	
	function getSubscribedStudents($course_id,$start_date,$end_date)
	{
		$this->db->select('students.*');
		$this->db->from('students_packages');
		$this->db->join('students','students.stu_id=students_packages.stu_id');
		$this->db->where('students_packages.course_id',$course_id);
		$this->db->where('DATE(students_packages.date)>=',$start_date);
		$this->db->where('DATE(students_packages.date)<=',$end_date);
		return $this->db->get()->result();
	}
	
	function getSubscribedCountAll($course_id)
	{
		$this->db->select('students_packages.*');
		$this->db->from('students_packages');
		$this->db->where('course_id',$course_id);
		return $this->db->get()->num_rows();
	}
	
	function getEarningAll($course_id)
	{
	    $this->db->select_sum('amount');
		$this->db->from('students_packages');
		$this->db->where('course_id',$course_id);
		$this->db->where('sp_status','Approved');
		$sum = $this->db->get()->row()->amount;
		if($sum == '')
		{
			return 0;
		}
		else {
			return $sum;
		}
	}
	
	function getOnlinePaymentsAll($course_id)
	{
	    $this->db->select_sum('amount');
		$this->db->from('students_packages');
		$this->db->where('course_id',$course_id);
		$this->db->where('payment_type','Online');
		$this->db->where('sp_status','Approved');
		$sum = $this->db->get()->row()->amount;
		if($sum == '')
		{
			return 0;
		}
		else {
			return $sum;
		}
	}
	
	function getTamkeenPaymentsAll($course_id)
	{
	    $this->db->select_sum('amount');
		$this->db->from('students_packages');
		$this->db->where('course_id',$course_id);
		$this->db->where('payment_type','Tamkeen');
		$this->db->where('sp_status','Approved');
		$sum = $this->db->get()->row()->amount;
		if($sum == '')
		{
			return 0;
		}
		else {
			return $sum;
		}
	}
	
	function getCashPaymentsAll($course_id)
	{
	    $this->db->select_sum('amount');
		$this->db->from('students_packages');
		$this->db->where('course_id',$course_id);
		$this->db->where('payment_type','By cash');
		$this->db->where('sp_status','Approved');
		$sum = $this->db->get()->row()->amount;
		if($sum == '')
		{
			return 0;
		}
		else {
			return $sum;
		}
	}
	
	function getSubscribedStudentsAll($course_id)
	{
		$this->db->select('students.*');
		$this->db->from('students_packages');
		$this->db->join('students','students.stu_id=students_packages.stu_id');
		$this->db->where('students_packages.course_id',$course_id);
		return $this->db->get()->result();
	}
 
}
?>
