<?php
class Model_classes extends CI_Model {
    function make_query() {
        $this->db->select('course_classes.*,course_title');
        $this->db->from('course_classes');
        $this->db->join('courses', 'courses.course_id=course_classes.course_id','left');
        if (isset($_POST["search"]["value"])) {
			$search = $_POST["search"]["value"];
            $this->db->where("(topic LIKE '%" . $search .  "%' OR course_title LIKE '%" . $search . "%')",NULL,false);
        }
        if (isset($_POST["order"])) {
            $this->db->order_by($_POST['order']['0']['column'], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by("cc_id", "desc");
        }
    }
    function make_datatables() {
        $this->make_query();
        if ($_POST["length"] != - 1) {
            $this->db->limit($_POST["length"], $_POST["start"]);
        }
        $query = $this->db->get();
        return $query->result();
    }
    function get_filtered_data() {
        $this->make_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function get_all_data() {
        $date = date('Y-m-d');
        $this->db->select("*");
        $this->db->from("course_sections");
        return $this->db->count_all_results();
    }
}
?>
