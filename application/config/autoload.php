<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();
$autoload['libraries'] = array('session','database','timezone');
$autoload['drivers'] = array();
$autoload['helper'] = array('function','url');
$autoload['config'] = array();
$autoload['language'] = array('arabic');
$autoload['model'] = array();
